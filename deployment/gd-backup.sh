#!/bin/bash

export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

## date format ##
NOW=$(date +"%F")

## Backup path ##
PGBAK="/media/yandex-disk/pg_dumps/"
PGDUMP="/home/webserver/pg-dumps/pg-"$NOW".backup"
IMAGES="/home/webserver/gd-clojure/resources/public/img/uploaded/original/"

function mount-yandex
{
    expect -c 'spawn mount.davfs https://webdav.yandex.ru /media/yandex-disk
expect "Username:"
send "dayte-dve\r"
expect "Password:"
send "Mig(Uwki3\r"
expect eof'
    logger "Yandex Disk Mounted"
}

function dump-postgres
{
    cd /var/lib/postgresql/
    export PGPASSWORD='1q%!@GSasd!@G'
    logger "Make group-deals dump"
    pg_dump -h 127.0.0.1 -U postgres -Fc group_deals -f pg-dump || touch "/home/webserver/pg-dumps/BAD.BACKUP"
    mv pg-dump $PGDUMP
    logger "PGDUMP " $PGDUMP " Created"
}

function backup-images
{
    rsync --progress -uogthvr $IMAGES /media/yandex-disk/images/
}

cd /home/webserver/
mount-yandex

dump-postgres
cp $PGDUMP $PGBAK

# backup-images

sync                            # wait while webdav is synced

umount /media/yandex-disk && logger "Yandex disk correctly unmounted"
