# IMPORTANT : this should not be used as fully non-interactive script. This is just command log and you MUST copy each part in shell by hands.
#!/bin/bash

set +e

# Create user 'webserver'
useradd -m webserver -s /bin/bash

# Setup SSH keys for 'webserver'
mkdir -p /home/webserver/.ssh

cat > /home/webserver/.ssh/id_rsa <<EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAyjFDteglB0ZwVdYq4GCDWXS6COkSAg+BMXzJPI1FhgCayt0S
HhcBJrR1cOsB9RMKZkGCBT7LqTwUTb+Ctd8dQcdDuxQj2l5j5xPP2pdej4Q6Y25U
KO3LtLpHODNc5SpUTN/vnpaiRMRO4MdYC9VOZLttG3Wk3y/alTpzBckTTPJyTqkX
36ygjmcEt4L83EYhTFCi585+R0sEeMucaW9fof5axfAOm4l2J4cXF7D/vxZRLBgT
Uib5MgT2S0X2C+/1+1lMP1OtsM1jikXNqJs55Eo5KIFh1IjWFlcD0B2sOVXphiUm
Mxt0aGb9xTpD0TWk5iPC4aH8izTf+28qj+HbrQIDAQABAoIBAA93Rzuh3zs4Ox2F
vU/ecg2krRpDYbpAn1w+tFHqaHgAkdaUpG8iyVbBlP4nK8YoR7JcrmpiTEfMrazo
ykHJKD7A1EB6IOfyMmgZjtukmnoV3jV+2fcrgVp4qTv2SdPJshsTQgv6b70c2v+8
n9J/5Xg1Efp4vDzKN0kU6Hl30P8gpYrcD/ZZiHqWWxlnIchNR+s6hrtiIxdsspA7
DFC/sNe4tUiZoaPMwm+fMg4EmrbZjPS0hKp1SyIc/n0eUMmWgLD38f4EsMiPC4Hn
v6fm7kds6fkG7FxEwP/fHbulNO5gImpMLMLSuPk5nSgEF13dFpD7I7rmENNIOfQw
rZI0ho0CgYEA9yp7uAkztVug5m2KXr+1B0MTiCw6ysURyL4O6mg9FOmqoHVjTL3P
RlFASUNM4ssitQ0z6+mPUR1L7RMGzVOmN7CzgF98IcAqr3lyeHQZZP7z4/uyFFYp
y/o9W8qH8WqOuy6jEW5aSKiD81MwbV+Lh1XFQ7wezO11LVCw8h7DHX8CgYEA0WtI
cveJ4b6nzOZt6NoS9FKMafFObgdQxqLOcFe0Ok5vw3Cf2BwCSCAje2VwfCuM31z2
+pKfcIzTj2srtQ665N/VZJ5JsSe+8K9gTQBJV48GFfTykgQcR+uUmGhNCBgBTmPW
FW67kFrLerXKwAcddFLZH7Mu8G2kDvmpJFPNdNMCgYEAyHImpYio67JkmZtKdS6p
xDrssymTgHAzzQX9mLJynuwlWT36CvmACHnKVeADkYXGHuiXLAJdHR5SPfCJOqlV
CqL93+UQCVsMAwTOBcu58IMnDcRIll+ORAMsEa39c5bELB9VVcSV6YjogcP8aSbs
RBQWB6mfnPgSaG6hIao6+EcCgYB2DILPg+hun3mZkD9epidPIIVUZxkt/uOyxS2X
uGi5AWQRCJiiSs+idrNpqLnq1OXZN19lymckG85aJzCBJKeHwwFYcb3apiwFIucn
IMU3HUFx+R0P4E9M32hpLUpXCKkb1j1k4YHLktLm/jknXBtnahhIvUn1b+iuJGAL
QU4szwKBgQDCJNM0r7+nN4JpqGUjtrdLHevxNFUXJ2SwTNvZ47t3hJGVW3+/V4bj
Qm3p6gqSJfkYZtlXIWwrXWbP2NLXxnz8zlQBBN+qIr+FF4xDGJRBrfLYl+k+nrH7
/qfQUI1okZpr+2cF8SO41N9/waR7xgw7KvkMB08UwysGw/T5IdeOKg==
-----END RSA PRIVATE KEY-----
EOF

cat > /home/webserver/.ssh/id_rsa.pub <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKMUO16CUHRnBV1irgYINZdLoI6RICD4ExfMk8jUWGAJrK3RIeFwEmtHVw6wH1EwpmQYIFPsupPBRNv4K13x1Bx0O7FCPaXmPnE8/al16PhDpjblQo7cu0ukc4M1zlKlRM3++elqJExE7gx1gL1U5ku20bdaTfL9qVOnMFyRNM8nJOqRffrKCOZwS3gvzcRiFMUKLnzn5HSwR4y5xpb1+h/lrF8A6biXYnhxcXsP+/FlEsGBNSJvkyBPZLRfYL7/X7WUw/U62wzWOKRc2omznkSjkogWHUiNYWVwPQHaw5VemGJSYzG3RoZv3FOkPRNaTmI8LhofyLNN/7byqP4dut webserver@x2vm42.xen2.ukrnames.com
EOF

chmod 0755 /home/webserver/.ssh
chmod 0600 /home/webserver/.ssh/id_rsa
chmod 0644 /home/webserver/.ssh/id_rsa.pub

chown webserver:webserver -R /home/webserver/

# Clone repository
apt-get install --assume-yes git
su -c "cd ~;git clone git@bitbucket.org:artem_barmin/gd-clojure.git" webserver

# Setup hooks
cp /home/webserver/gd-clojure/deployment/post-merge /home/webserver/gd-clojure/.git/hooks
chmod +x /home/webserver/gd-clojure/.git/hooks/post-merge
chown -R webserver:webserver /home/webserver

# Install postgres
wget -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ wheezy-pgdg main" >> /etc/apt/sources.list
aptitude update
aptitude install postgresql-common postgresql-9.3 libpq -t wheezy-pgdg
cp /home/webserver/gd-clojure/deployment/postgres/postgresql.conf /etc/postgresql/9.3/main/postgresql.conf

# Install mongodb
apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
echo "deb http://downloads-distro.mongodb.org/repo/debian-sysvinit dist 10gen"  >> /etc/apt/sources.list
aptitude update
apt-get install --assume-yes mongodb-10gen
/etc/init.d/mongodb restart

# periodical restart of mongo(to prevent memory leaks)
ln -s /home/webserver/gd-clojure/deployment/mongo-restart.sh /etc/cron.daily/mongo-restart

# Change password for postgres user
FILE=`mktemp`
echo ALTER USER postgres with password ''\''1q%!@GSasd!@G'\''' > $FILE
chmod +r $FILE
su -c "psql -f $FILE" postgres
rm -f $FILE

# Create database group-deals
su -c "psql -c 'create database group_deals;'" postgres

# Install nginx
echo "deb http://nginx.org/packages/debian/ wheezy nginx" >> /etc/apt/sources.list
wget -O key http://nginx.org/keys/nginx_signing.key && apt-key add key && rm -f key
apt-get install --assume-yes nginx

# Copy configs for nginx
cp -Rvf /home/webserver/gd-clojure/deployment/nginx/* /etc/nginx

# Restart nginx
service nginx restart

# install and configure monit
apt-get install --assume-yes monit
cp -Rvf /home/webserver/gd-clojure/deployment/monit/* /etc/monit

# Install java
apt-get install --assume-yes curl

# Install lein
curl https://raw.github.com/technomancy/leiningen/1.7.1/bin/lein > lein
mv lein /usr/bin
chmod +x /usr/bin/lein

# download and install java from oracle + 32 bit subsystem
apt-get install libc6-i386
/usr/bin/curl -fLC - --retry 3 --retry-delay 3 -O --header "Cookie: gpw_e24=h" http://download.oracle.com/otn-pub/java/jdk/7u40-b43/jdk-7u40-linux-i586.tar.gz
mkdir -p /usr/lib/java7/
tar -C /usr/lib/java7/ -zxvf ./jdk-7u40-linux-i586.tar.gz

# update debian alternatives
update-alternatives --install /usr/bin/java java /usr/lib/java7/jdk1.7.0_40/jre/bin/java 3
update-alternatives --config java

# install nodejs from experimental repository
echo "deb http://ftp.debian.org/debian experimental main contrib non-free" >> /etc/apt/sources.list
echo "deb http://ftp.debian.org/debian sid main contrib non-free" >> /etc/apt/sources.list
apt-get update
apt-get -t experimental install --assume-yes nodejs

# set timezone to zaporozhye
echo "TZ='Europe/Zaporozhye'; export TZ" > /etc/profile

# add rules to firewall
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT ! -i lo -d 127.0.0.0/8 -j REJECT

iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -j ACCEPT

iptables -A INPUT -p tcp --dport 22 -i eth0 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -i eth0 -j ACCEPT

iptables -A INPUT -j REJECT
iptables -A FORWARD -j REJECT

apt-get install --assume-yes iptables-persistent

#install backup scripts
apt-get install --assume-yes expect
apt-get install --assume-yes davfs2
mkdir /media/yandex-disk
mkdir /home/webserver/pg-dumps/
chmod +x /home/webserver/gd-clojure/deployment/gd-backup.sh
ln -s /home/webserver/gd-clojure/deployment/gd-backup.sh /etc/cron.daily/gd-backup

# install graphicsmagick
apt-get install --assume-yes libjpeg-dev libpng-dev

wget "http://downloads.sourceforge.net/project/graphicsmagick/graphicsmagick/1.3.18/GraphicsMagick-1.3.18.tar.gz" -O GraphicsMagick.tar.gz
tar -xzf GraphicsMagick.tar.gz
cd GraphicsMagick-1.3.18
./configure  '--prefix=/usr' '--enable-shared' '--disable-static' 'CFLAGS=-march=x86-64 -mtune=generic -O3 -pipe -fstack-protector --param=ssp-buffer-size=4' 'LDFLAGS=-Wl,-O1,--sort-common,--as-needed,-z,relro' 'CPPFLAGS=-D_FORTIFY_SOURCE=2' 'CXXFLAGS=-march=x86-64 -mtune=generic -O3 -pipe -fstack-protector --param=ssp-buffer-size=4'
make
make install

# install rsync(needed for backup of images)
apt-get install rsync
