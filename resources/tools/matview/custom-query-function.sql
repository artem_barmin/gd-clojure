CREATE OR REPLACE FUNCTION {{fn-name}}() RETURNS TRIGGER AS $$
DECLARE
BEGIN
    IF TG_OP = 'INSERT' THEN

        DELETE FROM {{mat-view}} where {{mat-dep-id}}=({{new-query}});

        INSERT INTO {{mat-view}}
        SELECT * FROM {{simple-view}} WHERE {{dep-id}}=({{new-query}});

        RETURN NEW;
    ELSIF TG_OP = 'UPDATE' THEN

        DELETE FROM {{mat-view}} where {{mat-dep-id}}=({{old-query}});

        INSERT INTO {{mat-view}}
        SELECT * FROM {{simple-view}} WHERE {{dep-id}}=({{new-query}});

        RETURN NEW;
    ELSIF TG_OP = 'DELETE' THEN

        DELETE FROM {{mat-view}} where {{mat-dep-id}}=({{old-query}});

        INSERT INTO {{mat-view}}
        SELECT * FROM {{simple-view}} WHERE {{dep-id}}=({{old-query}});

        RETURN OLD;
    END IF;
END;
$$ LANGUAGE plpgsql;
