CREATE OR REPLACE FUNCTION {{fn-name}}() RETURNS TRIGGER AS $$
DECLARE
BEGIN
    IF TG_OP = 'INSERT' THEN

        DELETE FROM {{mat-view}} where {{mat-dep-id}}=NEW.{{source-table-field}};

        INSERT INTO {{mat-view}}
        SELECT * FROM {{simple-view}} WHERE {{dep-id}}=NEW.{{source-table-field}};

        RETURN NEW;
    ELSIF TG_OP = 'UPDATE' THEN

        DELETE FROM {{mat-view}} where {{mat-dep-id}}=OLD.{{source-table-field}};

        INSERT INTO {{mat-view}}
        SELECT * FROM {{simple-view}} WHERE {{dep-id}}=NEW.{{source-table-field}};

        RETURN NEW;
    ELSIF TG_OP = 'DELETE' THEN

        DELETE FROM {{mat-view}} where {{mat-dep-id}}=OLD.{{source-table-field}};

        INSERT INTO {{mat-view}}
        SELECT * FROM {{simple-view}} WHERE {{dep-id}}=OLD.{{source-table-field}};

        RETURN OLD;
    END IF;
END;
$$ LANGUAGE plpgsql;
