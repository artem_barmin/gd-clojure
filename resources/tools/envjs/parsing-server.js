require('envjs/jquery');
require('transformer').JsonML;

Envjs.scriptTypes[''] = false;
Envjs.scriptTypes['text/javascript'] = false;
Envjs.scriptTypes['text/envjs'] = false;
Envjs.javaEnabled = false;
document.async = false;

var port = parseInt(process.env.START_PORT) || 1337;

// start tcp server for listening
var net = require('net');

process.on('uncaughtException', function (err) {
               console.log(err.stack);
           });

function selectorToNodes(matchedNodes)
{
    var selectorResult = new Array();

    for(var j = 0;j<matchedNodes.size();j++)
    {
        selectorResult.push(JsonML.parseDOM(matchedNodes.get(j)));
    }

    return selectorResult;
}

function jqueryRequest(selector)
{
    return $(selector);
}

net.createServer(function (socket) {
                     socket.on('data',function(data)
                               {
                                   var request = JSON.parse(data);
                                   if (request.stop)
                                   {
                                       socket.end("0");
                                       process.exit(0);
                                   }
                                   else
                                   {
                                       var processRequest = function() {
                                           try
                                           {

                                               console.log('loaded : ' + request.url + ' ' + document.title);
                                               var result = new Array();
                                               for(var i in request.selectors)
                                               {
                                                   var selectorRequest = request.selectors[i];
                                                   var requestResult = null;

                                                   // case : return list of matched DOM nodes
                                                   if (typeof selectorRequest == "string")
                                                   {
                                                       requestResult = {type:"simple",selector:selectorRequest,
                                                                        values:selectorToNodes(jqueryRequest(selectorRequest))};
                                                   }

                                                   // case : return list of nested DOM nodes, relative to parent
                                                   if (selectorRequest!=null && typeof selectorRequest == "object")
                                                   {
                                                       var parents = jqueryRequest(selectorRequest[0]);
                                                       var rest = selectorRequest.slice(1);

                                                       var childGroups = new Array();

                                                       // move throught all found parents and search for specific childs
                                                       for(var parentI=0;parentI<parents.size();parentI++)
                                                       {
                                                           var childResults = new Array();
                                                           childResults.push(selectorToNodes(jqueryRequest(parents[parentI])));
                                                           for(var child in rest)
                                                           {
                                                               childResults.push(selectorToNodes(jqueryRequest(parents[parentI]).find(rest[child])));
                                                           }
                                                           childGroups.push(childResults);
                                                       }
                                                       requestResult = {type:"composite",selector:selectorRequest, values:childGroups};
                                                   }

                                                   // empty result
                                                   if (requestResult == null)
                                                   {
                                                       requestResult = {type:"empty"};
                                                   }

                                                   // store result
                                                   result.push(requestResult);
                                               }
                                               socket.write(JSON.stringify(result));
                                               document.removeEventListener('load',processRequest);
                                               socket.end("");
                                           }
                                           catch(e)
                                           {
                                               document.removeEventListener('load',processRequest);
                                               if (e.message.match(/Syntax error/))
                                               {
                                                   console.log("Bad selector");
                                                   socket.write(JSON.stringify("bad selector"));
                                               }
                                               else
                                               {
                                                   console.log(e.stack);
                                               }
                                               socket.end("");
                                           }
                                       };
                                       console.log('loading : ' + request.url);
                                       document.addEventListener('load',processRequest);
                                       document.location = request.url;
                                   }
                                   return;
                               });
                 }).listen(port, '127.0.0.1');

console.log('Listen on ' + port);
