CREATE OR REPLACE FUNCTION {{function}}() RETURNS TRIGGER AS $$
DECLARE
BEGIN
    IF TG_OP = 'INSERT' THEN
        PERFORM pg_notify('cache_invalidation', 'INSERT:{{table}}:' || {{{insert}}});
        RETURN NEW;
    ELSIF TG_OP = 'UPDATE' THEN
        PERFORM pg_notify('cache_invalidation', 'UPDATE:{{table}}:' || {{{update}}});
        RETURN NEW;
    ELSIF TG_OP = 'DELETE' THEN
        PERFORM pg_notify('cache_invalidation', 'DELETE:{{table}}:' || {{{delete}}});
        RETURN OLD;
    END IF;
    return NEW;
END;
$$ LANGUAGE plpgsql;
