settings = {};

$(function()
  {

      d = document.location.search.substr(1);
      var p = d.split("&");
      for (i = 0; i < p.length; i++) {
          var curr = p[i].split('=');
          settings[curr[0]] = curr[1];
      }

      VK.addCallback("onSettingsChanged", onSettingsChanged);
      function onSettingsChanged(settings) {
          if ((settings& 256) > 0) {
              $("#addbutton").hide();
          }
          else {
              $("#delbutton").hide();
          }
      }

      api_settings = settings['api_settings'];
      if ((api_settings & 256) > 0) {
          $("#addbutton").hide();
      }
      else {
          $("#delbutton").hide();
      }

      FriendsOutApp();
  });

function showSettingsBox()
{
    VK.callMethod("showSettingsBox", 256);
}

function Random(L)
{
    return Math.floor(Math.random() * L);
}

var randomFriend;

function FriendsOutApp()
{
    VK.api("execute", { code: 'var fr=API.getFriends({"count":100}); return {"friends": fr,"in_app":API.getAppFriends(),"names":API.users.get({"uids":fr})};'},
           function (data) {
               var friends = data.response.friends;
               var in_app = data.response.in_app;
               var out_app = jQuery.grep(friends, function (id) {
                                             return jQuery.inArray(id, in_app) == -1;
                                         });
               var k = Random(out_app.length);
               var i = jQuery.inArray(out_app[k], friends);
               randomFriend = data.response.names[i];
           });
}

function inviteFriend()
{
    VK.callMethod("showRequestBox", randomFriend.uid, randomFriend.first_name + ' зайди ко мне', '123');
}

function createAlbum()
{
    VK.api("photos.createAlbum",{title: "hello world"});
}

function getAlbums()
{
    VK.api("photos.getAlbums",function(data)
           {
               var albumsToUse
                   = jQuery.grep(data.response,function(album)
                                 {
                                     console.log(album.title);
                                     return album.title=='hello world';
                                 });
               console.log(albumsToUse);
           });
}
