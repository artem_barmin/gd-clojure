var r;

function getHour(hour)
{
    return ((new Date).getHours() + hour - getStartRange() + 1);
}

Raphael.fn.drawGrid = function (h, w, hours, color) {
    path = [];
    for (var hour = 0; hour < hours; hour++) {
        var hourY = hour * Math.round(h/hours);

        var text = this.text(60, hourY + 10, getHour(hour) + ":00").attr("font-weight","bold");
        path = path.concat(["M", 0, hourY + 10, "H", 50]);
        path = path.concat(["M", 50 + text.getBBox().width, hourY + 10, "H", w]);
        var minStep = Math.round(h/(12 * hours));

        // first minute of hour
        var prevMinY = hourY + 1 * minStep;
        var text = this.text(60, prevMinY + 10, getHour(hour) + ":" + (1 * 5));
        path = path.concat(["M", 0, prevMinY + 10, "H", 50]);
        path = path.concat(["M", 50 + text.getBBox().width, prevMinY + 10, "H", w]);

        // all next minutes
        for (var min = 2; min < 12; min++) {
            var minY = hourY + min * minStep;
            if (minY - prevMinY > 20)
            {
                var text = this.text(60, minY + 10, getHour(hour) + ":" + (min * 5));
                path = path.concat(["M", 0, minY + 10, "H", 50]);
                path = path.concat(["M", 50 + text.getBBox().width, minY + 10, "H", w]);
                prevMinY = minY;
            }
        }
    }
    return this.path(path.join(",")).attr({stroke: color});
};

Raphael.fn.wrapText = function(x, y, width, content)
{
    var words = content.split("/");
    var t = this.text(x, y).attr('text-anchor', 'start');
    t.attr("text", content);
    return t;
};

Raphael.fn.drapGraph = function(points)
{
    path = ["M","0","0"];
    for (var i = 0; i < points.length; i++) {
        path = path.concat(["L", points[i].x, points[i].y]);
    }
    path.concat(["Z"]);
    return this.path(path.join(",")).attr({fill: "rgba(0,0,255,0.5)"}).attr("stroke","rgba(0,0,0,0.1)");
}

Raphael.fn.rectWithText = function(x, y, width, height, color, text, info)
{
    var rect = r.rect(x, y, width, height , 7);
    rect.attr({fill: color, stroke: "black", "fill-opacity": 1, "stroke-width": 2});
    var text = r.wrapText(x + 10, y + 9, width, text);

    var sset = r.group();
    sset.push(rect).push(text);

    $(sset.node).mouseover(function(){rect.animate({fill: "yellow"}, 80);});
    $(sset.node).mouseout(function(){rect.animate({fill: color}, 80);});

    $(sset.node).click(function(event)
                       {
                           $("#details")
                               .show()
                               .position({
                                             of: $(sset.node),
                                             my: "left top",
                                             at: "right top",
                                             offset: "150 0"
                                         });
                           $("#details .inspector").empty();
                           var infoToInspect = info;
                           if (info.length == 1)
                               infoToInspect = info[0];
                           $("#details .inspector")
                               .inspect(
                                   infoToInspect,
                                   'Детали',
                                   ["uri","service","timeFormatted"],
                                   ["timeOffset","_id", "source", "del", "timestamp"]);
                           event.stopPropagation();
                       });

    $("body").click(function(){$("#details").hide();});
    $("#details").click(function(event){event.stopPropagation();});

    return sset;
};

var typeColors = {
    "web-request": "orange",
    "service": "#9999ff"
};

var collectionColor = "#99ff99";
var template;

$(document).ready(function()
                  {
                      template = doT.template($("#item-template").text());
                  });

function showDetails(info)
{
    var result = "";
    for (i in info)
    {
        result += template(info[i]);
    }
    return result;
}

function event(scale, info, x)
{
    if (info.length == 1)
    {
        var infoElem = info[0];
        var color = typeColors[infoElem.type];
        var rect = Raphael.fn.rectWithText(x, scale * infoElem.timeOffset + 10, 130, 20, color,
                                           infoElem.uri || infoElem.service, info);
    }
    else if (info.length > 1)
    {
        var color = collectionColor;
        var height = info[info.length-1].timeOffset - info[0].timeOffset;
        var rect = Raphael.fn.rectWithText(x, scale * info[0].timeOffset + 10, 130, scale * height + 20, color,
                                           info.length + " request", info);
    }
}

function groupEvents(scale, events)
{
    var result = [];
    var collection = [events[0]];

    if (events.length <= 1)
    {
        return [events];
    }

    for (var i = 1; i < events.length; i++) {
        if (scale * (events[i].timeOffset - events[i - 1].timeOffset) < 20)
        {
            // add event to current collection
            collection.push(events[i]);
        }
        else
        {
            // flush collection and add it to result list
            result.push(collection);
            collection = [events[i]];
        }
    }

    result.push(collection);

    return result;
}

function drawAll(w,h)
{
    var scale = getRange();      //1 hour
    var visualScale = h / (60 * scale);

    r.drawGrid(h, w, scale, "#aaaaaa");

    var x = 100;
    for (idx in data)
    {
        var events = data[idx][1];
        var source = data[idx][0];

        if (source!=null && source.name && source.id)
        {
            r.wrapText(x, 5, 100, source.name + " (" + source.id + ")");
        }
        else
        {
            r.wrapText(x, 5, 100, "none");
        }

        var groupedEvents = groupEvents(visualScale, events);

        for (var i = 0; i < groupedEvents.length; i++) {
            event(visualScale, groupedEvents[i], x);
        }

        r.path(["M",x-5,10,"L",x-5,h + 15].join(",")).attr({stroke: "blue"});
        r.path(["M",x+150-5,10,"L",x+150-5,h + 15].join(",")).attr({stroke: "blue"});

        x+=150;
    }
}

function redrawAll()
{
    var w = $(window).width();
    var h = ($( "#slider-scale" ).data("slider").value() / 10) * $(window).height();
    r.setSize(w,h);
    r.clear();
    drawAll(w,h - 50);
}

var maxHours = 24;

$(document)
    .ready(
        function()
        {
            $( "#slider-scale" ).slider({range: "min",
			                 value: 10,
			                 min: 1,
			                 max: 100,
			                 slide: function( event, ui ) {
                                             w = $(window).width();
                                             h = (ui.value / 10) * $(window).height();
                                             r.setSize(w,h);
                                             r.clear();
                                             drawAll(w,h - 50);
			                 }
		                        });

            $( "#slider-range" ).slider({range: true,
			                 values: [23,24],
			                 min: 1,
			                 max: maxHours,
			                 slide: function( event, ui ) {
                                             var start = maxHours - ui.values[0];
                                             var end = maxHours - ui.values[1];

                                             $("#range-value").text(((new Date).getHours() - start + 1) + " - " + ((new Date).getHours() - end + 1));
			                 }
		                        });

            // $("#range-value").text(getStartRange() + " - " + getEndRange());

            var w = $(window).width(),h = $(window).height();
            r = Raphael($(".canvas").get(0), w, h);

            drawAll(w,h - 50);

            $(window).resize(redrawAll);

        });

function getStartRange()
{
    return maxHours - $( "#slider-range" ).data("slider").values()[0];
}

function getEndRange()
{
    return maxHours - $( "#slider-range" ).data("slider").values()[1];
}

function getRange()
{
    return $( "#slider-range" ).data("slider").values()[1] - $( "#slider-range" ).data("slider").values()[0];
}

function onLogsLoaded(res)
{
    data = eval(res);
    redrawAll();
}
