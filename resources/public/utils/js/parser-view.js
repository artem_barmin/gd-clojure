var timer = null;

function deepEquals( x, y ) {
    if ( x === y ) return true;
    // if both x and y are null or undefined and exactly the same

    if ( ! ( x instanceof Object ) || ! ( y instanceof Object ) ) return false;
    // if they are not strictly equal, they both need to be Objects

    if ( x.constructor !== y.constructor ) return false;
    // they must have the exact same prototype chain, the closest we can do is
    // test there constructor.

    for ( var p in x ) {
        if ( ! x.hasOwnProperty( p ) ) continue;
        // other properties were tested using x.constructor === y.constructor

        if ( ! y.hasOwnProperty( p ) ) return false;
        // allows to compare x[ p ] and y[ p ] when set to undefined

        if ( x[ p ] === y[ p ] ) continue;
        // if they have the same strict value or identity then they are equal

        if ( typeof( x[ p ] ) !== "object" ) return false;
        // Numbers, Strings, Functions, Booleans must be strictly equal

        if ( ! Object.equals( x[ p ],  y[ p ] ) ) return false;
        // Objects and Arrays must be tested recursively
    }

    for ( p in y ) {
        if ( y.hasOwnProperty( p ) && ! x.hasOwnProperty( p ) ) return false;
        // allows x[ p ] to be set to undefined
    }
    return true;
}

function throttle(f){
    if (timer != null)
    {
        clearTimeout(timer);
    }
    timer = setTimeout(f, 500);
}

wQuery.fn.serializeObject = function()
{
    var form = this;

    var o = {};
    var a = form.serializeArray();
    wQuery.each(a, function() {
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
    return o;
};

wQuery.expr[':'].regex = function(elem, index, match) {
    var matchParams = match[3].split(','),
    validLabels = /^(data|css):/,
    attr = {
        method: matchParams[0].match(validLabels) ?
            matchParams[0].split(':')[0] : 'attr',
        property: matchParams.shift().replace(validLabels,'')
    },
    regexFlags = 'ig',
    regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
}

var prevTarget = null;

function findElement(sel)
{
    return wQuery("body > *:not(.skip-searching)").find(sel);
}

function checkSel(sel,el)
{
    var elts = findElement(sel);
    if (elts.size()==1 && elts.get(0)==el.get(0))
    {
        return sel;
    }
    return false;
}

function isSkiped(target)
{
    return target.parents(".skip-searching,.ui-dialog").size()==0
        && !target.hasClass("skip-searching")
        && !target.hasClass("ui-dialog");
}

function refreshMasonry()
{
    wQuery('#stock-parsing').masonry({singleMode: false,
                                      transitionDuration: 0,
                                      gutter:10,
                                      columnWidth: 300});
}

wQuery(document).ready(function()
                       {
                           refreshMasonry();
                           wQuery("[style*='position: fixed']:not(.skip-searching)").remove()
                           wQuery("[style*='position:fixed']:not(.skip-searching)").remove()
                       });

wQuery( document ).on( "hover", "*",
                       function( e ) {
                           var target = wQuery(e.currentTarget);
                           if (isSkiped(target))
                           {
                               if(prevTarget)
                               {
                                   wQuery(prevTarget).css('border','none');
                               }

                               wQuery(e.currentTarget).css('border','1px solid red');

                               prevTarget=e.currentTarget;

                               wQuery("#current-selector")
                                   .text(computeSelector(wQuery(e.currentTarget)));

                               e.stopPropagation();
                           }
                       } );

wQuery( document ).on( "click", "*",
                       function( e ) {
                           var target = wQuery(e.currentTarget);
                           if (isSkiped(target))
                           {
                               var param = wQuery("[name=param]:checked").val();

                               wQuery("[name='" + param + "\[sel\]']").val(wQuery("#current-selector").text()).trigger('keyup');
                           }
                           e.stopPropagation();
                       } );

function renumberItems(event,ui)
{
    var container = wQuery(ui.item).closest("ul");
    container.find("> li.ui-state-default")
        .each(function(i)
              {
                  var node = wQuery(this);
                  node.find(":regex(name,\[[0-9]+\])")
                      .each(function()
                            {
                                var el = wQuery(this);
                                el.attr("name",el.attr("name").replace(/\[[0-9]+\]/g,"[" + i + "]"));
                            });
              });
}

function closeDialog()
{
    wQuery(".config:visible").data("dialog").close();
}

function afterModalInit()
{
    wQuery(".config:visible").dialog('option', 'position', 'top');

    wQuery(".ui-dialog:visible [id=sortable]").sortable({placeholder: "ui-state-highlight",
                                                         stop: renumberItems});

    wQuery( ".ui-dialog:visible .text-button" ).button();
}

function addFilter(html)
{
    var nextNumber = wQuery(".ui-dialog:visible [id=sortable] li").size();
    var html = html.replace(/---position---/g,nextNumber);
    wQuery(".ui-dialog:visible [id=sortable]").append(html);
    wQuery(".ui-dialog:visible [id=sortable]").sortable('refresh');
}

function removeFilter(el)
{
    wQuery(el).closest(".filter-item").remove();
    wQuery(".ui-dialog:visible [id=sortable]").sortable('refresh');
}

function saveFilters(destination)
{
    var content = wQuery(".ui-dialog:visible [id=sortable] > *").clone();
    wQuery(destination).empty();
    wQuery(destination).append(content);
}

var previousConfig = null;

function refreshStocks(force)
{
    throttle(function()
             {
                 var itemsNum = parseInt(wQuery("#reparse-stocks").val()) || 11;
                 var config = wQuery("#config-form").serializeObject();
                 if (force || !deepEquals(previousConfig,config))
                 {
                     previousConfig = wQuery.extend(true, {}, config);
                     for(var i = 0;i<itemsNum;i++)
                     {
                         var sel = "#stock-parsed" + i;
                         wQuery(sel).html("Loading...");
                         wQuery(sel).load("/stocks-rerender", wQuery.extend(config,{stockNumber: i}),refreshMasonry);
                     }
                 }
             });
}

function refreshSingleStocks(i)
{
    var config = wQuery("#config-form").serializeObject();

    var sel = "#stock-parsed" + i;
    wQuery(sel).html("Loading...");
    wQuery(sel).load("/stocks-rerender", wQuery.extend(config,{stockNumber: i}),refreshMasonry);
}

function fetchStocks()
{
    wQuery("#fetched-stocks").html("Loading...");
    wQuery("#fetched-stocks")
        .load("/stocks-fetch", wQuery("#config-form").serializeObject());
}

function saveConfig()
{
    wQuery("#fetched-stocks").html("Saving...");
    wQuery("#fetched-stocks")
        .load("/save-config", wQuery("#config-form").serializeObject());
}

function loadConfig()
{
    wQuery("#fetched-stocks").html("Loading config...");
    wQuery("#fetched-stocks")
        .load("/load-config",
              wQuery("#config-form").serializeObject(),
              function()
              {
                  window.location.reload();
              });
}

function addParameter(html)
{
    wQuery("#parameters-table").append(html);
}

function splitWords(str,regexp)
{
    return str
        .split(regexp)
        .filter(function(el){
                    return el.length>0;
                }) ;
}

function tagUnique(el)
{
    var tagName = el.prop("tagName");
    if (tagName.match(/(H[0-9])/))
        return checkSel(tagName,el);
    return false;
}

function idUnique(el)
{
    return checkSel("[id=" + el.attr("id") + "]",el);
}

function anyClassUnique(el)
{
    if (el.attr("class"))
    {
        var classes = splitWords(el.attr("class"),/\s+/);
        for(var i = 0;i < classes.length;i++)
        {
            var check = checkSel("." + classes[i],el);
            if(check)
            {
                return check;
            }
        }
    }
    return false;
}

function containsPlusClass(el)
{
    var words = splitWords(el.text(),/[\s:,.]+/).splice(0,5);

    // simple words check
    for(var j = 0;j < words.length;j++)
    {
        var check = checkSel(":contains(" + words[j] + "):last",el);
        if(check)
        {
            return check;
        }
    }

    // word + class check
    var classAttr = el.attr("class");

    if (classAttr)
    {
        var classes = splitWords(classAttr,/\s+/).splice(0,5);
        for(var i = 0;i < classes.length;i++)
        {
            for(var j = 0;j < words.length;j++)
            {
                var check = checkSel("." + classes[i] + ":contains(" + words[j] + ")",el);
                if(check)
                {
                    return check;
                }
            }
        }
    }

    return false;
}

function fullPath(el)
{
    return el.parentsUntil('body')
        .andSelf()
        .map(function() {
                 return this.nodeName + ':eq(' + wQuery(this).index() + ')';
             }).get().join('>');
}

function computeSelector(el)
{
    return tagUnique(el)
        || idUnique(el)
        || anyClassUnique(el)
        || containsPlusClass(el)
        || fullPath(el);
}

function startMerger()
{
    wQuery("#merger-result").html("Loading...");
    wQuery("#merger-result")
        .load("/stocks-merge", wQuery("#config-form").serializeObject(),
              function()
              {
                  if (wQuery('#merger-result').data('masonry'))
                  {
                      wQuery('#merger-result').data('masonry').destroy();
                  }
                  wQuery('#merger-result')
                      .masonry({singleMode: false,
                                transitionDuration: 0,
                                gutter:10,
                                columnWidth: 300});
              });
}
