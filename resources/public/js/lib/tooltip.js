;(function($) {

      // the tooltip element
      var helper = {},
      // the current tooltipped element
      current,
      // the title of the current element, used for restoring
      title,
      // timeout id for delayed tooltips
      tID,
      // IE 5.5 or 6
      IE = $.browser.msie && /MSIE\s(5\.5|6\.)/.test(navigator.userAgent),
      // flag for mouse tracking
      track = true;

      $.tooltip = {
	  blocked: false,
	  defaults: {
	      top: 15,
	      left: 15,
	      id: "tooltip"
	  }
      };

      $.fn.extend({customTooltip: function(settings) {
		       settings = $.extend({}, $.tooltip.defaults, settings);
		       createHelper(settings);
		       return this.each(function() {
					    $.data(this, "tooltip", settings);
					    // copy tooltip into its own expando and remove the title
					    this.tooltipText = this.dataset.title;
					    // also remove alt attribute to prevent default tooltip in IE
					    this.alt = "";
				        })
			   .mouseover(save)
			   .mouseout(hide)
			   .click(hide);
		   }});

      function createHelper(settings) {
	  // there can be only one tooltip helper
	  if( helper.parent )
	      return;
	  helper.parent = $('<div id="' + settings.id + '"><h3></h3><div class="body"></div><div class="url"></div></div>')
	      .appendTo(document.body)
	      .hide();

	  // save references to title and url elements
	  helper.title = $('h3', helper.parent);
	  helper.body = $('div.body', helper.parent);
	  helper.url = $('div.url', helper.parent);
      }

      function settings(element) {
	  return $.data(element, "tooltip");
      }

      // main event handler to start showing tooltips
      function handle(event) {
	  show();
	  $(document.body).bind('mousemove', update);
	  update(event);
      }

      // save elements title before the tooltip is displayed
      function save() {
	  // if this is the current source, or it has no title (occurs with click event), stop
	  if ( this == current || (!this.tooltipText && !settings(this).bodyHandler) )
	      return;

	  current = this;
	  title = this.tooltipText;

	  helper.title.html(title).show();
	  helper.body.hide();

	  handle.apply(this, arguments);
      }

      function show() {
	  tID = null;
	  helper.parent.show();
	  update();
      }

      /**
       * callback for mousemove
       * updates the helper position
       * removes itself when no current element
       */
      function update(event)	{
	  if($.tooltip.blocked)
	      return;

	  if (event && event.target.tagName == "OPTION") {
	      return;
	  }

	  // if no current element is available, remove this listener
	  if( current == null ) {
	      $(document.body).unbind('mousemove', update);
	      return;
	  }

          if (settings(current) == null)
          {
              return;
          }

	  // remove position helper classes
	  helper.parent.removeClass("viewport-right").removeClass("viewport-bottom");

	  var left = helper.parent[0].offsetLeft;
	  var top = helper.parent[0].offsetTop;
	  if (event) {
	      // position the helper 15 pixel to bottom right, starting from mouse position
	      left = event.pageX + settings(current).left;
	      top = event.pageY + settings(current).top;
	      var right='auto';
	      if (settings(current).positionLeft) {
		  right = $(window).width() - left;
		  left = 'auto';
	      }
	      helper.parent.css({
				    left: left,
				    right: right,
				    top: top
			        });
	  }

	  var v = viewport(),
	  h = helper.parent[0];
	  // check horizontal position
	  if (v.x + v.cx < h.offsetLeft + h.offsetWidth) {
	      left -= h.offsetWidth + 20 + settings(current).left;
	      helper.parent.css({left: left + 'px'}).addClass("viewport-right");
	  }
	  // check vertical position
	  if (v.y + v.cy < h.offsetTop + h.offsetHeight) {
	      top -= h.offsetHeight + 20 + settings(current).top;
	      helper.parent.css({top: top + 'px'}).addClass("viewport-bottom");
	  }
      }

      function viewport() {
	  return {
	      x: $(window).scrollLeft(),
	      y: $(window).scrollTop(),
	      cx: $(window).width(),
	      cy: $(window).height()
	  };
      }

      function hide(event) {
	  current = null;
	  helper.parent.hide();
      }
  })(jQuery);

$(document).on("mouseover","[data-title]",
               function(ev)
               {
                   if (!$.data(ev.currentTarget,"tooltip") || ev.currentTarget.tooltipText!=ev.currentTarget.dataset.title)
                   {
                       var el = $(ev.currentTarget);
                       el.customTooltip({});
                       el.trigger("mouseover");
                   }
               });
