$(function()
  {
      $(".fixed-panel")
          .each(function(){
                    var panel = $(this);
                    var top = panel.offset().top;
                    var left = panel.offset().left;
                    var width = panel.width();
                    var height = panel.height();

                    var placeholder = $("<div/>");
                    placeholder.hide();

                    panel.after(placeholder);

                    $(window)
                        .scroll(function()
                                {
                                    if ($(window).scrollTop() < top)
                                    {
                                        panel.css("top", "0");
                                        panel.css("left", "0");
                                        panel.css("position", "relative");

                                        placeholder.hide();
                                    }
                                    else if ($(window).scrollTop() > panel.offset().top)
                                    {
                                        placeholder.show();
                                        placeholder.css("width", width + "px");
                                        placeholder.css("height", height + "px");

                                        panel.css("position", "fixed");
                                        panel.css("top","0px");
                                        panel.css("left", left + "px");
                                        panel.css("width", width + "px");
                                    }
                                });
                    $(window).trigger("scroll");
                });
  });
