function findCustomDropdown(element)
{
    return $(element).closest(".dk_container");
}

function openCustomDropdown(el) {
    var dropdown = findCustomDropdown(el);

    if (dropdown.find("ul li").length)
    {
        dropdown.toggleClass('dk_open');
        hideValidation();
    }
}

function updateCustomDropdownField(option) {
    var value, label, dk;

    option = $(option);

    if (option.attr("disabled")) return;

    value = option.attr('data-dk-dropdown-value');
    label = option.find("> a").text();
    dk = findCustomDropdown(option);

    // set selected attribute - to make possible cloning
    dk.find("option").removeAttr('selected');
    dk.find(byParam("value",value,"option")).attr('selected','selected');

    // set select val
    dk.find("select").val(value).change();

    // update visual labels
    dk.find('.dk_label').text(label);
    dk.find('.dk_option_current').removeClass('dk_option_current');
    option.addClass('dk_option_current');

    dk.removeClass('dk_open');
}
