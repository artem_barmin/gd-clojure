function showPreview(element,wThumb,hThumb,w,h,coords)
{
    var rx = wThumb / coords.w;
    var ry = hThumb / coords.h;

    element.css({width: Math.round(rx * w) + 'px',
		 height: Math.round(ry * h) + 'px',
		 marginLeft: '-' + Math.round(rx * coords.x) + 'px',
		 marginTop: '-' + Math.round(ry * coords.y) + 'px'});

}

function getRectangle(w,h,wThumb,hThumb,coords)
{
    var rx = w / wThumb, ry = h / hThumb;
    return {w: Math.round(rx * coords.w),
	    h: Math.round(ry * coords.h),
	    x: Math.round(rx * coords.x),
	    y: Math.round(ry * coords.y)};
}

function initCropPlugin(image)
{
    var thumbnail = image.closest(".croped-element").find(".croped-thumbnail");
    image.load(function()
               {
                   var jcropApi = image.data('Jcrop');
                   if (jcropApi)
                   {
                       jcropApi.destroy();
                   }

                   var
                   wThumb = parseInt(thumbnail.attr("data-crop-width")),
                   hThumb = parseInt(thumbnail.attr("data-crop-height"));

                   thumbnail.attr("src",image.attr("src"));
                   var w = image.width(),h = image.height();
                   var updateThumbnail = function(coords)
                   {
                       showPreview(thumbnail,wThumb,hThumb,w,h,coords);
                   };

                   if (image.find(" + .jcrop-holder"))
                   {
                       image.Jcrop({aspectRatio: wThumb/hThumb,
                                    onSelect: updateThumbnail,
                                    onChange: updateThumbnail});
                   }

                   // compute rectangle params
                   var original = new Image();
                   original.src = image.attr("src");
                   var originalWidth = original.width,
                   originalHeight = original.height;

                   image.data('Jcrop')['getRectangle'] = function()
                   {
                       return getRectangle(originalWidth,originalHeight,
                                           w,h,
                                           image.data('Jcrop').tellScaled());
                   };

               });
}
