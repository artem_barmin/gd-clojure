function findInput(element)
{
    return $(element.target).closest("[role=spinner]").find("input[role=value]");
}

function spinnerOptions(input)
{
    return {
        min: parseInt(input.attr("min")) || 0,
        max: parseInt(input.attr("max")) || 100
    };
}

function constraintSpinnerInput(input)
{
    var input = findInput(input);
    var opts = spinnerOptions(input);
    var val = parseInt(input.val());
    if (input.val()==="")
    {
        return;
    }
    else if (isNaN(val))
    {
        input.val(opts.min);
    }
    else if (val.toString()!=input.val())
    {
        input.val(val);
    }
    else if (val > opts.max)
    {
        input.val(opts.max);
    }
    else if (val <= opts.min)
    {
        input.val(opts.min);
    }
    input.change();
}

function spinnerUp(input)
{
    var input = findInput(input);
    var opts = spinnerOptions(input);
    var val = parseInt(input.val());
    if (isNaN(val))
    {
        input.val(opts.min).change();
    }
    else
    {
        if (val + 1 <= opts.max)
        {
            input.val(val + 1).change();
        }
    }
}

function spinnerDown(input)
{
    var input = findInput(input);
    var val = parseInt(input.val());
    var opts = spinnerOptions(input);
    if (isNaN(val) || val <= (1 + opts.min))
    {
        input.val(opts.min).change();
    }
    else
    {
        input.val(val - 1).change();
    }
}

function spinnerMouseDown(button)
{
    var input = findInput(button);
    input.data("spinner-timer", setInterval(function(){$(button).click();},150));
}

function spinnerMouseUp(input)
{
    var input = findInput(input);
    clearInterval(input.data("spinner-timer")) ;
}

// Mouse holding events - automatic countin
$(document).on("mouseup",".spinner-button,[role=up],[role=down]", spinnerMouseUp);
$(document).on("mouseout",".spinner-button,[role=up],[role=down]", spinnerMouseUp);
$(document).on("mousedown",".spinner-button,[role=up],[role=down]", spinnerMouseDown);

// Basic + and - events
$(document).on("click",".spinner-top.spinner-button,[role=up]", spinnerUp);
$(document).on("click",".spinner-bottom.spinner-button,[role=down]", spinnerDown);

// Manual change events
$(document).on("keyup",".spinner-element > input[type=text],[role=spinner]>[role=value]", constraintSpinnerInput);
