var validateTarget = null;

function wysiwygValidationError(el,text)
{
    $(el).find(".wysiwyg-validation-error").text(text);
}

function hideValidation()
{
    $(".validation-error-tooltip").remove();
    $(".wysiwyg-validation-error").text("");
    return true;
}

function showValidationTooltip(text,el)
{
    // find closest visible parent to correctly compute position
    if (!$(el).is(":visible"))
    {
        el = $(el).closest(":visible");
    }

    hideValidation();

    var validationPanel = $("<div class='validation-error-tooltip none'/>");
    validationPanel.append($("<div class='validation-error-tooltip-arrow'/>"));
    validationPanel.click(hideValidation);
    validationPanel.append(text);

    $((validateTarget && $(validateTarget).get(0)) || "body").append(validationPanel);

    validationPanel
        .fadeIn(100)
        .position({of: el,
                   my: "left top",
                   at: "left bottom",
                   offset: "10px"});
}

function showValidation(text,el)
{
    trackVisit("validation-error",text);

    // check if element is located in the invisible tab
    var tab = $(el).parents("[role=tab]").filter(function(){return $(this).parent().is(":visible")}).first();
    if (tab.length && !tab.is(":visible"))
    {
        var container = tab.closest("[role=tabs-container]:visible");
        var currentTab = tab.attr("id");
        container.find(byParam("tab-name",currentTab,"a")).click();
        $(document).one("shown.bs.tab",function(){showValidationTooltip(text,el);});
    }
    else
    {
        showValidationTooltip(text,el);
    }

}

$(document).on("modal-dialog.close",hideValidation);
$(document).on("modal-dialog.init",hideValidation);
$(document).on("show.bs.tab",hideValidation);
$(document).on("keypress","input[type=text],textarea",hideValidation);
