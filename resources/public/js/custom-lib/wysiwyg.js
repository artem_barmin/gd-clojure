function insertWysiwygImage(selector,src)
{
    var editor = $(selector).data("cleditor");

    if($.browser.webkit)
    {
        editor.select();
    }
    editor.insertHtml("<img src='" + src + "'></img>");
    editor.changeInternal();
}

function insertTinyMceImage(src)
{
    tinymce.activeEditor.execCommand('mceInsertContent', false, "<img src='" + src + "'></img>");
}

function initUpdateAreaHooks()
{
    $.cleditor.defaultOptions.updateTextArea = function(html) {
        return convertHTMLtoBBCode(html);
    };
}

function enableTinyMCE(selector)
{
    tinyMCE.baseURL = '/js/tinymce';
    tinyMCE.suffix = '';
    tinymce.init({
        selector: selector,
        relative_urls: false,
        remove_script_host: false,
        document_base_url: window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : ''),
        menubar: false,
        extended_valid_elements : "iframe[name|src|framespacing|border|frameborder|scrolling|title|height|width]",
        toolbar: "undo redo | styleselect forecolor | bold italic underline strikethrough removeformat | alignleft aligncenter alignright alignjustify | " +
        "bullist numlist outdent indent | link code",
        style_formats: [
            {title: 'Заголовок 1', block: 'h1'},
            {title: 'Заголовок 2', block: 'h2'},
            {title: 'Заголовок 3', block: 'h3'},
            {title: 'Параграф', block: 'p'}
        ],
        language: 'ru',
        statusbar: false,

        formats: {
            forecolor: {inline: 'font', attributes: {color: '%value'}},
            alignleft: {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', attributes: {"align": 'left'}},
            aligncenter: {
                selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
                attributes: {"align": 'center'}
            },
            alignright: {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', attributes: {"align": 'right'}},
            alignfull: {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', attributes: {"align": 'justify'}}
        },
        convert_fonts_to_spans: false,
        plugins: 'paste code textcolor code link'
    });
}

function prepareTextForSending(elem, editorElem)
{
    var input = $(elem)
        .parents(".cleditorMain")
        .first()
        .find("textarea");

    var htmlValue = editorElem.doc.body.innerHTML;
    var bbcodeValue = convertHTMLtoBBCode(htmlValue);
    input.val(bbcodeValue);
}

function clearWysiwyg(elem)
{
    $(elem).find("textarea").val("");

    $(elem).find("textarea")
        .cleditor().get(0)
        .doc.body.innerHTML="";
}

function refreshModalEditors()
{
    $.lastDialog
        .find("textarea.wysiwyg-editor")
        .each(function(i,el)
              {
                  $(el).editor();
                  $(el).data().cleditor.refresh();
              });
}

$.fn.editor = function()
{
    initUpdateAreaHooks();
    if ($(this).size() != 0)
    {
        var elem = $(this);

        if (!elem.data().cleditor)
        {
            var mode = $(elem).attr("mode");
            var autosize = $(elem).attr("autosize");
            var mainCss, smileBarCss, toolBarCss, buttonsCss;

            switch(mode)
            {
            case "private":
                mainCss = "privateCleditorMain";
                smileBarCss = "privateSmileBar";
                toolBarCss = "privateToolBar";
                buttonsCss = "privateButtonCss";
                break;

            case "profile":
                mainCss = "profileCleditorMain";
                smileBarCss = "profileSmileBar";
                toolBarCss = "profileToolBar";
                buttonsCss = "privateButtonCss";
                break;

            case "forum":
                mainCss = "forumCleditorMain";
                smileBarCss = "forumSmileBar";
                toolBarCss = "forumToolBar";
                buttonsCss = "baseCleditorButton";
                break;

            default:
                mainCss = "baseCleditorMain";
                smileBarCss = "baseCleditorSmileybar";
                toolBarCss = "baseCleditorToolbar";
                buttonsCss = "baseCleditorButton";
            }

            elem.addClass("editor-initialized");

            var editorElem = elem.cleditor(
                {
                    controls: "bold italic underline strikethrough removeformat | bullets numbering | outdent indent | alignleft center alignright justify | color style | link source",
                    mainCss: mainCss,
                    toolBarCss: toolBarCss,
                    smileBarCss: smileBarCss,
                    buttonCss: buttonsCss,
                    styles: [["Header 1", "<h1>"],
                             ["Header 2", "<h2>"],
                             ["Header 3", "<h3>"]]}
            ).get(0);

            $('<div class="wysiwyg-validation-error"></div>').insertAfter(elem.parents(".cleditorMain").first().next());

            var processChange = function()
            {
                hideValidation();
                prepareTextForSending(elem, editorElem);
                if(autosize==="autosize")
                {
                    editorElem.resizeToFitContent();
                }
            };

            editorElem.changeInternal(processChange);
            elem.change(processChange);
        }
    }
};

$(document).bind("modal-dialog.init",refreshModalEditors);

$.fn.simpleEditor = function(width)
{
    $(this).editor(width, "");

    // hide smileys bar
    $(this).parents(".cleditorMain")
        .first()
        .parent()
        .find(".cleditorSmileybar")
        .hide();
};
