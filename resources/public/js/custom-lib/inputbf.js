// Расширяем valHooks - что бы при вызове val() на нашем элементе, в первую
// очередь проверялось что он менялся, и только если он менялся - возвращать
// реальное значение
jQuery.each(["input","textarea"],
            function(i,el)
            {
                jQuery.valHooks[el] =
                    jQuery.extend(jQuery.valHooks[el],
                                  {get:function(elem) {
                                       if(typeof $(elem).attr("data-inputbf-default") != "undefined"
                                          && $(elem).attr("data-inputbf-default")==elem.value)
                                           return "";
                                       return elem.value;
                                   }});
            });

function inputBFFocus(element)
{
    var $this = $(element);
    var $data = $this.data();
    if ($data.ValueBlured == undefined || $data.ValueBlured == $this.attr("data-inputbf-default"))
	$this.val('').addClass('inputbf-active');
}

function inputBFBlur(element)
{
    var $this = $(element);
    var $data = $this.data();
    // Если пользователь ничего не ввёл, возвращаем всё как было
    var cache = $this.val();
    if (cache == '')
	$this.val($this.attr("data-inputbf-default"))
	.removeClass('inputbf-active')
	.removeData('ValueBlured');
    // Иначе запоминаем введённый текст для сравнение с умолчанием при следующем фокусе
    else
	$data.ValueBlured = cache;

}
