var anything = "([\\s\\S]*?)";

function alignRegexp(htmlTag)
{
    return new RegExp("<" + htmlTag +  "[^>]+align=\"(left|right|center|justify)\".*?>" + anything + "</" + htmlTag +  ">", "gi");
}

function simpleTagRegexp(tag,bb)
{
    var bb = bb || tag;
    // match tag, after match or empty string with > or any number of attributes and then >
    // [\\s\\S] - multiline regexp
    var tagRegexp = new RegExp("<" + tag + "(?: .*?|)>" + anything + "</" + tag + ">","gi");
    return [tagRegexp, "[" + bb + "]$1[/" + bb + "]"];
}

function startTag(htmlTag)
{
    return new RegExp("<" + htmlTag + "(?: .*?|)>","gi");
}

function endTag(htmlTag)
{
    return new RegExp("</" + htmlTag + ">","gi");
}

function tagWithParameter(tag,parameter)
{
    return new RegExp("<" + tag + "[^>]*" + parameter + "=\"(.*?)\"[^>]*>" + anything + "</" + tag + ">","gi");
}

function imgDomParser()
{
    return [/<img.*?>/gi,
            function(img)
            {
                var el = $(img);
                var src = el.attr("src").match(/([a-z0-9]{40}).jpg/)[1];

                if (src)
                {
                    return ["[img",
                            " width=" + (el.attr("width") || "auto"),
                            " height=" + (el.attr("height") || "auto"),
                            "]",
                            src,
                            "[/img]"].join("");
                }
                else
                {
                    return "";
                }
            }];
}

function iframeDomParser()
{
    return [/<iframe.*?>/gi,
        function(iframe)
        {
            var el = $(iframe);
            var src = el.attr("src");

            if (src)
            {
                return ["[iframe",
                    " width=" + (el.attr("width") || "auto"),
                    " height=" + (el.attr("height") || "auto"),
                    " frameborder="+(el.attr("frameborder")),
                    "]",
                    src,
                    "[/iframe]"].join("");
            }
            else
            {
                return "";
            }
        }];
}

function divDomParser()
{
    return [/<div.*?>.*/gi,
        function(div)
        {
            var el = $(div);
            var html = $(div).html();
            var text = $(div).text();
            while(html.indexOf("<div") >-1)
                html = convertHTMLtoBBCode(html);
            if(text.length > html.length)
                html = text;
            return ["[div",
                " style=" + (el.attr("style")).split(' ').join(''),
                " class=" + (el.attr("class")),
                "][span]"+ html,
                "[/span][/div]"].join("");

        }];
}

function convertHTMLtoBBCode (html) {

    [[/[\r|\n]/g, ""],
     [/<br[^>]*>/gi, "[br][/br]"],
     [alignRegexp("h1"), "[h1=$1]$2[/h1]"],
     [alignRegexp("h2"), "[h2=$1]$2[/h2]"],
     [alignRegexp("h3"), "[h3=$1]$2[/h3]"],
     [alignRegexp("p"), "[align=$1]$2[/align]"],

     simpleTagRegexp("b"),
     simpleTagRegexp("p"),
     simpleTagRegexp("h1"),
     simpleTagRegexp("h2"),
     simpleTagRegexp("h3"),
     simpleTagRegexp("strong","b"),
     simpleTagRegexp("i"),
     simpleTagRegexp("em","i"),
     simpleTagRegexp("u"),
     simpleTagRegexp("strike","s"),
     simpleTagRegexp("del","s"),

     [startTag("ul"), "[list]"],
     [endTag("ul"), "[/list]"],
     [startTag("ol"), "[list=1]"],
     [endTag("ol"), "[/list]"],
     [startTag("li"), "[*]"],
     [endTag("li"), "[/*]"],

     // custom regexps for links and images
     [tagWithParameter("a","href"), "[url=$1]$2[/url]"],
     [tagWithParameter("font","color"), "[color=$1]$2[/color]"],
     [/<img[^>]*class=".*?smile([0-9]+).*?"[^>]*>/gi, "[img]$1[/img]"],
     [/<img[^>]*src="\/img\/uploaded\/w[0-9]*\/h[0-9]*\/([a-z0-9]{32}.jpg)"[^>]*>/gi, "[img-internal]$1[/img-internal]"],

     imgDomParser(),
     iframeDomParser(),
     //divDomParser(),
     // remove any other tags
     //[/<.*?>/g, ""],

     // convert entities

     [/&lt;/gi, "<"],
     [/&gt;/gi, ">"],
     [/&amp;/gi, "&"]
    ].forEach(function(item) {html = html.replace(item[0], item[1]);});

    return html;
};
