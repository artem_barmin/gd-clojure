var historyChangeCounter = 0;

function emptyRelativeUrl() {
    return $("<a>").attr("href","last-page").get(0).href.replace(/last-page$/,"");
}

function getUrlPartAsLong(regexp, def)
{
    var match = window.location.href.match(regexp);
    return (match && parseInt(match[0].match(/[0-9]+/)[0])) || def;
}

function getPageFromUrl()
{
    return getUrlPartAsLong(/page[0-9]+/, 1);
}

function getLimitFromUrl()
{
    return getUrlPartAsLong(/x[0-9]+/, null);
}

function getStockModalFromUrl()
{
    return getUrlPartAsLong(/stock[0-9]+/, null);
}

function pushUrl(url,title)
{
    if (getStockModalFromUrl() && url.match(/page[0-9]+/))
    {
        // skip - modal window have higher priority than stock
    }
    else
    {
        if (url=="")
        {
            var removeLastPart = location.pathname.replace(/\/[^\/]+$/,"") + "/";
            var fixTrailingSlash = removeLastPart.replace(/[/]+/g,"/");
            history.pushState(null,title,fixTrailingSlash + window.location.hash);
        }
        else
        {
            if (!window.location.href.match(url + "$"))
            {
                history.pushState(null,title,url + window.location.hash);
                historyChangeCounter++;
            }
        }
    }
}

function onPopState(fn)
{
    $(window).bind("popstate", function(e){
                       var returnLocation = history.location || document.location;
                       fn(returnLocation);
                   });
}

function backOrClear()
{
    if(historyChangeCounter>0)
    {
        history.back();
    }
    else
    {
        history.pushState(null,null,emptyRelativeUrl());
    }
}
