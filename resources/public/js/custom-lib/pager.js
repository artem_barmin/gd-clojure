var previousHeight;

// Delayed showing of content
function setPagerHeight(length)
{
    $(".pager-body-container").height(previousHeight * length);
}

function showByPortions(offset,container,data)
{
    if (offset < data.length)
    {
        var el = $(data[offset]);
        container.append(el);
        setPagerHeight(data.length);
        setTimeout(function()
                   {
                       // at first - set approximate height(based on height of one block)
                       if (offset===0)
                       {
                           previousHeight = el.height();
                           setPagerHeight(data.length);
                       }
                       // at the end - set exact height, to avoid gaps
                       else if (offset===data.length - 1)
                       {
                           $(".pager-body-container").height('auto');
                       }
                       showByPortions(offset+1,container,data);
                   },
                   0);
    }
}
