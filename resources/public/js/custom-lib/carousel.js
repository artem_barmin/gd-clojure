function carouselMove(el, direction)
{
    if ($(el).attr("disabled")) return;

    var container = $(el).closest(".simple-carousel").find(".images-container");
    var width = container.find("li").outerWidth(true);
    var maxCount = container.find("li").length;
    var left = container.position().left + width*direction;
    if (left <= 0 && Math.abs(left) <= (maxCount - 5) * width) {
        container.css("left", left + "px");
    }
}
