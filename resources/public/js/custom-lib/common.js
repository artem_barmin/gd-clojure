if (!window.commonInitialized)
{
    window.commonInitialized = true;

    function nano(template, data) {
        return template.replace(/\{([\w\.]*)\}/g, function(str, key) {
                                    var keys = key.split("."), v = data[keys.shift()];
                                    for (var i = 0, l = keys.length; i < l; i++) v = v[keys[i]];
                                    return (typeof v !== "undefined" && v !== null) ? v : "";
                                });
    }

    function updateTextAreaValues(form)
    {
        $(form).find("textarea").each(function() {$(this).text($(this).val());});
    }

    function serializeToMap(form)
    {
        var o = {};
        var a = form.serializeArray();
        $.each(a, function() {
                   if (o[this.name] !== undefined) {
                       if (!o[this.name].push) {
                           o[this.name] = [o[this.name]];
                       }
                       o[this.name].push(this.value || '');
                   } else {
                       o[this.name] = this.value || '';
                   }
               });
        return o;
    }

    $.fn.serializeObject = function()
    {
        var form = this;
        updateTextAreaValues(form);
        if (!$(this).is("form"))
        {
            form = $("<form></form>");
            $(this).clone().appendTo(form);
        }
        return serializeToMap(form);
    };

    $.fn.outerHTML = function(s) {
        return jQuery("<p>").append(this.eq(0).clone()).html();
    };

    function stopEvent(event)
    {
        if (window.event)
        {
            window.event.cancelBubble = true;
        }
        if (event.stopPropagation)
        {
            event.stopPropagation();
        }
        if (event.preventDefault)
        {
            event.preventDefault();
        }
    }

    function autoConverter(v,force_type)
    {
        if (typeof(v)==="object")
        {
            return v;
        }
        return {__type: force_type || typeof(v), value: v};
    }

    var postAjaxHandlers = [];

    function addPostAjaxHandler(fn)
    {
        postAjaxHandlers.push(fn);
    }

    function execAjaxHandlers()
    {
        for(var i in postAjaxHandlers)
        {
            postAjaxHandlers[i]();
        }
    }

    $(document).ajaxComplete(execAjaxHandlers);
    $(document).ready(execAjaxHandlers);
    $(document).bind("modal-dialog.init", execAjaxHandlers);

    function haveFunction(name)
    {
        return typeof jQuery.fn[name] != "undefined";
    }

    function haveElement(selector)
    {
        return jQuery(selector).size() > 0;
    }

    function notify(title,body,timeout)
    {
        toastr.info(body,title, {positionClass: 'toast-bottom-right',
                                 fadeIn: 300,
                                 fadeOut: 300,
                                 timeOut: (timeout || 5000)});
    }

    function persistentNotify(title,body)
    {
        toastr.info(body,title, {positionClass: 'toast-bottom-right',
                                 fadeIn: 300,
                                 fadeOut: 300,
                                 tapToDismiss: false,
                                 timeOut: 0,
                                 extendedTimeOut: false});
    }

    // Ajax loaders
    var ajaxPreloadingButton = null;

    $(document).ajaxStart(function() {
                              $(ajaxPreloadingButton).find("img").show();
                              $(ajaxPreloadingButton).find(".button-text").hide();
                              $(ajaxPreloadingButton).addClass('ajax-in-progress');
                          });

    $(document).ajaxComplete(function() {
                                 $(ajaxPreloadingButton).find("img").hide();
                                 $(ajaxPreloadingButton).find(".button-text").show();
                                 $(ajaxPreloadingButton).removeClass('ajax-in-progress');
                                 ajaxPreloadingButton = null;
                             });

    // Form cleaner
    function cleanForm(form)
    {
        $(form).find("input[type=text],textarea").val("").blur();
    }

    // Form serializer
    function formState(formSelector)
    {
        if ($(formSelector).size()===0)
            throw "No form found : " + formSelector;
        return $(formSelector).serializeObject();
    }

    function elementState(elementSelector)
    {
        return serializeToMap($(elementSelector));
    }

    $(document).ready(function()
                      {
                          $(".fout-hidden").css('visibility','visible');
                      });

    // Utility for multi region
    function multiRegionSuccess(id,sel,res,success)
    {
        hideValidation();
        if ($(sel).hasClass("region-inplace"))
        {
            $(sel).replaceWith(res);
        }
        else
        {
            $(sel).html(res);
        }

        success && success();

        $(document).trigger("region." + id);
    }

    function buildRegionFunction(name,id)
    {
        var toCamelCase = function(str)
        {
	    return str.toString().replace(/(\-[a-z])/g, function($1){return $1.toUpperCase().replace('-','');});
        };
        return window[toCamelCase(name) + toCamelCase(id) + "region"] || _.constant(null);
    }

    // Remotes fn
    function callRemote(fixedOptions)
    {
        return function(options,success,error)
        {
            var opts = $.extend(fixedOptions,options);
            return $.post("/service",opts)
                .done(success)
                .error(error);
        };
    }

    // Button utils
    function isButtonCanClick(button)
    {
        if ($(button).hasClass('enabled') && !$(button).hasClass('ajax-in-progress'))
        {
            ajaxPreloadingButton = button;
            return true;
        }
        return false;
    }

    if (!window.trackVisit)
    {
        window.trackVisit = function(){};
    }

    // Selector utils
    function byParam(param,value,element)
    {
        var value = value ? value.toString().replace(/\'/g,"\\\'") : "null";
        return (element ? element : "") + "[" + param + "='" + value + "']";
    }

    // Angular utils
    function getScopeParam(selector,param)
    {
        return angular.copy(angular.element($(selector)).scope()[param]);
    }

    // Convert string to integer
    String.prototype.hashCode = function() {
        var hash = 0, i, chr, len;
        if (this.length == 0) return hash;
        for (i = 0, len = this.length; i < len; i++) {
            chr   = this.charCodeAt(i);
            hash  = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    };
}
