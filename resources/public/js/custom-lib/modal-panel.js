$.lastDialogQueue = [];
$.lastDialog = null;
$.lastUsedContent = null;
$.lastDialogElement = null;

if (typeof window.closeOnlyOnButton == "undefined")
{
    closeOnlyOnButton = false;
}

function initDialog()
{
    $('.modal-close').
        unbind('click').
        click(function(event)
              {
                  closeDialog();
                  event.stopPropagation();
              });

    $(document).trigger("modal-dialog.init");

    setTimeout(function(){$(window).resize();}, 300);
}

function $dialog(sel)
{
    return $.lastDialog.find(sel);
}

function centerDialog()
{
    if ($.lastDialog != null)
    {
        $.lastDialog.dialog('option', 'position', 'center');
    }
}

// always center dialog after content is set
$(document).bind("modal-dialog.init",centerDialog);

function findClosest(element,selector)
{
    if ($(element).find(selector).size()!=0)
    {
        return $(element).find(selector).first();
    }
    else
    {
        return $(element).parents().find(selector).first();
    }
}

function findDialogContent(element, dontClone, modal)
{
    var eltToShow = findClosest(element,".dialog-content");

    // process lazy init case
    var lazyInitFunction = eltToShow.find("[region-function][class*=lazy-modal]").attr("region-function");

    // only 'clone' mode supported
    if (lazyInitFunction)
    {
        $.lastUsedContent = null;
        eltToShow =  eltToShow.clone().removeClass("dialog-content");
        eltToShow.find("[region-function][class*=lazy-modal]").removeClass("region");

        // callback to server
        window[lazyInitFunction](function(result,loaded)
                                 {
                                     if (loaded)
                                     {
                                         var block = modal.find(".modal-main-block");
                                         block.append(result.clone());
                                         $(document).trigger("modal-dialog.content");
                                         initDialog();
                                     }
                                     else
                                     {
                                         // lazy content already loaded - modal window is not yet initialized
                                         var block = eltToShow.find(".modal-main-block");
                                     }
                                     block.css({opacity:1, height:"auto"});
                                 });
    }
    else
    {
        if (dontClone)
        {
            $.lastUsedContent = eltToShow;
            eltToShow = eltToShow.toggleClass("dialog-content");
        }
        else
        {
            $.lastUsedContent = null;
            eltToShow =  eltToShow.clone().toggleClass("dialog-content");
        }
    }

    modal.append(eltToShow);
}

function openDialog(element, dontClone, restoreHistoryOnClose)
{
    var element = $(element);
    var modalWindow = $('<div/>').appendTo("body");

    $.lastDialogElement = element;
    $.lastDialog = modalWindow;
    $.lastDialogProperties = {
        restoreHistoryOnClose: restoreHistoryOnClose
    };

    $.lastDialogQueue.push({dialog:$.lastDialog,properties:$.lastDialogProperties});

    modalWindow.empty();
    findDialogContent(element, dontClone, modalWindow);

    $(document).trigger("modal-dialog.content");

    modalWindow.dialog({modal:true, width: 999});
    initDialog();
}

function openDialogAngular(element)
{
    openDialog(element,true);
    try{
        $.lastDialog.find("input").blur();
    } catch (x) {}
}

function closeDialog()
{
    if($.lastDialog != null)
    {
        $.lastDialog.dialog("close");
        trackVisit("modal-close");
        $(document).trigger("modal-dialog.close");
        if ($.lastUsedContent != null)
        {
            $.lastUsedContent.toggleClass("dialog-content");
            findClosest($.lastDialogElement,"[role=dialog-store]").append($.lastUsedContent);
            $.lastUsedContent = null;
        }

        if($.lastDialogProperties.restoreHistoryOnClose)
        {
            backOrClear();
            $.lastDialogProperties.restoreHistoryOnClose = false;
        }

        // restore dialog and properties from stack
        $.lastDialogQueue.pop();

        $.lastDialog.find(".modal-main-block").empty();

        var lastDialog = $.lastDialogQueue[$.lastDialogQueue.length-1];
        if (lastDialog)
        {
            $.lastDialog = lastDialog.dialog;
            $.lastDialogProperties = lastDialog.properties;
        }
    }
}

function dialogContent()
{
    $(document).trigger("modal-dialog.content");
}

function changeDialogContent(element)
{
    $.lastDialogElement = element;
    $.lastDialog.empty();
    findDialogContent(element, false, $.lastDialog);

    dialogContent();
    initDialog();
}

function resizeOverlay()
{
    $('.ui-widget-overlay').width($(document).width());
    $('.ui-widget-overlay').height($(document).height());
}

$(window).resize(resizeOverlay);
addPostAjaxHandler(resizeOverlay);
$(document).on('shown.bs.tab', resizeOverlay);

function currentDialog(element)
{
    if ($(element).find(".dialog-content").size()!=0)
    {
        return $(element).find(".dialog-content").first();
    }
    else
    {
        return $(element).parents().find(".dialog-content").first();
    }
}

$(document).bind("pager.change",centerDialog);
