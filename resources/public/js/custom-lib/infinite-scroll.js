(function($)
 {
     var infiniteScrollInit = function(element, datasource,limit,localMode)
     {
         var offset = limit;

         var requestInProcess = false;

         var limitExceed = false;

         var container = $(element);
         var offsetContainer = (localMode ? container : $(window));
         var body = $(element).find(".infinite-scroll-body");

         offsetContainer.scroll(function()
                                {
                                    if ((offsetContainer.scrollTop() + offsetContainer.height()) >
                                        (body.height() - 100))
                                    {
                                        fetchDataFromServer();
                                    }
                                });

         function fetchDataFromServer(success)
         {
             if (!requestInProcess && !limitExceed)
             {
                 requestInProcess = true;
                 datasource(offset,
                            function(data)
                            {
                                if (success)
                                {
                                    success();
                                }
                                addElements(data);
                            });
             }
         }

         function addElements(result)
         {
             var items = $(result).find(".infinite-scroll-row");
             if (items.size() > 0)
             {
                 var container = element.find(".infinite-table");
                 container.append(items);
                 offset += limit;
             }
             else
             {
                 limitExceed = true;
             }
             requestInProcess = false;
             $(window).resize();
             $(document).trigger("infinite-scroll.elements-added", [items]);
         }

         element.data("infinite-scroll",
                      {
                          refreshList:function()
                          {
                              offset = 0;
                              limitExceed = false;
                              fetchDataFromServer(function()
                                                  {
                                                      $(".infinite-table").empty();
                                                  });
                          }
                      });

         $(document).trigger("infinite-scroll.created");

     };

     $.fn.infiniteScroll = function(datasource,limit,localMode)
     {
         $(this).each(function(i,el)
                      {
                          var el = $(el);
                          if (!el.data("infinite-scroll"))
                          {
                              infiniteScrollInit(el,datasource, limit, localMode);
                          }
                      });
     };

 })(jQuery);
