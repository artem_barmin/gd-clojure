function getPosition(offset,callback,postCallback)
{
    var
    loadPage = window[pagerIntegrationLoadPage],
    nextOffset = window[pagerIntegrationNextOffset],
    prevOffset = window[pagerIntegrationPrevOffset],
    maxOffset = window[pagerIntegrationMaxOffset],
    pageSize = window[pagerIntegrationPageSize];

    var td = $($.lastDialogElement).closest(".pager-table > tbody > tr > td,.pager-table > tr > td");
    var allTd = $($.lastDialogElement).closest(".pager-table").find("> tbody > tr > td:has(.dialog-content)");
    var i = allTd.index(td) + offset;

    var updateModalContent = function(selector)
    {
        var elementWithModal = $(".pager-table > tbody").find(selector);
        if (elementWithModal.find(".dialog-content").length)
        {
            changeDialogContent(elementWithModal);
            if (postCallback)
                postCallback();
        }
    };

    if (i >= allTd.length && nextOffset < maxOffset)
    {
        callback();
        loadPage(nextOffset,pageSize,
                 function()
                 {
                     updateModalContent("tr:has(.dialog-content):first > td:has(.dialog-content):first");
                 });
    }
    else if (i < 0 && prevOffset >= 0 )
    {
        callback();
        loadPage(prevOffset,pageSize,
                 function()
                 {
                     updateModalContent("tr:has(.dialog-content):last > td:has(.dialog-content):last");
                 });
    }
    else if (i >= 0 && i<allTd.length)
    {
        callback();
        changeDialogContent(allTd.get(i));
        if (postCallback) postCallback();
    }
}

function pagerLeftItem(callback,postCallback)
{
    getPosition(-1,callback,postCallback);
}

function pagerRightItem(callback,postCallback)
{
    getPosition(1,callback,postCallback);
}
