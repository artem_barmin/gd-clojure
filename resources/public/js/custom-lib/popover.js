function currentPopover(element)
{
    if ($(element).find(".popover").size()!=0)
    {
        return $(element).find(".popover").first();
    }
    else
    {
        return $(element).parents().find(".popover").first();
    }
}

function openPopover(element)
{
    var contentElt = currentPopover(element);
    if (!$(element).data("bs.popover"))
    {
        $(element).popover({html : true,
                            trigger:'manual',
                            content: function() {
                                return contentElt.html();
                            }});
        $(element).on('shown.bs.popover', function (evt) {
                          $(element).data("bs.popover").$tip.data("source-popover",$(element));
                      });

        $(element).popover("show");
    }
    else
        $(element).popover("show");

    var lazyInitFunction = $(element).find("[region-function][class*=lazy-popover]").attr("region-function");
    if (lazyInitFunction)
       window[lazyInitFunction](function()
       {
           $(element).popover("show");
       });
}

function setPopoverContent(element,content)
{
    var trigger = $(element).closest("[data-popover-trigger]");
    var contentElt = currentPopover(element);
    var dataPopover = trigger.data("bs.popover");
    contentElt.find(".content").html(content.html());
    if (dataPopover && dataPopover.$tip)
    {
        dataPopover.$tip.find(".content").html(content.html());
        trigger.popover("show");
    }
}

function closePopover(element)
{
    var element = $(element);
    if (element.data("bs.popover"))
        element.popover("hide");
    else
        element.closest(".popover").data("source-popover").popover("hide");
}

$(document).on("mouseenter","[data-popover-trigger]", function(ev) {openPopover(ev.currentTarget);});
$(document).on("mouseleave","[data-popover-trigger]", function(ev) {closePopover(ev.currentTarget);});
