$.fn.tab.Constructor.prototype.show = function () {

    var $this    = this.element;
    var $ul      = $this.closest('ul:not(.dropdown-menu)');
    var selector = $this.data('target');

    if (!selector) {
        selector = $this.attr('href');
        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, ''); //strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return;

    var previous = $ul.find('.active:last a')[0];
    var e        = $.Event('show.bs.tab', {
                               relatedTarget: previous
                           });

    $this.trigger(e);

    if (e.isDefaultPrevented()) return;

    // Change : we find reference from parent container, this allows multiple instances of tabs
    var $target = $this.closest(".nav-tabs-container").find(selector);

    this.activate($this.parent('li'), $ul);
    this.activate($target, $target.parent(), function () {
                      $this.trigger({
                                        type: 'shown.bs.tab'
                                        , relatedTarget: previous
                                    });
                  });
};
