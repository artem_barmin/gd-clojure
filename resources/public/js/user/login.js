// Turn on caching of getScript requests
$.ajaxSetup({cache: true});

function remote_login_callback(elt)
{
    return function(result)
    {
        if (result == "Error")
        {
            showValidation("Ошибка входа, попробуйте позже",$(elt + ":visible:last"));
        }
        else
        {
            rerenderCallback();
        }
    };
}

function internal_login_callback(element)
{
    return function(result)
    {
        if (result == "Error")
        {
            showValidation("Введен не правильный логин или пароль!",$(element));
        }
        else
        {
            rerenderCallback();
        }
    };
}

function social_rejected(element)
{
    showValidation("Вы не разрешили вход через социальные сети!",element.filter(":visible"));
}

// Async init of vkontakte api
(function() {
     $.getScript("http://vkontakte.ru/js/api/openapi.js", function()
                 {
                     if (window.loginInfo)
                     {

                         VK.init({apiId: loginInfo.vk});

                         $.vkInitialized = true;

                         $(document).trigger("vk.init");

                         if (haveFunction("initVkModalLike"))
                         {
                             initVkModalLike();
                         }

                         VK.Auth.login = function(cb, settings) {
                             var channel, url;
                             if(!VK._apiId) {
                                 return false;
                             }
                             channel = window.location.protocol + '//' + window.location.hostname;
                             url = VK._domain.main + VK._path.login + '?client_id='+VK._apiId+'&display=popup&redirect_uri=close.html&response_type=token';
                             if(settings && parseInt(settings, 10) > 0) {
                                 url += '&scope=' + settings;
                             }
                             VK.Observer.unsubscribe('auth.onLogin');
                             VK.Observer.subscribe('auth.onLogin', cb);
                             VK.UI.popup({
                                             width: 620,
                                             height: 370,
                                             url: url
                                         });
                             var authCallback = function() {
                                 VK.Auth.getLoginStatus(function(resp) {
                                                            VK.Observer.publish('auth.onLogin', resp);
                                                            VK.Observer.unsubscribe('auth.onLogin');
                                                        }, true);
                             };

                             VK.UI.popupOpened = true;
                             var popupCheck = function() {
                                 if (!VK.UI.popupOpened) return false;
                                 try {
                                     if (VK.UI.active.closed) {
                                         VK.UI.popupOpened = false;
                                         authCallback();
                                         return true;
                                     }
                                 } catch(e) {
                                     VK.UI.popupOpened = false;
                                     authCallback();
                                     return true;
                                 }
                                 setTimeout(popupCheck, 100);
                             };

                             setTimeout(popupCheck, 100);
                         };

                         if($("#vk_groups").size() > 0)
                         {
                             VK.Widgets.Group("vk_groups", {mode: 0, width: "230", height: "400"}, 48899846);

                             var updateGroupStatus = function()
                             {
                                 trackSocialEvent("vk-group","48899846");
                             };

                             VK.Observer.subscribe("widgets.groups.joined",updateGroupStatus);
                             VK.Observer.subscribe("widgets.groups.leaved",updateGroupStatus);
                         }
                     }
                 });
 }());

// Method specific login handlers
// VK
function vk_login_handler(response)
{
    if (response.session) {
        loginCallback({type: "vk", code: $("[name=code]").val()}, remote_login_callback(".login-vkontakte"));
    }
    else {
        social_rejected($(".login-vkontakte"));
    }
}

// Site login
function site_login_handler()
{
    loginCallback({type: "site",
                   login: $(".login-fields-input[name=login]:visible").val(),
                   password: $(".login-fields-input[name=password]:visible").val()},
                  internal_login_callback("#login"));
}

// Site login from registration window
function reg_site_login_handler()
{
    loginCallback({type: "site",
                   login: $("[name=login-from-reg]:visible").val(),
                   password: $("[name=password-from-reg]:visible").val()},
                  internal_login_callback("[name=login-from-reg]:visible"));
}

$(document).ready(function(){
                      var sendOnEnter = function(ev)
                      {
                          if(ev.keyCode==13)
                          {
                              setTimeout(function(){
                                             $("#password-input").blur();
                                             site_login_handler();}
                                         ,100);
                          }
                      };

                      $(".login-fields-input").keypress(sendOnEnter);
                  });
