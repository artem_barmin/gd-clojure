function enableMultiUpload(id,options)
{
    var process = function()
    {
        $('.dropzone').on('dragenter', function (e) {
                              $(this).addClass("in");
                          });

        $('.dropzone').on('dragout', function (e) {
                              $(this).removeClass("in");
                          });

        $("[id=" + id + "]")
            .each(function()
                  {
                      var el = $(this);
                      if (el.data("processed-upload")==null)
                      {
                          el.fileupload(options);
                          el.data("processed-upload",true);
                      }
                  });
    };
    addPostAjaxHandler(process);
    $(document).ready(process);
}
