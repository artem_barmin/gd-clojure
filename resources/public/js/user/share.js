var buttonLikeId = 1;

function prepareVkButton(selector)
{
    var like = $.lastDialog.find(selector);
    if (like.size() > 0 && like.attr("id")==null)
    {
        var id = "vk-like" + buttonLikeId++;
        like.attr("id",id);
        VK._pData = null;       //force VK recheck page data
        return like;
    }
    return jQuery();
}

function initVkModalLike()
{
    if ($.lastDialog && $.vkInitialized)
    {
        var like = prepareVkButton(".vk_like");
        if (like.size() > 0)
        {
            var pageId = like.attr("page-id");
            VK.Widgets.Like(like.attr("id"),
                            {type:'button',
                             pageUrl:location.href,
                             pageTitle: like.attr("vk-title"),
                             pageDescription:like.attr("vk-description"),
                             pageImage: like.attr("vk-image"),
                             image: like.attr("vk-image")},
                            pageId);

            var storeLikeOnServer = function()
            {
                trackSocialEvent("vk-like",pageId);
            };

            VK.Observer.unsubscribe("widgets.like.shared");
            VK.Observer.subscribe("widgets.like.shared",storeLikeOnServer);

            VK.Observer.unsubscribe("widgets.like.unshared");
            VK.Observer.subscribe("widgets.like.unshared",storeLikeOnServer);
        }
    }
}

function initVkModalComments()
{
    if ($.lastDialog && $.vkInitialized)
    {
        var comments = prepareVkButton(".vk_comments");
        if (comments.size() > 0)
        {
            VK.Widgets.Comments(comments.attr("id"),{},comments.attr("page-id"));
        }
    }
}


function initVkWidgets()
{
    initVkModalLike();
    initVkModalComments();
}

$(document).bind("modal-dialog.init",initVkWidgets);
$(document).bind("vk.init", initVkWidgets);
