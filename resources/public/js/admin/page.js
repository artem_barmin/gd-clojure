function addImage(image)
{
    var scope = angular.element($("[ng-controller=PhotosCtl]")).scope();
    scope.addImage(image);
    scope.$apply();
}

function getCurrentImages()
{
    return angular.copy(angular.element($("[ng-controller=PhotosCtl]")).scope().images);
}

app.controller("PhotosCtl",
               ["$scope",function($scope)
                {
                    $scope.removeImage = function(index)
                    {
                        $scope.images.splice(index,1);
                    };

                    $scope.addImage = function(image)
                    {
                        $scope.images.push(image);
                    };

                    $scope.insertImage = function(image)
                    {
                        insertTinyMceImage(image.original);
                    };

                    $scope.$watch('images', function(o,n){if (o!==n) saveImages();},true);
                }
               ]);
