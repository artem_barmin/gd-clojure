var app = angular.module('deals', ["mgcrea.ngStrap","ngSanitize","ngAnimate","once"]);

function UserProfileManagementMixin($scope)
{
    $scope.address = {district: null};

    var getOfficeNovayaPochta = function()
    {
        return callRemote({service: "get-novaya-pochta"})
        ({"district": autoConverter($scope.address.district),
          "city": autoConverter($scope.address.city)});
    };

    var getCity = function()
    {
        return callRemote({service: "get-cities"}) ({"district": autoConverter($scope.address.district)});
    };

    $scope.$watch('address.district', function(o,n)
                  {
                      if (o !== n)
                      {
                          getCity().done(function(data) {$scope.address.cities = data;});
                          $scope.address.city = "";
                      }
                  });

    $scope.$watch('address.city',function(o,n)
                  {
                      if (o !== n)
                      {
                          getOfficeNovayaPochta().done(function(data) {$scope.address.offices = data;});
                          $scope.address.novayaPochta = "";
                      }
                  });
}

function UsersAutocompleteMixin($scope)
{
    $scope.users = function(val)
    {
        if (typeof val === "string")
            return userAutocomplete(val);
    };
}

// Top level controller, allows insert dynamic content
app.controller("RefreshCtl",
               ["$scope","$compile",function($scope,$compile)
                {
                    $scope.refresh = function(element)
                    {
                        $compile(element)($scope);
                        $scope.$apply();
                    };
                }
               ]);

// statistics tab with dirty status panel
function refreshTabs(value)
{
    if (value)
    {
        $(byParam("custom-radio-value",value)).click();
    }
    retailAdminTabsWithStatisticsregion(value);
}

function hideOrShowPendingPanel()
{
    var panel = $(".save-pending-changes").parent();
    if((_.isBoolean(statisticTabs.arrivalEmpty) && statisticTabs.arrivalEmpty)
        ||
        _.every(_.values(statisticTabs),_.partial(_.isEqual,0)))
    {
        panel.hide();
    }
    else
    {
        panel.show();
    }
}

$(function() {window.statisticTabs && hideOrShowPendingPanel();});
$(document).on("region.retail-admin-tabs-with-statistics",hideOrShowPendingPanel);
