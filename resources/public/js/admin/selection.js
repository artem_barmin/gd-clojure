var selectedItems = [];

$(document).on("click","[role=selection]",function(ev)
               {
                   var element = ev.currentTarget;
                   var container = $(element).closest("[data-id]");
                   if (!container.length) container = $(element);
                   var id = container.data("id");
                   if (_.contains(selectedItems,id))
                   {
                       container.removeClass("selection-active");
                       _.pull(selectedItems,id);
                   }
                   else
                   {
                       container.removeClass("selection-active");
                       selectedItems.push(id);
                   }
                   refreshHighlight();
               });

function refreshHighlight()
{
    $("[data-id]")
        .each(function(i,el)
              {
                  var el = $(el);
                  var contains = _.contains(selectedItems,el.data("id"));
                  el.toggleClass("selection-active",contains);
                  el.find("[name=choose]").attr('checked',contains);
              });

    $("[role=selected-count]").text(selectedItems.length ? "выбрано " + selectedItems.length : "");
    $(document).trigger("hightlight.refresh");
}

function selectAll(ids)
{
    selectedItems = ids;
    refreshHighlight();
}

function selectAllOnPage(el)
{
    var allStocksOnPage = _.map($("[data-id]"),function(el){return $(el).data("id");});
    if($(el).prop("checked"))
    {
        selectedItems = _.union(selectedItems,allStocksOnPage);
    }
    else
    {
        selectedItems = _.difference(selectedItems,allStocksOnPage);
    }
    refreshHighlight();
}

function deselectAll()
{
    selectedItems = [];
    refreshHighlight();
}

$(document).on("pager.change", refreshHighlight);
