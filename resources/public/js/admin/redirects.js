app.controller("redirectsCtrl",    ['$scope','$http','$timeout',function($scope,$http){

    $http.get("/restapi/seo/redirect")
        .success(function(data){
            $scope.redirects = data;
        });

    $scope.addRedirect = function(){
      $scope.redirects.push({from:'', to: '', enabled: false});
    };

    $scope.removeRedirect = function(index){
      $scope.redirects.splice(index,1)
    };

    $scope.validateURL = function(redirect){
        $http.get(redirect.to)
            .success(function(){
                redirect.status = true;
            })
            .error(function(){
                redirect.status = false;
            })
    };

    $scope.updateRedirects = function(){
        if(confirm("Вы уверенны что хотите сохранить изменения?")){
            var array = _.map($scope.redirects,function(redirect){
                return {id:redirect.id,
                        from: redirect.from,
                        to:redirect.to,
                        enabled:redirect.enabled}
            });
            $.post('/restapi/seo/redirect', {redirects: array});
        }
    };
}]);