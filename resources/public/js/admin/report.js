var contrastColors = ["#990000", "#400000", "#e53939", "#331a1a", "#f2b6b6", "#8c6969", "#4d3939", "#ff2200", "#e58273", "#8c4f46", "#bf5630", "#a6877c", "#ff6600", "#662900", "#bf8660", "#735039", "#995200", "#402200", "#e59539", "#734d00", "#bf9f60", "#f2deb6", "#8c8169", "#ffcc00", "#a68500", "#4c4113", "#ffe680", "#7f7b40", "#dae639", "#acbf60", "#558000", "#394d13", "#2f3326", "#7ee639", "#d9ffbf", "#8da67c", "#6cbf60", "#00732e", "#00401a", "#ace6cb", "#608071", "#00e699", "#29a67c", "#006652", "#00f2e2", "#003330", "#59b3ad", "#394d4b", "#006b73", "#008fb3", "#003340", "#79daf2", "#69858c", "#0099e6", "#005e8c", "#262f33", "#0058a6", "#0052cc", "#26364d", "#acc3e6", "#0044ff", "#7999f2", "#202d80", "#0000b3", "#000080", "#000066", "#26264d", "#8c86b3", "#9979f2", "#5b29a6", "#1b0033", "#9d3df2", "#e1bfff", "#75468c", "#520066", "#da79f2", "#311a33", "#4b394d", "#e600d6", "#99008f", "#a67ca3", "#400033", "#e60099", "#731d56", "#f279ca", "#33262f", "#ffbfe1", "#e5005c", "#660029", "#a6295b", "#990029", "#f27999"];

// filters callback
$(function()
  {
      if (window.salesReportResultregion)
      {
          $("[role=report-config]").on("change","input,select", function()
                                       {
                                           salesReportResultregion();
                                       });
          $("[name=group]").on("change",function(){selectedItems=[];});
          salesReportResultregion();
      }

      $(document).on("change","[name=tab-state]",function()
                    {
                        var currentTab = $("[name=tab-state]:checked").val();
                        $(".data-view").hide();
                        $(".data-view[id=" + currentTab + "]").show();
                        $(document).trigger("tab-selected." + currentTab);
                    });
  });

$(document).on("region.sales-report-result",function(){
                   makeLineChart();
                   makePieChart();
                   refreshHighlight();
               });

// Charts
function prepareSingleData(tr)
{
    var tr = $(tr);
    if (_.contains(selectedItems,tr.data("id")))
    {
        var yValues = tr.data("chart");
        var name = tr.data("real-name");
        return {key: name,
                color:contrastColors[name.hashCode() % contrastColors.length],
                values:_.map(yValues,function(el,i){return {y:el || 0,x:i};})};
    }
}

function prepareData()
{
    return _.compact(_.map($("tr[data-chart]"),prepareSingleData));
}

// Line chart
function makeLineChart()
{
    $('#line-chart svg').empty();
    nv.addGraph(function() {
                    var xValues = $("[data-x-labels]").data("x-labels");
                    var chart = nv.models.lineChart()
                        .margin({left: 100,right:100})
                        .useInteractiveGuideline(true)
                        .transitionDuration(350)
                        .x(function(d) { return d.x })
                        .showLegend(true)
                        .showYAxis(true)
                        .showXAxis(true);

                    chart.xAxis.axisLabel('Время').tickFormat(function(d)
                                                              {
                                                                  return xValues[d];
                                                              });
                    chart.yAxis.axisLabel('Продано').tickFormat(function(d)
                                                                {
                                                                    return d;
                                                                });

                    var data = prepareData();

                    d3.select('#line-chart svg').datum(data).call(chart);

                    nv.utils.windowResize(function() { chart.update() });

                    return chart;
                });
}

$(document).on("tab-selected.line-chart",makeLineChart);

// Pie chart
function makePieChart()
{
    $('#pie-chart svg').empty();
    nv.addGraph(function() {
                    var chart = nv.models.pieChart()
                        .labelType("percent")
                        .showLabels(true)
                        .donut(true)
                        .donutRatio(0.35);

                    var data = prepareData();

                    var pieData = _.map(prepareData(),function(el)
                                        {
                                            return {x:el.key,
                                                    y:_.reduce(el.values,function(a,b){return a+b.y;},0)};
                                        });

                    d3.select('#pie-chart svg').datum(pieData).transition().duration(350).call(chart);

                    nv.utils.windowResize(function() { chart.update() });

                    return chart;
                });
}

$(document).on("tab-selected.pie-chart",makePieChart);
