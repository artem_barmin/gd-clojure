function enableSearch(sel, callback)
{
    $(sel).keydown(_.debounce(callback,1000,{trailing: true, leading: false}));
}

$(function() {window.searchCallback && enableSearch("#search",searchCallback);});
