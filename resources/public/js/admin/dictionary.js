function deepTransform(obj, filter, transform) {
    var k, transformed, v, _i, _len;
    if (filter(obj)) {
        return transform(obj);
    } else if (_.isArray(obj)) {
        transformed = [];
        for (_i = 0, _len = obj.length; _i < _len; _i++) {
            v = obj[_i];
            transformed.push(deepTransform(v, filter, transform));
        }
        return transformed;
    } else if (_.isPlainObject(obj)) {
        transformed = {};
        for (k in obj) {
            v = obj[k];
            transformed[k] = deepTransform(v, filter, transform);
        }
        return transformed;
    } else {
        return obj;
    }
}

function transliterate(str){
    var arr={'а':'a', 'б':'b', 'в':'v', 'г':'g', 'д':'d', 'е':'e',
             'ж':'g', 'з':'z', 'и':'i', 'й':'y', 'к':'k', 'л':'l',
             'м':'m', 'н':'n', 'о':'o', 'п':'p', 'р':'r', 'с':'s',
             'т':'t', 'у':'u', 'ф':'f', 'ы':'i', 'э':'e', 'ё':'yo',
             'х':'h', 'ц':'ts','ч':'ch','ш':'sh','щ':'shch', 'ъ':'',
             'ь':'', 'ю':'yu', 'я':'ya'};
    var replacer=function(a){return arr[a]||a};
    return str.toLowerCase().replace(/[а-яё]/g,replacer);
}

app.controller("DictionaryCtl",
               ["$scope","$timeout",function($scope,$timeout)
                {
                    $scope.dictionary = dictionary;
                    $scope.conditions = deepTransform(dictionary["raw-conditions"],
                                                      _.isNumber,
                                                      function(a){
                                                          return _.find(existingParams,{id:a});
                                                      });

                    $scope.$watch('dictionary.param',function()
                                  {
                                      _.forEach(_.filter($scope.dictionary.param,'new'),
                                                function(param)
                                                {
                                                    if (param.name)
                                                        param.type = transliterate(param.name);
                                                });
                                  },true);

                    $scope.openBrandDialog = function(params)
                    {
                        $scope.params = params;
                        openDialogAngular("#brandParams");
                        $timeout(centerDialog);
                    };

                    $scope.openParamDialog = function(param)
                    {
                        $scope.param = param;
                        openDialogAngular("#paramDialog");
                        $timeout(centerDialog);
                    };

                    $scope.config = {};
                    $scope.visible = {};
                    $scope.selectParam = function(name,value,el)
                    {
                        if ($(el.currentTarget).is(":checked"))
                        {
                            if (!$scope.config[name])
                                $scope.config[name] = [];
                            $scope.config[name].push(value);
                        }
                        else
                        {
                            _.pull($scope.config[name],value);
                        }
                    };

                    $scope.openConditionsDialog = function()
                    {
                        openDialogAngular("#conditionsDialog");
                        $timeout(centerDialog);
                    };

                    $scope.saveCondition = function()
                    {
                        var config = $scope.config;
                        var conditions = _.map(config.conditions,
                                               function(cond)
                                               {
                                                   return _.find(existingParams,
                                                                 {name:cond.type, value:cond.value});
                                               });
                        delete config.conditions;
                        _.forEach(config,function(val,key)
                                  {
                                      $scope.conditions[key].push({condition: conditions, values:val});
                                  });
                        closeDialog();
                    };

                    $scope.paramNameFromDict = function(paramName)
                    {
                        var fromDict = _.find(dictionary.param,{type:paramName});
                        return fromDict ? fromDict.name : paramName;
                    };

                    $scope.getAllValues = getAllValues;

                    $scope.getDictionary = function()
                    {
                        var dictionary = _.cloneDeep($scope.dictionary);
                        dictionary.conditions = _.cloneDeep($scope.conditions);
                        deepTransform(
                            dictionary.conditions,
                            function(m)
                            {
                                return _.isPlainObject(m) && m.values;
                            },
                            function (m)
                            {
                                m.values = _.map(m.values,'value');
                                m.condition = _.zipObject(_.map(m.condition,'name'),_.map(m.condition,'value'));
                                return m;
                            });
                        return angular.copy(dictionary);
                    };

                    $scope.computeVisibilityHash = function(param,conditions)
                    {
                        return param + conditions.special + _.map(conditions.condition,'id').join(' ');
                    };

                    $scope.toggleVisibility = function(hash)
                    {
                        if ($scope.visible[hash])
                        {
                            $scope.visible[hash] = false;
                        }
                        else
                        {
                            $scope.visible[hash] = true;
                        }
                    };
                }
               ]);

function getDictionary()
{
    return getScopeParam("#dictionary","getDictionary")();
}

function getChangedParams()
{
    return _.filter(existingParams,function(p){return p.old!=p.value;});
}

function getValuesForParam(name,allParams)
{
    var result = [];
    if (dictionary.conditions)
        _.map(dictionary.conditions[name],
              function(cond)
              {
                  var findCondition = _.mapValues(cond.condition,function(v){return [v];});
                  if (_.find([allParams],findCondition) || !cond.condition)
                      result.push(cond.values);
              });
    return _.uniq(_.flatten(result));
}

function getAllValues(name)
{
    var result = [];
    _.map(dictionary.conditions[name],
          function(cond)
          {
              result.push(cond.values);
          });
    return _.uniq(_.flatten(result));
}
