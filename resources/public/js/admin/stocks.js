function emptySearch()
{
    $("#search").val("");
}

function listItemNew()
{
    return _.every(selectedItems,_.isString);
}

$(function()
  {
      var tooltip = $("#merge-stocks-tooltip");
      $(".stocks-list").on("click","input[id=choose]",
                           function(evt) {
                               setTimeout(function()
                                          {
                                              if (selectedItems.length > 1 &&  listItemNew())
                                              {
                                                  tooltip.css({top: evt.pageY - 26, left: evt.pageX + 20}).show();
                                              }
                                              else
                                              {
                                                  tooltip.hide();
                                              }
                                          },0);
                           });

      $(document).on("pager.change", function(){tooltip.hide();});

      $(document).on("hightlight.refresh",function()
                     {
                         if(selectedItems.length < 1)
                         {
                             $("#merge-stocks-tooltip").hide();
                         }
                     });
  });

// filters callback
$(function()
  {
      if (window.searchCallback)
          $(document).on("change","[role=main-filters] input:not([ignore]),[role=main-filters] select", searchCallback);
  });

// update group edit results
$(function()
  {
      $("[href=#group-results]").on("show.bs.tab",function(){
                                        groupEditResultsregion();
                                    });
  });

var filterName = _.memoize(function(type)
                           {
                               var param = _.find(dictionary.param,{type:type});
                               return param && param.name;
                           });

app.controller("FiltersConstraintCtl",
               ["$scope",function($scope)
                {
                    $scope.filtered = filtersAvailable;

                    $scope.$watch('search',processFiltering);

                    $scope.filterName = function(type)
                    {
                        return filterName(type);
                    };

                    var processFiltering = function()
                    {
                        if ($scope.search && $scope.search.length)
                        {
                            var search = $scope.search.toLowerCase();
                            $scope.filtered =
                                _.mapValues(filtersAvailable,
                                            function(vals)
                                            {
                                                return _.filter(vals,function(s)
                                                                {
                                                                    return s.toLowerCase().indexOf(search)>-1;
                                                                });
                                            });
                        }
                        else
                            $scope.filtered = filtersAvailable;
                    };

                    $(document).on("region.organizer-stock-filters",function()
                                   {
                                       processFiltering();
                                       $scope.$apply();
                                   });
                }]);

function openParamFilters(that)
{
    $("[role=param-filters]").toggle().position({of: $(that),my: "left top",at:"left bottom"});
}
