function setCurrentStock(stock)
{
    var scope = angular.element($("#stock-modal")).scope();
    scope.setStock(_.cloneDeep(stock));
    scope.$apply();
}

function getCurrentStock()
{
    return getScopeParam("#stock-modal","stock");
}

function getCurrentGroupStock()
{
    return getScopeParam("#group-stock-modal","stock");
}

function setCurrentGroupStock(stock)
{
    var scope = angular.element($("#group-stock-modal")).scope();
    scope.setStock(_.cloneDeep(stock));
    scope.$apply();
}

function addStockImage(image)
{
    var scope = angular.element($("#stock-modal")).scope();
    scope.addImage(image);
    scope.$apply();
}

function StockParametersViewMixin($scope)
{
    var paramsDictionary = _.groupBy(dictionary.param,'type');

    $scope.refreshParamNames = function()
    {
        $scope.paramNames =
            _.map($scope.stock.params,
                  function(vals,key)
                  {
                      return {type:key,
                              name: paramsDictionary[key] ? paramsDictionary[key][0].name : key,
                              original:key};
                  });
    };

    $scope.$watchCollection(function()
                            {
                                return Object.keys($scope.stock.params);
                            },
                            $scope.refreshParamNames);

    $scope.$watchCollection(function()
                            {
                                return _.pluck($scope.paramNames,"type");
                            } ,function()
                            {
                                _.forEach($scope.paramNames,function(param)
                                          {
                                              if (param.type!=param.original)
                                              {
                                                  $scope.stock.params[param.type]
                                                      = $scope.stock.params[param.original];
                                                  delete $scope.stock.params[param.original];
                                              }
                                          });
                            });
}

function StockCtl($scope)
{
    StockParametersViewMixin($scope);
    $scope.stock = newStock;
    $scope.originalStock = newStock;
    $scope.dictionary = dictionary;
    $scope.multipleAdd = false;
    $scope.stock.multipleAddAmount;

    $scope.setMultipleAdd = function()
    {
       this.multipleAdd = true;
    };

    $scope.setSingleAdd = function()
    {
        this.multipleAdd = false;
    };

    $scope.stock.image = $scope.stock.images[0];

    var st = $scope.stock;

    $scope.setStock = function(state)
    {
        $scope.originalStock = _.cloneDeep(state);
        $scope.stock = state;
        st = $scope.stock;
        st.image = st.images[0];
        st.accounting = st.accounting || dictionary.accounting;
        $scope.refreshCategory();
        $scope.refreshParamNames();
        _.forEach($scope.paramNames,
                  function(name) {$scope.config.useInVariants[name.type] = isVariantParameter(name.type);});
    };

    // Images
    $scope.addImage = function(path)
    {
        st.images.push(path);
        st.image = path;
    };

    $scope.removeImage = function(index)
    {
        st.images.splice(index, 1);
        st.image = st.images[0];
    };

    $scope.makeImageMain = function()
    {
        _.pull(st.images, st.image);
        st.images.unshift(st.image);
    };

    // Measurements
    $scope.addMeasurement = function()
    {
        st.description.measurements[$scope.config.currentMeasurementSize].push({});
    };

    $scope.removeMeasurement = function(index)
    {
        st.description.measurements[$scope.config.currentMeasurementSize].splice(index,1);
    };

    $scope.leftMeasurements = function()
    {
        return dictionary.measurements;
    };

    // Variants management
    $scope.addVariant = function()
    {
        if ($scope.canAddVariants())
            st.variant.push({params:{},contexts:{"wholesale-sale":{}, "retail-sale":{}}});
    };

    $scope.canAddVariants = function()
    {
        if(!_.some($scope.config.useInVariants,_.identity))
            return st.variant.length<1;
        return true;
    };

    $scope.removeVariant = function(index)
    {
        var variant = st.variant[index];
        if (variant.left==null || variant.left==0)
            st.variant.splice(index,1);
    };

    $scope.variantParamValues = function(name)
    {
        return st.params[name];
    };

    function cartesianProduct(arrays) {
        return _.reduce(arrays,
                        function(a, b)
                        {
                            return _.flatten(_.map(a,
                                                   function(x)
                                                   {
                                                       return _.map(b,
                                                                    function(y)
                                                                    {
                                                                        return x.concat([y]);
                                                                    });
                                                   }), true);
                        }, [ [] ]);
    };

    $scope.fillAllVariants = function()
    {
        // collect all params
        var params = [];
        var paramNames = [];
        _.map($scope.paramNames,
              function(paramName)
              {
                  if ($scope.config.useInVariants[paramName.type])
                  {
                      paramNames.push(paramName.type);
                      params.push($scope.stock.params[paramName.type]);
                  }
              });

        // generate cartesian product
        var cartesian = cartesianProduct(params);

        // generate needed variants
        _.forEach(cartesian,function(variant)
                  {
                      var params = _.zipObject(paramNames,variant);
                      if (!_.find($scope.stock.variant,{params:params}))
                      {
                          st.variant.push({params:params,contexts:{"wholesale-sale":{}, "retail-sale":{}}});
                      }
                  });
    };

    // Parameter names
    $scope.isParamNamesLeft = function()
    {
        return !_.isEmpty($scope.leftParams());
    };

    $scope.leftParams = function()
    {
        var existing = Object.keys($scope.stock.params);
        return _.filter(dictionary.param,function(param)
                        {
                            return !_.contains(existing,param.type);
                        });
    };

    $scope.addParameter = function()
    {
        if ($scope.isParamNamesLeft())
            st.params[$scope.leftParams()[0].type] = [""];
    };

    // Parameter values
    $scope.addParameterValue = function(paramName)
    {
        st.params[paramName].push("");
    };

    $scope.valueUsedInVariant = function(paramName,value)
    {
        return _.some(st.variant,function(variant) {return variant.params && variant.params[paramName]==value;});
    };

    $scope.deleteParameterValue = function(paramName,index)
    {
        // can delete only if it is not used in the variant
        var value = st.params[paramName][index];
        if (!$scope.valueUsedInVariant(paramName,value))
        {
            st.params[paramName].splice(index,1);
            if (_.isEmpty(st.params[paramName]))
            {
                delete st.params[paramName];
            }
        }
    };

    $scope.leftParamValues = function(name)
    {
        return getValuesForParam(name,st.params);
    };

    $scope.config = {currentMeasurementSize:null,useInVariants:{},usedInVariants:{}};
    $scope.globalPrice = {"wholesale-sale":{},"retail-sale":{}};

    $scope.$watch('globalPrice',function(o,n)
                  {
                      _.map($scope.globalPrice,function(val,key)
                            {
                                if (!_.isEmpty(val))
                                    _.forEach(st.variant,function(variant)
                                              {
                                                  if (!variant.contexts[key])
                                                      variant.contexts[key] = {};
                                                  _.assign(variant.contexts[key],val);
                                              });
                            });
                  },true);

    $scope.$watch('stock.sale',function(state)
                  {
                      st.context["retail-sale"].status = state && st.context["retail"].status;
                      st.context["wholesale-sale"].status = state && st.context["wholesale"].status;
                  });

    $scope.minimalPrice = function()
    {
        var minPrice = Infinity;
        for(var i in st.variant)
        {
            for(var ctx in st.variant[i].contexts)
            {
                var price = st.variant[i].contexts[ctx].price;
                if (price)
                    minPrice = Math.min(minPrice,price);
            }
        }
        if (isFinite(minPrice))
            return minPrice;
        else
            return 0;
    };

    $scope.refreshCategory = function()
    {
        st.category = {name: $("[name=category]")
                       .find(byParam("value",st.fkey_category))
                       .first()
                       .text()};
    };

    $scope.$watch('stock.fkey_category',$scope.refreshCategory);

    $scope.$watch('config.currentMeasurementSize',
                  function(size)
                  {
                      if (!st.description)
                          st.description = {};
                      if (!st.description.measurements)
                          st.description.measurements= {};
                      if (!st.description.measurements[$scope.config.currentMeasurementSize])
                          st.description.measurements[$scope.config.currentMeasurementSize] = [];
                  });

    var isVariantParameter = function(name)
    {
        // if parameter is used in variants - it should not be disabled easily(just be deleting all variants)

        // variantParams - is used for group operations(because we can omit variants for stock, but by default
        // some params should be marked as variant)
        return _.contains(st.variantParams || [],name)
            ||
            _.some(st.variant,function(variant)
                   {
                       if (!variant.params)
                           return false;
                       var val = variant.params[name];
                       return val!="null" && val;
                   }) ;
    };

    var checkVariants = function(use)
    {
        _.forEach($scope.paramNames,
                  function(name) {$scope.config.usedInVariants[name.type] = isVariantParameter(name.type);});
    };

    $scope.$watchCollection('paramNames',checkVariants);
    $scope.$watchCollection('stock.variant',checkVariants);


    $scope.addProperty = function() {
        if(typeof st.description.properties == "undefined"){
            st.description["properties"] = [];
        }
        st.description.properties.push({name:"",value:""});
    };

    $scope.removeProperty = function(index) {
        delete st.description.properties.splice(index,1);
    };
}
app.controller("StockCtl",["$scope",StockCtl]);
