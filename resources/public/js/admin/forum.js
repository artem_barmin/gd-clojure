var highlightForumMessage = function()
{
    if (window.location.hash)
    {
        var id = window.location.hash.match(/[0-9]+/)[0];
        $("[data-id=" + id + "]").addClass("highlight");
    }
};

$(window).on('hashchange', highlightForumMessage);
addPostAjaxHandler(highlightForumMessage);
