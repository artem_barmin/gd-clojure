function getStoreSettings ()
{
    var bannerCodes = [];
    angular.forEach(getCurrentImages(), function (image)
    {
        bannerCodes.push(image.code);
    });
    var reviewSettings = {"mainReviewNumber": $("#mainReviewNumber").val(),
                          "detailReviewNumber": $("#detailReviewNumber").val()};
    var newsSettings = {"mainNewsNumber": $("#mainNewsNumber").val(),
                        "detailNewsNumber": $("#detailNewsNumber").val()};
    return {"storeName": $("#store-name").val(),
            "banners": bannerCodes,
            "reviewSettings": reviewSettings,
            "newsSettings":newsSettings,
            "footerSettings": {"footerTop": implementRowSize(getCurrentFooterTop()),
                               "footerCenter": implementRowSize(getCurrentFooterCenter())}};
}

function implementRowSize (array)
{
    var fullCompleteArray = [];
    switch (array.length){
        case 1: {
            fullCompleteArray = setRowSizes(array, "col-lg-12 col-md-3");
            break;
        }
        case 2: {
            fullCompleteArray = setRowSizes(array, "col-lg-6 col-md-3");
            break;
        }
        case 3: {
            fullCompleteArray = setRowSizes(array, "col-lg-4 col-md-3");
            break;
        }
        case 4: {
            fullCompleteArray = setRowSizes(array, "col-lg-3 col-md-3");
            break;
        }
        default: {
            break;
        }
    }
    return fullCompleteArray;
}

function setRowSizes (array, rowSize)
{
    angular.forEach(array, function (element)
    {
        element.rowSize = rowSize;
    });
    return array;
}

function addFooterTop(settings)
{
    var scope = angular.element($("[ng-controller=SettingsCtl]")).scope();
    if (scope.footerTop.length < 4)
    {
        scope.addFooterTop(settings);
        scope.$apply();
    }

}

function addFooterCenter(settings)
{
    var scope = angular.element($("[ng-controller=SettingsCtl]")).scope();
    if (scope.footerCenter.length < 4)
    {
        scope.addFooterCenter(settings);
        scope.$apply();
    }
}

function addFooterCenterParamPair(element, settings)
{
    element.push(settings);
    var scope = angular.element($("[ng-controller=SettingsCtl]")).scope();
    scope.$apply();
}

function getCurrentFooterTop()
{
    return angular.copy(angular.element($("[ng-controller=SettingsCtl]")).scope().footerTop)
}


function getCurrentFooterCenter()
{
    return angular.copy(angular.element($("[ng-controller=SettingsCtl]")).scope().footerCenter)
}

function setGlif (elem, classValue)
{
    var input = $(elem).parent().parent().parent().parent().children("#glif-class");
    input.val(classValue);
    input.trigger('input');
}

app.controller("SettingsCtl",
               ["$scope", function($scope)
               {
                   $scope.footerTop = [];
                   $scope.footerCenter = [];

                   $scope.addFooterCenterParamPair = function (element, settings)
                   {
                       element.push(settings);
                   };

                   $scope.removePair = function (element, index)
                   {
                        element.splice(index, 1);
                   };

                   $scope.addFooterTop = function(settings)
                   {
                       $scope.footerTop.push(settings);
                   };
                   $scope.addFooterCenter = function(settings)
                   {
                       $scope.footerCenter.push(settings);
                   };
                   $scope.removeFooterTop = function(index)
                   {
                       $scope.footerTop.splice(index,1);
                   };
                   $scope.removeFooterCenter = function(index)
                   {
                       $scope.footerCenter.splice(index,1);
                   };
               }]);
