app.controller("OrdersCreationWizardCtl",
               ["$scope",function($scope)
                {
                    UsersAutocompleteMixin($scope);
                    UserProfileManagementMixin($scope);
                }
               ]);

function refreshList()
{
    $dialog("[role=infinite-scroll]").data("infinite-scroll").refreshList();
}

function extendOrderUsingWizard(userId,userName)
{
    openDialogAngular($("[role=create-panel][data-key=order]"));
    $dialog("[name=author]").val(userName);
    $dialog("[name=user]").val(userId);
}

$(function()
  {
      var wizard = $("[ng-controller=OrdersCreationWizardCtl]");
      if (wizard.size())
      {
          enableSearch(wizard.find("[name=search]"),refreshList);
          wizard.find("[role=wizard-stock-filters]").on("change","input,select",refreshList);
          $("[href=#result]").on("show.bs.tab",function(){wizardResultregion();});
      }
  });
