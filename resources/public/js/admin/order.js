app.controller("OrdersCustomNotificationCtl",
               ["$scope",function($scope)
                {
                    $scope.order = {};
                    $scope.$watch('order',function()
                                  {
                                      setTimeout(function()
                                                 {
                                                     $dialog("[data-title]").data("tooltip",null).customTooltip({});
                                                 },0);
                                  },true);
                }
               ]);

addPostAjaxHandler(function()
                   {
                       if ($.lastDialog && haveElement("[ng-controller=OrdersCustomNotificationCtl]"))
                           angular.element("body").scope().refresh($dialog("[ng-controller]"));
                   });

function getCurrentOrderInfo()
{
    return getScopeParam($dialog("[ng-controller=OrdersCustomNotificationCtl]"),"order");
}
