function recomputeInventoryStates()
{
    $("[data-warehouse]").each(function(index, element)
                               {
                                   var input = parseInt($(element).val());
                                   var warehouse = parseInt($(element).attr("data-warehouse"));
                                   $(element).toggleClass("inventory-wrong-balance-input",input != warehouse);
                                   $(element).toggleClass("inventory-positive-balance-input",input > warehouse);
                               });

    $(".admin-table-body-container.params-container")
        .removeClass("inventory-wrong-balance");
    $(".admin-table-body-container.params-container:has(.inventory-wrong-balance-input)")
        .addClass("inventory-wrong-balance");
}

$(document).bind("pager.change",recomputeInventoryStates);
