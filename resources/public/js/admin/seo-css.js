app.controller("seoCssCtrl",    ['$scope','$http','$timeout',function($scope,$http){

    $scope.fileChanged= function(file){
        $.post("/restapi/seo/css",{reference: file}).success(function(data){
            $scope.current = data;
            editor.setValue($scope.current.css);
        })
    };

    $http.get("/restapi/seo/css").success(function(data){
      $scope.files = data.files;
      $scope.currentFile = $scope.files[0].reference;
      $scope.fileChanged($scope.files[0].reference);
    });

    $scope.getCss = function (){
        if(confirm("Все не сохранённые данные будут потеряны. Продолжить?")){
            $scope.fileChanged($scope.current.reference);
        }
    };

    $scope.saveCss = function(){
        if(confirm("Вы уверенны что хотите сохранить изменения?")){
            $scope.current.css = editor.getValue();
            $.post('/restapi/seo/css/update', $scope.current);
        }
    };
}]);