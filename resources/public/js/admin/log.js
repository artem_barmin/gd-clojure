function startLogCapture(id)
{
    var loc = window.location;
    var wsUri = "ws://" + loc.host + "/admin/events/" + id;

    var ws = new WebSocket(wsUri);

    ws.onmessage = function(e)
    {
        var data = JSON.parse(e.data);

        if (data.event)
        {
            var tr = $("<tr/>");
            tr.addClass('infinite-scroll-row');

            var td = $("<td/>");
            tr.append(td);

            td.append(data.event);
            $("[role=infinite-scroll][data-name=events-log] tbody").prepend(tr);
        }
    };
};
