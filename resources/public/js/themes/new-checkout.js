function nextStep(current, next)
{
    var thisState = "." + current;
    var nextState = "." + next;
    $(String(thisState)).hide();
    $(String(nextState)).show();
}

function addAction (elem) {
    console.log($(elem));
    if ($(elem).hasClass('add-up')) {
        $("[name=quantity]", '.quantity-adder').val(parseInt($("[name=quantity]", '.quantity-adder').val()) + 1);
    } else {
        if (parseInt($("[name=quantity]", '.quantity-adder').val()) > 1) {
            $("input", '.quantity-adder').val(parseInt($("[name=quantity]", '.quantity-adder').val()) - 1);
        }
    }
}