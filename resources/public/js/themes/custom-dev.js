allFunctions = {};

addPostAjaxHandler(function()
                   {
                       $("[template-id]").each(function(i,el)
                                               {
                                                   var el = $(el);
                                                   var template = el.next();
                                                   var pos = template.offset();
                                                   el.show();
                                                   el.offset({left:pos.left + template.width() - 15, top:pos.top});
                                                   template.css("border","1px solid red");
                                               });
                   });

$(document).on("click","[toggle-next]",function(ev)
               {
                   var panel = $(ev.currentTarget).next("[toggle-panel]");
                   var indicator = $(ev.currentTarget).find("[toggle-indicator]");
                   panel.toggle();
                   indicator.html(panel.is(":visible")?"-":"+");
               });

function toggleDebugPanel(that,next)
{
    var $that = $(that);

    if ($that.data("template"))
    {
        $that.data("template").remove();
        $that.data("template",null);
    }
    else
    {
        next.done(function(res)
                  {
                      var pos = $that.offset();
                      var el = $that.next().clone();
                      $('body').append(el);
                      el.show();
                      el.offset({left:pos.left + $that.width(), top:pos.top});
                      $that.data("template",el);
                  });
    }
}
