var app = angular.module('app',['mgcrea.ngStrap', 'ngSanitize', 'ngAnimate']);

app.controller("ThemesController",
               ["$scope","$compile","$timeout",function($scope,$compile,$timeout)
                {
                    UserProfileManagementMixin($scope);

                    $scope.refresh = function(element,data)
                    {
                        var scope = $scope.$new();
                        _.assign(scope,data);
                        var compiled = $compile(element)(scope);
                        scope.$apply();
                        return compiled;
                    };
                }
               ]);

function renderTemplate(templateId,data)
{
    return angular.element("body").scope().refresh($(templateId).html(),data);
}

$(function()
  {
      if ($(".filters-panel").length)
      {
          var previousFilterState = formState(".filters-panel");
          $(document).on("change",".filters-panel input",function()
                         {
                             if(!angular.equals(previousFilterState,formState(".filters-panel")))
                             {
                                 previousFilterState = formState(".filters-panel");
                                 themedStockListLoadPage(0);
                                 themedFiltersPanelregion();
                             }
                         });
      }

      var search = "input#search,input[name=search]";
      $(document).on("keypress",search,function(event)
                     {
                         if (event.keyCode===13)
                         {
                             window.location = "/search/" + $(search).val() + "/";
                         }
                     });

      $(document).on("click",".clear-filter",function(event)
                     {
                         var checkboxes = $(event.target).closest(".filter-panel").find("input");
                         checkboxes.attr("checked", false);
                         checkboxes.first().change();
                     });

      $(document).on("keyup","[role=enter] input",function(event)
                     {
                         if(event.keyCode===13)
                         {
                             $(event.target).closest("[role=enter]").find("[role=submit] [onclick]").click();
                         }
                     });

      $(document).on("change","[role=param]",function()
                     {
                         var state = elementState("[role=param]");
                         var selectedParams = _.keys(state);

                         // get all needed variants
                         var variants = _.filter(window.variants,{params: state});

                         // correct stock price
                         if (variants.length==1)
                         {
                             $("[role=price]").text(variants[0].price);
                         }
                     });

  });

$(document).on("region.delayed-carousel",
               function()
               {
                   $('.carousel .item').each(
                       function()
                       {
                           var next = $(this).next();
                           if (!next.length)
                           {
                               next = $(this).siblings(':first');
                           }
                           next.children(':first-child').clone().appendTo($(this));

                           for (var i = 0; i < 2; i++)
                           {
                               next=next.next();
                               if (!next.length)
                               {
    	                           next = $(this).siblings(':first');
  	                       }

                               next.children(':first-child').clone().appendTo($(this));
                           }
                       });
               });

addPostAjaxHandler(function () {
    $.each($('[data-text]'), function(i,el){
        var el = $(el);
        el.text(el.data('text'));
        el.removeAttr('data-text');
    });
});

$(".order-button").live("click",function(){
    _gaq.push(['_setAccount', 'UA-46010790-1'],['_trackEvent', 'Cart', 'Click','Pokupka',1]);
    yaCounter22110703.reachGoal('ORDER'); return true;
});

$(document).on("region.bono-registration-modal-window",function()
               {
                   angular.element("body").scope().refresh($(".bono-registration-modal-window"),{});
               });
