function resolveTypeToLabel(mail)
{
    switch(mail)
    {
    case "novaya-pochta":
        return "Доставка на отделение Новой Почты";
    case "ukr-pochta":
        return "Доставка на отделение УкрПочты";
    }
}

function setShippingValueToHeader()
{
    var mail = formState($("#checkout"))["mail-type"];
    $("#shipping-result").show().text(resolveTypeToLabel(mail));
}

function setActivationPanelState(state)
{
    hideValidation();
    var finishButton = $(".retail-checkout-agreement .ok-button-plain");
    finishButton.removeClass("enabled");
    if(state=="aggreement")
    {
        finishButton.addClass("enabled");
    }
}

function activate(state)
{
    setShippingValueToHeader();
    $("h3." + state).mouseup();
    setActivationPanelState(state);
}

// Place right panel on the right side of active accordion panel
$( "#wizard" ).on( "accordionactivate", function( event, ui )
                   {
                       $(".retail-checkout-agreement").position({of:$("#wizard > h3[aria-selected=true]:visible"),
                                                                 my:"left top",
                                                                 at:"right top",
                                                                 offset:"15px 0px"});
                   } );
