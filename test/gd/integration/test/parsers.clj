(ns gd.integration.test.parsers
  (:use korma.core)
  (:use [taoensso.timbre :only (set-config!)])
  (:use gd.utils.common)
  (:use gd.model.model)
  (:use gd.log)
  (:use gd.model.test.data_builders)
  (:use clojure.test)
  (:use gd.utils.test)
  (:use [gd.utils.web :exclude (by-name current-url)])
  (:use [clj-time.core :only (interval in-secs)])
  (:use clj-time.coerce)
  (:use gd.views.test.utils.components)
  (:use gd.views.test.utils.common)
  (:use gd.views.test.utils.composite)
  (:use [clj-webdriver.taxi :exclude [exists? select]]))

(deftest parser-execution-integration-test
  (clean-scheduler)
  (clean-db)

  (save-categories-tree [["Retail-stocks" "stocks"
                          [["Женщинам" "female"
                            [["Майки и Футболки" "topsAndTShirts"
                              [["Майка" "top"]
                               ["Майка бельевая" "underTop"]
                               ["Борцовка" "highTop"]
                               ["Футболка" "tShirt"]]]]]
                           ["Мужчинам" "male"
                            [["Майки и футоболки" "topsAndTShirts"
                              [["Борцовка" "highTop"]
                               ["Футболка" "tShirt"]
                               ["Безрукавка" "withoutSleeve"]]]]]]]

                         ["other" "other"]])

  (let [root (make:user-internal
              "admin" "admin"
              :real-name "Администратор" :contact {:phone "+380509790587" :email "root@root.com"})
        supplier (make:supplier :stock-num 5
                                :stock (fn[i]
                                         {:category (:id (category:get-by-key "stocks-female-topsAndTShirts-top"))
                                          :images [{:path (use-image :items (inc i))}]
                                          :price 50
                                          :params {[:color false] [:green]
                                                   :size [:xxl [:xl 10]]
                                                   [:collection false] ["Лето"]}}))
        deal (make:deal :min_sum
                        :stock-num 5
                        :stock (fn[i] {:images [{:path (use-image :items (inc i))}]}))
        check-parsers-execution (fn[cond] (seq (select group-deal-update (where cond))))]

    (with-redefs [gd.model.model/offsets-for-tasks {:parser-execution 1000}]
      (wrap-security root

        (set-config! [:ns-blacklist] ["gd.parsers.jquery-parser"])

        (admin:set-parser-info :supplier supplier {:name "test"})
        (admin:set-parser-info :deal deal {:name "test"})

        (schedule:start-or-update 5 :parser-execution {:supplier supplier} nil)
        (schedule:start-or-update 5 :parser-execution nil deal)

        (while (not (and (check-parsers-execution {:fkey_supplier supplier})
                         (check-parsers-execution {:fkey_group-deal deal})))
          (Thread/sleep 100))

        (clean-scheduler)

        (testing "check that parsers are executed with enougth large period"
          (let [[t1 t2 :as times] (map :last-execution-time (select scheduled-events))]
            (is (= 2 (count times)))
            (is (> (in-secs (interval (from-sql-date t1) (from-sql-date t2))) 3))))

        (info "Parsers executed")

        (set-config! [:ns-blacklist] [])))

    (clean-db)))

;; ;; starter - start server, so we can parse it
;; (do
;;   (enable-dev-mode)
;;   (start-server)
;;   (run-tests))
