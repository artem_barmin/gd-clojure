(ns gd.views.test.utils.components
  (:require [clojure.string :as s])
  (:use gd.utils.common)
  (:use gd.model.model)
  (:use gd.model.test.data_builders)
  (:use clojure.test)
  (:use gd.utils.test)
  (:use gd.views.test.utils.common)
  (:use com.reasonr.scriptjure)
  (:use gd.views.test.utils.composite)
  (:use [clj-webdriver.taxi :exclude [exists?]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Common components
(defn scroll-to-bottom[]
  (let [height (execute-script "return $(document).height()")]
    (execute-script (format "scroll(0,%d)" height))))

(defn button[label]
  (click-text label))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Login interaction
(defn to-main[]
  (to (str "http://dayte-dve.com.ua:" (property "server.port" "8080"))))

(defn internal-login[login password]
  (to-main)
  (window-focus)
  (input-text "#login" login)
  (click "[value='Пароль'][type='text']")
  (input-text "#password" password)
  (click-text "Вход" "#login-button")
  (wait-until-element-present (contains "Выход")))

(defn logout[]
  (click-text "Выход"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Infite scroll interaction
(defn deal[name]
  (wait-url-change (click-text name)))

(defn stock-element[name]
  (contains name ".stock-image-list .cover"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Wysiwyg
(defn wysiwyg[text]
  (let [selector ".cleditorMain:visible textarea"]
    (execute-script
     (js (. ($ (clj selector)) val (clj text))
         (. (. ($ (clj selector)) data "cleditor") updateFrame)))))

(defn- wysiwyg-select-all[]
  (execute-script
   (js (. (. ($ ".cleditorMain:visible textarea") data "cleditor") select))))

(defn wysiwyg-execute-command[type]
  (wysiwyg-select-all)
  (click (format ".cleditorToolbar:visible div[title='%s']"
                 (type {:bold "Bold"
                        :italic "Italic"
                        :underline "Underline"
                        :clear-formatting "Remove Formatting"}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Forum shortcuts
(defn navigate-forum[& levels]
  (doseq [level levels]
    (click-text level)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ajax file interaction
(defn send-file[file-upload-element path]
  (execute-script (format "$(\"%s\").css('visibility','visible')" file-upload-element))
  (wait-ajax (send-keys file-upload-element (.getAbsolutePath (new java.io.File path))))
  (execute-script (format "$(\"%s\").css('visibility','hidden')" file-upload-element)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Search interaction
(defn open-search-resuts[string]
  (input-text "#searchbox" string)
  (wait-until-element-present ".ui-autocomplete"))

(defn search-results[]
  (partition 3 (map text (elements ".search-price,.search-name,.search-deal"))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tests runner and fixture setup
(defn browser-fixture[type f]
  (enable-dev-mode)
  (start-server)
  (start type)
  (implicit-wait 3000)
  (try
    (f)
    (catch Exception e
      (when (browser-active?)
        (screen)))
    (finally
      (when (not= "8080" (property "server.port" "8080"))
        (quit)))))

(defmacro run-web-tests[& {:keys [type] :or {type :chrome}}]
  `(do
     (gd.model.test.data_builders/enable-dev-mode)
     (use-fixtures :each (partial browser-fixture ~type))
     (run-tests)))
