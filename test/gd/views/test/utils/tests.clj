(ns gd.views.test.utils.tests
  (:use gd.utils.common)
  (:use clojure.test)
  (:use gd.views.test.utils.components)
  (:use gd.views.test.utils.common)
  (:use gd.views.test.utils.composite)
  (:use [clj-webdriver.taxi :exclude [exists?]]))

(defn uuid [] (str (java.util.UUID/randomUUID)))

(set-finder! css-finder)

(start :chrome)

(defn login[login password]
  (to "https://www.elance.com/php/landing/main/login.php")

  (input-text "#login_name" login)
  (input-text "#passwd" password)
  (click "#spr-sign-in-btn-standard")

  (wait-until-element-present "#challengeAnswerId")
  (input-text "#challengeAnswerId" "1")
  (click "#ContinueLogin")

  (wait-until-element-present "#tipMsg")

  (to "https://www.elance.com/profile"))

(defn exist-nested?[td el]
  (try (find-element-under td (clj-webdriver.core/by-css el))
       (catch org.openqa.selenium.NoSuchElementException e
         nil)))

(defn exist-nested-xpath?[td el]
  (try (find-elements-under td (clj-webdriver.core/by-xpath el))
       (catch org.openqa.selenium.NoSuchElementException e
         nil)))

(defn open-test[name]
  (let [td2 (last (xpath-finder (str "//tr[descendant::td/div[text()='" name "']]/td")))]
    (cond (exist-nested? td2 ".test_menu")
          (do
            (click (find-element-under td2 (clj-webdriver.core/by-css ".test_menu")))
            (while (not (exist-nested? td2 ".improve")))
            (click (find-element-under td2 (clj-webdriver.core/by-css ".improve"))))

          (let [take (exist-nested-xpath? td2 "a")]
            (seq (filter (comp (partial = "Take Test") text) take)))
          (let [take (exist-nested-xpath? td2 "a")
                [link] (filter (comp (partial = "Take Test") text) take)]
            (click link))

          (exist-nested? td2 ".take-test")
          (click (find-element-under td2 (clj-webdriver.core/by-css ".take-test")))

          (exist-nested? td2 ".take")
          (click (find-element-under td2 (clj-webdriver.core/by-css ".take")))

          (exist-nested? td2 ".improve")
          (click (find-element-under td2 (clj-webdriver.core/by-css ".improve")))

          (exist-nested? td2 ".resume")
          (click (find-element-under td2 (clj-webdriver.core/by-css ".resume")))

          true
          (throwf "Unusual case")))

  ;; (let [subtest "Core Java"
  ;;       selector (str ".eol-dialog-body .btn-xlarge[title='" subtest "']")]
  ;;   (if (visible? selector)
  ;;     (click selector)))

  (wait-until-element-present "#smartererTestingWidgetMasterIFrame")
  (switch-to-frame "#smartererTestingWidgetMasterIFrame")

  (wait-until-element-present ".beginTest")
  (click ".beginTest"))

(defonce results (atom #{}))

(defn save-results[]
  (spit (str "test/gd/views/test/utils/results/" (uuid))
        (with-out-str (clojure.pprint/pprint (vec @results))))
  (println "Results saved")
  (reset! results #{}))

(defn answers[]
  (->>
   (.listFiles (new java.io.File "test/gd/views/test/utils/results"))
   (mapcat (fn[file] (read-string (slurp (.getAbsolutePath file)))))
   (map (fn[{:keys [question answer]}] {question answer}))
   (mreduce identity)))

(defn try-answer[]
  (let [question (text (element ".question_text"))
        answer (get (answers) question)]
    (if answer
      (doseq [variant (elements ".lastUnit")]
        (if (= (text variant) answer)
          (click variant)))
      (click (element ".active_choice")))))

(defn get-answer[]
  (wait-until-element-present ".question_text")
  (wait-until-element-present ".correct_answer")
  (let [question (text (element ".question_text"))
        answer (text (element ".correct_answer"))]
    (swap! results conj {:question question :answer answer})))

(defn continue[]
  (wait-until-element-present "[value=continue]")
  (click "[value=continue]")
  (wait-until-element-present ".timer.active"))

(defn pass-tests[]
  (try
    (cond

      (present? "[value=continue]")
      (do
        (get-answer)
        (continue)
        true)

      (present? ".lastUnit .sign_in.begin")
      (do (refresh)
          false)

      true
      (do
        (try-answer)
        (get-answer)
        (continue)
        true))
    (catch Exception e
      (.printStackTrace e *out*)
      true)))

(defn collect-tests[]
  (wait-until-element-present "#smartererTestingWidgetMasterIFrame")
  (switch-to-frame "#smartererTestingWidgetMasterIFrame")

  (while (pass-tests) (println "Question parsed")))

(defn collect-several-questions[]
  (open-test "Adobe Illustrator")
  (when-not (present? "[value=continue]")
    (wait-until-element-present ".active_choice"))
  (while (pass-tests) (println "Question parsed"))
  (save-results))
