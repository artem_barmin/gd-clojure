(ns gd.views.test.utils.project_components
  (:use gd.utils.common)
  (:use gd.model.model)
  (:use gd.model.test.data_builders)
  (:use clojure.test)
  (:use gd.utils.test)
  (:use gd.views.test.utils.components)
  (:use gd.views.test.utils.common)
  (:use gd.views.test.utils.composite)
  (:use [gd.utils.web :exclude (by-name current-url)])
  (:use [clj-webdriver.taxi :exclude [exists?]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Misc utils
(defn paste-in [params key]
  (send-keys (by-name key) (get params key "")))

(defn clear-and-paste [params key]
  (clear (by-name key))
  (paste-in params key))

(defn select-category [name]
  (click ".tree-drilldown-container:last")
  (click-text name))

(defn press-and-check
  "Click on text and check that validation error occured."
  [text]
  (click-text text)
  (global (wait-until-element-present ".validation-error-tooltip")))

(defmacro in-modal[& body]
  `(section ".modal-main-block:visible"
            ~@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Server side helpers
(defn login-using-api[login password]
  (let [cookies
        (#'ring.middleware.cookies/parse-cookies
         {:headers {"cookie" (execute-script "return document.cookie")}})

        browser-session
        (get-in cookies ["PHPSESSID" :value])

        new-session
        (atom {})

        user-session
        (or (@session-map browser-session)
            (do (swap! session-map assoc browser-session new-session)
                new-session))]
    (swap! user-session assoc-in [:user] (user:check-credentials login password)))
  (refresh))
