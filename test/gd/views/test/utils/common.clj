(ns gd.views.test.utils.common
  (:use [clojure.java.shell :only (sh)])
  (:use clojure.walk)
  (:use com.reasonr.scriptjure)
  (:use clojure.test)
  (:use gd.utils.test)
  (:use gd.utils.common)
  (:use [gd.utils.web :exclude (by-name current-url)])
  (:use gd.log)
  (:use gd.views.test.utils.composite)
  (:use [clojure.java.io :only [file]])
  (:use [clojure.tools.namespace :only [find-namespaces-in-dir]])
  (:refer-clojure :exclude [val set])
  (:use [clj-webdriver.taxi :exclude [exists?]])
  (:use [clj-webdriver.driver :only [init-driver]])
  (:require [clj-webdriver.firefox :as ff])
  (:require [clj-webdriver.core :as core])
  (:require noir.server)
  (:import [org.openqa.selenium.chrome ChromeDriver ChromeDriverService ChromeOptions])
  (:import [org.openqa.selenium.htmlunit HtmlUnitDriver])
  (:import org.openqa.selenium.StaleElementReferenceException
           com.gargoylesoftware.htmlunit.BrowserVersion
           org.openqa.selenium.phantomjs.PhantomJSDriver
           org.openqa.selenium.remote.DesiredCapabilities
           org.openqa.selenium.chrome.ChromeDriverService$Builder))

(def timeout 5000)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Runner

(defn driver-active?[]
  (try (do (current-url) true) (catch Exception e false)))

(defn start[type & [use-xvfb]]
  (let [xvfb (or use-xvfb (property "use.xvfb" false))]
    (when xvfb
      (.waitFor (.exec (Runtime/getRuntime) "test/resources/start-xvfb.sh"))
      (Thread/sleep 1000))
    (when-not (driver-active?)
      (case type
        :firefox (set-driver! {:browser :firefox
                               :profile (doto (ff/new-profile)
                                          (ff/enable-extension "test/extensions/firebug-1.10.0a8.xpi")
                                          (ff/set-preferences {:extensions.firebug.showFirstRunPage false}))})
        :chrome (do
                  (set-driver!
                   (init-driver {:webdriver (let [service (-> (new ChromeDriverService$Builder)
                                                              (.usingChromeDriverExecutable (new java.io.File "/usr/bin/chromedriver"))
                                                              (.usingAnyFreePort)
                                                              (.withEnvironment (if xvfb {"DISPLAY" ":99"} {}))
                                                              (.build))]
                                              (.start service)
                                              (new ChromeDriver service))}))
                  ;; (sh "test/resources/maximize-chrome.sh")
                  )
        :htmlunit (set-driver!
                   (init-driver {:webdriver (doto (new HtmlUnitDriver BrowserVersion/INTERNET_EXPLORER_9)
                                              (.setJavascriptEnabled true))}))
        :phantomjs (set-driver! (init-driver
                                 {:webdriver (PhantomJSDriver. (DesiredCapabilities.))}))))))

(defonce server (atom nil))

(defn start-server[]
  (when-not @server
    (alter-var-root #'clojure.core/*out* (constantly (new java.io.PrintWriter *out*)))
    (noir.server/load-views "src/gd/views/")
    (reset! server (try
                     (noir.server/start (parse-int (property "server.port" "8080"))
                                        {:mode :dev
                                         :ns 'gd
                                         :session-store (session-store-extended)
                                         :session-cookie-attrs {:max-age (* 30 24 60 60)}})
                     (catch java.net.BindException e
                       true)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Custom actions
(defn move-to-element[selector]
  (core/->actions
      *driver*
    (core/move-to-element (first (jquery-composite-finder selector)))))

(defmacro do-until-element-really-present
  "Will correct 'Element is no longer attached to the DOM' exception. Execute code until success.

Example where we should use it:
  Reason : when we clicked to first level menu, second level menu is
  cleared, and after that - created as new element. So it is possible race condition,
  when previous occurense of 'second-level' present, but at the moment of click it is
  not present in DOM. For code see : root-layout-init.js:194 in wiSLA repository.

  wQuery('#box .head').empty(); <<<<<< here is error tiggered <<<<<<<
  wQuery('#box .head').append(subMenuContent); "
  [& action]
  `(loop []
     (if (try
           (do
             (do ~@action)
             false)
           (catch org.openqa.selenium.ElementNotVisibleException e#
             true)
           (catch org.openqa.selenium.StaleElementReferenceException e#
             true))
       (recur))))

(defn wait-until-element-present[selector]
  (do-until-element-really-present
   (wait-until #(and (:webelement (element selector)) (present? selector) (visible? selector)) timeout)))

(defn do-when-visible[action selector]
  (wait-until-element-present selector)
  (apply action [selector]))

(defn blur[]
  (click "body"))

(defn window-focus[]
  (switch-to-window 0))

(defn contains[text & [pre]]
  (format "%s :contains('%s'):visible:last" (or pre "") text))

(defn screen[]
  (let [file (new java.io.File (str "/tmp/webTestScreenshot.png"))]
    (info "Taken screenshot at" (.getAbsolutePath file))
    (take-screenshot :file (.getAbsolutePath file))))

(defn scroll-into-view[selector]
  (location-once-visible selector))

(defn click-text [text]
  (click (contains text)))

(defn select-text [text]
  (select (contains text)))

(defn wait-text [text]
  (wait-until-element-present (contains text)))

(defn set-text [selector value]
  (clear selector)
  (input-text selector value))

(defn to-page[host link]
  (to (str host (property "server.port" "8080") link)))

(defn by-name [value & {:keys [selector] :or {selector "="}}]
  (str "[name" selector "'" (name value) "']"))

(defn set-checkbox [q value]
  (if (not= (selected? q) value)
    (click q)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; If element in current view
(defn element-in-view[selector]
  (let [body-scroll (execute-script "
var a = $('body').scrollTop(), b = $('html').scrollTop();
if (a == 0)
return b;
return a;")
        window-height (execute-script "return window.innerHeight")]
    (when (element selector)
      (let[{:keys [y]} (location selector)]
        (and (> y body-scroll) (< y (+ body-scroll window-height)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Wait URL change
(defmacro wait-url-change[& body]
  `(let [old-url# (current-url)]
     (do ~@body)
     (wait-until #(not= (current-url) old-url#) timeout)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Take screenshot on failure

(defn browser-active?[]
  (try (boolean (current-url))
       (catch org.openqa.selenium.WebDriverException e
         (re-find #"Session ID may not be null" (.getMessage e)))
       (catch java.lang.IllegalArgumentException e
         false)))

(defonce screen-on-failure
  (let [original-report (get-method report :fail)]
    (defmethod report :fail [m]
      (do
        (when (browser-active?)
          (screen))
        (original-report m)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Wait AJAX handler

(defn in-progress-ajax-count[]
  (execute-script (js (return jQuery.active))))

(defn completed-ajax-count[]
  (execute-script (js (return window.completedAjaxCount))))

(defn completed-services-map[]
  (let [count (execute-script (js (return window.completedServicesCount)))]
    (mreduce (fn[entry] {(.getKey entry) (.getValue entry)}) count)))

(defn init-ajax-counter[]
  (execute-script
   (js (if (= (typeof window.completedAjaxCount) "undefined")
         (do (set! window.completedAjaxCount 0)
             (set! window.completedServicesCount {})
             (.ajaxComplete ($ "body")
                            (fn[event xhr settings]
                              (var data (.split settings.data #"[=&]"))
                              (var service (aget (.splice data (+ 1 (.indexOf data "service"))) 0))
                              (var previousCount (aget window.completedServicesCount service))
                              (if previousCount
                                (set! (aget window.completedServicesCount service) (+ previousCount 1))
                                (set! (aget window.completedServicesCount service) 1))
                              (++ window.completedAjaxCount))))))))

(defmacro wait-ajax
  "Wraps body in special waiting construct, that will wait until every ajax call, started
by body will complete.

Example : 'on select' event we send rerender request, and must wait for completion, then
we will wrap this block in wait-ajax"
  [& body]
  `(do
     (init-ajax-counter)
     (let [current-count# (completed-ajax-count)]
       (do ~@body)
       (wait-until #(and (= (in-progress-ajax-count) 0)
                         (> (completed-ajax-count) current-count#))
                   timeout))))

(defn wait-for-no-ajax[]
  (wait-until #(= (in-progress-ajax-count) 0) timeout))
