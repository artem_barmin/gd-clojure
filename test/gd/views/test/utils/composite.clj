(ns gd.views.test.utils.composite
  (:use [clj-webdriver.taxi :exclude [exists?]])
  (:use clj-webdriver.element)
  (:require [clojure.string :as strings]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; jQuery extensions
(def jquery-text-equals
  "if (typeof $.expr[':'].textEquals != 'function')
   {
 $.expr[':'].textEquals = function(a, i, m) {
                                             return $(a).text().match('^' + m[3] + '$') != null;
                                             };}")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils and helpers
(defn from-script[format-str & args]
  (execute-script jquery-text-equals)
  (init-elements (execute-script (apply format format-str args))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Composite jquery + dom locator, with dynamic narrowing of search area By
;; default : string - regarded as jquery selector, {:dom "script"} - as dom
;; selector by using 'with-parent' function we can build chain of the
;; selectors(implemented as simple list). Main entry call is
;; 'jquery-composite-finder' - it is responsible for processing chain of
;; selectors into one big selector.

(defn type-of-selector[elt]
  (if (:dom elt) :dom :jq))
(defmulti compose (fn[x y][(type-of-selector x) (type-of-selector y)]))

(defn dom[script] {:dom script})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Composition description
;;
;;  Extension of base location strategy : allows child and descendant searching with support of Dom and Jq native locators
;;  at any part.
;;
;;  Requirements:
;;  1. Every locator can be build from two types of locators : jQuery(for simple cases) and DOM(for complex cases)
;;  2. We must have possibility of composition of this type of locators in robust way(any combinations).
;;
;;  Idea: any result can be expressed as DOM locator(==arbitrary javascript function), but for some cases - for simplicity
;;  and readability we should use native jquery locators. We can write all possible combinations and think, what type
;;  of result should be:
;;  1. jQuery + jQuery = jQuery(native composition mechanisms - getDescendant)
;;  2. jQuery + DOM = DOM
;;  3. DOM + jQuery = DOM
;;  4. DOM + DOM = DOM
;;
;;  So we have to process 4 cases to build arbitrary combinations of locators. Child relation are left associative, so:
;;  a.child(b.child(c)) can be read as (a.child(b)).child(c), this approach allow us to use chains of jQuery locators at the
;;  beginning, that reduce amount of code.
;;
;;  Composition pattern(pattern - not pattern from GOF) :
;;  1. for jq + jq - this is simple getDescendant
;;  2. for all other cases used following pattern:
;;
;;
;;  jQuery(function()
;;  {
;;  var parent = 'left side of composition';
;;  var result = (function(parent)
;;  {
;;  'right side of composition'
;;  })(parent);
;;  return result;
;;  })()
;;
;;  Plain text description:
;;  1. We take left side of composition and assign it to variable 'parent'
;;  2. Then we build anonymous function for right side of composition(to avoid variables clashes), and send it parameter 'parent'
;;  3. Then we wrap everything in another one anonymous function(also for avoiding clashes)
;;  4. Then we wrap result into jQuery function - to return fancy jQuery wrapped object with a lot of useful methods
;;
;;  Algorithm is based on inductive type of building:
;;  1. We have base of induction
;;  - for jquery - simple return
;;  - for dom - add 'document' object as parent
;;  2. We have step of induction with assumption
;;  that we have parent.
;;  - here we process each of 4 cases of composition
;;
;;  Top level:
;;  1. On the most top level call we unwrap jQuery object into plain DOM object(this is requirement of Selenium environment). So we just do '.get(0)' on the result,
;;  and that's all.
;;
;;  Limitations:
;;  1. DOM locators should use variable 'parent' as base point of processing(not document, of jQuery)

(defmethod compose [:dom :jq] [domI jqI]
  (dom (format "jQuery(function(){var parent = (%s);
var result = (function(parent){return parent.find(\"%s\");})(parent);
return result}())" (:dom domI) jqI)))

(defmethod compose [:jq :dom] [jqI domI]
  (dom (format "jQuery(function(){var parent = jQuery(\"%s\");
var result = (function(parent){return %s})(parent);
return result;}())" jqI (:dom domI))))

(defmethod compose [:dom :dom] [dom1 dom2]
  (dom (format "jQuery(function(){var parent = (%s);
var result = (function(parent){return %s})(parent);
return result}())" (:dom dom1) (:dom dom2))))

(defmethod compose [:jq :jq] [jq1 jq2]
  (str jq1 " " jq2))

(defn compose-top-level[script]
  (case (type-of-selector script)
    :jq script
    :dom {:dom (format "(function(parent){return %s})(jQuery(document)).toArray()" (:dom script))}))

(defn compose-all[elt]
  (compose-top-level (reduce compose elt)))

(defn add-parent[parent elt]
  [elt parent])

(def ^{:dynamic true} *print-selector* nil)
(def last-printed-selector (atom nil))
(def last-printed-count (atom 0))

(defn composite[locators-chain]
  (let [filtered-locators (remove nil? locators-chain)
        composed (compose-all filtered-locators)]
    (when *print-selector*
      (if (not= @last-printed-selector composed)
        (reset! last-printed-count 0))
      (cond (and (= @last-printed-selector composed)
                 (= (mod @last-printed-count 100) 0))
            (prn composed @last-printed-count)

            (= @last-printed-count 0)
            (prn composed))
      (reset! last-printed-selector composed)
      (swap! last-printed-count inc))
    (case (type-of-selector composed)
      :jq (from-script "return $(\"%s\").toArray()" (strings/replace composed "\"" "\\\""))
      :dom (from-script "return %s" (:dom composed)) )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; High level interface
(def ^{:dynamic true} *composite-parent* nil)

(defn jquery-composite-finder
  ([q] (jquery-composite-finder *driver* q))
  ([driver q]
     (if (element? q)
       q
       (composite (flatten (concat *composite-parent* (if (vector? q) q [q])))))))

(defmacro print-selectors[& body]
  `(binding [*print-selector* true]
     (do ~@body)))

(defmacro section[selector & other]
  `(binding [*composite-parent* (cons ~selector *composite-parent*)]
    (do ~@other)))

(defmacro global[& other]
  `(binding [*composite-parent* nil]
     (do ~@other)))

;; Set as default finder
(set-finder! jquery-composite-finder)
