(ns gd.model.deal_update_test
  (:refer-clojure :exclude [extend])
  (:use gd.model.test.data_builders)
  (:use gd.utils.logger)
  (:use [clojure.algo.generic.functor :only (fmap)])

  (:use clojure.test)
  (:use faker.company)
  (:use clj-time.core)

  (:use clojure.data)

  (:use gd.model.model)
  (:use gd.parsers.llapi)

  (:use korma.core)

  (:use gd.utils.common)
  (:use gd.utils.db)
  (:use gd.utils.test)
  (:use gd.utils.web)

  (:require
   [clj-time.coerce :as tm]
   [gd.utils.security :as secur]))

(def default-params {:color [:red :green :yellow]
                     :size [:s :m :l]
                     [:collection false] ["Лето"]})

(defspec simple-stock-update

  (testing "простое обновление товара(имя, описания)"
    (let [deal (make:deal :min_sum)
          [stock1] (stocks deal)
          new-params {:name "a" :description "b" :params default-params}]
      (wrap-deal-security deal
        (stock:update-stock (:id stock1) new-params))
      (let [[stock1-updated] (stocks deal)
            ignore [:name :description]]
        (is (= (apply dissoc stock1-updated :stock-variant ignore)
               (apply dissoc stock1 :stock-variant ignore)))
        (is (= (dissoc new-params :params) (select-keys stock1-updated ignore)))
        (is (= (:params new-params) default-params)))))

  (testing "слияние двух товаров(для фикса багов при парсинге СП) - все заказы
должны переехать на новый товар")

  (testing "слияние товаров с изменением параметров в одном из товаров -
  отрабатывает как удаление параметров"))

(defspec test-update
  (let [deal (make:deal :min_sum)
        [stock1] (stocks deal)
        new-params {:name "a" :description "b" :params default-params}]
    (wrap-deal-security deal
      (stock:update-stock (:id stock1) new-params))
    (let [[stock1-updated] (stocks deal)
          ignore [:name :description]]
      (is (= (apply dissoc stock1-updated :stock-variant ignore)
             (apply dissoc stock1 :stock-variant ignore)))
      (is (= (select-keys new-params ignore) (select-keys stock1-updated ignore))))))

(defspec security-on-update
  (testing "проверка прав доступа для обновления товара"))

(defspec stock-price-update
  (let [owner (make:user)

        check-with-status-that-fix-price
        (fn[status]
          (let [deal (make:deal :min_sum :owner owner)
                [stock1 stock2] (stocks deal)
                order (make:deal-order deal)]
            (testing "изменение цены - все заказы в статусе оплата должны сохранить
  цену(на этой стадии управление ценой идет явно, из управления заказом,
  подразумевается что мы все согласовали)"

              (set-deal-order-status order status)

              ;; ensure that sum have expected value
              (is (= 120 (:sum (order-item-for-deal-order order owner 0))))

              ;; update stocks
              (wrap-deal-security deal
                (stock:update-stock (:id stock1) {:price 500})
                (stock:update-stock (:id stock2) {:price 600}))

              ;; check that sum of the user order is not changed, also for each item price
              ;; remains the same
              (is (= 120 (:sum (order-item-for-deal-order order owner 0))))

              ;; check that stocks in the list have old price(usefull for order rendering)
              (is (= 10 (:price (:stock (order-item-for-deal-order order owner 0)))))
              (is (= 20 (:price (:stock (order-item-for-deal-order order owner 1)))))

              ;; check that real price is changes
              (is (= (:price (first (stocks deal))) 500))
              (is (= (:price (second (stocks deal))) 600)))))]

    (wrap-security owner
      (let [deal (make:deal :min_sum :owner owner)
            [stock1 stock2] (stocks deal)
            order (make:deal-order deal)]
        (testing "изменение цены + заказы в статусе сбор заказов(нотификация
  пользователю, и изменение цены товаров и суммы в заказе)"
          (When (stock:update-stock (:id stock1) {:price 500 :params default-params}))
          (is (= 6000 (:sum (order-item-for-deal-order order owner 0)))))))

    (wrap-security owner
      (let [deal (make:deal :min_sum :owner owner)
            [stock1 stock2] (stocks deal)
            order (make:deal-order deal)]
        (testing "убираем все параметры товара - все связанные заказы должны
        отмениться"
          (When (stock:update-stock (:id stock1) {:price 500 :params {}}))
          (is (= 12 (:closed (order-item-for-deal-order order owner 0))))
          (is (= 0 (:sum (order-item-for-deal-order order owner 0)))))))

    (wrap-security owner
      (testing "изменение цены + любой другой статус(ничего, цена не меняется,
  нотификаций нет)"
        (check-with-status-that-fix-price :aggreement-with-supplier)
        (check-with-status-that-fix-price :order-payment)
        (check-with-status-that-fix-price :waiting-for-delivery)
        (check-with-status-that-fix-price :distribution)))))

(defspec stock-params-update
  (let [owner (make:user)
        user2 (make:user)]
    (wrap-security owner
      (testing "изменение параметров + заказы в статусе сбор заказов(нотификация
    + закрытие части заказа)"
        (let [deal (make:deal :min_sum :owner owner)
              second-order (wrap-security user2 (make:deal-order deal))
              _ (set-deal-order-status second-order :aggreement-with-supplier)
              order (make:deal-order deal)
              [stock1] (stocks deal)]

          (wrap-deal-security deal
            (stock:update-stock (:id stock1) {:params {:color [:red]}}))

          ;; order with orders-gathering is modified
          (is (= (:closed (order-item-for-deal-order order owner 0)) 12))
          ;; order in status : orders-gathering is untouched
          (is (= (:closed (order-item-for-deal-order second-order user2 0)) 0))

          ;; check that green color is disabled
          (let [disabled
                (->> (stocks deal)
                     first
                     :stock-parameter-value
                     (filter #(not (:enabled %)))
                     first)]
            (is (= "green" (:value disabled)))
            (is (= "color" (:name disabled))))))

      (testing "изменение параметров + другие статусы(ничего, т.к. параметры
      управляются явно)"
        (let [deal (make:deal :min_sum :owner owner)
              order (make:deal-order deal)
              [stock1] (stocks deal)]
          (wrap-deal-security deal
            (set-deal-order-status order :aggreement-with-supplier)
            (stock:update-stock (:id stock1) {:params {:color [:red]}}))
          (is (= (:closed (order-item-for-deal-order order owner 0)) 0))))

      (testing "параметры должны удаляться только для конкретного товара, другие
не должны изменяться, и должны оставаться доступными"
        (let [deal (make:deal :min_sum :owner owner)
              order (make:deal-order deal)
              [stock1] (stocks deal)]
          (wrap-deal-security deal
            (stock:update-stock (:id stock1) {:params {:color [:red]}}))
          (let [colors
                (get
                 (->> (stocks deal)
                      second
                      :stock-parameter-value
                      (group-by :name))
                 "color")]
            (is (every? :enabled colors))
            (is (= (map :value colors) ["red" "green" "yellow"]))))))))

(defspec stock-params-creation-test
  (let [owner (make:user)]
    (wrap-security owner
      (testing "добавление новых значений параметров в товар"
        (let [deal (make:deal :min_sum :owner owner)
              [stock1] (stocks deal)]
          (When (stock:update-stock (:id stock1) {:params {:color [:hello-world]}}))
          (let [colors
                (get
                 (->> (stocks deal)
                      first
                      :stock-parameter-value
                      (group-by :name))
                 "color")]
            (is (= (set (map :value colors))
                   (set ["hello-world" "red" "green" "yellow"]))))))

      (testing "добавление уже существующих значений в товар"
        (let [deal (make:deal :min_sum :owner owner)
              [stock1] (stocks deal)]
          (When (stock:update-stock (:id stock1) {:params {:color [:red]}}))
          (let [colors
                (get
                 (->> (stocks deal)
                      first
                      :stock-parameter-value
                      (group-by :name))
                 "color")]
            (is (= (map :value colors) ["red" "green" "yellow"])))))

      (testing "добавление новых параметров в товар"
        (let [deal (make:deal :min_sum :owner owner)
              [stock1] (stocks deal)]

          (testing "обновление товара с новыми параметрами"
            (When (stock:update-stock (:id stock1) {:params {:weight [:10 :20]}}))
            (let [weights
                  (get
                   (->> (stocks deal)
                        first
                        :stock-parameter-value
                        (group-by :name))
                   "weight")]
              (is (= (map :value weights) ["20" "10"]))))

          (testing "заказ товара с новыми парметрами"
            (let [order (make:user-order deal {:size :s :quantity 1 :color :red :weight :10})
                  item (map :value (:stock-parameter-value (order-item order 0)))]
              (is (= (set item) (set ["s" "red" "10"])))))

          (testing "нельзя заказать товар, у которого не хватает параметров для
      полного набора"
            (is
             (thrown-with-msg? Exception #"Not all stock parameters are choosed for order"
               (make:user-order deal {:size :s :quantity 1 :color :red})))))))))

(defspec stock-params-deletion-test
  (let [owner (make:user)]
    (wrap-security owner
      (testing "удаление параметров из товара, параметры не перечисленные в мапе должны помечаться
как недоступные к выбору"
        (let [deal (make:deal :min_sum :owner owner)
              order (make:deal-order deal)
              [stock1] (stocks deal)]
          (When (stock:update-stock (:id stock1) {:params {:color [:red]}}))
          (let [params (->> (stocks deal)
                            first
                            :stock-parameter-value
                            (group-by :name))

                sizes (get params "size")
                colors (get params "color")]
            (is (= (map :enabled colors) [true false false])) ;red is enabled, all other colors
                                        ;are disabled
            (is (every? (comp not :enabled) sizes))))))))

(defspec stock-params-restoring-test
  (let [owner (make:user)]
    (wrap-security owner
      (testing "удаление параметров из товара, а потом добавление параметров с тем же именем -
они должны восстановиться"
        (let [deal (make:deal :min_sum :owner owner)
              order (make:deal-order deal)
              [stock1] (stocks deal)]
          (When (stock:update-stock (:id stock1) {:params {:color [:red]}})
                (stock:update-stock (:id stock1) {:params {:color [:red :yellow :green]}}))
          (let [params (->> (stocks deal)
                            first
                            :stock-parameter-value
                            (group-by :name))
                colors (get params "color")]
            (is (every? :enabled colors))))))))

(defspec stock-params-visibility-change-test
  (testing "обновление видимости(в мапе - все параметры)"
    (let [deal (make:deal :min_sum)
          [stock1] (stocks deal)
          new-params {:name "a" :description "b"
                      :params {:color [:red :green :yellow :magenta]
                               :size [:s :m :l]
                               :collection ["Лето"]
                               [:invisible false] [:1]}}]
      (wrap-deal-security deal (stock:update-stock (:id stock1) new-params))
      (let [params
            (->> (stocks deal)
                 first
                 :stock-parameter-value
                 (group-by (juxt :name :visible)))

            collection
            (get params ["collection" true])

            invisible
            (get params ["invisible" false])]
        (is collection)
        (is invisible))))

  (testing "обновление параметров - видимость неуказанных параметров не должна
  измениться"
    (let [deal (make:deal :min_sum)
          [stock1] (stocks deal)
          new-params {:name "a" :description "b"
                      :params {:color [:red :green :yellow :magenta]}}]
      (wrap-deal-security deal (stock:update-stock (:id stock1) new-params))
      (let [params
            (->> (stocks deal)
                 first
                 :stock-parameter-value
                 (group-by (juxt :name :visible)))

            collection
            (get params ["collection" false])]
        (is collection)))))

(defspec stock-updating-test
  (let [owner (make:user)
        deal (make:deal :min_sum
                        :owner owner
                        :stock-num 2
                        :stock (fn[num] {:name (str 10 num) :price (* 10 num)}))]

    (testing "Test simple update"
      (let [{:keys [id] :as before} (nstock deal 0)]
        (wrap-deal-security deal
          (stock:update-stock id {:name "a"
                                  :price 20
                                  :params default-params}))
        (let [after (nstock deal 0)]
          (is (= "a" (:name after)))
          (is (= 20 (:price after)))
          (is (= (dissoc after :name :price :stock-variant)
                 (dissoc before :name :price :stock-variant))))))

    (testing "Test description update"
      (let [{:keys [id] :as before} (nstock deal 1)
            short-dsc {:Ключ3 "значение3"}
            dsc {:Ключ1 "значение1" :Ключ2 "значение2"}]
        (wrap-deal-security deal
          (stock:update-stock id {:description dsc
                                  :params default-params}))
        (let [after (nstock deal 1)]
          (is (= dsc (:description after)))
          (is (= (dissoc after :description :stock-variant)
                 (dissoc before :description :stock-variant))))))

    (testing "Security check")))

(defspec stock-real-update-test
  (let [deal (make:deal :min_sum)
        {:keys [id]} (nstock deal 0)
        params {[:collection false] (hash-set "Лето")
                [:size true] (hash-set "l" "m" "s")
                [:color true] (hash-set "yellow" "green" "red")}]

    (testing "Test update with real params"
      (wrap-deal-security deal
        (stock:update-stock id {:description "Dolorem omnis error" :id 66
                                :service "update-stock-info" :name "stock1"
                                :params params}))
      (is (= (->> (nstock deal 0)
                  :stock-parameter-value
                  (group-by (juxt (comp keyword :name) :visible))
                  (fmap (comp set (partial map :value))))
             params)))))

(defspec stock-update-showcase
  (let [deal (make:deal :min_sum)
        {:keys [id images]} (nstock deal 0)
        original-image-code (first images)
        get-images #(:images (nstock deal 0))
        new-image-code (images:save-to-code {:path (use-image :items (rnd 1 5))} stock-image-sizes)
        new-image-code2 (images:save-to-code {:path (use-image :items (rnd 1 5))} stock-image-sizes)]
    (wrap-deal-security deal
      (testing "Adding image"
        (stock:update-stock id {:params default-params
                                :images [new-image-code2 original-image-code new-image-code]})
        (is (= (get-images) [new-image-code2 original-image-code new-image-code])))

      (testing "Delete image"
        (stock:update-stock id {:params default-params
                                :images [original-image-code]})
        (is (= (get-images) [original-image-code]))))))

(defspec stock-update-category-change
  (let [deal (make:deal :min_sum)
        {:keys [id]} (nstock deal 0)
        old-category (:fkey_category (nstock deal 1))
        new-category (make:category)]

    (wrap-deal-security deal
      (testing "Update category of stock"
        (When (stock:update-stock id {:fkey_category (:id new-category)}))
        (is (= (:id new-category) (:id (:category (nstock deal 0)))))
        (is (= old-category (:id (:category (nstock deal 1)))))
        (is (every? :enabled (:stock-parameter-value (nstock deal 1))))))))

(defspec stock-save-new-with-index-check

  (let [deal (make:deal :min_sum :name "deal to search")
        nstocks (count (stock:user-stocks 9999 0 {:deal deal}))
        name "my new stock50"]

    (wrap-deal-security deal
      (testing "Save new stock"
        (stock:save-stock-general (make:stock {:name name}) deal)
        (is (= (inc nstocks) (count (stock:user-stocks 9999 0 {:deal deal}))))
        (is (= name (:name (first (search:find-stock name)))))
        (is (some #{name} (map :name (search:find-stock "deal to search"))))))))

(defspec stock-update-with-index-check

  (let [deal (make:deal :min_sum :name "deal to search")
        category (make:category)
        name "my new stock1"
        name2 "my new stock2"]

    (wrap-deal-security deal
      (testing "Update with name stock"
        (stock:update-stock (:id (nstock deal 0)) {:name name})
        (is (some #{name} (map :name (search:find-stock "deal to search")))))

      (testing "Update with category stock"
        (stock:update-stock (:id (nstock deal 1)) {:name name2 :fkey_category (:id category)})
        (is (some #{name2} (map :name (search:find-stock "deal to search"))))))))

(defspec stock-visibility-change
  (testing "check that all stocks have :visible status, and all stock are visible"
    (let [deal (make:deal :min_sum :stock (constantly {:status :visible}) :stock-num 5)]
      (is (every? #(= (:status %) :visible) (stocks deal)))
      (is (= 5 (count (stocks deal))))))

  (testing "check list - :invisible stocks should not be present"
    (let [deal (make:deal :min_sum :stock (constantly {:status :invisible}) :stock-num 5)]
      (is (= 0 (count (stocks deal))))
      (is (= 5 (count (stocks deal :show-invisible true))))
      (is (every? #(= (:status %) :invisible) (stocks deal :show-invisible true)))))

  (testing "create new stock"
    (let [deal (make:deal :min_sum :stock (constantly {:status :invisible}) :stock-num 5)]
      (wrap-deal-security deal
        (stock:save-stock-general (make:stock {:status :visible}) deal))
      (is (= 1 (count (stocks deal))))
      (is (= 6 (count (stocks deal :show-invisible true))))))

  (testing "update stock"
    (let [deal (make:deal :min_sum :stock (constantly {:status :visible}) :stock-num 5)
          stock (nstock deal 0)]
      (wrap-deal-security deal
        (testing "set stock to invisible"
          (is (= 5 (count (stocks deal))))
          (stock:update-stock (:id stock) (make:stock {:status :invisible}))
          (is (= 4 (count (stocks deal))))
          (is (= 5 (count (stocks deal :show-invisible true)))))

        (testing "set stock back to visible"
          (stock:update-stock (:id stock) (make:stock {:status :visible}))
          (is (= 5 (count (stocks deal))))
          (is (= 5 (count (stocks deal :show-invisible true)))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Deal update test
(defn create-stock[category name price & image-numbers]
  {:name name
   :price price
   :stock-variant nil
   :params {}
   :fkey_category category
   :images (map (fn[num]{:path (use-image :items num)}) image-numbers)})

(defn create-deal[parser-name & stocks]
  (let [deal
        (make:deal :min_sum
                   :stock (fn[i] (nth stocks (dec i)))
                   :stock-num (count stocks))]
    (wrap-deal-security deal
      (group-deal:update {:id deal :parser-info {:name parser-name}}))
    deal))

(defn stock-by-condition[fn deal]
  (first (filter fn (stocks deal))))

(defn get-changes[changeset-id]
  (map #(-> % (dissoc :id :fkey_group-deal-update :stock) (dissoc-in [:changeset :images :new]))
       (changes:get-all-deal-changes-log changeset-id :filters [:add :delete :change :question])))

(defspec deal-update-addition
  (testing "check addition"
    (let [cat (:id (category:get-by-key "other"))
          deal (create-deal "test-parser" (create-stock cat "Dress 4312" 100 1 2))]
      (wrap-deal-security deal
        (group-deal:update-stocks deal [(create-stock cat "Really new Dress" 150 3 4)
                                        (create-stock cat "Dress 4312" 100 1 2)]))
      (testing "only one old stock should be visible"
        (is (= [(stock-by-condition #(= (:name %) "Dress 4312") deal)] (stocks deal))))
      (mark-all-stocks-as-visible deal)
      (let [[change :as all-changes] (changes:get-all-deal-changes deal)
            new-stock-id (:id (stock-by-condition #(= (:name %) "Really new Dress") deal))]
        (is (= (count all-changes) 1))
        (is (= ((juxt :parser-name :fkey_group-deal) change) ["test-parser" deal]))
        (is (= (get-changes (:id change))
               (list {:changeset {:fkey_category {:new cat :old nil}
                                  :name {:new "Really new Dress" :old nil}
                                  :params {:new {} :old nil}
                                  :price {:new 150 :old nil}
                                  :stock-variant {:new nil :old nil}
                                  :images {:old nil}}
                      :changed-param-names ["name" "price" "stock-variant" "params" "fkey_category" "images"]
                      :fkey_stock new-stock-id
                      :new-params nil
                      :reasons []
                      :status nil
                      :type :add}))))
      (is (= 2 (count (stocks deal)))))))

(defspec deal-update-delete
  (testing "check deletion"
    (let [cat (:id (make:category))
          deal (create-deal "test-parser"
                            (create-stock cat "platie1" 100 1 2)
                            (create-stock cat "platie2" 150 3 4))]
      (wrap-deal-security deal
        (group-deal:update-stocks deal [(create-stock cat "platie1" 100 1 2 5 6)]))

      (is (= (stocks deal)
             [(stock-by-condition #(= (:name %) "platie1") deal)])))))

(defspec deal-update-modifying
  (testing "by name"
    (let [cat (:id (make:category))
          deal (create-deal "test-parser"
                            (create-stock cat "Dress 4312" 100 1 2)
                            (create-stock cat "Dress 4313" 150 3 4))
          old-images (:images (stock-by-condition #(= (:name %) "Dress 4312") deal))]
      (wrap-deal-security deal
        (group-deal:update-stocks deal [(create-stock cat "Dress 4312" 150 1 2 5)
                                        (create-stock cat "Dress 4313" 150 3 4)]))
      (let [all-changes (changes:get-all-deal-changes deal)]
        (is (= (count all-changes) 1))
        (is (= ((juxt :parser-name :fkey_group-deal) (first all-changes))
               ["test-parser" deal]))
        (is (= (get-changes (:id (first all-changes)))
               (list {:changeset {:price {:new 150 :old 100}
                                  :images {:old old-images}}
                      :fkey_stock (:id (stock-by-condition #(= (:name %) "Dress 4312") deal))
                      :changed-param-names ["price" "images"]
                      :reasons [{:type "name-exact-match" :value 15}
                                {:type "images-more-than-one-common-photo" :value 10}]
                      :new-params nil
                      :status nil
                      :type :change}))))))

  (testing "by images"
    (let [cat (:id (make:category))
          deal (create-deal "test-parser2"
                            (create-stock cat "Dress 4312" 100 1 2)
                            (create-stock cat "Dress 4313" 150 3 4))
          old-images (:images (stock-by-condition #(= (:name %) "Dress 4312") deal))]
      (wrap-deal-security deal
        (group-deal:update-stocks deal [(create-stock cat "Really new Dress" 170 3 4)
                                        (create-stock cat "Dress 4312" 100 1 2 5)]))
      (let [all-changes (changes:get-all-deal-changes deal)]
        (is (= (count all-changes) 1))
        (is (= ((juxt :parser-name :fkey_group-deal) (first all-changes))
               ["test-parser2" deal]))
        (is (= (set (get-changes (:id (first all-changes))))
               (hash-set
                {:changeset
                 {:images {:old old-images}},
                 :changed-param-names ["images"]
                 :fkey_stock (:id (stock-by-condition #(= (:name %) "Dress 4312") deal)),
                 :reasons
                 [{:type "name-exact-match", :value 15}
                  {:type "images-more-than-one-common-photo", :value 10}],
                 :status nil
                 :new-params nil
                 :type :change}
                {:changeset {:name {:new "Really new Dress" :old "Dress 4313"}
                             :price {:new 170 :old 150}}
                 :fkey_stock (:id (stock-by-condition #(= (:name %) "Really new Dress") deal))
                 :status nil
                 :changed-param-names ["name" "price"]
                 :reasons [{:type "images-match" :value 10}]
                 :new-params nil
                 :type :change})))))))

(defspec deal-update-modifying-profiles
  (testing "by name"
    (let [cat (:id (make:category))
          deal (create-deal "test-parser"
                            (create-stock cat "platie" 100 1 2)
                            (create-stock cat "platie" 150 3 4))]
      (wrap-deal-security deal
        (group-deal:update-stocks deal [(create-stock cat "platie" 150 1 2 5)
                                        (create-stock cat "platie" 150 3 4)]))

      ;; TODO : add check that profiles are correctly detected

      )))

(defspec deal-update-modify-parameters
  (let [initial-params
        {:params {"color" ["red" "green" "yellow"]
                  "size" ["s" "m" "l"]}}]

    (testing "same params - no difference"
      (let [cat (:id (make:category))
            deal (create-deal "test-parser"
                              (merge (create-stock cat "Dress 4312" 100 1 2) initial-params))]
        (wrap-deal-security deal
          (group-deal:update-stocks deal [(merge (create-stock cat "Dress 4312" 100 1 2) initial-params)]))
        (let [all-changes (changes:get-all-deal-changes deal)]
          (is (empty? (get-changes (:id (first all-changes))))))))

    (testing "add and delete some params"
      (let [cat (:id (make:category))
            deal (create-deal "test-parser"
                              (merge (create-stock cat "Dress 4312" 100 1 2) initial-params))
            new-params {"color" ["red" "magenta" "yellow"]
                        "size" ["s" "m" "l"]}]
        (wrap-deal-security deal
          (group-deal:update-stocks deal [(merge (create-stock cat "Dress 4312" 100 1 2)
                                                 {:params new-params})]))
        (is (= (:params (nstock deal 0)) (expand-stock-params new-params)))))))

(defspec deal-update-dry-run-mode
  (let [cat (:id (make:category))
        deal (create-deal "test-parser"
                          (create-stock cat "platie" 100 1 2)
                          (create-stock cat "platie" 150 3 4))
        params {:params {[:collection false] [:summer]
                         :size [:xl :l]}}]

    (wrap-deal-security deal
      (group-deal:update-stocks deal [(create-stock cat "platie" 100 1 2)
                                      (create-stock cat "platie" 150 3 4)
                                      (merge (create-stock cat "platie123" 150 5 6) params)]
                                :dry-run true)

      (let [change-id (:id (first (changes:get-all-deal-changes deal)))]
        (is (every? nil? (map :fkey_stock (get-changes change-id))))
        (changes:apply-dry-run-changes change-id)
        (mark-all-stocks-as-visible deal)
        (is (= [(:id (stock-by-condition #(= (:name %) "platie123") deal))]
               (map :fkey_stock (get-changes change-id))))
        (let [params (set (keys (:params (nstock deal 2))))]
          (is (not (params :collection)))
          (is (params :size)))))))

(defspec deal-delete-and-then-add
  (testing "delete stock, and after that add - it should be visible with specified set of params"
    (let [cat (:id (make:category))
          deal (create-deal "test-parser"
                            (create-stock cat "Dress 4312" 100 1 2))]
      (wrap-deal-security deal
        (group-deal:update-stocks deal [])
        (is (empty? (stocks deal)))
        (group-deal:update-stocks deal [(create-stock cat "Dress 4312" 100 1 2)])
        (mark-all-stocks-as-visible deal)
        (is (= (stocks deal) [(stock-by-condition #(= (:name %) "Dress 4312") deal)]))))))

(defspec deal-update-invisible-parameter
  (let [cat (:id (make:category))
        default {:name "stock1"
                 :price 10
                 :stock-variant nil
                 :fkey_category cat
                 :images [{:path (use-image :items 1)}]}
        deal (create-deal "test-parser"
                          (merge default {:params {[:collection false] ["Мужская коллекция->Спортивные костюмы"]}}))]
    (wrap-deal-security deal
      (group-deal:update-stocks deal [(merge default {:params {[:collection false] ["Спортивные костюмы"]}})]
                                :dry-run true)
      (let [change-id (:id (first (changes:get-all-deal-changes deal)))]
        (is (every?
             seq
             (map :new-params (changes:get-all-deal-changes-log change-id :filters [:change]))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Supplier update

(defspec supplier-update-basic
  (let [cat (:id (make:category))
        default {:name "stock1"
                 :price 10
                 :stock-variant nil
                 :fkey_category cat
                 :images [{:path (use-image :items 1)}]
                 :params {:size ["46"]}
                 :params-quantity {:size {"46" 1}}}
        supplier (make:supplier :stock-num 1
                                :stock (constantly default))]

    (wrap-supplier supplier
      (group-deal:update-stocks
       supplier [(merge default {:name "artem" :price 50})]
       :supplier-mode true))

    (is (= [50] (map :price (sup-stocks supplier))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Conflict resolving

(defspec resolve-conflict
  (testing "one stock have exact matches of name and url. other have :contains
  for name

Example:
Комплект кардиган+футболка RA-4056

and
Комплект кардиган+футболка RA-4056/1
Комплект кардиган+футболка RA-4056/2

We should not forbid :contains rule for name match, because some websites can
use it(prom.ua). So we assign more weight for exact match, and choose stock with
maximum weight(but it must be only one with maximum weight, otherwise - we have
multiple exact matches).

Also this stock should be processed as 'new' stock, as it not used in conflict
resolving.
"
    (let [cat (:id (make:category))
          deal (create-deal "test-parser"
                            (create-stock cat "stock" 100 1 2))]
      (wrap-deal-security deal
        (group-deal:update-stocks
         deal [(create-stock cat "stock" 500 1 2)
               (create-stock cat "stock1" 100 3 4)
               (create-stock cat "stock2" 100 3 4)])
        (mark-all-stocks-as-visible deal)
        (is (= [["stock" 500] ["stock1" 100] ["stock2" 100]]
               (map (juxt :name :price) (stocks deal)))))

      (testing "try to update, based on photos"
        (wrap-deal-security deal
          (group-deal:update-stocks
           deal [(create-stock cat "stock" 500 1 2)
                 (create-stock cat "stock" 700 3 4)])
          (is (= [["stock" 500] ["stock" 700] ["stock" 700]]
                 (map (juxt :name :price) (stocks deal)))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Runners

(run-model-tests)
