(ns gd.model.dictionary_test
  (:refer-clojure :exclude [extend])
  (:use [clojure.algo.generic.functor :only (fmap)])
  (:use gd.model.test.data_builders)
  (:use gd.utils.logger)
  (:use gd.utils.web)

  (:use clojure.test)
  (:use faker.company)
  (:use clj-time.core)

  (:use clojure.data)

  (:use gd.model.model)
  (:use gd.model.context)
  (:use gd.parsers.llapi)

  (:use korma.core)

  (:use gd.utils.common)
  (:use gd.utils.db)
  (:use gd.utils.test)

  (:require
   gd.parsers.hlapi
   [clj-time.coerce :as tm]
   [gd.utils.security :as secur]
   [ring.middleware.session.store :as ringstore]
   [monger.ring.session-store :as store]))

(defspec basic-dictionary-filling
  (let [supplier (make:supplier :stock (constantly {:accounting :simple}))]
    (wrap-supplier supplier

      (is (thrown-with-msg?
            Exception #"Parameter color green1 not found"
            (dictionary:update-dictionary
             {:conditions
              {"size" [{:condition {:color "red" :size "s"} :values ["s" "m"]}
                       {:condition {:color "green1" :size "s"} :values ["s" "m"]}]}})))

      (is (thrown-with-msg?
            Exception #"Parameter size s123 not found"
            (dictionary:update-dictionary
             {:conditions
              {"size" [{:condition {:color "red" :size "s"} :values ["s" "m"]}
                       {:condition {:color "green" :size "s"} :values ["s123" "m"]}]}})))

      (is (= {"size" [{:values ["s" "m" "l"] :special "other"}]
              "color" [{:values ["green" "yellow" "red"]  :special "other"}]
              "collection" [{:values ["Лето"] :special "other"}]}
             (:conditions (dictionary:get-dictionary))))

      (dictionary:update-dictionary
       {:conditions {"size" [{:condition {:color "red" :size "s"} :values ["s" "m"]}
                             {:condition {:color "green" :size "s"} :values ["s" "m"]}]}})

      (is (= {"size" [{:values ["l"] :special "other"}
                      {:values ["s" "m" "l"] :special "all"}
                      {:condition {"color" "red" "size" "s"} :values ["s" "m"]}
                      {:condition {"color" "green" "size" "s"} :values ["s" "m"]}]
              "color" [{:values ["green" "yellow" "red"] :special "other"}]
              "collection" [{:values ["Лето"] :special "other"}]}
             (:conditions (dictionary:get-dictionary)))))))

(defspec dictionary-param-rename
  (let [supplier (make:supplier :stock (constantly {:accounting :simple}))]
    (wrap-supplier supplier

      (testing "update during dictionary save"
        (dictionary:update-dictionary
         {:conditions {"size" [{:condition {:color "red-color" :size "s"} :values ["s" "m"]}
                               {:condition {:color "green" :size "s"} :values ["s" "m"]}]}}
         [{:old "red" :value "red-color" :name "color"}])

        (is (= {"size" [{:values ["l"] :special "other"}
                        {:values ["s" "m" "l"] :special "all"}
                        {:condition {"color" "red-color" "size" "s"} :values ["s" "m"]}
                        {:condition {"color" "green" "size" "s"} :values ["s" "m"]}]
                "color" [{:values ["red-color" "green" "yellow"] :special "other"}]
                "collection" [{:values ["Лето"] :special "other"}]}
               (:conditions (dictionary:get-dictionary))))

        (is (= {"color" ["green" "red-color" "yellow"], "size" ["l" "m" "s"], "collection" ["Лето"]}
               (retail:get-stock-params))))

      (testing "update of existing values"
        (dictionary:update-dictionary {} [{:old "red-color" :value "red-old" :name "color"}
                                          {:old "s" :value "s-new" :name "size"}])

        (is (= {"size" [{:values ["s-new" "m" "l"] :special "other"}]
                "color" [{:values ["red-old" "green" "yellow"] :special "other"}]
                "collection" [{:values ["Лето"] :special "other"}]}
               (:conditions (dictionary:get-dictionary))))

        (is (= {"color" ["green" "red-old" "yellow"], "size" ["l" "m" "s-new"], "collection" ["Лето"]}
               (retail:get-stock-params)))

        (testing "check that arrived stocks is correctly working after parameter renaming"
          (let [[{:keys [id]}] (sup-stocks supplier)]
            (arrival:save-multiple-stock-arrivals
             (arrival:create-new-stock-arrival "name" "supplier")
             {id [{:parameters {:size "m"}
                   :price 100
                   :quantity 50}]})))))))

(defspec dictionary-param-rename-same-value-already-exists
  (let [supplier (make:supplier :stock (constantly {:params {:color [:red :green :yellow]
                                                             :size [:s :m :l]}})
                                :stock-num 2)
        user (make:user)
        arrival (make:arrival supplier
                              :parameters [{:size "m" :color "red"}
                                           {:size "s" :color "green"}
                                           {:size "l" :color "yellow"}]
                              :count 5)
        stock1 (:id (first (sup-stocks supplier)))]

    (wrap-supplier supplier

      (wrap-security user
        (retail:make-order [{:fkey_stock stock1
                             :size :m :color :red :quantity 3}]))

      (is (= {"color" ["green" "red" "yellow"], "size" ["l" "m" "s"]}
             (retail:get-stock-params)))

      (is (=
           {{:size "l" :color "yellow"} 5 {:size "s" :color "green"} 5 {:size "m" :color "red"} 2}
           (:left-stocks (retail:get-admin-stock stock1))))

      (testing "red->green"
        (dictionary:update-dictionary {} [{:old "red" :value "green" :name "color"}])

        (testing "check that arrival,order items,availability params is also changed alltogether"
          (is (=
               [{:size "l" :color "yellow"} {:size "s" :color "green"} {:size "m" :color "green"}]
               (map :parameters (get (:stock-arrival-parameter (first (sup-stocks supplier))) arrival))))

          (is (=
               [{:size "m", :color "green"}]
               (map :params (mapcat :user-order-item (retail:active-orders nil nil)))))

          (is (=
               [{:size "m", :color "green"}]
               (map :params (mapcat :user-order-item (retail:active-orders nil nil)))))

          (is (=
               {{:size "l" :color "yellow"} 5 {:size "s" :color "green"} 5 {:size "m" :color "green"} 2}
               (:left-stocks (retail:get-admin-stock stock1))))))

      (testing "s->m"
        (dictionary:update-dictionary {} [{:old "s" :value "m" :name "size"}])

        (is (=
             [[{:size "l", :color "yellow"} 5]
              [{:size "m", :color "green"} 5]
              [{:size "m", :color "green"} 5]]
             (map (juxt :parameters :quantity)
                  (get (:stock-arrival-parameter (first (sup-stocks supplier))) arrival))))

        (is (=
             {{:size "l", :color "yellow"} 5, {:size "m", :color "green"} 7}
             (:left-stocks (retail:get-admin-stock stock1))))))))

(defspec dictionary-add-properties-from-save-stock
         (let [sup (make:supplier)]
           (wrap-supplier sup
                          (testing "adding properties to dictionary from save stock")
                          (stock:save-stock-general (make:stock {:description {:properties [{:name "тест1" :value "неважно"} {:name "тест2" :value "неважно"} {:name "тест3" :value "неважно"}]}}))
                          (is (= ["тест1" "тест2" "тест3"] (:properties (dictionary:get-dictionary)))))))

(defspec dictionary-add-properties-from-update-stock
         (let [sup (make:supplier)]
           (wrap-supplier sup
                          (testing "adding properties to dictionary from update stock"
                          (let [id (stock:save-stock-general (make:stock {:description {:properties [{:name "тест1" :value "неважно"} {:name "тест2" :value "неважно"} {:name "тест3" :value "неважно"}]}}))]
                            (stock:update-stock id                       {:description {:properties [{:name "тест1" :value "неважно"} {:name "тест4" :value "неважно"} {:name "тест5" :value "неважно"}]}})
                            (is (= ["тест1" "тест2" "тест3" "тест4" "тест5"] (:properties (dictionary:get-dictionary)))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Runners

(run-model-tests)
