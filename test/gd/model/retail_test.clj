(ns gd.model.retail_test
  (:refer-clojure :exclude [extend])
  (:use [clojure.algo.generic.functor :only (fmap)])
  (:use gd.model.test.data_builders)
  (:use gd.utils.logger)
  (:use gd.utils.web)

  (:use clojure.test)
  (:use faker.company)
  (:use clj-time.core)

  (:use clojure.data)

  (:use gd.model.model)
  (:use gd.model.context)
  (:use gd.parsers.llapi)

  (:use korma.core)

  (:use gd.utils.common)
  (:use gd.utils.db)
  (:use gd.utils.test)

  (:require
   gd.parsers.hlapi
   [clj-time.coerce :as tm]
   [gd.utils.security :as secur]
   [ring.middleware.session.store :as ringstore]
   [monger.ring.session-store :as store]))

(defspec retail-order-test
  (let [supplier (make:supplier :user {})
        [stock1] (sup-stocks supplier)
        user (make:user)
        order (wrap-supplier supplier
                (wrap-security user
                  (retail:make-order [{:fkey_stock (:id stock1) :size :s :color :green :quantity 12}])))]
    (wrap-supplier supplier
      (wrap-security (:id (first (retail:supplier-users)))
        (is (seq (notify:get order :retail-new-order)))))))

(defspec retail-order-additional-expenses
  (let [supplier (make:supplier :stock (constantly {:price 20}) :stock-num 1)
        [stock1] (sup-stocks supplier)
        _ (make:arrival supplier :parameters [{:size "s" :color "green"}] :count 50)
        user (make:user)
        order (wrap-supplier supplier
                (wrap-security user
                  (retail:make-order [{:fkey_stock (:id stock1) :size :s :color :green :quantity 2}])))]
    (wrap-supplier supplier

      (testing "можно добавлять скидки"
        (testing "assign 20% discount for retail order, sum of the order are
        reduces acoordinly"
          (additional-expense:assign-or-update order :retail-order [{:type :discount :percent 20}])
          (is (= [(Math/round (* 0.8 2 20))] (map :sum (retail:active-orders nil nil))))))

      (let [used-quantity
            (fn[]
              ((:left-stocks (first (retail:admin-supplier-stocks nil nil {})))
               {:color "green" :size "s"}))]
        (testing "возвраты - помечать n товаров как возвращенные, должны
попадать на склад обратно и не учитываться в расчеты суммы заказа"

          (testing "check before return - quantity is -2"
            (is (= 48 (used-quantity))))

          (let [[item1] (map :id (:user-order-item (retail:get-order order)))]
            (additional-expense:assign-or-update order :retail-order [{:type :return-expenses :money 20}])
            (additional-expense:assign-or-update item1 :user-order-item [{:type :return-stocks :items 1}]))

          (testing "check after return - quantity is -1"
            (is (= 49 (used-quantity))))))

      (testing "поле для дополнительных расходов по заказу(доставка, расходы по возврату)"))))

(defspec retail-order-partial-discount
  (let [supplier (make:supplier :stock (fn[i]
                                         {:price 20
                                          :meta-info {:skip-discount (= i 0)}})
                                :stock-num 2)
        [stock1 stock2] (sup-stocks supplier)
        _ (make:arrival supplier :parameters [{:size "s" :color "green"}] :count 50)
        user (make:user)
        order (wrap-supplier supplier
                (wrap-security user
                  (retail:make-order [{:fkey_stock (:id stock1) :size :s :color :green :quantity 2}
                                      {:fkey_stock (:id stock2) :size :s :color :green :quantity 3}])))]
    (wrap-supplier supplier
      (testing "assign 20% discount, but it only applied to stocks that don't
      have :skip-discount set to true"
        (additional-expense:assign-or-update order :retail-order [{:type :discount :percent 20}])
        (is (= [(+ (*  2 20) (Math/round (* 0.8 3 20)))]
               (map :sum (retail:active-orders nil nil))))))))

(defspec retail-order-price-from-arrival
  (testing "продажа товара по себестоимости - изменять продажную цену товара"))

(defspec retail-order-price-fixate
  (let [supplier (make:supplier
                  :stock
                  (constantly {:price 20
                               :context {:wholesale {:price 25 :status :invisible}
                                         :wholesale-sale {:price 30 :status :visible}}}) :stock-num 1)
        user (make:user)
        user2 (make:user)]
    (wrap-supplier supplier
      (wrap-context [:wholesale :wholesale-sale]
        (testing "store orders"
          (wrap-security user
            (retail:make-order
             [{:fkey_stock (:id (first (sup-stocks supplier))) :size :s :color :green :quantity 12}]))
          (wrap-security user2
            (retail:make-order
             [{:fkey_stock (:id (first (sup-stocks supplier))) :size :s :color :green :quantity 12}])))

        (testing "fetch orders"
          (is
           (= [30 30]
              (map (comp :price :stock) (mapcat :user-order-item (retail:active-orders nil nil))))))))))

(defspec retail-order-merge-order-items-into-existing-orders
  (let [supplier (make:supplier)
        user (make:user)
        user2 (make:user)
        [stock1 stock2] (map :id (sup-stocks supplier))]
    (wrap-security user

      (make:arrival supplier :parameters [{:size "s" :color "green"}
                                          {:size "m" :color "green"}]
                    :count 50)

      (wrap-supplier supplier
        (wrap-context [:wholesale :wholesale-sale]
          (testing "store first order"
            (let [order1
                  (retail:make-order
                   [{:fkey_stock stock1 :size :s :color :green :quantity 12}])]

              (testing "try to store new items - the should be added to existing order(but with respect to params)"
                (let [order2
                      (retail:make-order
                       [{:fkey_stock stock1 :size :s :color :green :quantity 14}
                        {:fkey_stock stock1 :size :m :color :green :quantity 14}
                        {:fkey_stock stock2 :size :s :color :green :quantity 13}])]
                  (is (= order1 order2))))

              (is (= 1 (count (retail:active-orders nil nil))))
              (is (= (sort [[stock1 (+ 12 14)] [stock1 14] [stock2 13]])
                     (->>
                      (retail:active-orders nil nil)
                      first
                      :user-order-item
                      (map (juxt :fkey_stock :quantity))
                      sort)))

              (testing "after move to next status new retail order should be created"
                (retail:order-next-status order1)
                (retail:order-next-status order1)
                (let [order3
                      (retail:make-order
                       [{:fkey_stock stock1 :size :s :color :green :quantity 14}])]
                  (is (not= order1 order3)))
                (is (= 2 (count (retail:active-orders nil nil)))))))

          (wrap-security user2
            (testing "orders from other user should not be merged"
              (retail:make-order
               [{:fkey_stock stock1 :size :s :color :green :quantity 12}])
              (is (= 3 (count (retail:active-orders nil nil)))))))))))

(defspec retail-order-merge-order-items-into-deleted-order-items
  (testing "first we are delete some order items, and then make order with the
same stock - they must became visible"
    (let [supplier (make:supplier)
          user (make:user)
          [stock1 stock2] (map :id (sup-stocks supplier))]
      (wrap-security user

        (make:arrival supplier :parameters [{:size "s" :color "green"}] :count 50)

        (wrap-supplier supplier
          (wrap-context [:wholesale :wholesale-sale]
            (testing "store first order"
              (let [order1
                    (retail:make-order
                     [{:fkey_stock stock1 :size :s :color :green :quantity 12}
                      {:fkey_stock stock2 :size :s :color :green :quantity 12}])]

                (testing "delete some items"
                  (retail:delete-order-item (:id (first (retail-order-items order1)))))

                (is (= [[stock2 12]]
                       (map (juxt :fkey_stock :quantity) (retail-order-items order1))))

                (testing "try to store new items - the should be added to
existing order, and order-item should be marked as :visible, but all
deleted :quantity should not be used"
                  (retail:make-order
                   [{:fkey_stock stock1 :size :s :color :green :quantity 14}]))

                (is (= [[stock1 14] [stock2 12]]
                       (map (juxt :fkey_stock :quantity) (retail-order-items order1))))))))))))

(defspec delete-empty-retail-order
  (testing "first we are delete some order items, and then make order with the
same stock - they must became visible"
    (let [supplier (make:supplier)
          user (make:user)
          [stock1 stock2] (map :id (sup-stocks supplier))]
      (wrap-security user
        (wrap-supplier supplier
          (wrap-context [:wholesale :wholesale-sale]
            (let [order1
                  (retail:make-order
                   [{:fkey_stock stock1 :size :s :color :green :quantity 12}])]

              (retail:delete-order-item (:id (first (retail-order-items order1))))

              (is (empty? (retail:active-orders nil nil)))

              (let [order2
                    (retail:make-order
                     [{:fkey_stock stock1 :size :s :color :green :quantity 12}])]
                (is (not= order1 order2))))))))))

(defspec merge-multiple-retail-orders
  (let [supplier (make:supplier)
        user (make:user)
        [stock1 stock2] (map :id (sup-stocks supplier))]
    (wrap-security user
      (wrap-supplier supplier

        (make:arrival supplier :parameters [{:size "s" :color "green"}] :count 10)

        (testing "basic merge of two order for same user"
          (wrap-context [:wholesale :wholesale-sale]
            (let [order1
                  (retail:make-order
                   [{:fkey_stock stock1 :size :s :color :green :quantity 12}])]

              (retail:order-next-status order1)
              (retail:order-next-status order1)

              (let [order2
                    (retail:make-order
                     [{:fkey_stock stock1 :size "s" :color "green" :quantity 12}
                      {:fkey_stock stock2 :size "s" :color "green" :quantity 12}])]
                (is (not= order1 order2))
                (testing "we use context for admin page"
                  (wrap-context [:wholesale :wholesale-sale :retail :retail-sale]
                    (retail:merge-orders [order1 order2])))
                (is (= [order1] (map :id (retail:active-orders nil nil))))
                (is (= [[stock1 {:size "s" :color "green"} 10 14]
                        [stock2 {:size "s" :color "green"} 10 2]]
                       (map (juxt :fkey_stock :params :quantity :closed) (retail-order-items order1))))

                (is (= 0 (get
                          (analytics:get-left-quantity-stocks [stock2])
                          [stock2 (map :id (#'gd.model.model/orders:order-item-to-parameters
                                            {:size :s :color :green :fkey_stock stock2}))])))))))))))

(defspec merge-multiple-retail-orders-with-one-stock-in-sale
  (let [supplier (make:supplier
                  :stock-num 2
                  :stock (fn[i]
                           {:context {:wholesale {:status :visible :price 20}
                                      :wholesale-sale {:status (if (= i 0) :visible :invisible)
                                                       :price 10}}}))
        user (make:user)
        [stock1 stock2] (map :id (sup-stocks supplier))]
    (wrap-security user
      (wrap-supplier supplier

        (make:arrival supplier :parameters [{:size "s" :color "green"}] :count 10)

        (testing "basic merge of two order for same user"
          (wrap-context [:wholesale :wholesale-sale]
            (let [order1
                  (retail:make-order
                   [{:fkey_stock stock1 :size :s :color :green :quantity 12}
                    {:fkey_stock stock2 :size :s :color :green :quantity 12}])]

              (retail:order-next-status order1)
              (retail:order-next-status order1)

              (let [order2
                    (retail:make-order
                     [{:fkey_stock stock1 :size "s" :color "green" :quantity 12}
                      {:fkey_stock stock2 :size "s" :color "green" :quantity 12}])]
                (is (not= order1 order2))
                (testing "we use context for admin page"
                  (wrap-context [:wholesale :wholesale-sale :retail :retail-sale]
                    (retail:merge-orders [order1 order2])))
                (is (= [order1] (map :id (retail:active-orders nil nil))))
                (testing "check also price of stock to ensure that wholesale-sale context is used"
                  (is (= [[stock1 {:size "s" :color "green"} 10 14 10]
                          [stock2 {:size "s" :color "green"} 10 14 20]]
                         (map (juxt :fkey_stock :params :quantity :closed (comp :price :stock))
                              (retail-order-items order1))))
                  (is (= {:note (str "Объединены заказы №" order2)}
                         (:additional-info (retail:get-order order1)))))))))))))

(defspec check-closed-retail-orders
  (let [supplier (make:supplier)
        user (make:user)
        [stock1 stock2] (map :id (sup-stocks supplier))]
    (wrap-security user
      (wrap-supplier supplier

        (make:arrival supplier :parameters [{:size "s" :color "green"}] :count 10)

        (wrap-context [:wholesale :wholesale-sale]
          (testing "create order and check that items are closed"
            (let [order1
                  (retail:make-order
                   [{:fkey_stock stock1 :size :s :color :green :quantity 12}])]

              (is (= [[stock1 {:size "s" :color "green"} 10 2]]
                     (map (juxt :fkey_stock :params :quantity :closed) (retail-order-items order1))))

              (testing "set exactly existing quantity - closed must became 0"
                (retail:change-order
                 (map (fn[m] (merge m {:quantity 10}))(retail-order-items order1)))
                (is (= [[stock1 {:size "s" :color "green"} 10 0]]
                       (map (juxt :fkey_stock :params :quantity :closed) (retail-order-items order1)))))

              (testing "change order and check that closed items are recomputed correctly"
                (retail:change-order
                 (map (fn[m] (merge m {:quantity 11}))(retail-order-items order1)))
                (is (= [[stock1 {:size "s" :color "green"} 10 1]]
                       (map (juxt :fkey_stock :params :quantity :closed) (retail-order-items order1)))))

              (testing "make order from same user - it must be merged into existing, and 'closed' recomputed "
                (retail:make-order
                 [{:fkey_stock stock1 :size :s :color :green :quantity 12}])
                (is (= [[stock1 {:size "s" :color "green"} 10 13]]
                       (map (juxt :fkey_stock :params :quantity :closed) (retail-order-items order1))))))))))))

(defspec multiple-orders-make-check-closed
  (let [supplier (make:supplier)
        user (make:user)
        [stock1 stock2 stock3] (map :id (sup-stocks supplier))]
    (wrap-security user
      (wrap-supplier supplier

        (make:arrival supplier :parameters [{:size "s" :color "green"}] :count 10)

        (wrap-context [:wholesale :wholesale-sale]
          (let [order1
                (retail:make-order
                 [{:fkey_stock stock1 :size :s :color :green :quantity 12}
                  {:fkey_stock stock2 :size :s :color :green :quantity 15}])]

            (is (= [[stock1 {:size "s" :color "green"} 10 2]
                    [stock2 {:size "s" :color "green"} 10 5]]
                   (sort-by first
                            (map (juxt :fkey_stock :params :quantity :closed) (retail-order-items order1)))))

            (retail:make-order
             [{:fkey_stock stock3 :size :s :color :green :quantity 17}])

            (is (= [[stock1 {:size "s" :color "green"} 10 2]
                    [stock2 {:size "s" :color "green"} 10 5]
                    [stock3 {:size "s" :color "green"} 10 7]]
                   (sort-by first
                            (map (juxt :fkey_stock :params :quantity :closed) (retail-order-items order1)))))))))))

(defspec hiding-invisible-params-and-price-recomputing
  (let [supplier (make:supplier
                  :stock-num 1
                  :stock (fn[i]
                           {:price 0
                            :params {:size [:s :m]}
                            :stock-variant {{:size :m} [{:context :wholesale :price 15}
                                                        {:context :retail :price 25}]
                                            {:size :s} 10}}))
        user (make:user)
        [stock1] (map :id (sup-stocks supplier))]
    (wrap-security user
      (wrap-supplier supplier

        (make:arrival supplier :parameters [{:size "m"}] :count 10)

        (testing "retrieve stock in wholesale, price should be computed as
        minimal from available sizes, also params must be consrainted to only
        params that arrived"
          (binding [*supplier-info* {:stocks-mode :available-from-arrival}]
            (wrap-context [:wholesale :wholesale-sale]
              (is (= 15.00M (:price (retail:get-stock stock1))))
              (is (= {"size" ["m"]} (:params (retail:get-stock stock1))))
              (is (= {{:size "m"} {:context :wholesale :price 15.00M} {:size "s"} nil}
                     (stock-variants (:stock-variant (retail:get-stock stock1))))))

            (wrap-context [:retail :retail-sale]
              (is (= 25.00M (:price (retail:get-stock stock1))))
              (is (= {{:size "m"} {:context :retail :price 25.00M} {:size "s"} nil}
                     (stock-variants (:stock-variant (retail:get-stock stock1))))))))))))

(defspec set-retail-price-as-percent-of-wholesale
  (let [supplier (make:supplier
                  :stock-num 1
                  :stock (fn[i]
                           {:price 0
                            :params {:size [:s :m]}
                            :stock-variant {{:size :m} [{:context :wholesale :price 15}]
                                            {:size :s} 10}}))
        user (make:user)
        [stock1] (map :id (sup-stocks supplier))]
    (wrap-security user
      (wrap-supplier supplier

        (wrap-context [:wholesale :wholesale-sale :retail :retail-sale]
          (retail:set-retail-price [stock1] {:max-price 10 :min-price 5 :change-percent 80}))

        (wrap-context [:retail :retail-sale]
          (is (= {{:size "m"} {:price 25.00M :context :retail} ;15 + (15*0.8 => 10) = 25
                  {:size "s"} {:price 18.00M :context :retail} ;10 * (10*0.8) = 18
                  }
                 (stock-variants (:stock-variant (retail:get-stock stock1))))))))))

(defspec order-parameters-check
  (let [order-deal
        (make:supplier
         :stock-num 2
         :stock (constantly {:price 10 :params {:color [:red :green [:yellow 20]] :size [:s]}}))
        [stock1 stock2] (sup-stocks order-deal)
        user (make:user)]

    (testing "Пытаемся сделать новый заказ - указав значения не для всех
    параметров товара - падает с исключением"
      (is
       (thrown-with-msg?
        Exception #"Not all stock parameters are choosed for order"
        (wrap-security user
                       (retail:make-order [{:fkey_stock (:id stock1) :size :l :quantity 12}
                                           {:fkey_stock (:id stock2) :size :l :quantity 7 :color :red}])))))

    (testing "Пытаемся сделать новый заказ - указав некоректные значения для параметра"
      (is
       (thrown-with-msg?
        Exception #"Not all stock parameters are choosed for order"
        (wrap-security user
                       (retail:make-order [{:fkey_stock (:id stock1) :size :s :color :green :quantity 12}
                                           {:fkey_stock (:id stock2) :size :blah :color :red :quantity 7}])))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Runners

(run-model-tests)
