(ns gd.model.notify_test
  (:refer-clojure :exclude [extend])
  (:use gd.views.notifications)
  (:use gd.model.test.data_builders)
  (:use gd.utils.pub_sub)
  (:use gd.utils.logger)
  (:use [clojure.algo.generic.functor :only (fmap)])

  (:use clojure.test)
  (:use faker.company)
  (:use clj-time.core)

  (:use clojure.data)

  (:use gd.model.context)
  (:use gd.model.model)
  (:use gd.parsers.llapi)

  (:use korma.core)

  (:use gd.utils.common)
  (:use gd.utils.web)
  (:use gd.utils.db)
  (:use gd.utils.test)

  (:require
   [clj-time.coerce :as tm]
   [gd.utils.security :as secur])
  (:import (java.util.concurrent Executors CountDownLatch TimeUnit)))

(defspec forum-messages-test
  (let [user1 (make:user)
        user2 (make:user)]

    (forum:save-or-update-tree
     [["Главная тема"
       :desc "Описание главной темы"
       :childs [["Под тема1" :desc "Описание под темы" :first-message "Hello"]
                ["Под тема2" :desc "Описание под темы" :first-message "Hello"]]]]
     {:id user1})

    (forum:add-message (theme-by-name "Под тема2") "Text" {:id user2})

    (wrap-security user1
      (is (seq (notify:get (theme-by-name "Под тема2") :new-forum-message))))))

(defspec use-invite-test
  (invite:initial-generation 2)
  (let [owner (make:user)
        code (wrap-security owner (:code (invite:capture)))
        registered (:id (invite:use-internal code "login" "password" "email@email.email" "phone"))]

    (wrap-security owner
      (let [events (notify:get registered :user-invite-use)]
        (is (= {:user (dissoc (user:get-full owner) :fillness :fillness-improve :fillness-reasons)
                :info {:user registered}
                :fkey_source-user registered
                :fkey_forum-theme nil
                :fkey_site-page nil
                :fkey_retail-order nil
                :fkey_user owner
                :source (select-single user
                                       (with auth-profile)
                                       (where {:id registered}))
                :opened false
                :type :user-invite-use}
               (dissoc (first events) :created :id)))))))

(defspec context-saving-test
  (clear-queue notification-channel 16)
  (let [latch (new java.util.concurrent.CountDownLatch 1)
        supplier (make:supplier)
        [stock1] (sup-stocks supplier)
        user (make:user)]
    (binding [*should-publish* true
              *host-id* 0]
      (with-redefs [host-info
                    (atom [{:supplier supplier}])

                    process-persistent-notifications
                    (fn[data]
                      (when (and (= *current-supplier* supplier)
                                 (seq data)
                                 (contains? data :id)
                                 (noir.options/dev-mode?))
                        (.countDown latch)))]
        (wrap-supplier supplier
          (wrap-security user
            (retail:make-order
             [{:fkey_stock (:id stock1) :size :s :color :green :quantity 12}])))
        (is (.await latch 5 TimeUnit/SECONDS))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Runners

(run-model-tests)
(clear-queue notification-channel 16)
