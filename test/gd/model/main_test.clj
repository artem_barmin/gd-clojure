(ns gd.model.main_test
  (:refer-clojure :exclude [extend])
  (:use gd.model.test.data_builders)
  (:use gd.utils.logger)

  (:use clojure.test)
  (:use faker.company)
  (:use clj-time.core)

  (:use clojure.data)

  (:use gd.model.model)
  (:use gd.parsers.llapi)

  (:use korma.core)

  (:use gd.utils.common)
  (:use gd.utils.db)
  (:use gd.utils.web)
  (:use gd.utils.test)

  (:require
    gd.parsers.hlapi
    [clj-time.coerce :as tm]
    [gd.utils.security :as secur]
    [ring.middleware.session.store :as ringstore]
    [monger.ring.session-store :as store]
    [clojure.java.jdbc :as jdbc])
  (:import (java.util.concurrent Executors CountDownLatch TimeUnit)))

(defspec private-messages-storage-test
  (let [user1 (make:user-whole)
        user2 (make:user-whole)]

    (testing "Storage messages test"
      (let [message
            (message:send
             {:text "Hi, [a]Artem[/a]" :type :private} user1 user2)

            [message-fetched]
            (message:messages user1 :sent :private)]

        ;; check everything except type,user and recipient
        (is
         (= (dissoc message :user :type :recipient)
            (dissoc message-fetched :user :type :recipient)))

        ;; check type
        (is (= (from-message-type (:type message)) :private))

        ;; chech author
        (is (= (:id (:user message-fetched)) (:id user1)))

        ;; check recipient
        (is (= (:id (:recipient message-fetched)) (:id user2)))))))

(defspec private-messages-reply-test
  (let [user1 (make:user-whole)
        user2 (make:user-whole)

        first-message
        (message:send
         {:text "Hi, [a]Artem[/a]" :type :private} user1 user2)

        reply-message
        (message:send
         {:text "Hi, [a]Artem[/a]" :type :private :reply-to (:id first-message)} user2 user1)

        [{:keys [reply-to]} :as messages]
        (message:messages user1 :inbox :private {:fetch-reply true})]

    (is (= (:id reply-to) (:id first-message)))
    (is (= (:id (:user reply-to)) (:id user1)))))

(defspec private-messages-lists-test
  (let [user1 (make:user-whole)
        user2 (make:user-whole)
        send (fn[from to]
               (message:send
                {:text "Hi, [a]Artem[/a]" :type :private} from to))
        count (fn[user type]
                (count (message:messages user type :private)))]

    (testing "1 - sent, 1 - inbox"
      (When (send user1 user2))

      (is (= (count user1 :sent) 1))
      (is (= (count user1 :inbox) 0))

      (is (= (count user2 :sent) 0))
      (is (= (count user2 :inbox) 1)))

    (testing "2 - sent, 2 - inbox"
      (When (send user2 user1))

      (is (= (count user1 :sent) 1))
      (is (= (count user1 :inbox) 1))

      (is (= (count user2 :sent) 1))
      (is (= (count user2 :inbox) 1)))

    (testing "filtering by reply-to condition"
      (let [message (:id (send user1 user2))]
        (When (message:reply-to-private user2 message "Reply"))
        (is (= (clojure.core/count (message:messages user1 :inbox :private {:reply-to [message]}))
               1))))))

(defspec private-messages-escaping-test
  (let [user1 (make:user-whole)
        user2 (make:user-whole)

        message (message:send
                 {:text "Nice <script>to</script> meet you [b]Kostya[/b]"
                  :type :private}
                 user1 user2)]

    (is
     (=
      (:text (first (message:messages user2 :inbox :private)))
      "Nice to meet you <b>Kostya</b>"))))

(defspec private-messages-deletion-test
  (let [user1 (make:user-whole)
        user2 (make:user-whole)
        hacker (make:user)

        {message1 :id} (message:send {:text "1" :type :private} user1 user2)
        {message2 :id} (message:send {:text "1" :type :private} user1 user2)]

    (testing "Delete by hacker"
      (is
       (thrown-with-msg? Exception #"Security violation"
         (wrap-security hacker
           (message:remove message1)))))

    (testing "Remove by author"
      (wrap-security (:id user1)
        (message:remove message1))

      (is (= 1 (count (message:messages user1 :sent :private)))))

    (testing "Remove by recipient"
      (wrap-security (:id user2)
        (message:remove message2))

      (is (= 0 (count (message:messages user1 :sent :private)))))))

(defspec forum-manipulation-api-test
  (let [user (make:user-whole)]

    (testing "Save sample tree"
      (forum:save-or-update-tree
       [["Главная тема"
         :desc "Описание главной темы"
         :childs [["Под тема1" :desc "Описание под темы" :first-message "Hello"]
                  ["Под тема2" :desc "Описание под темы" :first-message "Hello"]]]
        ["Вторая главная тема"
         :childs [["Вторая под тема" :first-message "Hello"]]]]
       user))

    (testing "Check that different layers of forum themes are created correctly"
      (is (= (map :name (forum:top-themes)) ["Главная тема" "Вторая главная тема"]))
      (is (= (map :description (forum:top-themes)) ["Описание главной темы" nil]))
      (is (= (map :name (forum:theme-childs (theme-by-name "Главная тема"))) ["Под тема1" "Под тема2"]))
      (is (= (map :name (forum:theme-childs (theme-by-name "Вторая главная тема"))) ["Вторая под тема"])))

    (testing "Check updating API - theme, annotated with exists must exists!"
      (is (thrown-with-msg?
            Exception #"Theme 'Главная тема123' is not exist"
            (forum:save-or-update-tree
             [["Главная тема123"
               :exists true
               :childs [["New fancy childs"]]]]
             user))))

    (testing "Check that we can't add new theme to editable theme"
      (is (thrown-with-msg?
            Exception #"Can't add theme to editable theme."
            (forum:save-or-update-tree
             [["Главная тема"
               :exists true
               :childs [["Под тема1"
                         :exists true
                         :childs [["New fancy childs"]]]]]]
             user))))

    (testing "Check that we can't add message to non-editable theme"
      (is (thrown-with-msg?
            Exception #"Can't add message to non-editable theme. Possible bug, or atack."
            (forum:add-message
             (theme-by-name "Главная тема")
             "Text"
             (user:check-credentials "abarmin" "123456")))))))

(defspec forum-delete-test
  (let [user (make:user-whole)
        hacker (make:user-whole)
        count #(:messages-count (theme-by-name-whole "Subtheme"))
        count2 #(:messages-count (theme-by-name-whole "Subtheme1"))]
    (forum:save-or-update-tree [["Главная тема"
                                 :childs [["Subtheme" :first-message "Hello"]
                                          ["Subtheme1" :first-message "Hello"]]]] user)

    ;; add message
    (is (= (count) 1))
    (is (= (count2) 1))
    (forum:add-message (theme-by-name "Subtheme") "Text" user)
    (is (= (count) 2))
    (is (= (count2) 1))

    ;; remove message
    (is (thrown-with-msg?
          Exception #"Security violation"
          (wrap-security (:id hacker)
            (forum:remove-message
             (theme-by-name "Subtheme")
             (:id (first (forum:theme-messages (theme-by-name "Subtheme") nil nil)))))))

    (wrap-security (:id user)
      (forum:remove-message
       (theme-by-name "Subtheme")
       (:id (first (forum:theme-messages (theme-by-name "Subtheme") nil nil)))))

    (is (= (count) 1))
    (is (= (count2) 1))))

(defspec forum-update-test
  (let [user (make:user-whole)
        hacker (make:user-whole)
        count #(:messages-count (theme-by-name-whole "Subtheme"))]
    (forum:save-or-update-tree [["Главная тема"
                                 :childs [["Subtheme" :first-message "Hello"]]]] user)

    (wrap-security (:id user)
      (message:update
       (:id (first (forum:theme-messages (theme-by-name "Subtheme") nil nil)))
       "changed message"))

    (wrap-security (:id hacker)
      (is (thrown-with-msg?
            Exception #"Security violation"
            (message:update
             (:id (first (forum:theme-messages (theme-by-name "Subtheme") nil nil)))
             "changed message"))))))

(defspec monger-session-store-test
  (make:user-internal "artem" "barmin")
  (testing "test that session storage for mongo is working correctly"
    (let [user (dissoc (user:check-credentials "artem" "barmin") :contact-raw)
          storage (store/monger-store "session_test")]
      (ringstore/write-session storage :user user)
      (is
       (=
        user
        (dissoc (secur/mongo-storage-transfomer (ringstore/read-session storage :user))
                :_id :date))))))

(defspec category-model-update
  (testing "check saving"
    (save-categories-tree
     [["Одежда" "clothes"
       [["Мужская" "male"]
        ["Унисекс" "unisex"]]]

    ["Красота и здоровье" "beauty"
     [["Косметика" "cosmetics"]
      ["Гигиена" "hygiene"]]]

    ["Иностранные закупки" "foreign"]])

    (is (= (simple-category-model)
           {"foreign" nil
            "other" nil                 ;created by default for all tests
            "beauty" {"hygiene" nil "cosmetics" nil}
            "clothes" {"unisex" nil "male" nil}})))

  (testing "check category tree update"
    (save-categories-tree
     [["Одежда" "clothes"
       [["New" "New"]]]

      ["Красота и здоровье" "beauty"
       [["Косметика" "cosmetics"
         [["New cosmetics" "New cosmetics"]]]]]

      ["Измененные иностранные закупки" "foreign"]])

    (is (= (simple-category-model)
           {"foreign" nil
            "other" nil                 ;created by default for all tests
            "beauty" {"hygiene" nil "cosmetics" {"New cosmetics" nil}}
            "clothes" {"unisex" nil "male" nil "New" nil}}))
    (is (= (:name (category:get-by-key "foreign")) "Измененные иностранные закупки"))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Materialized view tests

(defspec stock-search-mat-view
  (testing "Basic mat view checks, deal category and stock name changes - and
    check result table."
    (let [cat (make:category "cat")
          sup (make:supplier
                :category cat
                :stock-num 3
                :stock (fn[i]
                         {:name (str "name" (inc i))}))
          stocks (fn[](map (juxt :stock :category) (select :stock-search (order :fkey_stock))))]

      (is (= (stocks) [["name1" "cat"] ["name2" "cat"] ["name3" "cat"]]))

      (update category (set-fields {:name "a"}) (where {:id (:id cat)}))
      (update stock (set-fields {:name "stock for testing"}) (where {:id (:id (first (sup-stocks sup)))}))

      (is (= (stocks) [["stock for testing" "a"] ["name2" "a"] ["name3" "a"]])))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stock tags

(defspec stock-tags
  (let [cat (make:category "cat")
        deal (make:supplier :stock (fn[i]
                                     {:tags #{:hello :world}}))]
    (is (= #{:hello :world} (:tags (first (sup-stocks deal)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stock variants test(former price-change)

(defspec stock-variants-test
  (let [supplier (make:supplier :stock (fn[i]
                                         {:price 20
                                          :params {:color [:red :green :yellow] :size [:s :m]}
                                          :stock-variant {{:color :yellow :size :s} 25}})
                                :stock-num 1)
        user (make:user)
        [stock1] (map :id (sup-stocks supplier))]
    (wrap-security user
                   (wrap-supplier supplier

        (make:arrival supplier :parameters [{:size "s" :color "yellow"}] :count 10)

        (wrap-context [:wholesale :wholesale-sale]
          (let [order1 (retail:make-order [{:fkey_stock stock1 :size :s :color :yellow :quantity 2}])]
            (is (= (* 2 25) (:sum (retail:get-order order1))))))))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Category change url
(defspec category-test
    (category:update-short 411 "undernew"  "underwear")
    (is (= "stocks-female-undernew-linenshirt"
                            (first (->>
                                (select category
                                  (fields-only :key)
                                  (where {:id 423}))
                                (map :key ))))
    (is (= "stocks-detyam-underwear"
                            (first (->>
                                (select category
                                  (fields-only :key)
                                  (where {:id 489}))
                                (map :key )))))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Runners

(run-model-tests)
