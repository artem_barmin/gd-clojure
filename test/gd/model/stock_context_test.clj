(ns gd.model.stock_context_test
  (:refer-clojure :exclude [extend])
  (:use gd.model.test.data_builders
        [clojure.algo.generic.functor :only (fmap)])
  (:use gd.utils.logger)

  (:use clojure.test)
  (:use faker.company)
  (:use clj-time.core)

  (:use clojure.data)

  (:use gd.model.model)
  (:use gd.parsers.llapi)

  (:use korma.core)

  (:use gd.utils.common)
  (:use gd.utils.db)
  (:use gd.utils.web)
  (:use gd.utils.test)

  (:require
   gd.parsers.hlapi
   [clj-time.coerce :as tm]
   [gd.utils.security :as secur]
   [ring.middleware.session.store :as ringstore]
   [monger.ring.session-store :as store]))

(defspec retail-context-orders-test
  (testing "make two orders for same stock in different contexts - sum of order must be different"
    (let [supplier (make:supplier
                    :stock (constantly {:price 10
                                        :stock-variant {{:size :s :color :green}
                                                        [{:context :wholesale :price 50}
                                                         {:context :wholesale-sale :price 60}]}
                                        :context {:wholesale {:price 50}
                                                  :wholesale-sale {:price 60}}}))

          user (make:user)
          user2 (make:user)

          first-price
          (fn[](:price (first (sup-stocks supplier))))]

      (wrap-supplier supplier
                     (testing "check that stock prices are fetched correctly"
                       (is (= 10.00M (first-price)))
                       (is (= 50 (wrap-context [:wholesale] (first-price))))
                       (is (= 60 (wrap-context [:wholesale-sale] (first-price))))))

      (make:arrival supplier :parameters [{:size "s" :color "green"}] :count 50)

      (wrap-supplier supplier
        (let [stock1 (:id (first (sup-stocks supplier)))
              order1 (wrap-security user
                       (wrap-context [:wholesale-sale]
                         (retail:make-order [{:fkey_stock stock1
                                              :size :s
                                              :color :green
                                              :quantity 3}])))
              order2 (wrap-security user2
                       (wrap-context [:wholesale]
                         (retail:make-order [{:fkey_stock stock1
                                              :size :s
                                              :color :green
                                              :quantity 3}])))]
          (testing "For fixated orders we can fetch sum without any contexts"
            (is (= 180 (:sum (retail:get-order order1))))
            (is (= 150 (:sum (retail:get-order order2)))))))

      (wrap-supplier supplier
        (testing "multi-context environement. price and status are not accessible"
          (let [{:keys [price status]}
                (wrap-context [:wholesale :retail] (first (sup-stocks supplier)))]
            (is (thrown-with-msg?
                  Exception #"Attempt to access param in multi-context mode"
                  @price))
            (is (thrown-with-msg?
                  Exception #"Attempt to access param in multi-context mode"
                  @status))))))))

(defspec context-dependent-parameter-price-change
  (let [supplier (make:supplier
                  :stock (constantly {:price 10
                                      :params {:size [:s :m]
                                               :color [:red :green]}
                                      :stock-variant {{:size :s :color :green}
                                                      [{:price 45 :context :wholesale}
                                                       {:price 55 :context :retail}
                                                       {:price 50 :context :retail-sale}]}
                                      :context {:wholesale {:price 20}}})
                  :stock-num 1)

        user (make:user)
        user2 (make:user)

        stock1 (:id (first (sup-stocks supplier)))
        make-order (fn[] (retail:make-order [{:fkey_stock stock1
                                              :size :s
                                              :color :green
                                              :quantity 1}]))]

    (wrap-supplier supplier
     (is (= {"size" ["m" "s"]
            "color" ["green" "red"]}
           (wrap-context [:wholesale] (:all-params (first (sup-stocks supplier)))))))

    (make:arrival supplier :parameters [{:size "s" :color "green"}] :count 50)

    (wrap-supplier supplier
      (wrap-security user
        (let [order1 (wrap-context [:wholesale :wholesale-sale] (make-order))]
          (testing "For fixated orders we can fetch sum without any contexts"
            (is (= 45 (:sum (retail:get-order order1)))))))

      (wrap-security user2
        (let [order1 (wrap-context [:retail :retail-sale] (make-order))]
          (testing "For fixated orders we can fetch sum without any contexts"
            (is (= 55 (:sum (retail:get-order order1))))))))))

(defspec multi-context-price-and-stock-params
  (let [supplier (make:supplier
                  :stock (constantly {:price 10
                                      :stock-variant {{:size :s :color :red} 10}
                                      :params {:size [:s :m]
                                               :color [:red :green]}
                                      :context {:wholesale {:price 20}
                                                :wholesale-sale {:price 20}}}))]

    (testing "retail context is not exists - so we must copy all info from
wholesale(if exists) and from plain stock info"
      (wrap-supplier supplier
        (is (=
             {"size" ["m" "s"]
              "color" ["green" "red"]}
             (wrap-context [:wholesale :wholesale-sale :retail]
               (:all-params (first (sup-stocks supplier))))))))))

(defspec multi-context-stock-and-orders
  (testing "it is possible to make order in multi-context environement, but only
if it can be reduced to single-context. Context are selected for each specific
stock. Example : retail and retail-sale are two different contexts, but they
can be reduced to single context based on visibility status."
    (let [supplier (make:supplier
                    :stock-num 2
                    :stock (fn[i]
                             {:price 10
                              :params {:size [:s]
                                       :color [:green]}
                              :context {:wholesale
                                        {:price 20
                                         :status (if (= i 0) :invisible :visible)}
                                        :wholesale-sale
                                        {:price 15
                                         :status (if (= i 1) :invisible :visible)}}
                              :stock-variant {{:size :s :color :green}
                                              [{:price 23 :context :wholesale}
                                               {:price 22 :context :wholesale-sale}]}}))
          user (make:user)
          user2 (make:user)
          user3 (make:user)
          [stock1 stock2] (wrap-supplier supplier
                            (wrap-context [:wholesale :wholesale-sale]
                              (sort (map! :id (sup-stocks supplier)))))]

      (make:arrival supplier :parameters [{:size "s" :color "green"}] :count 50)

      (wrap-supplier supplier
        (testing "first stock - sale prices will be used"
          (let [order1 (wrap-security user
                         (wrap-context [:wholesale :wholesale-sale]
                           (retail:make-order [{:fkey_stock stock1
                                                :size :s
                                                :color :green
                                                :quantity 2}])))]

            (is (= (* 2 ( + 15 3 4)) (:sum (retail:get-order order1))))))

        (testing "second stock - normal prices will be used"
          (let [order2 (wrap-security user2
                         (wrap-context [:wholesale :wholesale-sale]
                           (retail:make-order [{:fkey_stock stock2
                                                :size :s
                                                :color :green
                                                :quantity 2}])))]

            (is (= (* 2 ( + 20 1 2)) (:sum (retail:get-order order2))))))

        (testing "incorrect context"
          (is
           (thrown-with-msg? Exception
             #"Stock don't have single context in multi-context mode"
             (wrap-context [:retail :wholesale-sale]
               (wrap-security user3
                 (retail:make-order [{:fkey_stock stock1
                                      :size :s
                                      :color :green
                                      :quantity 2}]))))))))))

(defspec multi-context-orders-with-empty-context
  (testing "try to make multi-context order with stocks without any stored
  contexts"
    (let [supplier (make:supplier
                    :stock-num 2
                    :stock (fn[i]
                             {:price 10
                              :stock-variant {{:size :s :color :green} 10}
                              :params {:size [:s]
                                       :color [:green]}}))
          [stock1 stock2] (wrap-supplier supplier
                            (wrap-context [:wholesale :wholesale-sale]
                              (sort (map! :id (sup-stocks supplier)))))
          user (make:user)]

      (make:arrival supplier :parameters [{:size "s" :color "green"}] :count 50)

      (wrap-security user
        (wrap-supplier supplier
          (testing "first stock - sale prices will be used"
            (let [order1 (wrap-context [:wholesale :wholesale-sale]
                           (retail:make-order [{:fkey_stock stock1
                                                :size :s
                                                :color :green
                                                :quantity 2}]))]

              (is (= 20 (:sum (retail:get-order order1)))))))))))

(defspec context-saving-on-user-registering
  (let [supplier (make:supplier)
        wholesale [:wholesale :wholesale-sale]
        retail [:retail :retail-sale]]
    (wrap-supplier supplier
      (let [user1
            (wrap-context wholesale
              (make:user))

            user2
            (wrap-context retail
              (make:user))]

        (wrap-supplier supplier
          (wrap-context wholesale
            (is (= [user1]
                   (map :id (user:get-all-users nil nil)))))

          (wrap-context retail
            (is (= [user2]
                   (map :id (user:get-all-users nil nil)))))

          (wrap-context (concat wholesale retail)
            (is (= [user1 user2]
                   (map :id (user:get-all-users nil nil))))))))))

(defspec empty-contexts-should-extract-info-from-stored-info
  (let [supplier (make:supplier
                  :stock-num 2
                  :stock (fn[i]
                           {:price 10
                            :params {:size [:s]
                                     :color [:green]}
                            :stock-variant {{:size :s :color :green} 40}}))]

    (wrap-supplier supplier
      (wrap-context [:wholesale-sale :wholesale :retail-sale :retail]
        (let [{:keys [all-params stock-variant]} (first (sup-stocks supplier))]
          (is (= {"size" ["s"] "color" ["green"]} all-params))
          (is (= {{:size "s" :color "green"} [{:price 40.00M :context :wholesale-sale}
                                              {:price 40.00M :context :wholesale}
                                              {:price 40.00M :context :retail-sale}
                                              {:price 40.00M :context :retail}]}
                 (fmap (partial map (fn[m] (select-keys m [:price :context]))) stock-variant))))))))

(defspec empty-retail-context-must-copy-from-wholesale
  (let [supplier (make:supplier
                  :stock-num 1
                  :stock (fn[i]
                           {:price 0
                            :context {:wholesale {:price 10 :status :visible}
                                      :wholesale-sale {:price 0 :status :invisible}}
                            :params {:size [:s :m]
                                     :color [:red]}
                            :stock-variant {{:size :m :color :red} [{:price 30 :context :wholesale}
                                                                    {:price 40 :context :wholesale-sale}]
                                            {:size :s :color :red} 30}}))
        user (make:user)]
    (wrap-supplier supplier
      (make:arrival supplier)

      (wrap-context [:retail :retail-sale]
        (testing "simple stock extraction"
          (let [{:keys [all-params price stock-variant]} (first (sup-stocks supplier))]
            (is (= {"size" ["m" "s"], "color" ["red"]} all-params))
            (is (= {{:size "m" :color "red"} {:price 30.00M :context :retail}
                    {:size "s" :color "red"} {:price 30.00M :context :retail}}
                   (fmap (fn[m] (select-keys m [:price :context])) stock-variant)))
            (is (= 10 price))))

        (testing "check that in orders - parameters are processed correctly"
          (wrap-security user
            (let [order1 (retail:make-order [{:fkey_stock (:id (first (sup-stocks supplier)))
                                              :size :m
                                              :color :red
                                              :quantity 3}])]
              (is (= (* 3 30) (:sum (retail:get-order order1)))))))))))

(defspec context-filtering-by-status
  (testing "retrieve stocks that are visible only in some contexts. We can use
default filtering, or pass custom filters(usefull for creating sale pages).
Custom context-status filters are applied as AND, default filters are applied as OR."

    ;; stock 1 - wholesale
    ;; stock 2 - wholesale-sale
    ;; stock 3 - no context(but wholesale = visible is true by default, so should be visible in :wholesale context,
    ;; also in :retail context)
    ;; stock 4 - only retail is visible
    (let [supplier (make:supplier
                    :stock-num 4
                    :stock (fn[i]
                             {:status :visible
                              :context (case i
                                         (0 1)
                                         {:wholesale {:price 0 :status (if (= i 0) :visible :invisible)}
                                          :wholesale-sale {:price 0 :status (if (= i 0) :invisible :visible)}}

                                         3
                                         {:wholesale {:price 0 :status :invisible}
                                          :retail {:price 0 :status :visible}}

                                         nil)}))
          [stock1 stock2 stock3 stock4] (map :id (sup-stocks supplier))]

      (wrap-supplier supplier

        (doseq [type [:retail :retail-sale :wholesale :wholesale-sale]]
          (stock-context:resolve-or-create-type type))

        (wrap-context [:wholesale-sale :wholesale]
          (testing "show only stocks that are in 'sales' status. Normal
'wholesale' should be invisible, but 'wholesale-sale' should be visible."
            (is (= [stock2]
                   (map :id (retail:supplier-stocks nil nil {:context-status {:wholesale-sale :visible
                                                                              :wholesale :invisible}})))))

          (testing "show only stocks that are not in sales mode"
            (is (= [stock1 stock3]
                   (map :id (retail:supplier-stocks nil nil {:context-status {:wholesale-sale :invisible
                                                                              :wholesale :visible}})))))

          (testing "Check default filtering. If any of requested contexts are in
          'visible' state, stock should be visible."
            (is (= [stock1 stock2 stock3] (map :id (retail:supplier-stocks nil nil {})))))

          (testing "Interference with global stock status. It overlaps any other
checks, and if global stock status is :invisible, it should be invisible without
respect of any context settings."
            (stock:update-stock stock1 {:status :invisible})
            (is (= [stock2 stock3] (map :id (retail:supplier-stocks nil nil {}))))))

        (wrap-context [:retail]
          (testing "Default filtering for retail mode. Stock1 is set to
:invisible mode, and this mode is propagated to :retail context automaticaly."
            (is (= [stock2 stock3 stock4] (map :id (retail:supplier-stocks nil nil {}))))))))))

(defspec context-filtering-by-status-empty-context
  (let [supplier (make:supplier
                  :stock-num 4
                  :stock (fn[i]
                           (merge {:status :visible}
                                  (when (= i 0)
                                    {:context {:wholesale {:price 0 :status :visible}}}))))]

    (testing "with empty context stocks should be visible anyway"
      (wrap-supplier supplier
        (wrap-context [:wholesale-sale :wholesale]
          (is (= 4
                 (count (retail:supplier-stocks
                         nil nil {:context-status {:wholesale-sale :invisible :wholesale :visible}})))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Runners

(run-model-tests)
