(ns gd.model.scheduled_test
  (:refer-clojure :exclude [extend])
  (:use gd.model.test.data_builders)
  (:use gd.utils.logger)

  (:use clojure.test)
  (:use faker.company)
  (:use clj-time.core)

  (:use clojure.data)

  (:use gd.model.model)
  (:use gd.parsers.llapi)

  (:use korma.core)

  (:use gd.utils.common)
  (:use gd.utils.db)
  (:use gd.utils.test)

  (:require
   gd.parsers.hlapi
   [clj-time.coerce :as tm]
   [gd.utils.security :as secur]
   [ring.middleware.session.store :as ringstore]
   [monger.ring.session-store :as store])
  (:import (java.util.concurrent Executors CountDownLatch TimeUnit)))

(defmacro fix-scheduled-transactions[& body]
  `(let [real-every# overtone.at-at/every
         connection# clojure.java.jdbc.internal/*db*]
     (with-redefs-fn {#'overtone.at-at/every
                      (fn[ms-period# fun# pool# & other#]
                        (apply real-every#
                               ms-period#
                               (fn[](transaction-with-connection connection# (fun#)))
                               pool#
                               other#))}
       (fn[] ~@body))))

(defspec scheduling-basic
  (let [sup (make:supplier)
        latch (new java.util.concurrent.CountDownLatch 1)]

    (fix-scheduled-transactions
     (with-redef-multimethods schedule:execute-job
       {:compute-reports (fn[type data deal-id]
                           (when (and (= type :compute-reports) (= data {:test "1"}))
                             (.countDown latch)))}
       (schedule:start-or-update 1 :compute-reports {:test "1"} sup)
       ;; vk-news processor should fire in at least 5 seconds
       (is (.await latch 5 TimeUnit/SECONDS))
       (Thread/sleep 1000))))

  (testing "detect results of task execution(+exceptions)"))

(defspec scheduling-restore-from-db
  (let [sup (make:supplier)
        latch (new java.util.concurrent.CountDownLatch 2)]
    (with-redefs [gd.model.model/offsets-for-tasks {:compute-reports 1000}]
      (fix-scheduled-transactions
       (with-redef-multimethods schedule:execute-job
         {:compute-reports (fn[type data deal-id]
                             (when (and (= type :compute-reports) (= data {:test "1"}))
                               (.countDown latch)))}

         ;; schedule task - it must execute and write 'last-execution-time' field
         (schedule:start-or-update 5 :compute-reports {:test "1"} sup)
         (is (:last-execution-time (first (schedule:all-tasks))))

         ;; clean scheduler tasks
         (clean-scheduler)

         ;; wait to be sure that diff will be computed correctly
         (Thread/sleep 1000)

         ;; restore all tasks from db. They must not fire exactly now, but
         ;; scheduled to make pause after previous task.
         ;; Also it must be : 3.9seconds < t < 4.1seconds
         (let [real-every overtone.at-at/every]
           (with-redefs-fn {#'overtone.at-at/every (fn[& args]
                                                     (let [delay
                                                           (:initial-delay (apply hash-map (drop 3 args)))]
                                                       (is (> delay (* 3.9 1000)))
                                                       (is (< delay (* 4.1 1000))))
                                                     (apply real-every args))}
             (fn[](schedule:restore-timers))))

         ;; vk-news processor should fire in at least 5 seconds
         (is (.await latch 10 TimeUnit/SECONDS))

         ;; check recorded events - they must have about 5 seconds of difference
         (let [[[status1 time1] [status2 time2]]
               (map (juxt :status :time) (schedule:get-execution-results (:id (first (schedule:all-tasks)))))]
           (is (= [:success :success] [status1 status2]))
           (is (= 5 (in-secs (interval (tm/from-date time1) (tm/from-date time2)))))))))))

(defspec scheduling-time-distribution-of-same-tasks
  (let [[deal1 deal2 deal3] [(make:supplier) (make:supplier) (make:supplier)]
        latch (new java.util.concurrent.CountDownLatch 3)]
    (fix-scheduled-transactions
     (with-redef-multimethods schedule:execute-job
       {:compute-reports (fn[type data deal-id]
                           (when (= type :compute-reports) (.countDown latch)))}

       ;; schedule task - it must execute and write 'last-execution-time' field
       (with-redefs [gd.model.model/offsets-for-tasks {:compute-reports 2000}]
         (schedule:start-or-update 10 :compute-reports {:test "1"} deal1)
         (schedule:start-or-update 10 :compute-reports {:test "2"} deal2)
         (schedule:start-or-update 10 :compute-reports {:test "3"} deal3)

         (is (.await latch 10 TimeUnit/SECONDS))

         (let [results (sort-by :time (mapcat (comp schedule:get-execution-results :id) (schedule:all-tasks)))]
           (is (= 3 (count results)))
           (is (= [2 2]
                  (map (fn[l r]
                         (in-secs (interval (tm/from-date l) (tm/from-date r))))
                       (map :time results)
                       (rest (map :time results)))))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Runners

(run-model-tests)
