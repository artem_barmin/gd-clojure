(ns gd.model.wysiwyg
  (:use clojure.test
        gd.utils.test)
  (:use gd.model.model)
  (:import [org.mozilla.javascript Scriptable Context ScriptableObject Function]
           name.fraser.neil.plaintext.diff_match_patch$Operation
           name.fraser.neil.plaintext.diff_match_patch))

(def ctx (Context/enter))

(def scope (.initStandardObjects ctx))

(defn html-to-bbcode[html]
  (let [l-ctx (Context/enter)
        l-scope (.newObject l-ctx scope)]
    (.setPrototype l-scope scope)
    (.setParentScope l-scope nil)
    (.put scope "htmlCode" scope (Context/toObject html scope))
    (.evaluateString l-ctx l-scope (str (slurp "resources/public/js/custom-lib/bbcode.js")
                                        "var result = convertHTMLtoBBCode(htmlCode)") "t" 1 nil)
    (let [result (.get l-scope "result" l-scope)]
      (Context/exit)
      result)))

(def diff-markers
  {diff_match_patch$Operation/EQUAL " "
   diff_match_patch$Operation/INSERT "+"
   diff_match_patch$Operation/DELETE "-"})

(defn- print-diff-entry [d]
  (let [m (diff-markers (.operation d))]
    (println (str " " m) (.trim (.text d)))))

(defn- ignore-diffs[diffs]
  (remove (fn[d] (#{"<wbr>" "style=\"margin-left:30px;\"" ""} (.trim (.text d)))) diffs))

(defn print-diff[x y]
  (let [dmp (diff_match_patch.)
        diffs (.diff_main dmp x y)]
    (do (.diff_cleanupSemantic dmp diffs)
        (let [diffs (ignore-diffs (.diff_main dmp x y))
              real-diffs (remove (fn[d] (= diff_match_patch$Operation/EQUAL (.operation d))) diffs)]
          (when (seq real-diffs)
            (doseq [d diffs]
              (print-diff-entry d)))
          (count real-diffs)))))

(deftest letter-test
  (let [html (slurp "test/resources/bbcode/letter.html")
        bbcode (process-text (html-to-bbcode html))]
    (is (zero? (print-diff html (bbcode-to-html bbcode))))))

(deftest letter-two-pass-test
  (let [html (slurp "test/resources/bbcode/letter.html")
        bbcode (process-text (html-to-bbcode html))
        html-second-pass (html-to-bbcode (bbcode-to-html bbcode))]
    (is (zero? (print-diff html (bbcode-to-html html-second-pass))))))

(deftest list-test
  (let [html (slurp "test/resources/bbcode/list.html")
        bbcode (process-text (html-to-bbcode html))]
    (is (zero? (print-diff html (bbcode-to-html bbcode))))))

(deftest list-two-pass-test
  (let [html (slurp "test/resources/bbcode/list.html")
        bbcode (process-text (html-to-bbcode html))
        html-second-pass (html-to-bbcode (bbcode-to-html bbcode))]
    (is (zero? (print-diff html (bbcode-to-html html-second-pass))))))

(run-tests)
