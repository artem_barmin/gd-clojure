(ns gd.model.simple-accounting
  (:refer-clojure :exclude [extend])
  (:use gd.model.test.data_builders)
  (:use gd.utils.logger)

  (:use clojure.test)
  (:use faker.company)
  (:use clj-time.core)

  (:use clojure.data)

  (:use gd.model.model)
  (:use gd.parsers.llapi)

  (:use korma.core)

  (:use gd.utils.common)
  (:use gd.utils.db)
  (:use gd.utils.web)
  (:use gd.utils.test)

  (:require
   gd.parsers.hlapi
   [clj-time.coerce :as tm]
   [gd.utils.security :as secur]
   [ring.middleware.session.store :as ringstore]
   [monger.ring.session-store :as store]))

(defspec stock-list-test
  (testing "all parameters should be available for stock"
    
    ))

(defspec arrival-auto-filling-test
  (testing "create order without warehouse remains - items should be added to special arrival"

    (let [supplier (make:supplier :stock-num 2 :stock (constantly {:accounting :simple}))
          user (make:user)
          user2 (make:user)
          [{:keys [id]}] (sup-stocks supplier)
          make-order (fn[user quantity]
                       (wrap-security user
                         (retail:make-order
                          [{:fkey_stock id :size :m :color :red :quantity quantity}])))
          left-stocks
          (fn[]
            ((juxt :consumed :arrived)
             (first (select :consumed-arrived-stocks (where {:fkey_stock id})))))]

      (wrap-supplier supplier
        (let [order1 (make-order user 12)
              order2 (make-order user2 13)]

          (let [arrival (:stock-arrival-parameter (retail:get-admin-stock id))]
            (is (= 1 (count arrival)))
            (is (= (map (comp :quantity first) (vals arrival)) [25])))
          
          (testing "check that remaining on warehouse is zero"
            (is (= [(+ 12 13) (+ 12 13)] (left-stocks))))

          (testing "change order from 12 to 28 - number of left should still be zero"

            (wrap-security user
              (retail:change-order-item (:id (first (:user-order-item (retail:get-order order1))))
                                        {:fkey_stock id :size :m :color :red :quantity 28}))
            
            (is (= [(+ 28 13) (+ 28 13)] (left-stocks))))

          (testing "close special arrival, new arrival should be created
automatically, new arrival should contain only 22 items(because previous items
is added to the previous arrival)"
            (special-arrival:close)
            
            (wrap-security user
              (retail:change-order-item (:id (first (:user-order-item (retail:get-order order1))))
                                        {:fkey_stock id :size :m :color :red :quantity 50}))

            (let [arrival (:stock-arrival-parameter (retail:get-admin-stock id))]
              (is (= 2 (count arrival)))
              (is (= (map (comp :quantity first) (map second (sort-by first arrival))) [41 22])))
            
            (is (= [(+ 50 13) (+ 50 13)] (left-stocks))))

          (testing "remove most of items from order, second arrival should be empty for this stock, and
first arrival should be reduced"
            (wrap-security user
              (retail:change-order-item (:id (first (:user-order-item (retail:get-order order1))))
                                        {:fkey_stock id :size :m :color :red :quantity 5}))

            (let [arrival (:stock-arrival-parameter (retail:get-admin-stock id))]
              (is (= (map (comp :quantity first) (vals arrival)) [(+ 5 13) 0])))
            
            (is (= [(+ 5 13) (+ 5 13)] (left-stocks)))))))))

(defspec arrival-auto-filling-test-after-remove
  (let [supplier (make:supplier :stock-num 2 :stock (constantly {:accounting :simple}))
        user (make:user)
        user2 (make:user)
        [{:keys [id]}] (sup-stocks supplier)
        make-order (fn[user quantity]
                     (wrap-security user
                       (retail:make-order
                        [{:fkey_stock id :size :m :color :red :quantity quantity}])))
        left-stocks
        (fn[]
          ((juxt :consumed :arrived)
           (first (select :consumed-arrived-stocks (where {:fkey_stock id})))))]

    (wrap-supplier supplier
      (let [order1 (make-order user 12)
            order2 (make-order user2 13)]

        (wrap-security user
          (retail:delete-order-item (:id (first (:user-order-item (retail:get-order order1))))))

        (testing "number of arrived stocks is automatically reduced"
          (is (= [13 13] (left-stocks))))))))

(defspec special-arrival-different-parameters-test
  (testing "create order without warehouse remains - items should be added to special arrival"

    (let [supplier (make:supplier :stock-num 2 :stock (constantly {:accounting :simple}))
          user (make:user)
          user2 (make:user)
          [{:keys [id]}] (sup-stocks supplier)
          make-order (fn[user quantity size]
                       (wrap-security user
                         (retail:make-order
                          [{:fkey_stock id :size size :color :red :quantity quantity}])))
          left-stocks
          (fn[]
            ((juxt :consumed :arrived)
             (first (select :consumed-arrived-stocks (where {:fkey_stock id})))))]

      (wrap-supplier supplier
        (let [order1 (make-order user 12 :s)
              order2 (make-order user2 12 :m)]
          
          (is (=
               [[12 12] [12 12]]
               (map (juxt :consumed :arrived) (select :consumed-arrived-stocks (where {:fkey_stock id}))))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Runners

(run-model-tests)
