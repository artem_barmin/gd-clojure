(ns gd.model.stock_arrival_test
  (:refer-clojure :exclude [extend])
  (:use gd.model.test.data_builders)
  (:use gd.utils.logger)

  (:use clojure.test)
  (:use faker.company)
  (:use clj-time.core)

  (:use clojure.data)

  (:use gd.model.model)
  (:use gd.parsers.llapi)

  (:use korma.core)

  (:use gd.utils.common)
  (:use gd.utils.db)
  (:use gd.utils.web)
  (:use gd.utils.test)

  (:require
   gd.parsers.hlapi
   [clj-time.coerce :as tm]
   [gd.utils.security :as secur]
   [ring.middleware.session.store :as ringstore]
   [monger.ring.session-store :as store]))

(defn stock-arrival-params[supplier]
  (->>
   (sup-stocks supplier)
   (map (comp vals :stock-arrival-parameter))
   flatten
   (sort-by :id)
   (map (juxt :parameters :quantity :price))))

(defspec basic-stock-arrival-test
  (testing "basic API for stock arrival - save and load"
    (let [supplier (make:supplier :stock-num 1)
          [{:keys [id]}] (sup-stocks supplier)
          arrival (arrival:create-new-stock-arrival "name" "supplier")]
      (arrival:save-multiple-stock-arrivals
       arrival {id [{:parameters {:size "m" :color "red"}
                     :price 100
                     :quantity 3}
                    {:parameters {:size "veeery" :color "green"}
                     :price 200
                     :quantity 4}]})

      (is (= [[{:color "red" :size "m"} 3 100.00M]
              [{:color "green" :size "veeery"} 4 200.00M]]
             (stock-arrival-params supplier)))

      ;; overwrite arrival
      (arrival:save-multiple-stock-arrivals
       arrival {id [{:parameters {:size "s" :color "red"}
                     :price 100
                     :quantity 3}]})

      (is (= [[{:color "red" :size "s"} 3 100.00M]]
             (stock-arrival-params supplier))))))

(defspec basic-left-stock-test
  (let [supplier (make:supplier :stock-num 2)
        [{:keys [id]}] (sup-stocks supplier)
        arrival (arrival:create-new-stock-arrival "name" "supplier")
        user (make:user)]
    (arrival:save-multiple-stock-arrivals
     arrival {id [{:parameters {:size "m" :color "red"}
                   :price 100
                   :quantity 3}
                  {:parameters {:size "veeery" :color "green"}
                   :price 200
                   :quantity 4}]})

    (wrap-supplier supplier
      (wrap-security user
        (retail:make-order
         [{:fkey_stock (:id (second (sup-stocks supplier)))
           :size :s :color :green :quantity 12}])))

    (let [[arrived consumed]
          (map :left-stocks (#'gd.model.model/analytics:annotate-left-stocks (sup-stocks supplier)))]
      (is (= 4 (get arrived {:color "green" :size "veeery"})))
      (is (= 3 (get arrived {:color "red" :size "m"})))
      (is (= 0 (get consumed {:color "green" :size "s"}))))))

(defspec order-item-unavailable-stock-quantity-test
  (testing "Делаем заказ, товар расходуется и для каждого order-item должно быть
  доступно количество недоступного товара"
    (let [supplier (make:supplier :stock-num 2)
          user (make:user)
          user2 (make:user)
          user3 (make:user)
          user4 (make:user)
          [{:keys [id]}] (sup-stocks supplier)
          arrival (arrival:create-new-stock-arrival "name" "supplier")
          make-order (fn[user quantity]
                       (wrap-security user
                         (retail:make-order
                          [{:fkey_stock id :size :m :color :red :quantity quantity}])))
          arrive (fn[quantity]
                   (arrival:save-multiple-stock-arrivals
                    arrival {id [{:parameters {:size "m" :color "red"}
                                  :price 100
                                  :quantity quantity}
                                 {:parameters {:size "m" :color "green"}
                                  :price 100
                                  :quantity quantity}]}))]

      (wrap-supplier supplier
        (arrive 15)

        (testing "делаем несколько заказов из под разных пользователей"
          (let [order1 (make-order user 12)
                order2 (make-order user2 13)
                order3 (make-order user3 15)]

            (testing "делаем левый заказ, просто чтобы проверить что количество
считается по совокупности всех параметров. Это не должно никак влиять на
результаты тестов"
              (wrap-security user4
                (retail:make-order
                 [{:fkey_stock id :size :m :color :green :quantity 10}])))

            (testing "делаем левый заказ, по параметрам без приходов - должно работать"
              (wrap-security user4
                (retail:make-order
                 [{:fkey_stock id :size :s :color :green :quantity 10}])))

            (is (= [0 10 15]
                   (map :closed (concat (retail-order-items order1)
                                        (retail-order-items order2)
                                        (retail-order-items order3)))))

            (testing "после нового прихода, ничего не раскидывается автоматом -
          предоставляем возможность раскидать все руками"
              (arrive 11)
              (is (= [0 10 15]
                     (map :closed (concat (retail-order-items order1)
                                          (retail-order-items order2)
                                          (retail-order-items order3))))))))))))

(defspec order-item-unavailable-stock-quantity-test-same-user-orders-merging
  (testing "делаем несколько заказов от одного пользователя - надо сливать не
      только order-item но и количество отмененных товаров"
    (let [supplier (make:supplier :stock-num 2)
          user (make:user)
          user2 (make:user)
          user3 (make:user)
          user4 (make:user)
          [{:keys [id]}] (sup-stocks supplier)
          arrival (arrival:create-new-stock-arrival "name" "supplier")
          make-order (fn[user quantity]
                       (wrap-security user
                         (retail:make-order
                          [{:fkey_stock id :size :m :color :red :quantity quantity}])))
          arrive (fn[quantity]
                   (arrival:save-multiple-stock-arrivals
                    arrival {id [{:parameters {:size "m" :color "red"}
                                  :price 100
                                  :quantity quantity}
                                 {:parameters {:size "m" :color "green"}
                                  :price 100
                                  :quantity quantity}]}))]

      (wrap-supplier supplier
        (arrive 15)

        (testing "делаем несколько заказов из под одного пользователя"
          (let [order1 (make-order user 12)
                order2 (make-order user 13)
                order3 (make-order user 15)]
            (is (= order1 order2 order3))
            (is (= [25] (map :closed (retail-order-items order1))))

            (testing "удаляем заказ - он не должен учиываться как заказынный"
              (wrap-security user
                (retail:delete-order-item (:id (first (retail-order-items order1)))))
              (let [new-order (make-order user 12)]
                (is (= [0] (map :closed (retail-order-items new-order))))))))))))

(defmacro with-only-visible[& body]
  `(binding [gd.model.context/*supplier-info* {:stocks-mode :available-from-arrival}]
     ~@body))

(defspec constrainted-retail-user-parameters-view-to-only-available
  (let [supplier (make:supplier :stock-num 2
                                :stock (fn[i]
                                         {:stock-variant (if (= i 0)
                                                           {{:size :s} 20}
                                                           {{:size :xl} 20})
                                          :params (if (= i 0)
                                                    {[:brand false] ["TM Azra"]
                                                     :size [:s :m]}
                                                    {[:brand false] ["TM Bono"]
                                                     :size [:xl :xxl]})}))
        [{:keys [id]}] (sup-stocks supplier)]

    (arrival:save-multiple-stock-arrivals
     (arrival:create-new-stock-arrival "name" "supplier")
     {id [{:parameters {:size "m"}
           :price 100
           :quantity 50}]})

    (with-only-visible
      (testing "only brands of visible stocks should be available"
        (wrap-supplier supplier
          (is (= ["TM Azra"] (get (retail:get-user-stock-params) "brand"))))))))

(defspec arrival-deletion-as-setting-quantity-to-zero
  (testing "check that setting quantity to 0 - is deleting stock from arrival"
    (let [supplier (make:supplier :stock-num 2
                                  :stock (constantly {:stock-variant {{:size :s} 20}
                                                      :params {:size [:s :m]}}))
          [{:keys [id]}] (sup-stocks supplier)]

      (wrap-supplier supplier
        (with-only-visible
          (let [arrival (arrival:create-new-stock-arrival "name" "supplier")]
            (is (empty? (sup-stocks supplier)))
            (arrival:save-multiple-stock-arrivals arrival {id [{:parameters {:size "m"} :quantity 3}]})
            (is (= [1] (map :stocks (arrival:all-arrivals nil nil))))

            (is (seq (sup-stocks supplier)))
            (arrival:save-multiple-stock-arrivals arrival {id []})
            (is (= [0] (map :stocks (arrival:all-arrivals nil nil))))
            (is (empty? (sup-stocks supplier)))))))))

(defspec arrival-price-not-changed-by-manager
  (testing "от менеджера цена приходит пустая, т.к. он её не видит. но она должна сохранятся"
    (let [supplier (make:supplier :stock-num 2
                                  :stock (constantly {:stock-variant {{:size :s} 20}
                                                      :params {:size [:s :m]}}))
          [{:keys [id]}] (sup-stocks supplier)]

      (wrap-supplier supplier
        (with-only-visible
          (let [arrival (arrival:create-new-stock-arrival "name" "supplier")]
            (arrival:save-multiple-stock-arrivals arrival {id [{:parameters {:size "m"}
                                                                :price 100
                                                                :quantity 3}]})

            (is (= [[{:size "m"} 3 100.00M]] (stock-arrival-params supplier)))

            (arrival:save-multiple-stock-arrivals arrival {id [{:parameters {:size "m"}
                                                                :quantity 3}]})

            (is (= [[{:size "m"} 3 100.00M]] (stock-arrival-params supplier)))))))))

(defspec filter-by-parameters-with-respect-of-arrival
  (let [supplier (make:supplier :stock-num 3
                                :stock (fn[i]
                                         {:stock-variant {{:size :s} 20}
                                          :params
                                          (merge
                                           {[:brand false] ["brand"]}
                                           (case i
                                             0 {:size [:s :m]}
                                             1 {:size [:s :xl :xxl]}
                                             2 {:size [:s]}))}))
        [stock1 stock2] (map :id (sup-stocks supplier))
        arrival (arrival:create-new-stock-arrival "name" "supplier")]
    (with-only-visible
      (wrap-supplier supplier
        (arrival:save-multiple-stock-arrivals
         arrival {stock1 [{:parameters {:size "s"} :price 100 :quantity 3}]
                  stock2 [{:parameters {:size "xl"} :price 100 :quantity 3}]})
        (testing "фильтруем список товаров по параметрам, с учетом доступности тех
  или иных параметров"
          (testing "фильтруем по параметрам которые есть на всех товарах, но
      доступны только для первого товара"
            (is (= [stock1] (map :id (retail:supplier-stocks nil nil {:filters {:size "s"}})))))
          (testing "фильтруем по невидимым параметрам(должны учитываться все, без
      завязок на приходы, но должны быть только те товары, по которым были приходы)"
            (is (= [stock1 stock2] (map :id (retail:supplier-stocks nil nil {:filters {:brand "brand"}}))))))
        (testing "получаем список доступных параметров для категории"
          (is (= {"brand" ["brand"] "size" ["s" "xl"]} (retail:get-user-stock-params))))))))

(defspec fill-price-after-parameters
  (let [supplier (make:supplier :stock-num 3
                                :stock (fn[i]
                                         {:stock-variant {{:size :s} 20}
                                          :params
                                          (merge
                                           {[:brand false] ["brand"]}
                                           (case i
                                             0 {:size [:s :m]}
                                             1 {:size [:s :xl :xxl]}
                                             2 {:size [:s]}))}))
        [stock1 stock2] (map :id (sup-stocks supplier))
        arrival (arrival:create-new-stock-arrival "name" "supplier")]
    (with-only-visible
      (wrap-supplier supplier
        (arrival:save-multiple-stock-arrivals
         arrival {stock1 [{:parameters {:size "s"} :quantity 3}]})
        (arrival:save-multiple-stock-arrivals
         arrival {stock1 [{:parameters {:size "s"} :quantity 3 :price 100.00M}]})
        (is (= [[{:size "s"} 3 100.00M]] (stock-arrival-params supplier)))))))

(defspec mat-view-debug
  (let [supplier (make:supplier :stock-num 3
                                :stock (fn[i]
                                         {:stock-variant {{:size :s} 20}
                                          :params
                                          (merge
                                           {[:brand false] ["brand"]}
                                           (case i
                                             0 {:size [:s :m]}
                                             1 {:size [:s :xl :xxl]}
                                             2 {:size [:s]}))}))
        [stock1] (map :id (sup-stocks supplier))
        user (make:user)
        arrival (arrival:create-new-stock-arrival "name" "supplier")]
    (wrap-supplier supplier
      (arrival:save-multiple-stock-arrivals arrival {stock1 [{:parameters {:size "s"} :quantity 10}]})

      (wrap-security user
        (retail:make-order
         [{:fkey_stock stock1 :size :s :quantity 5}]))

      (is (= [[nil nil nil] [nil nil nil] [{:size "s"} 10 nil]]
             (stock-arrival-params supplier)))

      (arrival:save-multiple-stock-arrivals arrival {stock1 []})

      (is (= [[nil nil nil] [nil nil nil] [nil nil nil]]
             (stock-arrival-params supplier))))))

(defspec inventory-with-snapshot
  (let [supplier (make:supplier :stock-num 3
                                :stock (fn[i]
                                         {:stock-variant {{:size :s} 20}
                                          :params
                                          (merge
                                           {[:brand false] ["brand"]
                                            :size [:s :m]})}))
        [stock1] (map :id (sup-stocks supplier))
        user (make:user)
        user1 (make:user)]

    (wrap-supplier supplier

      ;; initial arrival
      (make:arrival supplier :parameters [{:size "m"}] :count 50)

      ;; make two orders, first in status :processed, second in :created
      (let [order1 (wrap-security user
                     ;; take 12 items (left 38, consumed 12)
                     (let [order (retail:make-order
                                  [{:fkey_stock stock1 :size :m :quantity 12}])]
                       (retail:set-order-status order :processed)
                       order))

            order2 (wrap-security user1
                     ;; take 12 items (left 26, consumed 24)
                     (retail:make-order
                      [{:fkey_stock stock1 :size :m :quantity 12}]))]

        (is (= 24 (:consumed (first (select :consumed-arrived-stocks (where {:fkey_stock stock1}))))))

        (let [inventory (inventory:create-inventory "hello" supplier :inventory)]

          ;; inventory - we realized that actually only 5 items left on the warehouse
          (arrival:save-multiple-stock-arrivals inventory {stock1 [{:parameters {:size "m"} :quantity -38}]})
          (inventory:close-inventory inventory)

          ;; check invariant, at every time left stocks should not be negative
          (is (empty? (select :consumed-arrived-stocks (where (< (raw "arrived-consumed") 0)))))

          (is (= [5 5]
                 ((juxt :consumed :arrived)
                  (first (select :consumed-arrived-stocks (where {:fkey_stock stock1}))))))

          ;; 0 from 12
          (is (= [12 0] (first (map (juxt :quantity :closed) (retail-order-items order1)))))
          ;; 12 closed from 12, quantity = 0
          (is (= [0 12] (first (map (juxt :quantity :closed) (retail-order-items order2))))))))))

(defspec inventory+order-moved-to-created
  (let [supplier (make:supplier :stock-num 3
                                :stock (fn[i]
                                         {:stock-variant {{:size :s} 20}
                                          :params
                                          (merge
                                           {[:brand false] ["brand"]
                                            :size [:s :m]})}))
        [stock1] (map :id (sup-stocks supplier))
        user (make:user)]

    (wrap-supplier supplier

      ;; initial arrival
      (make:arrival supplier :parameters [{:size "m"}] :count 50)

      (let [order1 (wrap-security user
                     ;; take 12 items (left 38, consumed 12)
                     (let [order (retail:make-order [{:fkey_stock stock1 :size :m :quantity 12}])]
                       (retail:set-order-status order :in-work)
                       order))]

        (let [inventory (inventory:create-inventory "hello" supplier :inventory)]

          (wrap-security user
            ;; take 8 items (left 30, consumed 20)
            (retail:make-order [{:fkey_stock stock1 :size :m :quantity 8}]))

          ;; приходит новый заказ от того же пользователя. заказ переводится в состояние создан, но
          ;; в инвентаризации мы не должны его учитывать т.к. на момент создания заказа он уже был собран
          ;; система должна учитывать что на складе 38 ед. товара, а не 30
          (is (= {{:size "m"} 38}
                 (:left-snapshot (inventory:annotate-single-stock-with-snaphost inventory {:id stock1}))))
          ;; не меняем ничего, считаем что на складе 38 ед товара
          (arrival:save-multiple-stock-arrivals inventory {stock1 [{:parameters {:size "m"} :quantity 0}]})
          (inventory:close-inventory inventory)

          ;; 20 ед. списано, 50 зашло
          (is (= [20 50]
                 ((juxt :consumed :arrived)
                  (first (select :consumed-arrived-stocks (where {:fkey_stock stock1}))))))

          (is (= [20 0] (first (map (juxt :quantity :closed) (retail-order-items order1))))))))))

(defspec inventory+closed-recomputing-test
  (testing "при закрытии инвентаризации, может произойти уменьшение остатков.

Но уменьшение остатков может произойти только на кол-во: доступное на складе на момент создания переучета + кол-во в открытых заказах на момент создания переучета.

При закрытии мы выгребаем все открытые заказы, по дате создания, и закрываем часть позиций в них что бы сравнять кол-во на складе.

В результате consumed <= arrived инвариант соблюдается.

WARN: может возникнуть проблема. Покрыта следующим тестом.
"
    (let [supplier (make:supplier :stock-num 3
                                  :stock (fn[i]
                                           {:stock-variant {{:size :s} 20}
                                            :params {:size [:s :m]}}))
          [stock1] (map :id (sup-stocks supplier))
          user (make:user)]

      (wrap-supplier
       supplier

       ;; initial arrival
       (make:arrival supplier :parameters [{:size "m"}] :count 5)

       (wrap-security
        (make:user)
        (retail:make-order [{:fkey_stock stock1 :size :m :quantity 3}]))

       (wrap-security
        (make:user)
        (retail:make-order [{:fkey_stock stock1 :size :m :quantity 2}]))

       (let [inventory (inventory:create-inventory "hello" supplier :inventory)]

         (arrival:save-multiple-stock-arrivals inventory {stock1 [{:parameters {:size "m"} :quantity -4}]})
         (inventory:close-inventory inventory))))))

(defspec inventory+closed-recomputing-test+some-orders-closed-between
  (testing "если не трогать уже закрытые заказы, может возникнуть такая ситуация что было продано больше товаров чем закуплено:
1. сделали приход 5 ед
2. создали переучет
3. сделали заказ 3 ед(№1)
4. отправили его, закрыли
5. сделали заказ 2 ед
6. в переучете поставили 1(-4 ед. пойдет в базу)
7. при расчете уже нельзя закрыть заказ №1, таким образом проданных товаров будет больше пришедших

consumed <= arrived инвариант не соблюдается, и мы не можем его исправить, т.к. часть заказов уже закрыты.

по товарам с которыми были проблемы с переучетами, была такая проблема. Переучет создали 5 сентября, а закрыли только 6
возникло несоотвествие
в целом надо запрещать менять статусы заказов пока есть открытые переучеты "
    (let [supplier (make:supplier :stock-num 3
                                  :stock (fn[i]
                                           {:stock-variant {{:size :s} 20}
                                            :params {:size [:s :m]}}))
          [stock1] (map :id (sup-stocks supplier))
          user (make:user)]

    (wrap-supplier
     supplier

     ;; initial arrival
     (make:arrival supplier :parameters [{:size "m"}] :count 5)

     (let [inventory (inventory:create-inventory "hello" supplier :inventory)]

       (wrap-security
        (make:user)
        (retail:set-order-status (retail:make-order [{:fkey_stock stock1 :size :m :quantity 3}]) :closed))

       (wrap-security
        (make:user)
        (retail:make-order [{:fkey_stock stock1 :size :m :quantity 2}]))

       (arrival:save-multiple-stock-arrivals inventory {stock1 [{:parameters {:size "m"} :quantity -4}]})
       (inventory:close-inventory inventory))))))

(defspec inventory+working-to-open-move
  "1. на момент создания инвентаризаций есть заказы в статусе 'working'. Они не учитываются при создании снапшота.
2. В процессе инвентаризации, делается дозаказ, и они возвращаются в статус создан
3. при закрытии инвентаризации нужно игнорировать заказы возвращенные в статус создан, т.к. они уже могут быть собраны, да и в целом они не должны влиять на закрытое кол-во"
  (let [supplier (make:supplier :stock-num 3
                                :stock (fn[i]
                                         {:stock-variant {{:size :s} 20}
                                          :params {:size [:s :m]}}))
        [stock1 stock2] (map :id (sup-stocks supplier))
        user (make:user)]

    (wrap-supplier
     supplier

     ;; initial arrival
     (make:arrival supplier :parameters [{:size "m"}] :count 5)

     (let [order1
           (testing "создаем заказ в статусе 'создан' в процессе инвентаризации"
             (wrap-security
              (make:user)
              (retail:make-order [{:fkey_stock stock1 :size :m :quantity 2}])))

           order2
           (testing "создаем заказ, и переводим в статус - обработка. кол-во будет помечено как занятое в снапшоте"
             (wrap-security
              user
              (let [order (retail:make-order [{:fkey_stock stock1 :size :m :quantity 3}])]
                (retail:set-order-status order :in-work)
                order)))

           inventory (inventory:create-inventory "hello" supplier :inventory)]

       (testing "делаем дозаказ в первый заказ, он переходит в статус 'создан'. Любой другой товар"
         (wrap-security
          user
          (retail:make-order [{:fkey_stock stock2 :size :m :quantity 3}])))

       (testing "закрываем инвентаризацию. Она может закрываться только с кол-вом [-2,+inf], т.к. в снапшоте стоит 2"

         (is
          (thrown-with-msg?
           Exception #"Can't set less than in snapshot."
           (arrival:save-multiple-stock-arrivals inventory {stock1 [{:parameters {:size "m"} :quantity -5}]})))

         (arrival:save-multiple-stock-arrivals inventory {stock1 [{:parameters {:size "m"} :quantity -2}]})
         (inventory:close-inventory inventory))

       (testing "должен закрыться заказ order1, не смотря на то что order2 был
       создан позже. order2 - не должен быть тронут, т.к. на момент создани
       инвентаризации не был открыт"
         (is (= [2] (map :closed (retail-order-items order1))))
         (is (= [0 0] (map :closed (retail-order-items order2)))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Runners

(run-model-tests)
