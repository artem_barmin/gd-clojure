(ns gd.model.inplace_edit_test
  (:refer-clojure :exclude [extend])
  (:use gd.views.admin.stock-model)
  (:use gd.model.test.data_builders)
  (:use gd.utils.logger)

  (:use clojure.test)
  (:use faker.company)
  (:use clj-time.core)

  (:use clojure.data)

  (:use gd.model.model)
  (:use gd.model.context)
  (:use gd.parsers.llapi)

  (:use korma.core)

  (:use gd.utils.common)
  (:use gd.utils.db)
  (:use gd.utils.test)
  (:use [clojure.algo.generic.functor :only (fmap)])

  (:require
   [noir.session :as session]
   gd.parsers.hlapi
   [clj-time.coerce :as tm]
   [gd.utils.security :as secur]
   [ring.middleware.session.store :as ringstore]
   [monger.ring.session-store :as store]))

;; TODO : add check that javascript generates correct stock model

(defspec inplace-list-fetch-test
  (let [supplier (make:supplier
                  :stock-num 1
                  :stock (constantly
                          {:price 10
                           :stock-variant {{:size :s} 1}
                           :params {:size [:s :m]}
                           :context {:wholesale {:price 20}
                                     :wholesale-sale {:price 30}}}))
        stored-stock
        (fn[] (first (inplace:all-stocks 1 0 {:inline-state "all"})))]
    (testing "retail context info is copied from wholesale"
      (binding [session/*noir-session* (atom nil)
                *current-supplier* supplier]
        (wrap-context [:retail :wholesale :wholesale-sale :retail-sale]
          (is (= {"size" ["m" "s"]} (:params (stored-stock)))))))))

(defspec inplace-list-empty-context-fetch-test
  (let [supplier (make:supplier
                  :stock-num 1
                  :stock (constantly
                          {:price 10
                           :stock-variant {{:size :s} 1}
                           :params {:size [:s :m]}}))
        stored-stock
        (fn[]
          (first (inplace:all-stocks 1 0 {:inline-state "all"})))]
    (binding [session/*noir-session* (atom nil)
              *current-supplier* supplier]
      (wrap-context [:retail :wholesale :wholesale-sale :retail-sale]
        (is (= {"size" ["m" "s"]} (:all-params (stored-stock))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Runners

(run-model-tests)
