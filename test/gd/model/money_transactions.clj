(ns gd.model.money_transactions
  (:refer-clojure :exclude [extend])
  (:use gd.model.test.data_builders)
  (:use gd.utils.logger)

  (:use clojure.test)
  (:use faker.company)
  (:use clj-time.core)

  (:use clojure.data)

  (:use gd.model.model)
  (:use gd.parsers.llapi)

  (:use korma.core)

  (:use gd.utils.common)
  (:use gd.utils.db)
  (:use gd.utils.web)
  (:use gd.utils.test)
  (:require [gd.utils.security :as sec]))

(defspec basic-money-transactions
  (let [root (make:root)
        user (make:user)
        hacker (make:user)]
    (is (thrown-with-msg? Exception #"Security violation"
          (account:transact-money user 10 "hello")))

    (is (thrown-with-msg? Exception #"Security violation"
          (wrap-security hacker
            (account:transact-money user 10 "hello"))))

    (is (wrap-security root
          (account:transact-money user 10 "hello")))

    (testing "empty comments for transactions is not allowed"
      (is (thrown-with-msg? AssertionError #"Assert failed: \(seq comment\)"
            (wrap-security root
              (account:transact-money user 10 "")))))

    (testing "zero amount transaction is not allowed"
      (is (thrown-with-msg? AssertionError #"Assert failed: \(not= amount 0\)"
            (wrap-security root
              (account:transact-money user 0 "hello")))))))

(run-model-tests)
