(ns gd.model.user_test
  (:refer-clojure :exclude [extend])
  (:use gd.model.test.data_builders)
  (:use gd.utils.logger)

  (:use clojure.test)
  (:use faker.company)
  (:use clj-time.core)

  (:use clojure.data)

  (:use gd.model.model)
  (:use gd.parsers.llapi)

  (:use korma.core)

  (:use gd.utils.common)
  (:use gd.utils.db)
  (:use gd.utils.test)
  (:use gd.utils.web)

  (:require
   gd.parsers.hlapi
   [clj-time.coerce :as tm]
   [gd.utils.security :as secur]
   [ring.middleware.session.store :as ringstore]
   [monger.ring.session-store :as store]))

(defspec user-with-roles
  (testing "roles saving and update"
    (let [id (make:user :role ["moderator" "organizer"])]
      (is (= (:role (user:get-full id)) ["moderator" "organizer"]))

      (user:update id {:role ["user" "organizer"]})

      (is (= (:role (user:get-full id)) ["organizer" "user"]))))

  (testing "second user roles saving"
    (let [id (make:user :role ["organizer" "user"])]
      (is (= (:role (user:get-full id)) ["organizer" "user"])))))

(defspec roles-checking
  (let [user (make:user :role ["user" "organizer"])]
    (wrap-security user
      (is (secur/organizer?))
      (is (not (secur/root?)))))

  (testing "root checking"
    (let [user (make:user :auth-profile [{:type :internal :login "admin" :password "root"}]
                          :role ["user" "organizer"])]
      (wrap-security user
        (is (secur/organizer?))
        (is (secur/root?))))))

(defspec user-internal-auth-test
  (make:user-internal "artem" "barmin")
  (let [user1-fetched (user:check-credentials "artem" "barmin")
        not-found (user:check-credentials "blah" "blah")]

    (is (:images user1-fetched))
    (is (:auth-profile user1-fetched))
    (is (:contact user1-fetched))

    (is (nil? not-found))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Invites

(defspec invite-extrernal-registration
  (let [user (make:user)
        uuid "1"
        uuid2 "2"]

    (wrap-security user

      (invite:initial-generation 2)

      (testing "capture first and use it for external authorization"
        (let [{:keys [code]} (invite:capture)]

          (is (:id (invite:use-external code (user:authorize-external uuid :vk {}))))

          (testing "second usage of the same code for same user is allowed"
            (is (:id (invite:use-external code (user:authorize-external uuid :vk {})))))

          (testing "second usage of the same code for other user is forbidden"
            (is (not (invite:use-external code (user:authorize-external uuid2 :vk {})))))))

      (testing "attempt to enter the site without previously activated social account - should be allowed"
        (is (invite:use-external nil (user:authorize-external uuid2 :vk {}))))

      (testing "attempt to use existing but uncaptured code for authrorization"
        (let [[{:keys [code]}] (select invite (where {:invited nil}))]
          (is (not (invite:use-external code (user:authorize-external uuid2 :vk {}))))))

      (testing "attempt to use empty invite - should be allowed"
        (is (invite:use-external "" (user:authorize-external uuid2 :vk {})))))))

(defspec invite-internal-registration
  (let [user (make:user)
        uuid "1"
        uuid2 "2"]

    (wrap-security user

      (invite:initial-generation 2)

      (testing "capture first and use it for external authorization"
        (let [{:keys [code]} (invite:capture)]
          (is (:id (invite:use-internal code "artem" "password" "a@a.com")))))

      (testing "invalid code can't be used for authorization"
        (let [[{:keys [code]}] (select invite (where {:invited nil}))]
          (is (thrown-with-msg? Exception #"Code are already in use or invalid"
                (invite:use-internal code "artem1" "password" "a@a.com")))))

      (testing "empty code can be used"
        (is (:id (invite:use-internal nil "artem2" "password" "a@a.com")))
        (is (:id (invite:use-internal "" "artem3" "password" "a@a.com")))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; External auth test

(defspec user-external-auth-test
  (let [uuid "1"
        uuid2 "2053"

        durov-auth
        (user:authorize-external
         uuid
         :vk
         {:real-name "Durov" :sex :male})

        durov-second-auth
        (user:authorize-external uuid :vk {})

        durov-third-auth-with-update
        (user:authorize-external uuid :vk
                                 {:real-name "Artem"
                                  :sex :female})

        kristina-with-photo
        (user:authorize-external uuid2 :vk {:real-name "Kristina"})

        get-uuid
        (fn[user]
          (:uuid (first (:auth-profile user))))]

    (testing "check that info is fetched correctly"
      (is (= durov-auth durov-second-auth)))

    (testing "some explicit checks"
      (is (= (select-keys durov-auth [:real-name :sex])
             {:real-name "Durov" :sex :male})))

    (testing "check after update"
      (is (= (select-keys durov-third-auth-with-update [:real-name :sex])
             {:real-name "Artem" :sex :female}))
      (is (= (:type (first (:auth-profile durov-second-auth))) :vk)))

    (testing "check that in all situations id and uid of users are the same"
      (is (not (nil? durov-third-auth-with-update)))
      (is (= (:id durov-third-auth-with-update)
             (:id durov-auth)
             (:id durov-second-auth)))
      (is (= uuid
             (get-uuid durov-third-auth-with-update)
             (get-uuid durov-auth)
             (get-uuid durov-second-auth))))

    (testing "check that authorization thought different UUID create other user"
      (is (:id kristina-with-photo))
      (is (not= (:id kristina-with-photo) (:id durov-auth)))
      (is (not= (get-uuid kristina-with-photo) (get-uuid durov-auth))))))

(defspec user-external-keep-data-test
  (let [uuid "1"

        durov-auth
        (user:authorize-external uuid :vk {:real-name "Durov"})]

    (When (user:update (:id durov-auth) {:sex :male}))

    (is (= :male (:sex (user:authorize-external uuid :vk {:real-name "Durov" :sex nil}))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; User contacts

(defspec user-emails-contacts-test
  (make:user-internal "artem" "barmin")
  (let [hacker (make:user)
        user-full #(user:check-credentials "artem" "barmin")
        user #(:id (user-full))
        contacts #(:contact (user-full))
        new-contacts #(:new-contact (user-full))
        contacts-raw #(reverse (sort-by :created (:contact-raw (user-full))))
        emails (fn[](filter #(= :email (:type %)) (contacts-raw)))]

    ;; 1. все контакты не активные
    (is (= (:value (:phone (contacts))) "+380509790587"))
    (is (= (:value (:email (contacts))) "c@c.com"))
    (is (= (contacts) (new-contacts)))
    (is (every? (comp not :active second) (contacts)))

    ;; 2. обновляем контакты - они остаются неактивными + соблюдаются
    ;; инварианты(количество контактов одного типа не изменяется)
    ;;
    ;; на почту уходит письмо - с ссылкой на активацию(проверяем код)
    ;; TODO : add email check
    (user:update (user) {:contact {:email "barmin.artem@gmail.com"}})
    (is (= (count (emails)) 1))

    ;; 3. активируем почту - проверяем что она стала активной, проверяем
    ;; инварианты
    (user:activate-contact-by-code (:activation-code (first (emails))) :email)
    (is (= (:value (:email (contacts))) "barmin.artem@gmail.com"))
    (is (empty? (:email (new-contacts))))
    (is (:active (:email (contacts))))

    ;; 4. меняем почту - проверяем что теперь есть 2 контакта почты(1 -
    ;; активный, 1 - неактивный), проверяем что в отображение пользователя
    ;; попадает - активный контакт
    (user:update (user) {:contact {:email "hello.world@gmail.com"}})
    (is (= (count (emails)) 2))
    (is (:email (new-contacts)))
    (is (= (:value (:email (contacts))) "barmin.artem@gmail.com"))

    ;; 5. активируем контакт - прошлый email становится неактивным, текущий
    ;; становится активным - проверяем что в отображение пользвоателю попал
    ;; активный контакт, и в списке только один активный контакт
    (user:activate-contact-by-code
     (:activation-code (first (filter #(and (= :email (:type %))
                                            (not (:active %))) (contacts-raw))))
     :email)
    (is (= (count (emails)) 2))
    (is (= (count (filter :active (emails))) 1))
    (is (empty? (:email (new-contacts))))
    (is (= (:value (:email (contacts))) "hello.world@gmail.com"))

    ;; 6. проверка того что неактивные контакты не накапливаются - после
    ;; нескольких изменений, имеется всего один неактивный контакт после
    ;; последнего + каждый раз меняются коды, так что старым кодом нельзя
    ;; активировать новый контакт
    (user:update (user) {:contact {:email "1@gmail.com"}})
    (is (= (count (emails)) 3))
    (is (= (count (filter :active (emails))) 1))
    (is (:email (new-contacts)))
    (is (= (:value (first (emails))) "1@gmail.com"))
    (let [code1 (:activation-code (first (emails)))]
      (user:update (user) {:contact {:email "2@gmail.com"}})
      (is (= (count (emails)) 3))
      (is (= (:value (first (emails))) "2@gmail.com"))
      (is (:email (new-contacts)))
      (is (not= (:activation-code (first (emails))) code1)))

    ;; 7. проверка того что нельзя активировать контакт неправильным кодом,
    ;; причем это можно сделать без пользователя
    (is
     (thrown-with-msg? Exception #"Code not valid"
       (user:activate-contact-by-code "bad bad code" :email)))

    ;; ... а правильным можно
    (user:activate-contact-by-code (:activation-code (first (emails))) :email)
    (is (= (:active (first (emails))) true))))

;; TODO : сделать тесты параметризироваными, и прогонять для каждого типа контакта(phone и email)
(defspec user-phones-contacts-test
  (make:user-internal "artem" "barmin")
  (let [hacker (make:user)
        user-full #(user:check-credentials "artem" "barmin")
        user #(:id (user-full))
        contacts #(:contact (user-full))
        new-contacts #(:new-contact (user-full))
        contacts-raw #(reverse (sort-by :created (:contact-raw (user-full))))
        phones (fn[](filter #(= :phone (:type %)) (contacts-raw)))]

    ;; 1. все контакты не активные
    (is (= (:value (:phone (contacts))) "+380509790587"))
    (is (= (:value (:email (contacts))) "c@c.com"))
    (is (= (contacts) (new-contacts)))
    (is (every? (comp not :active second) (contacts)))

    ;; 2. проверка того что не может активировать хакер
    (is
     (thrown-with-msg? Exception #"Code not valid"
       (wrap-security hacker
         (user:activate-contact-by-code (:activation-code (first (phones))) :phone))))

    ;; ... а владелец может
    (wrap-security (user)
      (user:activate-contact-by-code (:activation-code (first (phones))) :phone)
      (is (= (:active (first (phones))) true)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Runners

(run-model-tests)
