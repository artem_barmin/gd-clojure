(ns leiningen.junit
  (:use [leiningen.compile :only [eval-in-project]]
        [clojure.contrib.find-namespaces :only [find-namespaces-in-dir]]
        [clojure.java.io :only [file]]))

(defn load-system-properties[file]
  `(with-open [file-stream# (java.io.FileReader. ~file)]
     (System/setProperties
      (doto (new java.util.Properties (System/getProperties))
        (.load file-stream#)))))

(defn require-all-test-namespaces
  "returns a form that when eval'd, requires all test namespaces"
  [project]
  `(do
     ~@(for [ns (find-namespaces-in-dir (file (:test-path project)))]
         `(require (quote ~ns)))))

(defn require-clojure-test-form []
  `(try
     (require 'clojure.test)
     (require 'clojure.test.junit)
     (require 'gd.utils.test)
     (catch Throwable e#
       (.printStackTrace e#)
       (System/exit 1))))

(defn run-form [project]
  `(do
     (with-open [file-stream# (java.io.FileWriter. "testreports.xml")]
       (binding [clojure.test/*test-out* file-stream#]
         (clojure.test.junit/with-junit-output
           (try
             ~(require-all-test-namespaces project)
             (catch Throwable e#
               (clojure.test/is false (format "Uncaught exception: %s" e#))
               (clojure.test/with-test-out (println "</testsuites>"))
               (System/exit 1))))))
     (System/exit 0)))

(defn junit
  [project]
  (let [forms [(load-system-properties "test/resources/test.properties")
               (require-clojure-test-form)
               (run-form project)]]
    (eval-in-project
     project
     nil
     (fn [java]
       (doseq [form forms]
         (.setValue (.createArg java) "-e")
         (.setValue (.createArg java) (prn-str form)))))))
