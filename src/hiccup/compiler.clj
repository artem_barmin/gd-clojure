(ns hiccup.compiler
  "Internal functions for compilation."
  (:use hiccup.util
        clojure.test)
  (:import [clojure.lang IPersistentVector ISeq IDeref]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Out buffer processing
(def ^:dynamic *output-buffer* nil)
(def ^:dynamic *time* (atom 0))
(def ^:dynamic *count* (atom 0))
(def ^:dynamic *count-ops* (atom {}))
(def debug-time false)

(def ^:dynamic *html-mode* :xml)

(defmacro check-time
  [expr]
  (if debug-time
    `(let [start# (. System (nanoTime))
           ret# ~expr]
       (swap! *time* + (- (. System (nanoTime)) start#))
       (swap! *count* inc)
       ret#)
    expr))

(defmacro mark-ops
  [op]
  (when debug-time
    `(swap! *count-ops* update-in [~op] (fn[old#] (if old#
                                                    (inc old#)
                                                    1)))))

(defmacro with-out-buffer[& body]
  `(binding [*output-buffer* (new StringBuilder)]
     ~@body
     (.toString ^StringBuilder *output-buffer*)))

(defmacro with-prealocated-out-buffer[length length-fn & body]
  `(binding [*output-buffer* (new StringBuilder ~length)
             *time* (atom 0)
             *count* (atom 0)
             *count-ops* (atom {})]
     ~@body
     (~length-fn (.length ^StringBuilder *output-buffer*))
     (when debug-time
       (prn "Elapsed time:" (double (/ @*time* 1000000)))
       (prn "Elapsed counts:" @*count*)
       (prn "Ops counts:" @*count-ops*))
     (.toString ^StringBuilder *output-buffer*)))

(definline out[^String str]
  `(check-time
    (do (.append ^StringBuilder *output-buffer* ^String ~str) nil)))

(definline out-safe[^String str]
  `(check-time
    (let [str# ~str]
      (if str#
        (.append ^StringBuilder *output-buffer* ^String str#))
      nil)))

(definline out-safe-number[str]
  `(check-time
    (let [str# ~str]
      (if str#
        (.append ^StringBuilder *output-buffer* str#))
      nil)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Rendering

(def names-cache (new java.util.concurrent.ConcurrentHashMap 5000))
(def values-cache (new java.util.concurrent.ConcurrentHashMap 5000))

(extend-protocol ToString
  java.lang.String
  (to-str [k] k))

(extend-protocol ToString
  clojure.lang.LazySeq
  (to-str [k] (apply str k)))

(extend-protocol ToString
  clojure.lang.PersistentList
  (to-str [k] (apply str k)))

(defn single-replace[^StringBuilder str ^String from ^String to]
  (loop [start 0]
    (let [index (.indexOf str from start)]
      (when (not= index -1)
        (.replace str index (+ index (.length from)) to)
        (recur (+ index (.length to))))))
  str)

(defn str-replace[str replacement-map]
  (let [str (new StringBuilder str)]
    (doseq [[from to] replacement-map]
      (single-replace str from to))
    str))

(defn escape-html-special
  "Change special characters into HTML character entities."
  [text]
  (mark-ops :escape-html)
  (let [text (to-str text)]
    (if (and (= -1 (.indexOf text "<"))
             (= -1 (.indexOf text ">"))
             (= -1 (.indexOf text "&"))
             (= -1 (.indexOf text "\""))
             (= -1 (.indexOf text "\n")))
      text
      (or
       (.get ^java.util.concurrent.ConcurrentHashMap values-cache ^String text)
       (let [result (str-replace text
                                 [["&" "&amp;"]
                                  ["<" "&lt;"]
                                  [">" "&gt;"]
                                  ["\"" "&quot;"]
                                  ["\n" " "]])]
         (.put ^java.util.concurrent.ConcurrentHashMap
               values-cache ^String text result)
         result)))))

(defn normalize-attr-name[name]
  (or
   (.get ^java.util.concurrent.ConcurrentHashMap names-cache ^Object name)
   (let [name (to-str name)
         result (.intern (str " " name "=\""))]
     (.put ^java.util.concurrent.ConcurrentHashMap names-cache ^Object name result)
     result)))

(defn render-attribute [[name value]]
  (mark-ops :attribute)
  (cond
    (not value)
    (out "")

    (true? value)
    (do
      (out (normalize-attr-name name))
      (out (to-str name))
      (out "\""))

    (delay? value)
    (do
      (out (normalize-attr-name name))
      (out @value)
      (out "\""))

    :else
    (do
      (out (normalize-attr-name name))
      (out (escape-html-special value))
      (out "\""))))

(defn- render-attr-map [attrs]
  (doseq [attr attrs]
    (render-attribute attr)))

(def ^{:doc "Regular expression that parses a CSS-style id and class from an element name."
       :private true}
  re-tag #"([^\s\.#]+)(?:#([^\s\.#]+))?(?:\.([^\s#]+))?")

(def tags-cache (new java.util.concurrent.ConcurrentHashMap 5000))

(defn- normalize-tag[^Object tag]
  (or
   (.get ^java.util.concurrent.ConcurrentHashMap tags-cache ^Object tag)
   (let [[_ tag-result id class] (re-matches re-tag (as-str tag))
         tag-attrs        (merge
                           (when id {:id id})
                           (when class {:class (.intern (.replace ^String class "." " "))}))
         result [(.intern (str "<" tag-result)) tag-attrs (.intern (str "</" tag-result ">"))]]
     (.put ^java.util.concurrent.ConcurrentHashMap tags-cache ^String tag result)
     result)))

(defn merge-attributes-with-tags[tag-attrs map-attrs]
  (if (and (contains? tag-attrs :class) (contains? map-attrs :class))
    (merge tag-attrs map-attrs {:class (str (to-str (:class tag-attrs)) " " (to-str (:class map-attrs)))})
    (merge tag-attrs map-attrs)))

(defn normalize-element
  "Ensure an element vector is of the form [tag-name attrs content]."
  [[tag & content]]
  (let [[tag-start tag-attrs tag-end] (normalize-tag tag)
        map-attrs (first content)]
    (if (map? map-attrs)
      [tag-start (merge-attributes-with-tags tag-attrs map-attrs) tag-end (next content)]
      [tag-start tag-attrs tag-end content])))

(defprotocol HtmlRenderer
  (render-html [this]
    "Turn a Clojure data type into a string of HTML."))

(extend-protocol HtmlRenderer
  IPersistentVector
  (render-html [element]
    (let [[tag-start attrs tag-end content] (normalize-element element)]
      (mark-ops :render-tag)
      (out tag-start)
      (render-attr-map attrs)
      (out ">")
      (render-html content)
      (out tag-end)))
  ISeq
  (render-html [this]
    (doseq [el this] (render-html el)))
  IDeref
  (render-html [this]
    (render-html @this))
  Object
  (render-html [this]
    (out this))
  nil
  (render-html [this]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Precompiler

(defn- unevaluated?
  "True if the expression has not been evaluated."
  [expr]
  (or (symbol? expr)
      (and (seq? expr)
           (not= (first expr) `quote))))

(defn compile-attr-map
  "Returns an unevaluated form that will render the supplied map as HTML
  attributes."
  [attrs]
  `(do
     ~@(map (fn[[k v]]
              (let [renderable? (fn[v] (or (keyword? v) (string? v)))]
                (cond
                  (and (renderable? k) (renderable? v))
                  `(out ~(str " " (to-str k) "=\"" (to-str v) "\""))

                  true
                `(render-attribute [~k ~v]))))
            attrs)))

(defn- form-name
  "Get the name of the supplied form."
  [form]
  (if (and (seq? form) (symbol? (first form)))
    (name (first form))))

(declare compile-seq)

(defmulti compile-form
  "Pre-compile certain standard forms, where possible."
  {:private true}
  form-name)

(defmethod compile-form "if"
  [[_ condition & body]]
  `(if ~condition ~@(compile-seq body)))

(defmethod compile-form "let"
  [[_ vars & body]]
  `(let ~vars ~@(compile-seq body)))

(defmethod compile-form "with-group"
  [[_ group & body]]
  `(hiccup.form/with-group ~group ~@(compile-seq body)))

(defmethod compile-form :default
  [expr]
  `(render-html ~expr))

(defn- not-hint?
  "True if x is not hinted to be the supplied type."
  [x type]
  (if-let [hint (-> x meta :tag)]
    (not (isa? (eval hint) type))))

(defn- hint?
  "True if x is hinted to be the supplied type."
  [x type]
  (if-let [hint (-> x meta :tag)]
    (isa? (eval hint) type)))

(defn- literal?
  "True if x is a literal value that can be rendered as-is."
  [x]
  (and (not (unevaluated? x))
       (or (not (or (vector? x) (map? x)))
           (every? literal? x))))

(defn- not-implicit-map?
  "True if we can infer that x is not a map."
  [x]
  (or (= (form-name x) "for")
      (not (unevaluated? x))
      (not-hint? x java.util.Map)))

(defn- element-compile-strategy
  "Returns the compilation strategy to use for a given element."
  [[tag attrs & content :as element]]
  (cond
    (every? literal? element)
    ::all-literal                    ; e.g. [:span "foo"]

    (and (literal? tag) (map? attrs) (every? (fn[[k v]] (and (literal? k) (literal? v))) attrs))
    ::literal-tag-and-attributes     ; e.g. [:span {} x]

    (and (literal? tag) (not (map? attrs)) (not-implicit-map? attrs))
    ::literal-tag-and-no-attributes  ; e.g. [:span ^String x]

    (literal? tag)
    ::literal-tag                    ; e.g. [:span x]

    :else
    ::default))                      ; e.g. [x]

(declare compile-seq)

(defmulti compile-element
  "Returns an unevaluated form that will render the supplied vector as a HTML
  element."
  {:private true}
  element-compile-strategy)

(defmethod compile-element ::all-literal
  [element]
  (let [result (with-out-buffer (render-html (eval element)))]
    `(out ~result)))

(defmethod compile-element ::literal-tag-and-attributes
  [[tag attrs & content]]
  (let [[tag-start attrs tag-end _] (normalize-element [tag attrs])]
    `(do
       (out ~tag-start)
       ~(compile-attr-map attrs)
       (out ">")
       ~@(compile-seq content)
       (out ~tag-end))))

(defmethod compile-element ::literal-tag-and-no-attributes
  [[tag & content]]
  (compile-element (apply vector tag {} content)))

(defmethod compile-element ::literal-tag
  [[tag attrs & content]]
  (let [[tag-start tag-attrs tag-end  _] (normalize-element [tag])
        attrs-sym         (gensym "attrs")]
    `(let [~attrs-sym ~attrs]
       (if (map? ~attrs-sym)
         ~`(do
             (out ~tag-start)
             (#'render-attr-map ~(if (nil? tag-attrs)
                                   `~attrs-sym
                                   `(merge-attributes-with-tags ~tag-attrs ~attrs-sym)))
             (out ">")
             ~@(compile-seq content)
             (out ~tag-end))
         ~`(do
             (out ~(str tag-start (with-out-buffer (render-attr-map tag-attrs)) ">"))
             ~@(compile-seq (cons attrs-sym content))
             (out ~tag-end))))))

(defmethod compile-element :default
  [element]
  `(render-html
    [~(first element)
     ~@(for [x (rest element)]
         (if (vector? x)
           (compile-element x)
           x))]))

(defn compile-seq
  "Compile a sequence of data-structures into HTML."
  [content]
  (for [expr content]
    (cond
      (vector? expr) (compile-element expr)
      (string? expr) `(out ~expr)
      (literal? expr) `(out-safe ~expr)
      (hint? expr String) `(out-safe ~expr)
      (hint? expr Number) `(out-safe-number ~expr)
      (seq? expr) (compile-form expr)
      :else `(render-html ~expr))))

(defn splice-all-do-inside-other-do[expr]
  (let [expr-do? (fn[expr](and (seq? expr) (= 'do (first expr))))]
    (clojure.walk/postwalk
     (fn[expr]
       (cond (expr-do? expr)
             (mapcat (fn[expr-part]
                       (if (expr-do? expr-part)
                         (rest expr-part)
                         [expr-part]))
                     expr)

             true
             expr))
     expr)))

(defn concat-all-sequential-outs[expr]
  (let [expr-out?
        (fn[expr](and (seq? expr) (= 'hiccup.compiler/out (first expr))))]
    (clojure.walk/postwalk
     (fn[expr]
       (cond (and (seq? expr) (= 'do (first expr)))
             (mapcat (fn[expr-part]
                       (if (expr-out? (first expr-part))
                         [`(out ~(clojure.string/join "" (map second expr-part)))]
                         expr-part))
                     (partition-by expr-out? expr))

             true
             expr))
     expr)))

(defn optimize-sequential-outs[expr]
  (concat-all-sequential-outs (splice-all-do-inside-other-do expr)))

(defn compile-html
  "Pre-compile data structures into HTML where possible."
  [& content]
  (optimize-sequential-outs `(with-out-buffer ~@(compile-seq content))))

(defn compile-external-html
  "Pre-compile data structures into HTML where possible."
  [size length-fn & content]
  (optimize-sequential-outs
   `(with-prealocated-out-buffer ~size ~length-fn ~@(compile-seq content))))

(deftest optimization-tests
  (is (=
       `(do (hiccup.compiler/out "ab") 1 (hiccup.compiler/out "c"))
       (optimize-sequential-outs `(do (out "a")
                                      (out "b")
                                      1
                                      (out "c")))))

  (is (=
       `(do (hiccup.compiler/out "abc")
            (if (clojure.core/= 1 1)
              (clojure.core/+ 1 2)
              (do (clojure.core/+ 1 2)
                  (hiccup.compiler/out "a1a2"))) (hiccup.compiler/out "d"))
       (optimize-sequential-outs `(do (out "a")
                                      (out "b")
                                      (do (out "c")
                                          (if (= 1 1)
                                            (+ 1 2)
                                            (do (+ 1 2)
                                                (out "a1")
                                                (do (out "a2"))))
                                          (out "d"))))))

  (is (= `(do (hiccup.compiler/out "ab")
              (clojure.core/let
                  [hiccup.compiler/a
                   (do (hiccup.compiler/out "c")
                       (if (clojure.core/= 1 1)
                         (clojure.core/+ 1 2)
                         (do (clojure.core/+ 1 2)
                             (hiccup.compiler/out "a1a2")))
                       (hiccup.compiler/out "d"))]
                hiccup.compiler/a))
         (optimize-sequential-outs `(do (out "a")
                                        (out "b")
                                        (let [a (do (out "c")
                                                    (if (= 1 1)
                                                      (+ 1 2)
                                                      (do (+ 1 2)
                                                          (out "a1")
                                                          (do (out "a2"))))
                                                    (out "d"))]
                                          a))))))

(run-tests)
