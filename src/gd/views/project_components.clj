(ns gd.views.project_components
  (:use
   gd.views.seo.meta
   clojure.pprint
   compojure.core
   gd.model.model
   gd.utils.web
   noir.request
   gd.views.messages
   gd.utils.common
   gd.views.components
   noir.core
   hiccup.core
   com.reasonr.scriptjure
   hiccup.page
   hiccup.form
   hiccup.element
   net.cgrand.enlive-html)
  (:require
   [gd.views.security :as security]
   [clojure.string :as strings]
   [noir.validation :as validation]
   [noir.session :as session]
   [clojure.set :as sets]))

(comment "Components that can be used in different parts of project, but contains
domain specific logic. They can't be regarded as common components.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stocks components

(defn render-params[{:keys [params] :as order-item}]
  (map (fn[[key value]]
         [:div
          [:span (! :stock.parameter key)] ": "
          value])
       params))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Phone activation modal dialog
(defn phone-activation-state-for-user[user]
  (let [value (get-in user [:contact :phone :value])
        new-value (get-in user [:new-contact :phone :value])
        active (get-in user [:contact :phone :active])]
    (cond (not value) :no-number-at-all
          (not active) :have-not-active
          (not new-value) :have-active
          true :have-new-number)))

(defn phone-activation-state[request-for-change]
  (let [user (security/logged)
        value (get-in user [:contact :phone :value])
        new-value (get-in user [:new-contact :phone :value])
        active (get-in user [:contact :phone :active])]
    [(if request-for-change :request-for-new-number-change (phone-activation-state-for-user user))
     value new-value active]))

(defn activation-icon[state]
  [:div {:style "display:inline-block;margin-top:-3px;height:25px;"}
   (if state
     (image {:style "margin-left: 8px;
                     margin-top: 5px;
                     vertical-align: bottom;"}
            "/img/activation/active.png")

     (image {:style "margin-left: 8px;
                     margin-top: 5px;
                     vertical-align: bottom;"}
            "/img/activation/not-active.png"))])

;; Map to make possible passing different callbacks to phone activation modal
(def phone-action-callbacks (atom nil))

(defn phone-activation-modal[& {:keys [header body on-activate]}]
  (let [type :phone
        [state value new-value active] (phone-activation-state nil)
        js-activate-hash (.hashCode (cljs on-activate))]
    (swap! phone-action-callbacks assoc js-activate-hash on-activate)
    (modal-window-with-header
      (or
       header
       (case state
         :no-number-at-all "Добавление номера"
         :have-active "Изменение номера"
         "Активация номера")) {:width 600}
         (multi-region*
             contact-activation-region js-activate-hash [change]
           (let [[state value new-value active] (phone-activation-state (c* change))
                 activate-hash (s* js-activate-hash)]
             [:div.form {:style "text-align:center;"}
              ;; :no-number-at-all - нет вобще телефона(начальное состояние)
              ;; :have-not-active - есть номер, но не активный
              ;; :have-active - есть активный
              ;; :have-new-number - есть активный, но есть еще и новый номер
              ;; :request-for-new-number-change - есть активный, но есть еще и новый номер + есть запрос на изменение нового на client side

              (when body
                [:div.justify {:style "border:1px solid; background-color:#6186AD; padding:15px;color:white;"} body])

              ;; change part
              (case state
                (:no-number-at-all :have-active :request-for-new-number-change)
                [:div {:style "margin:20px 20px 0 20px;"}
                 [:div {:style "margin-bottom:5px;"} "Мобильный телефон"]
                 (text-field {:class "fancy-input-text center"} :new-value)
                 [:div {:style "margin-left: auto; margin-right: auto; margin-top: 10px; height: 35px; width: 110px;"}
                  (small-blue-button "Получить код"
                                     :action (execute-form* update-contact [new-value]
                                                            (do
                                                              (user:update (:id (security/logged)) {:contact {*type new-value}} )
                                                              (security/update-logged-information))
                                                            :success (multi-rerender :contact-activation-region activate-hash)
                                                            :validate [(textarea-phone "new-value" "Номер в формате 0xxxxxxxxx")])
                                     :width 100)]
                 (if (= state :request-for-new-number-change)
                   [:div {:style "width: 110px; margin-left: auto; margin-right: auto; height: 35px;"}
                    (small-grey-button "Отмена" :action (cljs (multi-rerender :contact-activation-region activate-hash)) :width 100)])]

                (:have-not-active :have-new-number)
                [:div {:style "margin:10px 20px 10px 20px;"}
                 [:div {:style "margin-bottom: 5px;"} "Мобильный телефон"]
                 [:div.fancy-input-text.user-profile-input-contact.inline-block new-value]
                 [:span.right.link {:onclick (cljs (multi-rerender :contact-activation-region activate-hash true)) :style "position:absolute;font-size:12px;margin-left:20px;margin-top:5px;"}
                  "Изменить номер"]])

              ;; activation part
              (case state
                (:have-not-active :have-new-number)
                [:div.contact-activation {:style "margin:10px 20px 20px 20px;"}
                 [:div {:style "margin-bottom: 5px;"} "Код подтверждения *"]
                 (text-field {:class "fancy-input-text" :style "margin-bottom: 10px; text-align: center;"
                              :data-title (tooltip "На Ваш телефон в течение минуты придет <b>4</b>-значный код<br/>Пример кода : 0751")} :code)
                 [:div.center {:style "width:150px; "}
                  (small-blue-button "Отправить код"
                                     :action (execute-form* activate-by-code [code]
                                                            (do
                                                              (user:activate-contact-by-code code *type)
                                                              (security/update-logged-information)
                                                              (session/put! :phone-activated true))
                                                            :success (or (get @phone-action-callbacks activate-hash) (reload))
                                                            :error (js* (showValidation "Не правильный код" ($ "[name=code]:visible")))
                                                            :validate [(textarea-regexp "code" #"[0-9]{4}" "Код состоит из 4 цифр")])
                                     :width 150)]] nil)

              ;; send one more time
              (case state
                (:have-not-active :have-new-number)
                [:div.user-profile-contacts-warning {:style "margin:20px 20px 0 20px;"}
                 "*Если вы не получили смс - нажмите кнопку, и мы вышлем вам еще одно"
                 [:div.center {:style "width:150px; font-size:15px; padding-bottom: 15px;"}
                  (small-blue-button "Получить код еще раз"
                                     :action (execute-form* send-new-activation-code []
                                                            (user:send-new-code (:id (security/logged))))
                                     :width 150)]]
                nil)

              [:div.clear]])))))

(defn phone-activation[]
  (let [[state value new-value active] (phone-activation-state nil)]
    [:div.user-profile-info-row
     [:div.user-profile-info-key (! :user.profile.contact-phone)]

     ;; current value and status
     [:div.fancy-input-text.user-profile-input-contact.left (or value "не указан")]
     [:span {:data-title (tooltip (if active "Номер активирован" "Номер не активирован"))} (activation-icon active)]

     ;; request for activation or change
     [:div.phone-activation-button.right (small-blue-button (case state
                                                              :no-number-at-all "Добавить номер"
                                                              :have-active "Изменить номер"
                                                              "Активировать")
                                                            :action (modal-window-open true) :width 150)
      (phone-activation-modal)]]))
