(ns gd.views.messages
  (:require [noir.options :as options])
  (:use gd.utils.common))

(defn init-bundles[]
  (def bundle-messages
    {:ru (load-properties "messages/main.ru.properties" "messages/retail.ru.properties")
     :ua (load-properties "messages/main.ua.properties")}))

(init-bundles)

(defn !
  "Get message with 'key' from message bundle, according to current locale. If
  multiple keys passed to function - they will be concatenated and used as
  usuall key"
  [& keys]
  (if (options/dev-mode?)
    (init-bundles))
  (let [key-value (reduce #(str %1 (when-not (= (last %1) (last ".")) ".") %2)
                          (map as-str (remove nil? keys)))]
    (or (.get (bundle-messages :ru) key-value) key-value)))

(defn get-keys-by-regexp[reg]
  (filter (fn[k] (re-find reg k)) (keys (bundle-messages :ru))))
