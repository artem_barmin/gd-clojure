(ns gd.views.components
  (:use noir.core
        gd.utils.styles
        noir.request
        hiccup.def
        hiccup.core
        hiccup.page
        hiccup.form
        hiccup.element
        [hiccup.util :only (to-str)]
        com.reasonr.scriptjure
        [clojure.algo.generic.functor :only (fmap)]
        clojure.data.json
        [clj-time.core :only (in-minutes interval now in-days in-hours)]
        [clj-time.core :exclude [extend]]
        clj-time.coerce
        clojure.java.io
        clj-time.format
        gd.model.model
        gd.model.context
        gd.utils.web
        gd.utils.common)
  (:require [gd.utils.security :as security]
            [clojure.string :as string]
            [noir.response :as resp])
  (:require [clj-time.coerce :as tm])
  (:import [java.text.DateFormatSymbols])
  (:import (org.joda.time DateTimeZone)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stop propagation
(defmacro stop-propagation[& body]
  (let [stop (js (stopEvent event))]
    `(precompile-html [:div {:onclick ~stop :onmousedown ~stop :onmouseup ~stop}
                       ~@body])))

(defmacro stop-propagation-inline-block[& body]
  (let [stop (js (stopEvent event))]
    `(precompile-html [:div {:onclick ~stop :onmousedown ~stop :onmouseup ~stop :style "display:inline-block;"}
                       ~@body])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Input fields with blur/focus behaviour
(defn- input-focus-blur[type]
  (fn[& [attr-map? name value :as args]]
    (require-js :custom-lib :inputbf)
    (let [[attr-map? name value] (if (map? attr-map?) args (cons {} args))]
      (text-field (update-in (merge {:autocomplete "off"
                                     :data-inputbf true
                                     :data-inputbf-default value
                                     :onfocus (js (inputBFFocus this))
                                     :onblur (js (inputBFBlur this))}
                                    attr-map?)
                             [:class]
                             (fn[old-class]
                               (str (to-str old-class) " inputbf-inactive")))
                  name value))))

(def text-field* (input-focus-blur text-field))
(def password-field* password-field)
(def text-area* (input-focus-blur text-area))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Favicon

(defn favicon[href]
  (list
   [:link {:rel "icon" :href href :type "image/x-icon"}]
   [:link {:rel "shortcut icon" :href href :type "image/x-icon"}]
   [:link {:rel "shortcut icon" :href href :type "image/vnd.microsoft.icon"}]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Image with fixed sizes

(defn sized-image[& [attrs src]]
  (let [src (if (map? attrs) src attrs)
        attrs (and (map? attrs) attrs)
        {:keys [width height]} (images:parse-size-from-src src)]
    [:img (merge-with str attrs {:style (str "width:" width "px;height:" height "px;") :src src})]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Lazy image load(will be showed when scrolled into view)

(def ^:dynamic *lazy-image-priority* nil)

(defelem lazy-image[src & [priority]]
  (let [{:keys [width height]} (images:parse-size-from-src src)]
    (if (= *lazy-image-priority* :maximum)
      [:img {:src src}]
      [:img {:lazy-image "true" :src "/img/px.png" :data-original src
             :width width
             :height height
             :order (+ (or *lazy-image-priority* 0) (or priority 0))}])))

(defmacro lazy-with-priority[priority & body]
  `(binding [*lazy-image-priority* ~priority]
     ~@body))

(defmacro lazy-with-max-priority[& body]
  `(binding [*lazy-image-priority* :maximum]
     ~@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Spinner
(defn spinner[name & {:keys [style width text value max min onchange attrs add-on button-hide] :or {min 1 max 99999}}]
  (require-js :custom-lib :spinner)
  [:div.spinner-element.relative {:style (str (when width (css-inline :width width)))
                                  :role :spinner}
   (text-field (merge {:class "input input-text"
                       :style style
                       :role :value
                       :max max
                       :min min
                       :autocomplete "off"
                       :onchange onchange}
                      attrs)
               name (or value min))
   (if (not-empty add-on)
     [:span.add-on add-on])
   [:span (if button-hide {:class :none})
    [:span.spinner-top.spinner-button (merge {:role :up} (if-not add-on {:style "right:-15px;"}))
     [:span.spinner-plus.none "+"]]
    [:span.spinner-bottom.spinner-button (merge {:role :down} (if-not add-on {:style "right:-15px;"}))
     [:span.spinner-minus.none "-"]]]
   [:div.spinner-extension text]
   [:div.clear]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tooltip
(defmacro tooltip
  "Add custom tooltip to element. Should be used for :title attribute.

Example [:div {:data-title (tooltip \"Hello world\")}]"
  [content]
  `(do
     (require-js :lib :tooltip)
     (html [:div.tooltip-content ~content])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; infinite scroll

(defn infinite-scroll-body[empty-case columns template]
  (fn[data offset]
    (if (and (zero? offset) (zero? (count data)))
      empty-case
      [:table.infinite-table {:style (css-inline :width "100%" :border-collapse :collapse)}
       (map
        (fn[list]
          [:tr.infinite-scroll-row
           (map (fn [ent] [:td (template ent)]) list)])
        (partition-all columns data))])))

(defmacro infinite-scroll*[name datasource template
                           & {:keys [columns empty-case limit local max-height]
                              :or {columns 3 limit 10 local false}}]
  `(let [render-body# (infinite-scroll-body ~empty-case ~columns ~template)]
     (require-js :custom-lib :infinite-scroll)
     [:div.infinite-scroll-top-element {:style (if ~local (css-inline :overflow :auto
                                                                      :height ~max-height))
                                        :role :infinite-scroll
                                        :data-name ~(str name)}
      (init-jquery-plugin
       (str ".infinite-scroll-top-element[data-name='" ~(str name) "']")
       "infiniteScroll"
       (callback [~'offset]
                 (remote* ~name
                          (render-body# ~datasource ~'(c* offset))
                          :success ~'success
                          :result :html))
       ~limit
       ~local)
      [:div.infinite-scroll-body
       (render-body# (remote-body-basic* ~datasource) 0)]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tabs component
(defn tabbed-panel
  "Tabs based on Boostrap classes and Angular controller. Can be bound to angular variable."
  [{:keys [model disabled]} & tabs]
  (require-js :custom-lib :tabs)
  (let [set-model (fn[val](when model (js (set! (clj (symbol model)) (clj (name val))))))]
    [:div.nav-tabs-container {:ng-init (set-model (:name (first tabs))) :role :tabs-container}
     [:ul.nav.nav-tabs
      (map-indexed (fn[i {:keys [header name]}]
                     [:li {:class (when (zero? i) :active)}
                      [:a (merge {:href (as-str "#" name)
                                  :tab-name (as-str name)
                                  :ng-click (set-model name)}
                                 (if disabled
                                   {:ng-attr-data-toggle (str "{{" (js (? (clj disabled) "" "tab")) "}}")}
                                   {:data-toggle "tab"}))
                       header]])
                   tabs)]
     [:div.tab-content.group
      (map-indexed (fn[i {:keys [name body]}]
                     [:div.tab-pane.fade
                      {:id (as-str name)
                       :class (when (zero? i) "active in")
                       :role :tab}
                      body])
                   tabs)]]))

(defn tab[name header body]
  {:header header :name name :body body})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Simple small carousel
(defn simple-carousel[images size & {:keys [onclick]}]
  (require-js :custom-lib :carousel)
  (let [[w h] (resolve-size size)
        count (count images)
        visible 5
        disable (<= count visible)]
    (when (> count 1)
      [:div.simple-carousel.fixed-group
       [:div.left.prev.navigate-button {:onclick (js (carouselMove this 1))
                                        :disabled disable
                                        :enabled (not disable)}]
       [:div.left
        [:div.relative {:style (css-inline :overflow :hidden
                                           :width (+ (* visible w) 25)
                                           :height h)}
         [:ul.absolute.images-container {:style (css-inline :width "3000px;")}
          (map (fn[im]
                 [:li
                  [:img.clickable (merge {:src (images:get size im)} (when onclick {:onclick (onclick im)}))]])
               images)]]]
       [:div.left.next.navigate-button {:onclick (js (carouselMove this -1))
                                        :disabled disable
                                        :enabled (not disable)}]])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Modal window template
(defn modal-window-ex[{:keys [top-style arrows]} & content]
  (require-js :custom-lib :modal-panel)
  (require-css :lib :ui-core)

  (precompile-html
   [:div {:role :dialog-store}
    [:div.dialog-content
     [:div.modal-header-close [:div.modal-close]]
     [:div.header-modal]

     [:div.modal-main-wrapper

      [:table.center {:cellpadding 0 :cellspacing 0}
       [:tr {:style "height: 100%;"}
        [:td.modal-arrow (when arrows [:div.modal-left-arrow {:onclick (:left arrows)}])]
        [:td [:div.modal-main-block {:style top-style} content]]
        [:td.modal-arrow (when arrows [:div.modal-right-arrow {:onclick (:right arrows)}])]]]]

     [:div.footer-modal]]]))

(defn modal-window[& content]
  (apply modal-window-ex nil content))

(defn modal-window-open[& [dont-clone]]
  (js (openDialog this (clj dont-clone))))

(defn modal-window-open-angular[]
  (js (openDialogAngular this)))

(defn modal-close-only-on-button[]
  (js (set! closeOnlyOnButton true)))

(defn modal-window-with-track[track-name & [dont-clone]]
  (js (trackVisit (clj track-name))
      (openDialog this (clj dont-clone))))

(defn modal-window-open-and-push-url*[url & [dont-clone title]]
  (js* (pushUrl (clj url) (clj title))
       (openDialog this (clj dont-clone) true)))

(defn modal-window-open-and-push-url[url & [dont-clone]]
  (cljs (modal-window-open-and-push-url* url dont-clone)))

(defn modal-window-open*[& [dont-clone]]
  (js* (openDialog this (clj dont-clone))))

(defn modal-window-open-at-start[selector]
  (document-ready (js* (openDialog ($ (clj selector)) null true)
                       (. ($ document) bind "pager.change" centerDialog))))

(defn modal-window-plain[content]
  (modal-window-ex {:top-style "padding:0px;"} content))

(defn modal-window-with-header[header {:keys [id width] :or {width 500} :as params} content]
  (modal-window-ex
   (merge {:top-style "padding:0px;"} params)
   [:div.site-login-window {:id id
                            :style (str
                                    "width:" width "px;"
                                    "background-color: #f7f8fc;border-radius: 14px;background-image:url(/img/user-profile/ball3.gif);background-repeat:no-repeat;"
                                    "background-position: " (- width 96) "px 80%;")}

    [:div.main-block-wrapper {:style "border-collapse:collapse;"}
     [:div.main-block-header {:style "text-align:left;"}
      [:div.left-block-header header]]]
    content]))

(defn modal-window-without-header[{:keys [id width] :or {width 500} :as params} content]
  (modal-window-ex
   (merge {:top-style "padding:0px;"} params)
   [:div {:id id :style (css-inline :width width)}
    content]))

(defn modal-window-admin [header-name save-panel & content]
  [:div
   [:div#dialog-modal]
   [:div {:role :dialog-store}
    [:div.dialog-content.center.form {:style "width:907px;"}
     [:div.header-modal
      [:p.title.text-left {:style "overflow: hidden; text-overflow:ellipsis; width:600px;"} header-name]
      [:div.modal-close "×"]]

     [:div.modal-main-block {:style "padding:0px;"} content]
     [:div.footer-modal
      save-panel]]]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Popover

(defn popover[title content]
  (require-js :custom-lib :popover)
  [:div.popover.absolute.none
   [:div.arrow]
   [:h3.title.title-popover.group {:style "font-size:20px;"}
    [:p.left title]
    [:div.close.right {:onclick (js (closePopover this))} "x"]]
   content])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Rerenderable panel

(defmacro lazy-multi-region*
  "Lazy region. Will render result only on service request. Also will render request only once during
client session."
  [id key params & body]
  `(let [key# ~key
         [fn-name# loaded-var#] (generate-js-function-names (str ~(str id) key#) "region" "loaded")
         region-selector# (str ".region." ~(str id) "." key#)]
     [:div {:class (str ~(str "region " id) " " key#) :region-function fn-name# :region-content loaded-var#}
      (javascript-tag (js
                       (var (clj loaded-var#) nil)
                       (var (clj fn-name#)
                            (fn[~@params ~'success]
                              (if (clj loaded-var#)
                                (do
                                  (var ~'region ($ (clj region-selector#)))
                                  (if (. ~'region ~'is ":empty")
                                    (. ~'region ~'html (clj loaded-var#)))
                                  (if ~'success (~'success ~'region false)))
                                (clj
                                 (remote* ~id (do ~@body)
                                          :result :html
                                          :success (fn[~'res]
                                                     (hideValidation)
                                                     (set! (clj loaded-var#) ~'res)
                                                     (var ~'region ($ (clj region-selector#)))
                                                     (. ~'region ~'html (clj loaded-var#))
                                                     (if (and (! (= (typeof ~'success) "undefined"))
                                                              ~'success)
                                                       (~'success ~'region true))
                                                     (.trigger ($ ~'document) (str "region." ~(str id)))))))))))]))

(def lazy-modal-window-counter (atom 0))

(defn multi-region-class*
  [id key]
  (let [[remote-name id] (if (vector? id) id [id id])]
    (str "region " (as-str id) " " key " region-inplace")))

(defmacro multi-region-function*
  [id key params & body]
  (let [[remote-name id] (if (vector? id) id [id id])]
    `(javascript-tag (js (var (clj (generate-js-function-name (str ~(str id) ~key) "region"))
                              (fn[~@params ~'success]
                                (clj
                                 (remote* ~remote-name (do ~@body)
                                          :result :html
                                          :success (fn[~'res]
                                                     (multiRegionSuccess
                                                      ~(str id)
                                                      (clj (str ".region." ~(str id) "." ~key))
                                                      ~'res
                                                      ~'success))))))))))

(defmacro multi-region*
  "Parametrized region that can appear on page many times.

id - can be symbol, or vector. If name passed as vector, then first element - is name of remote,
 second element - is name which will be used for registering client side functions.

key - used for distinguishing different regions(it usually should be equals to some entity id)"
  [id key params & body]
  (let [[remote-name id] (if (vector? id) id [id id])]
    `[:div {:class (str ~(str "region " id) " " ~key)}
      (javascript-tag (js (var (clj (generate-js-function-name (str ~(str id) ~key) "region"))
                               (fn[~@params ~'success]
                                 (return
                                  (clj
                                   (remote* ~remote-name (do ~@body)
                                            :result :html
                                            :success (fn[~'res]
                                                       (multiRegionSuccess
                                                        ~(str id)
                                                        (clj (str ".region." ~(str id) "." ~key))
                                                        ~'res
                                                        ~'success)))))))))
      (remote-body-basic* (do ~@body))]))

(defn multi-dynamic-function
  "You should use this when id parameter not defined on server side"
  [id key]
  (js* (buildRegionFunction (clj (str (generate-js-function-name (name id) nil))) (clj key))))

(defn multi-rerender[id key & client-params]
  (apply list (custom-concat (list (multi-dynamic-function id key)) client-params)))

(defmacro region*
  "Parametrized region that can appear on page only once"
  [id client-params & body]
  `(multi-region* ~id "" ~client-params ~@body))

(defn rerender[id & client-params]
  (apply multi-rerender id "" client-params))

(defmacro auto-updated-region[id key {:keys [interval callback] :or {interval 2000}} & body]
  `(list
    (multi-region* ~id ~key [] (do ~@body))
    (document-ready (js* (setInterval
                          (fn[]
                            (clj (multi-rerender ~(str id) ~key))
                            (clj ~callback))
                          ~interval)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Lazy modal window
(comment "Content will be only loaded when opening required. Also possible
optimizations - load window in background after each ajax request. In this way
we can combine fast showing of first results(usually - list) and fast opening of
windows.")

(defmacro lazy-modal-window
  "Modal window that will load lazily(only once during client session, and on demand).

 name - just like name for any remote function"
  [name & content]
  `[:div
    [:div#dialog-modal]
    [:div.dialog-content.lazy-modal-window
     [:div.modal-header-close [:div.modal-close]]
     [:div.modal-header]

     [:div.modal-main-wrapper

      [:table.center {:cellpadding 0 :cellspacing 0 :style "margin:0; width:100%;"}
       [:tr {:style "height: 100%;"}
        [:td.modal-arrow]
        [:td [:div.modal-main-block {:style "padding:0px;height:300px;opacity:0;"}
              (lazy-multi-region* ~name (str "lazy-modal" (swap! lazy-modal-window-counter inc)) []
                ~@content)]]
        [:td.modal-arrow]]]]

     [:div.modal-footer]]])

(defmacro lazy-modal-window-with-header[name header {:keys [id width] :or {width 500}} content]
  `(lazy-modal-window ~name
                      [:div {:id ~id :style (str
                                             "width:" ~width "px;"
                                             "background-color: #f7f8fc;border-radius: 14px;background-image:url(/img/user-profile/ball3.gif);background-repeat:no-repeat;"
                                             "background-position: " (- ~width 96) "px 80%;")}

                       [:div.main-block-wrapper {:style "border-collapse:collapse;"}
                        [:div.main-block-header {:style "text-align:left;"}
                         [:div.left-block-header ~header]]]
                       ~content]))

(defmacro fancy-lazy-modal
  "Modal window that will load lazily(only once during client session, and on demand).

 name - just like name for any remote function"
  [name & content]
  `[:div
    [:div#dialog-modal]
    [:div.dialog-content.lazy-modal-window
     [:div.modal-main-block {:style "width:1000px; border-radius:6px;"}
      (lazy-multi-region* ~name (str "lazy-modal" (swap! lazy-modal-window-counter inc)) []
        ~@content)]]])

(defmacro fancy-lazy-modal-with-header[name header {:keys [id width] :or {width 500}} content footer]
  `(fancy-lazy-modal ~name
                     [:div.form {:id ~id :style (str "width:" ~width "px")}
                      [:div.header-modal
                       [:p.title.text-left ~header]
                       [:div.modal-close {:style "right:15px;"} "×"]]
                      ~content
                      [:div.footer-modal ~footer]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; WYSIWYG text area
(defn custom-text-area
  "WYSIWYG text area, that automatically update underlying input component, and
can be used in any forms for sending data.

if not-active is passed - then text area will no initalized at startup, and you
should initialize it from user code :
$(elem).find('.user-message-input').editor()"
  [name & {:keys [not-active class mode autosize value]}]
  (require-js :custom-lib :bbcode)
  (require-js :custom-lib :02-jquery.cleditor)
  (require-js :custom-lib :wysiwyg)
  (require-css :lib :jquery.cleditor)
  [:div {:class (as-str class)}
   (when-not not-active
     (init-jquery-plugin (str "[name='" name "']") "editor"))
   [:textarea.wysiwyg-editor {:name name
                              :mode (as-str mode)
                              :autosize autosize
                              :autocomplete "off"}
    value]])

(defelem fancy-textarea[name & [value]]
  (text-area {:class "fancy-textarea"
              :autocomplete "off"      ;must be used, to keep data after refresh
              }
             name
             value))

(defn custom-checkbox[& [attr-map? id checked? value :as args]]
  (let [[attr-map? id checked? value] (if (map? attr-map?) args (cons {} args))]
    [:div.inline-block {:style "width:18px;"}
     (check-box (merge attr-map? {:autocomplete "off"}) id checked? (or value true))
     [:label]]))

(defn custom-dropdown[& [attr-map? name options selected :as args]]
  (let [[attr-map? name options selected] (if (map? attr-map?) args (cons {} args))
        options (map (fn[opt] (if (sequential? opt) opt [opt opt])) options)
        [empty-value] (or
                       (:empty-value attr-map?)
                       (let [[empty] (find-first (fn[[_ val]] (or (= val "null") (not val))) options)]
                         [(str "'" empty "'")]))]
    (require-js :custom-lib :dropdown)
    (require-css :lib :dropkick)
    [:div.dk_container.dk_theme_default
     {:tabindex -1                      ;hack to make blur/focus works
      :onfocus (js (.addClass ($ this) "dk_focus"))
      :onblur (js (.removeClass ($ this) "dk_open dk_focus"))}
     [:a.dk_toggle
      {:onclick (js (openCustomDropdown this))}
      [:span.dk_label (or
                       (when-let [view (:ng-view attr-map?)] (str "{{" view "}}"))
                       (when-let [model (:ng-model attr-map?)] (str "{{" model " || " empty-value "}}"))
                       (first (find-by-val second selected options))
                       (first (first options))
                       selected)]]
     [:div.dk_options {:style (:style attr-map?)}
      [:ul.dk_options_inner
       (for [[text val extra-args] options]
         [:li (merge {:class (when (= val selected) "dk_option_current")
                      :onclick (js (updateCustomDropdownField this))
                      :data-dk-dropdown-value val}
                     extra-args)
          [:a text]])]]
     [:select (merge
               {:name (#'hiccup.form/make-name name)
                :id (#'hiccup.form/make-id name)
                :autocomplete "off"}
               attr-map?)
      (for [[text val extra-args] options]
        [:option (merge {:value val :selected (= val selected)} extra-args)
         text])]]))

(defn custom-radio-button[& [attr-map? name value visual-block selected :as args]]
  (let [[attr-map? name value visual-block selected] (if (map? attr-map?) args (cons {} args))]
    (list
     (radio-button (merge {:class "none" :autocomplete "off"} attr-map?) name selected value)
     [:div.custom-radio
      {:onclick (js
                 (var el (.prev ($ this)))
                 (var $this ($ this))
                 (var all ($ (str (byParam "name" (clj (as-str name))) " + .custom-radio")))
                 (.removeClass all "checked")
                 (.addClass $this "checked")
                 (.removeAttr el "checked")
                 (.attr el "checked" "checked")
                 (.change (.click el))
                 (hideValidation))
       :custom-radio-value value}
      visual-block])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fancy buttons

(defn button-fn[class]
  (fn [text & {:keys [action href width
                      disabled disabled-text disabled-tooltip
                      ng-action ng-disabled ng-disabled-tooltip]}]
    (let [action-params
          (cond href {:onclick (format "window.location.href='%s'" href)}
                action {:onclick (str "if(isButtonCanClick(this)){" action "}")}
                ng-action {:ng-click ng-action})

          width-params
          (when width {:style (css-inline :width width)})

          disabled-params
          (cond (and disabled disabled-tooltip)
                {:data-title (tooltip disabled-tooltip)}

                (and ng-disabled ng-disabled-tooltip)
                {:ng-attr-data-title (str "{{"
                                          (js (? (clj ng-disabled)
                                                 (clj ng-disabled-tooltip)
                                                 nil))
                                          "}}")
                 :ng-class (js {:disabled (clj ng-disabled)})}

                ng-disabled
                {:ng-class (js {:disabled (clj ng-disabled)})})

          text
          (if (and disabled disabled-text) disabled-text text)]
      (case (:buttons (:design *supplier-info*))

        :plain
        [:div (merge
               {:class (string/join " " [(when-not disabled "enabled")
                                         (when disabled "disabled")
                                         "center"
                                         "text-center"
                                         "plain-button"
                                         (class "plain")])}
               disabled-params width-params action-params)
         text]

        [:div (merge
               {:class (string/join " " [(when-not disabled "enabled")
                                         (when disabled "disabled")
                                         "center"
                                         "fixed-group"
                                         (class "container")])}
               disabled-params action-params)
         [:div {:class (class "left")}]
         [:div (merge {:class (str "button-center-text " (class "center"))}
                      width-params)
          [:img.none {:src "/img/ajax-loader.gif" :style "margin-left:auto;margin-right:auto;margin-top:9px;"}]
          [:div {:class (str "button-text noselect " (class "text"))}
           text]]
         [:div {:class (class "right")}]]))))

(defmacro define-button[name & aliases]
  (let [class (fn[postfix]
                (string/join " "
                             (cons (str name "-" postfix)
                                   (map (fn[alias] (str alias "-" postfix)) aliases))))]
    `(do
       (def ~name (button-fn ~class))
       ~@(map (fn[alias] `(def ~alias ~name)) aliases))))

(define-button blue-button neutral-button)
(define-button green-button)
(define-button red-button)
(define-button grey-button cancel-button)
(define-button orange-button ok-button)
(define-button small-blue-button small-neutral-button)
(define-button middle-blue-button middle-neutral-button)
(define-button small-grey-button small-cancel-button)
(define-button small-orange-button small-ok-button)
(define-button small-orange-button small-success-button)
(define-button small-orange-button small-info-button)
(define-button small-orange-button small-warning-button)
(define-button small-orange-button small-danger-button)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Close button
(defn close-button [click-action]
  [:div.glyphicon-container {:ng-click click-action}
   [:i.glyphicon.glyphicon-close.glyphicon-remove-sign.clickable]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Modal save panel
(defn modal-save-panel [action-button]
  [:div.group
   [:div.right.margin10l.action-button
    action-button]
   [:div.right
    (small-grey-button "Отменить" :action (js (closeDialog)) :width 120)]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Breadcrumbs

(defn breadcrumbs[breadcrumbs-anchors]
  (let [create-link-list
        (fn[links class-separator]
          (when (not (empty? links))
            (concat (map (fn[cat]
                           (list
                            [:div.breadcrumbs-div.breadcrumbs-link (link-to (:ref cat) (:name cat))]
                            [:div {:class (str "breadcrumbs-div "class-separator)}]))
                         (butlast links))
                    [[:div.breadcrumbs-div.breadcrumbs-link (link-to (:ref (last links)) (:name (last links)))]])))]
    [:div.breadcrumbs.group
     [:div.breadcrumbs-1-level.breadcrumbs-div
      (create-link-list breadcrumbs-anchors "breadcrumbs-1-level-separator")]]))

(defn build-breadcumbs-from-path[path build-url]
  (let [categories-breadcrumbs (category:tranform-path-to-categories path)
        all-categories-anchor {:ref "/"
                               :name (get (:headers (ring-request)) "host")}]
    (cons
     all-categories-anchor
     (map (fn[{:keys [name] :as category}] {:ref (build-url category) :name name})
          categories-breadcrumbs))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Formatting utils
(def ru-locale (new java.util.Locale "ru" "RU"))
(def en-locale (new java.util.Locale "en" "EN"))

(def russian-formatter
  (-> (formatter "dd MMM yyyy в HH:mm")
      (with-locale ru-locale)
      (with-zone (DateTimeZone/getDefault))))

(def russian-date-formatter
  (-> (formatter "dd MMM yyyy")
      (with-locale ru-locale)
      (with-zone (DateTimeZone/getDefault))))

(def english-date-formatter
  (-> (formatter "dd MMM yyyy")
      (with-locale en-locale)
      (with-zone (DateTimeZone/getDefault))))

(defn- resolve-formatter[language]
  (case language
    :english english-date-formatter
    russian-date-formatter))

(defn format-date[date & {:keys [language] :or {language :russian}}]
  (unparse (resolve-formatter language) (to-date-time date)))

(defn format-timestamp[date]
  (unparse russian-formatter (to-date-time date)))

(defn format-just-date[date & {:keys [language] :or {language :russian}}]
  (unparse (resolve-formatter language) (to-date-time date)))

(defn all-months[]
  (seq (.getMonths (new java.text.DateFormatSymbols ru-locale))))

(defn smart-text[num t1 t2:4 t5:9]
  (cond
    ;; 0 записей | ==1
    (= num 0) t5:9
    ;; 1 запись | ==1
    (= num 1) t1
    ;; 2,3,4 записи | >1 and < 5
    (and (> num 1) (< num 5)) t2:4
    ;; 5,6,7,8,9,10...19 записей | > 4 and < 20
    (and (> num 4) (< num 20)) t5:9
    ;; N*10 + 0 записей | 20,30...
    (and (> num 19) (= (mod num 10) 0)) t5:9
    ;; N*10 + 1 запись, N > 2 | 21,31,41,51
    (and (> num 20) (= (mod num 10) 1)) t1
    ;; N*10 + 2,3,4 записи, N > 2 | 22,23,24
    (and (> num 20) (> (mod num 10) 1) (< (mod num 10) 5)) t2:4
    ;; N*10 + 5,6,7.8,9 записей, N > 2 | 25,26,27,28,29
    (and (> num 20) (> (mod num 10) 4)) t5:9
    true t2:4))

(defn format-date-diff[date-or-interval]
  (let [interval (if (instance? org.joda.time.Interval date-or-interval)
                   date-or-interval
                   (interval (from-date date-or-interval) (now)))
        zero-is-nil (fn[val] (when-not (zero? val) val))]
    (if (< (in-minutes interval) 1)
      "Менее минуты назад"
      (let [days (zero-is-nil (in-days interval))
            hours (zero-is-nil (rem (in-hours interval) 24))
            minutes (zero-is-nil (rem (in-minutes interval) 60))]
        (apply str
               (interpose
                " "
                (flatten
                 [(when days [days (smart-text days "день" "дня" "дней")])
                  (when hours [hours (smart-text hours "час" "часа" "часов")])
                  (when minutes [minutes (smart-text minutes "минуту" "минуты" "минут")])
                  "назад"])))))))

(defn format-date-interval[booked-until]
  (let [expired (before? booked-until (now))

        period
        (.toPeriod
         (if expired
           (interval booked-until (now))
           (interval (now) booked-until)))]
    [:span {:style (if expired "color:#dc5920" "color:#659300")}
     (str (+
           (* 7 (.getWeeks period))
           (.getDays period))
          "д:"
          (.getHours period)
          "ч:"
          (.getMinutes period) "мин")]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Accordion
(defn- accordion-block[image header items active]
  [:div
   [:div.accordion-title
    [:table.deal-info-title-container
     [:tr {:style "height: 50px;"}
      [:td {:style "width: 80px;"}
       [:center
        [:img {:src image :style "vertical-align:middle;"}]]]
      [:td header]]]]
   [:div.accordion-content (if-not active {:style "display:none;"})
    [:table.deal-info-content-container
     [:tr
      [:td ]]
     [:tr
      [:td {:style "padding: 5px 10px 0 10px;"} (map #(vector :div %) items)]]
     [:tr
      [:td.deal-info-content-footer {:colspan "2"}]]]]])

(defn accordion[id & blocks]
  [:div.deal-desc {:id id}
   (init-jquery-plugin (str "#" id) "accordion" {:header ".accordion-title"
                                                 :active (count (take-while (comp not :active) blocks))})
   (map (fn[{:keys [image header items active]}]
          (accordion-block image header items active))
        blocks)])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ajax pager
(defn pages-widget[max-pages page whole-number page-render-fn & [disable-next-prev]]
  (assert page "Page should not be nil")
  (let [pages
        (if (< whole-number max-pages)
          (range 1 (inc whole-number))
          (let [max-min-pages 1
                max-branches (+ max-pages 1)
                max-branch (/ max-branches 2)

                less (cond
                       (< page max-branch) page
                       (> page (- whole-number max-branch)) (- max-branches (inc (- whole-number page)))
                       true max-branch)
                more (cond
                       (< page max-branch) (- max-branches page)
                       (> page (- whole-number max-branch)) (inc (- whole-number page))
                       true max-branch)

                left-links (range (- (inc page) less) page)
                right-links (range page (+ page more))

                first (range 1 (inc max-min-pages))
                last (map #(- whole-number %) (range (dec max-min-pages) -1 -1))

                links (concat left-links right-links)
                links (if (= (take max-min-pages links) first)
                        links
                        (concat first ["..."] (drop max-min-pages links)))
                links (if (= (take-last max-min-pages links) last)
                        links
                        (concat (drop-last max-min-pages links) ["..."] last))]
            links))]
    [:span.pager-main
     (list
      (when-not disable-next-prev
        (if (or (= page 1) (= whole-number 0))
          (page-render-fn :prev nil)
          (page-render-fn :prev (- page 1))))
      (map (fn[p]
             (cond
               (= p page) (page-render-fn :current p)
               (= p "...") [:span.page-navigation p]
               true (page-render-fn :active p)))
           pages)
      (when-not disable-next-prev
        (if (or (= page whole-number) (= whole-number 0))
          (page-render-fn :next nil)
          (page-render-fn :next (+ page 1)))))]))

(def ^:dynamic *dont-throw-exception* false)

(defn pager-pages
  "Render pages widget for pager"
  [page-loader-name pager-title initial-page-size count current top page]
  (when-not *dont-throw-exception*
    (let [pages (pages-count page count)]
      (when (and (> pages 0) (> current pages))
        (throwf "Wrong page"))))
  (let [active-link (fn[page-to-go & text]
                      [:a.pager-page
                       {:onclick (js ((clj page-loader-name) (clj (* (dec page-to-go) page)) (clj page))
                                     (clj (if-not top
                                            (js* (. ($ window) scrollTop (aget (. ($ ".stocks-list-ajax-pager:visible") offset) "top")))))
                                     (return false))
                        :href (if (= page-to-go 1)
                                (current-url)
                                (str "page" page-to-go (when (not= initial-page-size page) (str "x" page))))} text])
        not-active-link (fn[& text]
                          [:span.pager-non-active text])
        current-link (fn[& text]
                       [:span.pager-current-link text])]
    [:div.pager-pages-container {:class (when-not top :pager-pages-container-bottom)}
     [:p.title.pager-title.left (if (fn? pager-title) (pager-title count current) pager-title)]
     (pages-widget 7 current (pages-count page count)
                   (fn[type page]
                     (case type
                       :current (current-link page)
                       :active (active-link page page)
                       :next  (if page
                                (active-link page "вперед" [:div.pager-next-arrow])
                                (not-active-link "вперед" [:div.pager-next-arrow]))
                       :prev  (if page
                                (active-link page [:div.pager-prev-arrow] "назад")
                                (not-active-link [:div.pager-prev-arrow] "назад")))))]))

(defn pager-functions[base-name initial-page]
  (let [[page-loader-name next-offset-name prev-offset-name
         max-stocks-name current-offset reload page-size :as names]
        (generate-js-function-names base-name
                                    "LoadPage" "NextOffset" "PrevOffset" "MaxStocks"
                                    "CurrentOffset" "Reload" "PageSize" "ColumnsNum")

        pager-body
        (js* ($ (clj (str ".stocks-list-ajax-pager.js-pager-" base-name))))

        pager-success-function
        (js*
         (var pagerBody (clj pager-body))

         (set! (.. (.get pagerBody 0) innerHTML) (.. res header))

         (var table (.find pagerBody ".pager-table"))

         (showByPortions 0 table res.content)

         (. table css {:opacity 1})
         (. ($ document) trigger "pager.change")
         (if callback (callback)))

        before-load-function
        (js*
         (set! page (or page (clj initial-page)))
         (set! (clj page-size) page)
         (set! (clj next-offset-name) (+ offset page))
         (set! (clj prev-offset-name) (- offset page))
         (set! pageNum (+ 1 (/ offset page)))
         (if (= (clj initial-page) page)
           (if (= pageNum 1)
             (pushUrl "")
             (pushUrl (str "page" pageNum)))
           (pushUrl (str "page" pageNum "x" page)))
         (set! (clj current-offset) offset)
         (. ($ document) trigger "pager.before-change")
         (. (.find (clj pager-body) ".pager-table") css {:opacity 0.2}))]
    [names pager-success-function before-load-function]))

(defn pager-body[template tr-in-tbody custom-rows-mode]
  (fn[columns data]
    (map!
     (fn[tbody]
       (html
        [:tbody
         (map!
          (fn[list]
            (if (and custom-rows-mode (= 1 columns))
              (template (first list))
              [:tr.pager-row
               (map (fn [ent] [:td (template ent)]) list)
               (map (fn [_] [:td {:style "width:100%;"} "&nbsp;"]) (range 0 (- columns (count list))))]))
          tbody)]))
     (partition-all tr-in-tbody (partition-all columns data)))))

(defn pager-current-page[]
  (or (:page *component-info*) 1))

(defn pager-limit-choose[page-loader-name]
  [:div.limit-dropdown.pager-dropdown
   (custom-dropdown
    {:width 30
     :onchange (js ((clj page-loader-name) 0 (parseInt (get-val "[name=pager-limit]"))))}
    :pager-limit (range 10 110 30)
    (or (:limit *component-info*) 10))])

(defmacro pager[name datasource template &
                {:keys [page start-page draw-now columns manual-control empty-text
                        register-as custom-rows-mode pager-title tr-in-tbody]
                 :or {page 12 start-page 0 draw-now false columns 3 manual-control false tr-in-tbody 20}}]
  `(let [page-render#
         (pager-body ~template ~tr-in-tbody ~custom-rows-mode)

         real-name#
         (let [reg-as# ~(str register-as)
               name# ~(str name)]
           (if (seq reg-as#) reg-as# name#))

         [[page-loader-name# next-offset-name# prev-offset-name# max-stocks-name# current-offset# reload# page-size# columns-num#]
          success-callback# before-load#]
         (pager-functions real-name#  ~page)

         pages#
         (partial pager-pages page-loader-name# ~pager-title ~page)

         load-page#
         (js* (fn [~'offset ~'page ~'callback]
                (clj before-load#)
                (clj (remote* ~name
                              (let [page-size# ~'(c* page)
                                    page-size# (if (> page-size# 100) 100 page-size#)
                                    ~'page-size page-size#]
                                (let [res# (with-count ~datasource)
                                      page# (inc (/ ~'(c* offset) page-size#))]
                                  {:header
                                   (html [:div
                                          (javascript-tag (js (var (clj max-stocks-name#) (clj (:count res#)))))
                                          (render-singletons)
                                          (pages# (:count res#) page# true page-size#)
                                          [:div.pager-body-container
                                           [:table.pager-table {:cellpadding 0 :cellspacing 0}]]
                                          (pages# (:count res#) page# false page-size#)])
                                   :content
                                   (if (= (:count res#) 0)
                                     [(html ~empty-text)]
                                     (page-render# (parse-int (get ~'param-map :columns)) (:result res#)))}))
                              :success (fn[~'res]
                                         (clj success-callback#))
                              :fixed-options {:columns columns-num#}))))]

     [:div

      (require-js :lib :history)
      (require-js :custom-lib :pager)
      (require-js :custom-lib :history-utils)

      ;; Javascript API
      (javascript-tag (js (var (clj page-loader-name#) (clj load-page#))
                          (var (clj current-offset#) 0)
                          (var (clj columns-num#) (clj ~columns))

                          (var (clj page-size#) (clj ~page))
                          (var (clj next-offset-name#) (clj ~page))
                          (var (clj prev-offset-name#) (- 0 (clj ~page)))

                          (var (clj reload#) (fn[]
                                               ((clj page-loader-name#)
                                                (* (- (getPageFromUrl) 1) (clj page-size#))
                                                (getLimitFromUrl))))))

      (when-not (or ~manual-control ~draw-now (:bot-request (ring-request)))
        (document-ready (js*

                         (set! (clj page-size#) (or (getLimitFromUrl) (clj ~page)))
                         (set! (clj next-offset-name#) (clj page-size#))
                         (set! (clj prev-offset-name#) (- 0 (clj page-size#)))

                         ((clj reload#))
                         (onPopState (fn[]
                                       (var ~'offsetFromPage (* (- (getPageFromUrl) 1) (clj page-size#)))
                                       (if (! (= ~'offsetFromPage (clj current-offset#)))
                                         ((clj reload#))))))))

      [:div {:style "position:relative;"}

       (pager-limit-choose page-loader-name#)

       [:div.stocks-list-ajax-pager {:class (str "js-pager-" real-name#)}
        (when (or ~draw-now (:bot-request (ring-request)))
          (let [~'page-size ~page]
            (let [res# (with-count
                         (remote-body-basic* ~datasource
                                             :binding {:offset (* (dec (pager-current-page)) ~'page-size)}))
                  page# (or (pager-current-page) (inc (/ ~start-page ~page)))]
              [:div
               (javascript-tag (js (var (clj max-stocks-name#) (clj (:count res#)))))
               (pages# (:count res#) page# true ~page)
               [:div.pager-body-container
                [:table.pager-table {:cellspacing 0 :cellpadding 0}
                 (if (= (:count res#) 0)
                   ~empty-text
                   (page-render# ~columns (:result res#)))]]
               (pages# (:count res#) page# false ~page)])))]]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Modal next/prev arrows + pager integration

(defn register-modal-navigation[pager-name]
  (javascript-tag
   (cljs
    (splice
     (map
      (fn[target source]
        (js* (set! (clj target) (clj (str source)))))
      '[pagerIntegrationLoadPage pagerIntegrationNextOffset
        pagerIntegrationPrevOffset pagerIntegrationMaxOffset
        pagerIntegrationPageSize]
      (generate-js-function-names pager-name "LoadPage" "NextOffset" "PrevOffset" "MaxStocks" "PageSize"))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tree dropdown
(defn tree-dropdown[name model]
  [:div.tree-drilldown-container
   [:div.tree-drilldown-element name]
   [:div.tree-drilldown
    model]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Carousel that can interact with user
(defn interactive-carousel[photos]
  (require-js :custom-lib :interactive-carousel)
  (require-css :custom-lib :interactive-carousel)
  [:div
   [:div.icarousel-container
    [:div.icarousel-content
     (for [src photos]
       [:img.icarousel-images {:src src}])]

    [:div.icarousel-switchers
     (for [[i] (indexed photos)]
       [:div {:class (str "icarousel-switch sw" i)}])]
    [:div.icarousel-progress-bar
     (for [[i] (indexed photos)]
       [:div {:class (str "icarousel-progress pr" i)}])]]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Managed list

(defn managed-list-item[item-template & {:keys [remove-text]}]
  (fn[item]
    [:li.group.current-item
     [:div.managed-list-delete-btn.left.margin5r
      (small-danger-button (or remove-text [:i.glyphicon.glyphicon-sm.glyphicon-remove-sign])
                           :action (js (. (. ($ this) parents ".current-item") remove))
                           :width 34)]
     (item-template item)]))

(defn managed-list[items item-template & {:keys [new-item-text remove-text new-item-hook remote]}]
  (let [item-template (managed-list-item item-template)]
    [:div.managed-list-component
     [:ul.group.managed-list
      (map! item-template items)]

     [:div.managed-list-add-btn {:style "width:34px;"}
      (small-success-button (or new-item-text [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign])
                            :action (if remote
                                      (js
                                       (var list (get-section ".managed-list-component" ".managed-list:first"))
                                       (clj remote))
                                      (js
                                       (var elem ($ (clj (html (item-template nil)))))
                                       (append-element-section ".managed-list-component" ".managed-list:first" elem)
                                       (clj new-item-hook)))
                            :width 34)]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; File upload

(defmacro form-for-file-json*[action-name sizes body & {:keys [sizes-to-wait]}]
  `(execute-form* ~action-name []
                  (resp/content-type "application/json; charset=utf-8"
                                     (json-str {:files
                                                (let [~'code (process-uploaded-image-to-code
                                                              ~'param-map ~sizes
                                                              :sizes-to-wait ~sizes-to-wait)]
                                                  ~body)}))
                  :call-type :link))

(defn custom-file-upload[id remote-action & {:keys [success width text button error]
                                             :or {text "Загрузить фото" width 200 button small-blue-button}}]
  [:div.file-upload
   (file-upload {:style "visibility:hidden;width:1px;position:absolute;"
                 :onchange (js (. $ ajaxFileUpload
                                  {:secureuri false
                                   :dataType "xml"
                                   :fileElementId id
                                   :url (clj remote-action)
                                   :success (fn[data]
                                              (hideValidation)
                                              (if (! (= data ""))
                                                ((clj success) (. (. ($ data) find ".upload-parent-frame") children "*"))))
                                   :timeout 20000
                                   :error (clj error)}))}
                id)
   (button text
           :action (js (. ($ (clj (str "[name=" (name id) "]:visible:first"))) click))
           :width width)])

(defn multi-file-upload[id remote-action & {:keys [success js-options text width]
                                            :or {width 200 text "Загрузить фото"}}]
  [:div.file-upload
   (require-js :lib :jquery.fileupload)
   (require-js :user :fileupload)
   [:input {:id id
            :type :file
            :name id
            :data-url remote-action
            :multiple true
            :style "visibility:hidden;width:1px;position:absolute;"}]
   (document-ready (js* (enableMultiUpload
                         (clj (name id))
                         (clj
                          (merge {:dataType "json"
                                  :done (when success
                                          (js* (fn[e data]
                                                 ((clj success) (.. data result files) e))))}
                                 js-options)))))
   (small-success-button text
                         :action (js (.click (get-section ".file-upload" "input")))
                         :width width)])

(defn process-uploaded-image-to-code[param-map sizes & {:keys [params sizes-to-wait]}]
  (let [name (some (fn[[k v]] (and (:tempfile v) k)) (map vec param-map))
        image {:path (.getAbsolutePath (file (:tempfile (get param-map name)))) :params params}]
    (images:save-to-code image sizes (or (seq sizes-to-wait) sizes))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; If registred - output block, othwerwise don't output anything
(defmacro maybe-block[& body]
  `(when (security/logged)
     (list ~@body)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Delivery selection

(def ^:private novaya-pochta-info (read-json (slurp "resources/tools/novya-pochta.txt") false))

(defn district-input[value]
  (custom-dropdown {:ng-model "address.district"} :district
                   (cons ["Выберите область" "null"] (keys novaya-pochta-info))
                   value))

(defn city-input[value]
  (remote* get-cities (keys (novaya-pochta-info (c* district))))
  [:input {:ng-options "city as city for city in address.cities | filter:$viewValue"
           :ng-model "address.city"
           :autocomplete "off"
           :name :city
           :data-min-length 0
           :value value
           :bs-typeahead true
           :class "form-control"
           :placeholder "Киев"}])

(defn novaya-pochta-input[value]
  (remote* get-novaya-pochta (get-in novaya-pochta-info [(c* district) (c* city)]))
  [:input {:ng-options "office as office for office in address.offices | filter:$viewValue"
           :ng-model "address.novayaPochta"
           :name :novaya_pochta
           :autocomplete "off"
           :data-min-length 0
           :value value
           :bs-typeahead true
           :class "form-control"
           :placeholder "Отделение №1"}])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Inplace region(modify result of function - add region class to template) + appends
(defn modify-result-attributes[result attributes]
  (when-not (vector? result)
    (throwf "Result is not a vector, can't be processed"))
  (let [[tag maybe-attrs & other] result]
    (if (map? maybe-attrs)
      (apply vector tag (merge-with (fn[a b]
                                      (str a " " b))
                                    maybe-attrs attributes)
             other)
      (apply vector tag attributes maybe-attrs other))))

(defn add-region-class[region-name entity-id result]
  (modify-result-attributes result {:class (multi-region-class* region-name entity-id)}))

(defmacro inplace-multi-region[name id client-params body]
  (let [plain-name (vec (map clojure.core/name (if (sequential? name) name [name name])))]
    `(list
      (multi-region-function* ~name ~id ~client-params (add-region-class ~plain-name ~id ~body))
      (add-region-class ~plain-name ~id ~body))))

(defmacro region-request
  "If we are in remote request for current region - execute request-fn, otherwise - return entity"
  [region request-fn entity]
  (let [action-name (remote-name region)]
    `(if (= *in-remote* ~action-name)
       ~request-fn
       ~entity)))
