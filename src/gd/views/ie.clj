(ns gd.views.ie
  (:use noir.core
        hiccup.util
        digest
        [noir.response :only (set-headers)]
        [noir.options :only (dev-mode?)]
        [clojure.java.shell :only (sh)]
        noir.request
        gd.utils.web
        gd.views.components
        gd.views.messages
        gd.model.model
        hiccup.page
        hiccup.element
        hiccup.core))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; IE specific
(defpage "/tools/PIE.htc" []
  (set-headers
   {"Content-Type" "text/x-component"
    "Expires" "Thu, 15 Apr 2020 20:00:00 GMT"}
   (slurp "resources/public/js/lib/PIE.htc")))
