(ns gd.views.seo.sitemap
  (:use noir.request
        gd.utils.common
        [noir.response :only (xml content-type)]
        noir.core
        gd.utils.db
        gd.utils.web
        hiccup.core
        hiccup.page
        korma.core
        gd.model.model)
  (:require [clojure.string :as s]))

(defn sitemap[params & urls]
  (html
   (list
    (xml-declaration "UTF-8")
    [:urlset {:xmlns "http://www.sitemaps.org/schemas/sitemap/0.9"}
     (map (fn[url]
            [:url
             [:loc url]
             (when-let [freq (:freq params)]
               [:changefreq freq])])
          urls)])))
