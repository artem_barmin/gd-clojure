(ns gd.views.seo.meta
  (:use gd.utils.common
        gd.utils.web
        gd.model.model))

(defn set-title[& title]
  (swap! *seo-parts* assoc :title (clojure.string/join " " (flatten title)))
  nil)

(defn set-meta[type & value]
  (swap! *seo-parts* assoc-in [:meta type] (clojure.string/join " " (flatten value)))
  nil)

(defn set-link[type & value]
  (swap! *seo-parts* assoc-in [:link type] (clojure.string/join " " (flatten value)))
  nil)

(defn update-meta[type & value]
  (swap! *seo-parts* update-in [:meta type] str (clojure.string/join " " (flatten value)))
  nil)

(defn get-title[]
  (:title @*seo-parts*))

(defn get-meta[]
  (concat (map (fn[[type value]]
                 [:meta {:name type :content value}])
               (:meta @*seo-parts*))
          (map (fn[[type value]]
                 [:link {:rel type :href value}])
               (:link @*seo-parts*))))

(defn noindex[]
  (update-meta :robots "noindex, follow"))
