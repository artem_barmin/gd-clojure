(in-ns 'gd.views.themes.bono.basic)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Footer
(defn- footer-css[]
  (list
   [".footer-center" :background-image "url('/img/themes/bono/footer_3d_div_bg_dark-green.png')" :height 150 :width 1103 :color :white :padding 35]
   [".footer-side" :background-image "url('/img/themes/bono/footer_3d_div_bg_light-green.png')" :height 150]
   [".top-footer-side" :height 19]
   [".top-footer-left-corner" (bgimage "/img/themes/bono/footer_3d_div_left.png") :display :inline-block]
   [".top-footer-right-corner" (bgimage "/img/themes/bono/footer_3d_div_right.png") :display :inline-block]
   [".top-footer-spacer" :background-color "#DCEBB1" :width 1050 :height 19 :display :inline-block]))

(defn- footer[]
  (list
   [:div.width.footer-side.top-footer-side {:style "margin-top:-19px;"}
    [:div.text-center
     [:div.top-footer-left-corner]
     [:div.top-footer-spacer]
     [:div.top-footer-right-corner]]]
   [:div.width.footer-side
    [:div.center.footer-center "© 2014 ООО «BONO»"]]))
