(in-ns 'gd.views.themes.bono.basic)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; User profile dropdown
(defn profile-dropdown-icon[name]
  (list
   [(str "." name "-icon")
    :margin 10
    (bgimage (str "/img/themes/bono/icon/" name ".png"))]

   [(str ".dropdown-icon:hover ." name "-icon")
    :margin 10
    (bgimage (str "/img/themes/bono/icon/" name "_hover.png"))]))

(defn- user-profile-dropdown-css[]
  (list
   [".profile-dropdown:hover .profile-menu" :display :block]
   [".profile-dropdown" :background-image "url('/img/themes/bono/user_button.png')" :border "1px solid #405500" :border-radius 4
    :box-shadow "0 1px 3px 2px #5C771E" :height 33 :position :absolute :right 62 :top 6 :width 191 :cursor :pointer
    [".profile-icon" :margin "8px 11px" (bgimage "/img/themes/bono/icon/profile.png")]
    [".profile-title" :line-height 36 :color "#f9ffd9"]
    [".profile-arrow" :margin "14px 13px 0 0" (bgimage "/img/themes/bono/user_arrow.png")]
    [".profile-menu:hover" :display :block]
    [".profile-menu" :left 0 :top -2 :border-radius 4 :background-color "white" :border "1px solid #7e7e7e" :box-shadow "0 0 6px -2px black" :z-index "400"
     [".dropdown-icon" :height 40 :line-height 40]
     [".dropdown-icon:hover div" :color "#659300"]
     [".dropdown-icon:hover span" :color "#659300"]]]
   [".tooltip-triangle" (bgimage "/img/themes/bono/tooltip_triangleLK.png") :left 163 :top -8]
   (profile-dropdown-icon "orders")
   (profile-dropdown-icon "settings")
   (profile-dropdown-icon "exit")))

(defn- user-profile-dropdown[]
  (if (sec/logged)
    [:div.profile-dropdown.dropdown
     [:div.group
      [:div.profile-icon.left]
      [:div.profile-title.left "Личный кабинет"]
      [:div.profile-arrow.right]]
     [:div.profile-menu.relative.none {:style "width:220px;"}
      [:div.tooltip-triangle.absolute]
      [:a.dropdown-icon.border-bottom.group {:href (link:orders)}
       [:div.left.orders-icon] [:div.left "Мои заказы"]]
      [:a.dropdown-icon.border-bottom.group {:href (link:profile)}
       [:div.left.settings-icon] [:div.left "Настройки профиля"]]
      [:div.dropdown-icon.group {:onclick (action:logout)}
       [:div.left.exit-icon]
       [:div.left "Выход"]]]]
    [:div.profile-dropdown
     [:div.profile-title.text-center {:onclick (modal-window-open true)} "Войти на сайт"
      (design:registration-modal)]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Header
(defn- header-css[]
  (list
   [".left-ribbon" (bgimage "/img/themes/bono/ribbon_red_left.png")]
   [".right-ribbon" (bgimage "/img/themes/bono/ribbon_red_right.png")]
   [".center-ribbon" (bgimage "/img/themes/bono/ribbon_red_middle.png") :width 570]
   [".text-on-ribbon" :color "white" :font-size 32 :padding-top 15 :line-height 47 :height 47 :text-shadow "1px 1px #000000" :top 22 :left 290]
   [".header-bottom" :top -40 :margin-right 50]
   [".header-left-bottom" (bgimage "/img/themes/bono/phone_bg_left.png")]
   [".header-center-bottom" :background-color "#749626" :width 755 :height 50]
   [".header-right-bottom" (bgimage "/img/themes/bono/phone_bg_right.png")]
   [".header-menu a" :color "#dff58d"]
   [".header-menu a:hover" :color "green"]
   [".phone-icon" (bgimage "/img/themes/bono/phone_icon.png")]
   [".phone-number-img" :margin-top 8 (bgimage "/img/themes/bono/number.png")]
   [".phone" :color "#ecff89"]
   [".search-field"
    :background-color "#D5EA6C"
    :border "1px solid #EBFFA2"
    :color "#717171"
    :height 30
    :margin "8px 20px"
    :padding "2px 35px 3px 10px"
    :width 395
    :vertical-align :middle
    :outline "0 none"
    :border-radius 20
    :line-height 29]
   [".search-icon" :position :absolute :left 387 :top 14 (bgimage "/img/themes/bono/search_icon.png")]))

(defn- header[]
  [:div.fixed-group.center.relative
   [:div.group.header-menu {:style "width:1125px; padding:5px 0 1px;"}
                                        ;[:div.right.margin15r [:a {:href "#"} "Вопросы и ответы"]]
    [:div.right.margin15r [:a {:href "/guestbook/"} "Отзывы"]]
    [:div.right.margin15r [:a {:href "/contacts/"} "Контакты"]]
    [:div.right.margin15r [:a {:href "/news/"} "Новости"]] [:div.right.margin15r
                                                            [:a {:href "/pages/dostavka_i_oplata/"} "Условия сотрудничества"]]
    [:div.right.margin15r [:a {:href "/pages/about/"} "О компании"]]]
   [:div.fixed-group {:style "width:1220px;"}
    [:div.left.left-ribbon]
    [:div.left.center-ribbon]
    [:div.left.right-ribbon]] [:p.absolute.text-on-ribbon "Трикотаж от производителя, заказ от 300 грн."]
   [:div.fixed-group.header-bottom.relative.align-right
    [:div.left.header-left-bottom] [:div.left.header-center-bottom
                                    [:div.phone-icon.left]
                                    [:div.phone-number-img.left]
                                    [:div.relative.left
                                     (text-field
                                      {:class "search-field"
                                       :autocomplete "off"
                                       :placeholder "Поиск"}
                                      :search (info:current-search-query))
                                     [:div.search-icon]]]
    [:div.left.header-right-bottom (user-profile-dropdown)]]
   [:div.absolute.logo.clickable {:onclick (js (set! location.href "/"))}]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Top menu
(defn- top-menu-css[]
  (list
   [".sale,.new" :color :white :line-height 48]
   [".new" :width 68 :height 48 :padding "0 10px"]
   [".new:hover" :background-image "url('/img/themes/bono/horisontal_menu_green_select.png')" :height 48 :width 68 :cursor :pointer :padding "0 10px"]
   [".sale" (bgimage "/img/themes/bono/horisontal_menu_red_sticker.png")
    [".text" :padding-left 32]]
   [".sale:hover" (bgimage "/img/themes/bono/horisontal_menu_red_sticker_select.png") :cursor :pointer]
   [".bucket-block" :width 185 :border-radius "0 3px 0 0" :font-family "'Calibri', Sans serif" (grad-image "/img/themes/bono/horisontal_menu_orange.png")
    [".dropdown-menu" :margin-top 10]
    [".text" :color :white :padding-left 60 :font-size 14 :line-height 13 :padding-left 41 :padding-top 5]
    [".icon" :left -4 (bgimage "/img/themes/bono/basket.png") :position :absolute]
    [".inline-bucket" :background-color "#FFFFFF" :border-radius 4 :box-shadow "0 0 5px 1px #737373" :left -66 :top 48 :width 380 :z-index "10" :border "1px solid #7e7e7e"
     [".scroll-top-img, .scroll-bottom-img" :height 27]
     [".scroll-top-img" :background-image "url('/img/themes/bono/basket_tooltip_scroll_bg_top.png')" :border-radius "4px 4px 0 0"]
     [".scroll-top-img:hover" :background-image "url('/img/themes/bono/basket_tooltip_scroll_bg_top_hover.png')"]
     [".scroll-bottom-img" :background-image "url('/img/themes/bono/basket_tooltip_scroll_bg_bottom.png')" :border-radius "0 0 4px 4px"]
     [".scroll-bottom-img:hover" :background-image "url('/img/themes/bono/basket_tooltip_scroll_bg_bottom_hover.png')"]
     [".scroll-top-arrow-img" (bgimage "/img/themes/bono/basket_tooltip_scroll_arrow_top.png") :left 178 :top 5]
     [".scroll-bottom-arrow-img" (bgimage "/img/themes/bono/basket_tooltip_scroll_arrow_bottom.png") :left 178 :bottom 5]
     [".button-text" :font-size 17]]]
   [".bucket-block:hover .inline-bucket-menu" :display :block]
   [".inline-bucket-menu:hover" :display :block]
   [".icon:hover .inline-bucket-menu" :display :block]))

(declare inline-bucket)

(defmethod themes:render [:basic :inline-bucket] [_]
  [:div.inline-bucket-dropdown
   [:div.text
    [:div "В корзине"]
    [:div "товаров: " (info:bucket-count) " шт"]
    [:div "на сумму: " (info:bucket-sum) " грн"]]
   [:div.inline-bucket-menu.none [:form (inline-bucket)]]])

(defn- top-menu[]
  [:div.top-categories.group.fixed-panel {:style "z-index:200;"}
   [:div.left {:style (css-inline :width 640)}
    [:span.left.categories-separator {:style (css-inline :margin-left 10)}]
    (with-subtheme :top (link:categories-tree))]
   [:div.right
    [:div.left.sale [:a.text {:href "/sale/"} "Распродажа"]]
    [:div.left.new [:a {:href "/new/"} "Новое"]]
    [:div.left.bucket-block.container
     [:div.icon]
     (design:inline-bucket)]]])
