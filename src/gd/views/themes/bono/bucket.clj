(in-ns 'gd.views.themes.bono.basic)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Inline bucket

(defn- inline-bucket-css[]
  (list
   [".inline-bucket" :font-size 14
    [".spinner-element input" :padding "1px 5px" :font-size 14]
    [".spinner-button" :display :none]
    [".container" :max-height 300 :overflow :auto]
    [".item" :background-color "#F2F0F0" :padding 5 :margin-bottom 5 :color "#433434"
     [".item-name, .item-value" :height 15 :line-height 15 :vertical-align :middle]
     [".item-name" :width 75]
     [".item-value" :width 50]]
    [".item:hover" :background-color "#ecf5bd"]
    [".bucket-input" :border "1px solid #949494" :border-radius 3 :font-size 12 :padding 2 :vertical-align :middle :width 35]]))

(defn- inline-bucket[]
  [:div.absolute.inline-bucket
   [:div.tooltip-triangle.absolute]
   [:div.relative {:style "overflow:auto; max-height:207px; min-height:10px; padding:5px 5px 0 5px;"}
    (with-subtheme :inline
      (design:bucket))]
   [:div.bucket-sum.margin10r "Итого: " (info:bucket-sum) " грн."]
   [:div.group.margin5
    [:div.margin5r.left (orange-button "Перейти в корзину" :href "/bucket/" :width 162)]
    [:div.left (red-button "Создать заказ" :width 162 :action (action:open-order-modal))]]])

(defmethod themes:render [[:inline :basic] :bucket-item] [_ {:keys [stock quantity params id sum] :as item}]
  [:div.group.item.form
   [:div.left
    [:img.margin5r {:src (images:get :stock-medium (first (:images stock)))}]]
   [:div.left
    [:div.bucket-stock-name.clickable {:onclick (js (set! location.href (clj (link:stock-link stock))))} (:name stock)]
    [:div
     [:div.table-row
      [:div.table-cell.item-name "Размер:"] [:div.table-cell.item-value (:size params)]
      [:div.table-cell.item-name "Количество:"]
      [:div.table-cell.item-value
       (spinner
        :quantity
        :width 40
        :onchange (action:change-item item)
        :value quantity)]]
     [:div.table-row
      [:div.table-cell.item-name "Цена:"] [:div.table-cell.item-value (:price stock)]
      [:div.table-cell.item-name "Сумма:"] [:div.table-cell.item-value sum " грн."]]]]
   [:div.left.delete {:style "margin-top:17px;" :onclick (action:delete-item item)}]])

(defmethod themes:render [:basic :stock-modal] [_ {:keys [stock quantity params id sum] :as item}]
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bucket
(defn- bucket-css[]
  (list
   [".bucket-sum" :color "#dc5920" :font-size 18 :text-align :right]
   [".bucket-stock-name" :color "#6a9709" :text-decoration "underline" :line-height 15]
   [".controls-link" :color "#659300" :font-size 18 :font-family "'Cuprum', Sans serif"
    ["p" :border-bottom "1px solid #659300" :margin-bottom 0 :line-height 15]]
   [".controls-link:hover" :color "#068306" :cursor "pointer" :border-color "#068306"]
   [".check-icon" (bgimage "/img/themes/bono/check.png") :background-repeat :no-repeat]
   [".controls-link:hover .check-icon" (bgimage "/img/themes/bono/check_hover.png") :background-repeat :no-repeat]
   [".delete-icon" (bgimage "/img/themes/bono/delete.png") :background-repeat :no-repeat]
   [".controls-link:hover .delete-icon" (bgimage "/img/themes/bono/delete_hover.png") :background-repeat :no-repeat]
   [".arrow-left-icon" (bgimage "/img/themes/bono/arrow_left.png") :background-repeat :no-repeat]
   [".controls-link:hover .arrow-left-icon" (bgimage "/img/themes/bono/arrow_left_hover.png" :background-repeat :no-repeat)]
   [".printer-icon" (bgimage "/img/themes/bono/printer.png") :background-repeat :no-repeat]
   [".controls-link:hover .printer-icon" (bgimage "/img/themes/bono/printer_hover.png" :background-repeat :no-repeat)]
   [".top" :background-color :white :padding "15px 15px 5px" (border-radius :left-top 4 :right-top 4)]
   [".controls" :background-image "url('/img/themes/bono/basket_footer.png')" :height 63 (border-radius :right-bottom 4 :left-bottom 4) :margin "0 -10px -10px"
    [".inner" :padding 10]]
   [".bucket-header" :font-family "'Cuprum', sans-serif" :font-size 28 :color "#659300"]
   [".note-text" :color "#595858" :font-size 17]
   [".items" :border-collapse "separate" :border-spacing "0 2px" :margin-top 10 :width "100%"]
   [".items-header" :background-color "#B7D2A2"
    ["td" :color "#595858" :font-style "italic" :padding 5]]
   ["tr.form:hover td" :background-color "#ecf5bd"]
   [".form"
    ["td" :background-color "#e8e8e8" :padding "5px 0"]]
   [".delete" :cursor "pointer" (bgimage "/img/themes/bono/cross_small.png")]
   [".delete:hover" (bgimage "/img/themes/bono/cross_small_hover.png")]
   [".delete:active" (bgimage "/img/themes/bono/cross_small_clicked.png")]
   [".fixed-25px-width-cell" :min-width 25]
   [".fixed-50px-width-cell" :min-width 50]
   [".fixed-75px-width-cell" :min-width 75]
   [".fixed-100px-width-cell" :min-width 100]
   [".fixed-125px-width-cell" :min-width 125]
   [".fixed-150px-width-cell" :min-width 150]
   [".fixed-200px-width-cell" :min-width 200]
   [".full-width-cell" :width "100%"]
   [".edit-small" (bgimage "/img/themes/bono/edit_small.png") :cursor :pointer]
   [".edit-small:hover" (bgimage "/img/themes/bono/edit_small_hover.png")]
   [".edit-small:active" (bgimage "/img/themes/bono/edit_small_clicked.png")]))

(defmethod themes:render [:basic :bucket-item] [_ {:keys [stock quantity params id sum] :as item}]
  [:tr.form
   [:td.text-center.padding5 (custom-checkbox)]
   [:td
    [:img.left {:style "margin:0 15px 0 5px;" :src (images:get :stock-medium (first (:images stock)))}]
    [:a.bucket-stock-name {:href (link:stock-link stock)} (:name stock)]
    [:div.margin10t (str "Цена за шт.: " (:price stock) " грн.")]]
   [:td [:div.dropdown {:style (css-inline :width 60)}
         (custom-dropdown {:onchange (action:change-item item)} :size
                          (get-in stock [:params "size"])
                          (:size params))]]
   [:td (text-field {:class "input margin5l"
                     :style "width: 70px;"
                     :autocomplete :off
                     :onchange (action:change-item item)}
                    :quantity quantity)]
   [:td [:div.margin5l sum " грн."]]
   [:td [:div.delete {:onclick (action:delete-item item)}]]])

(defmethod themes:render [:basic :bucket-main] [_]
  (page-layout
   [:div.padding10
    [:div.note-text "Обратите внимание, Вы авторизованы как " (:name (sec/logged))]
    [:table.items
     [:tr.items-header
      [:td "&nbsp;"]
      [:td "Наименование и цена за шт."]
      [:td "Размер"]
      [:td "Количество"]
      [:td "Сумма"]
      [:td "&nbsp;"]]
     (design:bucket)]
    [:div.border-bottom {:style "height:3px;"}]
    [:div.bucket-sum.margin5b "Итого: " (info:bucket-sum) " грн."]
    [:div.controls
     [:div.group
      [:div.controls-link.left.margin20t.margin20l
       [:div.inline-block.check-icon.margin5r.valign-middle]
       [:p.inline-block.valign-middle "Отметить все"]]
      [:div.controls-link.left.margin20t.margin20l
       [:div.inline-block.delete-icon.margin5r.valign-middle]
       [:p.inline-block.valign-middle "Удалить отмеченные"]]
      [:div.right.margin10t.margin15l.margin15r.margin15b
       (red-button "Создать заказ" :width 150 :action (action:open-order-modal))]]]]))

(defmethod themes:render [:basic :order-item] [_ {:keys [stock quantity params id sum
                                                         booked-until closed original-quantity payment-status]
                                                  :as item}]
  (let [{:keys [name price images]} stock]
    [:tr.form
     [:td.fixed-25px-width-cell.text-center {:style "padding-right:5px;"}
      (custom-checkbox)]
     [:td.fixed-50px-width-cell
      [:a {:href (link:stock-link stock)}
       (sized-image {:data-title (tooltip [:table
                                           [:tr
                                            (map (fn[img]
                                                   [:td (image (images:get :stock-large img))])
                                                 images)]])}
                    (images:get :stock-medium (first images)))]]
     [:td.full-width-cell {:style "padding-left:10px;"}
      [:div [:a.bucket-stock-name.bold {:href (link:stock-link stock)}
             name]]
      [:div [:span.bold "Размер: "] [:span (:size params)]]
      [:div [:span.bold "Цена за шт.: "] [:span price] [:span "грн"]]]

     [:td.fixed-125px-width-cell.text-center original-quantity]

     [:td.fixed-200px-width-cell.text-center
      (when (> quantity 0) [:div quantity " шт. - отложено"])
      (when (> closed 0) [:div {:style "color:#dc5920;"} closed " шт. - нет в наличии"])]

     [:td.fixed-125px-width-cell
      [:div.text-center
       [:span.inline-block sum]
       [:span.inline-block " грн."]]]

     [:td.fixed-200px-width-cell.text-center
      (cond (and (= :created (order-status)) (not= :payed payment-status) booked-until)
            (format-date-interval (tm/from-date booked-until))

            (= :payed payment-status)
            "Оплачено"

            true
            (! :retail-order.status (order-status)))]

     [:td.fixed-50px-width-cell.text-center
      (if (#{:created :in-work} (order-status))
        (list
         [:div.margin5b.delete {:onclick (action:remove-order-item item)}]
         [:div.relative.edit-small {:data-placement "left"
                                    :data-animation "am-flip-x"
                                    :onclick (js (openPopover this))}
          (popover
           "Редактировать заказ"
           [:div.padding5.form {:style "width:208px;"}
            [:div.group.margin10b
             [:div.left {:style (css-inline :line-height 26)} "Количество"]
             [:div.left.margin10l {:style (css-inline :width 108)}
              (text-field {:style (css-inline :height 26 :font-size 18)} :quantity original-quantity)]]
            [:div.cuprum.font18px.fixed-200px-width-cell {:style "margin:0 0 -4px -2px;"}
             (green-button "Применить изменения" :width 180 :action (action:edit-order-item item))]])]))]]))

(defmethod themes:render [:basic :order]
  [_ {:keys [id changed status user-type additional-info sum user-order-item
             discount sum-with-discount payment-expenses summary-sum] :as order}]
  [:div.padding10
   [:div.bono-order-container.group
    [:span.left {:style "font-size:18px;"} "№" id " - "]
    [:span.red {:style "font-size:18px; margin-left:5px;"}
     (! :retail-order.status status)]
    [:span.right.margin5r {:style "color:#909090"} (format-date changed)]]

   [:table.items
    [:tr.items-header
     [:td {:colspan 3} "Наименование, размер, цена за шт."]
     [:td.text-center "Заказано"]
     [:td.text-center "Забронировано"]
     [:td.text-center "Сумма"]
     [:td.text-center "Статус"]
     [:td "&nbsp;"]]
    (design:order-items order)]
   [:div.border-bottom {:style "height:3px;"}]
   [:div.bucket-sum.margin5b.margin10r "Итого: " sum " грн."]
   [:div.controls
    [:div.group
     [:a.block.controls-link.left.margin20t.margin20l {:href (link:all-stocks)}
      [:div.inline-block.arrow-left-icon.margin5r.valign-middle]
      [:p.inline-block.valign-middle "Вернуться к покупкам"]]
     [:div.controls-link.left.margin20t {:style "margin-left:150px;"}
      [:div.inline-block.check-icon.margin5r.valign-middle]
      [:p.inline-block.valign-middle "Отметить все"]]
     [:div.controls-link.left.margin20t.margin20l
      [:div.inline-block.delete-icon.margin5r.valign-middle]
      [:p.inline-block.valign-middle "Удалить отмеченные"]]
     [:a.block.controls-link.right.margin20t.margin20r
      {:href (link:order-print id)}
      [:div.inline-block.printer-icon.margin5r.valign-middle]
      [:p.inline-block.valign-middle "Распечатать заказ"]]]]])

(defmethod themes:render [:basic :orders] [_ ]
  (common-layout
    [:div
     [:div.main-list-body.margin10b
      (top-menu)
      (design:breadcrumbs)
      [:h1.cuprum.green.margin15l "Заказы"]
      [:div.panel (design:orders)]]]))
