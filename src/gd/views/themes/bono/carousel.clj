(in-ns 'gd.views.themes.bono.basic)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Carousel
(defn- carousel-css[]
  (list
   [".carousel-wrapper" :width 1050 :background-color "#f3f3f3"
    [".img-name" :color "#749800" :display :block :width 198 :font-weight :bold :padding "5px 5px 0"]
    [".img-brand" :margin-bottom 40 :width 198 :padding "0 5px"]
    [".button" :top 50]
    [".carousel-info" :padding "10px 20px"]
    (simple-buttons ".carousel-control.left" "/img/themes/bono/arrow_slider_left")
    (simple-buttons ".carousel-control.right" "/img/themes/bono/arrow_slider_right")
    [".info-carousel .prev, .info-carousel .next" :position :absolute :top 110 :background-color :transparent :z-index "9"]
    [".carousel-control" :top "35%"]
    [".carousel-control.right" :right 0]
    [".carousel-control.left" :left 0]
    [".carousel-control.left:hover, .carousel-control.right:hover" :background-color :transparent]]
   [".col-xs-3" :padding-left 26]))

(defn carousel[]
  [:div.carousel-wrapper
   (region* delayed-carousel[]
     (when *in-remote*
       [:div.carousel-info
        [:div#myCarousel.carousel.slide {:data-ride "carousel"}
         [:div.carousel-inner
          (map-indexed
           (fn[i {:keys [id images name all-params] :as stock}]
             (let [{:keys [brand size color]} all-params]
               [:div (if (== i 0) {:class "item active"} {:class "item"})
                [:div.col-xs-3
                 [:a {:href (link:stock-link stock)}
                  [:img.img-responsive {:src (images:get :stock-large (first images))}]
                  [:div.img-name name]
                  [:div.img-brand "Производитель: " brand]]]]))
           (retail:supplier-stocks 10 0 {}))]

         [:a.left.carousel-control {:href "#myCarousel" :data-slide "prev"}]
         [:a.right.carousel-control {:href "#myCarousel" :data-slide "next"}]]]))
   (document-ready (rerender :delayed-carousel))])
