(in-ns 'gd.views.themes.bono.basic)

(defmethod themes:render [:basic :settings] [_]
  (common-layout
    [:div.main-list-body.margin10b
     (top-menu)
     (design:breadcrumbs)
     [:h1.cuprum.green.margin15l
      (:name (last *current-breadcrumbs*))]
     [:div.panel
      (user-info-modal :user-profile true)]]))
