(in-ns 'gd.views.themes.bono.basic)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Categories
(defn- categories-css[]
  (list
   [".top-categories,.left-categories"
    ["ul" :list-style-type :none :margin 0]
    ["li" :list-style :none]]
   [".top-categories"
    [".level1" :float :left]
    ["a" :display :block :height 48 :line-height 48 :color :white :text-decoration :none]]
   [".left-categories"
    ["a" :display :block :height 30 :line-height 30 :color :white :text-decoration :none :padding-left 10 :border-bottom "1px dotted #d0d8da"]
    ["a:hover, .selected-category > a" :background-color "#ecf5bd" :color "#659300 !important"]]
   ;; Top categories
   [".top-categories" :border-radius "3px 3px 0 0" :font-family "'Cuprum', sans-serif" :font-size 18 (grad-image "/img/themes/bono/horisontal_menu_green.png")]
   [".top-categories .categories-tree"
    ["a" :cursor :pointer]
    [".category-link" :color :white]
    [".level1" :height 48 :line-height 48 :float "left"]
    [".list-level2" :line-height 16 :display "none" :position :absolute :z-index "10" :background-color :black]]
   [".category-panel" :padding "0 15px"]
   [".category-panel:hover"
    :background-image "url('/img/themes/bono/horisontal_menu_green_select.png')"
    :height 48
    :cursor :pointer
    :padding "0 15px"]
   [".categories-separator" :margin-top 9 (bgimage "/img/themes/bono/divide_menu.png")]
   ;; Left categories
   [".left-categories" :width 210
    [".list-level1 li:first-child" :border-radius "3px 3px 0 0"]
    [".list-level1 li:last-child ul" :border-radius "0 0 3px 3px"]
    [".not-current-category.level1" :display :none]
    [".empty-parent-category .level1" :display :block]
    [".category-link" :color :white :font-size 14]
    [".level1" :background-image "url('/img/themes/bono/horisontal_menu_red.png')" :border-radius "0 0 3px 3px" :font-size 16]
    [".level1 > a" :padding-left "10px !important" :color "white !important" :border-bottom :none :font-size 16 :background-color "transparent !important"]
    [".level1 > a:hover" :color "white !important" :border-bottom :none :background-color "#a82d10"]
    [".list-level1" :border "1px solid #d9d9d9" :padding 0 :margin-left 10 :border-radius 3]
    [".list-level2" :background-color :white :padding "0 0 10px"
     [".category-link" :color "#595858"]]
    [".level2"
     [".category-link" :color "#333333"]]
    [".level3"
     [".category-link" :color "#595858"]]
    [".level4"
     [".category-link" :color "#737373"]]]))

(defmethod themes:render [[:top :basic] :category-child] [_ {:keys [name path level] :as category}]
   (list
   (link-to {:class "category-link category-panel left"}
                 (link:category-link category) name)
        [:div.right.categories-separator]))

(defmethod themes:render [:basic :category-child] [_ {:keys [name path level] :as category}]
  (link-to {:class :category-link :style (str "padding-left:" (* 10 (- level 1)) "px;")} (link:category-link category) name))
