(in-ns 'gd.views.themes.bono.basic)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bono modal
(defn- bono-modal-css[]
  (list
   [".cuprum" :font-family "'Cuprum',sans-serif"]
   [".dialog-content" :display :none]
   [".bono-modal" :background-color :white :border-radius 6 :position :relative :display :inline-block]
   [".modal-close"
    :background "none repeat scroll 0 0 transparent"
    :border "0 none"
    :cursor "pointer"
    :padding "0"
    :color "#000000"
    :font-size "36px"
    :font-weight "bold"
    :opacity "0.5"
    :text-shadow "0 1px 0 #FFFFFF"
    :background-image "none !important"
    :text-align "center"
    :height "20px !important"
    :width "20px !important"
    :position "absolute"
    :right "20px"
    :top "3px"]

   [".input"
    :border-radius "3px"
    :color "#555555"
    :font-size 16
    :display "inline-block"
    :padding "3px 6px"
    :vertical-align "middle"
    :background-color "#FFFFFF"
    :transition "border 0.2s linear 0s, box-shadow 0.2s linear 0s"
    :border-image "none"
    :border-style "solid"
    :border-width "1px"
    :width "100%"
    :border-color "#AAAAAA #C4C4C4 #C4C4C4"
    :border-right "1px solid #C4C4C4"
    :box-shadow "0 1px 0 rgba(255, 255, 255, 0.9)"]

   [".small-input" :font-size 13]
   [".big-input" :font-size 18]
   [".form-control" :font-size 16]

   [".fields-block" :width 380 :margin "0 20px"]
   [".width350px" :width 350]
   [".width390px" :width 390]
   [".modal-link" :color "#659300" :font-family "'Cuprum',sans-serif" :font-size 18 :text-decoration :underline :cursor :pointer :height 28 :line-height 28]
   [".bono-modal-footer"
    :background-color "#f3f3f3"
    :border-top "1px dashed #c3c8c9"
    :color "#8f8f8f"
    :font-size 19
    (border-radius :left-bottom 6 :right-bottom 6)]
   ["#ukraine .dk_container" :width "100%" :font-size 16]
   ["#ukraine .dk_toggle" :width "100%" :padding "3px 12px 3px 5px"]
   ["#ukraine .dk_label" :background-position "98% center"]
   ["#ukraine .dk_options" :margin-top "10px !important"]
   [".border-bottom" :border-bottom "1px solid #ebebeb"]
   [".social-btn" :margin 15]
   [".facebook-btn" (bgimage "/img/themes/bono/buttons/facebook.png")]
   [".facebook-btn:hover" (bgimage "/img/themes/bono/buttons/facebook_hover.png")]
   [".facebook-btn:active" (bgimage "/img/themes/bono/buttons/facebook_clicked.png")]
   [".vk-btn" (bgimage "/img/themes/bono/buttons/vk.png")]
   [".vk-btn:hover" (bgimage "/img/themes/bono/buttons/vk_hover.png")]
   [".vk-btn:active" (bgimage "/img/themes/bono/buttons/vk_clicked.png")]
   [".ok-btn" (bgimage "/img/themes/bono/buttons/ok.png")]
   [".ok-btn:hover" (bgimage "/img/themes/bono/buttons/ok_hover.png")]
   [".ok-btn:active" (bgimage "/img/themes/bono/buttons/ok_clicked.png")]
   [".mail-btn" (bgimage "/img/themes/bono/buttons/mail.png")]
   [".mail-btn:hover" (bgimage "/img/themes/bono/buttons/mail_hover.png")]
   [".mail-btn:active" (bgimage "/img/themes/bono/buttons/mail_clicked.png")]
   [".google-btn" (bgimage "/img/themes/bono/buttons/google.png")]
   [".google-btn:hover" (bgimage "/img/themes/bono/buttons/google_hover.png")]
   [".google-btn:active" (bgimage "/img/themes/bono/buttons/google_clicked.png")]
   [".bono-modal .slider-right, .bono-modal .slider-left" :display "block"]
   [".bono-modal .slider-right, .bono-modal .slider-left" :top 300]))

(defn- remember-pass[]
  (add-singleton
   [:script#popoverRemPass {:type "text/ng-template"}
    [:div.popover.absolute.none {:style "width:250px;"}
     [:div.arrow]
      [:h3.popover-title.group
       [:p.left.margin10 "Восстановить пароль"]
       [:div.close.right.margin10 {:ng-click "$hide()"} "×"]]
     [:div.margin15.group
      [:div.left {:style "line-height:26px;"} "e-mail"]
      [:div.left.margin10l {:style "width:175px;"}
       (text-field {:class "input small-input"} :email)]]
     [:div.cuprum.font18px.margin5b
      (green-button "Получить пароль"
                    :width 220
                    :action (execute-form* remind-password[email]
                                           (user:remind-password email)
                                           :validate [(textarea-email "email" "Не правильный формат email")
                                                      (remote-validator "email"
                                                                        (remote* themes-email-restore
                                                                                 (when-not (user:check-email-exists (c* value))
                                                                                   (throwf "Email not exists")))
                                                                        "Указанный почтовый адрес не был <br/> зарегистрирован в системе")]
                                           :success (js*
                                                     (closeDialog)
                                                     (notify "Восстановление пароля" "Ссылка на восстановление пароля успешно отправлена на ваш e-mail"))))]]]))

(defn- login[]
  [:div.fields-block.left.form {:role :enter}
   [:div.margin10b.inline-block.left
    (text-field {:class "input big-input width350px" :placeholder "Укажите e-mail"} :login)]
   [:div.margin10b.inline-block.left
    (password-field {:class "input big-input width350px" :placeholder "Введите пароль"} :password)]
   [:div.margin10b.inline-block.modal-link.text-center.width.text-left.margin5l.left
    {:data-trigger "click"
     :data-template "popoverRemPass"
     :bs-popover true
     :data-placement "left"
     :data-animation "am-flip-x"}
    "Забыли пароль?"]
   [:div.margin10b.margin20t.margin20l.inline-block.width350px.left
   {:style "font-size:19px;" :role :submit}
    (red-button "Войти" :width 195 :action (action:login))]])

(defn- registration[]
  [:div.fields-block.left.form {:role :enter}
   [:div.margin10b.inline-block.right
    (text-field {:class "input big-input width350px" :placeholder "Укажите e-mail" :autocomplete "off"} :login)]
   [:div.margin10b.inline-block.right
    (password-field {:class "input big-input width350px" :placeholder "Введите пароль" :autocomplete "off"} :password)]
   [:div.margin10b.inline-block.right
    (password-field {:class "input big-input width350px" :placeholder "Повторите выбранный пароль" :autocomplete "off"} :password-repeat)]
   [:div.margin10b.margin20t.margin20l.inline-block.width350px.right
   {:style "font-size:19px;" :role :submit}
    (orange-button "Зарегистрироваться" :width 195 :action (action:register))]])

(defmethod themes:render [:basic :unregistered] [_]
  [:div
   (remember-pass)
   (lazy-modal-window
    bono-registration-modal-window
    [:div.bono-modal.form
     [:div.modal-close "×"]
     [:div.alert.text-center {:style "padding: 30px 30px 20px;"} "Для осуществления покупки необходима авторизация!"]
     [:div.title.group
      [:div.title-side.left "Войдите на сайт"]
      [:div.title-circle.left "или"]
      [:div.title-side.left "Зарегистрируйтесь"]]
     [:div.group.margin5l
      (login)
      (registration)]
     [:div.bono-modal-footer.group
      [:div.left {:style "height:39px; line-height:39px; margin:15px 0 0 15px;"}"Войти с помощью -"]
      [:div.left.social-btn.facebook-btn]
      [:div.left.social-btn.vk-btn]
      [:div.left.social-btn.ok-btn]
      [:div.left.social-btn.mail-btn]
      [:div.left.social-btn.google-btn]]])])

(def novaya-pochta (map str (range 1 100)))

(defn user-info-edit[{:keys [name contact address address-region extra-info]}]
  (let [edit? {:ng-show "editMode"}
        show? {:ng-hide "editMode"}]
    [:div.ng-cloak
     [:div.well.well-sm
      [:div.form-group
       [:label.margin5b "Фамилия и имя получателя:"]
       [:div show? name]
       [:div edit? (text-field {:placeholder "Петр Иванов" :autocomplete "off" :class "form-control"}
                               :name name)]]
      [:div.form-group
       [:label.margin5b "Контактный телефон:"]
       (let [phone (:value (:phone contact))]
         [:div show? phone]
         [:div edit? (text-field {:placeholder "066 123 45 67" :autocomplete "off" :class "form-control"}
                                 :phone phone)])]]

     [:div.margin10t (text-field {:ng-model "deliveryType" :class "none"} :delivery)
      (tabbed-panel
       {:model "deliveryType" :disabled (js* (! editMode))}
       (tab :ukraine "Украина"
            [:div.well.well-sm.margin10t
             [:div.form-group
              [:label "Город и область:"]
              [:div show? address "(" address-region ")"]
              [:div.group edit?
               [:div.left.orange-dropdown (district-input address-region)]
               [:div.left.margin10l (city-input address)]]]

             [:div.form-group
              [:label "Номер отделения \"Новой почты\":"]
              [:p.group show? (:novaya_pochta extra-info)]
              [:div.group edit? (novaya-pochta-input (:novaya_pochta extra-info))]]])
       (tab :other "Другие страны"
            [:div.well.well-sm.margin10t
             [:div.form-group
              [:label "Полный адрес доставки:"]
              [:div show? address]
              [:div edit? (text-field {:placeholder "69097, Россия, г.Москва, пер.Литейный, стр.21, кв.15"
                                       :autocomplete "off" :class "form-control"} :full-address address)]]]))]]))

(defn- current-user[]
  (when (sec/id)
    (user:get-full (sec/id))))

(wrap-request-cache #'current-user)

(defn- user-info-modal[& {:keys [user-profile]}]
  (let [{:keys [name contact address address-region extra-info]} (current-user)
        edit? {:ng-show "editMode"}
        show? {:ng-hide "editMode"}]
    [:div.bono-modal.form {:style "width:100%;"
                           :ng-init (js (set! address {:district (clj address-region)
                                                       :city (clj address)
                                                       :novayaPochta (clj (:novaya_pochta extra-info))})
                                        (set! editMode (clj (case (:delivery extra-info)
                                                              "other" (empty? address)
                                                              "ukraine" (empty? (:novaya_pochta extra-info))
                                                              true)))
                                        (set! deliveryType (clj (or (:delivery extra-info) "ukraine"))))}
     [:div.modal-close "×"]
     [:div.padding10 (user-info-edit (current-user))]
     [:div.bono-modal-footer.group
      [:div.left.margin20l.margin15t.margin15b show?
       (green-button "Изменить данные"
                     :width 220
                     :ng-action (js (set! editMode true)))]

      [:div.left.margin20l.margin15t.margin15b edit?
       (green-button "Отмена"
                     :width 220
                     :ng-action (js (set! editMode false)))]

      [:div.right.modal-button.margin20r.margin15t.margin15b show?
       [:div.order-button
       (red-button "Подтвердить заказ"
                   :width 220
                   :action (action:create-order))]]

      [:div.right.panel-button.margin20r.margin15t.margin15b edit?
       [:div (when-not user-profile {:class "order-button"})
       (red-button "Сохранить изменения"
                   :width 350
                   :action (if user-profile (action:update-user) (action:create-order)))]]

      [:div.right.modal-button.margin20r.margin15t.margin15b edit?
       [:div.order-button
       (red-button "Сохранить изменения и подтвердить заказ"
                   :width 350
                   :action (action:create-order))]]]]))

(defmethod themes:render [:basic :registered-with-no-info] [_]
  (modal-window-without-header {:width 850} (user-info-modal)))

(defmethod themes:render [:basic :registered-with-filled-info] [_]
  (modal-window-without-header {:width 850} (user-info-modal)))
