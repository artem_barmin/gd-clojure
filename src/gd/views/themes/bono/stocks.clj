(in-ns 'gd.views.themes.bono.basic)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stocks
(defn- stocks-css[]
  (list
   [".slider-left, .slider-right" :top 119 :display "none"]
   [".slider-right" :right 3 (bgimage "/img/themes/bono/arrow_slider_right_small.png")]
   [".slider-right:hover" (bgimage "/img/themes/bono/arrow_slider_right_hover_small.png")]
   [".slider-right:active" (bgimage "/img/themes/bono/arrow_slider_right_clicked_small.png")]
   [".slider-left" :left 3 (bgimage "/img/themes/bono/arrow_slider_left_small.png")]
   [".slider-left:hover" (bgimage "/img/themes/bono/arrow_slider_left_hover_small.png")]
   [".slider-left:active" (bgimage "/img/themes/bono/arrow_slider_left_clicked_small.png")]
   [".big-slider.slider-right" :right 3 (bgimage "/img/themes/bono/arrow_slider_right.png")]
   [".big-slider.slider-right:hover" (bgimage "/img/themes/bono/arrow_slider_right_hover.png")]
   [".big-slider.slider-right:active" (bgimage "/img/themes/bono/arrow_slider_right_clicked.png")]
   [".big-slider.slider-left" :left 3 (bgimage "/img/themes/bono/arrow_slider_left.png")]
   [".big-slider.slider-left:hover" (bgimage "/img/themes/bono/arrow_slider_left_hover.png")]
   [".big-slider.slider-left:active" (bgimage "/img/themes/bono/arrow_slider_left_clicked.png")]
   [".header" :background-color "#e8e8e8" :font-weight :bold :height 25 :line-height 25 :padding "0 5px"]
   [".name" :color "#659300" :font-size 18 :font-style "italic" :padding "5px 0" :width 163 :text-decoration "underline"]
   [".measurement-info" :margin-left 55 :text-align "left"
    [".measure-name" :display "inline-block"]]
   [".model-icon" :margin "0 10px 5px 0" (bgimage "/img/themes/bono/model_icon.png")]
   [".top-stock-block:hover"
    [".fast-order-panel,.clickable-layer" :display "block"]]
   [".top-stock-block" :padding 7 :border-radius 3 :background-color :white :border "1px solid #d9d9d9"
    [".price" :color :green :font-size 22 :font-style "italic"]
    [".delimeter" :border-bottom "1px solid #D8D8A8" :margin "10px -10px"]
    [".info-panel" :height 25 :margin-top 5
     [".text" :font-size 15 :width 163 :line-height 14]]
    [".fast-order-panel" :height 25 :margin-top 5]
    [".lens-size" (bgimage "/img/themes/bono/lens_size.png")]
    [".lens-size:hover" (bgimage "/img/themes/bono/lens_size_hover.png")]
    [".lens-size:active" (bgimage "/img/themes/bono/lens_size_clicked.png")]
    [".description" :width 270 :min-height 350]
    [".popover"  :border "1px solid #5AB01A" :border-radius "6px" :box-shadow "0px 0px 4px 0px #709b12"
     [".close" :display :none]]
    [".small-ok-button-plain" :font-size 12 :text-shadow "1px 1px 1px rgba(0, 0, 0, 0.5)"]]
   [".breadcrumbs-home-icon" (bgimage "/img/themes/bono/home_icon.png") :left 14 :position "absolute" :top 59]
   [".breadcrumbs" :border-bottom "1px solid #C7C7C7" :margin "5px 10px 7px" :padding "0 0 1px 20px"
    [".breadcrumb-separator" (bgimage "/img/themes/bono/breadcrumbs_arrow.png") :display :inline-block :margin-left 7 :margin-right 7]
    ["a" :color "#595858"]
    [".last-item" :color "#909090"]]
   [".filter-header .glyphicon-arrow-up" :display :inline-block]
   [".filter-header.collapsed .glyphicon-arrow-down" :display :inline-block]
   [".filter-header.collapsed .glyphicon-arrow-up" :display :none]
   [".filter-block" :margin-bottom 6
    ["input[type='radio'], input[type='checkbox']" :margin-top 0 :margin-right 5 :width 15 :height 20 :vertical-align :middle]
    ["label" :margin-bottom 0 :font-weight :normal :line-height 20 :color "#379933" :max-width 120]]))

(defn- stock-modal [{:keys [images params] :as stock}]
  (modal-window-without-header
   {:width 425}
   (let [image (first images)]
     [:div.bono-modal.form
      [:div.modal-close "×"]
      [:div.group.margin10 {:style "display:inline-block !important;"}
       (text-field {:class "none"} :quantity "1")
       [:div.margin10l.left {:style "line-height:25px;"} "Размер: "]
       [:div.margin10l.left (custom-dropdown {:width 20} :size (get params "size"))]
       [:div.margin10l.left.text-center.clickable
        (small-ok-button "в корзину" :action (action:add-to-bucket stock))]]
      [:img.relative {:role "main-image"
                      :style "margin:0 10px;"
                      :src (images:get :retail-large-image image)}]
      [:div.margin10l.group.margin10b
       (simple-carousel images :bono-small :onclick (partial action:set-main-image :retail-large-image))]])))

(defn- prepare-stock-info[stock]
  (let [{:keys [description stock-variant price]} stock]
    {:description (if (map? description)
                    (let [{:keys [other]} description]
                      (assoc description
                        :other (when other (.replaceAll other "[\r\n]" " "))))
                    {:other (string-converter description)})
     :variants (mreduce (fn[[k v]] {(:size k) (:price v)}) stock-variant)
     :price price}))

(defn- stock-measurements[]
  [:div.left.margin10b
   [:strong "Замеры для размера {{selected}}"]
   [:div.measurement-info {:ng-repeat "measurement in stock.description.measurements[selected]"}
    [:span.measure-name {:style "width:135px;"}  "{{measurement.name}}:"]
    [:span.measure-value "{{measurement.value}} см."]]])

(defn- stock-list-description[]
  (add-singleton
   [:script#popoverDescription {:type "text/ng-template"}
    [:div
     [:div.description.relative
      [:div.group {:style "height:100px;"}
       (stock-measurements)
       [:div.model-icon.left.absolute {:style "top:30px;"}]]
       [:div.properties-info {:ng-repeat "propertie in stock.description.properties"}
       [:span.propertie-name {:style "width:135px;"} "{{propertie.name}} :"]
       [:span.propertie-value  "{{propertie.value}}"]]
      [:div.header "Цена"]
      [:div "{{stock.variants[selected]}} грн."]
      [:div.header "Описание"]
      [:div.margin5t "{{stock.description.other}}"]]]]))

(defn- stock-list-description-server[selected {:keys [description variants]}]
  [:div
   [:div.description.relative
    [:div.group {:style "height:100px;"}
     [:div.left.margin10b
      [:strong "Замеры для размера " selected]
      (map (fn[{:keys [name value]}]
             [:div.measurement-info
              [:span.measure-name {:style "width:135px;"} name]
              [:span.measure-value value " см."]])
           (get-in description [:measurements (keyword selected)]))]
     [:div.model-icon.left.absolute {:style "top:30px;"}]]
    (when-not (empty? (first (:properties description))) (map (fn [{:keys [name value]}] [:p [:strong name ": "] value]) (:properties description)))
    [:div.header "Цена"]
    [:div (get variants selected)" грн."]
    [:div.header "Описание"]
    [:div.margin5t (:other description)]]])

(defmethod themes:render [:basic :stock-list] [_]
  (stock-list-description)
  (common-layout
    [:div.main-list-body.margin10b
     (top-menu)
     (design:breadcrumbs)
     [:div.group.padding10b.relative
      (when (= :main *stock-mode*)
        [:div.left.left-categories (link:categories-tree)])
      [:div.left {:style (if (= :main *stock-mode*) "width:840px;" "width:100%;")}
       [:h1.green.cuprum {:style "margin-left:11px;width:800px;font-size:24px;"}
        (or (:title *current-page*) (:name *current-category*))]
       [:div.left {:style (if (= :main *stock-mode*) "max-width:625px;" "max-width:833px;")}
        (design:stock-list)]
       (when (not= :search *stock-mode*)
         [:div.right.relative {:style "margin-right:12px;"}
          [:div.absolute {:style (css-inline :right 2 :top -4)}
           [:div#filter-btn.btn.btn-default
            {:style (css-inline :margin "5px 10px" :padding "4px 0 2px" :width 180 :font-size 16)
             :onclick (js
                       (var filters ($ ".filters-panel"))
                       (if (.is filters ":visible")
                         (do (++ themedStockListColumnsNum)
                             (.hide filters)
                             (.text ($ "#filter-btn") "Показать фильтры"))
                         (do (-- themedStockListColumnsNum)
                             (.show filters)
                             (.text ($ "#filter-btn") "Спрятать фильтры")))
                       (themedStockListLoadPage 0))}
            "Спрятать фильтры"]]
          (design:stock-filters)])]]]
    (when (= 1 (pager-current-page)) (info-block))))

(defmethod themes:render [:basic :filter-panel] [_ type name values count current-state]
  (let [id (as-str "filters-" type)]
    [:div.filter-panel.margin10b
     [:div.filter-header.block.pointer {:data-toggle "collapse" :data-target (str "#" id)}
      [:div.left name
       [:i.glyphicon.glyphicon-arrow-up.none {:style "font-size:10px;"}]
       [:i.glyphicon.glyphicon-arrow-down.none {:style "font-size:10px;"}]]
      [:div.right.margin5r.clear-filter
       {:title "Сбросить фильтр"
        :onclick (js (var checkboxes (.find (.closest ($ this) ".filter-panel") "input"))
                     (.attr checkboxes "checked" false)
                     (.change (.first checkboxes)))} "×"]]
     [:div.filter-content.collapse.in {:id id}
      (let [item (fn [val]
                   [:div.filter-block
                    (check-box {:autocomplete "off"} type (current-state val) val)
                    [:label val]
                    [:span " (" (count val) ")"]])]
        (map item values))]]))

(defmethod themes:render [:basic :stock-template] [_ {:keys [name images price params description id] :as stock}]
  (require-js :custom-lib :popover)
  (let [sizes (get params "size")
        stock-info (prepare-stock-info stock)]
    [:div.top-stock-block.margin10l.margin10t.relative

     [:form.relative
      [:div.slider-left.absolute]
      [:div.slider-right.absolute]
      [:div.stock-block {:data-popover-trigger true :popover-id id}
       [:a {:href (link:stock-link stock)}
        [:img {:src (images:get :stock-bono (first images))}]]
       [:a.name.block.cutted {:href (link:stock-link stock)} name]
       [:div.price {:data-text (str price " грн. (опт)") }]
       [:div.delimeter]
       [:div.info-panel
        [:div.text [:span {:data-text "Размеры: "}] (map! (fn [size] [:span.margin5r {:data-text size}]) sizes) ]]

       [:a.absolute.clickable-layer.none
        {:href (link:stock-link stock)
         :style (css-inline :top 0 :left 0 :z-index "0" :height "100%" :width "100%"
                            :background-color "rgba(0, 0, 0, 0.7)")}]

       [:div.fast-order-panel.absolute.form.none
        {:data-id id
         :style (css-inline :top 0 :left 0
                            :z-index "99"
                            :margin-top 0 :padding "10px 10px 0 0")}
        [:div.group.padding10r.padding10t
         {:data-toggle "buttons"
          ;:onchange (js
          ;           (setPopoverContent
          ;            (clj (str "[popover-id=" id "]"))
          ;            (renderTemplate "#popoverDescription"
          ;                            {:selected (.. (formState (clj (str "[data-id=" id "]"))) size)
          ;                             :stock (clj stock-info)})))
          }

         (map-indexed (fn [i size]
                        [:label.btn.filter-cell-item.left.margin10l.margin10b {:class (when (zero? i) :active)}
                         (radio-button {:autocomplete "off"} :size (zero? i) size) size])
                      sizes)]
        [:div.group
         [:div.text-center.clickable.left.margin10l
          (ok-button "Купить" :width 90 :action (action:add-to-bucket stock))]
         [:div.right
          (grey-button [:img {:style "margin-top:8px;" :src "/img/themes/bono/lens_size_white.png"}]
                       :widht 34 :action (modal-window-open true))]]]

       ;[:div.popover.absolute.none
       ; [:div.arrow]
       ; [:h3.title.title-popover.group {:style "font-size:20px;"}
       ;  [:p.left name]
       ;  [:div.close.right {:onclick (js (closePopover this))} "x"]]
       ; (lazy-multi-region* lazy-popover (str "lazy-popover" (swap! lazy-modal-window-counter inc)) []
       ;                     (let [{:keys [params] :as stock} (retail:get-stock (s* id))
       ;                           sizes (get params "size")
       ;                           stock-info (prepare-stock-info stock)]
       ;                       [:div.content (stock-list-description-server (first sizes) stock-info)]))]

       (stock-modal stock)]]]))
