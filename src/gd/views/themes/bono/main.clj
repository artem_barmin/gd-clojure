(in-ns 'gd.views.themes.bono.basic)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main page
(defn- main-css[]
  (list
   [".clickable" :cursor :pointer]
   [".grey" :color "#878882"]
   [".green" :color "#659300"]
   [".red" :color "#a82d10"]
   ["h1, h2, h3, h4, h5, h6" :margin-top 0]
   ["h1" :font-size 26]
   ["h2" :font-size 24]
   ["h3" :font-size 22]
   ["h4" :font-size 20]
   ["h5" :font-size 18]
   ["h6" :font-size 16]
   ["p" :margin-bottom 0]
   ["a:hover, a:focus" :text-decoration :none :color :inherit]
   [".close" :line-height 14 :font-size 18]
   ["input" :border "1px solid #909090"
    :outline "none !important"
    :border-radius 3
    :font-size 18
    :padding 5
    :width "100%"]
   ["textarea" :border "1px solid #909090"
    :border-radius 3
    :font-size 18
    :padding 5
    :width "100%"]
   [".input:focus" :border-color "#659300"]
   [".form-control:focus" :border-color "#5AB01A !important" :box-shadow "0 0 5px #9CBE3C !important"]
   [".input-textarea" :height "90%" :font-family "'PT Sans',sans-serif" :font-size "14px"]
   [".font18px" :font-size 18]
   [".dashed-bottom" :border-bottom "1px dashed"]
   [".validation-error-tooltip" :background "none repeat scroll 0 0 rgba(0, 0, 0, 0.75) !important" :border-radius 3 :color :white :padding 10
    [".validation-error-tooltip-arrow"
     :border-bottom "10px solid rgba(0, 0, 0, 0.75)"
     :border-left "10px solid transparent"
     :border-right "10px solid transparent"
     :display :block
     :position :absolute
     :top -10]]
   [".main-block" :height 435
    [".center-menu-block" :width 600 :height 435 :position :relative
     ["li" :display :block :float :left :margin-top -8 :height 435 :text-align :center :width 624]]
    [".login-icon" (bgimage "/img/themes/bono/lock.png") :top 56 :right 118]
    [".register" :color "#C52925" :text-decoration :underline :top 85 :right 60 :cursor :pointer]
    [".login" :color "#C52925" :text-decoration :underline :top 55 :right 68 :cursor :pointer :font-size 22 :font-weight :bold :text-decoration :none]
    [".circle" :text-align :right
     [".text" :padding-top 50 :padding-right 32 :color "#C52925" :width 125 :font-weight :bold :font-size 17]
     [".text:first-line" :font-size 22]
     (map (fn[type]
            [(str "." (name type)) (bgimage (str "/img/themes/bono/circle_" (name type) ".png"))])
          [:contacts :enter :delivery :answers :catalog])
     [".contacts" :text-align :center]
     [".answers .text" :padding-right 31 :padding-top 41]
     [".contacts .text" :padding-right 25 :padding-top 57]
     [".delivery .text" :padding-top 95 :padding-right 50]
     [".catalog .text" :padding-top 98 :padding-right 46]]
    [".info-block" :width 225 :height 415 :border-radius 3 :background-color "rgba(255,255,255,0.6)"
     [".header"
      (border-radius :left-top 3 :right-top 3)
      (tile-image "/img/themes/bono/news_header.png")
      :color "white"
      :font-weight :bold
      :text-align :center
      :font-size 20
      :background-color :green
      :height 43
      :padding 10]]
    [".box-left" (bgimage "/img/themes/bono/3d+div_left.png")]
    [".box-right" (bgimage "/img/themes/bono/3d+div_right.png")]
    [".box-center" (bgimage "/img/themes/bono/3d+div_middle.png") :width 500]]

   [".template-container div:nth-child(1)" :border-top "1px dashed transparent" :padding-top 0]
   [".template-container" :position :relative :height 372
    [".all-items" :position :absolute :bottom 5 :right 10]]
   [".item-container" :border-top "1px dashed #749800" :margin "0 0 10px" :padding-top 10 :width 205 :overflow :hidden
    [".item-time" :font-size :14 :line-height 14]
    [".item-title" :line-height 14 :margin "7px 0" :max-height 26 :overflow :hidden :display :block]
    [".item-text" :font-size 14 :height 43 :line-height 15 :overflow :hidden]]))

(defn- short-template [name url funck]
  [:div.template-container.padding10
   funck
   [:a.all-items.red {:href url} (str "все " name)]])

(defn- short-item[time url title text]
  [:div.item-container
   [:div.item-time.grey (format-date time)]
   [:a.item-title.green {:href  url} title]
   [:p.item-text text]])

(defn- short-news []
  (short-template
   "новости"
   "/news/"
   (map (fn [{:keys [title url time content-short] :as news}]
          (short-item time (str "/news/detail/" url "/") title content-short))
        (site-pages:get-pages-list 3 0 {:type :news :status :published}))))

(defn- short-review []
  (short-template
   "отзывы"
   "/guestbook/"
   (map (fn [{:keys [text sent-time user id] :as message}]
          (let [[title text] (prepare-message message)]
            (short-item sent-time (str "/guestbook/" "#review" id) title text)))
        (retail:get-review 3 0 ))))

(defmethod themes:render [:basic :main] [_]
  (common-layout
    [:div.fixed-group.center.main-block
     [:div.left.info-block
      [:div.header "Новости"]
      (short-news)]
     (let [circle (fn[type text href & info]
                    [:div.absolute {:style (css-inline info) :class type}
                     [:a.text.align-right.block {:href href} text]])]
       [:div.left.relative.center-menu-block.circle
        (javascript-tag (js (var slideNum 0)
                            (setInterval (fn[]
                                           (set! slideNum (+ 1 (% slideNum 15)))
                                           (.attr ($ "#mainSlider") "src"
                                                  (str "/img/bono/centerSlider/" slideNum ".png")))
                                         3500)))
        [:ul [:li [:img#mainSlider {:src "/img/bono/centerSlider/1.png"}]]]
        (circle :contacts "Наши контакты" "/contacts/" :left 71 :top -40)
        (circle :enter "" "" :right 30 :top -13)
        (circle :delivery "Доставка и оплата" "/pages/dostavka_i_oplata/" :left 0 :top 77)
        (circle :answers "Вопросы и ответы" "" :left 65 :top 281)
        (circle :catalog "Каталог товаров" "/stocks/" :right 0 :top 137)
        [:div.login-icon.absolute]
        [:a.login.absolute {:onclick (modal-window-open true)} "Вход"
         (design:registration-modal)]
        [:a.register.absolute {:onclick (modal-window-open true)} "Регистрация"
         (design:registration-modal)]])
     [:div.left.info-block
      [:div.header "Отзывы"]
      (short-review)]]
    [:div.fixed-group.center
     [:div.left.box-left]
     [:div.left.box-center]
     [:div.left.box-right]]
    (info-block)))
