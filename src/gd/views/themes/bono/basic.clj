(ns gd.views.themes.bono.basic
  (:use gd.views.resources
        clojure.algo.generic.functor
        gd.views.themes.main
        gd.views.bucket.simple
        compojure.core
        gd.views.seo.meta
        gd.views.messages
        gd.utils.sms
        gd.utils.mail
        gd.utils.web
        gd.utils.common
        gd.views.components
        noir.core
        [noir.options :only (dev-mode?)]
        [noir.response :only (redirect status set-headers)]
        [clj-time.core :exclude [extend]]
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:use gd.utils.common
        gd.utils.styles
        gd.views.styles.main
        gd.views.styles.utils
        gd.views.styles.buttons
        gd.views.styles.common)
  (:use gd.model.model)
  (:require [clj-time.coerce :as tm])
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Info block
(declare carousel)

(defn- info-block-css[]
  (list
   [".info-block-title" :background-color "#F3F3F3" :padding "10px 10px 3px" :margin-bottom -5]
   [".left-header-img" (bgimage "/img/themes/bono/3d+div_left.png")]
   [".center-header-img" :width 582 :height 19 :background-image "url('/img/themes/bono/3d+div_middle.png')"]
   [".right-header-img" (bgimage "/img/themes/bono/3d+div_right.png")]
   [".info-block-delimeter" :background-image "url('/img/themes/bono/separator.png')" :height 9 :width "100%"]
   [".page-content" :width 1050 :padding "20px 35px" :background-color "#dcebb1"]))

(defn- info-block[& [content]]
  [:div.center {:style "width:1050px;"}
   [:div.group
    [:div.left-header-img.left]
    [:div.center-header-img.left]
    [:div.right-header-img.left]]
   [:div.info-block-title.title-italic.text-center
    "Обновление наличия - ежедневно!!! Поставки нового товара осуществляются дважды в неделю!!!"]
   [:div.relative (carousel)]
   [:div.info-block-delimeter]
   [:div.page-content (or content (:content *current-page*))]])

(load "header")
(load "footer")
(load "common")
(load "pages")
(load "bucket")
(load "carousel")
(load "categories")
(load "modal")
(load "stocks")
(load "stock")
(load "user")
(load "main")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Common css
(defn- pager-css[]
  (list
   [".pager-pages-container" :background-color "#D9D9D9" :margin "0 10px" :padding "5px"]
   [".pager-dropdown" :display "none"]
   [".pager-main" :display "block" :text-align "center"]
   [".pager-non-active" :color "#909090" :margin "0 5px" :border "1px solid #D9D9D9" :padding "4px 7px"]
   [".pager-table" :margin "0 10px 10px 0"]
   [".pager-page" :margin-left 5 :margin-right 5 :border-radius 3 :padding "4px 7px" :border "1px solid #D9D9D9"]
   [".pager-page:hover" :background-color "#bbd3a8" :padding 4 :padding-left 7 :padding-right 7 :border "solid 1px #afca9a"]
   [".pager-current-link" :padding 4 :padding-left 7 :padding-right 7 :border-radius 3 :background-color "#bdbdbd" :border "solid 1px #9c9c9c" :margin "0 5px" :padding "4px 7px"]
   [".pager-prev-arrow,.pager-next-arrow" :display :none]))

(defn- simple-carousel-css[]
  (list
   [".bono-modal"
    [".simple-carousel"
     [".container" :height "95px !important"]]]
   [".info-carousel"
    [".simple-carousel"
     [".container" :height "365px !important"]]]
   [".simple-carousel" :margin-top 5
    ["ul" :list-style-type :none :margin 0 :padding 0]
    ["li" :list-style :none :float :left :margin-right 5 :margin-bottom 5]

    [".navigate-button"
     :width 15
     :height 90
     :cursor :pointer
     :background-position "center center"]

    [".prev" (bg-simple "/img/themes/bono/arrow_mini-photo_enable_left.png")
     :margin-right 5]
    [".next" (bg-simple "/img/themes/bono/arrow_mini-photo_enable_right.png")]

    [".prev[disabled]" (bg-simple "/img/themes/bono/arrow_mini-photo_disable_left.png")
     :margin-right 5]
    [".next[disabled]" (bg-simple "/img/themes/bono/arrow_mini-photo_disable_right.png")]

    [".prev[enabled]:hover" :background-color "#D9D9D9"]
    [".next[enabled]:hover" :background-color "#D9D9D9"]

    [".images-container" :left 0 (with-prefixes "transition" "all 0.1s ease-in-out") ]]))

(defn- filter-css[]
  (list
   [".filters-panel" :margin-top 43]
   [".filter-panel" :background-color "white" :border "1px solid #D9D9D9" :width 200 :border-radius 3]
   [".filter-header" :background-image "url('/img/themes/bono/horisontal_menu_green_select.png')" :color :white :height 30 :line-height 30 :border-radius "3px 3px 0 0" :padding-left 10]
   [".filter-content" :padding "9px 9px 0 9px" :max-height 400 :overflow :auto
    [".checked" :background-color "#CCCCCC" :border-color "#949494"]]
   [".filter-cell-item"
    :min-width 37
    :color :white
    :padding "10px 5px 8px"
    :line-height 10
    :border-radius "5px 5px 3px 3px"
    :text-align :center
    :margin-bottom 10
    :cursor :pointer
    :text-shadow "1px 1px 1px rgba(0, 0, 0, 0.5)"
    :text-transform :uppercase
    :background-image "url('/img/themes/bono/buttons/green_grad.png')"]
   [".filter-cell-item.active" :background-image "url('/img/themes/bono/buttons/orange_clicked_grad.png')"]
   [".filter-cell-item.active:hover" :background-image "url('/img/themes/bono/buttons/orange_hover_grad.png')"]

   [".filter-cell-item:hover"
    :background-image "url('/img/themes/bono/buttons/green_hover_grad.png')"
    :color :white
    :box-shadow "0 3px 5px rgba(0, 0, 0, 0.125) inset"
    :text-decoration :none]
   [".filter-cell-item:active" :background-image "url('/img/themes/bono/buttons/green_clicked_grad.png')"]
   [".filter-row-item" :margin-bottom 10]
   [".clear-filter" :font-size 22 :margin-right 10]
   [".clear-filter:hover" :cursor :pointer :color :black]))

(defn- dropdown-css[]
  (list
   [".dk_toggle"
    :width "100%"
    :border-radius 3
    :transition "border-color 0.5s ease 0s"
    :padding "5px 12px 3px 5px !important"
    :background-color "#EEEEEE"
    :color "#444444"
    :background-repeat "no-repeat"
    (with-inner-prefixes "background-image" "linear-gradient" "bottom,  #F2F2F2, #E8E8E8")]

  [".dk_container" :height 25]
   [".dk_options"
    :border-radius "3px !important"
    :background-clip "padding-box"
    :background-color "#FFFFFF"
    :border "1px solid #C0C0C0 !important"
    :border-radius "6px"
    :box-shadow "0 1px 4px rgba(102, 102, 102, 0.3) !important"
    :display "none"
    :left "0"
    :list-style "none outside none"
    :margin "2px 0 0 !important"
    :min-width "60px"
    :padding "5px 0"
    :position "absolute !important"
    :top "100%"
    :z-index "1000"]

   [".dk_open .dk_options" :display "block" :margin-bottom "5px" :position "static"]
   [".dk_open, .dk_focus .dk_toggle" :box-shadow "none!important"]
   [".dk_open .dk_toggle" :border-radius 3]
   [".dk_options_inner" :border-radius 0]
   [".dk_options li" :line-height "20px"]

   [".dk_options a"
    :clear :both
    :color "#444444"
    :display :block
    :font-weight :normal
    :line-height "20px"
    :padding "3px 20px"
    :white-space :nowrap
    :background-color "transparent !important"
    :opacity "1 !important"
    :padding "3px 20px 3px 6px !important"
    :border "none !important"]

   [".dk_options a:hover" :background-color "#659300 !important" :border "0 none !important" :border-radius :none :color "#FFFFFF"]
   [".dk_label" :background-image "url('/img/dropdown/dk_simple_black_arrow.png')" :background-repeat "no-repeat" :background-position "95%"]

   [".bucket-item .dk_toggle" :min-width 55]))

(defn- buttons-css[]
  (binding [*buttons-base* "/img/themes/bono/buttons/"]
    (list
     (button "red")
     (button "green")
     (button "orange")
     (button "grey")
     (simple-buttons ".small-orange-button-text" "/img/themes/bono/buttons/orange-small")
     [".small-orange-button-text" :color :white :line-height 26 :font-size 12 :text-shadow "1px 1px 1px rgba(0, 0, 0, 0.5)"]
     [".orange-button-text" :font-size 19]
     [".red-button-text" :font-size 19]
     [".green-button-text" :font-size 19])))

(css "resources/public/css/themes/bono/main1.css"
     (user-profile-dropdown-css)
     (header-css)
     (footer-css)
     (common-layout-css)
     (categories-css)
     (top-menu-css)
     (carousel-css)
     (info-block-css)
     (page-layout-css)
     (bono-modal-css)
     (inline-bucket-css)
     (bucket-css)
     (stocks-css)
     (single-stock-css)
     (main-css)
     (pager-css)
     (simple-carousel-css)
     (filter-css)
     (dropdown-css)
     (buttons-css))
