(in-ns 'gd.views.themes.bono.basic)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Single stock
(defn- single-stock-css[]
  (list
   [".single-stock"
    [".single-stock-name" :font-size 24 :color "#659300"]
    [".images-carousel" :margin-right 5]
    [".info-block" :padding-left 5 :color "#4B4A4A" :width 395]
    [".info" :background-color :white :border-radius 5 :border "1px solid #d9d9d9" :width 820 :margin-left 10 :padding 5 :margin-bottom 10]
    [".name" :color "#659300" :font-size 30 :margin "5px 0"]
    [".section" :padding-top 10 :padding-bottom 10 (border :color "#e8e8e8" :enable [:top])
     [".section-name" :font-weight :bold :margin "5px 10px 5px 0"]]
    [".block"
     [".header,.description,.section" :padding-left 3]
     [".description" :font :inherit :white-space :pre-wrap :word-wrap :break-word :margin-top 5 :margin-bottom 5]]
    [".sizes"
     [".size" :border-radius 5 :background-color "#b7d2a2" :border "1px solid #A5C28E" :color "#114C1E" :font-size 18 :margin-right 5 :min-width 35 :padding 3 :text-align :center :cursor :pointer :line-height 26]
     [".checked .size" :background-color :orange :border "1px solid #DC5920" :color "#932308" :cursor "default"]]
    [".price" :color "#dc5920" :font-size 30]
    [".price-section" :margin-top 10 :padding-top 10 :padding-bottom 10 (border :color "#fcb89a" :enable [:top :bottom])]]))

(defn- render-description-info[description]
  [:div.extra-info.group
   (stock-measurements)
   [:div.model-icon.right {:style "margin:5px 25px 5px 10px;"}]])

(defmethod themes:render [:basic :single-stock] [_ {:keys [id images price name all-params description] :as stock}]
  (let [{:keys [other]} description]
    (common-layout
      [:div.main-list-body.single-stock.margin10b.ng-cloak
       (top-menu)
       (design:breadcrumbs)
       [:div.group.form {:ng-init (js (set! stock (clj (prepare-stock-info stock))))}
        [:div.left.left-categories (link:categories-tree)]
        [:div.left.info
         [:div.left.images-carousel
          [:img {:src (images:get :retail-large-image (first images))
                 :role :main-image
                 :alt name}]
          (simple-carousel images
                           :bono-small :onclick (partial action:set-main-image :retail-large-image))]
         [:div.left.info-block
          [:h1.single-stock-name name]
          [:div.block
           [:div.header "Характеристики"]
           [:div.section [:span.section-name "Производитель:"] (get all-params "brand")]
           [:div.section.group
            (render-description-info description)]
           [:div.section [:span.section-name "Дополнительно:"]
           (when-not (empty? (first (:properties description))) (map (fn [{:keys [name value]}] [:p [:strong name ": "] value]) (:properties description)))]]
          [:div.block
           [:div.header "Описание"]
           [:div.description {:itemprop "description"} (when other (.replaceAll other "(?s)(\r\n)+" "<br/>"))]]
          [:div.section [:div.section-name "Размер:"]
           [:div.fixed-group.sizes {:data-toggle "buttons" :ng-model "selected" :bs-radio-group true}
            (map-indexed (fn[i size]
                           [:label.btn.filter-cell-item.margin10l.margin10ba
                            (radio-button {:autocomplete "off"} :size (= i 0) size) size])
                         (get all-params "size"))]]
          [:div.section [:span.section-name "Количество:"]
           (spinner :quantity)]
          [:div.price-section
           [:div.fixed-group.width100
            [:div.left.price "{{stock.variants[selected] || stock.price}} грн."]
            [:div.right
             (orange-button "Добавить в корзину"
                            :width 180
                            :action
                            (action:add-to-bucket stock))]]]]]]]
      nil)))
