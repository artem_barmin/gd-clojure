(in-ns 'gd.views.themes.bono.basic)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Common-layout
(defn- common-layout-css []
  (list
   ["*" :margin 0 :padding 0]
   [:body :font-family "Calibri, Sans !important" :font-size 16 :background-repeat :repeat-x :background-color "#b0dc33" (tile-image "/img/themes/bono/bg_grad.jpg")]
   [".body" :width 1050]
   [".ui-widget-overlay" :background-color :black :opacity "0.5" :position :absolute :top 0 :left 0]
   [".logo" (bgimage "/img/themes/bono/logo.png") :top 1 :left 60]
   [".border-bottom" :border-bottom "1px solid #ededed"]
   [".top-section" :background-repeat :no-repeat (tile-image "/img/themes/bono/bg.jpg")]
   [".input, .dk_toggle" :border "1px solid #909090" :border-radius 3 :padding "1px 4px"]
   [".input:active, .input:hover, .input:focus, .dk_toggle:hover, .dk_focus .dk_toggle" :border-color "#5ab01a" :box-shadow "0 0 5px #9cbe3c"]
   ["a" :text-decoration :none :color :black]
   ["a:focus" :outline :none]
   [".alert" :color "#dd2a00" :font-family "'Cuprum',sans-serif" :font-size 19]
   [".title-popover" :font-size "20px !important"]
   [".title" :color "#659300" :font-family "'Cuprum',sans-serif" :font-size 26
    [".title-side" :height 80 :line-height 80 :margin "0 91px"]
    [".title-circle" :background-color "#E8E8E8" :border-radius 37 :height 65 :line-height 37 :margin "0 4px 0 38px" :padding 14]]
   [".title-italic" :color "#dd2a00" :font-size 20 :font-style :italic :font-weight :bold]))




(import 'java.security.MessageDigest
        'java.math.BigInteger)

(defn md5 [s]
  (let [algorithm (MessageDigest/getInstance "MD5")
        size (* 2 (.getDigestLength algorithm))
        raw (.digest algorithm (.getBytes s))
        sig (.toString (BigInteger. 1 raw) 16)
        padding (apply str (repeat (- size (count sig)) "0"))]
    (str padding sig)))

(defn- site-heart-auth []
  (let [{:keys [id name] :as user} (user:get-full (sec/id))
        json  (-> {:nick (get-in user [:contact :email :value])
                   :id id
                   :email (get-in user [:contact :email :value])}
                  cheshire.core/generate-string)
        time  (quot (System/currentTimeMillis) 1000)
        user-base64  (String. (ring.util.codec/base64-encode (.getBytes json)))
        sign (md5 (str "cJXskSUStj" user-base64 time))]
(str user-base64 "_" time "_" sign)))

(defn- common-layout[& body]
  (require-css :themes-external "/css/themes/bono/main1.css")
  [:div.top-section
   (header)
   [:div.body.center body]
   (footer)
   [:script {:async true :type "text/javascript" :src "http://code.netroxsc.ru/A17E0F26-101F-B415-13AD-AFA2D7B75D3C/c.js?tmpl=6"}]
   [:script {:type "text/javascript"}
    (str "(function(){
    var widget_id = 748485;
    _shcp =[{widget_id : widget_id"
    (when (sec/logged)
      (str " ,auth : '"(site-heart-auth)"'"))
    "}];
    var lang =(navigator.language || navigator.systemLanguage || navigator.userLanguage ||\"en\").substr(0,2).toLowerCase();
    var url =\"widget.siteheart.com/widget/sh/\"+ widget_id +\"/\"+ lang +\"/widget.js\";
    var hcc = document.createElement(\"script\");
    hcc.type =\"text/javascript\";
    hcc.async = true;
    hcc.src =(\"https:\"== document.location.protocol ?\"https\":\"http\")+\"://\"+ url;
    var s = document.getElementsByTagName(\"script\")[0];
    s.parentNode.insertBefore(hcc, s.nextSibling);})();")]]);748485
