(in-ns 'gd.views.themes.bono.basic)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Page-layout
(defn- page-layout-css[]
  (list
   [".main-list-body" :background-color "#E8E8E8" :border-radius 3 :display :inline-block :margin-top -30 :position :relative :display :inline-block]
   [".header-title" :color "#659300" :font-family "'Cuprum',sans-serif" :font-size 28]
   [".panel" :margin 10 :background-color "#FFFFFF" :border-radius 4 :box-shadow "0 1px 2px 0 #595858"
    [".modal-close" :display :none]
    [".modal-button" :display :none]
    [".panel-button" :display :block]]
   [".panel-button" :display :none]
   [".pages-list"
    [".pager-table" :margin 0]
    [".pager-pages-container" :margin 0]
    [".pager-row"
     ["td" :padding "5px 10px"]]]))

(defn- page-layout[body]
  (common-layout
    [:div.main-list-body.margin10b
     (top-menu)
     (design:breadcrumbs)
     [:h1.cuprum.green.margin15l
      (:name (last *current-breadcrumbs*))]
     [:div.panel
      body]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Site-pages

(defmethod themes:render [:basic :pages] [_ content]
  (page-layout
   [:div.padding20
    content]))

(defn- review-template [{:keys [text sent-time user id] :as message}]
  (let [[user text] (prepare-message message)]
    [:div {:id (str "review" id)}
     [:div.green (format-just-date sent-time)]
     [:div.red user]
     [:div text]]))

(defn- news-template[{:keys [title url time content-short] :as news}]
  [:div
   [:div.group
    [:a.red.left {:href (str "/news/detail/" url "/")} title]
    [:div.grey.right (format-date time)]]
   [:div content-short]
   [:a.dashed-bottom.green {:href (str "/news/detail/" url "/")} "Читать дальше"]])

(defmethod themes:render [:basic :news-detail] [_ content]
  (page-layout
   [:div.pages-list.padding10 content]))

(defmethod themes:render [:basic :news] [_]
  (page-layout
   [:div.pages-list.padding10
     (pager bono-news
            (site-pages:get-pages-list 8 (c* offset) {:type :news})
            news-template
            :columns 1
            :page 8)]))

(defmethod themes:render [:basic :guestbook] [_]
  (page-layout
   [:div.pages-list.padding10
    (if (sec/logged)
      (list
       [:h2.cuprum "Оставить отзыв:"]
       [:div.form
        [:div
         [:h4.cuprum "Текст сообщения"]
         (text-area {:class "input input-big"} "message")]
        [:div.cuprum.font18px.margin10
         (green-button
          "Оставить отзыв"
          :width 200
          :action (execute-form* bono-guestbook-message [message]
                                 (retail:send-review message)
                                 :validate [(textarea-required "message" "Введите текст сообщения")]
                                 :success (js* (notify "Спасибо" "Ваш отзыв отправлен администрации"))))]])
      [:div.cuprum.font18px.red
       {:style "margin-bottom:10px;"}
       "Для того чтобы оставить отзыв, вам необходимо войти под своей учетной записью"])

     (pager bono-review
            (retail:get-review 8 (c* offset))
            review-template
            :columns 1
            :page 8)]))

(defmethod themes:render [:basic :contacts] [_]
  (page-layout
   [:div.padding10
    (let [{:keys [title content]}
          (site-pages:get-page-for-url "contacts")]
      [:div.cuprum.font18px
       [:h1 title]
       [:span content]
       [:br]
       [:p.red.curpum "Отправить сообщение:"]
       [:div.form
        [:label "Ваше имя"] [:span.bono_r.bono_mand "*"]
        [:input.margin10b {:type "text" :name "name" :class "input big-input"}]
        [:label "Ваш e-mail"] [:span.bono_r.bono_mand "*"]
        [:input.margin10b {:type "text" :name "email" :class "input big-input"}]
        [:label "Телефон"] [:span.bono_r.bono_mand " "]
        [:input.margin10b {:type "text" :name "phone" :class "input big-input"}]
        [:label "Город (страна)"] [:span.bono_r.bono_mand " "]
        [:input.margin10b {:type "text" :name "address" :class "input big-input"}]
        [:label "Сообщение"] [:span.bono_r.bono_mand "*"]
        [:textarea.margin10b {:type "text" :name "text" :class "input big-input"}]
        [:div.cuprum.font18px.margin5t
         (green-button
          "Отправить"
          :width 180
          :action (execute-form* bono-contact-send [name email phone addres text]
                                 (send-mail "koval111vladislav@gmail.com"
                                            (str "Сообщение от " name "(" addres ")")
                                            (str phone "\n" email "\n" text))
                                 :validate [(textarea-required "name" "Не заполнено имя")
                                            (textarea-required "email" "Не заполнен e-mail")
                                            (textarea-required "text" "Не заполнено сообщение")
                                            (textarea-email "email" "Не правильный формат email")]
                                 :success (js* (notify "Спасибо" "Ваше сообщение успешно отправлено"))))]]])]))
