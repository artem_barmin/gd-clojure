(ns gd.views.themes.external)

(defn google-analytics[]
  " <script type='text/javascript'>
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-16622757-50']);
        _gaq.push(['_trackPageview']);

        _gaq.push(['t2._setAccount', 'UA-46010790-1']);
        _gaq.push(['t2._trackPageview']);

        (function() {
                      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                    })();
    </script>")

(defn yandex-metrika[]
  "<script type=\"text/javascript\">
(function (d, w, c) {
                     (w[c] = w[c] || []).push(function() {
                                                          try {
                                                               w.yaCounter22110703 = new Ya.Metrika({id:22110703,
                                                                                                     webvisor:true,
                                                                                                     clickmap:true,
                                                                                                     trackLinks:true,
                                                                                                     accurateTrackBounce:true});
                                                               w.yaCounter23539387 = new Ya.Metrika({id:23539387,
                                                                                                     clickmap:true,
                                                                                                     trackLinks:true,
                                                                                                     accurateTrackBounce:true});
                                                               } catch(e) { }
                                                          });

                     var n = d.getElementsByTagName(\"script\")[0],
                     s = d.createElement(\"script\"),
                     f = function () { n.parentNode.insertBefore(s, n); };
                                      s.type = \"text/javascript\";
                                      s.async = true;
                                      s.src = (d.location.protocol == \"https:\" ? \"https:\" : \"http:\") + \"//mc.yandex.ru/metrika/watch.js\";

                                      if (w.opera == \"[object Opera]\") {
                                                                          d.addEventListener(\"DOMContentLoaded\", f, false);
                                                                          } else { f(); }
                                                                                  })(document, window, \"yandex_metrika_callbacks\");
                                                                                    </script>")
