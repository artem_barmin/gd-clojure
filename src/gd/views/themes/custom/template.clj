(ns gd.views.themes.custom.template
  (:use clojure.test
        clojure.algo.generic.functor
        gd.utils.common
        com.reasonr.scriptjure
        gd.utils.test
        hiccup.core
        clojure.walk)
  (:import HTMLSchema)
  (:require [net.cgrand.xml :as xml])
  (:require [gd.views.themes.custom.infix :as infix]
            [net.cgrand.enlive-html :as en]))

(defn- startparse-tagsoup [s ch]
  (doto (org.ccil.cowan.tagsoup.Parser.)
    (.setFeature "http://www.ccil.org/~cowan/tagsoup/features/default-attributes" false)
    (.setFeature "http://www.ccil.org/~cowan/tagsoup/features/default-attributes" false)
    (.setFeature "http://www.ccil.org/~cowan/tagsoup/features/cdata-elements" true)
    (.setFeature "http://www.ccil.org/~cowan/tagsoup/features/ignorable-whitespace" true)
    (.setFeature "http://www.ccil.org/~cowan/tagsoup/features/ignorable-whitespace" true)
    (.setContentHandler ch)
    (.setProperty "http://www.ccil.org/~cowan/tagsoup/properties/auto-detector"
                  (proxy [org.ccil.cowan.tagsoup.AutoDetector] []
                    (autoDetectingReader [#^java.io.InputStream is]
                      (java.io.InputStreamReader. is "UTF-8"))))
    (.setProperty "http://www.ccil.org/~cowan/tagsoup/properties/schema" (HTMLSchema.))
    (.parse s)))

(defn- load-html-resource
  "Loads and parse an HTML resource and closes the stream."
  [stream]
  (filter map?
          (with-open [#^java.io.Closeable stream stream]
            (xml/parse (org.xml.sax.InputSource. stream) startparse-tagsoup))))

(defn enlive-node->hiccup[node]
  (if (map? node)
    ;; for comment node {:type :comment, :data "[if IE]> ..."}
    (if (= :comment (:type node))
      (str "<!--" (:data node) "-->")
      (let [{:keys [tag attrs content]} node
            hiccup-form (if (empty? attrs) [tag] [tag attrs])
            cnts (filter #(not (and (string? %)
                                    (re-matches #"\n\s*" %))) content)]
        (reduce conj hiccup-form (map enlive-node->hiccup cnts))))
    node))

(defn html->hiccup [s]
  (let [nodes (-> s
                  java.io.StringReader.
                  load-html-resource)]
    (->> (map enlive-node->hiccup nodes)
         (filter #(not (and (string? %)
                            (re-matches #"\n\s*" %)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils

(defn- record?[form]
  (instance? clojure.lang.IRecord form))

;; fix to work with records
(alter-var-root #'clojure.walk/walk
                (constantly
                 (fn[inner outer form]
                   (cond
                     (list? form) (outer (apply list (map inner form)))
                     (instance? clojure.lang.IMapEntry form) (outer (vec (map inner form)))
                     (seq? form) (outer (doall (map inner form)))
                     (record? form) (outer form)
                     (coll? form) (outer (into (empty form) (map inner form)))
                     :else (outer form)))))

(defn fix-vectors[args]
  (postwalk (fn[m]
              (cond (sequential? m) (vec m)
                    (keyword? m) (name m)
                    true m)) args))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Parse template
(defrecord SpecialTag [type name params-or-body])

(defn- parse-name[s]
  (let [s (as-str s)]
    (if (= s ".")
      ["this"]
      (->>
       (.split (as-str s) "\\.")
       (map (fn[s] ((cond (re-find #"^[0-9]+$" s) parse-int
                          true as-str) s)))
       vec))))

(defn- parse[s]
  (let [fix-quotes (fn[s] (.replaceAll s "'" "\""))
        parse-param (fn[param] (cond (symbol? param) (SpecialTag. :variable (parse-name param) [])
                                     true (SpecialTag. :value param [])))
        parse-params (fn[params] (binding [*read-eval* false]
                                   (->> params
                                        fix-quotes
                                        read-string
                                        (map parse-param))))]
    (cond-let [[_ fun params]]

      (re-find #"(.*?)(\(.*\))" s)
      (SpecialTag. :function (parse-name fun) (parse-params params))

      []
      (SpecialTag. :variable (parse-name s) []))))

(defn- parse-tags[template]
  (let [special #"(.*)(<special:block.*?/special:block>)(.*)"
        simple #"(.*)\{\{(.*?)\}\}(.*)"
        fn-tag #"^fn:(.+)"]
    (prewalk (fn[s]
               (cond
                 ;; complex tags inside string(usually attributes)
                 (and (string? s) (re-find special s))
                 (let [[_ before tag after :as special] (re-find special s)]
                   (if special
                     (let [[tag {:keys [value]} body]
                           (first (html->hiccup tag))

                           block
                           (SpecialTag. :block (parse value) (parse-tags body))]
                       (apply list (remove empty? (list before block after))))
                     s))

                 ;; simple tags(anywhere)
                 (and (string? s) (re-find simple s))
                 (let [[_ before tag after :as special] (re-find simple s)]
                   (if special
                     (apply list (remove empty? (list before (parse tag) after)))
                     s))

                 (and (vector? s) (= :keep (first s)))
                 (SpecialTag. :keep nil (rest s))

                 ;; html type function calling(all attributes treated as named params), body treated as nested
                 (and (vector? s) (= :fn (first s)))
                 (let [[_ attrs & body] s]
                   (SpecialTag. :block
                                (SpecialTag. :function
                                             (parse-name (:fn attrs))
                                             (->> (dissoc attrs :fn)
                                                  (map (fn[[k v]] [(SpecialTag. :value k [])
                                                                   (let [parsed (parse-tags v)]
                                                                     (if (and
                                                                          (coll? parsed)
                                                                          (= 1 (count parsed))
                                                                          (instance? SpecialTag (first parsed)))
                                                                       parsed
                                                                       (SpecialTag. :value parsed [])))]))
                                                  flatten))
                                (parse-tags body)))

                 ;; complex tags with nested content
                 (and (vector? s) (= :special:block (first s)))
                 (let [[tag {:keys [value type]} & body] s]
                   (SpecialTag. :block (parse value) (parse-tags body)))

                 true s))
             template)))

(defn- convert-repeat-to-tags
  "Convert syntax for repeat and nested call into tag-like structure, to be
  parsed by hiccup bridge"
  [template]
  (let [process (fn[template]
                  (-> template
                      (clojure.string/replace #"\{\{([#^])(.*?)\}\}"
                                              (fn[[_ type match]]
                                                (str "<special:block "
                                                     "type=\""
                                                     type
                                                     "\" value=\""
                                                     (java.util.regex.Matcher/quoteReplacement
                                                      (.replaceAll match "\"" "'"))
                                                     "\">")))
                      (clojure.string/replace #"\{\{/(.*?)\}\}" "</special:block>")))]
    (->> (str "<special:start-template>" template "</special:start-template>")
         html->hiccup
         (prewalk (fn[s] (if (string? s) (process s) s)))
         html)))

(defn- normalize-template
  "Flatten all lists"
  [template]
  (let [flatten-list (fn[x] (filter (complement seq?) (rest (tree-seq seq? seq x))))]
    (prewalk (fn[s]
               (cond
                 (instance? clojure.lang.IMapEntry s) s
                 (seq? s) (let [res (flatten-list s)]
                            (if (= 1 (count res)) (first res) res))
                 (vector? s) (vec (mapcat (fn[n] (if (list? n) (flatten-list n) [n])) s))
                 true s))
             template)))

(defn prepare-template[template]
  (-> template
      convert-repeat-to-tags
      html->hiccup
      ((comp rest first))        ;remove toplevel template tag
      parse-tags
      normalize-template))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Render template
(defn- is-partial?[s]
  (and (record? s) (:type s)))

(declare render-partial)
(declare render-template)

(def ^:dynamic *evaluation-context* nil)

(defn- render-template-basic[template data]
  (prewalk (fn[s]
             (cond
               (is-partial? s)
               (render-partial data s)

               true s))
           template))

(defn- show-name[name-lst]
  (clojure.string/join "." (map name name-lst)))

(defn- render-block[data {:keys [type] :as name} body]
  (case type
    :function (let [fun (render-partial data name)]
                (cond
                  (function? fun)
                  (let [binding-vars (:binding (meta fun))]
                    (when binding-vars (push-thread-bindings binding-vars))
                    (try
                      (binding [*evaluation-context* data]
                        (fun (render-template-basic body data)))
                      (finally (when binding-vars (pop-thread-bindings)))))

                  fun
                  (render-template-basic body data)

                  true
                  nil))
    :variable (let [variable (render-partial data name)]
                (cond
                  (and (not (nil? variable)) (coll? variable))
                  (map-indexed
                   (fn[index val]
                     (render-template
                      body
                      (merge data
                             {:index index
                              :first (zero? index)
                              :last (= index (dec (count variable)))}
                             (cond (instance? clojure.lang.IMapEntry val) {:key (first val)
                                                                           :val (second val)}
                                   (map? val) val
                                   true {:this val}))))
                   variable)

                  (not (nil? variable))
                  (render-template-basic body data)

                  true
                  nil))))

(defn- render-partial[data {:keys [type name params-or-body] :as all}]
  (case type
    :keep params-or-body
    :function (try
                (let [resolve (comp keywordize-keys (partial render-partial data))
                      fun (get-in data name)]
                  (if (nil? fun)
                    (throwf "Function %s is not found" (show-name name)))
                  (let [result (binding [*evaluation-context* data]
                                 (apply fun (map resolve params-or-body)))]
                    (if (vector? result) (html result) result)))
                (catch clojure.lang.ArityException e
                  (throwf "Function %s is called with wrong number of arguments" (show-name name))))
    :variable (or (get-in data name) nil)
    :block (render-block data name params-or-body)
    :value name))

(defn resolve-variable[var-name]
  (get-in *evaluation-context* (parse-name var-name)))

(defn- compile-expression[expr-string]
  (let [[res vars] (infix/parse expr-string)
        vars (set vars)]
    (postwalk (fn[s]
                (if (vars s)
                  `(resolve-variable ~(name s))
                  s))
              res)))

(def global-functions
  {:if (fn[var]
         (fn[body]
           (if var body nil)))
   :js (fn[var & keys]
         (cljs (if (seq keys)
                 (select-keys var (map keyword keys))
                 var)))
   :calc (fn[text]
           (eval (compile-expression text)))
   :if-not (fn[var]
             (fn[body]
               (if var nil body)))})

(defn render-template[template data & [html-data]]
  (let [data (merge (fix-vectors global-functions)
                    (fix-vectors data)
                    (mreduce (fn[[k v]] {(as-str k) v}) html-data))]
    (render-template-basic template data)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tests

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Parsing tests

(deftest attribute-rendering
  (is (= "<a href=\"some-test/stocks/\">Hello</a>"
         (html (render-template
                (prepare-template "<a href='some-test{{link.catalog}}'>Hello</a>")
                {:link {:catalog "/stocks/"}})))))

(deftest empty-function-call
  (is (= "Stock list Some text"
         (html (render-template
                (prepare-template "Stock list {{component.stock-list()}}")
                {:component {:stock-list (fn[] "Some text")}})))))

(deftest vector-get
  (is (= "<div>Stock <img src=\"/img/uploaded/w100/h100/a.jpg\"></img></div>"
         (html (render-template
                (prepare-template "<div>Stock <img src=\"{{component.image([100 100],stock.images.0)}}\"/></div>")
                {:component {:image (fn[[w h] code] (str "/img/uploaded/w" w "/h" h "/" code ".jpg"))}
                 :stock {:images ["a" "b" "c"]}})))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Rendering tests
(defn- do-template[template data & [html-data]]
  (normalize-template (render-template (prepare-template template) data html-data)))

(deftest repeat-and-normalize-render-test
  (is (do-template "<div>Stock {{stock.name}} {{#stock.sizes}}<span>{{.}}</span>{{/stock.sizes}}</div>"
                   {:stock {:sizes ["xl" "xxl"]}})))

(deftest function-with-nested-body
  (is
   (= [:div
       "Stock "
       [:div
        {:p1 "simple", :p2 1, :p3 2}
        (list [:span "hello"] [:div "b"] " ")]]
      (do-template
       "<div>Stock {{#modal(\"simple\",1,2)}}<span>{{stock}}</span><div>b</div> {{/modal}}</div>"
       {:modal (fn[param1 param2 param3] (fn[nested][:div {:p1 param1 :p2 param2 :p3 param3} nested]))
        :stock "hello"}))))

(deftest primitive-value-list-iteration
  (is
   (=
    [:div "Stock " (list [:span "xl"] [:span "xxl"] [:span 1] [:span 2])]
    (do-template "<div>Stock {{#stock.sizes}}<span>{{.}}</span>{{/stock.sizes}}</div>"
                 {:stock {:sizes ["xl" "xxl" 1 2]}}))))

(deftest map-value-list-iteration
  (is
   (=
    [:div "Stock " (list [:span "xl" "-" "1"] [:span "xxl" "-" "2"])]
    (do-template "<div>Stock {{#stock.sizes}}<span>{{name}}-{{val}}</span>{{/stock.sizes}}</div>"
                 {:stock {:sizes [{:name "xl" :val "1"} {:name "xxl" :val "2"}]}}))))

(deftest map-value-map-iteration
  (is
   (=
    [:div "Stock " (list [:span "m" " " "2"] [:span "xl" " " "1"])]
    (do-template "<div>Stock {{#stock.variants}}<span>{{key.size}} {{val}}</span>{{/stock.variants}}</div>"
                 {:stock {:variants {{:size :xl} "1" {:size :m} "2"}}}))))

(deftest correct-attribute-rendering
  (is (= " <label class=\"btn btn-primary margin10\"> <input type=\"radio\" autocomplete=\"off\" name=\"size\" value=\"s\"></input>s </label>  <label class=\"btn btn-primary margin10\"> <input type=\"radio\" autocomplete=\"off\" name=\"size\" value=\"m\"></input>m </label> "
         (html
          (render-template
           (prepare-template "{{#stock.params.size}} <label class=\"btn btn-primary margin10\"> <input type=\"radio\" autocomplete=\"off\" name=\"size\" value=\"{{.}}\">{{.}} </label> {{/stock.params.size}}")
           {:stock {:params {:size ["s" "m"]}}})))))

(deftest internal-map-keywordized-before-passing-into-function
  (is (= [:button {:onclick 1} "Купить"]
         (do-template "<button onclick=\"{{action.add-to-bucket(stock)}}\">Купить</button>"
                      {:action {:add-to-bucket (fn[{:keys [id]}] id)}
                       :stock {"id" 1}}))))

(deftest correct-this-passing-as-attribute
  (is (=
       (list "axl" "as" "am")
       (do-template "{{#values}}{{action(.)}}{{/values}}"
                    {:action (fn[id] (str "a" id))
                     :values ["xl" "s" "m"]}))))

(deftest template-processing
  (is
   (=
    [:div "Stock " [:span (list "hello" " " [:span "hello"])]]
    (let [template "<span>{{stock}} {{.}}</span>"]
      (do-template
       "<div>Stock {{#template('some')}}<span>{{stock}}</span>{{/template}}</div>"
       {:template (fn[_] (fn[nested]
                           (do-template template {:stock "hello"} {:this nested})))
        :stock "hello"})))))

(deftest nested-template-processing
  (is
   (=
    [:div "Stock "
     [:div "start "
      [:div (list "Parent template: "
                  [:span "Child template : "
                   [:span "Lowest template " "Stock name"]])]]]
    (let [templates {:child  "<div>start {{#template('parent')}}<span>Child template : {{.}}</span>{{/template}}</div>"
                     :parent "<div>Parent template: {{.}}</div>"}
          stock-name "Stock name"]
      (letfn[(template-fn[name]
               (fn[nested]
                 (->
                  (get templates (keyword name))
                  prepare-template
                  (render-template {:stock stock-name :template template-fn}
                                   {:this nested})
                  normalize-template)))]
        (do-template
         "<div>Stock {{#template('child')}}<span>Lowest template {{stock}}</span>{{/template}}</div>"
         {:template template-fn
          :stock stock-name}))))))

(deftest repeat-with-index-and-conditions
  (is
   (=
    "<div>Stock <span>s first  </span><span>m last  not-first  </span></div>"
    (html
     (do-template "<div>Stock {{#stock.size}}<span>{{.}}
 {{#if(first)}} first {{/if}}
 {{#if(last)}} last {{/if}}
 {{#if-not(first)}} not-first {{/if-not}} </span>
 {{/stock.size}}
 </div>"
                  {:stock {:size ["s" "m"]}})))))

(deftest simple-conditional-blocks-with-functions
  (is
   (=
    "<div>Stock  first  </div>"
    (html
     (do-template "<div>Stock {{#current(stock.size.0)}} first {{/current}}
 {{#current(stock.size.1)}} second {{/current}} </div>"
                  {:stock {:size ["s" "m"]}
                   :current (fn[a] (= a "s"))})))))

(deftest simple-conditional-blocks-with-var
  (is
   (=
    "<div>Stock  first  </div>"
    (html
     (do-template "<div>Stock {{#stock.size.0}} first {{/stock.size}}
 {{#stock.size.1}} second {{/stock.size}} </div>"
                  {:stock {:size ["s"]}})))))

(deftest functions-inside-attributes
  (is
   (=
    "<label class=\"btn filter-cell-item left margin10l margin10b first  some-class\"></label>"
    (html
     (do-template "
 <label
 class=\"btn filter-cell-item left margin10l margin10b {{#if(first)}}first{{/if}} {{#if-not(last)}}last{{/if-not}} {{var}}\">
 </label>"
                  {:first true
                   :last true
                   :var "some-class"})))))

(deftest functions-inside-attributes-single-quotes
  (is
   (=
    "<label class=\"btn filter-cell-item left margin10l margin10b first  some-class\"></label>"
    (html
     (do-template "
 <label
 class='btn filter-cell-item left margin10l margin10b {{#if(first)}}first{{/if}} {{#if-not(last)}}last{{/if-not}} {{var}}'>
 </label>"
                  {:first true
                   :last true
                   :var "some-class"})))))

(deftest pass-map-as-parameter
  (is (=
       [:button {:onclick 5} "Купить"]
       (do-template "<button onclick=\"{{action.add-to-bucket({'id' 5})}}\">Купить</button>"
                    {:action {:add-to-bucket (fn[{:keys [id] :as all}] id)}}))))

(deftest special-syntax-for-fn
  (is
   (=
    [:div " " [:button {:action "call1", :disabled 1, :width "100"} "hello"] " "]
    (do-template "<div> <fn fn='button.small-ok' action='{{action.add-to-bucket(stock)}}' disabled='{{stock.id}}' width=100>hello</fn> </div>"
                 {:button {:small-ok (fn[& {:keys[action disabled width] :as params}]
                                       (fn[text] [:button {:action action :disabled disabled :width width} text]))}
                  :stock {:id 1}
                  :action {:add-to-bucket (fn[{:keys [id] :as all}] (str "call" id))}}))))

(deftest keep-tag-dont-processed
  (is (= "1{{b}}3"
         (html (do-template "{{a}}<keep>{{b}}</keep>{{c}}" {:a 1 :b 2 :c 3})))))

(deftest simple-eval
  (is (= "hello 50 world"
         (html
          (do-template "hello {{calc('(category.level-1)*10')}} world"
                       {:calc (fn[text]
                                (eval (compile-expression text)))
                        :category {:level 6}})))))

(run-tests)
