(ns gd.views.themes.custom.main
  (:use gd.views.resources
        com.reasonr.scriptjure
        gd.views.themes.custom.template
        gd.views.themes.custom.inspect
        gd.views.themes.custom.tcomponents
        gd.model.model
        gd.model.context
        gd.utils.web
        gd.utils.common
        gd.views.themes.main
        gd.views.components
        hiccup.core
        [gd.utils.styles :only [css]])
  (:require [gd.views.security :as sec]))

;; :stock-list
;; :filter-panel type name values count-map
;; :stock-template {:keys [name images price params description] :as stock}
;; :settings
;; :main
;; :bucket-item {:keys [stock quantity params id sum] :as item}
;; :bucket-main
;; :order-item item
;; :order order
;; :orders
;; :unregistered
;; :registered-with-no-info
;; :registered-with-filled-info
;; :single-stock {:keys [id images price name all-params description] :as stock}
;; :category-child {:keys [name path level] :as category}
;; :category-list {short-name category} level on-path
;; :pages content
;; :news-detail content
;; :news
;; :guestbook
;; :contacts

(def argument-names {:stock-template [:stock]
                     :single-stock [:stock]
                     :single-param [:type :name :values]

                     :category-list [:child :level :on-path :current]

                     :filter-panel [:type :name :values :count]

                     :bucket-item [:item]
                     :order-item [:item]
                     :order [:order]
                     :order-information [:order]
                     :order-detaile [:order]

                     :category-child [:category]

                     :pages [:content]
                     :news-detail [:content]

                     :news-item-short [:item]
                     :review-item-short [:item]

                     :stock-modal [:stock]})

(defn- do-template[type data html-data]
  (assert (re-find #"^[a-z-]+$" (name type)) "Name of template should have only a-z and -")
  (render-template
   (prepare-template (slurp (as-str "themes/"
                                    (clojure.string/join "/" (map as-str (flatten *current-theme*)))
                                    "/" type ".html")))
   data
   html-data))

(defmethod themes:render :default [type & args]

  (require-js :themes :new-checkout)
  (require-css :lib :jquery.ui.core)
  (require-css :themes-external "/css/themes/basic/stylesheet.css")
  (require-css :themes-external "/css/themes/basic/bootstrap.css")
  (require-css :themes-external "/css/themes/basic/animation.css")
  (require-css :external "http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css")
  (require-css :themes-external "/css/themes/basic/pavproductcarousel.css")
  (require-css :themes-external "/css/themes/basic/pavnewsletter.css")
  (require-css :themes-external "/css/themes/basic/pavcontentslider.css")
  (require-css :themes-external "/css/themes/basic/pavtestimonial.css")
  (require-css :themes-external "/css/themes/basic/pavblog.css")

  (require-css :themes-external "/css/themes/basic/paneltool.css")
  (require-css :themes-external "/css/themes/basic/custom-styles.css")

  (let [args (zipmap (type argument-names) args)
        wrap-button (fn[button-fn]
                      (fn[& params]
                        (fn[text]
                          (apply button-fn (concat [text] params)))))]
    (letfn [(common[]{:link {:catalog (link:all-stocks)
                             :category #'link:category-link}
                      :settings (store-custom)
                      :checkout {:register #'action:register
                                 :delivery (fn[] (next-step (js* (nextStep "payment-address" "confirm"))))
                                 :make-order (fn [] (action:create-order (js* (set! window.location.href "/stocks/"))))
                                 :login #'action:login
                                 :panel (fn[name]
                                          (multi-region* checkout-panel name []
                                            (do-template (str "checkout-" (s* name)) (merge (common) args) {})))
                                 :state #'checkout-selector}
                      :action {:add-to-bucket #'action:add-to-bucket
                               :create-order #'action:create-order
                               :update-user (fn [] (action:update-user (js* (set! window.location.href "/stocks/"))))
                               :open-stock-modal #'action:open-stock-modal
                               :logout (fn [] (action:logout (js* (set! window.location.href "/stocks/"))))
                               :login (fn [] (action:login (js* (set! window.location.href "/profile/"))))
                               :register (fn [] (action:register (js* (set! window.location.href "/stocks/"))))
                               :open-modal (fn [] (modal-window-open true))}
                      :global {:category *current-category*}
                      :user (sec/logged)
                      :include (fn[name]
                                 (do-template name (merge (common) args) {}))
                      :template (fn[name]
                                  (fn[nested]
                                    (do-template name (merge (common) args) {:this nested})))
                      :singleton (fn[]
                                   (fn[nested]
                                     (add-singleton nested)))
                      :button {:small-ok (wrap-button small-ok-button)
                               :cancel (wrap-button cancel-button)
                               :ok (wrap-button ok-button)}
                      :util {:format-date #'format-date}
                      :bucket-info {:total-sum #'info:bucket-sum
                                    :count #'info:bucket-count}
                      :with-subtheme (fn[theme]
                                       (with-meta
                                         (fn[nested]
                                           nested)
                                         {:binding {#'*current-theme* [*current-theme* theme]}}))
                      :inputs {:district #'district-input
                               :city #'city-input
                               :novaya-pochta #'novaya-pochta-input
                               :user-init (fn[]
                                            (let [{:keys [name contact address address-region extra-info]}
                                                  (sec/logged)]
                                              (js (set! address {:district (clj address-region)
                                                                 :city (clj address)
                                                                 :novayaPochta (clj (:novaya_pochta extra-info))})
                                                (set! editMode (clj (case (:delivery extra-info)
                                                                      "other" (empty? address)
                                                                      "ukraine" (empty? (:novaya_pochta extra-info))
                                                                      true)))
                                                (set! deliveryType (clj (or (:delivery extra-info) "ukraine"))))))}
                      :component {:dropdown #'custom-dropdown
                                  :template-seo #'design:template-seo
                                  :inline-bucket #'design:inline-bucket
                                  :popover (fn[& {:keys[title]}]
                                             (fn[nested]
                                               (popover title nested)))
                                  :short-news #'design:short-news
                                  :short-reviews #'design:short-reviews-carousel
                                  :reviews #'render-reviews
                                  :render-all-news #'all-news
                                  :spinner (fn[& {:keys [] :as params}]
                                             (fn[_] (apply spinner (concat [(:id params)] params))))
                                  :stock-carousel #'design:carousel
                                  :stock-carousel-inner #'design:single-carousel-inner
                                  :stock-vertical-carousel-inner #'design:double-vertical-carousel-inner
                                  :stock-quadro-vertical-carousel-inner #'design:quadro-vertical-carousel-inner
                                  :stock-horizontal-carousel-inner #'design:double-horizontal-carousel-inner
                                  :breadcrumbs #'design:breadcrumbs-new
                                  :filters #'design:stock-filters
                                  :modal {:simple (fn[& {:keys [] :or {width 425} :as params}]
                                                    (fn[nested]
                                                      (modal-window-without-header params nested)))}
                                  :image-container #'image-container
                                  :bucket-item #'bucket-item
                                  :stock-list #'design:stock-list
                                  :categories-tree #'link:categories-tree
                                  :categories #'link:custom-categories-tree
                                  :bucket #'design:bucket
                                  :orders #'design:orders
                                  :order-items #'order-items
                                  :order-controlls #'order-controls
                                  :banner-carousel-inner #'design:banner-carousel-inner
                                  :right-banners #'design:right-banners
                                  :footer-top-items #'design:footer-top-items
                                  :footer-center-items #'design:footer-center-items
                                  :image #'images:get}})]
      (try
        (inspect-functions (common))
        (do-template type (merge (common) args) nil)
        (catch java.io.FileNotFoundException e
          [:div (format "File %s.html not found" (name type))])))))
