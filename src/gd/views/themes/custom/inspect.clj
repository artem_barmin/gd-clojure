(ns gd.views.themes.custom.inspect
  (:use gd.views.resources
        gd.views.themes.custom.template
        gd.model.context
        clojure.data.json
        com.reasonr.scriptjure
        gd.utils.web
        clojure.test
        gd.utils.common
        hiccup.page
        hiccup.element
        gd.views.themes.main
        gd.views.components
        hiccup.core
        clojure.walk
        clostache.parser))

(defn- inspect-values[args]
  (let [val-type (fn[v]
                   (cond (sequential? v) "[]"
                         (map? v) "{}"
                         true ""))]
    (cond (and (sequential? args) (every? string? args))
          (str args)

          (and (sequential? args) (seq args))
          (map-indexed (fn[i el] [:div.group.inspect-nested
                                  [:table
                                   [:tr
                                    [:td (str i ":")]
                                    [:td (html (inspect-values el))]]]]) args)

          (and (sequential? args) (empty? args))
          "[]"

          (map? args)
          (map (fn[[k v]] (let [value (html (inspect-values v))
                                hidden? (> (count value) 100)
                                type [:span.inspect-type (val-type v)]
                                k (str k)]
                            [:div.inspect-nested
                             (if hidden?
                               [:a.inspect-toggle-name {:toggle-next true}
                                [:span {:toggle-indicator true :style "margin-right:3px;"}
                                 (if hidden? "+" "-")] k type]
                               [:span.inspect-simple-name k type])
                             ":"
                             [:span {:toggle-panel true :class (when hidden? :none)} value]]))
               (sort-by (fn[[k v]][(val-type v) (str k)]) args))

          (function? args)
          "function"

          true (str args))))

(def cache (->
            (new com.google.common.collect.MapMaker)
            (.expiration 30 java.util.concurrent.TimeUnit/MINUTES)
            (.makeMap)))

(defn inspect-args[args]
  ;; (require-css :util :inspect)
  ;; (require-js :themes :custom-dev)
  (let [args (fix-vectors args)
        id (gen-client-id)]
    (.put cache id args)
    [:div.none.absolute {:template-id true :style "z-index:100;"}
     [:div.inspect-toggle-info {:onclick (js (toggleDebugPanel this (clj (multi-rerender :inspect-value-by-id id))))}
      "V"]
     [:div.none.absolute.inspect-panel (multi-region* inspect-value-by-id id []
                                         (when *in-remote*
                                           (inspect-values (.get cache (s* id)))))]]))

(defn inspect-functions[common]
  (add-singleton
   [:script {:type "text/javascript"}
    (js (set! allFunctions (clj (prewalk
                                 (fn[m]
                                   (cond (var? m)
                                         (let [{:keys [arglists name]} (meta m)]
                                           (str (first arglists)))

                                         (keyword? m)
                                         (name m)

                                         (set? m)
                                         (vec m)

                                         (function? m)
                                         "function"

                                         true m))
                                 common))))]))
