(ns gd.views.themes.custom.tcomponents
  (:use hiccup.core
        hiccup.form
        hiccup.element
        gd.views.components
        gd.views.messages
        gd.model.model
        gd.model.context
        gd.views.themes.main
        gd.utils.web
        gd.utils.styles
        com.reasonr.scriptjure
        clojure.data.json)
  (:require [gd.views.security :as sec])
  (:require [clj-time.coerce :as tm]))

(defn store-custom []
  (:template-settings (:info (:domain (last (domain:get-client-domains))))))

(defn design:short-reviews-carousel []
  (map-indexed (fn [i {:keys [id] :as message}]
        [:div (if (== i 0) {:class "item active"} {:class "item"})
         (let [[title text] (prepare-message message)]
           (themes:render :review-item-short (assoc message
                                               :url (str "/guestbook/#review" id)
                                               :title title
                                               :text text)))])
    (retail:get-review (Integer/parseInt (:mainReviewNumber (:reviewSettings (store-custom)))) 0)))

(defn design:banner-carousel-inner []
  (map-indexed (fn [i image]
         [:div (if (== i 0) {:class "item active"} {:class "item"})
          [:a
           [:img {:src (images:get [858 373] image)}]]])
    (rest (rest (:banners (store-custom))))))

(defn design:right-banners []
  (list
    [:p {:style "margin-bottom:20px"}
     [:img {:src (images:get [273 197] (first (:banners (store-custom))))}]]
    [:p {:style ""}
     [:img {:src (images:get [273 156] (first (rest (:banners (store-custom)))))}]]
    ))

(defn design:footer-top-items []
  (map (fn [element]
         [:div {:class (:rowSize element)}
          [:span {:class (:icon element)}"&nbsp;"]
          [:h3 (:title element)]
          [:p (:description element)]])
    (:footerTop (:footerSettings (store-custom)))))

(defn design:footer-center-items []
  (map (fn [element]
         [:div {:class (:rowSize element)}
          [:div {:class "box pav-custom  "}
           [:div {:class "box-heading"}
            [:h2 (:heading element)]]
           [:div {:class "box-content"}
            [:ul.list
             (map (fn [component]
                    [:li
                     [:a {:href (:href component)} (:text component)]])
               (:content element))]]]])
    (:footerCenter (:footerSettings (store-custom)))))

(defn- review-template [{:keys [text sent-time user id] :as message}]
  (let [[user text] (prepare-message message)]
    [:div.review-list
     [:div {:id (str "review" id)}
      [:div.author
       [:b user]
       (str " "(format-just-date sent-time))]
      [:div.text text]]]))

(defn render-reviews []
  (list
    (if (sec/logged)
    (list
      [:h2#review-title "Оставить отзыв:"]
      [:div.form
       [:b "Текст сообщения"]
       (text-area {:class "input input-big"
                   :cols "40"
                   :rows "8"
                   :style "width: 98%"} "message")
       [:div {:style "text-align:right"}
        [:a {:class "button"
             :onclick (execute-form* bono-guestbook-message [message]
                        (retail:send-review message)
                        :validate [(textarea-required "message" "Введите текст сообщения")]
                        :success (js* (notify "Спасибо" "Ваш отзыв отправлен администрации")))}
         "Оставить отзыв"]]])
    [:div.cuprum.font18px.red
     {:style "margin-bottom:10px;"}
     "Для того чтобы оставить отзыв, вам необходимо войти под своей учетной записью"])

  (pager bono-review
    (retail:get-review (Integer/parseInt (:detailReviewNumber (:reviewSettings (store-custom)))) (c* offset))
    review-template
    :columns 1
    :page (Integer/parseInt (:detailReviewNumber (:reviewSettings (store-custom)))))))


(defn- news-template[{:keys [title url time content-short] :as news}]
  [:div.leading-blogs.clearfix
   [:div.row
    [:div.col-lg-12
     [:div.blog-item
      [:div.image.clearfix]
      [:div.row
       [:div.blog-meta.col-lg-4.col-md-4.col-sm-6.col-xs-12
        [:ul
         [:li.created
          [:span.fa.fa-time "Дата :"] (format-date time)]
         ]]
       [:div.blog-body.col-lg-8.col-md-8.col-sm-6.col-xs-12
        [:div.blog-header.clearfix
         [:h3.blog-title
          [:a {:href (str "/news/detail/" url "/") :title title} title]]]
        [:div.description
         [:p content-short]]
        [:div.blog-readmore
         [:a {:href (str "/news/detail/" url "/") :class "button"} "Читать дальше"]]]]]]]])

(defn all-news []
  (pager bono-news
    (site-pages:get-pages-list (Integer/parseInt (:detailNewsNumber (:newsSettings (store-custom)))) (c* offset) {:type :news})
    news-template
    :columns 1
    :page (Integer/parseInt (:detailNewsNumber (:newsSettings (store-custom))))))

(defn bucket-item [stock]
  [:div {:class "product-extra form"}
   [:div {:class "quantity-adder pull-left"} "Количество:"
    [:div.wrap-qty
     [:span {:class "add-action add-down"
             :onclick "addAction(this)"} "-"]
     (text-field {:id "quantity"
                  :class ".quantity-adder"
                  :style "width: 60px; text-align: center;"} :quantity "1")
     [:span {:class "add-action add-up"
             :onclick "addAction(this)"} "+"]]]
   [:br]
   [:div
    [:div {:class "quantity-adder pull-left"} "Размер: "]
    [:div.margin10l.left (custom-dropdown {:width 20} :size (:size (:params stock)) )]]
   [:br]
   [:button.button {:onclick (do (action:add-to-bucket stock))}
    [:span {:class "fa fa-shopping-cart icon"}]
    [:span.margin10l "В корзину"]]])

(defn image-container [{:keys [images]}]
  [:div {:class ".col-lg-7 col-sm-6 image-container"}
   [:div.image
    [:a
     [:img#image {:class "product-image-zoom"
                  :src (images:get [500 375]  (first images))
                  :data-zoom-image (images:get [600 450]  (first images))} ]]]
   [:div#image-additional {:class "image-additional slide carousel"}
    [:div#image-additional-carousel {:class "carousel-inner"}
     [:div {:class "item active"}
      (map
        (fn [image]
          [:a {:data-image (images:get [600 450] image)}
           [:img.product-image-zoom {:src (images:get [114 86] image)
                  :style "max-width:114px"
                  :data-zoom-image (images:get [600 450] image)}]]) images)]]]
   [:script {:type "text/javascript"}
    "
        $('#image-additional .item:first').addClass('active');
        $('#image-additional').carousel({interval:false})
    "]])


(defn design:template-seo [template-title]
  (javascript-tag (js (.ready ($ document) (fn [] (set! document.title (clj template-title)))))))

(defn checkout-selector []
  (cond
    ;; registered and info not filled
    (sec/logged)
    (js "logged-without-info")
    ;; unregistered(and also not filled info)
    true
    (js "unlogged")))

(defn next-step [& [success]]
  (execute-form* validate-data-before-order []
    (prn param-map)
    :success (or success (reload))
    :keep-data true
    :validate (user-update-validator)))

(defn order-item [{:keys [stock quantity params id sum
                            booked-until closed original-quantity payment-status]
                     :as item}]
  (let [{:keys [name price images]} stock]
    [:tr.form
     [:td.fixed-25px-width-cell.text-center {:style "padding-right:5px;"}
      (custom-checkbox)]
     [:td.fixed-50px-width-cell
      [:a {:href (str "/stock/" (:id stock))}
       (sized-image {:data-title (tooltip [:table
                                           [:tr
                                            (map (fn[img]
                                                   [:td (image (images:get :stock-large img))])
                                              images)]])}
         (images:get :stock-medium (first images)))]]
     [:td.full-width-cell {:style "padding-left:10px;"}
      [:div [:a.bucket-stock-name.bold {:href (str "/stock/" (:id stock))}
             name]]
      [:div [:span.bold "Размер: "] [:span (:size params)]]
      [:div [:span.bold "Цена за шт.: "] [:span price] [:span "грн"]]]

     [:td.fixed-125px-width-cell.text-center original-quantity]

     [:td.fixed-200px-width-cell.text-center
      (when (> quantity 0) [:div quantity " шт. - отложено"])
      (when (> closed 0) [:div {:style "color:#dc5920;"} closed " шт. - нет в наличии"])]

     [:td.fixed-125px-width-cell
      [:div.text-center
       [:span.inline-block sum]
       [:span.inline-block " грн."]]]

     [:td.fixed-200px-width-cell.text-center
      (cond (and (= "created" (order-status)) (not= :payed payment-status) booked-until)
        (format-date-interval (tm/from-date booked-until))

        (= :payed payment-status)
        "Оплачено"

        true
        (! :retail-order.status (order-status)))]

     [:td.text-center
      (if (#{"created" "in-work"} (order-status))
        (list
          [:div.margin5b.delete {:onclick (action:remove-order-item item)}
           [:img {:src "/img/themes/bono/cross_small.png"}]]
          [:div.relative.edit-small {:data-placement "left"
                                     :data-animation "am-flip-x"
                                     :onclick (js (openPopover this))}
           [:img {:src "/img/themes/bono/edit_small.png"}]
           (popover
             "Редактировать заказ"
             [:div.padding5.form {:style "width:208px;"}
              [:div.group.margin10b
               [:div.left {:style (css-inline :line-height 26)} "Количество"]
               [:div.left.margin10l {:style (css-inline :width 108)}
                (text-field {:style (css-inline :height 26 :font-size 18)} :quantity original-quantity)]]
              [:div.cuprum.font18px.fixed-200px-width-cell {:style "margin:0 0 -4px -2px;"}
               (green-button "Применить изменения" :width 180 :action (action:edit-order-item item))]])]))]]))

(defn order-controls [id]
  [:div.controls
   [:div.group
    [:a.block.controls-link.left.margin20t.margin20l {:href (link:all-stocks)}
     [:div.inline-block.arrow-left-icon.margin5r.valign-middle]
     [:p.inline-block.valign-middle "Вернуться к покупкам"]]
    [:a.block.controls-link.right.margin20t.margin20r
     {:href (link:order-print id)}
     [:div.inline-block.printer-icon.margin5r.valign-middle]
     [:p.inline-block.valign-middle "Распечатать заказ"]]]])

(defn order-items [{:keys [user-order-item] :as order}]
  (map (fn [{:keys [id] :as item}]
         (inplace-multi-region
           themes-order-item id []
           (let [item (region-request themes-order-item (orders:get-order-item (s* id)) item)
                 order (region-request themes-order-item (retail:get-order (:fkey_retail-order item)) order)]
             (binding [*current-order* order]
               (order-item item)))))
    (sort-by :id user-order-item)))
