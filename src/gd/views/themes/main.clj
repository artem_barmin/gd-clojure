(ns gd.views.themes.main
  (:use gd.views.resources
        gd.log
        gd.views.themes.external
        compojure.core
        gd.views.seo.meta
        gd.views.bucket.simple
        gd.views.messages
        gd.utils.sms
        gd.utils.mail
        gd.utils.web
        gd.utils.common
        gd.views.components
        noir.core
        [noir.options :only (dev-mode?)]
        [noir.response :only (redirect status set-headers)]
        [clj-time.core :exclude [extend]]
        clojure.algo.generic.functor
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:use gd.model.model)
  (:use gd.model.context)
  (:require [clostache.parser :as clostache])
  (:require [clj-time.coerce :as tm])
  (:require [gd.utils.security :as internal-sec])
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(def ^:dynamic *current-category*)
(def ^:dynamic *current-category-parents*)
(def ^:dynamic *current-stock*)
(def ^:dynamic *current-breadcrumbs*)
(def ^:dynamic *current-page*)
(def ^:dynamic *current-order*)
(def ^:dynamic *stock-mode* :main)

(defmulti themes:render (fn [type & values] [*current-theme* type]))
(defmulti themes:config (fn [] [*current-theme*]))

(defmacro with-subtheme [theme & body]
  `(binding [*current-theme* [~theme *current-theme*]]
     (html ~@body)))

(defmacro web-cached [key root-entity id body]
  `(cached [*current-theme* ~key] ~root-entity ~id (html ~body)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Common partials

(defn link:category-link [{:keys [path] :as category}]
  (str "/stocks" path))

(defn link:all-stocks []
  "/stocks/")

(defn link:order-print [id]
  (str "/service/orders/print/" id))

(defn link:new-stocks []
  "/new/")

(defn link:sale-stocks []
  "/sale/")

(defn link:orders []
  "/orders/")

(defn link:profile []
  "/profile/")

(defn link:stock-link [{:keys [id] :as stock}]
  (str "/stock/" id "/"))

(defn- prepare-order-params [{:keys [size quantity]}]
  {:size size
   :quantity (if (empty? quantity) 1 (if (< (parse-int quantity)  0) 0 (parse-int quantity)))})


(defn- prepare-order-params-no-size [{:keys [quantity]}]
  {:size "m"
   :quantity (if (empty? quantity) 1 (parse-int quantity))})

(defn action:add-to-bucket [{:keys [id] :as stock}]
  (execute-form* themes-add-to-bucket [size quantity]
                 (bucket:add-to-bucket
                  (merge {:fkey_stock *id} (prepare-order-params param-map)))
                 :validate [(textarea-required "size" "Не выбран размер")]
                 :success (js* (notify "Товар добавлен в корзину")
                               (clj (rerender :inline-bucket-rerender)))))

(defn action:change-item [{:keys [id]}]
  (execute-form* themes-change-item-bucket []
                 (bucket:change-item *id (update-in param-map [:quantity ] parse-int))
                 :success (js* (notify "Изменения сохранены")
                               (clj (rerender :inline-bucket-rerender))
                               (clj (multi-rerender :bucket-item id)))))

(defn action:delete-item [{:keys [id]}]
  (execute-form* themes-delete-item-bucket []
                 (bucket:delete *id)
                 :success (js* (notify "Товар удален из корзины")
                               (clj (rerender :inline-bucket-rerender)))))

(defn action:set-main-image [size im]
  (js (.attr (get-closest "[role=main-image]") "src"
             (clj (images:get size im)))))

(defn- open-registration [mode]
  (js (openDialog (byParam "data-registration" (clj (as-str mode))) true)))

(defn- order-modal-state []
  (cond
    ;; registered and info is filled
    (and (sec/logged) (:phone (sec/logged)))
    :registered-with-filled-info ;; registered and info not filled
    (sec/logged)
    :registered-with-no-info ;; unregistered(and also not filled info)
    true
    :unregistered))

(defn action:open-order-modal []
  (open-registration (order-modal-state)))

(defn process-bucket []
  (info "Process bucket for user" (sec/id) (:name (sec/logged)))
  (when-let [items (bucket:plain-items)]
    (->>
     items
     (map (fn [item] (select-keys item [:size :fkey_stock :quantity ])))
     retail:make-order)
    (bucket:clear)))

(defn update-user [{:keys [district city full-address novaya_pochta delivery name phone]}]
  (user:update (sec/id)
               (merge
                (case delivery
                  "ukraine" {:address city
                             :address-region district}
                  "other" {:address full-address})
                {:real-name name
                 :contact {:phone phone}
                 :extra-info {:novaya_pochta novaya_pochta
                              :delivery delivery}})))

(defn user-update-validator []
  [(textarea-required "name" "Необходимо заполнить имя и фамилию")
   (textarea-required "phone" "Необходимо заполнить номер телефона")
   (textarea-phone "phone" "Номер в формате 0xxxxxxxxx")
   (validator-switch
    "delivery"
    (validator-case "ukraine"
                    (dropdown-required "district" "Необходимо указать область")
                    (textarea-required "city" "Необходимо указать город")
                    (textarea-required "novaya_pochta" "Необходимо указать номер отделения Новой почты"))
    (validator-case "other"
                    (textarea-required "full-address" "Необходимо указать адрес доставки")))])

(defn action:update-user [& [success]]
  (execute-form* themes-update-user []
                 (do (update-user param-map)
                     (sec/update-logged-information))
                 :success (or success (reload))
                 :keep-data true
                 :validate (user-update-validator)))

(defn action:create-order [& [success]]
  (execute-form* themes-create-order []
                 (do
                   (update-user param-map)
                   (sec/update-logged-information)
                   (process-bucket))
                 :success (or success (reload))
                 :keep-data true
                 :validate (user-update-validator)))

(defn action:login [& [success]]
  (execute-form* themes-login [login password]
                 (if-let [auth-info (user:check-credentials login password)]
                   (internal-sec/set-logged auth-info)
                   (throwf "Authorization error"))
                 :error (js* (showValidation "Не правильный логин или пароль" (.find form "[name=login]")))
                 :success (or success (reload))
                 :keep-data true
                 :validate [(textarea-required "login" "Необходимо заполнить логин")
                            (textarea-required "password" "Необходимо заполнить пароль")]))

(defn action:open-stock-modal[{:keys [id]}]
  (js (openDialog ($ (clj (str "[data-stock-modal=" id "]"))) true)))

(defn action:logout [& [success]]
  (cljs (remote* themes-logout (internal-sec/logout) :success (fn [] (clj (or success (reload)))))))

(defn login-unique-validator []
  (remote-validator "login"
                    (remote* themes-login-unique
                             (when-not (user:check-email-uniquness (c* value))
                               (throwf "Email not unique")))
                    "Такой логин уже занят"))

(defn action:register [& [success]]
  (execute-form* themes-register [login password password-repeat]
                 (do
                   (invite:use-internal nil login password login "")
                   (internal-sec/set-logged (user:check-credentials login password)))
                 :success (or success (reload))
                 :validate [(textarea-required "login" "Необходимо заполнить логин")
                            (textarea-email "login" "Неправильный формат e-mail")
                            (textarea-required "password" "Необходимо заполнить пароль")
                            (same-passwords "password" "password-repeat" "Пароли не совпадают")
                            (login-unique-validator)]))

(defn action:remove-order-item [{:keys [fkey_retail-order id]}]
  (cljs (remote* themes-remove-order-item
                 (do (retail:delete-order-item (s* id)) nil)
                 :success (fn [] (clj (multi-rerender :themes-order fkey_retail-order))))))

(defn action:edit-order-item [{:keys [fkey_retail-order id]}]
  (execute-form* themes-change-order-item [params]
                 (do (retail:change-order-item *id (prepare-order-params param-map)) nil)
                 :success (multi-rerender :themes-order fkey_retail-order)))

(defn info:bucket-sum []
  (bucket:sum))

(defn info:bucket-count []
  (bucket:count))

(defn info:current-search-query []
  (let [[search query] (drop 2 (.split (:uri (noir.request/ring-request)) "/"))]
    (when (= search "search")
      (ring.util.codec/url-decode query))))

(defn link:categories-tree []
  (let [categories-data (category:user-tree)
        current-path (set (map :id (category:parents (:id *current-category*))))]
    [:div (when-not *current-category* {:class :empty-parent-category})
     (letfn [(category-tree [list-level data]
               [:ul {:class (str "list-level" list-level)}
                (map (fn [[_ {:keys [id name child path level] :as cat}]]
                       (let [current-category (themes:render :category-child cat)]
                         [:li {:class (strings/join
                                       " "
                                       [(if (current-path id) "current-category" "not-current-category")
                                        (if (= (:id *current-category*) id) "selected-category")
                                        (str "level" level)])}
                          current-category (when (seq child) (category-tree (inc list-level) child))]))
                     data)])]
       [:div.categories-tree (category-tree 1 categories-data)])]))

(defn link:custom-categories-tree [& [categories-data level]]
  (let [categories-data (if (and (not categories-data) (not level))
                          (category:user-tree)
                          categories-data)]
    (when (seq categories-data)
      (themes:render :category-list
                     categories-data
                     (inc (or level 0))
                     (fn[& a])
                     (fn[& a])))))

(defn design:inline-bucket[]
  (region* inline-bucket-rerender []
    (themes:render :inline-bucket)))

(defn- resolve-param-name[type]
  (find-by-val :type (name type) (:param (dictionary:get-simple-dictionary))))

(defn design:stock-filters []
  (region* themed-filters-panel []
    (binding [*current-category* (s* (:id *current-category*))
              *stock-mode* (s* *stock-mode*)]
      (let [filters-state (c* (formState ".filters-panel"))
            filter-params {:categories (when *current-category* [{:id *current-category*}])
                           :mode *stock-mode*
                           :search (info:current-search-query)}
            params (retail:get-user-stock-params filter-params)
            counts (retail:get-user-stock-params-count (assoc filter-params :filters filters-state))
            filters-state-set (fmap (comp set maybe-seq) filters-state)]
        [:div.filters-panel (map (fn [[type values]]
                                   (let [filter (:filter (resolve-param-name type))]
                                     (when (seq filter)
                                       (modify-result-attributes
                                        (themes:render :filter-panel type filter values
                                                       (fn [value] (or (get-in counts (map name [type value])) 0))
                                                       (fn [value] (boolean
                                                                    (when-let [values (get filters-state-set (keyword type))]
                                                                      (values value)))))
                                        {:class "filter-panel"}))))
                                 params)]))))

(defn design:params[{:keys [params stock-variant]}]
  [:div
   (javascript-tag (js (set! variants (clj (vec (vals stock-variant))))))
   (map (fn[[name values]]
          (themes:render :single-param name (:name (resolve-param-name name)) values))
        params)])

(defn design:stock-list []
  (let [stock-template
        (fn [{:keys [id] :as stock}]
          (web-cached
           :template-stock :stock id
           [:div
            [:div.none {:data-stock-modal id}
             (modal-window-without-header {} (themes:render :stock-modal stock))]
            (themes:render :stock-template stock)]))]
    (case *stock-mode*
      :main (pager themed-stock-list
                   (retail:supplier-stocks page-size (c* offset)
                                           (let [filters (c* (formState ".filters-panel"))]
                                             {:category (s* (:id *current-category*))
                                              :filters (fmap maybe-seq filters)}))
                   stock-template
                   :columns 3
                   :draw-now true
                   :tr-in-tbody 3
                   :page 24)
      :sale (pager themed-sale-stock-list
                   (retail:supplier-stocks page-size (c* offset)
                                           (let [filters (c* (formState ".filters-panel"))]
                                             {:filters (fmap maybe-seq filters)
                                              :context-status {:wholesale-sale :visible
                                                               :wholesale :visible}}))
                   stock-template
                   :columns 4
                   :register-as themed-stock-list
                   :draw-now true
                   :tr-in-tbody 3
                   :page 24)
      :new (pager themed-new-stock-list
                  (retail:supplier-stocks page-size (c* offset)
                                          (let [filters (c* (formState ".filters-panel"))]
                                            {:filters (fmap maybe-seq filters)
                                             :new true}))
                  stock-template
                  :columns 4
                  :register-as themed-stock-list
                  :draw-now true
                  :tr-in-tbody 3
                  :page 24)
      :search (pager themed-stock-search
                     (retail:supplier-stocks page-size (c* offset)
                                             {:search-string (or (c* (get-val "[name=search]"))
                                                                 (info:current-search-query))})
                     stock-template
                     :columns 3
                     :register-as themed-stock-list
                     :draw-now true
                     :tr-in-tbody 3
                     :page 30))))

(defn design:orders []
  (map
   (fn [{:keys [id] :as order}]
     (inplace-multi-region
      themes-order id []
      (let [order (region-request themes-order (retail:get-order (s* id)) order)]
        (themes:render :order order))))
   (retail:get-user-orders)))

(defn design:short-news []
  (map (fn [{:keys [title url time content-short] :as news}]
         (themes:render :news-item-short (update-in
                                          news [:url ] (fn [url] (str "/news/detail/" url "/")))))
       (site-pages:get-pages-list 3 0 {:type :news :status :published})))

(defn prepare-message [{:keys [text sent-time user id] :as message}]
  (let [old-review-regexp #"^[а-яА-Яa-zA-Z0-9 ]+:"]
    (cond
      (re-find old-review-regexp text) (let [user (re-find old-review-regexp text)]
                                         [user (.substring text (count user))])
      (:address user) [(str (:name user) "(" (:address user) ")") text]
      true [(:name user) text])))

(defn design:short-reviews []
  (map (fn [{:keys [id] :as message}]
         (let [[title text] (prepare-message message)]
           (themes:render :review-item-short (assoc message
                                               :url (str "/guestbook/#review" id)
                                               :title title
                                               :text text))))
       (retail:get-review 3 0)))

(defn design:image-carousel-inner []
  (map-indexed
   (fn [i {:keys [id images name all-params] :as stock}]
     (let [{:keys [brand size color]} all-params]
       [:div (if (== i 0) {:class "item active"} {:class "item"})
        [:div.col-xs-3 [:a {:href (link:stock-link stock)}
                        [:div.image [:img.img-responsive {:src (images:get :stock-large (first images))}]]
                        [:div.img-name name]
                        [:div.img-brand "Производитель: " brand]]]]))
   (retail:supplier-stocks 10 0 {})))

(defn design:single-carousel-inner []
  (map-indexed
   (fn [i {:keys [id images name all-params]}]
     (let [{:keys [brand size color]} all-params]
       [:div (if (== i 0) {:class "item active"} {:class "item"})
        [:div.col-xs-3 [:a {:href (link:stock-link stock)}
                        [:div.image [:img.img-responsive {:src (images:get :stock-large (first images))}]]
                        [:div.img-name name]
                        [:div.img-brand "Производитель: " brand]]]]))
   (retail:supplier-stocks 10 0 {})))

(defn render-product-block [{:keys [id images name all-params price description params] :as stock} image-size]
  (let [{:keys [brand size color]} all-params]
    [:div.product-block
     [:a {:href (link:stock-link stock)}
      [:div.image [:div.image_container [:img.img-responsive {:src (images:get image-size (first images))}]]
       [:a {:class "pav-colorbox cboxElement" :onclick (modal-window-open true)}
        [:span {:class "fa fa-eye"}] "Просмотр"]
       [:div.img-overlay ]]
      [:div.product-meta.form [:div.name [:a {:href (link:stock-link stock)} name]]
       [:div.rating [:img {:src "/img/image/stars-5.png"}]]
       [:div.description (:other description)]
       [:div.price [:span.price-new price " грн"]]
        [:div {:class "cart form"}
         (text-field {:class "none"} :quantity "1")
         (text-field {:class "none"} :size (first (get params "size")))
         [:button.button {:onclick (action:add-to-bucket stock)}
                    [:span {:class "fa fa-shopping-cart icon"}]
                    [:span.margin10l "В корзину"]]]]]
     [:div.hidden (modal-window-without-header {} (themes:render :stock-modal stock))]]))

(defn design:double-vertical-carousel-inner []
  (map-indexed
   (fn [i [stock1 stock2]]
     [:div (if (== i 0) {:class "item active"} {:class "item"})
      [:div {:class "row box-product"}
       [:div {:class "col-lg-6 col-sm-6 col-xs-12"}
        (render-product-block stock1 [280 280])]
       [:div {:class "col-lg-6 col-sm-6 col-xs-12"}
        (render-product-block stock2 [280 280])]]])
   (partition 2 (retail:supplier-stocks 10 0 {}))))

(defn design:quadro-vertical-carousel-inner []
  (map-indexed
   (fn [i [stock1 stock2 stock3 stock4]]
     [:div (if (== i 0) {:class "item active"} {:class "item"})
      [:div {:class "row box-product"}
       [:div {:class "col-lg-3 col-sm-3 col-xs-12"}
        (render-product-block stock1 [280 380])]
       [:div {:class "col-lg-3 col-sm-3 col-xs-12"}
        (render-product-block stock2 [280 380])]
       [:div {:class "col-lg-3 col-sm-3 col-xs-12"}
        (render-product-block stock3 [280 380])]
       [:div {:class "col-lg-3 col-sm-3 col-xs-12"}
        (render-product-block stock4 [280 380])]]])
   (partition 4 (retail:supplier-stocks 10 0 {}))))

(defn design:double-horizontal-carousel-inner []
  (map-indexed
   (fn [i [stock1 stock2]]
     [:div (if (== i 0) {:class "item active"} {:class "item"})
      [:div {:class "row box-product"}
       [:div {:class "col-lg-12 col-sm-12 col-xs-12"}
        (render-product-block stock1 [270 230])]]
      [:div {:class "row box-product"}
       [:div {:class "col-lg-12 col-sm-12 col-xs-12"}
        (render-product-block stock2 [270 230])]]])
   (partition 2 (retail:supplier-stocks 10 0 {}))))

(defn design:carousel []
  [:div.carousel-wrapper
   (region* delayed-carousel []
     (when *in-remote*
       [:div.carousel-info
        [:div#myCarousel.carousel.slide {:data-ride "carousel"}
         [:div.carousel-inner
          (map-indexed
           (fn [i {:keys [id images name all-params] :as stock}]
             (let [{:keys [brand size color]} all-params]
               [:div (if (== i 0) {:class "item active"} {:class "item"})
                [:div.col-xs-3 [:a {:href (link:stock-link stock)}
                                [:img.img-responsive {:src (images:get :stock-large (first images))}]
                                [:div.img-name name]
                                [:div.img-brand "Производитель: " brand]]]]))
           (retail:supplier-stocks 10 0 {}))]
         [:a.left.carousel-control {:href "#myCarousel" :data-slide "prev"}]
         [:a.right.carousel-control {:href "#myCarousel" :data-slide "next"}]]]))
   (document-ready (rerender :delayed-carousel))])

(defn design:registration-modal []
  (themes:render :unregistered))

(defn design:bucket []
  (map! (fn [{:keys [id] :as item}]
          (inplace-multi-region
           bucket-item id []
           (let [item (region-request bucket-item (bucket:get-item (s* id)) item)]
             (themes:render :bucket-item item))))
        (bucket:items)))

(defn design:order-items [{:keys [user-order-item] :as order}]
  (map (fn [{:keys [id] :as item}]
         (inplace-multi-region
          themes-order-item id []
          (let [item (region-request themes-order-item (orders:get-order-item (s* id)) item)
                order (region-request themes-order-item (retail:get-order (:fkey_retail-order item)) order)]
            (binding [*current-order* order]
              (themes:render :order-item item)))))
       (sort-by :id user-order-item)))

(defn order-status []
  (:status *current-order*))

(defn design:breadcrumbs []
  (list
   [:div.breadcrumbs-home-icon ]
   [:div.breadcrumbs [:span {:typeof "v:Breadcrumb"}
                      (interpose
                       [:span.breadcrumb-separator ]
                       (concat
                        [[:a {:property "v:title"
                              :href "/"
                              :rel "v:url"
                              :style "padding: 0px;"}
                          "Главная"]]
                        (map (fn [{:keys [name path]}]
                               [:a {:href path} name])
                             (butlast *current-breadcrumbs*))
                        [[:span.last-item (:name (last *current-breadcrumbs*))]]))]]))

(defn design:breadcrumbs-new []
  [:ol.breadcrumb
   [:li [:a {:href "/"} "Главная"]]
   (map (fn [{:keys [name path]}]
          [:li [:a {:href path} name]])
        (butlast *current-breadcrumbs*))
   [:li (:name (last *current-breadcrumbs*))]])

(defn process-required [type]

  (require-js :custom-lib :validation)
  (require-js :custom-lib :common)
  (require-js :custom-lib :fixed-panel)

  (require-js :admin :common)

  (require-js :custom-lib :history-utils)
  (require-js :lib :history)

  (require-js :custom-lib :notify)
  (require-css :custom-lib :notify)
  (require-css :user :main)

  (require-js :themes :app)
  (require-js :custom-lib :modal-panel)

  (require-js :lib :angular-strap.tpl)
  (require-js :lib :angular-strap)
  (require-js :lib :angular-animate)
  (require-js :lib :lodash)

  (require-js :external :jquery-ui)
  (require-js :external :jquery)
  (require-js :external :angular-sanitize)
  (require-js :external :angular)
  (require-js :external :bootstrap-js)
  (require-css :external :bootstrap-css)
  (require-css :lib :angular-strap)

  ;; process other required scripts
  (process-js-and-css type))

(defn theme-resources []
  (list
   (process-required :css)
   (process-required :js)
   (str "<link href='http://fonts.googleapis.com/css?family=Cuprum:700&subset=cyrillic' rel='stylesheet' type='text/css'>")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SEO

(defn- get-site-page-for-current-url [& [url]]
  (let [url
        (or
         url
         (-> (:uri (noir.request/ring-request))
             (.replaceAll "/themes/" "")
             (.replaceAll "/page[0-9]+" "")
             (.replaceAll "/$" "")))]
    (if (seq url) url "main")))

(defn- fill-seo-from-page-object [seo]
  (let [{:keys [title description keywords]} seo]
    (when seo
      (when (seq title) (set-title title))
      (when (seq keywords) (set-meta :keywords keywords))
      (when (seq description) (set-meta :description description)))))

(defn- fill-seo []
  (let [available? (fn [var] (and (thread-bound? var) (deref var)))]

    ;; default values, available only for categories
    (when-let [name (:name (available? #'*current-category*))]
      (set-title name "оптом в Украине - Интернет магазин")
      (set-meta :keywords name "оптом в Украине - Интернет магазин")
      (set-meta :description "На нашем сайте представлены" name
                "оптом в Украине по доступным ценам, стоимости - Интернет магазин"))


    (when-let [title (:title (available? #'*current-page*))]
      (set-title title "- Bono - новости")
      (set-meta :keywords title)
      (set-meta :description title))

    ;; override by custom values
    (fill-seo-from-page-object
     (cond (let [seo (:seo (available? #'*current-page*))]
             (and seo (every? (comp seq val) seo)))
           (:seo *current-page*)

           true
           (:seo (site-pages:get-page-for-url (or
                                               (:path (available? #'*current-category*))
                                               (get-site-page-for-current-url))))))

    ;; append page number for stock list and news
    (when (and (or (available? #'*current-category*)
                   (available? #'*current-page*))
               (not= (pager-current-page) 1))
      (update-meta :keywords " - страница " (pager-current-page))
      (update-meta :description " - страница " (pager-current-page))
      (noindex)
      (let [url (str "http://bono.in.ua" (current-url))]
        (set-link :canonical url)
        (set-link :next (str url "page" (inc (pager-current-page))))
        (set-link :prev (str url "page" (dec (pager-current-page))))))

    ;; stock templates from category
    (when (and (available? #'*current-category*) (available? #'*current-stock*))
      (let [{:keys [title-template keyword-template description-template]} (:seo *current-category*)
            {:keys [name price category]} *current-stock*
            name (-> name
                     (clojure.string/replace #"[0-9]" "")
                     (clojure.string/replace #"  " " "))
            sex (:name (first *current-category-parents*))
            sex (cond
                  (not sex) nil
                  (re-find #"(?ium)муж" sex) "мужские"
                  (re-find #"(?ium)жен" sex) "женские"
                  (re-find #"(?ium)дет" sex) "детские")
            template-info {:sex sex :name name :price price :category (.toLowerCase (:name category))}]
        (set-title
         (clostache/render
          (or title-template "{{name}} | Купить {{sex}} {{category}} в интернет-магазине Боно")
          template-info))
        (swap! *seo-parts* dissoc-in [:meta :keywords])
        (set-meta
         :description (clostache/render
                       (or description-template "{{name}} - Купить {{sex}} {{category}} в интернет-магазине Боно. Оптом по цене {{price}} грн.")
                       template-info))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Endpoints

(defn page-404[]
  (noir.response/status 404
                        (noir.response/content-type "text/html;charset=utf-8"
                                                    (noir.statuses/get-page 404))))

(defn- common-layout [body]
  (fill-seo)
  (let [body (html
              [:body {:ng-controller "ThemesController"}
               (html body)
               [:div {:data-registration (order-modal-state)}
                (themes:render (order-modal-state))]
               (render-singletons)])]
    (html [:html {:ng-app "app"}
           [:head [:link {:rel "icon" :href "/img/themes/bono/favicon.ico" :type "image/x-icon"}]
            [:link {:rel "shortcut icon" :href "/img/themes/bono/favicon.ico" :type "image/x-icon"}]
            [:link {:rel "shortcut icon" :href "/img/themes/bono/favicon.ico" :type "image/vnd.microsoft.icon"}]
            [:link {:rel "canonical" :href (str "http://bono.in.ua" (:original-uri noir.request/*request*))}]

            [:title (get-title)]
            (get-meta)
            (theme-resources)
            (when-not (dev-mode?)
              (google-analytics))
            (when-not (dev-mode?)
              (yandex-metrika))]
           body])))

(defmacro catch-to-404[& body]
  `(try
     ~@body
     (catch Exception e#
       (if (= (.getMessage e#) "Wrong page")
         (page-404)
         (throw e#)))))

(defpage "/themes/" []
  (binding [*current-page* (site-pages:get-page-for-url (get-site-page-for-current-url))]
    (common-layout (themes:render :main))))

(defn- stock-list [category-info]
  (binding [*current-category* (category:tranform-path-to-current-category category-info)]
    (if (and (not (= category-info nil)) (= *current-category* nil))
      (page-404)
      (binding [*current-page* (site-pages:get-page-for-url (:path *current-category*))
                *current-breadcrumbs* (cons
                                       {:name "Список товаров"
                                        :path "/stocks/"}
                                       (map (fn [{:keys [name] :as category}]
                                              {:name name
                                               :path (link:category-link category)})
                                            (reverse (category:parents (:id *current-category*)))))]
        (common-layout (themes:render :stock-list))))))

(compojure-route
 (GET ["/themes/stocks/:path/" :path #".*"] [path]
      (catch-to-404 (stock-list path))))

(defpage "/themes/stocks/" []
  (catch-to-404 (stock-list nil)))

(defpage "/themes/stock/:id/" {id :id}
  (let [stock (retail:get-stock (parse-int id))]
    (if (or (= (:id stock) nil) (every? #(= 0 %) (vals (:left-stocks stock))))
      (page-404)
      (let [{:keys [category name] :as stock} stock
            parents (reverse (category:parents (:id category)))]
        (binding [*current-category* category
                  *current-stock* stock
                  *current-category-parents* parents
                  *current-breadcrumbs* (conj (vec (map (fn [{:keys [name] :as category}]
                                                          {:name name
                                                           :path (link:category-link category)})
                                                        parents))
                                              {:name name})]
          (common-layout (themes:render :single-stock stock)))))))

(defpage "/themes/new/" []
  (binding [*current-breadcrumbs* [{:name "Новые поступления" :path "/new/"}]
            *stock-mode* :new
            *dont-throw-exception* true]
    (common-layout (themes:render :stock-list))))

(defpage "/themes/sale/" []
  (binding [*current-breadcrumbs* [{:name "Распродажа" :path "/sale/"}]
            *stock-mode* :sale ]
    (common-layout (themes:render :stock-list))))

(compojure-route
 (GET ["/themes/search/:query/" :query #".*"] [query]
   (binding [*current-breadcrumbs* [{:name "Результаты поиска" :path (str "/search/" query "/")}]
             *stock-mode* :search ]
     (common-layout (themes:render :stock-list)))))

(defpage "/themes/news/" []
  (noindex)
  (binding [*current-breadcrumbs* [{:name "Новости" :path "/news/"}]]
    (common-layout (themes:render :news))))

(defpage "/themes/news/detail/:page/" {page :page}
  (noindex)
  (let [{:keys [title content] :as current-page} (site-pages:get-page-for-url page)]
    (binding [*current-breadcrumbs* [{:name "Новости" :path "/news/"}
                                     {:name title :path (str "/news/detail/" page "/")}]
              *current-page* current-page]
      (common-layout (themes:render :news-detail content)))))

(defpage "/themes/contacts/" []
  (binding [*current-breadcrumbs* [{:name "Контакты" :path "/contacts/"}]]
    (common-layout (themes:render :contacts))))

(defpage "/themes/profile/" []
  (binding [*current-breadcrumbs* [{:name "Настройки профиля" :path "/profile/"}]]
    (common-layout (themes:render :settings))))

(defpage "/themes/orders/" []
  (binding [*current-breadcrumbs* [{:name "Мои заказы" :path "/orders/"}]]
    (common-layout (themes:render :orders))))

(defpage "/themes/bucket/" []
  (binding [*current-breadcrumbs* [{:name "Корзина" :path "/bucket/"}]]
    (common-layout (themes:render :bucket-main))))

(defpage "/themes/guestbook/" []
  (noindex)
  (binding [*current-breadcrumbs* [{:name "Гостевая" :path "/guestbook/"}]]
    (common-layout (themes:render :guestbook))))

(defpage "/themes/pages/:page/" {page :page}
  (let [{:keys [title content] :as current-page} (site-pages:get-page-for-url page)]
    (binding [*current-page* current-page
              *current-breadcrumbs* [{:name title :path (str "/" page "/")}]]
      (common-layout (themes:render :pages content)))))

(defpage "/themes/main-profile/" []
  (binding [*current-breadcrumbs* [{:name "Аккаунт" :path "/main-profile/"}]]
    (common-layout (themes:render :main-profile))))

(defpage "/themes/login/" []
  (binding [*current-breadcrumbs* [{:name "Логин" :path "/login/"}]]
    (common-layout (themes:render :login))))

(defpage "/themes/register/" []
  (binding [*current-breadcrumbs* [{:name "Регистрация" :path "/register/"}]]
    (common-layout (themes:render :register))))

(defpage "/themes/checkout/" []
  (binding [*current-breadcrumbs* [{:name "Создание заказа" :path "/checkout/"}]]
    (common-layout (themes:render :checkout))))
