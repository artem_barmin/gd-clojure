(ns gd.views.lost_pages
  (:use noir.statuses
        hiccup.core
        hiccup.page
        gd.utils.web
        gd.views.themes.main
        gd.views.themes.bono.basic))

(set-page! 404
           (html5
            (binding [noir.request/*request* {:uri "mine"}
                      *singletons-list* (atom ())]
              (#'gd.views.themes.bono.basic/common-layout
               [:div.group
                [:div.left-header-img.left]
                [:div.center-header-img.left]
                [:div.right-header-img.left]]

               [:div.center {:style "height:100%;width:1050px;"}
                [:div.page-content
                 [:div {:class "page content" :height "300"}
                  [:h1 {:align "center"} "Error 404 : Cтраница не найдена." ]
                  [:h2 {:align "center"} [:a {:href "/stocks/" } "перейти: \"Каталог товаров\""]]]]]))
            (require-css :user :main)
            (process-required :css)
            (include-js "/js/failover/jquery.min.js")
            (include-js "/js/failover/jquery-ui.min.js")
            (include-js "/css/landing/bootstrap/js/bootstrap.js")
            (include-js "/js/lib/angular.js")
            (include-js "/js/lib/angular-strap.js")
            (include-js "/js/lib/angular-strap.tpl.js")
            (include-js "/js/lib/history.js")
            (include-js "/js/lib/lodash.js")
            (include-js "/js/lib/angular-animate.js")
            (include-js "/js/custom-lib/common.js")
            (include-js "/js/custom-lib/popover.js")
            (include-js "/js/custom-lib/modal-panel.js")
            (include-js "/js/user/login.js")))
