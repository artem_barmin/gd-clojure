(ns gd.views.admin.orders
  (:use gd.model.model
        [clojure.algo.generic.functor :only (fmap)]
        gd.model.context
        gd.views.admin.order_wizard
        gd.views.admin.stocks
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        clojure.data.json
        gd.views.components
        gd.views.admin.stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        [clj-time.core :exclude [extend]]
        hiccup.element)
  (:require [clostache.parser :as clostache])
  (:require [clj-time.coerce :as tm])
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Custom notifications for user

(def custom-notification-temlates
  {:order-sum {:email {:header "Данные для оплаты заказа № {{order.id}}"
                       :body [:div
                              [:p "Уважаемый клиент,"]
                              [:p "Сумма к оплате по Вашему заказу № {{order.id}} {{order.summarySum}} грн."]
                              [:br]
                              [:p "В том числе:"]
                              [:p "Сумма заказа: {{order.wholeSum}}"]
                              [:p "Скидка: {{order.discount}}"]
                              [:p "Сумма со скидкой: {{order.sumWithDiscount}}"]
                              [:p "Расходы на банковские услуги: {{order.paymentExpenses}}"]
                              [:p "Итоговая сумма: {{order.summarySum}}" ]
                              [:br]
                              [:p "Номер карты ПриватБанка 5168 7423 4527 6657. Получатель: Колесник Владимир."]
                              [:p "Подробную информацию о Вашем заказе Вы можете увидеть в Вашем кабинете на сайте:"
                               [:a {:href "http://bono.in.ua/orders/"} "http://bono.in.ua/orders/"]]]}
               :sms "Suma k oplate {{order.summarySum}} grn.Karta PrivatBanka 5168 7423 4527 6657. Poluchatel Kolesnik Vladimir.Podrobnosti v lichnom kabinete. Spasibo za zakaz!"}
   :declaration-number {:email {:header "Ваша посылка с заказом № {{order.id}} отправлена."
                                :body [:div
                                       [:p "Уважаемый клиент,"]
                                       [:p "Ваша посылка с заказом № {{order.id}} отправлена."]
                                       [:br]
                                       [:p "Номер декларации Новой почты:"
                                        [:a {:href "http://novaposhta.ua/frontend/tracking/ua"} "{{order.declarationNumber}}"]]
                                       [:p "Подробную информацию о Вашем заказе Вы можете увидеть в Вашем кабинете на сайте: "
                                        [:a {:href "http://bono.in.ua/orders/"} "http://bono.in.ua/orders/"]]
                                       [:br]
                                       [:p "Мы будем Вам благодарны, если Вы оставите отзыв о нашей работе: "
                                        [:a {:href "http://bono.in.ua/guestbook/"} "Отзывы"]]]}
                        :sms  "Vasha posylka otpravlena. Nomer deklaracii Novoj pochty {{order.declarationNumber}}. Vsego Vam nailutshego!!!"}})

(defn- order-custom-user-notifications[{:keys [id sum-can-discounted status additional-info user] :as order}
                                       discount sum-with-discount payment-expenses summary-sum]
  (let [render-data
        {:id id
         :wholeSum sum-can-discounted
         :discount discount
         :sumWithDiscount sum-with-discount
         :paymentExpenses payment-expenses
         :summarySum summary-sum
         :declarationNumber (:declaration-number additional-info)}

        owner-id
        (:id user)

        get-check-key
        (fn[info type]
          (get-in {:declaration-number {:sms :sms-for-declaration :email :email-for-declaration}
                   :order-sum {:sms :sms-for-order :email :email-for-order}} [info type]))

        render-template
        (fn[info type data]
          (let [template (get-in custom-notification-temlates [info type])
                process (fn[tmpl] (if data (clostache/render (html tmpl) {:order data}) (html tmpl)))]
            (case type
              :sms {:body (process template)}
              :email {:body (process (:body template))
                      :header (process (:header template))})))

        custom-notification-button
        (fn[info type button-text]
          [:div {:ng-attr-data-title (tooltip [:div (let [{:keys [header body]} (render-template info type nil)]
                                                      (list header body))])}
           ((if ((get-check-key info type) additional-info) small-danger-button small-warning-button)
            button-text
            :width 110
            :action (js
                     (if (confirm (clj (str "Вы уверены что хотите оповестить пользователя по " button-text "?")))
                       (clj
                        (remote* order-custom-notify
                                 (let [data (c* (getCurrentOrderInfo))
                                       [type info owner-id] [(s* type) (s* info) (s* owner-id)]
                                       {:keys [header body]} (render-template info type data)]
                                   (case type
                                     :email (user:send-mail owner-id header body)
                                     :sms (user:send-sms owner-id body))
                                   (retail:change-order-additional-info (s* id) {(get-check-key info type) true
                                                                                 info (get data ({:declaration-number :declarationNumber
                                                                                                  :order-sum :order-sum}
                                                                                                 info))})
                                   nil)
                                 :success (fn[]
                                            (clj (multi-rerender :supplier-order-region id))
                                            (alert "Сообщение успешно отправлено")))))))])]
    [:div {:ng-controller "OrdersCustomNotificationCtl"}
     [:table.width100.not-print {:ng-init (js (set! order (clj render-data)))}
      [:tr
       (when (= :in-work status) [:td {:width "445px"} [:div.admin-order-total "Отправка суммы заказа клиенту"]])
       [:td [:div.admin-order-total "№ декларации"]]]
      [:tr
       (when (= :in-work status)
         [:td
          [:table
           [:td.width
            (spinner :order-sum :value (or summary-sum 0) :min 0 :width 80 :add-on "грн." :attrs {:ng-model "order.summarySum"})]
           [:td.padding5l (custom-notification-button :order-sum :sms "SMS")]
           [:td.padding5l (custom-notification-button :order-sum :email "e-mail")]]])

       [:td.padding5l
        [:table
         [:tr
          [:td.width
           (text-field {:class "input input-text left" :ng-model "order.declarationNumber"} :declaration-number)]
          [:td.padding5l (custom-notification-button :declaration-number :sms "SMS")]
          [:td.padding5l (custom-notification-button :declaration-number :email "e-mail")]]]]]]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Modal info
(defn- admin-order-user-info[{:keys [id real-name full-address contact extra-info]}]
  [:div.panel.transparent-panel
   [:table.table
    [:tr
     [:td.name.left250px.fixed-150px-width-cell "Имя"]
     [:td.address.left250px.full-width-cell "Адрес"]
     [:td.phone.left250px.fixed-150px-width-cell "Телефон"]
     [:td.mail.left250px.fixed-150px-width-cell "Email"]
     [:td.not-print.fixed-75px-width-cell "Вход на сайт"]]
    [:tr
     [:td.name.left350px [:div [:a {:href (str "/admin/users/" (get-in contact [:email :value])) :target "_blank"} real-name]]]
     [:td.address.left350px [:span full-address]
      (if (seq (:novaya_pochta extra-info))
        [:span (:novaya_pochta extra-info)])]
     [:td.phone.left350px (get-in contact [:phone :value])]
     [:td.mail.left350px (get-in contact [:email :value])]
     [:td.not-print
      [:div [:a.link {:href "/index.php?route=account/account"
                      :target "_blank"
                      :onclick (js (if (confirm "Вы уверены что хотите войти как пользователь?")
                                     (clj (remote* retail-login-as-user
                                                   (user:login-as-user (s* id))))))} "Залогиниться"]]]]]])

(defn- admin-single-order-item[mode {:keys [id fkey_stock stock params original-quantity quantity sum
                                            note status closed fkey_group-deal fkey_retail-order
                                            additional-expense booked-until payment-status]
                                     :as order-item}]
  (let [selected-context (:selected-context stock)]
    (with-group id
      [:tr.admin-table-body-container
       (when (= mode :edit)
         [:td.not-print
          (small-danger-button
           [:i.glyphicon.glyphicon-sm.glyphicon-remove {:style "margin:0 -7px;"}]
           :width 32
           :action (cljs (remote* remove-retail-admin-bucket-item
                                  (do (retail:delete-order-item (s* id)) nil)
                                  :success (fn[]
                                             (organizerOrdersReload)
                                             (clj (multi-rerender :supplier-order-region fkey_retail-order))))))])

       [:td
        (hidden-field :id id)
        [:a {:href (str "/stock/" (:id stock))
             :target "_blank"
             :title
             (tooltip
              [:div {:style "padding:0 5px 5px; font-size:10px !important;"}
               [:div (draw-category (:fkey_category stock))]])}
         (:name stock)
         (when (= selected-context :wholesale-sale) "(распродажа)")]]

       [:td (render-params {:params params})]
       [:td (:price stock) " грн."]
       [:td
        (if (= mode :edit)
          (spinner :quantity
                   :add-on "шт."
                   :width 60
                   :value original-quantity
                   :min 0)
          [:p {:style "margin:0;"} original-quantity])]

       [:td.text-center quantity]
       [:td.text-center closed]
       [:td sum " грн."]

       (when (= mode :edit)
         [:td.not-print
          (spinner :returned
                   :width 60
                   :add-on "шт."
                   :value (:items (find-by-val :type :return-stocks additional-expense))
                   :min 0
                   :max quantity)])])))

(defn- admin-order-items-info[user-order-item]
  [:div.admin-order-holder.order-details
   [:table.table
    [:tr.admin-table-head
     [:td.fixed-50px-width-cell.not-print]
     [:td.full-width-cell {:style "margin-left:55px;"} "Наименование"]
     [:td.fixed-100px-width-cell "Параметры"]
     [:td.fixed-75px-width-cell "Цена"]
     [:td.fixed-100px-width-cell "Заказ"]
     [:td.fixed-75px-width-cell.text-center "Бронь"]
     [:td.fixed-75px-width-cell.text-center "Нет"]
     [:td.fixed-75px-width-cell "Итого"]
     [:td.not-print.fixed-125px-width-cell "Возврат"]]
    (with-group :items
      (html
       (map (partial admin-single-order-item :edit) (sort-by :id user-order-item))))]])


(defn- admin-declaration-and-notes-save-changes[id]
  (let [admin-order-save-or-recompute
        (fn[callback]
          (execute-form* retail-save-order-change[delivery-expenses return-expenses
                                                  payment-expenses discount items
                                                  note declaration-number status]
                         (do
                           (retail:change-order-additional-info
                            *id {:note note
                                 :declaration-number declaration-number})
                           (additional-expense:assign-or-update
                            *id
                            :retail-order
                            (remove nil?
                                    [{:type :return-expenses :money (parse-int return-expenses)}
                                     {:type :delivery-expenses :money (parse-int delivery-expenses)}
                                     {:type :discount :percent (parse-int discount)}
                                     {:type :payment-expenses :percent (if payment-expenses 1 0)}]))
                           (let [items (vals items)
                                 items (map (fn[m] (update-multi m [:id :quantity :returned] parse-int)) items)]
                             (when-not (= :closed (keyword status))
                               (retail:change-order items))
                             (doseq [{:keys [returned booked-hours quantity id]} items]
                               (when (> returned 0)
                                 (additional-expense:assign-or-update
                                  id
                                  :user-order-item
                                  [{:type :return-stocks :items returned}]))))
                           (retail:set-order-status *id (keyword status))
                           nil)
                         :success callback))]
    (list
     [:div.left.margin5r
      (small-success-button
       "Применить"
       :width 120
       :action (admin-order-save-or-recompute (js* (organizerOrdersReload)
                                                   (clj (multi-rerender :supplier-order-region id)))))]
     [:div.left
      (small-ok-button
       "Сохранить"
       :width 120
       :action (admin-order-save-or-recompute (js* (organizerOrdersReload)
                                                   (closeDialog))))])))

(defn- admin-order-additional-expenses[{:keys [id additional-expense sum-can-discounted sum status additional-info user] :as order}
                                       discount sum-with-discount payment-expenses summary-sum]
  (let [val-by-type (fn[type](find-by-val :type type additional-expense))]
    [:div.print-summary
     [:div.group
      [:div.admin-order-holder.left
       [:table.additional-payment-info
        [:tr.discount
         [:td.first-td.not-print
          [:p "Скидка:"]]
         [:td.order-discount.middle-td.not-print
          (spinner :discount
                   :add-on "%"
                   :width 80
                   :value (:percent (val-by-type :discount))
                   :min 0)]
         [:td.name-td
          [:div "Скидка: "]]
         [:td.value-td
          [:div (or discount "0") " грн."]]]
        [:tr.delivery-expenses
         [:td.first-td.not-print
          [:p "Расходы:"]]
         [:td.order-delivery-expenses.middle-td.not-print
          (spinner :delivery-expenses
                   :add-on "грн."
                   :width 80
                   :value (:money (val-by-type :delivery-expenses))
                   :min 0)]
         [:td.name-td
          [:div "Сумма со скидкой: "]]
         [:td.value-td
          [:div (or sum-with-discount "0") " грн."]]]
        [:tr.payment-expenses
         [:td.first-td.not-print
          [:p "Банковские услуги 1%:"]]
         [:td.order-payment-expenses.middle-td.not-print
          (let [payment (:percent (val-by-type :payment-expenses))]
            (custom-checkbox :payment-expenses (and payment (> payment 0))))]
         [:td.name-td
          [:div "Расходы на банковские услуги: "]]
         [:td.value-td
          [:div (or payment-expenses "0") " грн."]]]
        [:tr.return-expenses
         [:td.first-td.not-print
          [:p "Расходы по возврату:"]]
         [:td.order-return-expenses.middle-td.not-print
          (spinner :return-expenses
                   :add-on "грн."
                   :width 80
                   :value (:money (val-by-type :return-expenses))
                   :min 0)]
         [:td.name-td
          [:div "Итоговая сумма:"]]
         [:td.value-td
          [:div (or summary-sum "0") " грн."]]]]]

      [:div.group
       [:div.left.margin5.not-print
        [:a {:target "_blank" :href (str "/service/orders/print/" id)}
         [:div.enabled.center.text-center.plain-button.orange-button-plain.small-danger-button-plain
          {:style "width:85px;"} "Печать"]]]

       [:div.orange-dropdown.right.margin5.not-print {:style "width:170px;"}
        (custom-dropdown :status (map (fn[st]
                                        [(! :retail-order.status st) st])
                                      (remove nil? [:created :prepared-to-work :in-work :payed-cash :payed-clearing (when (sec/partner?) :closed)]))
                         status)]]]
     [:div.admin-order-total.inner-info-title.not-print
      [:div.text-left "Комментарий клиента к заказу"]]
     [:div.not-print (text-area {:class "input input-textarea" :readonly ""} :comment (:comment additional-info))]
     [:div.admin-order-total.inner-info-title.not-print
      [:div.text-left "Примечание к заказу (для внутреннего использования)"]]
     [:div.not-print (text-area {:class "input input-textarea"} :note (:note additional-info))]

     (order-custom-user-notifications order discount sum-with-discount payment-expenses summary-sum)]))

(defn- admin-order-items-sum [{:keys [original-sum sum user-order-item]}]
  [:div.admin-order-total.print-summary {:style "margin-top:-3px;"}
   [:p "Итого по заказу: " (or original-sum sum) " грн. "]
   [:p "Количество товаров: " (reduce + (map :quantity user-order-item)) " шт. "]])

(defn- admin-order-modal-content[{:keys [user-order-item id status user-type additional-info user
                                         additional-expense sum original-sum
                                         discount payment-expenses sum-with-discount contexts]
                                  :as order}]
  [:div.form.margin5
   [:div.bono-logo.left0px]
   [:div
    (admin-order-user-info user)
    (admin-order-items-sum order)
    (admin-order-additional-expenses order discount sum-with-discount payment-expenses sum)
    [:div.group.margin5t (admin-declaration-and-notes-save-changes id)]
    [:div.margin5t (admin-order-items-info user-order-item)]]])

(defn admin-stock-edit-modal[{:keys [id] :as order}]
  (fancy-lazy-modal-with-header retail-admin-order (str "Редактирование заказа №" id) {:id id :width 1000}
    (let [order (retail:get-order (s* id))]
      (multi-region* supplier-order-region id []
        (let [order (region-request supplier-order-region (retail:get-order (s* id)) order)]
          (admin-order-modal-content order))))
    (modal-save-panel (admin-declaration-and-notes-save-changes (s* id)))))

(defn order-diff-modal-content[{:keys [diff id user-order-item] :as order}]
  (let [order {:added 1 :deleted 0 :changed 2}]
    [:div.padding5
     [:div.margin10b
      (map (fn[[type items]]
             [:div {:class type}
              [:div.margin5b (! :order.diff type)]
              [:table.admin-order-holder
               [:tr.admin-table-head
                [:td.full-width-cell "Наименование"]
                [:td.fixed-100px-width-cell "Параметры"]
                [:td.fixed-75px-width-cell "Цена"]
                [:td.fixed-100px-width-cell "Заказ"]
                [:td.fixed-75px-width-cell.text-center "Бронь"]
                [:td.fixed-75px-width-cell.text-center "Нет"]
                [:td.fixed-75px-width-cell "Итого"]]
               (map (partial admin-single-order-item :view) items)]])
           (sort-by (comp order first) diff))]

     [:div
      [:div.margin5b "Весь заказ"]
      [:table.admin-order-holder
       [:tr.admin-table-head
        [:td.full-width-cell "Наименование"]
        [:td.fixed-100px-width-cell "Параметры"]
        [:td.fixed-75px-width-cell "Цена"]
        [:td.fixed-100px-width-cell "Заказ"]
        [:td.fixed-75px-width-cell.text-center "Бронь"]
        [:td.fixed-75px-width-cell.text-center "Нет"]
        [:td.fixed-75px-width-cell "Итого"]]
       (map (partial admin-single-order-item :view) user-order-item)]]]))

(defn- order-user-modal [{:keys [id name full-address contact auth-profile] :as user}]
  (modal-window-admin
    (if id (str "Редактирование пользователя №" id) "Создание нового пользователя")
    (modal-save-panel
      (small-ok-button "Сохранить"
                       :width 120
                       :action
                       (if id
                         (execute-form* edit-user[name phone email login password password_old login roles]
                                        (do
                                          (user:update-from-admin *id {:role (maybe-seq roles)} name phone password password_old login)
                                          (sec/update-logged-information-for-user *id))
                                        :success (js*
                                                   (closeDialog)
                                                   (organizerOrdersReload)
                                                   (notify "Готово" "Пользователь успешно сохранен"))))))
    [:div.padding10
     [:p.margin5b "ФИО"]
     [:div.margin10b
      (text-field {:class "input input-text"} :name name)]

     [:div.group
      [:div.left.margin20r
       [:p.margin5b "Логин"]
       [:div.fixed-width-433px.margin10b
        (text-field {:class "input input-text"} :login (:login (first auth-profile)))]]
      [:div.left
       [:p.margin5b "Пароль"]

       [:div
        (text-field {:class "input input-text" :type "hidden"} :password_old (:password (first auth-profile)))]

       [:div.fixed-width-433px.margin10b
        (text-field {:class "input input-text"} :password (:password (first auth-profile)))]]]

     [:div.group
      [:div.left.margin20r
       [:p.margin5b "Телефон"]
       [:div.fixed-width-433px.margin10b
        (text-field {:class "input input-text"} :phone (get-in contact [:phone :value]))]]
      [:div.left
       [:p.margin5b "Почта"]
       [:div.fixed-width-433px.margin10b
        (text-field {:class "input input-text"} :email (get-in contact [:email :value]))]]]

     [:p.margin5b "Адрес"]
     [:div.margin10b
      (text-field {:class "input input-text"} :full-address full-address)]]))



(defn order-list-template[{:keys [user user-order-item id status user-type additional-info changed sum diff] :as order}]
  [:tr
   [:td (custom-checkbox {:onclick (js (if (.is ($ this) ":checked")
                                         (.push selectedOrders (clj id))
                                         (_.pull selectedOrders (clj id))))}
                         :selected)]
   [:td [:p id]]
   [:td (if (:id user)
          [:p (if (seq (:note additional-info))
                {:data-title
                 (tooltip
                  [:div {:style "width:350px; text-align:center; margin:0 5px 5px 5px;"}
                   (:note additional-info)])})
           (or (:real-name user) (get-in user [:contact :email :value])) "[" (:id user) "]"]
          (str additional-info))]
   [:td [:p {:data-title (tooltip [:div.admin-order-additional-info-tooltip
                                   [:div.table [:p "Сумма заказа: "][:span (:original-sum order)" грн."]]
                                   [:div.table [:p "Скидка: "][:span (:discount order)" грн."]]
                                   [:div.table [:p "Сумма со скидкой: "][:span (:sum-with-discount order)" грн."]]
                                   [:div.table [:p "Банковские услуги: "][:span (:payment-expenses order)" грн."]]
                                   [:div.table [:p "Итоговая сумма: "][:span sum " грн."]]])} sum]]
   [:td (format-date changed)]
   [:td (let [booked-timers (map tm/from-date (remove nil? (map :booked-until user-order-item)))]
          (if (seq booked-timers)
            (format-date-interval (reduce (fn[dt-a dt-b] (if (before? dt-a dt-b) dt-a dt-b)) booked-timers))
            "Все заказы оплачены"))]
   [:td (! :retail-order.status status)]
   [:td [:div.centered-cell-with-margin {:onclick (modal-window-open)}
         (if (empty? (map (fn[[type vals]] (count vals)) diff))
           [:p "Нет"]
           (list
            [:a.link "Есть"]
            (modal-window-admin
             "Изменения заказа"
             (modal-save-panel
              (list
               [:div.right.margin10l
                (small-ok-button "Принять" :action (cljs (remote* retail-update-order-last-viewed
                                                                  (do (retail:update-last-viewed (s* id)) nil)
                                                                  :success (fn[]
                                                                             (closeDialog)
                                                                             (organizerOrdersReload))))
                                 :width 120)]
               [:div.right.small-danger-button-plain {:style "width:120px;"}
                [:a {:target "_blank" :href (str "/service/orders/print/diff/" id)}
                 [:div.enabled.center.text-center.plain-button.orange-button-plain.ok-button-plain "Печать"]]]))
             (order-diff-modal-content order))))]]
   [:td {:style "padding:5px 10px;"}

    [:div.left
     (small-ok-button
      [:i.glyphicon.glyphicon-sm.glyphicon-pencil {:style "margin:0 -7px;"}]
      :width 32
      :action (modal-window-open))
     (admin-stock-edit-modal order)]
    [:div.left
     (small-success-button
      [:i.glyphicon.glyphicon-sm.glyphicon-plus {:style "margin:0 -7px;"}]
      :disabled (not ((set order-statuses-allowed-to-modify) status))
      :width 32
      :action (js (extendOrderUsingWizard (clj (:id user)) (clj (get-in user [:contact :email :value])))))]
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    [:div.left
     (small-info-button
       [:i.glyphicon.glyphicon-sm.glyphicon-user {:style "margin:0 -7px;"}]
       :width 32
       :action (modal-window-open))
    (order-user-modal user)]
    ]])

(defn order-filters[]
  (list
   [:div.group
    [:div.margin10r.left
     (small-success-button (list [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Заказ")
                           :width 130
                           :action (create-panel :order))]

    [:div.blue-dropdown.margin10r.left {:style "width:145px;"}
     (custom-dropdown
      {:onchange (js (organizerOrdersLoadPage 0))}
      :status (cons ["Статус заказа" "null"]
                    (map (fn[st]
                           [(! :retail-order.status st) st])
                         [:created :prepared-to-work :in-work :payed-cash :payed-clearing :closed])))]

    [:div.margin10r.left.none
     (javascript-tag (js (var selectedOrders [])))
     (small-info-button "Объединить"
                        :action (cljs (remote* retail-merge-selected-orders
                                               (retail:merge-orders (map parse-int (c* selectedOrders)))
                                               :success (fn[](organizerOrdersLoadPage 0)))))]]))

(defpage "/retail/admin/orders/" {}
  (common-admin-layout
      (order-filters)
    [:div.retail-standard-panel.admin-panel {:style "padding:5px 0 1px 0;"}
     [:div.admin-stock-list
      [:div
       (search-callback (js* (organizerOrdersLoadPage 0)))

       [:div.alert.alert-danger.margin15
        [:span.table-cell "Сумма незакрытых заказов: "]
        [:span.table-cell.padding5l
         (region* retail-orders-overall-sum[]
           (when *in-remote*
             (retail:active-orders-sum :status [:created :prepared-to-work :in-work :payed-cash :payed-clearing])))
         (document-ready (js* (.on ($ document) "pager.change" (fn[] (clj (rerender :retail-orders-overall-sum))))))]
        [:span.table-cell.padding5l " грн."]]

       (pager supplier-orders
              (modify-result
               (retail:active-orders
                page-size (c* offset)
                :status (keyword-converter (c* (get-val ".controls-block [name=status]")))
                :sort (keyword (c* (get-val "[name=sort]")))
                :search-string (let [search (c* (get-val "[name=search]"))]
                                 (and (seq search) {:flags [:w] :str search})))
               (fn[res] (concat [:header] res)))
              (fn[entity]
                (cond (= entity :header)
                      (precompile-html
                       [:tr.admin-table-head
                        [:td.fixed-50px-width-cell "Выбрать"]
                        [:td.fixed-75px-width-cell "№"]
                        [:td.full-width-cell "Покупатель"]
                        [:td.fixed-100px-width-cell "Сумма"]
                        [:td.fixed-150px-width-cell "Дата"]
                        [:td.fixed-150px-width-cell "Счетчик"]
                        [:td.fixed-75px-width-cell "Статус"]
                        [:td.fixed-75px-width-cell "Изменения"]
                        [:td.fixed-125px-width-cell "&nbsp;"]])

                      true
                      (order-list-template entity)))
              :register-as organizer-orders
              :page 40
              :columns 1
              :custom-rows-mode true
              :pager-title "Заказы")]]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Orders printing

(defn order-print[{:keys [user-order-item id status user-type additional-info user
                           additional-expense sum original-sum
                           discount payment-expenses sum-with-discount contexts]
                    :as order}]
  [:div.form.margin5
   [:div.bono-logo.left0px]
   [:div.margin5b (admin-order-user-info user)]
   [:div.margin5b (admin-order-items-info user-order-item)]
   [:div.margin5b (admin-order-items-sum order)]
   [:div.margin5b (admin-order-additional-expenses order discount sum-with-discount payment-expenses sum)]])

(defpage "/service/orders/print/:id" {id :id}
  (html
   (include-css "/css/admin/print.css")
   (order-print (retail:get-order (parse-int id)))))

(defpage "/service/orders/print/diff/:id" {id :id}
  (html
   (include-css "/css/admin/print.css")
   (order-diff-modal-content
    (retail:get-order (parse-int id)))))
