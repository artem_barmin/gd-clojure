(ns gd.views.admin.group-stock-model
  (:use
   clojure.test
   clojure.data
   gd.views.components
   gd.views.admin.stock-model
   gd.views.admin.arrival-model
   gd.utils.test
   com.reasonr.scriptjure
   gd.utils.web
   gd.model.model
   gd.model.context
   gd.utils.common
   [clojure.algo.generic.functor :only (fmap)]
   hiccup.core
   hiccup.element
   hiccup.form
   hiccup.page
   clojure.data.json
   clojure.walk
   gd.utils.db)
  (:require
   [clojure.string :as strings]
   [noir.session :as session]
   [monger.collection :as mc]
   [monger.conversion :as mcon]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Prepare group of stocks for JSON

(defn- prepare-stock-variant-for-view
  "Leave only meaningfull parameters of variants. Used for computing intersections and diffs."
  [{:keys [stock-variant]}]
  (fmap (partial map (fn[var] (select-keys var [:price :context]))) stock-variant))

(defn- map-intersection[maps]
  (reduce (fn[int map] (last (diff int map))) maps))

(defn prepare-group-stocks
  "Finds intersection in parameters, variants between all stocks"
  [ids]
  (let [stocks (inplace:get-stocks-with-changes ids)
        intersection (fn[props-fn]
                       (let [res (map-intersection (map props-fn stocks))]
                         (if (sequential? res)
                           (remove nil? res)
                           res)))
        intersection-primitive (fn[props-fn]
                                 (let [[val :as vals] (distinct (map props-fn stocks))]
                                   (if (= 1 (count vals)) val nil)))
        empty-variant? (fn[{:keys [contexts]}] (some (comp not :price) (vals contexts)))
        sale? (every? true? (map is-sale? stocks))]

    (merge (new-stock-default)
           {:description {:measurements (intersection (comp :measurements :description))
                          :other (intersection-primitive (comp :other :description))}
            :fkey_category (intersection-primitive :fkey_category)
            :name (intersection-primitive :name)
            ;; we don't have variants, but some params should be marked as invisible
            :variantParams (intersection (comp (partial map first)
                                               (partial filter second)
                                               keys
                                               :params-with-visibility))
            :variant (->>
                      stocks
                      (map prepare-stock-variant-for-view)
                      map-intersection
                      (prepare-stock-variant {}) ;TODO : compute intersection of
                                        ;variants that forbidden to be
                                        ;deleted
                      (remove empty-variant?))
            :context {:wholesale {:status true}
                      :wholesale-sale {:status sale?}
                      :retail {:status true}
                      :retail-sale {:status sale?}}
            :sale sale?
            :params (or
                     (mreduce (fn[key] {key (vec (intersection (comp set (fn[m](get m key)) :all-params)))})
                              (intersection (comp set keys :all-params)))
                     {})})))

(defn prepare-group-arrival
  "Finds combination of all parameters, variants between all stocks"
  [ids]
  (let [stocks (inplace:get-stocks-with-changes ids)

        combination (fn[props-fn]
                      (apply clojure.set/union (concat (map props-fn stocks))))

        combination-primitive (fn[props-fn]
                                (let
                                  [[val :as vals] (map props-fn stocks)
                                   name-set (clojure.string/join "," vals)]
                                  name-set))]

    {:name (combination-primitive :name)
     :stock-variant (or
                      (combination (comp set keys :stock-variant))
                      {})}))

(defn group-of-stocks[client-state]
  (map (fn[id] (if (re-find #"^[0-9]+$" id) (parse-int id) id)) client-state))

(defn- new-state-for-group-update
  "Performs merging of new state and old stock state. "
  [{:keys [id description params-with-visibility] :as old-stock}
   {:keys [price context stock-variant fkey_category params name] :as new-stock}]
  (let [;; copy visibility
        new-context
        (mreduce (fn[[type info]] {type (merge info {:status (:status (type context))})}) (:context old-stock))

        new-stock-variant
        (cond (seq stock-variant) stock-variant
              true (:stock-variant old-stock))

        new-params
        (mreduce (fn[name]
                   {name (or (seq (get params name)) (seq (get params-with-visibility name)) [])})
                 (distinct-by first (mapcat keys [params params-with-visibility])))

        ;; transform measurements to be sure that they represented as map(in some cases size might be 0,1,2)
        new-measurements
        (let [measurements (:measurements (:description new-stock))]
          (cond
            (sequential? measurements) (reduce merge (map-indexed (fn[i val] {i val}) measurements))
            (map? measurements) measurements
            (nil? measurements) {}
            true (throwf "Measurements is not a map type!")))

        new-properties
        (let [properties (seq (:properties (:description new-stock)))]
          (cond
           (sequential? properties) properties
           (empty? properties) (:properties (:description old-stock))))

        description
        (deep-merge-take-last
         (clojure.walk/keywordize-keys description)
         (clojure.walk/keywordize-keys
          (merge (mreduce (fn[[k v]] (when v {k v})) (:description new-stock))
                 {:measurements new-measurements}))
         {:properties new-properties})]
    (merge
     (when (and name (not= name (:name old-stock)))
       {:name name})
     (when (and fkey_category (not= fkey_category (:fkey_category old-stock)))
       {:fkey_category fkey_category})
     {:params new-params
      :context new-context
      :stock-variant new-stock-variant
      :description description})))

(defn- group-stock-update
  "Group update of stocks. Performns partial update of stock:

- brand, sizes and measurements
- processed tag
- category
- name
"
  [ids state]
  (let [existing-stocks (group-by-single :id (inplace:get-stocks-with-changes ids))
        new-stock (prepare-stock-params state)]
    (doseq [external-id ids]
      (inplace:merge-changes
       (merge
        (new-state-for-group-update (get existing-stocks external-id) new-stock)
        {:id external-id})
       :already-prepared true))))

(defn group-stock-save-button[]
  (small-ok-button "Сохранить"
                   :action
                   (cljs (remote* retail-stock-group-editing
                                  (let [ids (group-of-stocks (c* selectedItems))]
                                    (group-stock-update ids (c* (getCurrentGroupStock)))
                                    (if (every? string? ids) "new" "changed"))
                                  :success (fn[tab]
                                             (deselectAll)
                                             (refreshTabs tab)
                                             (closeDialog))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Reder changes that will be made

(defn- map-with-values-diff[old-map new-map]
  (mreduce (fn[param]
             (let [old (set (get old-map param))
                   new (set (get new-map param))
                   [removed added] (diff old new)]
               (when (or (seq removed) (seq added))
                 {param {:removed removed
                         :added added}})))
           (set (concat (keys old-map) (keys new-map)))))

(defn- description-diff[{measurements-old :measurements :as old}
                        {measurements-new :measurements :as new}]
  (let [[old new] (map string-converter [old new])
        old (dissoc old :measurements)
        new (dissoc new :measurements)
        [changed] (diff old new)
        measurements (map-with-values-diff measurements-old measurements-new)]
    (merge (when (seq measurements) {:measurements measurements})
           (mreduce (fn[[property]]
                      {property {:old (get old property)
                                 :new (get new property)}})
                    changed))))

(defn- visibility-diff[context-old context-new]
  (let [normalize (partial fmap :status)
        [context-old context-new] (map normalize [context-old context-new])
        [changed] (diff context-old context-new)]
    (mreduce (fn[[type]]
               {type {:old (type context-old)
                      :new (type context-new)}})
             changed)))

(defn- stock-diff[old-stock new-stock]
  (let [normalize (fn[v] (select-keys v [:name :price :fkey_category]))
        [changed-properties] (apply diff (map normalize [new-stock old-stock]))
        params-diff (map-with-values-diff (:params old-stock) (:params new-stock))
        status-diff (visibility-diff (:context old-stock) (:context new-stock))
        desc-diff (description-diff (:description old-stock) (:description new-stock))
        variant-diff (map-with-values-diff
                       (prepare-stock-variant-for-view old-stock)
                       (prepare-stock-variant-for-view new-stock))]
    (merge
     (mreduce (fn[[property]]
                {property {:old (get old-stock property)
                           :new (get new-stock property)}})
              changed-properties)
     (when (seq status-diff) {:context status-diff})
     (when (seq desc-diff) {:description desc-diff})
     (when (seq variant-diff) {:stock-variant variant-diff})
     (when (seq params-diff) {:param params-diff}))))

(defn group-stock-changes [ids state]
  (let [existing-stocks (group-by-single :id (inplace:get-stocks-with-changes ids))
        new-stock (prepare-stock-params state)
        changes (map (fn[external-id]
                       (let [{:keys [params-with-visibility] :as old} (get existing-stocks external-id)
                             new (new-state-for-group-update old new-stock)]
                         (merge (stock-diff (assoc old :params params-with-visibility) new)
                                {:id external-id})))
                     ids)]
    changes))

(defn group-aggregated-changes[changes]
  (apply deep-merge-with (fn[& all] (distinct (flatten all))) changes))
