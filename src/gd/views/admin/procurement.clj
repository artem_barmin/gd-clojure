(ns gd.views.admin.procurement
  (:use gd.model.model
        clojure.algo.generic.functor
        gd.views.admin.arrival-model
        gd.views.admin.components
        gd.views.admin.arrival-list
        gd.views.admin.dictionary
        gd.views.admin.arrival
        gd.views.admin.common
        gd.views.admin.stocks
        gd.model.context
        gd.views.messages
        hiccup.def
        clojure.data.json
        clojure.test
        gd.views.components
        gd.views.admin.stock-model
        gd.utils.web
        gd.utils.db
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [clojure.set :as set])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defmethod arrival-filter-on-save :procurement [arrival params]
  (when (seq params)
    (let [stocks (stock:get-multiple-stock (keys params))]
      (reduce merge
              (map (fn[[stock-id param-for-stock]]
                     {stock-id
                      (cond
                        (= :removed param-for-stock)
                        []

                        (seq param-for-stock)
                        (map (fn[{:keys [quantity] :as param}]
                               (assoc param :quantity (or quantity 0)))
                             param-for-stock)

                        true
                        (let [sizes (get-in (find-by-val :id stock-id stocks) [:params :size])]
                          (map (fn[[size]] {:price 0 :quantity 0 :parameters {:size size}}) sizes)))})
                   params)))))

(defmethod inplace-status-param-functions :need-procurement [limit offset params]
  (retail:admin-supplier-stocks
   limit offset
   (merge
    params
    {:limited-ids (procurement:need-procurement-stock-ids (current-arrival))
     :force-limited-ids true
     :state [:visible :not-prepared :invisible]})))

(defmethod inplace-status-param-functions :all-same-supplier [limit offset params]
  (retail:admin-supplier-stocks
   limit offset
   (merge
    params
    {:limited-ids (procurement:stocks-from-same-supplier (current-arrival))
     :force-limited-ids true
     :state [:visible :not-prepared :invisible]})))

(defn procurement:count-need-procurement[]
  (count (procurement:need-procurement-stock-ids (current-arrival))))

(defn input-change-callback[id & {:keys [success]
                                  :or {success (js* (clj (multi-rerender :arrival-stock-editing id))
                                                    (refreshTabs))}}]
  (cljs (remote*
         save-procurement-params-for-stock
         (arrival:save-quantity-to-session
          (s* id)
          (process-arrival-from-params (c* (formState (.closest ($ this) ".params-container")))))
         :success (fn[] (clj success)))))

(defn procurement-stock-template[{:keys [name images meta-info price description group-deal
                                         stock-parameter-value id category additional-stock-window status
                                         album price-change fkey_category state new-sizes stock-arrival-parameter
                                         all-params params left-stocks context]
                                  :as stock}]
  (let [stored-arrival (get stock-arrival-parameter (current-arrival))]
    [:tr
     [:td
      (sized-image {:style "margin:5px 0 1px -5px;"
                    :class "organizer-stock-list-image"
                    :data-title (tooltip (image (images:get :stock-large (first images))))}
                   (images:get :stock-medium (first images)))]
     [:td name]
     [:td.params-container
      (let [last-arrival (get-last-arrival stock-arrival-parameter)]
        (map (fn[value]
               (let [params-key
                     {:size value}

                     requested-stocks
                     (or (get left-stocks params-key) 0)

                     last-arrived-parameters
                     (find-by-val :parameters params-key (get stock-arrival-parameter last-arrival))

                     last-arrival-info-tooltip
                     (let [arrival-params
                           (get stock-arrival-parameter last-arrival)

                           {:keys [stock-arrival]}
                           (find-first (fn[{:keys [stock-arrival parameters]}]
                                         (and stock-arrival (= parameters params-key)))
                                       arrival-params)

                           {:keys [quantity price]}
                           last-arrived-parameters]
                       (tooltip
                        [:div {:style "margin:0 5px 5px;"}
                         [:div "Цена (bono.in.ua): "
                          (:price (find-by-val :context :wholesale (get stock-variant params-key)))
                          " грн."]
                         (if stock-arrival
                           [:div "Последний приход: " (format-date (stock-arrival :created))]
                           [:div "В последний приход не завозили"])
                         (when (and quantity stock-arrival)
                           [:div "Кол-во в последнем приходе: " quantity " шт."])
                         (when (and price stock-arrival)
                           [:div "Закупочная цена в последнем приходе: " price " грн."])]))]
                 (let [arrival-params
                       (arrival:get-quantity-from-session id)

                       procurement-params
                       (get stock-arrival-parameter (current-arrival))

                       {:keys [price quantity]}
                       (or (find-by-val :parameters params-key arrival-params)
                           (find-by-val :parameters params-key procurement-params))]
                   [:div.left {:data-title last-arrival-info-tooltip
                               :style "height:auto; margin:2px;"}
                    (hidden-field :size value)
                    [:div.group {:style "width:95px;"}
                     [:div.stock-procurement-tag-group.left
                      [:span.stock-tag.admin-stock-size value]
                      [:span.stock-tag.admin-stock-quantity requested-stocks " шт."]
                      [:span.admin-stock-price-input
                       (text-field {:onchange (input-change-callback id)} :price
                                   (or price (:price last-arrived-parameters)))]]
                     [:div.left
                      [:span.admin-stock-quantity-input
                       (spinner :quantity
                                :width 35
                                :min 0
                                :value (or quantity 0)
                                :onchange (input-change-callback id))]]]])))
             (:size params)))]
     [:td (get-brand all-params)]
     [:td (draw-category (:id category))]
     [:td
      (if (or (arrival:marked? id) stored-arrival)
        (list [:a.link {:onclick (arrival-remove-stock id)}
               "Удалить"])
        [:a.link {:onclick (arrival-add-stock id)}
         "Добавить"])]]))

(defn procurement-stock-list[]
  [:div.admin-stock-list.procurement-list
   (pager supplier-stocks-procurement
          (modify-result
           (supplier-stocks-with-filters false page-size)
           (fn[res] (concat [:header] res)))
          (fn[entity]
            (cond
              (= entity :header)
              [:tr.admin-table-head
               [:td.fixed-50px-width-cell "Фото"]
               [:td.fixed-150px-width-cell "Название"]
               [:td.full-width-cell "Параметры"]
               [:td.fixed-100px-width-cell "Бренд"]
               [:td.fixed-200px-width-cell "Категория"]
               [:td.fixed-75px-width-cell "Операции"]]

              true
              (let [{:keys [id] :as stock} entity]
                (inplace-multi-region
                 [procurement-stock-editing arrival-stock-editing] id []
                 (let [stock (region-request procurement-stock-editing
                                             (inplace:get-stock-with-changes (s* id))
                                             stock)]
                   (procurement-stock-template stock))))))
          :page 40
          :columns 1
          :custom-rows-mode true
          :pager-title "Товары к закупке"
          :register-as organizer-stock-pages)])

(defpage "/retail/admin/procurements/" {}
  (arrival-list :procurement))

(defpage "/retail/admin/procurement/:id/" {id :id}
  (common-admin-layout
      (list
       (add-var-to-global-context :arrival id)
       (arrival-stock-creation))
    [:div.admin.group.admin-panel.retail-standard-panel
     (arrival-edit-panel)
     (arrival-general-info-panel)
     (inplace-statistics-panel-with-tabs
       :procurement-tab
       {:name "Все товары" :value "all-same-supplier" :count #(count (procurement:stocks-from-same-supplier (current-arrival)))}
       {:name "Нужно купить" :value "need-procurement" :count #(procurement:count-need-procurement)}
       {:name "Закупка" :value "arrived" :count #(arrival:count-arrived)})

     (inplace-change-callbacks)
     (procurement-stock-list)]))
