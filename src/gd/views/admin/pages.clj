(ns gd.views.admin.pages
  (:use gd.views.resources
        gd.views.admin.common
        [clojure.algo.generic.functor :only (fmap)]
        gd.views.bucket.simple
        compojure.core
        gd.model.model
        gd.model.context
        gd.utils.web
        gd.utils.common
        gd.views.messages
        gd.views.components
        noir.core
        [noir.options :only (dev-mode?)]
        [noir.response :only (redirect status set-headers)]
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defn- admin-page-template [entity type parent-url id]
  [:tr
   [:td (! :site-pages (:status entity))]
   [:td [:p.cutted {:style "width:200px;"} (:url entity)]]
   [:td [:p.cutted {:style "width:200px;"} (:title entity)]]
   [:td [:p.cutted {:style "width:325px;"}
         (if (or (= type :news) (= type :blogs))
           (:content-short entity)
           (when-let [content (:content entity)]
             (render-html content)))]]
   [:td {:style "padding:5px;"}
    (small-ok-button [:i.glyphicon.glyphicon-sm.glyphicon-pencil {:style "margin:0 -7px;"}]
                     :width 32
                     :action (js (set! location.pathname (clj (str "/" parent-url "/edit/" (name type) "/" (:id entity))))))]


   (if (or (= type :news) (= type :blogs))
     [:td {:style "padding:5px;"}

      (small-danger-button [:i.glyphicon.glyphicon-sm.glyphicon-remove {:style "margin:0 -7px;"}]
                     :width 32
                     :action #_(cljs (remote* retail-admin-del-news (site-page:delete (s* id))
                                            :success (. location reload)))
                           (js (if (confirm (str "Точно удалить новость?"))
                                 (clj (remote* retail-admin-delete-stock
                                               (let [id (s* id)]
                                                   (site-page:delete id))
                                               :success (. location reload))))))])])

(defn pages-list [type add-link & {:keys [parent-url hide-addition custom-addition]
                                   :or {parent-url "admin"}}]
  [:div.admin.group {:style "margin-top:5px"}
   [:div.group.admin-panel.retail-standard-panel
    [:div.admin-stock-list
     (pager supplier-pages
            (modify-result
             (site-pages:get-pages-list page-size (c* offset)  {:type type})
             (fn[res](concat [:header] res)))
            (fn[entity]
              (cond (= entity :header)
                    (precompile-html
                     [:tr.admin-table-head
                      [:td.fixed-75px-width-cell "Статус"]
                      [:td.fixed-200px-width-cell "Адрес"]
                      [:td.fixed-200px-width-cell "Заголовок"]
                      [:td.full-width-cell "Описание"]
                      [:td.fixed-50px-width-cell]
                      [:td.fixed-50px-width-cell]])

                    true
                    (admin-page-template entity type parent-url (:id entity))))
            :columns 1
            :page 30
            :custom-rows-mode true
            :pager-title (case type
                           :page "Список страниц"
                           :news "Список новостей"
                           :blogs "Список блогов"
                           :reviews "Список отзывов"
                           :category-page "Страницы категорий"))]]])

(defpage "/retail/admin/page/" {}
  (common-admin-layout
      [:a.small-success-button-plain.inline-block {:href "/admin/edit/page/new"}
       [:div.admin-action-panel.admin-adding-panel
        [:div [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Страница"]]]
    (pages-list :page "/admin/edit/page/new")))

(defpage "/retail/admin/news/" {}
  (common-admin-layout
      [:a.small-success-button-plain.inline-block {:href "/admin/edit/news/new"}
       [:div.admin-action-panel.admin-adding-panel
        [:div [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Новость"]]]
    (pages-list :news "/admin/edit/news/new")))



(defpage "/retail/admin/blogs/" {}
  (common-admin-layout
      [:a.small-success-button-plain.inline-block {:href "/admin/edit/blogs/new"}
       [:div.admin-action-panel.admin-adding-panel
        [:div [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Блог"]]]
    (pages-list :blogs "/admin/edit/blogs/new")))




(def wysiwyg-images-upload-lock (Object.))

(defn- images-management[]
  (let [preview-size [100 100]
        prepare-single-image (fn[code]
                               {:small (images:get preview-size code)
                                :original (images:get nil code)
                                :code code})]

    (list

     (javascript-tag
      (js (var saveImages
               (fn[]
                 (clj (remote* save-retail-admin-wysiwyg-images
                               (locking wysiwyg-images-upload-lock
                                 (do (retail:save-supplier-images (map :code (c* (getCurrentImages))))
                                     nil))))))))

     [:div.left.margin10b.margin10l
      (multi-file-upload
       (gen-client-id)
       (form-for-file-json* upload-retail-admin-wysiwyg-image [nil preview-size]
                            (prepare-single-image code)
                            :sizes-to-wait [nil preview-size])
       :width 120
       :text (list [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Фото")
       :success (js* addImage))]

     [:div {:style "margin-left:125px; clear:both;"
            :ng-controller "PhotosCtl"
            :ng-init (js (set! images (clj (map prepare-single-image (retail:supplier-images)))))}
      [:div.table.relative {:ng-show "images.length"}
       [:div.admin-upload-holder.left {:ng-repeat "image in images track by $index"}
        [:img.admin-upload-img {:ng-src "{{image.small}}" :ng-click (js (insertImage image))}]
        (close-button (js (removeImage $index)))]]])))

(defn add-page [id page-type & {:keys [new-initial-state parent-url]
                                :or {parent-url "admin"}}]
  (let [{:keys [title status url content content-short seo published-by-mail contexts]}
        (if (= id "new") new-initial-state (site-pages:get-page-for-id (parse-int id)))]
    (require-js :lib :tinymce.jquery)
    (require-js :admin :page)
    (require-js :custom-lib :wysiwyg)
    (require-js :custom-lib :bbcode)
    [:div.group.margin15t
     (document-ready (js* (enableTinyMCE "[name=content]")))
     [:div.page_form_container
      [:div.page_form_title "Название:"]
      (text-area {:class "input input-textarea" :autocomplete "off"} :title title)]

     (if (or (= page-type :news) (= page-type :blogs))
       [:div.page_form_container
        [:div.page_form_title "Краткое описание:"]
        (text-area {:class "input input-textarea" :autocomplete "off"} :content-short content-short)]
       [:div.page_form_container
        [:div.page_form_title "Cсылка:"]
        (text-area (merge {:class "input input-textarea" :autocomplete "off"}
                          (when-not (= id "new") {:readonly "true"}))
                   :url url)])

     [:div
      (with-group :seo
        (map!
         (fn[type]
           [:div.page_form_container
            [:div.page_form_title (name type) ":"]
            (text-area {:class "input input-textarea" :autocomplete "off"} type (type seo))])
         [:title :keywords :description :custom-name]))]

     (if (= (count *context-types*) 1)

       [:div.none (custom-checkbox :contexts true (first *context-types*))]
       [:div.page_form_container.group
        (if (= page-type :blogs)
        {:style "display:none"})
        [:div.page_form_title "Публиковать:"]
        [:div.margin5t.left
         (custom-checkbox :contexts (:wholesale contexts) :wholesale)]
        [:div.left {:style "margin:7px 10px 0 5px;"} "Оптовый"]
        [:div.margin5t.left
         (custom-checkbox :contexts (:retail contexts) :retail)]
        [:div.left {:style "margin:7px 10px 0 5px;"} "Розничный"]])

     [:div
      [:div.page_form_title.left "Статус:"]
      [:div.red-dropdown.left {:style "width:150px;"}
       (custom-dropdown {:autocomplete "off" :style "height:1px;width:1px;"} :status
                        (map (fn[s][(! :site-pages s) s]) [:published :not-published])
                        (or status :pubslished))]

      (images-management)]

     [:div.page_form_container
      [:div {:style "margin-left:82px; margin-top:7px; position:absolute;"} "Текст:"]
      [:div.page_editor_container
       (text-area :content content)
       (text-area {:class "none"} :content-real)]]]))

(defn control-buttons [id page-type & {:keys [new-initial-state parent-url]
                                       :or {parent-url "admin"}}]
  (let [{:keys [title status url content content-short seo published-by-mail contexts]}
        (if (= id "new") new-initial-state (site-pages:get-page-for-id (parse-int id)))
        back-url (if (= page-type :category-page)
                   "/seo/categories-editor/"
                   (str "/" parent-url "/" (name page-type) "/"))

        save-action
        (fn[success]
          (js
           (.triggerSave tinyMCE false true)
           (.text ($ "[name=content-real]")
                  (convertHTMLtoBBCode (.getContent (.get tinymce "content"))))
           (clj (execute-form* apply-site-pages[content-real]
                               (let [params-map
                                     (-> param-map
                                         (update-in [:status] keyword)
                                         (update-in [:contexts] maybe-seq)
                                         (assoc :content (process-text content-real))
                                         (assoc :type *page-type))]
                                 (if (= *id "new")
                                   (site-pages:create-page params-map)
                                   (site-pages:update-page *id params-map))
                                 nil)
                               :success success
                               :keep-data true
                               :call-type :call*))))]
    [:div.table
     (when (not= id "new")
       [:div.table-cell.padding10r
        (small-warning-button "Применить"
                           :action (save-action (reload))
                           :width 120)])
     [:div.table-cell.padding10r
      (small-ok-button "Сохранить"
                       :action (save-action (js* (set! location.pathname (clj back-url))))
                       :width 120)]
     [:div.table-cell.padding10r
      (small-cancel-button "Отмена"
                           :href back-url
                           :width 120)]

     (when (and (sec/admin?) (= :news page-type) (not= id "new") (not published-by-mail))
       (list
        [:div.table-cell.padding10r
         (small-danger-button "Разослать письмом"
                              :action (js (if (confirm "Точно разослать?")
                                            (clj (remote* send-new-as-email-to-all-users
                                                          (news:publish-new (s* (parse-int id)))))))
                              :width 170)]
        [:div.table-cell.padding10r
         (small-info-button "Тестовая рассылка"
                               :action (modal-window-open)
                               :width 170)
         (modal-window-admin
          "Тестовая рассылка - будет проходить на один адрес"
          (modal-save-panel
           (small-ok-button "Отправить"
                            :action (execute-form* send-new-as-email-to-single-users[email]
                                                   (news:test-publish-new (parse-int *id) email))
                            :width 120))
          [:div.form.padding10
           [:p.margin5b "E-mail для тестового письма:"]
           [:div.margin20b
            (text-field {:class "input input-text"} :email)]])]))]))

(defpage "/retail/admin/edit/page/:id"  {id :id}
  (common-admin-layout
      (control-buttons id :page)
    (add-page id :page)))

(defpage "/retail/admin/edit/news/:id"  {id :id}
  (common-admin-layout
      (control-buttons id :news)
    (add-page id :news)))

(defpage "/retail/admin/edit/blogs/:id"  {id :id}
  (common-admin-layout
      (control-buttons id :blogs)
    (add-page id :blogs)))

(defpage "/retail/seo/css/" data
         (seo-admin-layout nil
             [:div.block {:ng-controller "seoCssCtrl" :style "height:100%"}
              [:div {:id "editor"} "Для начала работы выберите файл."]
              [:div.retail-standard-panel {:style "background-color:black;z-index:999;opacity:0.5;width:230px; height:130px;position:fixed;right:25px;bottom:100px;border-radius:10px;"}
               [:span.margin10l.white "Файл:"]
               [:select.form-control {:style "width:210px;margin:10px" :ng-change "fileChanged(currentFile)" :ng-model "currentFile"}
                [:option {:ng-repeat "file in files" :value "{{file.reference}}"} "{{file.filename}}"]]
               [:button.btn.btn-default {:ng-click "getCss()" :style "margin: 10px; width:100px" :title "Отменить все несохранённые изменения"} "Отменить"]
               [:button.btn.btn-default.padding5l  {:ng-click "saveCss()" :style "width:100px" :title "Сохранить все изменения"} "Сохранить"]]
              [:script {:src "//cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/ace.js" :type "text/javascript" :charset "utf-8"}]
              [:script " var editor = ace.edit('editor'); editor.setTheme('ace/theme/chrome'); editor.getSession().setMode('ace/mode/css');"]]))

(defpage "/retail/seo/redirect/" data
         (seo-admin-layout nil
             [:div.block {:ng-controller "redirectsCtrl"}
              [:div.margin5l.margin5t {:ng-click "updateRedirects()" :style "width:200px"} (small-success-button "Сохранить" :ng-click "asdasdasd")]
              [:div {:style "width:1000px"}
                [:div.margin5l.margin5t.left {:ng-repeat "redirect in redirects"}
                 [:div.inline-block {:ng-click "removeRedirect($index)"} (small-danger-button [:i.glyphicon.glyphicon-sm.glyphicon-remove-sign.inline-block] :width "32px")]
                 [:input.form-control.margin5l {:ng-disabled "redirect.enabled" :title "Адрес отправления" :type "text" :style "width:400px;display:inline-block" :ng-model "redirect.from"}]
                 [:input.form-control.margin5l {:ng-disabled "redirect.enabled" :title "Адрес назначения":type "text" :style "width:400px;display:inline-block" :ng-model "redirect.to"}]
                 [:div.margin5l.inline-block {:title "Включить/отключить перенаправление"} (custom-checkbox {:ng-model "redirect.enabled"})]
                 [:span.btn {:title "Проверить адрес назначения" :ng-click "validateURL(redirect)"} [:i.fa.fa-refresh]]
                 [:i.fa.fa-check.margin5l {:ng-show "redirect.status" :style "color:green" :title "Адрес назначения действителен"}]
                 [:i.fa.fa-times.margin5l {:ng-hide "redirect.status":style "color:red" :title "Адрес назначения не действителен"}]]
                [:div {:style "clear:both"}]
                [:div.margin5l.margin5t.left {:style "width:32px" :ng-click "addRedirect()"} (small-success-button [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign.inline-block])]]]))