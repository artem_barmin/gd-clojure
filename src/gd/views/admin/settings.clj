(ns gd.views.admin.settings
  (:use gd.views.resources
        gd.log
        [gd.parsers.common :only (req-post *custom-headers* *cookies*)]
        gd.views.admin.common
        gd.views.admin.components
        [clj-time.core :exclude (extend)]
        clj-time.format
        [clojure.algo.generic.functor :only (fmap)]
        gd.views.bucket.simple
        compojure.core
        gd.model.model
        gd.model.context
        gd.utils.web
        gd.utils.common
        gd.views.messages
        gd.views.components
        noir.core
        [noir.options :only (dev-mode?)]
        [noir.response :only (redirect status set-headers)]
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [noir.session :as session]
            [noir.cookies :as cookies]))

;; в config.php HTTP_ADMIN должен стоять в retail.com.ua(в admin папке тоже)

(defn current-php-session[]
  (or (cookies/get "PHPSESSID")
      (let [id (str (java.util.UUID/randomUUID))]
        (cookies/put! "PHPSESSID" id)
        id)))
;TODO req-post to custom subdomain
(defn get-remote-token[php-session]
  (binding [*custom-headers* {"Cookie" (str "PHPSESSID=" php-session)}
            *cookies* (clj-http.cookies/cookie-store)]
    (let [request-host ((:headers noir.request/*request*) "host")]
      (->>
       (get-in
        (req-post (str "http://" request-host "/admin/index.php?route=common/login")
                  {:username "admin"
                   :password "admin"})
        [:headers "location"])
       (re-find #"token=(.*)")
       second))))

(def token-key :opencart-admin-token)

(defn get-token[]
  (or (when-let [{:keys [token created]} (session/get token-key)]
        (when-not (before? (clj-time.coerce/to-date-time created) (-> 1 minutes ago))
          token))
      (let [token (get-remote-token (current-php-session))]
        (info "Get remote token" token (current-php-session))
        (session/put! token-key {:token token :created (now)})
        token)))

(def opencart-pages
  {"Дополнения" "extension/module"
   "Статьи" "catalog/information"
   "Оплата" "extension/payment"
   "Настройки магазина" "setting/setting"
   "Журнал ошибок" "tool/error_log"
   "Макеты" "design/layout"
   "Банеры" "design/banner"
   "Валюты" "localisation/currency"})

(defn build-url[route]
  (let [request-host ((:headers noir.request/*request*) "host")]
    (str "http://" request-host "/admin/index.php?route=" route "&token=" (get-token))))

(defpage "/retail/admin/settings/" {}
  (require-js :admin :reviews)
  (require-js :admin :settings)
  (common-admin-layout
   [:div.admin-toggle-block.group
    [:div.admin-filter-panel
     (map (fn[[name route]]
            [:div.left.margin5l
             (small-ok-button
              name
              :width 150
              :action (js (.attr ($ "#main-frame") "src"
                                 (clj (build-url route)))))])
          opencart-pages)]]
   [:div.clearfix.padding15
    [:iframe#main-frame
     {:frameBorder "0"
      :src (build-url (second (first opencart-pages)))
      :onload (js (window.scrollTo 0 0))
      :style "width:100%;height:4000px;"
      :scrolling "no"}]]))
