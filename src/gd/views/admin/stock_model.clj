(ns gd.views.admin.stock-model
  (:use
   clojure.test
   gd.utils.test
   gd.utils.web
   gd.model.model
   gd.model.context
   gd.utils.common
   [clojure.algo.generic.functor :only (fmap)]
   hiccup.core
   hiccup.element
   hiccup.form
   hiccup.page
   clojure.data.json
   clojure.walk
   gd.utils.db)
  (:require
   [clojure.string :as strings]
   [noir.session :as session]
   [monger.collection :as mc]
   [monger.conversion :as mcon]))

(comment "Model for sessions of inplace editing. They store current changed
stocks and user selection. Assumes work in multi-context environement(single
:status and :price are not processed)")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Session management

(defn- get-current-session[]
  (:inplace-edit @session/*noir-session*))

(defn- assoc-in-current-session[key val]
  (session/swap! assoc-in (concat [:inplace-edit] key) val)
  nil)

(defn- update-in-current-session[key fun]
  (session/swap! update-in (concat [:inplace-edit] key) fun)
  nil)

(defn- dissoc-in-current-session[key]
  (session/swap! dissoc-in (concat [:inplace-edit] key))
  nil)

(defn- reset-current-session[]
  (session/swap! assoc-in [:inplace-edit] nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Common transformation utils

(defn- select-minimal-price-variant[variants context]
  (let [filter-by-context
        (fn[variants]
          (if context
            (filter (comp (partial = context) :context) variants)
            variants))

        prices
        (mapcat (fn[[params variants]]
                  (->>
                   variants
                   maybe-seq
                   filter-by-context
                   (map :price)
                   (remove nil?)
                   (remove zero?)))
                variants)]
    (if (seq prices) (reduce min prices) 0)))

(defn- process-stock-variants[{:keys [variant] :as stock}]
  (assoc stock :stock-variant
         (mreduce (fn[{:keys [params contexts]}]
                    (when (seq (remove empty? (map :price (vals contexts))))
                      {params
                       (map (fn[[context price]]
                              (-> price
                                  (merge {:context context})
                                  (update-in [:price] int-converter)
                                  (select-keys [:price :context])))
                            contexts)}))
                  variant)))

(defn- process-params
  "Append to each param visibility marker. If parameter is used in stock
variants - it should be marked as visible and take part in the order creation
process. Empty lists of parameters are not added to result.

variantParams - used for group operations, where we don't have stock-variants, but should not change
visibility of parameter"
  [{:keys [params stock-variant variantParams] :as stock}]
  (let [stock-variant (mreduce (fn[[k v]] (when k {k v})) stock-variant)
        params-in-variants (set (map name (concat (mapcat (comp keys first) stock-variant)
                                                  (map keyword variantParams))))]
    (assoc stock :params
           (mreduce (fn[[name values]]
                      (when-let [values (seq (distinct (remove empty? values)))]
                        (let [name (clojure.core/name name)]
                          {[name (boolean (params-in-variants name))] values})))
                    params))))

(defn- process-single-status[status]
  (case status
    ("visible" "true") :visible
    ("invisible" "false") :invisible
    :invisible))

(defn- process-contexts[{:keys [stock-variant context] :as stock}]
  (assoc stock
    :price (select-minimal-price-variant stock-variant nil)
    :context (mreduce (fn[type]
                        {type {:status (process-single-status (:status (type context)))
                               :price (select-minimal-price-variant stock-variant type)}})
                      *context-types*)
    :status :visible))

(defn- process-images-to-code[images]
  (map :code images))

(defn- process-accounting[{:keys [accounting] :as stock}]
  (let [accounting-dict (:accounting (dictionary:get-dictionary))]
    (assoc stock :accounting (keyword (or accounting accounting-dict :warehouse)))))

(defn- process-properties [param-map]
  (assoc-in param-map [:description :properties]
            (into []
                  (-> #(and (< (.length (:value %)) 1) (and (< (.length (:name %)) 1)))
                      (remove (:properties (:description param-map)))
                      doall))))

(defn- process-measurements [param-map]
  (let [measurements (:measurements (:description param-map))
        valid-measurements (mreduce (fn [key]
                                      {key (vec (filter #(and (and (not= "null" (:name %)) (or (number? (:name %))  (seq (:name %))))
                                                              (and (not= "null" (:value %)) (or (number? (:value %)) (seq (:value %)))))
                                                        (measurements key)))})
                                    (keys measurements))]

    (if measurements
      (assoc-in param-map [:description :measurements] valid-measurements)
      param-map)))

(defn prepare-stock-params
  "Main stock saving transformer. Transform params from web view into model view."
  [param-map]
  (->
   (postwalk string-converter param-map)
   process-stock-variants
   process-contexts
   process-params
   process-accounting
   (update-in [:tags] (comp set keys))
   (update-if-contains :images process-images-to-code)
   (update-if-contains :fkey_category int-converter)
   process-properties
   process-measurements))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stock info merge changes

(defn inplace:merge-changes
  "Accepts raw stock state"
  [stock-state & {:keys [already-prepared]}]
  (let [{:keys [id params stock-variant] :as processed-stock}
        ((if already-prepared identity prepare-stock-params) stock-state)]
    (update-in-current-session
     [:changed (str id)]
     (fn[already-updated]
       (->
        (deep-merge-take-last
         already-updated
         processed-stock
         {:state :dirty})
        (merge {:params params
                :stock-variant stock-variant
                :params-with-visibility params}))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Store information API

(defn inplace:new-stock?[id]
  (and (string? id) (.startsWith id "id")))

(defn inplace:store-additional-state[type value]
  (assoc-in-current-session [:additional type] value))

(defn inplace:store-new-stock[code & [overwrite]]
  (let [other-category-id (:id (category:get-by-key "other"))
        id (gen-client-id)]
    (assoc-in-current-session
     [:new id]
     (merge
      {:images [code]
       :state :new
       :context (mreduce (fn[type] {type {:price 0 :status :not-prepared}}) *context-types*)
       :fkey_category other-category-id
       :id id}
      overwrite))))

(defn inplace:store-new-stock-with-params[stock]
  (let [id (gen-client-id)]
    (assoc-in-current-session
     [:new id]
     (merge
      {:id id
       :params-with-visibility (:params stock)}
      stock))))

(defn inplace:remove[id]
  (dissoc-in-current-session [:new id])
  (dissoc-in-current-session [:changed id]))

(declare inplace:get-stock-with-changes)

(defn inplace:copy-stock-to-new[source-id image]
  (let [stock (inplace:get-stock-with-changes source-id)]
    (inplace:store-new-stock
     image
     (select-keys stock [:name :fkey_category :stock-parameter-value
                         :params :all-params :params-with-visibility :price]))))

(defn inplace:reject-all[]
  (reset-current-session))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Examine current state of storage

;;;;;;;;;;;;;;;;;;;;
;; Stock merging methods

(defn- stock-parameter-values-from-map[params-map]
  (mapcat
   (fn[[param-name values]]
     (let [[param-name param-enabled] (if (sequential? param-name) param-name [param-name])]
       (map
        (fn[value]
          {:name (name param-name)
           :value value
           :visible param-enabled
           :enabled true})
        values)))
   params-map))

(defn- retrieve-changes
  "Fill passed stock with changed properties from session. If changed state for
:new stock - then remove mark :dirty."
  [stock]
  (let [changed (get-in (get-current-session) [:changed (str (:id stock))])
        {:keys [params-with-visibility] :as new} (deep-merge-take-last stock changed)]
    (-> new
        (assoc :stock-parameter-value (stock-parameter-values-from-map params-with-visibility))
        (assoc :stock-variant (some :stock-variant [changed stock])) ;stock-variant should be overwriten
        (assoc :bypass-cache (boolean changed))
        stock:parameters-for-choosing)))

(defn- new-stocks
  "Simple list function for :new session stocks"
  [limit offset]
  (if *count-query*
    [{:count (count (:new (get-current-session)))}]
    (let [res (->> (vals (:new (get-current-session)))
                   (drop (or offset 0)))]
      (if limit (take limit res) res))))

(defn inplace:changed-stock-ids[]
  (->>
   (clojure.set/difference
    (set (keys (:changed (get-current-session))))
    (set (keys (:new (get-current-session)))))
   (filter (fn[id] (re-find #"^[0-9]+$" id)))
   (map parse-int)))

(defmulti inplace-status-param-functions
  "Dispatch on used inline-state"
  (fn[_ _ {:keys [inline-state]}] (keyword inline-state)))

(defmethod inplace-status-param-functions :default [limit offset params]
  (retail:admin-supplier-stocks limit offset params))

(defmethod inplace-status-param-functions :changed [limit offset params]
  (retail:admin-supplier-stocks
   limit offset
   (merge params
          {:limited-ids (inplace:changed-stock-ids)
           :force-limited-ids true})))

(defmethod inplace-status-param-functions :new [limit offset params]
  (new-stocks limit offset))

(defn inplace:all-stocks
  "Execute query based on inline-state and annotate stock with changes"
  [limit offset {:keys [inline-state only-id] :as params}]
  (let [res (inplace-status-param-functions limit offset params)]
    (if (or *count-query* only-id) res (map retrieve-changes res))))

(defn inplace:count-new[]
  (count (:new (get-current-session))))

(defn- get-new[id]
  (get-in (get-current-session) [:new (str id)]))

(defn inplace:get-stocks-with-changes[ids]
  (map retrieve-changes
       (concat (stock:get-multiple-stock (filter number? ids))
               (map get-new (filter string? ids)))))

(defn inplace:get-stock-with-changes[id]
  (retrieve-changes (or (get-new id) (retail:get-admin-stock (parse-int id)))))

(defn inplace:empty?[]
  (empty? (dissoc (get-current-session) :additional)))

(defn inplace:count-changes[]
  (count (inplace:changed-stock-ids)))

(defn inplace:get-additional-state[type]
  (get-in (get-current-session) [:additional (name type)]))

(defn inplace:maybe-add-new-stocks
  "Append new stocks to existing result set."
  [limit offset result original-count]
  (let [right-limit (+ limit offset)
        new-offset (let [offset (- offset original-count)]
                     (if (> offset 0) offset 0))
        new-limit (let [new-limit (- right-limit original-count)]
                    (if (> new-limit limit) limit new-limit))]
    (if (>= right-limit original-count)
      (concat result (new-stocks new-limit new-offset))
      result)))

(defn inplace:merge-stocks[ids]
  (cond
    (every? inplace:new-stock? ids)
    (let [[fst & rest] ids
          other-images (mapcat (comp :images inplace:get-stock-with-changes) rest)]

      ;; remove other stocks
      (doseq [id rest]
        (session/swap! dissoc-in [:inplace-edit :new id])
        (session/swap! dissoc-in [:inplace-edit :changed id]))

      ;; concat images
      (session/swap! update-in [:inplace-edit :new fst :images] concat other-images))

    (not-any? inplace:new-stock? ids)
    (stock:merge-stocks (map parse-int ids))

    true
    (throwf "Can't merge new and old stocks")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Final result storage API
;; TODO : maybe we should run this in separate thread with indicator + push notification
(defn inplace:save-all[]
  ;; store updates
  (doseq [id (inplace:changed-stock-ids)]
    (let [{:keys [state id params-with-visibility] :as stock}
          (inplace:get-stock-with-changes (str id))]
      (when (= state :dirty)
        (stock:update-stock
         (parse-int id)
         (-> stock
             (select-keys [:id :context :description :fkey_category :tags :name :stock-variant :accounting])
             (assoc :params params-with-visibility))))))

  ;; store new stocks, status is set to :visible by default, ignoring real
  ;; settings(also for contexts)
  (doseq [{:keys [id] :as stock} (vals (:new (get-current-session)))]
    (let [{:keys [params-with-visibility price context accounting] :as stock}
          (inplace:get-stock-with-changes id)]
      (stock:save-stock-general
       (->
        stock
        (select-keys [:images :description :fkey_category :tags :name :stock-variant :accounting])
        (assoc :params params-with-visibility)
        (assoc :status :visible)
        (assoc :price (or price 0))
        (assoc :context (mreduce (fn[[type ctx]]
                                   {type (assoc ctx
                                           :status
                                           (case type
                                             (:retail-sale :wholesale-sale) :invisible
                                             :visible))})
                                 context))))))

  (reset-current-session))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Prepare stock info for JSON

(def params-default {})

(defn new-stock-default[]
  {:images []
   :fkey_category (:id (category:get-by-key "other"))
   :context {:retail {:status true}
             "retail-sale" {:status false}
             :wholesale {:status true}
             "wholesale-sale" {:status true}}
   :description {}
   :variant []
   :params params-default})

(defn prepare-single-image[image]
  {:small (images:get :deal-small image)
   :big (images:get :deal-huge image)
   :code image})

(defn- prepare-statuses
  "Remove delayed statuses, and transform :visible to true and :invisible to false"
  [stock]
  (clojure.walk/postwalk
   (fn[a]
     (cond (delay? a) nil
           (and (map? a) (contains? a :status)) (update-in a [:status] (partial = :visible))
           true a))
   stock))

(defn prepare-stock-variant[left-stocks stock-variant]
  (let [compute-percent (fn[retail wholesale]
                          (when (and retail wholesale (not (zero? wholesale)))
                            (Math/round (* 100.0 (/ (- retail wholesale) wholesale)))))]
    (map (fn[[params variants]]
           (let [{:keys [wholesale retail] :as variants} (group-by-single :context (remove nil? variants))
                 [wholesale retail] (map :price [wholesale retail])]
             {:params (or params {})
              :left (or (get left-stocks params) 0)
              :contexts (assoc-in variants [:retail :percent] (compute-percent retail wholesale))}))
         stock-variant)))

(defn is-sale?[{:keys [context]}]
  (boolean (some (fn[type](= :visible (get-in context [type :status]))) [:retail-sale :wholesale-sale])))

(defn prepare-single-stock[id]
  (let [{:keys [context stock-variant all-params images left-stocks description] :as stock}
        (inplace:get-stock-with-changes id)]
    (postwalk
     string-converter
     (->
      stock
      (select-keys [:category :fkey_category :context :id :description :name :accounting])
      (assoc :sale (is-sale? stock)
             :variant (prepare-stock-variant left-stocks stock-variant)
             :params (or all-params params-default)
             :images (map prepare-single-image images))
      prepare-statuses))))
