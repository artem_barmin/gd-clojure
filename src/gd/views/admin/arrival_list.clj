(ns gd.views.admin.arrival-list
  (:use gd.model.model
        gd.views.admin.components
        gd.views.admin.dictionary
        [clojure.algo.generic.functor :only (fmap)]
        gd.views.admin.common
        gd.views.admin.stocks
        gd.model.context
        gd.views.messages
        hiccup.def
        clojure.data.json
        clojure.test
        gd.views.components
        gd.views.admin.stock-model
        gd.utils.web
        gd.utils.db
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [clojure.set :as set])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Arrival list management

(defn- create-arrival-window[type]
  (modal-window-admin
   (case type
     :arrival "Создание нового прихода"
     :procurement "Создание новой закупки"
     :inventory "Создание нового переучета")
   (modal-save-panel (small-ok-button
                      "Готово"
                      :width 120
                      :action (execute-form* create-stock-arrival[arrival-name supplier]
                                             ((case *type
                                                :inventory inventory:create-inventory
                                                arrival:create-new-stock-arrival)
                                              arrival-name supplier *type)
                                             :success (js*
                                                       (set! (.. location href)
                                                             (str "/admin/" (clj (name type)) "/" result "/"))))))

   [:div.form {:style "padding:15px;"}
    [:table
     [:tr.fixed-height-35px
      [:td.text-right.padding10r {:style "min-width:70px; padding:15px 10px;"} "Название"]
      [:td {:style "width:100%"} (text-field {:class "input input-text"} :arrival-name)]]
     (when-not (= type :inventory)
       [:tr.fixed-height-35px
        [:td.text-right.padding10r "Поставщик"]
        [:td.blue-dropdown (custom-dropdown :supplier (get-param-values :supplier))]])]]))

(defn arrival-list[type]
  (common-admin-layout
      [:div.small-success-button-plain {:style "width:130px;" :onclick (modal-window-open)}
       (case type
         :arrival (list [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Приход")
         :procurement (list [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Закупка")
         :inventory (list [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Переучет"))
       (create-arrival-window type)]
    [:div.admin.group
     [:div.group.admin-panel.retail-standard-panel
      [:div.admin-stock-list
       (pager supplier-stock-all-arrivals
              (modify-result
               (arrival:all-arrivals page-size (c* offset) :type (s* type))
               (fn[res](concat [:header] res)))
              (fn[{:keys [type] :as entity}]
                (cond (= entity :header)
                      (precompile-html
                       [:tr.admin-table-head
                        [:td.full-width-cell "Название"]
                        [:td.fixed-150px-width-cell "Поставщик"]
                        [:td.fixed-150px-width-cell "Автор"]
                        [:td.fixed-150px-width-cell "Дата"]
                        [:td.fixed-150px-width-cell "Кол-во товаров"]
                        [:td.fixed-150px-width-cell]])

                      true
                      (let [{:keys [id created parameters stocks sum
                                    not-filled-stocks status type user]} entity]
                        [:tr
                         [:td
                          [:p.text-left (:name parameters)
                           (when (sec/partner?)
                             [:span (case type
                                      :arrival (str " (cумма прихода: " sum " грн.)")
                                      :procurement (str " (cумма закупки: " sum " грн.)")
                                      :inventory (if (= :closed status) " - уже закрыт" " - еще открыт"))])]]
                         [:td
                          (if (= type :inventory)
                            "—"
                            [:p.text-left (:supplier parameters)])]
                         [:td
                          [:p (or (:real-name user) "—")]]
                         [:td
                          [:p (format-date created)]]
                         [:td
                          [:p.text-left stocks (smart-text stocks " товар" " товара" " товаров")
                           (when (sec/partner?)
                             (when (and not-filled-stocks (> not-filled-stocks 0) (= type :arrival))
                               [:span.red " (" not-filled-stocks ")"]))]]
                         [:td {:style "padding:5px;"}
                          (when-not (or (> stocks 0) (= :inventory type))
                            [:div.right.margin5l
                             (small-danger-button [:i.glyphicon.glyphicon-sm.glyphicon-remove {:style "margin:0 -7px;"}]
                                                  :width 34
                                                  :action (cljs
                                                           (remote* remove-empty-arrival
                                                                    (do (arrival:remove-arrival-by-id (s* id)) nil)
                                                                    :success (fn[] (supplierStockAllArrivalsLoadPage 0)))))])
                          (if (= :closed status)
                            [:div.right.margin5l
                             (small-info-button [:i.glyphicon.glyphicon-sm.glyphicon-eye-open {:style "margin:0 -7px;"}]
                                                :width 34
                                                :action (js (set! window.location (clj (str "/admin/" (name type) "/" id "/")))))]
                            [:div.right.margin5l
                             (small-ok-button [:i.glyphicon.glyphicon-sm.glyphicon-pencil {:style "margin:0 -7px;"}]
                                              :width 34
                                              :action (js (set! window.location (clj (str "/admin/" (name type) "/" id "/")))))])]])))
              :columns 1
              :custom-rows-mode true
              :pager-title
              (case type
                :arrival "Список приходов"
                :procurement "Список закупок"
                :inventory "Список переучетов"))]]]))

(defpage "/retail/admin/arrivals/" {}
  (arrival-list :arrival))
