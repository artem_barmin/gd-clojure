(ns gd.views.admin.users
  (:use gd.model.model
        gd.model.context
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        clojure.data.json
        gd.views.components
        gd.views.admin.stock-model
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        [clj-time.core :exclude [extend]]
        hiccup.element)
  (:require [clj-time.coerce :as tm])
  (:require [clojure.string :as strings])
  (:require [gd.utils.security :as internal-sec])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defn roles-management[{:keys [id role]} & {:keys [roles] :or {roles [:organizer :moderator :user]}}]
  [:div.form
   (managed-list role
                 (fn[role]
                   (let [role (keyword role)]
                     [:div.left {:class (when (and role (not ((set roles) role))) "none")}
                      [:div.orangedropdown {:style "width:149px;"}
                       (custom-dropdown :roles
                                        (map (fn[role] [(! :user.role role) role])
                                             (remove nil? (cons role roles)))
                                        role)]])))])

(defn- user-modal [{:keys [id name full-address contact auth-profile] :as user}]
  (modal-window-admin
   (if id (str "Редактирование пользователя №" id) "Создание нового пользователя")
   (modal-save-panel
    (small-ok-button "Сохранить"
                     :width 120
                     :action
                     (if id
                       (execute-form* edit-user[name phone email login password password_old login roles]
                                      (do
                                        (user:update-from-admin *id {:role (maybe-seq roles)} name phone password password_old login)
                                        (sec/update-logged-information-for-user *id))
                                      :success (js*
                                                (closeDialog)
                                                (retailUsersReload)
                                                (notify "Готово" "Пользователь успешно сохранен"))))))
   [:div.padding10
    [:p.margin5b "ФИО"]
    [:div.margin10b
     (text-field {:class "input input-text"} :name name)]

    [:div.group
     [:div.left.margin20r
      [:p.margin5b "Логин"]
      [:div.fixed-width-433px.margin10b
       (text-field {:class "input input-text"} :login (:login (first auth-profile)))]]
     [:div.left
      [:p.margin5b "Пароль"]

      [:div
       (text-field {:class "input input-text" :type "hidden"} :password_old (:password (first auth-profile)))]

      [:div.fixed-width-433px.margin10b
       (text-field {:class "input input-text"} :password (:password (first auth-profile)))]]]

    [:div.group
     [:div.left.margin20r
      [:p.margin5b "Телефон"]
      [:div.fixed-width-433px.margin10b
       (text-field {:class "input input-text"} :phone (get-in contact [:phone :value]))]]
     [:div.left
      [:p.margin5b "Почта"]
      [:div.fixed-width-433px.margin10b
       (text-field {:class "input input-text"} :email (get-in contact [:email :value]))]]]

    [:p.margin5b "Адрес"]
    [:div.margin10b
     (text-field {:class "input input-text"} :full-address full-address)]

    (when (or (sec/partner?) (sec/root?))
      (list
       [:p.margin5b "Роль"]
       (roles-management user :roles
                         (cond
                           (sec/root?) [:user :manager :partner :seo :admin :copywriter]
                           (sec/partner?) [:user :manager :seo :copywriter]))))]))


(defn- user-template[{:keys [name full-address contact id] :as user}]
  [:tr
   [:td [:a.link {:onclick (modal-window-open)} (str name " [" id "]")]
    (user-modal user)]
   [:td [:p.table-cell "500"]]
   [:td [:p "500 грн."]]
   [:td
    [:p (get-in contact [:email :value])]
    [:p (get-in contact [:phone :value])]]
   [:td
    [:div.group
     [:div.left.margin15r
      [:a.link {:onclick (modal-window-open)} "e-mail"]
      (modal-window-admin
       "Отправить email"
       (modal-save-panel (small-ok-button
                          "Отправить"
                          :width 120
                          :action (execute-form* retail-user-send-email[header text]
                                                 (user:send-mail *id header text))))
       [:div.form.padding10
        [:p.margin5b "Заголовок сообщения"]
        [:div.margin10b
         (text-field {:class "input input-text"} :header)]
        [:p.margin5b "Текст сообщения"]
        [:div.admin-user-sms-textarea.fixed-height-100px
         (text-area {:class "input input-textarea"}:text)]])]

     [:div.left.margin15r
      [:a.link {:onclick (modal-window-open) } "SMS"]
      (modal-window-admin
       "Отправить sms"
       (modal-save-panel (small-ok-button
                          "Отправить"
                          :width 120
                          :disabled (empty? (get-in contact [:phone :value]))
                          :action (execute-form* retail-user-send-sms[text]
                                                 (user:send-sms *id text))))
       [:div.form.padding10
        [:p.margin5b "Текст сообщения"]
        [:div.admin-user-sms-textarea.fixed-height-100px
         (text-area {:class "input input-textarea"}:text)]])]

     [:div.left
       [:a.link {:onclick
                        (js (if (confirm "Вы уверены что хотите удалить пользователя?")
                                   (clj (remote* retail-delete-user-all
                                              (user:delete-user-all (s* id))
                                                 :success (. location reload)
                                                 ))))
                 :style "color:red" } "X"]
      ]]

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    [:div
     [:a.link {:onclick (js (if (confirm "Вы уверены что хотите убрать из рассылки пользователя?")
                              (clj (remote* retail-delete-from-natification
                                            (user:delete-from-natification (s* id) )
                                            ))))}
      "Убрать из рассылки"]]
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    [:div
     [:a.link {:onclick (js (if (confirm "Вы уверены что хотите войти как пользователь?")
                              (clj (remote* retail-login-as-user
                                            (user:login-as-user (s* id))
                                            :success (fn[] (.open window "/index.php?route=account/account" "_blank"))))))}
      "Войти как пользователь"]]



    [:div.left.margin15r
     [:a.link {:onclick (modal-window-open)} "Передать заказы"]
     (modal-window-admin
       "Передать заказы"
       (modal-save-panel (small-ok-button
                           "Отправить"
                           :width 120
                           :action (execute-form* retail-order-user-change[idnew]
                                                  (retail:change-order-user *id idnew)

                           :success (js*
                                      (closeDialog)
                                      (notify "Готово" "Заказы закреплены за новым пользователем")))))

       [:div.form.padding10
        [:p.margin5b "Номер пользователя"]
        [:div.fixed-width-433px.margin10b
         (text-field {:class "input input-text" :placeholder "Введите номер пользователя, которому передать заказы"} :idnew )]]

       )]]])
(defn users-page[query]
  (common-admin-layout
    [:div
    [:div {:style "width:130px;display: inline-block"}
       (small-success-button (list [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Клиент")
                        :width 130
                        :action (modal-window-open))
       (user-modal nil)]
;;---------------SEND SMS TO ANY NUMBER------------------------------------------------------------------
      [:div {:style "width:160px;display: inline-block; float:right;"}
       (small-info-button (list [:i.glyphicon.glyphicon-sm.glyphicon-envelope] " Отправтиь sms")
                             :width 160
                             :action (modal-window-open))
       (modal-window-admin
         "Отправить sms"
         (modal-save-panel (small-ok-button
                             "Отправить"
                             :width 120
                             :action (execute-form* retail-any-send-sms[phone text]
                                                    (user:send-any-sms phone text)
                                                    :success (js*
                                                               (closeDialog)
                                                               (notify "Готово" "смс отправлено")))))

         [:div.form.padding10
          [:p.margin5b "Текст сообщения"]
          [:div.admin-user-sms-textarea.fixed-height-100px
           (text-area {:class "input input-textarea"}:text)]
          [:p.margin5b "Номер телефона"]
           (text-field {:class "input input-text" :id "any-naumber" :placeholder "Введите номер телефона в международном формате"} :phone )])
       #_(user-modal nil)]
;;---------------------------------------------------------------------------------------------------------
]

    [:div.retail-standard-panel.admin-panel {:style "padding:5px 0 1px 0;"}
     [:div.admin-stock-list
      [:div.admin-search-panel.group
       (when (and (seq query) (not (re-find #"page[0-9]+" query)))
         (document-ready (js* (.change (.val ($ "[name=search]") (clj query))))))]
      (search-callback (js* (retailUsersLoadPage 0)))
      (pager retail-users
             (modify-result
              (user:get-all-users page-size (c* offset)
                                  :search-string (let [search (c* (get-val "[name=search]"))]
                                                   (and (seq search) {:flags [:w] :str search})))
              (fn[res] (concat [:header] res)))
             (fn[entity]
               (cond (= entity :header)
                     (precompile-html
                      [:tr.admin-table-head
                       [:td.full-width-cell "Покупатель"]
                       [:td.fixed-150px-width-cell "Заказы"]
                       [:td.fixed-150px-width-cell "Сумма заказов"]
                       [:td.fixed-150px-width-cell "Контакты"]
                       [:td.fixed-200px-width-cell "Операции"]])
                     true
                     (user-template entity)))
             :page 10
             :custom-rows-mode true
             :columns 1
             :pager-title "Список покупателей")]]))

(defpage "/retail/admin/users/:query" {query :query}
  (users-page query))

(defpage "/retail/admin/users/" {}
  (users-page nil))
