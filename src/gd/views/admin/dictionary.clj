(ns gd.views.admin.dictionary
  (:use gd.model.model
        [clojure.algo.generic.functor :only (fmap)]
        gd.model.context
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        clojure.data.json
        gd.views.components
        gd.views.admin.stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defn get-param-values[param]
  (distinct
   (concat
    [[(! :deal.profile.filter param) "null"]]
    (let [params (param (dictionary:get-dictionary))]
      (map name (if (map? params) (keys params) (maybe-seq params)))))))

(wrap-request-cache #'get-param-values)
(wrap-request-cache #'dictionary:get-dictionary)

(defn- client-managed-list[param & {:keys [controls new-item]}]
  [:div.managed-list-component
   [:ul.group.managed-list
    [:li.group.current-item {:ng-repeat (format "param in %s track by $index" param)}
     [:div.managed-list-delete-btn.left.margin5r
      (small-danger-button [:i.glyphicon.glyphicon-sm.glyphicon-remove-sign]
                           :ng-action (js (.splice (clj (symbol param)) $index 1))
                           :width 34)]
     (or
      controls
      [:div.left {:style "width:550px;"}
       (text-field {:class "input input-text" :autocomplete "off" :ng-model (format "%s[$index]" param)} :param)])]]
   [:div {:style "width:34px;"}
    (small-success-button [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign]
                          :ng-action (js
                                      (set! (clj (symbol param)) (or (clj (symbol param)) []))
                                      (.push (clj (symbol param)) (clj (or new-item ""))))
                          :width 34)]])

(defn- suppliers-management[]
  [:div.group.margin10b
   [:div.margin5b "Поставщики:"]
   (client-managed-list "dictionary.supplier")])

(defn- parameter-info-management[]
  [:div.group.margin10b

   [:div#paramDialog
    (modal-window-admin
     "Параметры"
     [:div
      (when (sec/admin?)
        (list
         [:div.margin5b.text-left "Служебное название (Внутрисистемный идентификатор):"]
         [:div.margin10b (text-field {:class "input input-text" :autocomplete "off" :ng-model "param.type"}
                                     :param)]))
      [:div.margin5b.text-left "Название (Отображение на админке):"]
      [:div.margin10b (text-field {:class "input input-text" :autocomplete "off" :ng-model "param.name"}
                                  :param)]
      [:div.margin5b.text-left "Фильтр (Отображение на фильтрах):"]
      [:div.margin20b (text-field {:class "input input-text" :autocomplete "off" :ng-model "param.filter"}
                                  :param)]])]

   [:div.margin5b "Параметры:"]
   (client-managed-list
    "dictionary.param"
    :controls [:div
               [:div.left {:style "width:510px;"}
                (text-field {:class "input input-text" :autocomplete "off" :ng-model "param.name"}
                            :param)]
               [:div.left.margin5l.margin5r
                (small-ok-button
                 [:i.glyphicon.glyphicon-sm.glyphicon-pencil {:style "margin:0 -7px;"}]
                 :ng-action (js (openParamDialog param)))]]
    :new-item {:new true})

   [:div.margin5b "Замеры:"]
   (client-managed-list "dictionary.measurements")
   [:div.margin5b "Описания:"]
   (client-managed-list "dictionary.properties")])

(defn- parameter-management[]
  [:div.group.margin10b {:ng-repeat "(paramName,conditions) in conditions"}
   [:div.margin5b [:b "{{paramNameFromDict(paramName)}}:"]]

   [:div.margin5b.panel.panel-default {:ng-repeat "condition in conditions"}
    [:div.margin5t.margin5b.margin15l {:ng-click (js (toggleVisibility (computeVisibilityHash paramName condition)))
                         :style "cursor:pointer;"}
     [:div {:ng-if "condition.condition.length"}
      "Условие: " [:span {:ng-repeat "cond in condition.condition"}
                   "{{paramNameFromDict(cond.name)}} - {{cond.value}}"]]
     [:div {:ng-if "!condition.condition.length"}
      "Без условия "
      [:span {:ng-if "condition.special=='all'"} "Все значения"]
      [:span {:ng-if "condition.special=='other'"} "Не ограниченные значения"]]]
    [:div.top-border.margin10t {:ng-if (js (aget visible (computeVisibilityHash paramName condition)))}
     (client-managed-list
      "condition.values"
      :controls [:div
                 [:div.left.margin5l.margin5r.margin2t
                  (custom-checkbox {:ng-click (js (selectParam paramName (aget condition.values $index) $event))}
                                   nil false nil)]
                 [:div.left {:style "width:500px;"}
                  [:div.relative
                   (text-field {:class "input input-text"
                                :autocomplete "off"
                                :ng-model "condition.values[$index].value"}
                               :param)
                   [:div.absolute {:style "color:gray;right:3px;top:5px;"
                                   :ng-if "condition.values[$index].old!=condition.values[$index].value"}
                    "Старое значение: {{condition.values[$index].old}}"]]]]
      :new-item {})]]])

(defpage "/retail/admin/dictionary/" {}
  (require-js :admin :dictionary)
  (common-admin-layout
      [:div.group {:style "text-align:left;"}
       [:div.left
        (small-ok-button
         "Сохранить"
         :width 140
         :action (cljs
                  (remote* save-supplier-dictionary
                           (do (dictionary:update-dictionary
                                (fmap string-converter (c* (getDictionary)))
                                (c* (getChangedParams)))
                               nil)
                           :success (fn[] (. location reload)))))]
       [:div.left.margin5l
        (small-success-button
         "Добавить условия"
         :width 150
         :action (js (.click ($ "#addConditions .plain-button"))))]]

    [:div#dictionary.admin-dictionary-form.group {:ng-controller "DictionaryCtl"}

     [:div
      (custom-checkbox {:ng-model "dictionary.accounting"
                        :ng-true-value "simple"
                        :ng-false-value "warehouse"}
                       :accounting)
      "Безостаточный учет по умолчанию"]

     (javascript-tag (js (set! existingParams (clj (map (fn[{:keys [value] :as param}]
                                                          (assoc param :old value))
                                                        (dictionary:all-params))))))

     [:div#addConditions.none
      (small-success-button
      "Добавить условия"
      :width 160
      :ng-action (js (openConditionsDialog)))]

     [:div#conditionsDialog
      (modal-window-admin
       "Условия для параметров"
       (modal-save-panel
        (small-ok-button
         "Сохранить"
         :ng-action (js (saveCondition))))
       [:div.padding15
        (client-managed-list
         "config.conditions"
         :controls [:div.left {:style "width:835px;"}
                    [:div
                     [:div.group.red-dropdown.margin5b
                      (custom-dropdown {:ng-model "param.type"}
                                       :param
                                       (concat [["Название параметра" "null"]
                                                ["{{param}}" "{{param}}"
                                                 {:ng-repeat "param in ['size','color','brand','collection']"}]]))]
                     [:div.group.red-dropdown.margin5b
                      (custom-dropdown {:ng-model "param.value"}
                                       :size
                                       (concat [["Значение параметра" "null"]
                                                ["{{val}}" "{{val}}"
                                                 {:ng-repeat "val in getAllValues(param.type)"}]]))]]]
         :new-item {})])]

     (suppliers-management)
     (parameter-info-management)
     (parameter-management)]))
