(ns gd.views.admin.visual-news
  (:refer-clojure :exclude [extend])
  (:use noir.core
        gd.views.admin.pages
        hiccup.page
        hiccup.element
        hiccup.form
        gd.utils.common
        gd.utils.web
        gd.utils.mail
        gd.views.admin.common
        net.cgrand.enlive-html
        hiccup.def
        gd.views.components
        gd.model.model
        hiccup.core
        com.reasonr.scriptjure
        noir.util.test
        [clojure.java.shell :only (sh)]
        clj-time.core)
  (:require [gd.views.security :as security]
            [clojure.java.io :as io]))

(defn- news-web-resource[news-id resource-id]
  (str "/img/uploaded/news/" news-id "/" resource-id ".jpg"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Image utils

(defn- crop-image[source-file target-file [w h x y] [target-w target-h]]
  (process-image-sync
   (delay source-file)
   (new java.io.File target-file)
   [target-w target-h]
   {:crop (str w "x" h "+" x "+" y)
    :force-overwrite true
    :quality 100}))

(defn- add-text-to-image[source text]
  (sh "mogrify"
      source
      "-strokewidth" "0"
      "-fill" "rgba( 0, 0, 0 , 0.4 )"
      "-draw" "rectangle 0,0 2000,60"
      "-fill" "rgba(255,207,33,0.8)"
      "-pointsize" "46"
      "-interword-spacing" "1"
      "-font" "Helvetica"
      "-gravity" "NorthEast"
      "-weight" "Bold"
      "-annotate" "+50+10" text
      "-pointsize" "18"
      "-annotate" "+15+10" "грн."
      "-pointsize" "30"
      source))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Image selection iframe

(defn- image-selection-infinite-list[]
  (infinite-scroll* image-selection-frame
                    (mapcat (fn[{:keys [id images]}]
                              (map (fn[im](merge {:id id :image im})) images))
                            (retail:supplier-stocks 100 (c* offset) {}))
                    (fn[{:keys [image id]}]
                      [:img {:src (images:get :stock-bucket image)
                             :onclick (js
                                       (var image (. ($ (.. parent document)) find ".modal-main-block:visible .croped-image"))
                                       (. image attr "data-stock" (clj id))
                                       (. image attr "src" (clj (images:get nil image))))}])
                    :columns 4
                    :limit 100))

(defpage image-selection-frame "/retail/admin/image-selection-frame/" {}
  (html
   [:head
    (include-js "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js")
    (include-js "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js")
    (include-js "/js/lib/tooltip.js")

    (include-js "/js/custom-lib/common.js")
    (include-js "/js/custom-lib/infinite-scroll-new.js")

    (include-css "/css/user/styles.css")
    (include-css "/utils/css/jquery-ui-1.8.23.custom.css")]
   [:body {:style "margin:0!important;"} (image-selection-infinite-list)]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Image selection modal

(defn- prepare-and-store-image
  "Resize, crop stock image + draw text on it"
  [target-image stock-id source [x y w h] [target-width target-height]]
  (let [source
        (str "resources/public" source)

        target
        (str "resources/public" target-image)

        price
        (get-in (stock:get-stock stock-id) [:context :wholesale :price])]

    ;; make directories
    (.mkdirs (.getParentFile (io/file target)))

    ;; process image
    (crop-image source target [w h x y] [target-width target-height])
    (add-text-to-image target (str price))

    ;; return new link
    (str target-image "?time=" (.getTime (new java.util.Date)))))

(defn- choose-news-image-action[target-image target-size cell-selector]
  (cljs (remote* choose-news-image-action
                 (let [{:keys [x y w h]}
                       (c* (. (. (in-dialog "img.croped-image") data "Jcrop") getRectangle))

                       src
                       (c* (. (in-dialog "img.croped-image") attr "src"))

                       dimensions
                       (map #(Math/round (Float/parseFloat %)) [x y w h])

                       stock-id
                       (parse-int (c* (. (in-dialog "img.croped-image") attr "data-stock")))]
                   (prepare-and-store-image (s* target-image) stock-id src dimensions (s* target-size)))
                 :success (fn[path]
                            (var cell ($ (clj cell-selector)))
                            (. cell show)
                            (. cell attr "src" (eval path))
                            (closeDialog)))))

(defn- image-selection-modal[target-image target-size cell-selector]
  (lazy-modal-window-with-header
    deal-news-photo-choose
    "Выберите фотографию" {:width 1160}
    (let [[width height] target-size]
      [:div.padding10
       [:div.group
        [:div.left
         [:iframe {:src "/admin/image-selection-frame/" :width 322 :height 495}]]
        [:div.left
         [:div.croped-element {:style "width:802px; padding:0 0 0 10px;"}
          [:img.croped-image.left
           {:style "max-width:500px;max-height:500px;"}]
          [:div.left {:style (str "width:" width "px;height:" height "px;overflow:hidden; margin-left:10px;")}
           [:img.croped-thumbnail {:data-crop-width width :data-crop-height height}]]]]]
       (blue-button "Выбрать фотографию"
                    :action (choose-news-image-action (s* target-image) (s* target-size) (s* cell-selector)))])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Email html template

(def ^:dynamic current-cell-number)

(defn- table-cell-with-image[news-id & {:keys [colspan rowspan height width] :or {height 295 width 225}}]
  (let [cell-id (swap! current-cell-number inc)
        cell-selector (str "image-cell-" cell-id)
        source-image (news-web-resource news-id cell-id)]
    [:td {:width width
          :height height
          :colspan colspan
          :rowspan rowspan
          :onclick (modal-window-open)
          :style "background-color:white;"}
     [:img {:style "float:left;"
            :class cell-selector
            :src (str source-image "?time=" (.getTime (new java.util.Date)))}]
     (image-selection-modal source-image [width height] (str "." cell-selector))]))

(defn mail-template[news-id]
  (binding [current-cell-number (atom 0)]
    (let [table-cell (partial table-cell-with-image news-id)
          {:keys [content url]} (site-pages:get-page-for-id news-id)]
      (doall
       [:table {:style "margin-left: auto; margin-right: auto; font-family: arial, sans-serif; background-color:#E8AE0F; color:white;"
                :width "950px"
                :cellpadding "0"
                :cellspacing "10px"}
        [:tbody
         [:tr.header
          [:td {:height "183px"
                :width "225px"
                :style "background:url('/img/bono/news/news_logo.png') no-repeat scroll 0 0 #82AF1E;"}]
          [:td {:colspan "3"
                :style "background:url('/img/bono/news/news_header_bg.png') no-repeat scroll 0 0 #82AF1E;"
                :onclick (modal-window-open true)}
           [:div.description-text {:style "padding:10px;"}
            [:a {:style "color:white;text-decoration:none;font-size:16px;"} content]]
           (modal-window-with-header "Заголовок новости" {:width 800}
             [:div.form.description-form
              (custom-text-area "description" :mode "private" :value content)
              ;; (blue-button "Сохранить"
              ;;              :width 200
              ;;              :action (execute-form* save-visual-news-description[description]
              ;;                                     (let [original-page (site-pages:get-page-for-id *news-id)
              ;;                                           description (process-text description)]
              ;;                                       (site-pages:update-page-by-id
              ;;                                        *news-id (merge original-page {:content description}))
              ;;                                       (bbcode-to-html description))
              ;;                                     :success (js* (. ($ "[name=description]") val result)
              ;;                                                   (. ($ ".description-text") html result)
              ;;                                                   (closeDialog))
              ;;                                     :keep-data true))
              ])]]

         [:tr.body_row1 {:style "vertical-align:top;"}
          (table-cell)
          (table-cell :colspan 2 :rowspan 2 :height 600 :width 461)
          (table-cell)]

         [:tr.body_row2 {:style "vertical-align:top;"}
          (table-cell)
          (table-cell)]

         [:tr.body_row3 {:style "vertical-align:top;" :height "300px;"}
          (table-cell :height 300)
          [:td {:style "background-image:url('/img/bono/news/news_info_bg.png'); background-repeat:no-repeat;"
                :width 226}]
          (table-cell :colspan 2 :width 460 :height 300)]

         [:tr.footer {:height "50px;"}
          [:td {:colspan "4" :style "background-color:#c16262;"}
           "<p style='text-align:center; font-size:12px;'>Вы получаете эту рассылку, поскольку указали свой адрес на сайте bono.in.ua и выразили согласие на получение информации о наших предложениях.
           <br/>Если Вы хотите отказаться от получения рассылки в будущем, нажмите <a style='font-weight:bold; color:white;' href=''>здесь</a>.</p> "]]]]))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Transform HTML to email form

(defn generate-html-email[news-id]
  (let [base-url "http://bono:8080"]
    (clojure.string/join
     (emit*
      ((transform-content
         [:div#dialog-modal] (substitute nil)
         [:div.dialog-content] (substitute nil)
         [#{:td :div}] (remove-attr :onclick :onmousedown :onmouseup)
         [:img] (fn[nd]
                  (update-in nd [:attrs :src] (fn[prop] (str base-url prop))))
         [:a] (fn[nd]
                (if (get-in nd [:attrs :href])
                  (update-in nd [:attrs :href] (fn[prop] (str base-url prop)))
                  (let [href (get-in nd [:attrs :stock-href])]
                    (assoc-in nd [:attrs :href] (str base-url href)))))
         [:td] (fn[nd]
                 (update-in nd [:attrs :style] (fn[prop] (.replaceAll prop "url\\('" (str "url('" base-url))))))
       (first (html-snippet (html (mail-template news-id)))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main entry point

(defn process-email[news-id address]
  (let [content (generate-html-email news-id)
        header (str "Новые поступления товаров : " (:name (site-pages:get-page-for-id news-id)))]
    (send-mail address header content)))

(defpage "/retail/admin/edit/visual-news/:id" {news-id :id}
  (let [news-id (parse-int news-id)]
    (common-admin-layout

      (require-js :lib :jquery.Jcrop)
      (require-js :custom-lib :crop)
      (require-css :lib :jquery.Jcrop)

      [:div.retail-standard-panel.center {:style "width:1023px;"}
       [:div.padding10.global-news-template
        (mail-template news-id)]
       [:div.group {:style "padding:5px 37px 20px;"}
        [:div.left
         (blue-button "Послать на адрес" :width 200 :action (modal-window-open))
         (modal-window-with-header "Выберите адрес назначения" {}
           [:div.form
            (text-field :address)
            (blue-button "Послать на адрес" :width 200
                         :action (execute-form* send-deal-news[address]
                                                (process-email *news-id address)
                                                :success (js* (closeDialog))))])]]])))

(defpage "/retail/admin/visual-news/" {}
  (common-admin-layout
    (pages-list
     :visual-news nil
     :custom-addition
     [:a.block {:onclick (modal-window-open)}
      [:div.admin-action-panel.admin-adding-panel
       [:div.normal-text.text-center "Добавить"]]
      (modal-window-with-header "Создание рассылки" {}
        [:div.form.padding10
         [:div.padding5b "Заголовок:"]
         [:div.padding5b (text-field {:style "width:475px;"} :name)]
         [:div
          ;; (blue-button "Создать"
          ;;             :action (execute-form* create-visual-news[name url]
          ;;                                    (site-pages:create-or-update-page
          ;;                                     url name nil nil :visual-news :not-published)
          ;;                                    :success (js*
          ;;                                              (set! (.. location href)
          ;;                                                    (str "/admin/edit/visual-news/" result)))))
          ]])])))
