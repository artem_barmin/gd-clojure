(ns gd.views.admin.arrival-model
  (:use gd.model.model
        gd.views.admin.stock-model
        [clojure.algo.generic.functor :only (fmap)]
        gd.views.components
        gd.utils.common
        gd.utils.web
        com.reasonr.scriptjure)
  (:require [clojure.string :as strings])
  (:require [clojure.set :as set])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils for in session storage

(defn current-arrival[]
  (when-let [arrival (get-var-from-global-context :arrival)]
    (parse-int arrival)))

(defn current-arrival-session[]
  (get-in @session/*noir-session* [:arrive-session (current-arrival)]))

(defn reset-current-arrival-session[]
  (session/swap! dissoc-in [:arrive-session])
  nil)

(defn- assoc-current-arrival-session[key val]
  (session/swap! assoc-in [:arrive-session (current-arrival) key] val)
  nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Arrival in-session model

(defn arrival:empty?[]
  (empty? (current-arrival-session)))

(defn arrival:mark-for-processing[id]
  (assoc-current-arrival-session id nil))

(defn arrival:remove-mark[id]
  (session/swap! dissoc-in [:arrive-session (current-arrival) id]))

(defn arrival:marked?[id]
  (contains? (current-arrival-session) id))

(defn arrival:removed?[id]
  (= :removed (get (current-arrival-session) id)))

(defn arrival:removed-stock-ids[]
  (remove nil? (map (fn[[id val]] (when (= :removed val) (parse-int id))) (current-arrival-session))))

(defn arrival:arrived-stock-ids[]
  (set/difference (set (map parse-int (keys (current-arrival-session))))
                  (set (arrival:removed-stock-ids))))

(defn arrival:count-arrived[]
  (arrival:count-in-database (current-arrival) (arrival:arrived-stock-ids) (arrival:removed-stock-ids)))

(defn arrival:save-quantity-to-session[stock-id params]
  (assoc-current-arrival-session stock-id params))

(defn get-stock-variant [id stocks]
  (:stock-variant (first (filter #(= id (:id %)) stocks))))

(defn contains-variant? [stock-variant single-param-map]
  (seq (filter #(= (:parameters single-param-map) %) (keys stock-variant))))

(defn arrival:save-multiple-arrivals[ids params]
  (let [stocks (stock:get-multiple-stock ids)]
    (doseq [id ids]
      (arrival:save-quantity-to-session id
        (filter #(contains-variant? (get-stock-variant id stocks) %) params)))))

(defn arrival:get-quantity-from-session[stock-id]
  (let [params (get (current-arrival-session) stock-id)]
    (if (= params :removed) [] params)))

(defmethod inplace-status-param-functions :arrived [limit offset params]
  (retail:admin-supplier-stocks
   limit offset
   (merge
    params
    {:arrival-session (arrival:arrived-stock-ids)
     :exclude-ids (arrival:removed-stock-ids)
     :state [:visible :not-prepared :invisible]
     :arrival (current-arrival)})))

(defn arrival:added-stock?
  "Check if stock is added to session or stored"
  [{:keys [id stock-arrival-parameter name] :as stock}]
  (let [stored-arrival (get stock-arrival-parameter (current-arrival))]
    (and (not (arrival:removed? id))
         (or (arrival:marked? id)
             stored-arrival))))
