(ns gd.views.admin.categories
  (:use gd.model.context
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        clojure.data.json
        gd.model.model
        gd.views.components
        gd.views.admin.stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(def ^:dynamic *view-mode* false)

(defmacro only-edit[& body]
  `(when-not *view-mode*
     ~@body))

(defn- service-category?[{:keys [short-name]}]
  (#{"new" "sale"} short-name))

(defn draw-category[{:keys [id name child key level annotation stocks seo short-name] :as cat}]
  (let [draw-image
        (fn[id code]
          [:div
           (hidden-field (str "annotations[" id "][image]") code)
           [:img {:src (if code (images:get :tiny-avatar code) "/img/px.png")
                  :data-title (tooltip [:img {:src (images:get :stock-large code)}])
                  :style "width:25px;height:25px;border:1px solid #CDCDCD;border-radius:2px;"
                  :onclick (js (.click (get-section ".file-upload" "input")))}]])]
    (list
     [:div.seo-category-info-container.annotation.group.margin5b {:style (str "padding-left:" (* 15 level) "px;")}
      (when-not (service-category? cat)
        (only-edit
          [:div.margin5r.left
           (small-success-button
            [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign]
            :width 34
            :action (modal-window-open))
           (modal-window-admin
            "Создание подкатегории"
            (modal-save-panel
             (small-ok-button "Сохранить"
                              :width 120
                              :action (execute-form* add-category-child[name short-name]
                                                     (category:save *id name (.toLowerCase short-name))
                                                     :validate [(textarea-required "name" "name")
                                                                (textarea-required "short-name" "short-name")
                                                                (textarea-regexp "short-name" #"[a-z-]+" "only in english")]
                                                     :success (reload))))
            [:div.form.padding10
             [:p.margin10b "Название"]
             [:div.margin10b
              (text-field {:class "input input-textarea"} :name)]
             [:p.margin10b "Английская транскрипция для адресной строки браузера"]
             [:div.margin10b
              (text-field {:class "input input-textarea"} :short-name)]])]))

      [:div.left
       (if *view-mode*
         [:div {:style (str "width:" (- 350 (* 15 level)) "px;")} name " ("
          [:a.link {:href (str "/seo/edit/category/" id)} "SEO"] ")"]
         (text-field {:style (str "width:" (- 350 (* 15 level)) "px;")
                      :class "input input-text"
                      :data-title key
                      :onkeyup (cljs (remote* change-category-name
                                              (do (category:update (s* id) (c* (. ($ this) val))) nil)))}
                     :name name))]

      (let [{:keys [images comment]} annotation]
        (list
         [:div.category-image-item.left
          (only-edit
            [:form.upload.none
             (multi-file-upload
              (gen-client-id)
              (form-for-file-json* category-annotation-save-image [nil
                                                                   :annotations-small
                                                                   :stock-large
                                                                   :stock-bono
                                                                   :tiny-avatar]
                                   (do
                                     (retail:add-or-update-annotation *id nil [code])
                                     (draw-image *id code))
                                   :sizes-to-wait [:tiny-avatar :stock-large])
              :text "Загрузить фото"
              :success (js* (fn[data e]
                              (.html (get-section-el (.. e target) ".annotation" ".image") data))))])

          [:div.image.center {:onclick (js (.click (get-section ".category-image-item" ".upload [onclick]")))}
           (draw-image id (first images))]]

           (only-edit
             [:div.category-annotation-item.left
              {:style "width:290px"}
              [:div (text-field {:autocomplete "off"
                                 :class        "input input-text"
                                 :onkeyup      (cljs (remote* category-annotation-save-comment
                                                              (do (retail:add-or-update-annotation
                                                                    (s* id)
                                                                    (c* (get-val this))
                                                                    nil)
                                                                  nil)))}
                                (str "annotations[" id "][comment]")
                                comment)]])
           ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
           ;(only-edit
             [:div.category-updatelink-item.left

              [:div.form.inline-block
               (small-ok-button
                 [:i.glyphicon.glyphicon-sm.glyphicon-link]
                 :width 34
                 :action (modal-window-open))
                          (modal-window-admin
                            "Изменить URL категории"
                            (modal-save-panel
                              (small-ok-button
                                "Сохранить"
                                :width 120
                                :action (execute-form*
                                          update-category-child [short-name short-name-orinal]
                                          (category:update-short *id (.toLowerCase short-name) short-name-orinal)
                                            :validate [(textarea-required "short-name" "short-name")
                                                       (textarea-regexp "short-name" #"[a-z-]+" "only in english")]
                                            :success (reload))))
                                                [:div.form.padding10
                                                 [:p.margin10b "URL категории"]
                                                 [:div.margin10b
                                                  (text-field {:class "input input-textarea"} :short-name short-name)
                                                  (text-field {:class "input input-textarea" :style "display:none"} :short-name-orinal short-name)]

                                                 ])]];)
           ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
           (only-edit
             [:div.category-remove-item.left
              (when (and (empty? child) (= stocks 0) (not (service-category? cat)))
                [:div.form.inline-block
                 (small-danger-button
                   [:i.glyphicon.glyphicon-sm.glyphicon-remove-sign]
                   :width 34
                   :action (execute-form* delete-category []
                                          (category:delete *id)
                                          :success (reload)))])])

      [:div.left.form.margin5t
       [:a.link {:onclick (modal-window-open)} "Шаблон для товаров"]
       (modal-window-admin
        "Создание СЕО-шаблона для товаров"
        (modal-save-panel
         (small-ok-button "Сохранить"
                          :action (execute-form* save-category-seo-info[seo]
                                                 (category:update-seo *id seo)
                                                 :success (js* (closeDialog)))
                          :width 120))
        [:div.form.padding10
         [:div.alert.alert-success.margin10b {:ng-non-bindable true}
          "Параметры для шаблонизации:<br/> {{name}} - название товара<br/> {{price}} - цена товара<br/><br/>Пример использования: Интернет магазин предлагает Вам – {{name}} купить оптом в Украине по цене {{price}} грн<br/><br/>Шаблон применяется ко всем товарам, входящим в данную категорию."]
         (with-group :seo
           (map!
            (fn[type]
              [:div
               [:p.margin10b (clojure.core/name type)]
               [:div.margin10b
                (text-area {:class "input input-textarea" :autocomplete "off"} type (type seo))]])
            [:title-template :keyword-template :description-template]))])]
     (map draw-category (vals child))))])))

(defn draw-categories-tree[]
  (map draw-category (vals (category:tree-with-annotations))))

(defpage "/retail/admin/categories-editor/" {}
  (common-admin-layout
      [:div {:style "width:120px;"}
       (small-success-button (list [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Категория")
                        :width 120
                        :action (modal-window-open))
       (modal-window-admin
        "Создание корневой категории"
        (modal-save-panel
         (small-ok-button "Сохранить"
                          :width 120
                          :action (execute-form* add-root-category[name short-name]
                                                 (category:save nil name (.toLowerCase short-name))
                                                 :validate [(textarea-required "name" "name")
                                                            (textarea-required "short-name" "short-name")
                                                            (textarea-regexp "short-name" #"[a-z-]+" "only in english")]
                                                 :success (reload))))
        [:div.form.padding10
         [:p.margin10b "Название"]
         [:div.margin10b
          (text-field {:class "input input-textarea"} :name)]
         [:p.margin10b "Английская транскрипция для адресной строки браузера"]
         [:div.margin10b
          (text-field {:class "input input-textarea"} :short-name)]])]
    [:div.center {:style "width:1023px;"}


            [:div.admin-category-header.group
             [:div.category-name-item.left {:style "margin-left:72px;"} "Название категории"]
             [:div.category-image-item.left "Фото"]
             [:div.category-annotation-item.left "Комментарий"]
             [:div.category-remove-item.left]
             [:div.category-remove-item.left]]
            [:div {:style "padding-bottom:30px;margin-bottom:20px;"}
             (draw-categories-tree)]]))
