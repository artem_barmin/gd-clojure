(ns gd.views.admin.order_wizard
  (:use gd.model.model
        gd.views.admin.components
        gd.log
        gd.views.bucket.simple
        [clojure.algo.generic.functor :only (fmap)]
        gd.model.context
        gd.views.admin.stocks
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        clojure.data.json
        gd.views.components
        gd.views.admin.stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        [clj-time.core :exclude [extend]]
        hiccup.element
        gd.views.themes.main)
  (:require [gd.views.themes.bono.basic :as basic])
  (:require [clj-time.coerce :as tm])
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defmacro wrap-context-with-filter[& body]
  `(wrap-context [:wholesale]
     (binding [*supplier-info* (merge *supplier-info* {:stocks-mode :available-from-arrival})]
       (doall (do ~@body)))))

(defn- user-selection[]
  [:div.panel.panel-default.margin10t.padding10t {:ng-init (js (set! userType "new"))}
   (text-field {:ng-model "userType" :class "none"} :userType)
   (tabbed-panel
    {:model "userType"}
    (tab :existing "Существующий пользователь"
         [:div.margin10t.well.well-sm
          [:p.margin5b "Введите данные пользователя (имя/фамилия либо email)"]
          [:div.group (user-autocomplete nil)]])
    (tab :new "Новый пользователь"
         [:div.margin10t
          {:ng-init (js (set! editMode true)
                        (set! deliveryType "ukraine"))}
          [:div.form-inline.well.well-sm.margin15b
           [:div.form-group.margin10r {:style "width:420px;"}
            [:label "Логин"]
            (text-field {:class "input input-text" :placeholder "Логин"} :login)]
           [:div.form-group {:style "width:425px;"}
            [:label "Пароль"]
            (text-field {:class "input input-text" :placeholder "Пароль"} :password)]]
          (basic/user-info-edit nil)]))])

(defn- selected-stock[{:keys [id stock quantity sum] :as item}]
  (let [{:keys [name params images]} stock]
    [:tr.form {:onchange (execute-form* wizard-change-item-bucket[]
                                        (bucket:change-item *id (update-in param-map [:quantity] parse-int))
                                        :success (rerender :wizard-selected))}
     [:td name]
     [:td (spinner :quantity :value quantity :width 40 :button-hide true)]
     [:td [:div.orange-dropdown (custom-dropdown :size (get params "size") ((comp :size :params) item))]]
     [:td sum " грн."]
     [:td
      (small-danger-button [:i.glyphicon.glyphicon-sm.glyphicon-remove]
                           :action (execute-form* wizard-delete-item-bucket[]
                                                  (bucket:delete *id)
                                                  :success (rerender :wizard-selected))
                           :width 32)]]))

(defn- selected-items[]
  (wrap-context-with-filter (bucket:items)))

(defn- stocks-selection[]
  [:div.container-fluid.margin10t
   [:div.col-xs-6.well.well-sm
    [:div.margin10b {:role :wizard-stock-filters}
     (stock-filters-block nil)]
    [:div.margin5b "Введите название товара"]
    [:div.margin10b.margin5t
     (text-field {:class "input input-text"} :search)]
    (infinite-scroll* wizard-stocks-list
                      (wrap-context-with-filter
                        (retail:supplier-stocks
                         12
                         (c* offset)
                         (prepare-stock-filters
                          (c* (get-val (in-dialog "[name=search]")))
                          (c* (formState (in-dialog "[role=wizard-stock-filters]"))))))
                      (fn[{:keys [id name] :as stock}]
                        [:div.panel.panel-default.margin5r.margin5b.relative
                         {:onclick (cljs (remote* wizard-select-stock
                                                  (let [{:keys [params]} (wrap-context-with-filter
                                                                           (retail:get-stock (s* id)))]
                                                    (bucket:add-to-bucket {:fkey_stock (s* id)
                                                                           :quantity 1
                                                                           :size (first (get params "size"))}))
                                                  :success (fn[] (clj (rerender :wizard-selected)))))}

                         [:img {:src (images:get :admin-sq-large (first (:images stock)))}]
                         [:div.absolute.clickable.faded-item name]])
                      :limit 12
                      :local true
                      :max-height 385
                      :columns 3)]
   (region* wizard-selected[]
     [:div.col-xs-6 {:style "height:512px; overflow:auto;"}
      [:table.table
       (map selected-stock (selected-items))]])])

(defn- result[]
  [:div.results.margin10t
   [:div {:style "height:497px; overflow:auto;"}
    (map (fn[{:keys [stock quantity params id sum] :as item}]
           [:div.item.form.fluid-container.table-style.group
            {:onchange (execute-form* wizard-change-item-bucket[]
                                      (bucket:change-item *id (update-in param-map [:quantity] parse-int))
                                      :success (rerender :wizard-selected))}
            [:div.col-xs-1
             [:img.margin5r {:src (images:get :stock-medium (first (:images stock)))}]]
            [:div.col-xs-3 (:name stock)]
            [:div.col-xs-2 "Размер: " (:size params)]
            [:div.col-xs-2 "Количество: " quantity]
            [:div.col-xs-2 "Цена: " (:price stock)]
            [:div.col-xs-2 "Сумма: " sum]])
         (selected-items))]
   [:div.text-right.title  "Итого: " (bucket:sum) " грн."]])

(defn- process-bucket-for-user[items]
  (info "Process bucket for user" (sec/id) (:name (sec/logged)))
  (when (seq items)
    (->>
     items
     (map (fn[item] (select-keys item [:size :fkey_stock :quantity])))
     retail:make-order)))

(defn order-creation-wizard[]
  (require-js :admin :order-wizard)
  (modal-window-admin
   "Создание заказа"
   (modal-save-panel
    (small-ok-button
     "Сохранить" :width 120
     :action (execute-form* wizard-create-order[userType user login password phone]
                            (let [user-id
                                  (case userType
                                    "new"
                                    (let [{:keys [id]} (invite:use-internal nil login password login phone)]
                                      (wrap-security id
                                        (update-user param-map)
                                        (sec/update-logged-information)
                                        id))

                                    "existing"
                                    (parse-int user))

                                  items
                                  (bucket:plain-items)]
                              (bucket:clear)
                              (wrap-context-with-filter
                                [(wrap-security user-id
                                   (process-bucket-for-user items))]))
                            :success (reload)
                            :validate [(validator-switch
                                        "userType"
                                        (validator-case "new"
                                                        [(textarea-required "login" "Необходимо указать логин")
                                                         (textarea-email "login" "Неправильный формат e-mail")
                                                         (login-unique-validator)
                                                         (textarea-required "password" "Необходимо указать пароль")
                                                         (user-update-validator)])
                                        (validator-case "existing"
                                                        (textarea-required "author"
                                                                           "Необходимо выбрать пользователя")))])))
   [:div.padding10 {:ng-controller "OrdersCreationWizardCtl"}
    (tabbed-panel
     {}
     (tab :user "Пользователь" (user-selection))
     (tab :stocks "Товары" (stocks-selection))
     (tab :result "Результат" (region* wizard-result[]
                                (wrap-context-with-filter (result)))))]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Register order creation wizard

(register-create-panel :order modal-window-open-angular order-creation-wizard)
