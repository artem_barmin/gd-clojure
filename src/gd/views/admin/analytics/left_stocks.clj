(ns gd.views.admin.analytics.left-stocks
  (:use gd.model.model
        clj-time.format
        [clojure.algo.generic.functor :only (fmap)]
        gd.model.context
        gd.views.admin.components
        gd.utils.styles
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        clojure.data.json
        gd.views.admin.stocks
        gd.views.components
        gd.views.admin.stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:import (org.joda.time DateTimeZone))
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defn- aggregate-left-statistics[statistics]
  (->>
   statistics
   (mapcat :left-stock-statistics)
   (remove nil?)
   (map (fn[{:keys [quantity price]}]
          (if (and quantity price)
            (* price quantity)
            0)))
   (reduce +)))

(defn- stock-template[{:keys [name images id category  status
                              fkey_category state params all-params
                              visits left-stocks stock-arrival-parameter
                              tags stock-variant
                              left-stock-statistics]
                       :as stock}]
  [:tr {:data-id id
        :class (when (= state :dirty) :inline-state-dirty)}
   [:td
    (hidden-field :id id)
    (custom-checkbox {:role :selection} :choose false)]
   [:td
    (sized-image {:class (list "organizer-stock-list-image" (when (is-sale? stock) " is-sale"))
                  :data-title (images-tooltip images)}
                 (images:get :stock-medium (first images)))]
   [:td {:data-title (tooltip (params-tooltip all-params))} name]
   [:td
    (hidden-field  :fkey_category fkey_category)
    [:div
     {:data-title
      (tooltip
       [:div {:style "padding:0 5px 5px; font-size:10px !important;"}
        [:div (draw-category fkey_category)]])}
     [:div (draw-category-name fkey_category)]]]
   [:td
    (if (seq stock-variant)
      [:div.group {}
       (map (fn[[params contexts]]
              (let [requested-stocks (or (get left-stocks params) 0)
                    final-price (fn[type] (or (:price (find-by-val :context type contexts)) 0))]
                [:div.stock-tag-group.left
                 [:span.stock-tag.admin-stock-size {} (draw-variant params)]
                 [:span.stock-tag.admin-stock-quantity {} requested-stocks " шт."]
                 [:span.stock-tag.admin-stock-price {} (final-price :wholesale) ",00"]]))
            stock-variant)]
      [:div.text-left "Варианты не были добавлены"])]
   [:td (or (get-brand all-params) "Не установлен")]
   [:td {:style "width:100px;"}
    id
    " "
    (aggregate-left-statistics left-stock-statistics) " грн."]])

(defpage "/retail/admin/analytics/left-stocks/" {}
  (require-js :admin :reports)
  (common-admin-layout
      [:div {:role :report-config}
       [:div {:role :filters}
        (stock-filters-block nil)]]

    (pager left-stock-report-pager
           (analytics:left-stocks-statistics
            page-size
            (c* offset)
            (prepare-stock-filters
             (c* (get-val "[name=search]"))
             (c* (formState "[role=filters]"))))
           stock-template
           :page 100
           :columns 1
           :custom-rows-mode true)))
