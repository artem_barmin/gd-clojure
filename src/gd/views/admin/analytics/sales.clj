(ns gd.views.admin.analytics.sales
  (:use gd.model.model
        clj-time.format
        [clojure.algo.generic.functor :only (fmap)]
        gd.model.context
        gd.views.admin.components
        gd.utils.styles
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        clojure.data.json
        gd.views.admin.stocks
        gd.views.components
        gd.views.admin.stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:import (org.joda.time DateTimeZone))
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(def group-by-transform {:stocks :id
                         :whole :whole
                         :categories :fkey_category
                         :brands [:params :brand]})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Different views of the result

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stocks list

(defn- stock-entity[{:keys [stock params consumed statistics left sold] :as entry}]
  [:div {} (str entry)])

(defn- stocks[group period]
  (pager sale-report-pager
         (let [report (analytics:sale-report
                       :period (s* period)
                       :group (group-by-transform (s* group))
                       :filters (prepare-stock-filters
                                 (c* (get-val "[name=search]"))
                                 (c* (formState "[role=filters]"))))]
           (if gd.utils.db/*count-query*
             [{:count (count report)}]
             (take page-size (drop (c* offset) report))))
         stock-entity
         :page 30
         :columns 1
         :custom-rows-mode true))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Categories tree

(defn- sum-statistics[items]
  (let [all-periods (distinct (mapcat keys items))]
    (mreduce (fn[period]
               {period (let [items (map (fn[m] (get m period)) items)]
                         {:sold (reduce + (map (fn[{:keys [sold]}](or sold 0)) items))
                          :arrived (reduce + (map (fn[{:keys [arrived]}](or arrived 0)) items))})})
             all-periods)))

(defn- categories[filters report & [level tree]]
  (let [level (or level 0)
        tree (or tree (stock:container-categories-tree (merge filters {:show-invisible true})))]
    (mapcat (fn[[k {:keys [child name id] :as cat}]]
              (let [childs (when child (categories filters report (inc level) child))
                    [parent] (reverse (category:parents id))
                    parent (when-not (= (:id parent) id) parent)]
                (cons {:name [:div {:style (css-inline :padding-left (* 16 level))} name]
                       :real-name (str (:name parent) (when parent "→") name)
                       :level level
                       :items (or (get report id)
                                  (->> childs
                                       (filter (comp (partial = (inc level)) :level))
                                       (map :items)
                                       sum-statistics))}
                      childs)))
            tree)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Params list

(defn- params[filters result]
  (map (fn[[brand items]] {:name brand :real-name brand :items items}) result))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Whole amount

(defn- whole[filters {:keys [whole]}]
  [{:name "Все результаты" :real-name "Все результаы" :items whole}])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Web endpoint

(defn- normalize-items[items]
  (let [all-periods (sort (distinct (mapcat (comp keys :items) items)))]
    {:periods all-periods
     :items (map (fn[{:keys [items] :as item}]
                   (assoc item :items (map (fn[period] (get items period)) all-periods)))
                 items)}))

(defn- render-items-to-table[period result-format result]
  (let [y-value (fn[{:keys [sold arrived]}] (case result-format
                                              :sold sold
                                              :arrived (long (or arrived 0))
                                              :profit (if (and sold arrived) (- sold arrived) 0)))
        x-value (fn[val] (cond (= period :whole) "Весь период"
                               (sequential? val) (clojure.string/join "/" val)
                               true (str val)))
        {:keys [items periods]} (normalize-items result)]
    [:table.table
     [:tr {:data-x-labels (cljs (vec (map x-value periods)))}
      [:th {:style "width:30px;"} (custom-checkbox {:onchange (js (selectAllOnPage this))} :choose false)]
      [:th "Название"]
      (map (fn[period] [:th (x-value period)]) periods)]
     (map-indexed
      (fn[id {:keys [name items real-name] :as item}]
        [:tr.report-item {:data-id id
                          :role :selection
                          :data-chart (cljs (map y-value items))
                          :data-real-name real-name}
         [:td (custom-checkbox :choose false)]
         [:td name]
         (map (fn[item] [:td.padding5 (y-value item)]) items)])
      items)]))

;; TODO : group by stocks
;; TODO : group by price
(defpage "/retail/admin/analytics/sales/" {}
  (require-js :admin :reports)
  (require-js :admin-lib :d3)
  (require-js :admin-lib :nv.d3)
  (require-js :admin-lib :legend)
  (require-css :admin-lib :charts.nv.d3)
  (common-admin-layout
      [:div {:role :report-config}
       [:div {:role :filters}
        (stock-filters-block nil)]]

    [:div
     [:div.row.margin15t {:role :report-config}
      [:div.col-xs-3 {:role :filters}
       (let [date {:style "width:100px;"
                   :bs-datepicker true
                   :data-container "body"
                   :data-date-format "yyyy-MM-dd"
                   :data-autoclose true}]
         [:div.panel.panel-default.margin15l
          [:div.panel-heading "Период:"]
          [:div.panel-body
           [:i.fa.fa-calendar.margin3r] "С:"
           [:input.input.input-text.margin5l.margin20r (merge date {:ng-model :from :name :from})]
           [:i.fa.fa-calendar.margin3r] "По:"
           [:input.input.input-text.margin5l (merge date {:ng-model :to :name :to})]]])]

      [:div.col-xs-3 {:role :config}
       [:div.panel.panel-default
        [:div.panel-heading "Результаты:"]
        [:div.panel-body.blue-dropdown (custom-dropdown :result-format [["Продажи" :sold]
                                                                        ["Закупки" :arrived]
                                                                        ["Прибыль" :profit]])]]]
      [:div.col-xs-3 {:role :config}
       [:div.panel.panel-default
        [:div.panel-heading "Группировка:"]
        [:div.panel-body.blue-dropdown (custom-dropdown :group [["Категории" :categories]
                                                                ["По всем товарам" :whole]
                                                                ;; ["Товары" :stocks]
                                                                ["Бренды" :brands]])]]]
      [:div.col-xs-3 {:role :config}
       [:div.panel.panel-default.margin15r
        [:div.panel-heading "Период аггрегации:"]
        [:div.panel-body.blue-dropdown (custom-dropdown :period [["По месяцам" :month]
                                                                 ["За весь период" :whole]
                                                                 ["По неделям" :week]
                                                                 ["По дням" :day]
                                                                 ["По дням недели" :day-of-week]])]]]]

     [:div.group
      [:div.admin-inplace-edit-tabs.alert.alert-success {:style "margin:0 15px 15px;"}
       [:div.group
        (custom-radio-button :tab-state :table [:div.admin-inplace-edit-tabs.left "Таблица"] true)
        (custom-radio-button :tab-state :line-chart [:div.admin-inplace-edit-tabs.left "Линейный график"])
        (custom-radio-button :tab-state :pie-chart [:div.admin-inplace-edit-tabs.left "Круговая диаграмма"])]]]

     [:div#table.group.data-view.padding15l.padding15r
      (region* sales-report-result[]
        (when *in-remote*
          (let [{:keys [group period result-format]}
                (fmap keyword (c* (formState "[role=config]")))

                english-date-formatter (-> (formatter "yyyy-MM-dd")
                                           (with-locale en-locale)
                                           (with-zone (DateTimeZone/getDefault)))]
            (case group
              ;; multipage - should be showed as pager
              :stocks (stocks group period)
              ;; other reports are single paged
              (let [{:keys [from to] :as filters}
                    (prepare-stock-filters (c* (get-val "[name=search]")) (c* (formState "[role=filters]")))]
                (render-items-to-table
                 period result-format
                 ((case group
                    :whole whole
                    :brands params
                    :categories categories)
                  filters
                  (analytics:sale-report :period period
                                         :group (group-by-transform group)
                                         :from (when (seq from) (parse english-date-formatter from))
                                         :to (when (seq to) (parse english-date-formatter to))
                                         :filters filters))))))))]
     [:div#line-chart.group.data-view.none
      [:svg {:style "height:500px;width:1200px;display:inline-block;"}]]
     [:div#pie-chart.group.data-view.none
      [:svg {:style "height:500px;width:1200px;display:inline-block;"}]]]))
