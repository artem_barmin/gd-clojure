(ns gd.views.admin.reviews
  (:use gd.views.resources
        gd.views.admin.common
        gd.views.admin.components
        [clj-time.core :exclude (extend)]
        clj-time.format
        [clojure.algo.generic.functor :only (fmap)]
        gd.views.bucket.simple
        compojure.core
        gd.model.model
        gd.model.context
        gd.utils.web
        gd.utils.common
        gd.views.messages
        gd.views.components
        noir.core
        [noir.options :only (dev-mode?)]
        [noir.response :only (redirect status set-headers)]
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clj-time.coerce :as tm])
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defn- admin-review-edit-modal [{:keys [text sent-time user removed id] :as entity}]
  (modal-window-admin
   (if id (str "Редактирование отзыва №" id) "Создание нового отзыва")
   (modal-save-panel (small-ok-button
                      "Сохранить" :width 120
                      :action (execute-form* create-review[]
                                             (let [{:keys [sent-time user removed text]}
                                                   (-> param-map
                                                       (update-in [:sent-time] (partial parse english-date-formatter))
                                                       (update-in [:user] parse-int)
                                                       (update-in [:removed] (partial = "removed")))]
                                               (retail:send-review-admin {:text text
                                                                          :type :supplier-review
                                                                          :sent-time (tm/to-timestamp sent-time)
                                                                          :removed removed}
                                                                         {:id user}))
                                             :success (js* (supplierReviewsReload)
                                                           (closeDialog)))))
   [:div {:ng-controller "ReviewsCtl"}
    [:div.padding10 {:ng-init (js (set! selectedDate (clj (format-just-date
                                                           (or sent-time (now))
                                                           :language :english))))}

     [:div.group
      [:div.left.margin20r {:style "width:538px;"}
       [:p.margin5b "Автор"]
       [:div.margin10b
        (user-autocomplete user)]]

      [:div.left.margin20r
       [:p.margin5b "Дата"]
       [:div.margin10b
        (text-field {:class "input input-text"
                     :data-animation "am-flip-x"
                     :ng-model "selectedDate"
                     :bs-datepicker true
                     :data-autoclose true
                     :data-date-format "dd MMM yyyy"}
                    :sent-time)]]

      [:div.left
       [:p.margin5b "Публикация"]
       [:div.red-dropdown.margin10b {:style "width:143px;"}
        (custom-dropdown
         {:width 265}
         :removed [["Да" :published]
                   ["Нет" :removed]]
         (if removed :removed :published))]]]

     [:p.margin5b "Текст"]
     [:div.fixed-height-100px.margin20b
      (text-area {:class "input input-textarea"} :text text)]]]))

(defn admin-reviews-template [{:keys [text sent-time user removed id] :as entity}]
  (cond (= entity :header)
        [:tr.admin-table-head
         [:td.fixed-200px-width-cell "Автор"]
         [:td.fixed-150px-width-cell "Дата"]
         [:td.fixed-100px-width-cell "Публиковать"]
         [:td.full-width-cell "Текст"]
         [:td.fixed-25px-width-cell ]]

        true
        [:tr
         [:td (:name user)]
         [:td (format-date sent-time)]
         (let [removed (boolean removed)]
           [:td.link-container.form
            [:a {:href "#"
                 :onclick (execute-form* admin-reviews-message-show []
                                         (if *removed
                                           (message:restore *id)
                                           (message:remove *id))
                                         :success (js* (supplierReviewsReload)))}
             (if removed "Нет" "Да")]])
         [:td [:p.cutted {:style "width:490px"} text]]
         [:td {:style "padding:5px 10px;"}
          (small-ok-button
           [:i.glyphicon.glyphicon-sm.glyphicon-pencil {:style "margin:0 -7px;"}]
           :width 32
           :action (modal-window-open-angular)) (admin-review-edit-modal entity)]]))

(defpage "/retail/admin/review/" {}
  (require-js :admin :reviews)

  (common-admin-layout
      [:div {:style "width:140px;"}
       (small-success-button
        (list [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Отзыв")
        :width 130
        :action (modal-window-open-angular))
       (admin-review-edit-modal nil)]
    [:div.admin.group

     [:div.group.admin-panel.retail-standard-panel
      [:div.admin-stock-list
       (pager supplier-reviews
              (modify-result
               (message:messages {:id *current-supplier*} :inbox :supplier-review
                                 {:offset-num (c* offset) :limit-num page-size :show-removed true})
               (fn[res](concat [:header] res)))
              admin-reviews-template
              :columns 1
              :custom-rows-mode true
              :pager-title "Список отзывов"
              :page 30)]]]))
