(ns gd.views.admin.login
  (:use gd.views.resources
        compojure.core
        gd.views.admin.common
        gd.views.themes.custom.template
        gd.utils.web
        gd.model.model
        gd.utils.common
        gd.utils.mail
        gd.views.components
        noir.core
        [noir.options :only (dev-mode?)]
        [noir.response :only (redirect status set-headers)]
        hiccup.core
        com.reasonr.scriptjure
        [clostache.parser :only (render-resource)]
        hiccup.page
        hiccup.form
        hiccup.element
        gd.model.test.data_builders)
  (:require [gd.utils.security :as internal-sec])
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defn registration-notification [login email password]
  (let [data {:name login
              :pass password}]
    (send-mail email
               "Поздравляем с регистрацией в Smartcommerce"
               (render-resource "mail/smartcommerce-registration.html" data))))

(defn create-partner[login email password phone]
  (gd.utils.db/transaction
    (let [supplier (retail:create-supplier login)
          user (:id (invite:use-internal nil login password email phone))]
      (retail:assign-supplier-user user supplier)
      (domain:add-admin supplier)
      (user:update user {:role ["user" "partner" "seo"]}))))

(def admin-access #{"admin" "manager" "partner" "moderator"})
(def seo-access #{"admin" "seo"})
(def copywriter-access #{"copywriter"})

(defn- login-and-get-host[login password]
  (if-let [auth-info (user:check-credentials login password)]
    (do (internal-sec/set-logged auth-info)
        (let [roles (:role auth-info)]
          (cond
            (some #(admin-access %) roles)
            "/admin/stocks/"
            (some #(copywriter-access %) roles)
            "/copywriter/stocks/"
            (some #(seo-access %) roles)
            "/seo/categories-editor/"
            :else
            (throwf "Not have permission to access"))))
    (throwf "Authorization error")))


(defn get-all-admin-mail []
  (->>
    (user:get-all-users nil nil :role :admin)
    (map (comp :email :contact))
    (map :value)
    (remove nil?)))

(defn mail-validator []
  [(textarea-required "name" "Необходимо заполнить имя и фамилию")
   (textarea-required "phone" "Необходимо заполнить номер телефона")
   (textarea-phone "phone" "Номер в формате 0xxxxxxxxx")
   (textarea-required "email" "Необходимо заполнить email")])

(def master-admin ["koval111vladislav@gmail.com" "artem.barmin@gmail.com" "kolesnikne@bk.ru"])

(defn send-mail-for-all-admin []
  (execute-form* send-mail-for-all-admin [name phone email]
    (map (fn [address] (send-mail address
                         "Заявка на нового пользователя"
                         (str "Заявка на нового пользователя" \n  "имя:" name \n "телефон:" phone \n "email:" email)))
      master-admin)
    :keep-data true
    :success (js* (cleanForm form)
               (.modal ($ "#registration") "hide")
               (.modal ($ "#success") "show"))
    :validate (mail-validator)))


(defpage "/smartcommerce/" []
  (xhtml (render-template
           (prepare-template (slurp "src/gd/views/admin/index.html"))
           {:send-form #'send-mail-for-all-admin
            :br "<br>"})))

(defpage "/smartcommerce/login/" []
  (admin-common-require)
  (html
   (xml-declaration "UTF-8")
   [:html {:xmlns "http://www.w3.org/1999/xhtml"
           "xmlns:og" "http://ogp.me/ns#"
           "xml:lang" "ru"
           :lang "ru"}
    [:head
     [:title "SmartCommerce. Страница входа"]
     (favicon "/img/grid-admin/favicon.ico")
     (resources)
     (include-css "http://weloveiconfonts.com/api/?family=brandico")]
    [:body.signin-page
     [:div#floater {:style "float:left; height:50%; margin-bottom:-200px;"}]
     [:section#signin-container {:style "clear:both; height:300px; width:312px; margin:0 auto;"}
      [:div.header
       (image "/img/grid-admin/logo.png")]
      [:div
       [:fieldset.form
        [:div
         (text-field {:placeholder "Логин"} :login)
         (password-field {:placeholder "Пароль"} :password)]
        [:a.register {:onclick (modal-window-open)} "Регистрация"]
        [:a.forgot-password {:href "#"} "Забыли?"]
        [:div.small-ok-button-plain
         {:onclick (execute-form* login-smartcommerce[login password]
                                  (login-and-get-host login password)
                                  :success (js* (set! location result)))}
         "Вход"]]
       (modal-window-admin
        "Регистрация нового пользователя"
        (modal-save-panel
         (small-ok-button "Зарегистрироваться"
                          :action (execute-form* create-partner[login email password]
                                                 (do (create-partner login email password "")
                                                     (registration-notification login email password)
                                                     (create-instance login)
                                                     (login-and-get-host login password))
                                                 :success (js* (set! location (str #_result "/smartcommerce/login/"))))))
        [:div.form
         [:div {:style "padding:5px 0;"}
          [:div
           (text-field {:placeholder "Логин" :class "signin-modal-input input input-text"} :login)]
          [:div
           (text-field {:placeholder "E-mail" :class "signin-modal-input input input-text"} :email)]
          [:div
           (password-field {:placeholder "Пароль" :class "signin-modal-input input input-text"} :password)]]])]
      [:div.social
       [:p "... или войдите через"]
       [:a.twitter [:span.brandico.brandico-twitter-bird]]
       [:a.odnoklassniki [:span.brandico.brandico-odnoklassniki]]
       [:a.vkontakte [:span.brandico.brandico-vkontakte-rect]]]]]]))
