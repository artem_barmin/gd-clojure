(ns gd.views.admin.seo.pages
  (:use gd.model.context
        gd.views.admin.pages
        gd.views.admin.categories
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        clojure.data.json
        gd.model.model
        gd.views.components
        gd.views.admin.stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defpage "/retail/seo/page/" {}
  (seo-admin-layout
   (pages-list :page "/seo/edit/page/new" :parent-url "seo")))

(defpage "/retail/seo/edit/page/:id"  {id :id}
  (seo-admin-layout
   (control-buttons id :page :parent-url "seo")
   (add-page id :page :parent-url "seo")))
