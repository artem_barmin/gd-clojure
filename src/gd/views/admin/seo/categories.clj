(ns gd.views.admin.seo.categories
  (:use gd.model.context
        gd.views.admin.categories
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        clojure.data.json
        gd.model.model
        gd.views.components
        gd.views.admin.stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defpage "/retail/seo/categories-editor/" {}
  (seo-admin-layout nil
    [:div.retail-standard-panel.center {:style "width:600px;"}
     [:div.seo-category-header.group
      [:div.category-name-item.left {:style "margin-left:35px;"} "Название категории"]
      [:div.category-image-item.left "Фото"]]
     [:div {:style "padding-bottom:30px;margin-bottom:20px;"}
      (binding [*view-mode* true]
        (html (draw-categories-tree)))]]))
