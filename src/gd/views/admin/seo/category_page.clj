(ns gd.views.admin.seo.category-page
  (:use gd.model.context
        gd.views.admin.pages
        gd.views.admin.categories
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        clojure.data.json
        gd.model.model
        gd.views.components
        gd.views.admin.stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defpage "/retail/seo/category-page/" {}
  (seo-admin-layout nil (pages-list :category-page "/seo/edit/category-page/new" :parent-url "seo")))

(defpage "/retail/seo/edit/category-page/:id"  {id :id}
  (seo-admin-layout
   (control-buttons id :category-page :parent-url "seo")
   (add-page id :category-page :parent-url "seo")))

(defpage seo-pages-for-category "/retail/seo/edit/category/:id"  {id :id}
  (let [id (parse-int id)
        {:keys [name path]} (category:get-by-id id)
        {:keys [id]} (site-pages:get-page-for-url path)]
    (seo-admin-layout
     (control-buttons (str (or id "new"))  :category-page :parent-url "seo")
     (add-page (or id "new") :category-page
               :parent-url "seo"
               :new-initial-state {:url path :title name}))))
