(ns gd.views.admin.seo.news
  (:use gd.model.context
        gd.views.admin.pages
        gd.views.admin.categories
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        clojure.data.json
        gd.model.model
        gd.views.components
        gd.views.admin.stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defpage "/retail/seo/news/" {}
  (seo-admin-layout (pages-list :news "/seo/edit/news/new"
                                :parent-url "seo"
                                :hide-addition true)))

(defpage "/retail/seo/edit/news/:id"  {id :id}
  (seo-admin-layout
   (control-buttons id :news :parent-url "seo")
   (add-page id :news :parent-url "seo")))

(defpage "/retail/seo/blogs/" {}
   (seo-admin-layout
     [:a.small-success-button-plain.inline-block {:href "/seo/edit/blogs/new"}
      [:div.admin-action-panel.admin-adding-panel
       [:div [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Блог"]]]
     (pages-list :blogs "/seo/edit/blogs/new"
                                       :parent-url "seo"
                                       :hide-addition true)))

(defpage "/retail/seo/edit/blogs/:id"  {id :id}
         (seo-admin-layout
           (control-buttons id :blogs :parent-url "seo")
           (add-page id :news :parent-url "seo")))