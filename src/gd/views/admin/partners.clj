(ns gd.views.admin.partners
  (:use gd.model.model
        gd.utils.logger
        gd.log
        org.httpkit.server
        gd.views.admin.components
        gd.views.admin.dictionary
        [clojure.algo.generic.functor :only (fmap)]
        gd.model.context
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        [noir.response :only (redirect)]
        clojure.data.json
        gd.views.components
        gd.views.admin.stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        [clj-time.core :exclude (extend)]
        hiccup.form
        hiccup.element)
  (:require [overtone.at-at :as at])
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defn- round-hours[time]
  (-> time (.withMillisOfSecond 0) (.withSecondOfMinute 0) (.withMinuteOfHour 0)))

(def request-to-login (atom {}))

(defn- user-template[{:keys [name full-address contact id stocks orders users supplier] :as user}]
  [:tr
   [:td [:a.link {:href (str "/admin/system/log/" id)} name]]
   [:td (let [url (str (:name supplier) ".sm24.com.ua")]
          [:a.link {:href (str "http://" url)} url])]
   [:td [:a.link "Бесплатный"]]
   [:td [:a.link stocks "/50"]]
   [:td [:a.link users]]
   [:td [:a.link orders]]
   [:td [:a.link "121/150"]]
   [:td [:a.link {:onclick (modal-window-open)}
         "Заметки"
         (modal-window-admin
          "Управление заметками"
          (modal-save-panel
           (small-ok-button
            "Сохранить"
            :width 120
            :action (execute-form* save-partner-notes[notes]
                                   (user:add-or-update-bookmark notes :fkey_user *id))))
          [:div (text-area {:name :notes :placeholder "Можно оставлять свои заметки" :class "form-control"}
                           :notes
                           (:note (user:get-bookmark :fkey_user id)))])]]
   [:td [:a.link
         {:onclick (js (if (confirm "Вы уверены что хотите войти как пользователь?")
                         (clj (remote* retail-login-as-user
                                       (let [uid (str (java.util.UUID/randomUUID))]
                                         (swap! request-to-login assoc uid (s* id))
                                         uid)
                                       :success (fn[res]
                                                  (.open window
                                                         (str (clj (str "http://" (:name supplier) ".sm24.com.ua/login-as/")) res)
                                                         "_blank"))))))}
         "Войти как пользователь"]]])

(defn- login-as-request[id]
  (let [user-id (get @request-to-login id)
        {:keys [name supplier]} (retail:get-partner user-id)]
    (user:login-as-user user-id)
    (swap! request-to-login dissoc id)
    (redirect (str "http://" (:name supplier) ".sm24.com.ua/admin/stocks/"))))

(defpage "/themes/login-as/:id" {id :id}
  (login-as-request id))

(defpage "/retail/admin/system/partners/" {}
  (common-admin-layout nil
    [:div.retail-standard-panel.admin-panel {:style "padding:5px 0 1px 0;"}
     [:div.admin-stock-list
      (pager retail-users
             (modify-result
              (retail:get-all-partners page-size (c* offset))
              (fn[res] (concat [:header] res)))
             (fn[entity]
               (cond (= entity :header)
                     (precompile-html
                      [:tr.admin-table-head
                       [:td.full-width-cell "Партнер"]
                       [:td.full-width-cell "Сайт"]
                       [:td.fixed-150px-width-cell "Тариф"]
                       [:td.fixed-150px-width-cell "Товары"]
                       [:td.fixed-150px-width-cell "Клиенты"]
                       [:td.fixed-150px-width-cell "Заказы"]
                       [:td.fixed-150px-width-cell "SMS"]
                       [:td.fixed-150px-width-cell "Заметки"]
                       [:td.fixed-150px-width-cell "Войти"]])

                     true
                     (user-template entity)))
             :page 10
             :custom-rows-mode true
             :columns 1
             :pager-title "Список партнеров")]]))

(defonce clients (atom #{}))
(defonce track-sessions (atom {}))

(defn send-all[session-id data]
  (doseq [{:keys [channel id]} @clients]
    (when ((get @track-sessions id) session-id)
      (when (open? channel)
        (send! channel (json-str data))))))

(defn chat-handler [req]
  (with-channel req channel
    (info channel "connected")
    (swap! clients conj {:channel channel :id (:id (:params req))})
    (on-close channel (fn [status]
                        (swap! clients disj channel)
                        (info channel "closed, status" status)))))

(defn log-template[{:keys [type uri service result timestamp time host session-id]}]
  [:div.panel.panel-default.margin5r.margin5b.relative
   [:div.container-fluid
    [:div.col-xs-1
     (if (= (keyword result) :success)
       [:i.fa.fa-check {:style "color:#95CA4D;"}]
       [:i.fa.fa-ban {:style "color:#E7699E;"}])
     (name type)]
    [:div.col-xs-2 (format-timestamp timestamp)]
    [:div.col-xs-2 (or service uri)]
    [:div.col-xs-1 time " ms"]
    [:div.col-xs-1 host]]])

(defpage "/retail/admin/system/log/:id" {id :id}
  (require-js :admin :log)
  (swap! track-sessions assoc id (set (logger:sessions (parse-int id))))
  (common-admin-layout nil
    [:div
     (document-ready (js* (startLogCapture (clj id))))
     (infinite-scroll* events-log
                       (logger:get-messages-for-user (parse-int id) 100 (or (c* offset) 0))
                       log-template
                       :limit 100
                       :columns 1)]))

(custom-handler "/admin/events/:id" {:as req}
                (chat-handler req))

(add-watch log-listener :websocket (fn[_ _ _ {:keys [type session-id] :as new}]
                                     (if (#{:service :web-request} type)
                                       (send-all session-id {:event (html (log-template new))}))))

(defonce scheduled (at/every (* 1000 30)
                             (fn[]
                               (doseq [{:keys [channel]} @clients]
                                 (try
                                   (send! channel (json-str {:ping true}))
                                   (catch Throwable e))))
                             (at/mk-pool :cpu-count 1)))
