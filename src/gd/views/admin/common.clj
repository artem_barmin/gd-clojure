(ns gd.views.admin.common
  (:use gd.views.resources
        gd.model.context
        [clojure.algo.generic.functor :only (fmap)]
        gd.views.bucket.simple
        compojure.core
        gd.model.model
        gd.utils.web
        gd.utils.common
        gd.views.components
        noir.core
        [noir.options :only (dev-mode?)]
        [noir.response :only (redirect status set-headers)]
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Global create actions

(defonce ^:dynamic create-panels (atom nil))

(defn register-create-panel[name open-action modal]
  (swap! create-panels assoc name {:open open-action
                                   :modal modal}))

(defn create-panel[name]
  (js (.click ($ (clj (as-str "[role=create-panel][data-key=" name "]"))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Resources
(defn resources []
  (list
   (process-js-and-css :css)
   (process-js-and-css :js)
   (javascript-tag
    (js (var trackVisit (fn[]))
        (var trackSocialEvent (fn[]))))))

(defn- admin-require-css[]
  (require-css :custom-lib :notify)
  (require-multiple-css :styles :main :components :modal)
  (require-css :admin :admin)
  (require-css :grid :structure)
  (require-css :grid :base_skin)
  (require-css :grid :commons))

(defn- admin-require-js[]
  (require-js :admin :inventory)
  (require-js :admin :search)
  (require-js :admin :stocks)
  (require-js :admin :common)
  (require-js :admin :selection)
  (require-js :admin :order)
  (require-js :admin :seo-css)
  (require-js :admin :redirects)
  (require-js :user :fileupload)

  (require-js :custom-lib :next-prev-pager)
  (require-js :custom-lib :inputbf)
  (require-js :custom-lib :spinner)
  (require-js :custom-lib :dropdown)
  (require-js :custom-lib :modal-panel)
  (require-js :custom-lib :validation)
  (require-js :custom-lib :common)
  (require-js :custom-lib :notify)

  (require-css :lib :dropkick)
  (require-js :lib :lodash)
  (require-js :lib :once)
  (require-js :lib :tooltip)
  (require-js :lib :jquery.fileupload)

  (require-js :lib :angular-strap.tpl)
  (require-js :lib :angular-strap)
  (require-js :lib :angular-animate)

  (require-js :external :jquery-ui)
  (require-js :external :jquery)
  (require-js :external :angular-sanitize)
  (require-js :external :angular)
  (require-js :external :bootstrap-js)

  (require-css :external "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css")
  (require-css :external "http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css")
  (require-css :external "http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css")

  (require-css :lib :angular-strap))

(defn admin-common-require[]
  (admin-require-js)
  (admin-require-css))

(defn- menu-item[name icon & [url]]
  [:li.menu-item {:onclick (js
                            (.removeClass (.siblings ($ this)) "selected")
                            (.hide ".submenu")
                            (.toggleClass ($ this) "selected"))}
   [:a {:href url}
    [:div.menu-item-icon {:class icon}]
    [:div.menu-item-text name]
    [:div.menu-item-hover]]])

(defn- with-submenu[name icon & items]
  (let [submenu
        [:div.submenu.none
         [:p.title.margin20b name]
         (map
          (fn[[link name]]
            [:div.submenu-item.margin5b
             [:a.retail-link {:href link} [:p name]]])
          items)]

        submenu-show-hide
        {:onclick (js
                   (if (> (.. (.siblings ($ this) ".selected") length) 0)
                     (do
                       (.removeClass (.siblings ($ this)) "selected")
                       (.removeClass ($ ".content-block") "navigation-offset")
                       (.hide (.siblings (.next ($ this)) ".submenu"))
                       (.hide ($ ".submenu-arrow"))
                       (.hide (.siblings (.next ($ this)) ".submenu-popup"))))
                   (.toggleClass ($ ".content-block") "navigation-offset")
                   (.toggle (.next ($ this)))
                   (.toggleClass ($ this) "selected"))}]

    (list
     [:li.menu-item submenu-show-hide
      [:a
       [:div.menu-item-icon {:class icon}]
       [:div.menu-item-text name]
       [:div.menu-item-hover]]]
     submenu)))

(defn- with-popup-submenu[name icon & items]
  (let [submenu
        [:div.submenu-popup.none
         (map
          (fn[[key name popup-icon]]
            (let [{:keys [open modal]} (get @create-panels key)]
              [:div.submenu-popup-item {:onclick (and open (open)) :role :create-panel :data-key key}
               [:div.submenu-popup-icon.center {:class popup-icon}]
               [:p name]
               (and modal (modal))]))
          items)]

        submenu-show-hide
        {:onclick (js
                   (if (> (.. (.siblings ($ this) ".selected") length) 0)
                     (do
                       (.removeClass (.siblings ($ this)) "selected")
                       (.removeClass ($ ".content-block") "navigation-offset")
                       (.hide (.siblings (.next ($ this)) ".submenu"))
                       (.hide (.siblings (.next ($ this)) ".submenu-popup"))))
                   (.toggleClass ($ this) "selected")
                   (.toggle (.next ($ this)))
                   (.toggle ($ ".submenu-arrow"))
                   (.position ($ ".submenu-popup") {:of ($ this)
                                                    :my "right center"
                                                    :at "left center"
                                                    :offset "-24 0"}))}]

    (list
     [:li.menu-item submenu-show-hide
      [:a
       [:div.menu-item-icon {:class icon}]
       [:div.menu-item-text name]
       [:div.menu-item-hover]]
      [:div.submenu-arrow.none]]
     submenu)))

(defn custom-dropdown-with-panel [panel-name]
  [:div.header-btn {:onclick (js (.toggle ($ ".domain-block")))}
   [:i.glyphicon.glyphicon-domain.glyphicon-globe]
   [:span.header-btn-text panel-name]
   [:div.domain-block.none
    (remove
     nil?
     (map
      (fn[{:keys [host domain]}]
        (when (= (get-in domain [:info :context-filter]) ["wholesale"])
          (let [status "trial"]
            [:div {:class (if (= status "expired")
                            "domain-item domain-disabled"
                            "domain-item")}
             [:div.domain-item-title status]
             [:div.domain-item-body
              [:a {:href host} host]]])))
      (domain:get-client-domains)))
    [:div.domain-arrow]]])

(defn base-admin-layout[menu controls body]
  (admin-common-require)
  (let [menu (html menu)
        body (html body)]
    (html
     (xml-declaration "UTF-8")
     (doctype :xhtml-strict)
     [:html {:xmlns "http://www.w3.org/1999/xhtml"
             "xmlns:og" "http://ogp.me/ns#"
             "xml:lang" "ru"
             :lang "ru"
             :ng-app "deals"}
      [:head
       [:title "Страница администратора"]
       (include-css "http://fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic")
       (favicon "/img/grid-admin/favicon.ico")
       (resources)
       [:body {:ng-controller "RefreshCtl"}
        [:header
         [:div.navbar.navbar-container
          [:div.col-sm-4.col-md-4.col-lg-4
           [:div.left.margin10l (image "/img/grid-admin/logo.png")]]
          [:div.col-sm-8.col-md-8.col-lg-8
           [:div.right.margin15l
            (custom-dropdown-with-panel "Выбор домена")]
           [:div.search-container.right
            (text-field
             {:class "search-input"
              :autocomplete "off"
              :onfocus (js (.addClass (.parent ($ this)) "active"))
              :onblur (js (.removeClass (.parent ($ this)) "active"))
              :placeholder "Поиск..."}
             :search)
            [:div.search-icon]]]]
         [:div.navbar.navbar-border
          [:div.fixed-height-7px.col-xs-3.col-sm-3.col-md-2.col-lg-2.light-green-background]
          [:div.fixed-height-7px.col-xs-3.col-sm-3.col-md-2.col-lg-2.blue-background]
          [:div.fixed-height-7px.col-xs-3.col-sm-3.col-md-2.col-lg-2.yellow-background]
          [:div.fixed-height-7px.col-xs-3.col-sm-3.col-md-2.col-lg-2.orange-background]
          [:div.fixed-height-7px.hidden-xs.hidden-sm.col-md-2.col-lg-2.dark-green-background]
          [:div.fixed-height-7px.hidden-xs.hidden-sm.col-md-2.col-lg-2.pink-background]]]
        (when (seq menu)
          [:nav.navmenu.navmenu-container.fixed-left
           [:ul menu]])
        [:section.content-block.form.ng-cloak
         [:div.controls-block controls]
         body]
        (render-singletons)]]])))

(defn common-admin-layout[controls & body]
  (base-admin-layout
   (list
    (with-submenu "Склад" :inventory-icon
      ["/admin/procurements/" "Закупки"]
      ["/admin/arrivals/" "Приходы"]
      ["/admin/inventories/" "Переучеты"]
      ["/admin/stocks/" "Список товаров"])
    (with-submenu "Магазин" :store-icon
                  ["/admin/orders/" "Заказы"]
                  ["/admin/users/" "Клиенты"]
                  ["/admin/page/" "Страницы"]
                  ["/admin/news/" "Новости"]
                  ["/admin/blogs/" "Блог"]
                  ["/admin/review/" "Отзывы"]
                  ["/admin/settings/" "Настройки"])
    (with-submenu "Аналитика" :analytic-icon
      ["/admin/analytics/sales/" "Отчет о продажах"]
      ["/admin/analytics/left-stocks/" "Отчет об остатках"])
    (with-submenu "Словарь" :hndbook-icon
      ["/admin/categories-editor/" "Категории"]
      ["/admin/dictionary/" "Параметры"])
    (when (sec/root?)
      (with-submenu "Система" :hndbook-icon
        ["/admin/system/partners/" "Список клиентов"]))
    (with-popup-submenu "Создать" :add-icon
      [:procurement "Создать Закупку" :create-doc-icon]
      [:arrival "Создать Приход" :create-doc-icon]
      [:inventory "Создать Переучет" :create-doc-icon]
      [:stock "Создать Товар" :create-stock-icon]
      [:user "Создать Пользователя" :create-user-icon]
      [:order "Создать Заказ" :create-order-icon]))
   controls
   body))

(defn copywriter-admin-layout[controls & body]
  (base-admin-layout
    (list
      (with-submenu "Склад" :inventory-icon
                           [ "/copywriter/stocks/" "Список товаров"])
      (with-popup-submenu "Создать" :add-icon
                           [:stockcopy "Создать Товар" :create-stock-icon])
      )
    controls
    body))


(defn seo-admin-layout[controls & body]
  (require-css :seo :seo)
  (base-admin-layout
   (list
    (menu-item "Категории" :hndbook-icon "/seo/categories-editor/")
    (menu-item "Новости" :hndbook-icon "/seo/news/")
    (menu-item "Блог" :hndbook-icon "/seo/blogs/")
    (with-submenu "Страницы" :hndbook-icon
      ["/seo/page/" "Страницы контента"]
      ["/seo/category-page/" "Страницы категорий"])
    (menu-item "CSS" :hndbook-icon "/seo/css/")
    (menu-item "Redirects" :hndbook-icon "/seo/redirect/"))
   controls
   body))

(pre-route "/retail/seo/*" {} (when-not (sec/seo?) (status 404 nil)))
(pre-route "/retail/copywriter/*" {} (when-not (sec/copywriter?) (status 404 nil)))
(pre-route "/retail/admin/*" {}  (when-not (sec/admin?) (status 404 nil)))

(defn search-callback[function]
  (javascript-tag (js (set! searchCallback (fn[] (clj function))))))