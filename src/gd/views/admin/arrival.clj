(ns gd.views.admin.arrival
  (:use gd.model.model
        gd.views.admin.components
        gd.views.admin.dictionary
        gd.views.admin.arrival-model
        gd.views.admin.group-stock-model
        [clojure.algo.generic.functor :only (fmap)]
        gd.views.admin.common
        gd.views.admin.stocks
        gd.model.context
        gd.views.messages
        hiccup.def
        clojure.data.json
        clojure.test
        gd.views.components
        gd.views.admin.stock-model
        gd.utils.web
        gd.utils.db
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [clojure.set :as set])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Views for stock arrival

(defn- param-to-price-from-arrival[stock-arrival-parameter]
  (fmap
   (fn[parameters]
     (:price (last (sort-by :fkey_stock-arrival parameters))))
   (group-by :parameters (apply concat (vals stock-arrival-parameter)))))

(deftest arrival-merging-to-price-test
  (is
   (=
    {{:size "xl"} 50, {:size "m"} 200}
    (param-to-price-from-arrival
     {1 [{:id 1 :fkey_stock-arrival 1 :parameters {:size "m"} :price 100}
         {:id 1 :fkey_stock-arrival 1 :parameters {:size "xl"} :price 50}]
      2 [{:id 1 :fkey_stock-arrival 2 :parameters {:size "m"} :price 200}]})))

  (is
   (=
    {{:size "xl"} 50 {:size "m"} 100}
    (param-to-price-from-arrival
     {1 [{:id 1 :fkey_stock-arrival 1 :parameters {:size "m"} :price 100}
         {:id 1 :fkey_stock-arrival 1 :parameters {:size "xl"} :price 50}]
      2 []}))))

(defn process-arrival-from-params[{:keys [params]}]
  (map (fn[[_ {:keys [quantity price params]}]]
         (merge
          {:parameters params}
          (when (seq quantity) {:quantity (parse-int quantity)})
          (when (seq price) {:price (parse-int price)})))
       params))

(defn stock-arrival-modal[{:keys [id stock-arrival-parameter params name images context stock-variant] :as stock}]
  (let [save-without-close
        (js*
         (fn[]
           (set! closeDialogOnSuccess false)
           (.click (in-dialog ".action-button [onclick]"))))]
    (modal-window-admin
     (str "Приход товара: " name)
     (modal-save-panel
      (small-ok-button
       "Сохранить"
       :width 120
       :action (execute-form* save-stock-arrival[]
                              (arrival:save-quantity-to-session
                               *id (process-arrival-from-params param-map))
                              :success (js*
                                        (if (.. window closeDialogOnSuccess)
                                          (closeDialog))
                                        (set! closeDialogOnSuccess true)
                                        (refreshTabs)
                                        (clj (multi-rerender :arrival-stock-editing id))))))
     (list
      (when (sec/partner?)
        [:input.right.input.input-text
         {:style "margin-top:-42px; margin-right:65px; width:120px;"
          :placeholder "Цена прихода"
          :onkeypress (js (if (== event.which 13)
                            (do
                              (.val ($dialog "[id*=price]") (get-val this))
                              (recomputePricePercentage))))}])

      [:div.admin-arrival-form.group.form.relative
       [:div.modal-left-arrow.absolute
        {:style "top:130px; left:-50px;"
         :onclick (js (pagerLeftItem (clj save-without-close) recomputePricePercentage))}]
       [:div.modal-right-arrow.absolute
        {:style "top:130px; right:-50px;"
         :onclick (js (pagerRightItem (clj save-without-close) recomputePricePercentage))}]

       [:div.group
        [:img.left {:src (images:get :stock-large (first images))
                    :style "margin:10px 0 10px 10px;"}]
        [:div.admin-arrival-managed-list.left
         (let [arrival-params
               (or (arrival:get-quantity-from-session id)
                   (get stock-arrival-parameter (current-arrival)))

               last-used-price
               (param-to-price-from-arrival stock-arrival-parameter)]
           [:div.managed-list-component
            [:ul.group.managed-list
             (with-group :params
               (map!
                (fn[params]
                  (with-group (gen-client-id)
                    (let [{:keys [price quantity]} (find-by-val :parameters params arrival-params)]
                      [:li.fixed-group.current-item.well.well-sm
                       [:div.admin-arrival-parameter-container
                        (when (sec/partner?)
                          (list
                           [:div.admin-arrival-stock-price.text-center
                            [:span.bono-price.text-center
                             (or (->>
                                  (get stock-variant params)
                                  (find-by-val :context :wholesale)
                                  :price)
                                 0)]
                            [:span " грн."]]
                           [:div.admin-arrival-markup-percentage.text-center
                            [:span "("]
                            [:span.percentage-markup]
                            [:span "%)"]]))
                        (with-group :params
                          (map! (fn[[name value]] (hidden-field name value)) params))
                        [:p.admin-arrival-size {} (draw-variant params)]
                        [:div.admin-arrival-quantity
                         (text-field {:class "input input-text"}
                                     :quantity (or quantity 0))]
                        (when (sec/partner?)
                          [:div.admin-arrival-incoming-price
                           (text-field {:class "input input-text" :onkeyup (js (recomputePricePercentage))}
                                       :price (or price (get last-used-price params) 0))])]])))
                (distinct (concat (keys stock-variant) (map :parameters arrival-params)))))]])]]]))))

(defn arrival-multiple-edit-modal [ids {:keys [name stock-variant]}]
  [:div#arrival-multiple-edit-modal
   (let [save-without-close
         (js*
           (fn[]
             (set! closeDialogOnSuccess false)
             (.click (in-dialog ".action-button [onclick]"))))]
     (modal-window-admin name
       (modal-save-panel
         (small-ok-button
           "Сохранить"
           :width 120
           :action (execute-form* save-multiple-stock-arrival[]
                     (arrival:save-multiple-arrivals
                       ids (process-arrival-from-params param-map))
                     :success (js*
                                (set! closeDialogOnSuccess true)
                                (if (.. window closeDialogOnSuccess)
                                  (closeDialog))
                                (refreshTabs)
                                (clj (for [id ids] (multi-rerender :arrival-stock-editing id)))
                                (set! selectedItems [])))))
       (list
         (when (sec/partner?)
           [:input.right.input.input-text
            {:style "margin-top:-42px; margin-right:65px; width:120px;"
             :placeholder "Цена прихода"
             :onkeypress (js (if (== event.which 13)
                               (do
                                 (.val ($dialog "[id*=price]") (get-val this))
                                 (recomputePricePercentage))))}])
         [:div.admin-arrival-form.group.form.relative
          [:div.group
           [:div.managed-list-component
            [:ul.group.managed-list
             (with-group :params
               (map!
                 (fn[params]
                   (with-group (gen-client-id)
                     [:li.fixed-group.current-item.well.well-sm
                      [:div.admin-arrival-parameter-container
                       (with-group :params
                         (map! (fn[[name value]] (hidden-field name value)) params))
                       [:p.admin-arrival-size {} (draw-variant params)]
                       [:div.admin-arrival-quantity
                        (text-field {:class "input input-text" :placeholder "Кол-во"}
                          :quantity)]
                       [:div.admin-arrival-incoming-price
                        (text-field {:class "input input-text" :onkeyup (js (recomputePricePercentage)) :placeholder "Цена"}
                          :price)]]]))
                 stock-variant))]]]])))])

(defn arrival-add-stock[id]
  (cljs
   (remote* mark-stock-arrived
            (arrival:mark-for-processing (s* id))
            :success (fn[]
                       (refreshTabs)
                       (clj (multi-rerender :arrival-stock-editing id))))))

(defn arrival-remove-stock[id]
  (cljs (remote* arrive-remove-stock
                 (let [stock (s* id)]
                   (if (arrival:marked? stock)
                     (arrival:remove-mark stock)
                     (arrival:save-quantity-to-session stock :removed))
                   nil)
                 :success (fn[]
                            (refreshTabs)
                            (organizerStockPagesReload)))))

(defn arrival-stock-template[{:keys [name images price id album fkey_category
                                     stock-arrival-parameter all-params]
                              :as stock}]
  (let [stored-arrival (get stock-arrival-parameter (current-arrival))
        arrival-parameters (or (arrival:get-quantity-from-session id) stored-arrival)
        stock-arrival-sum (reduce +
                                  (map (fn[price quantity] (* (or price 0) (or quantity 0)))
                                       (map :price arrival-parameters)
                                       (map :quantity arrival-parameters)))]
    [:tr {:data-id id}
     {:data-title (tooltip [:div "Сумма по товару: " stock-arrival-sum])}
     (javascript-tag
      (js (var recomputePricePercentage
               (fn[]
                 (.each ($ ".ui-dialog .current-item")
                        (fn [i el]
                          (var bonoPrice (.text (.find ($ this) ".bono-price")))
                          (var incomingPrice (.val (.find ($ this) "[name*=price]")))
                          (var percentage (. Math round (* (/ (- bonoPrice incomingPrice) incomingPrice) 100)))
                          (.text (.find ($ el) ".percentage-markup") percentage)))))))

     [:td
      (hidden-field :id id)
      (custom-checkbox {:role :selection} :choose false)]
     [:td.inline-block {:style "height:64px;"}
      (sized-image {:style "margin:5px 0 0 -5px;"
                    :class "organizer-stock-list-image"
                    :data-title (tooltip (image (images:get :stock-large (first images))))}
                   (images:get :stock-medium (first images)))]
     [:td name]
     [:td (get-brand all-params)]
     [:td (draw-category fkey_category)]
     [:td.text-left
      (if (seq arrival-parameters)
        (map (fn[{:keys [parameters quantity price]}]
               [:div.parameter-tag.left
                [:div.size (draw-variant parameters)]
                [:div.quantity quantity "шт."]
                (when (sec/partner?)
                  [:div.price (when (or (nil? price) (= (long price) 0))
                                {:style "color:black;"})
                   (or price "нет")])])
             arrival-parameters)
        [:p.text-left "Нет информации о добавленых размерах"])]
     [:td
      (cond
        (and stored-arrival (arrival:removed? id))
        [:a.link {:onclick (arrival-add-stock id)}
         "Восстановить"]

        (or (arrival:marked? id) stored-arrival)
        [:div
         [:a.link {:onclick (js
                             (set! closeDialogOnSuccess true)
                             (clj (modal-window-open*))
                             (recomputePricePercentage))}
          "Редактировать" (stock-arrival-modal stock)]
         [:a.link {:onclick (arrival-remove-stock id)}
          "Удалить"]]

        true
        [:a.link {:onclick (arrival-add-stock id)}
         "Добавить"])]]))

(defmulti arrival-filter-on-save
  "Dispatch on type of arrival"
  (fn[{:keys [type]} params] (keyword type)))

(defmethod arrival-filter-on-save :arrival [arrival params]
  (fmap (fn[stock-params]
          (if (= :removed stock-params)
            []
            (filter (fn[{:keys [quantity]}] (and quantity (> quantity 0))) stock-params)))
        params))

(defn arrival:save-all[]
  (let [arrival (arrival:get-arrival-by-id (current-arrival))]
    (arrival:save-multiple-stock-arrivals
     (current-arrival) (arrival-filter-on-save arrival (current-arrival-session)))
    (reset-current-arrival-session)))

(defn arrival-edit-panel[]
  [:div.none
   [:div.save-pending-changes.alert.alert-danger.margin15
    [:div.table-cell.padding10r "Сохранить сделанные изменения?"]
    [:div.table-cell.padding10r
     (small-danger-button
      "Сохранить"
      :action (cljs (remote* arrival-save-pending
                             (arrival:save-all)
                             :success (fn[]
                                        (refreshTabs "all")
                                        (clj (rerender :retail-admin-arrival-sum))))))]

    [:div.table-cell.padding10r
     (small-cancel-button
      "Отменить"
      :action (cljs (remote* arrival-reject-pending
                             (reset-current-arrival-session)
                             :success (fn[] (refreshTabs "all")))))]]])

(defn arrived-stock-list[]
  [:div.admin-stock-list
   (pager supplier-stocks-arrival
          (modify-result
           (supplier-stocks-with-filters false page-size)
           (fn[res] (concat [:header] res)))
          (fn[entity]
            (cond
              (= entity :header)
              (precompile-html
               [:tr.admin-table-head
                [:td.fixed-25px-width-cell
                 (custom-checkbox {:onchange (js (selectAllOnPage this))} :choose false)]
                [:td.fixed-50px-width-cell "Фото"]
                [:td.fixed-200px-width-cell "Название"]
                [:td.fixed-150px-width-cell "Бренд"]
                [:td.fixed-200px-width-cell "Категория"]
                [:td.full-width-cell.text-left "Параметры"]
                [:td.fixed-100px-width-cell "Операции"]])

              true
              (let [{:keys [id] :as stock} entity]
                (inplace-multi-region
                 arrival-stock-editing id []
                 (let [stock (region-request arrival-stock-editing (inplace:get-stock-with-changes (s* id)) stock)]
                   (arrival-stock-template stock))))))
          :page 10
          :columns 1
          :custom-rows-mode true
          :register-as organizer-stock-pages
          :pager-title "Список товаров")])

(defn arrival-stock-creation[]
  [:div.admin-toggle-block.group
   [:div.admin-filter-panel
    [:div.group.organizer-stock-filter-block.fout-hidden
     [:div.admin-filters-panel.left
      [:div.group.organizer-stock-filter-block.fout-hidden.admin-filters
       (stock-filters)]]
     [:div.left
      [:div.admin-create-stock-btn
       (small-cancel-button "Создать товар" :width 145 :action (create-panel :stock))]]
     [:div.left.margin5r
      (small-cancel-button
        "Редактирование"
        :action
        (js (.done (clj (rerender :arrival-group-set))
              (fn[]
                (openDialog "#arrival-multiple-edit-modal" true)))))]]]])

(defn arrival-general-info-panel[]
  (let [{:keys [parameters created]} (arrival:get-arrival-by-id (current-arrival))]
    [:div

     (search-callback (js* (organizerStockPagesLoadPage 0)))

     (region* retail-admin-arrival-sum[]
       [:div.panel.transparent-panel.padding15.margin15
        [:p.table-cell.padding5r "Название"]
        [:div.table-cell.padding25r
         (text-field {:class "input input-text" :disabled true} :name (:name parameters))]
        [:p.table-cell.padding5r "Поставщик"]
        [:div.table-cell.padding25r
         (text-field {:class "input input-text" :disabled true} :supplier (:supplier parameters))]
        [:p.table-cell.padding5r "Дата"]
        [:div.table-cell.padding25r
         (text-field {:class "input input-text" :disabled true} :created (format-date created))]
        [:p.table-cell
         "Сумма прихода: " (:sum (arrival:get-arrival-sum (current-arrival))) " грн."]])]))

(defpage "/retail/admin/arrival/:id/" {id :id}
  (require-js :admin :stock)
  (common-admin-layout
      (list
       (add-var-to-global-context :arrival id)
       (arrival-stock-creation))
    [:div.admin.group.admin-panel.retail-standard-panel
     (register-modal-navigation "organizer-stock-pages")

     [:form#group-operations]
     (arrival-edit-panel)
     (arrival-general-info-panel)
     (inplace-statistics-panel-with-tabs
       :arrival-tab
       {:name "Все товары" :value "all"}
       {:name "Товары в приходе" :value "arrived" :count #(arrival:count-arrived)})
     (inplace-change-callbacks)
     (region* arrival-group-set[]
       [:div
        (if-not (nil? (c* selectedItems))
          (arrival-multiple-edit-modal (map parse-int (c* selectedItems)) (prepare-group-arrival (group-of-stocks (c* selectedItems)))))])

     (arrived-stock-list)]))

(run-tests)
