(ns gd.views.admin.forum
  (:use gd.model.model
        hiccup.def
        com.reasonr.scriptjure
        gd.views.admin.common
        clojure.data.json
        gd.views.components
        gd.views.admin.stock-model
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [gd.views.security :as sec]))

(defn- forum-breadcrumbs[id]
  (when id
    (interpose "/"
               (cons
                [:a {:href "/forum/"} "Все темы"]
                (map (fn[{:keys [id name]}] [:a {:href (str "/forum/" id "/")} name])
                     (forum:breadcrumbs id))))))

(defn- forum-themes[id]
  (let [theme-id (or id 0)]
    (base-admin-layout
     nil
     [:div
      (small-ok-button "Создать тему" :action (modal-window-open))
      (modal-window-admin
       "Создание новой темы"
       (modal-save-panel
        (small-ok-button
         "Создать"
         :action (execute-form* create-forum-theme[name message editable description category]
                                (let [theme-id
                                      (let [theme-id *theme-id]
                                        (if (zero? theme-id) nil theme-id))]
                                  (if editable
                                    (forum:add-theme-with-first-message
                                      name message (sec/logged)
                                      :parent theme-id
                                      :description description
                                      :category category)
                                    (forum:add-parent-theme
                                     name (sec/logged)
                                     :parent theme-id
                                     :description description
                                     :category category))))))
       [:div.form
        [:div {:style "padding:5px 0;"}
         [:div
          (custom-checkbox :editable)
          "Editable"]
         [:div
          (text-field {:placeholder "Название" :class "signin-modal-input input input-text"} :name)]
         [:div
          (text-field {:placeholder "Сообщение" :class "signin-modal-input input input-text"} :message)]
         [:div
          (text-area {:placeholder "Описание" :class "signin-modal-input input input-text"} :description)]
         [:div
          (text-area {:placeholder "Категория" :class "signin-modal-input input input-text"} :category)]]])]
     [:div.group

      (forum-breadcrumbs id)

      (pager forum-themes
             (modify-result
              (let [theme-id (s* theme-id)]
                (if (zero? theme-id)
                  (forum:top-themes)
                  (forum:theme-childs theme-id)))
              (fn[res](concat [:header] res)))
             (fn[{:keys [name editable message messages-count childs-count id] :as theme}]
               (cond (= theme :header)
                     [:tr.admin-table-head
                      [:td.fixed-25px-width-cell "Names"]
                      [:td.fixed-50px-width-cell "Topics"]
                      [:td.fixed-150px-width-cell "Posts"]
                      [:td.fixed-100px-width-cell "Last post"]]

                     true
                     [:tr
                      [:td [:a {:href (str "/forum/" id "/")} name]]
                      [:td childs-count]
                      [:td messages-count]
                      [:td (:text message)]]))
             :columns 1
             :page 10
             :custom-rows-mode true)])))

(defn- page-for-message[id]
  (dec (forum:get-page-by-message-id id)))

(defn- offset-for-message[id]
  (* (page-for-message id) 10))

(defn- reply-modal[theme-id & [reply-to]]
  (modal-window-admin
   "Создание нового сообщения"
   (modal-save-panel
    (small-ok-button
     "Создать"
     :action (execute-form* create-forum-message[message reply-to]
                            (let [{:keys [id]} (forum:add-message *theme-id message
                                                                  (sec/logged)
                                                                  (parse-int reply-to))]
                              (offset-for-message id))
                            :success (js* (forumMessagesLoadPage (parseInt result))
                                          (closeDialog)))))
   [:div.form
    [:div {:style "padding:5px 0;"}
     (hidden-field :reply-to reply-to)
     (text-area {:placeholder "Сообщение" :class "signin-modal-input input input-text"} :message)]]))

(defn- forum-messages[id]
  (require-js :admin :forum)
  (base-admin-layout
   nil
   [:div
    (small-ok-button "Создать сообщение" :action (modal-window-open))
    (reply-modal id)]
   [:div.group

    (forum-breadcrumbs id)

    (pager forum-messages
           (modify-result
            (forum:theme-messages (s* id) page-size (c* offset))
            (fn[res](concat [:header] res)))
           (fn[{:keys [id text sent-time user recipient message reply-to] :as msg}]
             (cond (= msg :header)
                   [:tr.admin-table-head
                    [:td.fixed-25px-width-cell "Отправлено"]
                    [:td.fixed-25px-width-cell "Ответ на"]
                    [:td.fixed-50px-width-cell "Пользователь"]
                    [:td.fixed-150px-width-cell "Текст"]]

                   true
                   (let [theme-id (:id recipient)
                         reply (:id reply-to)]
                     [:tr {:data-id id}
                      [:td (format-timestamp sent-time)]
                      [:td (when reply
                             [:a {:href (str "/forum/" theme-id "/page" (inc (page-for-message reply)) "#" reply)}
                              reply])]
                      [:td (:name user)]
                      [:td text]
                      [:td
                       [:a {:onclick (modal-window-open)} "Ответить"]
                       (reply-modal theme-id id)]])))
           :columns 1
           :page 10
           :custom-rows-mode true)]))

(defpage "/smartcommerce/forum/:id/" {id :id}
  (let [id (parse-int id)]
    (if (forum:theme-editable? id)
      (forum-messages id)
      (forum-themes id))))

(defpage "/smartcommerce/forum/" {}
  (forum-themes nil))
