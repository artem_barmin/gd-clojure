(ns gd.views.admin.components
  (:use gd.views.resources
        gd.views.admin.common
        gd.views.admin.arrival-model
        gd.views.admin.stock-model
        [clj-time.core :exclude (extend)]
        clj-time.format
        [clojure.algo.generic.functor :only (fmap)]
        gd.views.bucket.simple
        compojure.core
        gd.model.model
        gd.model.context
        gd.utils.web
        gd.utils.common
        gd.views.messages
        gd.views.components
        noir.core
        [noir.options :only (dev-mode?)]
        [noir.response :only (redirect status set-headers)]
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defn- prepare-user[{:keys [id name address address-region]}]
  {:value id :label (str "#" id " : " name "(" address ")") :address (str address " " address-region)})

(defn user-autocomplete[user]
  [:div {:ng-init (js (set! selectedUser (clj (when user (prepare-user user)))))}
   (javascript-tag
    (js (var userAutocomplete
             (clj (callback [search]
                            (remote* user-autocomplete
                                     (let [search-string (c* search)]
                                       (when (seq search-string)
                                         (map
                                          prepare-user
                                          (user:get-all-users 10 0 :search-string search-string))))))))))
   (text-field {:class "none" :ng-model "selectedUser.value"} :user)
   (text-field {:class "input input-text"
                :ng-model "selectedUser.label"
                :data-html "1"
                :typeahead-on-select (js (set! selectedUser $item))
                :data-animation "am-flip-x"
                :ng-options "user as user.label for user in users($viewValue)"
                :data-min-length 0
                :bs-typeahead true}
               :author)])

(defn- inplace-statistics-panel-with-tabs-common[current-val tab-definitions & {:keys [force state-name]}]
  (let [stored-tab
        (inplace:get-additional-state (keyword state-name))

        tab-definitions
        (map (fn[{:keys [name value count]}] {:name name
                                              :value value
                                              :count (if count (count) 0)})
             tab-definitions)

        [first-tab]
        (map :value tab-definitions)

        option
        (fn[name value count]
          (custom-radio-button
           {:onchange (js
                       (processAdditionalState (clj state-name) (clj value))
                       (organizerStockPagesLoadPage 0))}
           :tab-state value
           [:div.admin-inplace-edit-tabs.left name
            (when (and (number? count) (> count 0)) (list "(" count ")"))]
           (= value (or force stored-tab current-val first-tab))))]
    [:div.group
     (javascript-tag (js (var statisticTabs
                              (clj (merge (mreduce (fn[{:keys [value count]}] {value count}) tab-definitions)
                                          (when (current-arrival)
                                            {:arrivalEmpty (arrival:empty?)}))))))
     (map (fn[{:keys [name value count]}] (option name value count)) tab-definitions)]))

(defn inplace-statistics-panel-with-tabs[state-name & tab-definitions]
  (let [state-name (name state-name)]
    [:div.admin-inplace-edit-tabs.alert.alert-success {:style "margin:15px 15px 0 15px;" :role :tabs-panel}
     (region* retail-admin-tabs-with-statistics[force]
       (inplace-statistics-panel-with-tabs-common
         (c* (get-val "[name=tab-state]:checked")) tab-definitions
         :force (c* force)
         :state-name (s* state-name)))]))
