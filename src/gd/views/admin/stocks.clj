(ns gd.views.admin.stocks
  (:use gd.model.model
        gd.utils.pdf
        gd.views.admin.group-stock-model
        gd.views.admin.stock
        gd.views.admin.components
        gd.views.admin.dictionary
        [clojure.algo.generic.functor :only (fmap)]
        gd.model.context
        gd.views.admin.common
        gd.views.messages
        hiccup.def
        clojure.data.json
        gd.views.components
        gd.views.admin.stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session])
  (:require [noir.response :as response]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils

(defn draw-category[category-id]
  (clojure.string/join
   " → "
   (or (seq (reverse (map :name (category:parents category-id)))) ["Другая"])))

(defn draw-category-name[category-id]
  (or (first (map :name (category:parents category-id)))
      "Другая"))

(defn get-brand[params]
  (first (get params "brand")))

(defn draw-variant[variant-params]
  (if-let [params (seq (map second (sort-by first variant-params)))]
    (interpose " " params)
    [:span {:style "color:#36A6E6;"} "нет"]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Components
(defn inplace-change-callbacks[]
  [:div
   (javascript-tag
    (js
     (var processAdditionalState
          (clj (callback
                [type value]
                (remote* track-additional-state-changes
                         (inplace:store-additional-state (c* type) (c* value))))))))])

(defn stock-merge[]
  [:div#merge-stocks-tooltip.none.alert.alert-info.absolute {:style "z-index:100;"}
   [:a.link
    {:onclick
     (cljs (remote* group-merge-stock
                    (inplace:merge-stocks (group-of-stocks (c* selectedItems)))
                    :success  (fn[]
                                (organizerStockPagesReload)
                                (deselectAll))))}
    "Слить выбранные товары"]])

(defn prepare-stock-filters[search filters-form]
  (let [parse-tag (fn [value]
                    (let [value (string-converter value)]
                      (if-let [[_ value] (and value (re-find #"not\[(.*)\]" value))]
                        {:inverse value}
                        value)))

        parse-sale (fn[value]
                     (let [value (keyword-converter value)]
                       (case value
                         :sale {:context-status {:wholesale-sale :visible
                                                 :wholesale :visible}}
                         :not-sale {:context-status {:wholesale-sale :invisible
                                                     :wholesale :visible}}
                         nil)))]
    (merge
     {:search-string (and (seq search) {:flags [:w] :str search})}
     (parse-sale (:status filters-form))
     (-> filters-form
         (update-in [:category] int-converter)
         (update-in [:filters] (partial fmap string-converter))
         (update-in [:tags] (fn[tag] (remove nil? [(parse-tag tag)])))))))

(defmacro supplier-stocks-with-filters[only-id & [page-size]]
  `(inplace:all-stocks
    ~page-size
    ~(if only-id nil `(c* offset))
    (merge
     (prepare-stock-filters
      (c* (get-val "[name=search]"))
      (c* (formState "[role=main-filters]")))
     {:only-id ~only-id
      :inline-state (:tab-state (c* (formState "[role=tabs-panel]")))})))

(defn stock-filters-block[current-state]
  (let [filters (retail:get-stock-params)]
    [:div.group
     [:div.admin-filter-holder.red-dropdown.left.margin5r
      (small-ok-button "Фильтры" :action (js (openParamFilters this)))
      (javascript-tag (js (set! filtersAvailable (clj (dissoc filters "brand")))))
      (add-singleton
       [:div.absolute.none {:style "z-index:50;" :role :param-filters}
        [:div.panel.shadow-panel.margin5t {:ng-controller "FiltersConstraintCtl" :role :main-filters}

         [:input.input.input-text.form-control {:placeholder "Фильтрация" :ng-model "search" :ignore true}]

         [:div.panel-body {:style "width:600px;max-height:800px;overflow:auto;"}
          [:div.left {:ng-repeat "(name,values) in filtered"}
           [:b {:once-text "filterName(name)"}]
           [:div {:ng-repeat "value in values"}
            (custom-checkbox {:once true
                              :once-attr-value "value"
                              :once-attr-name "'filters[' + name + ']'"}
                             nil false nil)
            [:span.margin5l.margin10r {:once-text "value"}]]]]]])]

     (with-group :filters
       [:div.admin-filter-holder.green-dropdown.left.margin5r
        (custom-dropdown :brand (cons ["Бренд" "null"] (filters "brand"))
                         :selected (get current-state :brand))])

     [:div.admin-filter-holder.red-dropdown.left.margin5r
      (custom-dropdown :status [["Распродажа" "null"]
                                ["В распродаже" :sale]
                                ["Не в распродаже" :not-sale]])]

     [:div.admin-filter-holder.left.admin-categories-filter.margin5r.stock-category-form.orange-dropdown
      (custom-dropdown {:style "width:220px;"}
                       :category (cons ["Категория" "null"]
                                       (map (fn[cat] (update-in cat [2] dissoc :disabled))
                                            (category-dropdown 0 (category:tree)))))]]))

(defn stock-filters[]
  (region* organizer-stock-filters[]
    [:div {:role :main-filters}
     (stock-filters-block (c* (formState "[role=main-filters]")))]))

(defn- retail-massive-selection[]
  (region* massive-selection[]
    [:div
     [:div.left.margin5r
      (small-cancel-button "Выбрать все"
                           :width 120
                           :action (js (var stocks (clj (map :id (supplier-stocks-with-filters true))))
                                       (selectAll stocks)))]
     [:div.left.margin5r
      (small-cancel-button "Сбросить все"
                           :width 120
                           :action (js (deselectAll)))]]))

(defn add-stock[]
  [:div.stock-creation-container
   (small-cancel-button "Добавить фото"
                        :action (js (.click (get-section ".stock-creation-container" ".file-upload > input"))))

   [:div.none.form
    (multi-file-upload
     :retail-stock-upload
     (form-for-file-json* retail-stock-upload stock-image-sizes
                          (inplace:store-new-stock code)
                          :sizes-to-wait [:stock-medium])
     :js-options {:limitConcurrentUploads 5
                  :progressall (js* (fn[e data]
                                      (var progress (* 100 (/ data.loaded data.total)))
                                      (.css ($ "#load-progress") "width" (str progress "%"))))
                  :stop (js* (fn[] (refreshTabs "new")))})]])

(defn inplace-edit-panel[]
  [:div.none
   [:div.save-pending-changes.alert.alert-danger.margin15
    [:div.table-cell.padding10r "Сохранить сделанные изменения?"]
    [:div.table-cell.padding10r
     (small-danger-button
      "Сохранить"
      :action (cljs (remote* inline-edit-save-pending
                             (inplace:save-all)
                             :success (fn[]
                                        (clj (rerender :organizer-stock-filters
                                                       (js* (fn[](refreshTabs "all")))))))))]

    [:div.table-cell.padding10r
     (small-cancel-button
      "Отменить"
      :action (cljs (remote* inline-edit-reject-pending
                             (inplace:reject-all)
                             :success (fn[] (refreshTabs "all")))))]]])

(defn get-last-arrival[stock-arrival-parameter]
  (let [arrivals (map :stock-arrival (apply concat (vals stock-arrival-parameter)))]
    (when-let [arrivals (seq (filter (comp (partial = :arrival) :type) arrivals))]
      (reduce max (map :id arrivals)))))

(defn images-tooltip[images]
  (tooltip [:table.margin5b
            [:tr
             (map (fn[img]
                    [:td (image (images:get :stock-large img))])
                  images)]]))

(defn params-tooltip[params]
  (map (fn[[name values]] [:div name ":" values]) params))

(defn retail-stock-template[{:keys [name images id category  status
                                     fkey_category state params all-params
                                     visits left-stocks stock-arrival-parameter
                                     tags stock-variant]
                              :as stock}]
  [:tr {:data-id id
        :class (when (= state :dirty) :inline-state-dirty)}
   [:td
    (hidden-field :id id)
    (custom-checkbox {:role :selection} :choose false)]
   [:td
    (sized-image {:class (list "organizer-stock-list-image" (when (is-sale? stock) " is-sale"))
                  :data-title (images-tooltip images)}
                 (images:get :stock-medium (first images)))]
   [:td {:data-title (tooltip (params-tooltip all-params))} name]
   [:td
    (hidden-field  :fkey_category fkey_category)
    [:div
     {:data-title
      (tooltip
       [:div {:style "padding:0 5px 5px; font-size:10px !important;"}
        [:div (draw-category fkey_category)]])}
     [:div (draw-category-name fkey_category)]]]
   [:td
    (let [last-arrival (get-last-arrival stock-arrival-parameter)]
      (if (seq stock-variant)
        [:div.group {}
         (map (fn[[params contexts]]
                (let [requested-stocks (or (get left-stocks params) 0)
                      final-price (fn[type] (or (:price (find-by-val :context type contexts)) 0))]
                  (precompile-html
                   [:div.stock-tag-group.left
                    {:data-title
                     (tooltip
                      [:div {:style "padding:0 5px 5px; font-size:10px !important;"}
                       (when (sec/partner?)
                         (when-let [price
                                    (->>
                                     (get stock-arrival-parameter last-arrival)
                                     (find-by-val :parameters params)
                                     :price)]
                           [:div "Последняя цена прихода: " ^Number price " грн."]))
                       [:div "Розничная цена: " ^Number (final-price :retail) " грн."]
                       [:div "Акционная цена: " ^Number (final-price :wholesale-sale) " грн."]])}
                    [:span.stock-tag.admin-stock-size {} (draw-variant params)]
                    [:span.stock-tag.admin-stock-quantity {} requested-stocks " шт."]
                    [:span.stock-tag.admin-stock-price {} (final-price :wholesale) ",00"]])))
              stock-variant)]
        [:div.text-left "Варианты не были добавлены"]))]
   [:td (or (get-brand all-params) "Не установлен")]
   [:td
    [:div.left
     (small-ok-button
      [:i.glyphicon.glyphicon-sm.glyphicon-pencil {:style "margin:0 -7px;"}]
      :width 32
      :action (cljs (remote* retail-admin-edit-stock (prepare-single-stock (s* id))
                             :success (fn[result]
                                       (setCurrentStock result)
                                       (openDialogAngular "#stock-modal-edit")
                                       (select-tab :main)))))]

    [:div.left.margin5l
     (small-danger-button
      [:i.glyphicon.glyphicon-sm.glyphicon-remove {:style "margin:0 -7px;"}]
      :width 32
      :action (js (if (confirm "Точно удалить товар?")
                    (clj (remote* retail-admin-delete-stock
                                  (let [id (s* id)]
                                    (if (inplace:new-stock? id)
                                      (inplace:remove id)
                                      (stock:soft-delete id)))
                                  :success (fn[]
                                             (refreshTabs)
                                             (organizerStockPagesReload)))))))]]])



(defn retail-stock-copy-template[{:keys [name images id category  status
                                    fkey_category state params all-params
                                    visits left-stocks stock-arrival-parameter
                                    tags stock-variant]
                             :as stock}]
  [:tr {:data-id id
        :class (when (= state :dirty) :inline-state-dirty)}
   [:td
    (hidden-field :id id)
    (custom-checkbox {:role :selection} :choose false)]
   [:td
    (sized-image {:class (list "organizer-stock-list-image" (when (is-sale? stock) " is-sale"))
                  :data-title (images-tooltip images)}
                 (images:get :stock-medium (first images)))]
   [:td {:data-title (tooltip (params-tooltip all-params))} name]
   [:td
    (hidden-field  :fkey_category fkey_category)
    [:div
     {:data-title
      (tooltip
        [:div {:style "padding:0 5px 5px; font-size:10px !important;"}
         [:div (draw-category fkey_category)]])}
     [:div (draw-category-name fkey_category)]]]
   [:td
    (let [last-arrival (get-last-arrival stock-arrival-parameter)]
      (if (seq stock-variant)
        [:div.group {}
         (map (fn[[params contexts]]
                (let [requested-stocks (or (get left-stocks params) 0)
                      final-price (fn[type] (or (:price (find-by-val :context type contexts)) 0))]
                  (precompile-html
                    [:div.stock-tag-group.left
                     {:data-title
                      (tooltip
                        [:div {:style "padding:0 5px 5px; font-size:10px !important;"}
                         (when (sec/partner?)
                           (when-let [price
                                      (->>
                                        (get stock-arrival-parameter last-arrival)
                                        (find-by-val :parameters params)
                                        :price)]
                             [:div "Последняя цена прихода: " ^Number price " грн."]))
                         [:div "Розничная цена: " ^Number (final-price :retail) " грн."]
                         [:div "Акционная цена: " ^Number (final-price :wholesale-sale) " грн."]])}
                     [:span.stock-tag.admin-stock-size {} (draw-variant params)]
                     [:span.stock-tag.admin-stock-quantity {} requested-stocks " шт."]
                     [:span.stock-tag.admin-stock-price {} (final-price :wholesale) ",00"]])))
              stock-variant)]
        [:div.text-left "Варианты не были добавлены"]))]
   [:td (or (get-brand all-params) "Не установлен")]
   [:td
    [:div.left
     (small-ok-button
       [:i.glyphicon.glyphicon-sm.glyphicon-pencil {:style "margin:0 -7px;"}]
       :width 32
       :action (cljs (remote* retail-admin-edit-stock (prepare-single-stock (s* id))
                              :success   (fn[result]
                                         (setCurrentStock result)
                                         (openDialogAngular "#stock-copy-modal-edit")
                                         (select-tab :main)
                                           )


                              )))]

    [:div.left.margin5l
     ]]])


(defn- flatten-seqs
  "Given a vector, return a vector with seqs
  and seqs-of-seqs flattened recursively.
  (seqs in sub-vectors will not be flattened.)"
  [xs]
  (vec
   (loop [xs xs]
     (if (some seq? xs)
       (recur (mapcat #(if (seq? %) % (list %)) xs))
       xs))))

(defn- export-to-pdf[stocks]
  (let [path (.getAbsolutePath (java.io.File/createTempFile "pdf" ""))]
    (generate-pdf
     path
     [:paragraph {:ttf-name "DejaVu Sans"}
      "Товары"
      (vec
       (concat
        [:pdf-table
         {:width-percent-number 100}
         [40 10 10 10]]
        (flatten-seqs
         (map
          (fn[{:keys [stock-variant left-stocks name] :as stock}]
            (let [filtered-variants
                  (remove
                   (fn[[params contexts]]
                     (zero? (or (get left-stocks params) 0)))
                   stock-variant)]
              (map-indexed
               (fn[i [params contexts]]
                 (let [requested-stocks (or (get left-stocks params) 0)
                       final-price (fn[type] (or (:price (find-by-val :context type contexts)) 0))]
                   (when-not (zero? requested-stocks)
                     (filterv
                      identity
                      [(when (zero? i)
                         [:pdf-cell {:valign :middle :rowspan (count filtered-variants)}
                          (str name " (" (get-brand (:all-params stock)) ")")])
                       [:pdf-cell {:valign :middle} (str (:size params))]
                       [:pdf-cell {:valign :middle}
                        (str requested-stocks " шт.")]
                       [:pdf-cell {:valign :middle} (str (final-price :wholesale) " грн.")]]))))
               filtered-variants)))
          stocks))))])
    (.replaceAll path "/tmp/" "")))

(defn admin-stock-filters[]
  [:div.admin-toggle-block.group
   [:div.admin-filter-panel
    [:div.group.organizer-stock-filter-block.fout-hidden
     [:div.admin-filters-panel.left
      [:div.group.organizer-stock-filter-block.fout-hidden.admin-filters
       (stock-filters)]]
     [:div.div.admin-filter-holder.red-dropdown.left.margin5r
      (custom-dropdown :actions [["Действия" "Действия"]
                                 ["Редактирование" "Редактирование"
                                  {:onclick (cljs (remote* admin-group-edit-stock-open
                                                           (prepare-group-stocks (group-of-stocks (c* selectedItems)))
                                                           :success (fn [result]
                                                                      (setCurrentGroupStock result)
                                                                      (openDialogAngular "#group-stock-modal-edit")
                                                                      (select-tab :main))))}]
                                 ["Создать товар" ""
                                  {:onclick (str "angular.element('#spinner-for-multiple-add').scope().setMultipleAdd(); " (create-panel :stock))}]
                                 ["Экспорт в PDF"
                                  ""
                                  {:onclick (js ((fn[]
                                                   (var offset 0)
                                                   (clj (remote* export-pdf
                                                                 (export-to-pdf
                                                                  (supplier-stocks-with-filters false))
                                                                 :success (fn [result]
                                                                            (.open window (str "/admin/pdf/" result) "_blank")))))))}]
                                 ["Добавить в  \"Новое\"" ""
                                  {:onclick (cljs (remote* set-stock-as-new (do (stock:set-as-new (map! parse-int (c* selectedItems))) nil)
                                                           :success (fn [result]
                                                                      (notify "Добавить в \"Новое\":" "Товары успешно добавлены!"))))}]])]
     (retail-massive-selection)]]])

(defn copywriter-stock-filters[]
  [:div.admin-toggle-block.group
   [:div.admin-filter-panel
    [:div.group.organizer-stock-filter-block.fout-hidden
     [:div.admin-filters-panel.left
      [:div.group.organizer-stock-filter-block.fout-hidden.admin-filters
       (stock-filters)]]
     [:div.div.admin-filter-holder.red-dropdown.left.margin5r]
     (retail-massive-selection)]]])


(defn stock-edit-dialog[]
  [:div#group-stock-modal-edit (modal-window-admin "Групповое редактирование товаров"
                                                   (modal-save-panel nil)
                                                   (group-stock-editing-modal))])

(defpage "/retail/admin/stocks/" {}
  (common-admin-layout (admin-stock-filters)
    [:div.admin.group
     [:div.group.admin-panel.retail-standard-panel
      [:div
       (stock-merge)
       (inplace-change-callbacks)
       (inplace-edit-panel)
       (inplace-statistics-panel-with-tabs
         :stock-tab
         {:name "Все товары" :value "all"}
         {:name "Добавленные" :value "new" :count #(inplace:count-new)}
         {:name "Измененные" :value "changed" :count #(inplace:count-changes)})

       (stock-edit-dialog)
       (search-callback (js* (deselectAll)
                             (organizerStockPagesLoadPage 0)
                             (clj (rerender :massive-selection))))

       [:div.admin-stock-list
        [:div.stocks-list
         (pager supplier-stocks
                (modify-result
                 (supplier-stocks-with-filters false page-size)
                 (fn[res](concat [:header] res)))
                (fn[entity]
                  (cond (= entity :header)
                        (precompile-html
                         [:tr.admin-table-head
                          [:td.fixed-25px-width-cell
                           (custom-checkbox {:onchange (js (selectAllOnPage this))} :choose false)]
                          [:td.fixed-50px-width-cell "Фото"]
                          [:td.fixed-150px-width-cell "Название"]
                          [:td.fixed-100px-width-cell "Группа"]
                          [:td.full-width-cell "Остатки по размерам"]
                          [:td.fixed-100px-width-cell "Бренд"]
                          [:td.fixed-100px-width-cell "Операции"]])

                        true
                        (let [{:keys [id] :as stock} entity]
                          ;(cached
                           ;:admin-main-stock-template :stock stock
                           (html
                            (inplace-multi-region
                             [inplace-stock-editing stock-editing] id []
                             (let [stock (region-request inplace-stock-editing
                                                         (inplace:get-stock-with-changes (s* id))
                                                         stock)]
                               (retail-stock-template stock))))
                           ;)
                          )))
                :page 100
                :columns 1
                :custom-rows-mode true
                :register-as organizer-stock-pages
                :pager-title (fn[count current]
                               [:span "Список товаров (" count ")"
                                [:span.badge {:role :selected-count}]]))]]]]]))

(defpage "/retail/admin/pdf/:path" {path :path}
  (response/content-type "application/pdf" (new java.io.File (str "/tmp/" path))))
