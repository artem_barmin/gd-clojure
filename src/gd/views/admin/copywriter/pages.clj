(ns gd.views.admin.copywriter.pages
  (:use gd.views.resources
        gd.views.admin.common
        [clojure.algo.generic.functor :only (fmap)]
        gd.views.bucket.simple
        compojure.core
        gd.model.model
        gd.model.context
        gd.utils.web
        gd.utils.common
        gd.views.messages
        gd.views.components
        gd.views.admin.stocks
        gd.views.admin.components
        gd.views.admin.stock-model
        noir.core
        [noir.options :only (dev-mode?)]
        [noir.response :only (redirect status set-headers)]
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defpage "/retail/copywriter/page/" {}
         (copywriter-admin-layout
           [:a.small-success-button-plain.inline-block {:href ""}
            [:div.admin-action-panel.admin-adding-panel
             [:div [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Страница"]]]
           ))

(defpage "/retail/copywriter/stocks/" {}
         (copywriter-admin-layout(copywriter-stock-filters)
                              [:div.admin.group
                               [:div.group.admin-panel.retail-standard-panel
                                [:div
                                 (stock-merge)
                                 (inplace-change-callbacks)
                                 (inplace-edit-panel)
                                 (inplace-statistics-panel-with-tabs
                                   :stock-tab
                                   {:name "Все товары" :value "all"}
                                   {:name "Добавленные" :value "new" :count #(inplace:count-new)}
                                   {:name "Измененные" :value "changed" :count #(inplace:count-changes)})

                                 (stock-edit-dialog)
                                 (search-callback (js* (deselectAll)
                                                       (organizerStockPagesLoadPage 0)
                                                       (clj (rerender :massive-selection))))

                                 [:div.admin-stock-list
                                  [:div.stocks-list
                                   (pager supplier-stocks
                                          (modify-result
                                            (supplier-stocks-with-filters false page-size)
                                            (fn[res](concat [:header] res)))
                                          (fn[entity]
                                            (cond (= entity :header)
                                                  (precompile-html
                                                    [:tr.admin-table-head
                                                     [:td.fixed-25px-width-cell
                                                      (custom-checkbox {:onchange (js (selectAllOnPage this))} :choose false)]
                                                     [:td.fixed-50px-width-cell "Фото"]
                                                     [:td.fixed-150px-width-cell "Название"]
                                                     [:td.fixed-100px-width-cell "Группа"]
                                                     [:td.full-width-cell "Остатки по размерам"]
                                                     [:td.fixed-100px-width-cell "Бренд"]
                                                     [:td.fixed-100px-width-cell "Операции"]])

                                                  true
                                                  (let [{:keys [id] :as stock} entity]

                                                    (cached
                                                      :admin-main-stock-copy-template :stock stock
                                                      (html
                                                        (inplace-multi-region
                                                          [inplace-stock-editing stock-editing] id []
                                                          (let [stock (region-request inplace-stock-editing
                                                                                      (inplace:get-stock-with-changes (s* id))
                                                                                      stock)]
                                                            (retail-stock-copy-template stock))))
                                                      )


                                                    )))
                                          :page 100
                                          :columns 1
                                          :custom-rows-mode true
                                          :register-as organizer-stock-pages
                                          :pager-title (fn[count current]
                                                         [:span "Список товаров (" count ")"
                                                          [:span.badge {:role :selected-count}]]))]]]]]))