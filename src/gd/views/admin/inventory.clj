(ns gd.views.admin.inventory
  (:use gd.model.model
        gd.views.admin.components
        gd.views.admin.arrival-model
        clojure.algo.generic.functor
        gd.views.admin.dictionary
        gd.views.admin.arrival-list
        gd.views.admin.arrival
        gd.views.admin.procurement
        gd.views.admin.common
        gd.views.admin.stocks
        gd.model.context
        gd.views.messages
        hiccup.def
        clojure.data.json
        clojure.test
        gd.views.components
        gd.views.admin.stock-model
        gd.utils.web
        gd.utils.db
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [clojure.set :as set])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defn- closed-inventory? []
  (= :closed (:status (arrival:get-arrival-by-id (current-arrival)))))

(wrap-request-cache #'closed-inventory?)

(defn inventory-sizes-config-template[{:keys [price id category stock-arrival-parameter stock-variant
                                              all-params params left-stocks left-snapshot context]
                                       :as stock}]
  [:div.params-container
   (let [all-arrived-params (set (map :parameters (apply concat (vals stock-arrival-parameter))))
         last-arrival (get-last-arrival stock-arrival-parameter)]
     (with-group :params
       (map! (fn[[params]]
               (when (all-arrived-params params)
                 (let [last-arrived-parameters
                       (find-by-val :parameters params
                                    (get stock-arrival-parameter last-arrival))

                       last-arrival-info-tooltip
                       (let [arrival-params
                             (get stock-arrival-parameter last-arrival)

                             {:keys [stock-arrival]}
                             (find-first (fn[{:keys [stock-arrival parameters]}]
                                           (and stock-arrival (= parameters params)))
                                         arrival-params)

                             {:keys [quantity price]}
                             last-arrived-parameters]
                         (tooltip
                          [:div {:style "margin:0 5px 5px;"}
                           [:div "Цена (bono.in.ua): "
                            (or (:price (find-by-val :context :wholesale (get stock-variant params))) "---")
                            " грн."]
                           (if stock-arrival
                             [:div "Последний приход: " (format-date (stock-arrival :created))]
                             [:div "Приходов не было"])
                           (when (and quantity stock-arrival)
                             [:div "Кол-во в последнем приходе: " quantity " шт."])
                           (when (and price stock-arrival)
                             [:div "Закупочная цена в последнем приходе: " price " грн."])]))]
                   (let [arrival-params
                         (arrival:get-quantity-from-session id)

                         inventory-params
                         (get stock-arrival-parameter (current-arrival))

                         {:keys [price quantity]}
                         (or (find-by-val :parameters params arrival-params)
                             (find-by-val :parameters params inventory-params))]
                     (with-group (gen-client-id)
                       [:div.left {:data-title last-arrival-info-tooltip
                                   :style "height:auto; margin:2px; width:93px;"}
                        (with-group :params
                          (map! (fn[[name value]] (hidden-field name value)) params))
                        (let [warehouse (or (get left-snapshot params) 0)
                              quantity (or quantity (- warehouse))
                              real-quantity (and warehouse quantity (+ warehouse quantity))]
                          [:div.fixed-group.parameter-tag.left
                           [:div.size (draw-variant params)]
                           [:div.quantity warehouse " шт"]
                           [:div.price
                            (hidden-field
                             {:onchange (input-change-callback
                                         id
                                         :success (js* (refreshTabs)
                                                       (recomputeInventoryStates)))}
                             :quantity quantity)
                            (text-field {:readonly (closed-inventory?)
                                         :class "input"
                                         :style "padding:0;"
                                         :data-warehouse warehouse
                                         :onchange (js (var quantity (.prev ($ this)))
                                                       (var diff (- (parseInt (.val ($ this))) (clj warehouse)))
                                                       (.change (.val quantity diff)))}
                                        :real-quantity real-quantity)]])])))))
             stock-variant)))])

(defn inventory-stock-template[{:keys [name images meta-info price description group-deal
                                       stock-parameter-value id category additional-stock-window status
                                       album fkey_category state new-sizes stock-arrival-parameter
                                       all-params params left-stocks left-snapshot context]
                                :as stock}]
  [:tr
   [:td
    (sized-image {:style "margin:5px 0 0 -5px;"
                  :class "organizer-stock-list-image"
                  :data-title (images-tooltip images)}
                 (images:get :stock-medium (first images)))]
   [:td name]
   [:td (inventory-sizes-config-template stock)]
   [:td (get-brand all-params)]
   [:td (draw-category (:id category))]

   (when-not (closed-inventory?)
     [:td.link
      (if (arrival:added-stock? stock)
        [:a {:onclick (arrival-remove-stock id)}
         "Удалить"]
        [:a {:onclick (cljs
                       (remote* mark-stock-inventory
                                (let [{:keys [params id left-snapshot]}
                                      (inventory:annotate-single-stock-with-snaphost
                                        (current-arrival) (inplace:get-stock-with-changes (s* id)))]
                                  (arrival:save-quantity-to-session
                                   id (map (fn[[params quantity]]
                                             {:parameters params
                                              :quantity (* -1 (or quantity 0))})
                                           left-snapshot)))
                                :success (fn[]
                                           (refreshTabs)
                                           (clj (multi-rerender :arrival-stock-editing id)))))}
         "Добавить"])])])

(defn inventory-stock-list[]
  [:div.admin-stock-list.inventory-list
   (pager supplier-stocks-inventory
          (modify-result
           (->>
            (supplier-stocks-with-filters false page-size)
            (inventory:annotate-stock-with-snaphost (current-arrival)))
           (fn[res] (concat [:header] res)))
          (fn[entity]
            (cond (= entity :header)
                  (precompile-html
                   [:tr.admin-table-head
                    [:td.fixed-50px-width-cell "Фото"]
                    [:td.fixed-150px-width-cell "Название"]
                    [:td.full-width-cell "Остаток на складе"]
                    [:td.fixed-100px-width-cell "Бренд"]
                    [:td.fixed-200px-width-cell "Категория"]
                    (when-not (closed-inventory?)
                      [:td.fixed-75px-width-cell "Операции"])])

                  true
                  (let [{:keys [id] :as stock} entity]
                    (inplace-multi-region
                     [inventory-stock-editing arrival-stock-editing] id []
                     (let [stock (region-request inventory-stock-editing
                                   (first (inventory:annotate-stock-with-snaphost
                                            (current-arrival)
                                            [(inplace:get-stock-with-changes (s* id))]))
                                   stock)]
                       (inventory-stock-template stock))))))
          :page 40
          :columns 1
          :custom-rows-mode true
          :pager-title "Переучет"
          :register-as organizer-stock-pages)])

(defpage "/retail/admin/inventories/" {}
  (arrival-list :inventory))

(defn inventory-management[]
  [:div.admin-toggle-block.group
   [:div.admin-filter-panel
    [:div.group.organizer-stock-filter-block.fout-hidden
     [:div.admin-filters-panel.left
      [:div.group.organizer-stock-filter-block.fout-hidden.admin-filters
       (stock-filters)]]
     [:div.admin-create-stock-btn.left {:style "line-height:30px;"}
      (if (closed-inventory?)
        [:div.margin10l "Переучет закрыт"]
        (if (sec/partner?)
          (small-success-button "Применить изменения"
                           :action (js (if (confirm "Вы уверены? После закрытия, редактировать переучет нельзя")
                                         (clj (remote* close-inventory
                                                       (do (inventory:close-inventory (current-arrival)) nil)
                                                       :success (fn[] (. location reload)))))))))]]]])

(defmethod arrival-filter-on-save :inventory [arrival params]
  (fmap (fn[stock-params] (if (= :removed stock-params) [] (filter :quantity stock-params))) params))

(defn inventory:not-missed-session-stocks[]
  (remove nil?
          (map (fn[[k params]]
                 (when-not (= :removed params)
                   (when (every? (fn[{:keys [quantity]}] (or (not quantity) (= quantity 0))) params) k)))
               (current-arrival-session))))

(defn inventory:missed-session-stocks[]
  (remove nil?
          (map (fn[[k params]]
                 (or (and (seq? params) (empty? params) k)
                     (when-not (= :removed params)
                       (when (some (fn[{:keys [quantity]}] (and quantity (not= quantity 0))) params) k))))
               (current-arrival-session))))

(defn- missed-quantity-stocks[]
  (if (closed-inventory?)
    (inventory:missed-quantity-stocks (current-arrival))
    (set/union
     (set/difference (set (inventory:missed-quantity-stocks (current-arrival)))
                     (set (arrival:removed-stock-ids))
                     (set (inventory:not-missed-session-stocks)))
     (set (inventory:missed-session-stocks)))))

(defmethod inplace-status-param-functions :inventory-statistics [limit offset params]
  (retail:admin-supplier-stocks
   limit offset
   (merge
    params
    {:limited-ids (missed-quantity-stocks)
     :exclude-ids (arrival:removed-stock-ids)
     :force-limited-ids true
     :state [:visible :not-prepared :invisible]})))

(defmethod inplace-status-param-functions :all-arrived-stock [limit offset params]
  (retail:admin-supplier-stocks
   limit offset
   (merge
    params
    {:at-least-arrived-stocks true
     :state [:visible :not-prepared :invisible]})))

(defpage "/retail/admin/inventory/:id/" {id :id}
  (add-var-to-global-context :arrival id)
  (common-admin-layout (inventory-management)
    [:div.admin.group.admin-panel.retail-standard-panel
     (arrival-edit-panel)
     (arrival-general-info-panel)
     [:div.alert.alert-info.margin15
      (let [{:keys [positive negative]} (inventory:sums (current-arrival))]
        (list
         [:span {:style "color:#1FACA3;"} "Плюс: " positive]
         [:span.margin15l {:style "color:#E06337;"} "Минус: " negative]))]
     (inplace-statistics-panel-with-tabs
       :inventory-tab
       {:name "Все товары" :value "all-arrived-stock"}
       {:name "Переучет" :value "arrived" :count #(arrival:count-arrived)}
       {:name "Статистика" :value "inventory-statistics" :count #(count (missed-quantity-stocks))})

     (inplace-change-callbacks)
     (inventory-stock-list)]))
