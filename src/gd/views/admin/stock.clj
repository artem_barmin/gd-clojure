(ns gd.views.admin.stock
  (:use gd.model.model
        gd.utils.styles
        gd.views.admin.common
        clojure.walk
        gd.views.admin.dictionary
        clojure.data
        clojure.algo.generic.functor
        gd.model.context
        gd.views.messages
        hiccup.def
        clojure.data.json
        korma.core
        gd.views.components
        gd.views.admin.stock-model
        gd.views.admin.group-stock-model
        [gd.views.project_components :only (render-params)]
        gd.utils.web
        gd.utils.common
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]
            [clojure.string :as string]))

(defjsmacro select-tab[tab]
  (var el ($dialog (clj (as-str "[href=#" tab "]"))))
  (if (! (.hasClass (.parent el) "active"))
    (.one ($ document) "shown.bs.tab" centerDialog))
  (.tab el "show"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; AngularJS templates

(defn- variants[]
  (let [spinner (fn[model & [add-on]]
                  (spinner :global :add-on (or add-on "грн.") :attrs {:ng-model model} :width 65 :min 0))]
    [:div.modal-tab-block
     [:div.well.well-sm {:style "height:100%; width:100%; display:table;"}
      [:table
       [:tr
        [:td
         [:div {:data-title "Добавить все комбинации параметров, влияющих на цену"}
         (small-ok-button [:i.glyphicon.glyphicon-arrow-down.glyphicon-sm]
                           :width 34
                           :ng-action (js (fillAllVariants)))]]
        [:td
         {:ng-repeat "paramName in paramNames"
          :ng-if "config.useInVariants[paramName.type]"}]
        [:td (spinner "globalPrice.wholesale.price")]
        [:td (spinner "globalPrice.retail.price")]
        [:td (spinner "globalPrice['wholesale-sale'].price")]
        [:td (spinner "globalPrice['retail-sale'].price")]]

       [:tr.fixed-height-25px
        [:td.fixed-50px-width-cell]
        [:td.fixed-175px-width-cell
         {:ng-repeat "paramName in paramNames"
          :ng-if "config.useInVariants[paramName.type]"}
         "{{paramName.name}}:"]
        [:td.fixed-125px-width-cell "Цена, Bono"]
        [:td.fixed-125px-width-cell "Цена, LP"]
        [:td.fixed-125px-width-cell "Скидка, Bono"]
        [:td.fixed-125px-width-cell "Скидка, LP"]]

       [:tbody
        [:tr.fixed-height-35px {:style "vertical-align:top;"
                                :ng-repeat "variant in stock.variant"}
         [:td
          (small-danger-button [:i.glyphicon.glyphicon-sm.glyphicon-remove-sign]
                               :ng-action (js (removeVariant $index))
                               :ng-disabled (js* (and variant.left (not= variant.left 0)))
                               :ng-disabled-tooltip "Нельзя удалить варианты по которым есть остатки"
                               :width 34)]
         [:td {:ng-repeat "paramName in paramNames"
               :ng-if "config.useInVariants[paramName.type]"}
          [:div.orange-dropdown {:style "width:140px;"}
           (custom-dropdown {:ng-model "variant.params[paramName.type]"
                             :empty-value ["paramName.name"]}
                            :size [["{{paramName.name}}" "null"]
                                   ["{{val}}" "{{val}}" {:ng-repeat "val in variantParamValues(paramName.type)"}]])]]
         [:td (spinner "variant.contexts.wholesale.price")]
         [:td (spinner "variant.contexts.retail.price")]
         [:td (spinner "variant.contexts['wholesale-sale'].price")]
         [:td (spinner "variant.contexts['retail-sale'].price")]]
        [:tr.fixed-height-35px
         [:td.left {:style "padding-left:8px;"}
          (small-success-button [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign]
                                :width 34
                                :ng-action (js (addVariant))
                                :ng-disabled (js* (! (canAddVariants)))
                                :ng-disabled-tooltip "Без параметров можно добавить только один вариант")]]]]]]))

(defn- parameters []
  [:div.modal-tab-block
   [:div.panel.panel-default
    [:div.inline-block
     [:div.inline-block.valign-top.margin5b {:ng-repeat "paramName in paramNames"}
      [:div {:style "width:215px;"}
       [:div.orange-dropdown.margin5r.group {:style "width:209px; margin-bottom:1px;"}
        (custom-dropdown {:ng-model "paramName.type" :ng-view "paramName.name"}
                         :param
                         [["{{param.name}}" "{{param.type}}" {:ng-repeat "param in leftParams()"}]])]

       [:div.group
        [:div.margin15l.left
         (custom-checkbox {:ng-model "config.useInVariants[paramName.type]"
                               :ng-disabled (js (aget config.usedInVariants paramName.type))}
                              :variant)]
        [:div.margin5l.margin5t.left "Влияет на цену"]]

       [:div.group
        [:div {:style "width:240px;"
               :ng-repeat "value in stock.params[paramName.type] track by $index"}

         [:div.left.margin5r.margin5t
          (small-danger-button [:i.glyphicon.glyphicon-sm.glyphicon-remove-sign]
                               :ng-action (js (deleteParameterValue paramName.type $index))
                               :ng-disabled (js* (valueUsedInVariant paramName.type value))
                               :ng-disabled-tooltip "Нельзя удалять параметр который используется для цен"
                               :width 34)]

         [:div.left.margin5t {:style "width:170px;"}
          (text-field {:ng-model "stock.params[paramName.type][$index]"
                       :class "input input-text"
                       :ng-options "param for param in leftParamValues(paramName.type)"
                       :data-min-length 0
                       :bs-typeahead true} :value)]]]

       [:div.margin5t {:style "width:34px;"}
        (small-success-button [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign]
                              :ng-action (js (addParameterValue paramName.type)) :width 34)]]]

     [:div.inline-block
      (small-success-button (list [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Параметр")
                            :ng-action (js (addParameter))
                            :ng-disabled (js* (! (isParamNamesLeft)))
                            :ng-disabled-tooltip "Нет доступных параметров для добавления"
                            :width 130)]]]])

(defn- measurements []
  [:div.modal-tab-block
   [:div.well.well-sm.margin5t.margin10b.group {:style "min-height:162px;"}
    [:div {:ng-if "!stock.params.size.length"}
     "Для добавления замеров необходимо добавить размеры"]
    [:div {:ng-if "stock.params.size.length"}
     [:div.margin5b "Замеры для размера:"]
     [:div.group {:style "width:189px;"}
      [:div.orange-dropdown
       (custom-dropdown {:width 45 :ng-model "config.currentMeasurementSize"}
                        :measurement ["Размер"
                                      ["{{param}}" "{{param}}"
                                       {:ng-repeat "param in stock.params.size"}]])]]

     [:div
      [:div.group {:ng-repeat "measurement in stock.description.measurements[config.currentMeasurementSize] track by $index"}
       [:div.left.margin5r.margin5t
        (small-danger-button [:i.glyphicon.glyphicon-sm.glyphicon-remove-sign]
                             :ng-action (js (removeMeasurement $index))
                             :width 34)]

       [:div.green-dropdown.fixed-width-150px.left.margin5r.margin5t

        (text-field {:ng-model "measurement.name"
                     :class "input input-text"
                     :ng-options "param for param in leftMeasurements()"
                     :data-min-length 0
                     :placeholder "Тип замера"
                     :bs-typeahead true} :value)]
       [:div.left.margin5r.margin5t
        (text-field {:ng-model "measurement.value" :class "input input-text"} :value)]]]
     [:div.margin5t {:style "width:34px;"}
      (small-success-button [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] :width 34
                            :ng-action (js (addMeasurement)))]]]])

(defn- edit-photo []
  [:div
   [:div.modal-image.left
    [:div.fixed-height-380px [:img {:style "height:380px;width:269px;"
                                    :ng-src "{{stock.image.big || '/img/grid-admin/no_photo.svg'}}"}]]]
   [:div.modal-img-controls.margin5l.left {:style "height:379px; width:622px;"}
    [:div.well.well-sm.group {:style "margin-bottom:0px;"}
     [:div.left
      (multi-file-upload :stock-showcase-upload
                         (form-for-file-json* stock-image-upload stock-image-sizes
                                              (prepare-single-image code))
                         :success (js* addStockImage)
                         :text (list [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign] " Фото")
                         :width 150)]
     [:div.left.margin10l (small-warning-button "Сделать главной" :width 150 :ng-action (js (makeImageMain)))]]
    [:div.stock-images-list.margin5t.well.well-sm {:style "height:322px; overflow:auto; margin-bottom:0;"}
     [:div.relative.left {:ng-repeat "image in stock.images track by $index"}
      [:img.margin10r.margin10b {:ng-click (js (set! stock.image image))
                                 :ng-src "{{image.small}}"}]
      (close-button (js (removeImage $index)))]]]])

(defn- properties-edit []
  [:div.modal-tab-block
   [:div.panel.panel-default
    [:div.inline-block
      [:div {:style "width:215px;"}
       [:div.group
        [:div {:style "width:625px;" :ng-repeat "item in stock.description.properties track by $index"}
         [:div.left.margin5r.margin5t (small-danger-button [:i.glyphicon.glyphicon-sm.glyphicon-remove-sign] :ng-action (js (removeProperty $index)) :width 34)]

         [:div.left.margin5t {:style "width:170px;"}
              (text-field {:ng-model        "item.name"
                           :class           "input input-text"
                           :bs-typeahead true
                           :ng-options "param for param in dictionary.properties"
                           :data-min-length 0} :name)]
          [:div.left.margin5t.margin5l {:style "width:400px;"}
              (text-field {:ng-model        "item.value"
                           :class           "input input-text"
                           :data-min-length 0} :value)]]]
       [:div.margin5t {:style "width:34px;"}
        (small-success-button [:i.glyphicon.glyphicon-sm.glyphicon-plus-sign]
                              :ng-action "addProperty()" :width 34)]]]]])

(defn category-name-with-count [child name id]
  (let [{:keys [cnt]} (first (select stock
                        (aggregate (count :*) :cnt)
                        (where {:fkey_category id})))]
  (str (string/trim name) (when-not child (str " (" cnt ")")))))

(defn category-dropdown[& [level categories]]
  (let [level (or level 0)
        tree (or categories (stock:container-categories-tree {:show-invisible true}))]
    (mapcat (fn[[k {:keys [child name id] :as cat}]]
              (cons [(category-name-with-count child name id) id (merge (when child {:disabled true})
                                    {:style (css-inline :padding-left (* 8 level))})]
                    (when child (category-dropdown (inc level) child))))
            tree)))

(defn- visibility[]
  [:div.margin5b.group
   [:div.left (custom-checkbox {:ng-model "stock.context.wholesale.status"} :visible)]
   [:div.left.margin5l "Bono"]
   [:div.left.margin15l (custom-checkbox {:ng-model "stock.context.retail.status"} :visible)]
   [:div.left.margin5l "LP"]
   [:div.left.margin15l (custom-checkbox {:ng-model "stock.sale"} :visible)]
   [:div.left.margin5l "Распродажа"]])

(defn- main-properties []
  [:div.group
   [:div.modal-image.left
    [:div.fixed-height-380px {:onclick (js (select-tab :photo))}
     [:img {:style "height:380px;width:269px;"
            :ng-src "{{stock.images[0].big || '/img/grid-admin/no_photo.svg'}}"}]]]

   [:div.margin5l.well.well-sm.left {:style "height:379px; width:622px;"}
    [:div.margin2b "Название:"]
    [:div.margin5b
     (text-field {:ng-model "stock.name" :class "input input-text"} :name)]

    [:div.margin2b "Категория:"]
    [:div.orange-dropdown.group.margin5b
     (custom-dropdown {:ng-model "stock.fkey_category"
                       :ng-view "stock.category.name"}
                      :category
                      (cons ["Другая" (:id (category:get-by-key "other"))]
                            (let [invisible (set (map (fn[key] (:id (category:get-by-key key :relative true)))
                                                      ["new" "sale"]))]
                              (remove (comp invisible second) (category-dropdown 0 (category:tree))))))]

    [:div.margin2b "Цены и параметры:"]
    [:div.margin5b
     [:div.input.input-disabled.input-text.clickable {:onclick (js (select-tab :variants))}
      [:div.left "{{minimalPrice()}} грн"]
      [:div.right "Редактировать →"]]]

    [:table
     [:tr
      [:td
       [:div.margin2b "Настройки видимости:"]
       (visibility)]
      [:td
       [:div.margin2b "Настройки учета:"]
       [:div.margin5b.group
        [:div.left (custom-checkbox {:ng-model "stock.accounting"
                                     :ng-true-value "simple"
                                     :ng-false-value "warehouse"}
                                    :accounting)]
        [:div.left.margin5l "Учет без остатков"]]]]]

    [:div.margin2b "Описание:"]
    [:div.margin5b {:style "height:117px;"}
     (text-area {:ng-model "stock.description.other" :class "input input-textarea"} :description)]]])

(defn- save-or-update-stock[{:keys [id multipleAddAmount] :as state}]
  (cond
    (and (not id) (not multipleAddAmount))
    (do (stock:save-stock-general (prepare-stock-params state))
        {})

    (and (not id) (>= (parse-int multipleAddAmount) 1))
    (do (dotimes [n (parse-int multipleAddAmount)]
          (inplace:store-new-stock-with-params (prepare-stock-params state)))
        {:tab "new"})

    (inplace:new-stock? id)
    (do (inplace:merge-changes state)
        {:id id})

    true
    (do (stock:update-stock (parse-int id) (prepare-stock-params state))
        {:id (parse-int id)})))

(defn stock-save-panel[]
  [:div.group
   [:div.right.margin10l
    (small-ok-button "Сохранить"
                     :action (cljs (remote* save-or-update-stock
                                            (let [state (c* (getCurrentStock))]
                                              (save-or-update-stock state))
                                            :success (fn[state]
                                                       (if state.tab
                                                         (refreshTabs state.tab))
                                                       (if state.id
                                                         ((clj (multi-dynamic-function :stock-editing (js* state.id))) refreshHighlight))
                                                       (closeDialog)
                                                       (clj (rerender :massive-selection))
                                                       (clj (rerender :organizer-stock-filters)))))
                     :width 120)]
   [:div.right
    (small-grey-button "Отменить" :action (js (closeDialog)) :width 120)]])

(defn stock-cancel-panel[]
  [:div.group
   [:div.right
    (small-grey-button "Отменить" :action (js (closeDialog)) :width 120)]])

(defn single-stock-editing-modal[]
  (require-js :admin :stock)
  [:div#stock-modal.ng-cloak {:ng-controller "StockCtl"}
   [:div.form.padding5 {:style "width:907px; height:416px;"}
    (tabbed-panel
     {}
     (tab :main "Общее" (main-properties))
     (tab :photo "Фотографии" (edit-photo))
     (tab :params "Параметры" (parameters))
     (tab :variants "Цены" (variants))
     (tab :measurements "Замеры" (measurements))
     (tab :properties-edit "Описание" (properties-edit)))
    [:div#spinner-for-multiple-add{:ng-show "multipleAdd"} [:span "Количество товаров:"]
     (spinner "amount-of-stocks" :attrs {:ng-model "stock.multipleAddAmount"})]]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Register stock creation panel

(defn gen-stock-modal-edit []
  [:div
   (javascript-tag (js (set! newStock (clj (new-stock-default)))
                       (set! dictionary (clj (dictionary:get-dictionary)))))
   [:div#stock-modal-edit (modal-window-admin "Редактирование товара"
                                              (stock-save-panel)
                                              (single-stock-editing-modal))]])

(defn gen-stock-copy-modal-edit []
  [:div
   (javascript-tag (js (set! newStock (clj (new-stock-default)))
                       (set! dictionary (clj (dictionary:get-dictionary)))))
   [:div#stock-copy-modal-edit (modal-window-admin "Информация о товаре"
                                              (stock-cancel-panel)
                                              (single-stock-editing-modal))]])
(register-create-panel :stock
                       (fn[]
                         (js
                           (setCurrentStock newStock)
                           (openDialogAngular "#stock-modal-edit")))
                       gen-stock-modal-edit)

(register-create-panel :stockcopy
                       (fn[]
                         (js
                           (setCurrentStock newStock)
                           (openDialogAngular "#stock-copy-modal-edit")))
                       gen-stock-copy-modal-edit)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Group stock editing

(defn- group-main-properties []
  [:div.margin5l.margin5r.well.well-sm.left {:style "height:379px;"}
   [:div.margin2b "Название:"]
   [:div.margin5b
    (text-field {:ng-model "stock.name" :class "input input-text"} :name)]

   [:div.margin2b "Категория:"]
   [:div.orange-dropdown.group.margin5b
    (custom-dropdown {:ng-model "stock.fkey_category"
                      :ng-view "stock.category.name"}
                     :category
                     (concat [["Категория" "null"]
                              ["Другая" (:id (category:get-by-key "other"))]]
                             (category-dropdown)))]

   [:div.margin2b "Настройки видимости:"]
   (visibility)

   [:div.margin2b "Описание:"]
   [:div.margin5b {:style "height:117px;"}
    (text-area {:ng-model "stock.description.other" :class "input input-textarea"} :description)]])

(defn- prepare-values[coll]
  (seq (distinct (flatten-coll coll))))

(defn- render-params-diff[params]
  (when (seq params)
    [:div.group.bs-callout.bs-callout-warning
     [:h4 "Изменения параметров:"]
     (map (fn[[[param] {:keys [removed added]}]]
            [:div.group
             [:div.left
              [:div.bs-callout.bs-callout-warning
               [:h4 "Для параметра " [:b (name param)]]
               (when-let [removed (prepare-values removed)]
                 [:div "Старое значение (Удалено): " (map (fn[val] [:span val " "]) removed)])
               (when-let [added (prepare-values added)]
                 [:div "Новое значение (Добавлено): " (map (fn[val] [:span val " "]) added)])]]])
          params)]))

(defn- render-measurements-diff[measurements]
  (when (seq measurements)
    [:div.group.bs-callout.bs-callout-warning
     [:h4 "Изменения замеров:"]
     (map (fn[[size {:keys [removed added]}]]
            [:div.group
             [:div.left
              [:div.bs-callout.bs-callout-warning
               [:h4 "Для размера " [:b (name size)]]
               (when-let [removed (prepare-values removed)]
                 [:div "Старое значение (Удалено): "
                  (map (fn[{:keys [name value]}] [:span name ":" value " "]) removed)])
               (when-let [added (prepare-values added)]
                 [:div "Новое значение (Добавлено): "
                  (map (fn[{:keys [name value]}] [:span name ":" value " "]) added)])]]])
          measurements)]))

(defn- render-stock-variants-diff[variants]
  (map (fn[[params {:keys [removed added]}]]
         (cond (and (seq removed) (empty? added))
               [:div.group.bs-callout.bs-callout-danger
                [:h4 "Удалено"]
                [:div "Параметры " params " и вся связазанная с ним информация"]]

               (and (seq removed) (seq added))
               [:div.group.bs-callout.bs-callout-warning
                [:h4 "Изменены цены"]
                [:div.left "Параметры " params]]

               (and (empty? removed) (seq added))
               [:div.group.bs-callout.bs-callout-success
                [:h4 "Добавлены цены"]
                [:div.left "Параметры " params]]))
       variants))

(defn- render-status-diff[statuses]
  (let [template {:wholesale "Товар %s на оптовом сайте"
                  :retail "Товар %s на розничном сайте"
                  :wholesale-sale "Товар %s на оптовом сайте в распродаже"
                  :retail-sale "Товар %s на розничном сайте в распродаже"}]
    (map (fn[[type {:keys [old new]}]]
           (let [[text class]
                 (case (map first [old new])
                   [:visible :invisible] ["перестанет отображаться" :bs-callout-danger]
                   [:invisible :visible] ["будет отображаться" :bs-callout-success]
                   [:not-prepared :invisible] ["перестанет отображаться" :bs-callout-danger]
                   [:not-prepared :visible] ["будет отображаться" :bs-callout-success])]
             [:div.group.bs-callout {:class class}
              [:h4 "Изменение видимости"]
              [:div.left (format (type template) text)]]))
         statuses)))

(defn- render-name-diff[{:keys [old new] :as names}]
  (when names
    [:div.group.bs-callout {:class class}
     [:h4 "Изменение имени:"]
     [:div.left (map (fn[name] [:span name " "]) (maybe-seq old)) " → " new]]))

(defn- group-results[stocks state]
  (let [changes (group-stock-changes stocks state)
        aggregated (group-aggregated-changes changes)]
    [:div {:style "margin:5px 10px 0"}
     [:div (render-name-diff (:name aggregated))]
     [:div (render-status-diff (:context aggregated))]
     [:div (render-params-diff (:param aggregated))]
     [:div (render-stock-variants-diff (:stock-variant aggregated))]
     [:div (render-measurements-diff (:measurements (:description aggregated)))]
     (group-stock-save-button)
     (when (sec/root?)
       [:pre (with-out-str (clojure.pprint/pprint aggregated))])]))

(defn group-stock-editing-modal[]
  [:div#group-stock-modal.ng-cloak {:ng-controller "StockCtl"}
   [:div.form.padding5 {:style "width:907px;"}
    (tabbed-panel
     {}
     (tab :main "Общее" (group-main-properties))
     (tab :params "Параметры" (parameters))
     (tab :variants "Цены" (variants))
     (tab :measurements "Замеры" (measurements))
     (tab :group-results "Результаты" (region* group-edit-results[]
                                        (group-results
                                         (group-of-stocks (c* selectedItems))
                                         (c* (getCurrentGroupStock))))))]])
