(ns gd.views.admin.styles.base_skin
  (:use gd.utils.common)
  (:use gd.utils.styles)
  (:use gd.views.styles.utils)
  (:use gd.views.styles.common))

(defn menu-icon [name]
  (list
   [(str "." name "-icon")
    (tile-image (str "/img/grid-admin/menu-icons/" name "-icon.png"))]

   [".menu-item:hover, .menu-item.selected"
    [(str "." name "-icon")
     (tile-image (str "/img/grid-admin/menu-icons/" name "-icon-hover.png"))]]))

(defn submenu-icon [name]
  (list
   [(str "." name "-icon")
    (tile-image (str "/img/grid-admin/menu-icons/" name "-icon.png"))]

   [(str ".submenu-popup-item:hover ." name "-icon")
    (tile-image (str "/img/grid-admin/menu-icons/" name "-icon-hover.png"))]))

(defn fixed-width-cell [width]
  [(str ".fixed-" width "px-width-cell")
   :min-width (str width "px")])

(defn plain-aliased-button[alias background-color gradient-from-color gradient-to-color border-color disabled-background-color disabled-border-color open-color]
  (let [name (str ".small-" alias "-plain")]
    (list
     [name
      :background-color background-color
      :background-repeat "repeat-x"
      :border (str "1px solid " border-color)
      :border-radius "3px"
      :color "#FFFFFF"
      :cursor "pointer"
      :box-shadow "0 1px 0 rgba(255, 255, 255, 0.3) inset"
      :text-shadow "0 -1px 0 rgba(0, 0, 0, 0.25)"
      :text-align "center"
      :font-size "15px"
      :padding "4px 15px!important"
      (with-inner-prefixes "background-image" "linear-gradient" (str "bottom, " gradient-from-color", " gradient-to-color))]

     [(str name ".enabled:not(.disabled):hover, " name ".enabled:not(.disabled):focus, " name ".enabled:not(.disabled):active, " name ".active:not(.disabled)")
      :background-color gradient-to-color
      :background-image "none"
      :color "#FFFFFF"]

     [(str name ".disabled, " name "[disabled]")
      :background (str "none repeat scroll 0 0 " disabled-background-color)
      :opacity "0.8"
      :border-color disabled-border-color]

     [(str name ".active.enabled:not(.disabled), " name ".enabled:not(.disabled):active, .open .dropdown-toggle")
      :background (str "none repeat scroll 0 0 " open-color)
      :box-shadow "0 1px 3px rgba(0, 0, 0, 0.1) inset !important"])))

(defn colored-dropdown [parent-class background-color gradient-from-color gradient-to-color border-color disabled-background-color disabled-border-color open-color]
  (list
   [(str "." parent-class " .dk_toggle")
    :background-color background-color
    :background-repeat "no-repeat"
    (tile-image "/img/dropdown/dk_simple_arrow.png")
    :border (str "1px solid " border-color "!important")
    :border-radius "3px"
    :color "#FFFFFF"
    :cursor "pointer"
    :box-shadow "0 1px 0 rgba(255, 255, 255, 0.3) inset"
    :text-shadow "0 -1px 0 rgba(0, 0, 0, 0.25)"
    :padding "4px 10px 5px !important"
    :font-size "14px"
    (with-inner-prefixes "background-image" "linear-gradient" (str "bottom, " gradient-from-color ", " gradient-to-color))]

   [(str "." parent-class " .dk_toggle:hover")
    :background-color gradient-to-color
    :background-image "none"
    :color "#FFFFFF"]

   [(str "." parent-class " .dk_toggle.disabled")
    :background (str "none repeat scroll 0 0 " disabled-background-color)
    :border-color border-color]

   [(str "." parent-class " .dk_toggle.disabled:active")
    :background (str "none repeat scroll 0 0 " open-color)]

   [(str "." parent-class " .dk_open .dk_toggle")
    :border-radius "3px !important"
    :background (str "none repeat scroll 0 0 " open-color " !important")
    :box-shadow "0 2px 4px rgba(0, 0, 0, 0.15) inset, 0 1px 2px rgba(0, 0, 0, 0.05) !important"]))

(defn alert [alias background-color border-color color]
  [(str "." alias)
   :background-color background-color
   :border (str "1px solid " border-color)
   :box-shadow "0 1px 0 rgba(255, 255, 255, 0.8)"
   :color color
   :border-radius "3px"
   :padding "8px 35px 8px 14px"
   :text-shadow "0 1px 0 rgba(255, 255, 255, 0.5)"])

(defn fixed-width [width]
  [(str ".fixed-width-" width "px")
   :width (str width "px")])

(defn fixed-height [height]
  [(str ".fixed-height-" height "px")
   :height (str height "px")])

(defn bs-callout [alias bg-color color]
  (list
   [(str ".bs-callout-" alias)
    :background-color bg-color
    :border-color color]

   [(str ".bs-callout-" alias " h4")
    :color color]))

(css "resources/public/css/grid/base_skin.css"

     ["body"
      :background "url('/img/grid-admin/body-bg.png') repeat scroll 0 0 #FAFAFA"
      :font-family "'PT Sans', sans-serif !important"
      :font-size "14px"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Login
     ["body.signin-page"
      :background "url('/img/grid-admin/login-bg.png') repeat scroll 0 0 #292929"
      :display :table
      :height "100%"
      :margin "0 auto"

      ["a, input, button" :outline "0 none !important"]
      ["form .btn, input, label, .social p" :font-size 14]
      ["input:not(.signin-modal-input)"
       :background "none repeat scroll 0 0 #FFFFFF"
       :border-color "#DEDEDE"
       :border-image :none
       :border-radius 0
       :border-style :solid
       :border-width "0 0 1px"
       :box-shadow :none
       :height 40
       :margin 0
       :padding "0 15px"
       :width "100%"]

      ["input[type='text']" :padding-right 105]
      ["input[type='password']" :padding-right 75]

      ["input:nth-child(1):not(.signin-modal-input)"
       :border-radius "3px 3px 0 0"]

      ["input:nth-child(2):not(.signin-modal-input)"
       :border "0 none"
       :border-radius "0 0 3px 3px"]

      ["a.forgot-password, a.register"
       :border-radius 3
       :color "#3A3A3A"
       :display :block
       :float :right
       :font-size 11
       :height 22
       :line-height 22
       :padding "0 6px"
       :position :relative
       :text-decoration :none
       :z-index 10
       :cursor :pointer]

      ["a.forgot-password:hover, a.register:hover"
       :text-decoration :underline
       :background "none repeat scroll 0 0 rgba(0, 0, 0, 0.05)"]

      ["a.register"
       :background "none repeat scroll 0 0 rgba(63, 137, 222, 0.5)"
       :margin "-72px 10px 0 0"]

      ["a.forgot-password"
       :background "none repeat scroll 0 0 rgba(243, 161, 57, 0.5)"
       :margin "-32px 10px 0 0"]

      ["#signin-container"
       :margin "0 auto"
       :width 312

       ["fieldset"
        :border "0 none"
        :margin 0
        :padding 0]

       [".header"
        :display :block
        :font-size 16
        :line-height 22
        :margin "0 auto 30px"
        :text-decoration :none
        :width 270]

       [".fields"
        :border "1px solid #000000"
        :border-radius 3
        :box-shadow "0 1px 0 rgba(255, 255, 255, 0.2)"]

       [".small-ok-button-plain"
        :border-radius "5px !important"
        :margin-left 1
        :margin-top 15
        :padding "7px 15px !important"
        :font-size "14px !important"]]
      [".social" :text-align :center :width 312 :cursor :pointer

       ["p"
        :color "#777777"
        :display :block
        :height 20
        :margin "30px 0 20px"
        :width 310
        :text-shadow "0 -1px 0 rgba(0, 0, 0, 0.8)"]

       ["p:before, p:after"
        :background "none repeat scroll 0 0 rgba(0, 0, 0, 0.3)"
        :box-shadow "0 1px 0 rgba(255, 255, 255, 0.07)"
        :content "''"
        :display :block
        :height 1
        :margin-top 10
        :position :absolute
        :width 60]

       ["p:after" :margin-left 251 :margin-top -10]

       ["a"
        :background "none repeat scroll 0 0 rgba(0, 0, 0, 0.2)"
        :border-radius 999
        :box-shadow "0 -1px 0 rgba(255, 255, 255, 0.1) inset"
        :color "rgba(255, 255, 255, 0.3)"
        :display :inline-block
        :font-size 22
        :height 50
        :line-height 50
        :margin "0 10px"
        :text-decoration :none
        :width 50]

       ["a:hover" :box-shadow :none :color "#FFFFFF"]

       ["a.twitter:hover" :background "none repeat scroll 0 0 #38A1C4"]
       ["a.odnoklassniki:hover" :background "none repeat scroll 0 0 #e87812"]
       ["a.vkontakte:hover" :background "none repeat scroll 0 0 #597BA5"]]]

     [".brandico:before" :font-family "'brandico', sans-serif"]

     [".ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable"
      :position "absolute !important"]

     [".signin-modal-input"
      :margin "5px 10px"
      :width "885px !important"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Glyphicon
     [".glyphicon-lg" :top 6 :font-size 20 :line-height 20 :margin-left -7]
     [".glyphicon-nm" :top 6 :font-size 18 :line-height 18 :margin-left -7]
     [".glyphicon-sm" :top 3 :font-size 16 :line-height 16 :margin-left -7]
     [".glyphicon-close" :color "#D85837" :left -2 :top -1]
     [".glyphicon-close:hover" :color "#A1391E"]
     [".glyphicon-container"
      :background-color :white
      :border-radius 11
      :color :white
      :font-size 27
      :font-weight :bold
      :height 21
      :left -2
      :line-height 17
      :position :absolute
      :text-align :center
      :top -1
      :width 24]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Call out
     [".bs-callout"
      :border-left "3px solid #EEEEEE"
      :margin "0 0 20px"
      :padding 20
      ["h4"
       :margin-bottom 5
       :margin-top 0]
      ["p:last-child"
       :margin-bottom 0]
      [".code"
       :background-color "#FFFFFF"
       :border-radius 3]
      [".bs-callout"
       :padding "0 0 0 20px"
       :margin "20px 0"]]

     (bs-callout "danger" "#FDF7F7" "#D9534F")
     (bs-callout "warning" "#FCF8F2" "#F0AD4E")
     (bs-callout "info" "#F4F8FA" "#5BC0DE")
     (bs-callout "success" "#dff0d8" "#3C763D")

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Common
     ["a:hover, a:focus" :text-decoration :none :color "#03ABE5"]
     ["label" :display :inline]
     ["p" :margin-bottom 0]
     ["ul" :margin-bottom 0]
     [".panel"
      :min-height 20
      :padding 5
      :border-radius 3]

     [".grey-panel"
      :background "none repeat scroll 0 0 rgba(0, 0, 0, 0.02)"
      :background-color "#F5F5F5"
      :border "1px solid #E3E3E3"
      :box-shadow "0 1px 1px rgba(0, 0, 0, 0.05) inset"]

     [".shadow-panel"
      :background-clip :padding-box
      :background-color "#FFFFFF"
      :border "1px solid #C0C0C0"
      :border-radius 3
      :box-shadow "0 1px 4px rgba(102, 102, 102, 0.3)"]

     [".transparent-panel"
      :background "none repeat scroll 0 0 rgba(0, 0, 0, 0.02)"
      :background-color "white"
      :border "1px solid #E3E3E3"
      :box-shadow "0 1px 1px rgba(0, 0, 0, 0.05) inset"]

     (fixed-width 25)
     (fixed-width 50)
     (fixed-width 75)
     (fixed-width 100)
     (fixed-width 150)
     (fixed-width 170)
     (fixed-width 300)
     (fixed-width 320)
     (fixed-width 433)
     (fixed-width 595)
     (fixed-width 610)
     (fixed-width 870)
     (fixed-width 897)

     (fixed-height 7)
     (fixed-height 25)
     (fixed-height 30)
     (fixed-height 35)
     (fixed-height 100)
     (fixed-height 301)
     (fixed-height 358)
     (fixed-height 380)
     (fixed-height 416)

     [".input"
      :border-radius "3px"
      :color "#555555"
      :display "inline-block"
      :font-size "13px"
      :padding "5px 6px"
      :vertical-align "middle"
      :background-color "#FFFFFF"
      :transition "border 0.2s linear 0s, box-shadow 0.2s linear 0s"
      :border-image "none"
      :border-style "solid"
      :border-width "1px"
      :width "100%"
      :border-color "#AAAAAA #C4C4C4 #C4C4C4"
      :border-right "1px solid #C4C4C4"
      :box-shadow "0 1px 0 rgba(255, 255, 255, 0.9)"]

     [".input:focus"
      :border-color "#4BA5E6"
      :box-shadow "0 0 5px rgba(75, 165, 230, 0.5)"]

     [".input-disabled"
      :color "#888888"
      :background-color "#EEEEEE"
      :border-color "#CCCCCC"]

     [".input-disabled:hover"
      :color :white
      :background-color "#C3C3C3"]

     [".input-text"
      :height "32px"
      :line-height "20px"]

     [".input-textarea"
      :height "100%"
      :font-family "'PT Sans',sans-serif"
      :font-size "14px"]

     [".ui-dialog-titlebar-close" :display :none]
     [".clickable" :cursor :pointer]
     [".link" :color "#36a6e6" :cursor :pointer]

     [".well" :margin-bottom 0]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Stock parameters icon
     [".parameter-tag"
      :height 55
      :font-size 13
      :margin "3px"
      :text-align :center
      :color :white
      ["div"
       :padding-left 3
       :padding-right 3]
      [".size"
       :background-color :black
       :border-radius "4px 4px 0 0"
       :text-transform :uppercase]
      [".price"
       :background-color "#36A6E6"
       :border-radius "0 0 4px 4px"]
      [".quantity" :background-color :gray]]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Title
     [".title" :font-size "20px"]
     [".pager-title" :margin "8px 0"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Background colors
     [".light-green-background"
      :background-color "#95ca4d"]

     [".blue-background"
      :background-color "#36a6e6"]

     [".yellow-background"
      :background-color "#ecb936"]

     [".orange-background"
      :background-color "#e06337"]

     [".dark-green-background"
      :background-color "#1faca3"]

     [".pink-background"
      :background-color "#e7699e"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Navbar and navmenu
     [".navbar-container"
      :background "url('/img/grid-admin/header-big.png') repeat scroll 0 0 transparent"
      :padding "10px 0 0"]

     [".navbar-border"
      :box-shadow "0 1px 1px 0 rgba(0, 0, 0, 0.4)"]

     [".navmenu"
      :background "url('/img/grid-admin/main-menu-bg.gif') repeat-y scroll 0 0 transparent"]

     [".menu-item"
      :text-decoration "none"
      :word-wrap "break-word"
      :cursor "pointer"]

     [".menu-item.selected"
      :transition "background-color 0.15s ease-in 0s, background-image 0.15s ease-in 0s, box-shadow 0.15s ease-in 0s"
      :background-color "rgba(65, 71, 82, 0.8)"
      :opacity "1"
      :transform "scale(1, 1)"
      :border-radius "4px"
      (with-prefixes "box-shadow" "0 1px 0 0 rgba(221, 231, 233, 0.4), 0 1px 0 #31363F inset, 0 0 0 1px rgba(25, 30, 42, 0.1) inset, 0 1px 2px rgba(25, 30, 42, 0.7) inset")
      (with-inner-prefixes "background-image" "linear-gradient" "top , rgba(37, 43, 48, 0), rgba(37, 43, 48, 0.7)")]

     [".menu-item-icon"
      :z-index "2"
      :background-repeat "no-repeat"
      :background-position "center"]

     (menu-icon "inventory")
     (menu-icon "store")
     (menu-icon "analytic")
     (menu-icon "hndbook")
     (menu-icon "add")

     [".menu-item-text"
      :color "#DDE7E9"
      :font-size "13px"
      :text-align "center"
      :text-decoration "none"
      :z-index "2"]

     [".menu-item-hover"
      :background-color "#7E8797"
      :border-radius "4px"
      :box-shadow "0 1px 1px 0 rgba(0, 0, 0, 0.3), 0 1px 0 0 rgba(0, 0, 0, 0.15), 0 -1px 0 0 rgba(0, 0, 0, 0.05)"
      :opacity "0"
      (with-prefixes "transition" "opacity 0.2s ease-in 0s, transform 0.2s ease-in 0s")
      (with-prefixes "transform" "scale(0.7, 0.7)")
      (with-inner-prefixes "background-image" "linear-gradient" "top , rgba(103, 113, 129, 0), #677181")]

     [".menu-item:hover .menu-item-hover"
      :opacity "1"
      (with-prefixes "transform" "scale(1, 1)")]

     [".menu-item.selected:hover .menu-item-hover"
      :display "none"
      :opacity "0"]

     [".submenu"
      :background-color "#30373D"
      :z-index "290"
      :color "white"
      :box-shadow "3px 0 4px 0 rgba(0, 0, 0, 0.4)"
      :padding "10px 15px"]

     [".submenu-item"
      :font-size "14px"
      :color "#92a1ae"]

     [".submenu-item:hover"
      :color "white"]

     [".submenu-popup"
      (with-prefixes "box-sizing" "content-box")
      :background-color "#30373D"
      :border-radius "2px"
      :padding-left "6px"
      :padding-top "6px"
      :width "334px"]

     [".submenu-popup-item"
      (with-prefixes "box-sizing" "border-box")
      :background-color "#27282A"
      :border-radius "2px"
      :color "#626666"
      :font-size "14px"
      :padding "10px 10px 0"
      :text-align "center"
      :text-decoration "none"
      :width "105px"
      :float "left"]

     [".submenu-popup-item:hover"
      :background-color "#03ABE5"
      :color "#FFFFFF"]

     [".submenu-popup-icon"
      :background-repeat "no-repeat"]

     [".submenu-arrow"
      :border-color "transparent #30373D transparent transparent"
      :border-image "none"
      :border-style "solid"
      :border-width "20px 20px 20px 0"]

     (submenu-icon "create-user")
     (submenu-icon "create-order")
     (submenu-icon "create-doc")
     (submenu-icon "create-stock")

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Search

     [".search-container"
      :background-color "#2D323C"
      :border-color "#1D1E21 #202125 #232529 #212225"
      :border-style "solid"
      :border-width "1px"
      :border-image "none"
      :border-radius "2px";
      :box-shadow "1px 1px 0 rgba(228, 228, 228, 0.15), 0 0 3px 1px rgba(0, 0, 0, 0.2) inset"
      :line-height "12px"
      :margin "3px 0 0"
      :color "white"
      :padding "3px 30px 3px 5px"
      :vertical-align "top"
      (with-prefixes "transition" "width 0.25s ease-in 0s, background-color 0.25s ease-in 0s")]

     [".search-container:after"
      :background "url('/img/grid-admin/search-icon-light.png') no-repeat scroll 0 0 transparent"
      :content "''"
      :margin "5px -7px 0"]

     [".search-input"
      :background-color "transparent"
      :border "none"
      :box-shadow "none"
      :color "#E3ECEE"
      :font-size "13px"
      :margin "0"
      :outline "none"
      :padding "0"
      :line-height 20]

     [".search-input:focus"
      :background "url('/img/grid-admin/body-bg.png') repeat scroll 0 0 #FAFAFA"
      :z-index "996"
      :color "black!important"
      :line-height 20]

     [".search-container.active"
      :background "url('/img/grid-admin/body-bg.png') repeat scroll 0 0 #FAFAFA"
      :z-index "996"]

     [".search-container.active:after"
      :opacity "0"]

     [".search-container.active .search-icon"
      :opacity "1"]

     [".search-icon"
      :background "url('/img/grid-admin/search-icon-dark.png') no-repeat transparent"
      :cursor "default"
      :opacity "0"
      :margin "5px -7px 0"
      (with-prefixes "transition" "opacity 0.25s ease-in 0s")]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Controls block
     [".controls-block"
      :background "url('/img/grid-admin/bg-body.jpg') repeat scroll 0 0 #E9E9E9"
      :border-bottom "1px solid #CDCDCD"
      :color "#626262"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Styles.css compatibility
     [".inputbf-active, .inputbf-inactive"
      :color "#E3ECEE!important"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Alerts
     ;; (alert "warning-alert" "#FEF7CD" "#F3E4AD" "#AA7D29")
     ;; (alert "error-alert" "#F7E4E4" "#F7CCCC" "#B85355")
     ;; (alert "success-alert" "#E1F2D3" "#CBE7C5" "#5C8847")
     ;; (alert "info-alert" "#D5EEF1" "#C8E0E6" "#4E7B86")

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Tabs
     [".admin-inplace-edit-tabs"
      :cursor :pointer
      :padding "5px 10px"]

     ["input:checked + .custom-radio .admin-inplace-edit-tabs"
      :background "none repeat scroll 0 center transparent"
      :border-bottom-color "#E06337"
      :border-image "none"
      :border-style "none none solid"
      :border-width "0 0 2px"
      :color "#E06337"
      :cursor "default"
      :padding "5px 0 0"
      :margin-left 10
      :margin-right 10]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Buttons
     [".small-cancel-button-plain"
      :background-color "#EEEEEE"
      :background-repeat "repeat-x"
      :border "1px solid #C4C4C4"
      :border-radius "3px"
      :cursor "pointer"
      :padding "5px 15px"
      :box-shadow "0 1px 0 #FFFFFF inset"
      :color "#444444"
      :text-shadow "0 1px 0 #FFFFFF"
      (with-inner-prefixes "background-image" "linear-gradient" "bottom,  #F2F2F2, #E8E8E8")]

     [".small-cancel-button-plain:hover, .small-cancel-button-plain:focus, .small-cancel-button-plain:active, .small-cancel-button-plain.active, .small-cancel-button-plain.disabled, .small-cancel-button-plain[disabled]"
      :background-color "#E8E8E8"
      :background-image "none !important"
      :color "#444444"]

     [".small-cancel-button-plain.disabled, .small-cancel-button-plain[disabled]"
      :background "none repeat scroll 0 0 #EDEDED"
      :border "1px solid #D8D8D8"
      :color "#888888"
      :opacity "1"
      :text-shadow "none !important"]

     [".small-cancel-button-plain.disabled:active, .small-cancel-button-plain[disabled]:active, .small-cancel-button-plain.disabled.active, .small-cancel-button-plain.active[disabled]"
      :background "none repeat scroll 0 0 #EDEDED"
      :box-shadow "none !important"]

     [".small-cancel-button-plain:active, .small-cancel-button-plain.active, .open .dropdown-toggle"
      :background-color "#E5E5E5"
      :box-shadow "0 1px 3px rgba(0, 0, 0, 0.1) inset !important"]

     [".standard-button-width"
      :width "121px"]

     (plain-aliased-button "ok-button" "#3B94E5" "#3DA0EA" "#3983DE" "#3C6791" "#6EA8E6" "#668DB2" "#3580DD")
     (plain-aliased-button "info-button" "#4AC1C8" "#4EC9CE" "#45B4BF" "#36919B" "#7CC3CA" "#6EADB3" "#41B2BE")
     (plain-aliased-button "success-button" "#ACC70A" "#BACF0B" "#98BA09" "#7C9710" "#ACC352" "#98AA4F" "#94B509")
     (plain-aliased-button "warning-button" "#EEA123" "#F2AC1E" "#E7912A" "#C07C1E" "#ECAC58" "#C79856" "#E68F25")
     (plain-aliased-button "danger-button" "#E46837" "#EC7337" "#D85837" "#A1391E" "#E07E63" "#B86A57" "#D75433")

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Dropdown
     (colored-dropdown "blue-dropdown" "#3B94E5" "#3DA0EA" "#3983DE" "#3C6791" "#6EA8E6" "#668DB2" "#3580DD")
     (colored-dropdown "cyan-dropdown" "#4AC1C8" "#4EC9CE" "#45B4BF" "#36919B" "#7CC3CA" "#6EADB3" "#41B2BE")
     (colored-dropdown "green-dropdown" "#ACC70A" "#BACF0B" "#98BA09" "#7C9710" "#ACC352" "#98AA4F" "#94B509")
     (colored-dropdown "orange-dropdown" "#EEA123" "#F2AC1E" "#E7912A" "#C07C1E" "#ECAC58" "#C79856" "#E68F25")
     (colored-dropdown "red-dropdown" "#E46837" "#EC7337" "#D85837" "#A1391E" "#E07E63" "#B86A57" "#D75433")

     [".dk_options"
      :border-radius "3px !important"
      :background-clip "padding-box"
      :background-color "#FFFFFF"
      :border "1px solid #C0C0C0 !important"
      :border-radius "6px"
      :box-shadow "0 1px 4px rgba(102, 102, 102, 0.3) !important"
      :display "none"
      :left "0"
      :list-style "none outside none"
      :margin "5px 0 0 !important"
      :min-width "160px"
      :padding "5px 0"
      :position "absolute !important"
      :top "100%"
      :z-index "1000"]

     [".dk_open .dk_options"
      :display "block"
      :margin-bottom "5px"
      :position "static"]

     [".dk_open, .dk_focus .dk_toggle"
      :box-shadow "none!important"]

     [".dk_toggle"
      :width "100% !important"]

     [".dk_container"
      :background-image "none !important"
      :height 30
      :width "100%"]

     [".dk_options li"
      :line-height "20px"]

     [".dk_options a"
      :clear "both"
      :color "#444444"
      :display "block"
      :font-weight "normal"
      :line-height "20px"
      :padding "3px 20px"
      :white-space "nowrap"
      :background-color "transparent !important"
      :opacity "1 !important"
      :padding "3px 20px !important"
      :border "none !important"]

     [".dk_options_inner li[disabled] a:hover"
      :background-color "#F2F2F2 !important"
      :color "#C4C4C4"
      :cursor :default]

     [".dk_options"
      ["li:hover, .dk_option_current li" :background-color "#3690E6 !important"]
      ["li:hover a, .dk_option_current li a"
       :border "0 none"
       :color "#FFFFFF"
       :cursor :pointer]]

     [".dk_options_inner"
      :border-radius "0 !important"
      :margin-bottom 0]

     [".dk_label"
      :background-image "url('/img/dropdown/dk_simple_arrow.png')"
      :background-repeat "no-repeat"
      :background-position "95%"]

     [".dk_options_inner, .dk_touch .dk_options" :max-height 300]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Pager dropdown
     [".pager-dropdown .dk_toggle"
      :background-color "#FFFFFF"
      :background-repeat "no-repeat"
      (tile-image "/img/dropdown/dk_simple_arrow.png")
      :border "1px solid #DDDDDD"
      :border-radius "3px"
      :cursor "pointer"
      :box-shadow "0 1px 0 rgba(255, 255, 255, 0.3) inset"
      :font-size "14px"
      :padding "3px 7px !important"
      :width "52px !important"
      :color "#464646"]

     [".pager-dropdown .dk_toggle:hover"
      :background-color "#FFFFFF"
      :background-image "none"
      :border-color "#3690E6"
      :color "#444444"]

     [".pager-dropdown .dk_options"
      :min-width "50px !important"
      :margin-top "1px !important"]

     [".pager-dropdown .dk_open .dk_toggle"
      :border-radius "3px"
      :border-color "#3690E6"]

     [".pager-dropdown"
      :position "absolute"
      :top "15px"
      :right "30px"
      :width 50]

     [".pager-dropdown .dk_label"
      :background-image "url('/img/dropdown/dk_simple_black_arrow.png') !important"]

     [".pager-dropdown .dk_options a"
      :padding "3px 0 3px 10px !important"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Pager
     [".pager-main"
      :display "block"
      :margin "13px auto"
      :width "500px"
      :text-align "center"]

     [".pager-main"
      ["span, a"
       :border "1px solid #DDDDDD"
       :border-radius "3px"
       :color "#464646"
       :margin-right "5px"
       :line-height "20px"
       :padding "4px 12px"
       :text-decoration "none"
       :background-color "white"]]

     [".pager-current-link"
      :background "none repeat scroll 0 0 #3690E6 !important"
      :border "none !important"
      :color "#FFFFFF !important"
      :font-weight "700"
      :cursor "default"]

     [".pager-page:hover"
      :border-color "#3690E6"
      :color "#444444"]

     [".pager-non-active"
      :color "#999999 !important"
      :cursor "default"]

     [".pager-prev-arrow, .pager-next-arrow"
      :display "none"]

     [".page-navigation"
      :cursor "default"]

     [".pager-pages-container"
      :border "1px solid #CDCDCD"
      :border-bottom "none"
      :display "block"
      :padding "5px 20px"
      :margin "15px 15px 0"
      :width "inherit !important"
      :border-top-left-radius "4px"
      :border-top-right-radius "4px"
      :height "57px !important"
      (with-inner-prefixes "background-image" "linear-gradient" "bottom,  #F2F2F2, #E8E8E8")]

     [".pager-pages-container-bottom"
      :margin "1px 15px 10px"
      :border-bottom-left-radius "4px"
      :border-bottom-right-radius "4px"
      :border-top-left-radius "0"
      :border-top-right-radius "0"
      :border-top "none"
      :border-bottom "1px solid #DDDDDD"
      (with-inner-prefixes "background-image" "linear-gradient" "bottom,  #E8E8E8, #F2F2F2")]

     [".pager-pages-container-bottom .pager-title"
      :display "none"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Data table
     [".pager-table"
      :background-color "#FFFFFF"
      :border-collapse "collapse"
      :margin "0 15px"]

     [".pager-table tr"
      :height "35px"
      :border "1px solid #DDDDDD"]

     [".pager-table td"
      :padding "0 10px"]

     [".admin-table-head"
      :border "1px solid #CDCDCD"
      :border-top "none"
      :background-color "#e0e0e0"]

     [".full-width-cell"
      :width "100%"]

     [".table > thead > tr > th, .table > tbody > tr > th,
       .table > tfoot > tr > th, .table > thead > tr > td,
       .table > tbody > tr > td, .table > tfoot > tr > td"
      :vertical-align :middle]

     [".table" :margin-bottom 0]

     (fixed-width-cell 50)
     (fixed-width-cell 75)
     (fixed-width-cell 100)
     (fixed-width-cell 125)
     (fixed-width-cell 150)
     (fixed-width-cell 175)
     (fixed-width-cell 200)

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Report CSS
     [".report-item.selection-active > td"
      :background-color "#dff0d8"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Modal window
     [".modal-main-block"
      :background-color "#FFFFFF"]

     [".modal-tab-block"
      :height 385
      :overflow :auto
      :padding-bottom 5]

     [".header-modal"
      :background-color "#F5F5F5"
      :border-radius "6px 6px 0 0"
      :border-bottom "1px solid #DDDDDD"
      :box-shadow "0 1px 0 #FFFFFF inset"
      :margin-top "0"
      :padding "15px 15px 13px"
      :text-align "right"]

     [".modal-close"
      :background "none repeat scroll 0 0 transparent"
      :border "0 none"
      :cursor "pointer"
      :padding "0"
      :color "#000000"
      :font-size "26px"
      :font-weight "bold"
      :opacity "0.2"
      :text-shadow "0 1px 0 #FFFFFF"
      :background-image "none !important"
      :text-align "center"
      :height "20px !important"
      :width "20px !important"
      :position "absolute"
      :right "64px"
      :top "31px"]

     [".modal-close:hover"
      :color "#000000"
      :cursor "pointer"
      :opacity "0.4"
      :text-decoration "none"
      :background-image "none !important"]

     [".footer-modal"
      :background-color "#F5F5F5"
      :border-radius "0 0 6px 6px"
      :border-top "1px solid #DDDDDD"
      :box-shadow "0 1px 0 #FFFFFF inset"
      :margin-bottom "0"
      :padding "14px 15px 15px"
      :text-align "right"]

     [".nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus"
      :border-bottom "2px solid #3690E6"
      :border-top :none
      :border-left :none
      :border-right :none
      :color "#3690E6"
      :outline :none]

     [".nav > li > a"
      :padding 0
      :margin "0 15px 5px"
      :border-top :none
      :border-right :none
      :border-left :none]

     [".nav > li > a:hover, .nav > li > a:focus"
      :border-bottom "2px solid gray"
      :margin "0 15px 5px"
      :color :gray
      :background-color :transparent
      :border-top :none
      :border-right :none
      :border-left :none]

     [".nav-tabs" :border-bottom :none]

     [".ng-cloak" :display "none"]

     [".faded-item"
      :color :white
      :padding 10
      :background-color "rgba(0, 0, 0, 0.65)"
      :top 0
      :left 0
      :width "100%"
      :height "100%"]

     [".table-style"
      :border-top "1px solid #DDDDDD"
      :padding 5
      :vertical-align :middle]

     [".header-btn"
      :background "-moz-linear-gradient(center top , #202736, #161B26) repeat scroll 0 0 rgba(0, 0, 0, 0)"
      :border "1px solid #151618"
      :border-radius 4
      :box-shadow "1px 1px 0 0 #293244 inset, -1px -1px 0 0 #191D29 inset"
      :cursor :pointer
      :display :inline-block
      :height 28
      :margin-top 3
      :vertical-align :top]

     [".header-btn:hover"
      :background "-moz-linear-gradient(center top , #424957, #2F333E) repeat scroll 0 0 rgba(0, 0, 0, 0);"
      :box-shadow "1px 1px 0 0 #555E6E inset, -1px -1px 0 0 #333843 inset"]

     [".header-btn-text"
      :color "#E3EBED"
      :display :inline-block
      :line-height 14
      :margin "5px 10px 0 0"
      :min-width 11
      :text-align :right
      :vertical-align :top]

     [".glyphicon-domain"
      :color :white
      :margin-left 8
      :margin-right 8
      :margin-top 5]

     [".domain-disabled"
      [".domain-item-title"
      :background "url('/img/grid-admin/domain-title-bg-disable.gif') repeat scroll 0 0 #90A0A9"
      :border-color "#6B7C8A #7C8D98 #859198"
      :box-shadow "0 1px 1px rgba(22, 50, 93, 0.3) inset, 0 1px 0 rgba(255, 255, 255, 0.4) !important"
      :text-shadow "0 1px #345677"]

      [".domain-item-body"
       :background-color "#CCD7D9"
       :color "#394C5B"
       :box-shadow "0 0 1px rgba(22, 50, 93, 0.3) inset, 0 1px 0 #FFFFFF"
       :border-left "1px solid #B4C1C7"
       :border-right "1px solid #B4C1C7"]]

     [".domain-block"
      :width 250
      :position :absolute
      :top 45
      :right 15
      :background "none repeat scroll 0 0 #EBF0F2"
      :border "1px solid #B2BEC8"
      :border-radius 4
      :box-shadow "0 19px 18px 0 rgba(72, 93, 99, 0.45) !important"
      :opacity "1"
      :transition "opacity 0.2s ease-out 0s"
      :z-index "1000"
      :padding-top 7]

     [".domain-item"
      :padding "0 8px 9px"]

     [".domain-item-title"
      :background "url('/img/grid-admin/domain-title-bg.gif') repeat scroll 0 0 #7A9EC0"
      :border-color "#5A7A9E #6A8CAE #7690A8"
      :border-image "none"
      :border-style "solid"
      :border-width 1
      :box-shadow "0 1px 1px rgba(22, 50, 93, 0.3) inset, 0 1px 0 rgba(255, 255, 255, 0.4)"
      :text-shadow "0 1px #345677"
      :border-radius "4px 4px 0 0"
      :box-shadow "0 1px 0 rgba(255, 255, 255, 0.4)"
      :color "#FFFFFF"
      :height 30
      :padding "3px 0 7px 10px"
      :position :relative]

     [".domain-item-body"
      :background "none repeat scroll 0 0 #BFD5E9"
      :border-image :none
      :border-left "1px solid #A8BFD6"
      :border-right "1px solid #A8BFD6"
      :border-style "none solid"
      :border-width "medium 1px"
      :box-shadow "0 0 1px rgba(22, 50, 93, 0.3) inset, 0 1px 0 #FFFFFF"
      :border-radius "0 0 4px 4px"
      :padding "3px 0 7px 10px"
      :box-shadow "0 1px 0 #FFFFFF"]

     [".domain-arrow"
      :border-color "#EBF0F2 rgba(0, 0, 0, 0)"
      :border-image :none
      :border-style :solid
      :border-width "0 11px 12px"
      :height 0
      :left 183
      :margin-top -119
      :position :absolute
      :top "50%"
      :width 0]

     [".top-border"
      :padding "10px 15px"
      :border-top "1px solid rgba(0, 0, 0, 0)"
      :border-color "#DDDDDD"
      :color "#333333"])
