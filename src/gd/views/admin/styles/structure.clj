(ns gd.views.admin.styles.structure
  (:use gd.utils.common)
  (:use gd.utils.styles)
  (:use gd.views.styles.utils)
  (:use gd.views.styles.common))

(css "resources/public/css/grid/structure.css"

     ["body"
      :padding "62px 0 0 80px"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Navigation bar

     [".navbar"
      :left 0
      :border-radius 0
      :margin-bottom 0
      :position "fixed"
      :right 0
      :z-index "930"
      :border :none]

     [".navbar-container"
      :height "55px"]

     [".navbar-border"
      :min-height "7px"
      :height "7px"
      :width "100%"
      :z-index "920"]

     ["header .navbar-container"
      :top 0]

     ["header .navbar-border"
      :top 55]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Navigation menu

     [".navmenu"
      :height "100%"
      :position "fixed"
      :top 0
      :z-index "910"]

     [".navmenu-container"
      :width "80px"]

     [".navmenu-container ul"
      :padding-top "62px"]

     [".menu-item"
      :display "block"
      :height "80px"
      :margin "7px 5px 0"
      :position "relative"
      :width "70px"]

     [".menu-item-icon"
      :height "49px"
      :position "relative"]

     [".menu-item-text"
      :padding "0 2px"
      :position "relative"]

     [".menu-item.selected"
      :height "80px"
      :width "70px"]

     [".menu-item-hover"
      :height "77px"
      :left 0
      :position "absolute"
      :top 0
      :width "68px"]

     [".navigation-offset"
      :margin-left "210px"]

     [".submenu"
      :bottom "0"
      :position "fixed"
      :top "62px"
      :left "80px"]

     [".submenu-item"
      :width "180px"]

     [".submenu-popup"
      :position "absolute"
      :margin-left "24px"
      :margin-top "0px"]

     [".submenu-popup-item"
      :display "block"
      :height "105px"
      :margin-right "6px"
      :margin-bottom "6px"]

     [".submenu-popup-icon"
      :height "32px"
      :width "32px"
      :margin-bottom "10px"]

     [".submenu-arrow"
      :height 0
      :left "75px"
      :margin-top "-20px"
      :position "absolute"
      :top "50%"
      :width 0]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Search

     [".search-container"
      :display "inline-block"
      :overflow "hidden"
      :position "relative"
      :width "200px"]

     [".search-container:after"
      :display "block"
      :height "26px"
      :position "absolute"
      :right "0"
      :top "0"
      :width "30px"]

     [".search-input"
      :height "19px"
      :width "100%"]

     [".search-input:focus"
      :width "350px"]

     [".search-container.active"
      :width "350px"]

     [".search-icon"
      :height "26px"
      :position :absolute
      :right "0"
      :top "0"
      :width "30px"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Controls block

     [".controls-block"
      :min-height "20px"
      :padding "15px"])
