(ns gd.views.resources
  (:use noir.core
        clojure.algo.generic.functor
        [gd.utils.common :only (exists?)]
        clojure.test
        gd.utils.test
        gd.utils.web
        gd.utils.graph
        gd.utils.common
        hiccup.page
        hiccup.element
        hiccup.core
        com.reasonr.scriptjure)
  (:require
   [noir.session :as session]
   [gd.views.security :as security]))

(defn require-multiple-css[& names]
  (doseq [lib names]
    (require-css :user lib)))

(run-tests)
