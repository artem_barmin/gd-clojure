(ns gd.views.user.notifications
  (:use
   gd.views.notifications
   gd.views.project_components
   gd.utils.mail
   gd.views.messages
   gd.utils.pub_sub
   [clostache.parser :only (render-resource)]
   clojure.walk
   gd.utils.basic-templates
   gd.model.model
   gd.utils.sms
   com.reasonr.scriptjure
   hiccup.element
   noir.core
   gd.utils.web
   hiccup.form
   gd.views.components
   gd.utils.common
   hiccup.core
   hiccup.page
   net.cgrand.enlive-html)
  (:require
   [gd.views.security :as security]
   [noir.session :as session]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Notification rendering subsystem(will be used for different targets - html, sms, vk and other)

(defsubstag user-name
  (security/link-navigate (:user *current-context*)))

(defsubstag forum-theme-link
  (link-to {:class "link"}
           (url-for forum-entry {:id (:id (:theme *current-context*))})
           (:name (:theme *current-context*))))

(defsubstag deal-link
  (link-to {:class "link"}
           (url-for deal-page {:id (:id (:deal *current-context*))})
           (:name (:deal *current-context*))))

(defsubstag stock-link
  (link-to {:class "link"}
           (url-for stock-shortcut {:id (:id (:stock *current-context*))})
           (:name (:stock *current-context*))))

(defmethod render-notification :default [type info source]
  {:user "undefined"
   :body (str info source)
   :tags "undefined"})

(defmethod render-notification :new-user-order [_ {:keys [author]} {:keys [group-deal name]}]
  {:user (user:get-full author)
   :body (list "Пользователь " (user-name) " сделал новый заказ в " (deal-link))
   :tags (:name group-deal)
   :context {:deal group-deal}})

(defmethod render-notification :deal-order-change-status [_ {:keys [new-status]} {:keys [group-deal order-name]}]
  {:user (:organizer group-deal)
   :body (if (empty? new-status)
           (list "Организатор " (user-name) " изменил статус СП " (deal-link))
           (list "Организатор " (user-name) " перевел закупку " order-name " на статус "
                 [:span.blue (! :deal.status new-status)]))
   :tags (:name group-deal)
   :context {:deal group-deal}})

(defmethod render-notification :new-forum-message [_ {:keys [text id author]} theme]
  {:user (user:get-full author)
   :body (list "Пользователь " (user-name) " оставил новое сообщение в теме " (forum-theme-link))
   :tags "Форум"
   :context {:theme theme}})

(defmethod render-notification :new-user-order-owner [_ _ {:keys [deal-order]}]
  {:user (security/logged)
   :body (list "Вы совершили заказ в СП " (deal-link))
   :tags (:name (:group-deal deal-order))
   :context {:deal (:group-deal deal-order)}})

(defmethod render-notification :user-invite-use [_ {:keys [user]} _]
  {:user (user:get-full user)
   :body (list "Пользователь " (user-name) " зарегистрировался по вашему приглашению")
   :tags "Приглашения"})

(defmethod render-notification :new-private-message [_ {:keys [text]} user]
  {:user user
   :body (list "Пользователь " (user-name) " послал вам личное сообщение")
   :tags "Личное сообщение"})

(defmethod render-notification :reject-user-order-item [_ {:keys [stock]} user-order]
  (let [stock (when stock (stock:get-stock stock))]
    {:stock stock
     :user (when-not stock (:user user-order))
     :body (list "Товара " (stock-link) " нет в наличии")
     :tags (:name (:group-deal stock))
     :context {:stock stock}}))

(defmethod render-notification :payment-request [_ {:keys [sum date comment]} user-order]
  {:user (:user user-order)
   :body (list "Пользователь " (user-name) " сообщил об оплате. "
               "Сумма: " sum " грн. "
               "Время: " (format-date (java.util.Date. date)) " "
               (when (seq (last comment)) (list "Коментарий:" (last comment))) " "
               " Вам необходимо подтвердить или опровергнуть факт оплаты со страницы "
               [:a.link {:href "http://dayte-dve.com.ua/user/orders/"} "управления заказами."])
   :tags (:name (:group-deal (:deal-order user-order)))})

(defmethod render-notification :payment-reject [_ {:keys [comment]} user-order]
  {:user (:organizer (:group-deal (:deal-order user-order)))
   :body (list "Организатор " (user-name) " не подтвердил факт оплаты. "
               (when (seq comment) (list "Коментарий:" comment))
               " Вы можете повторно сообщить об оплате(уточнить) с корзины.")
   :tags (:name (:group-deal (:deal-order user-order)))})

(defmethod render-notification :payment-confirm [_ {:keys [comment]} user-order]
  {:user (:organizer (:group-deal (:deal-order user-order)))
   :body (list "Организатор " (user-name) " подтвердил факт оплаты. ")
   :tags (:name (:group-deal (:deal-order user-order)))})

(defmethod render-notification :payment-confirm [_ {:keys [comment]} user-order]
  {:user (:organizer (:group-deal (:deal-order user-order)))
   :body (list "Организатор " (user-name) " подтвердил факт оплаты. ")
   :tags (:name (:group-deal (:deal-order user-order)))})

(defmethod render-notification :money-transaction [_ {:keys [amount comment]} user]
  {:user user
   :body (list "Пользователь " (user-name) " перевел на ваш счет " [:span.blue amount] " грн.")
   :tags "Личный счет"})
