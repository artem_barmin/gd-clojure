(ns gd.views.bucket.simple
  (:use gd.views.resources
        gd.log
        compojure.core
        gd.model.model
        gd.views.project_components
        gd.utils.web
        gd.utils.common
        gd.views.components
        noir.core
        [noir.response :only (redirect)]
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(comment "Provides simple API for in-memory bucket")

(defn bucket-session-key[]
  :saved-retail-orders)

(def id-generator (atom 0))

;; TODO : we need persistent id generator, because after restart items may be assigned old ID
(defn- gen-id[]
  (swap! id-generator inc))

(defn- find-item-by-id[item-id]
  (find-first (comp (partial = item-id) :id) (session/get (bucket-session-key))))

(defn bucket:prepare-order-items[items]
  (let [stocks (group-by-single :id (retail:get-multiples-stocks (map :fkey_stock items)))]
    (map (fn[{:keys [fkey_stock quantity id note status] :as order-item}]
           (orders:user-order-stock-parameter-post
            {:id id
             :stock (get stocks fkey_stock)
             :note note
             :fkey_stock fkey_stock
             :quantity quantity
             :closed 0
             :status (keyword status)
             :stock-parameter-value (map (fn[[name value]] {:name name :value value})
                                         (dissoc order-item :id :quantity :fkey_stock))}))
         items)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bucket manipulation

(defn bucket:add-to-bucket[order-item]
  (let [item-to-search (dissoc order-item :quantity :id)
        all-items (session/get (bucket-session-key))]
    (session/put!
     (bucket-session-key)
     (if-let [{:keys [id]} (find-first (fn[item]
                                         (= item-to-search (dissoc item :quantity :id)))
                                       all-items)]
       (map (fn[item]
              (if (= id (:id item))
                (update-in item [:quantity] + (:quantity order-item))
                item))
            all-items)
       (cons (merge order-item {:id (gen-id)}) all-items)))
    (info "Stock added to retail bucket" order-item)))

(defn bucket:change-item[item-id params]
  (let [params (dissoc params :service)
        valid-params (if (< (parse-int (:quantity params)) 0) (assoc params :quantity 0) params)]
    (prn valid-params)
    (when (find-item-by-id item-id)
      (session/put! (bucket-session-key)
                    (map (fn[{:keys [id] :as item}]
                           (if (= item-id id)
                             (merge item valid-params)
                             item))
                         (session/get (bucket-session-key)))))))

(defn bucket:delete[id]
  (when (find-item-by-id id)
    (session/put! (bucket-session-key)
                  (remove (comp (partial = id) :id) (session/get (bucket-session-key))))))

(defn bucket:clear[]
  (session/put! (bucket-session-key) nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bucket acessing API

(defn bucket:plain-items[]
  (session/get (bucket-session-key)))

(defn bucket:items[]
  (bucket:prepare-order-items (bucket:plain-items)))

(wrap-request-cache #'bucket:items)

(defn bucket:count[]
  (reduce + (map :quantity (bucket:plain-items))))

(defn bucket:sum
  ([]
     (bucket:sum (bucket:items)))
  ([items]
     (->>
      items
      (map :sum)
      (reduce +))))

(defn bucket:get-item[item-id]
  (find-first (fn[{:keys [id]}] (= item-id id)) (bucket:items)))
