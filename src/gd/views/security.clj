(ns gd.views.security
  (:use
   [noir.response :only [redirect]]
   noir.core
   gd.utils.web
   gd.utils.common
   gd.log
   gd.model.model
   gd.views.components
   clojure.data.json
   com.reasonr.scriptjure
   hiccup.element
   hiccup.page
   hiccup.form
   noir.validation)
  (:require
   [clojure.string :as strings]
   [noir.server :as server]
   [gd.utils.security :as sec]))

(def logged #'sec/logged)
(def root? #'sec/root?)
(def organizer? #'sec/organizer?)
(def admin? #'sec/admin?)
(def manager? #'sec/manager?)
(def partner? #'sec/partner?)
(def get-last-update-time #'sec/get-last-update-time)

(def id #'sec/id)

(defn seo?[& [user]]
  (or (admin?)
      (sec/have-role? user "seo")))

(defn copywriter?[& [user]]
  (or (admin?)
      (sec/have-role? user "copywriter")))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Important checks
(defn active-phone?[]
  (get-in (logged) [:contact :phone :active]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; State updater
(defn update-logged-information
  "Reloads information about currently logged user. Should be used, when user info
is changed during session."
  []
  (when (logged)
    (sec/set-logged (user:get-full (:id (logged))))))

(defn update-logged-information-for-user
  "Reloads info for other user"
  [user-id]
  (sec/set-specific-logged (user:get-full user-id))
  nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Images navigator
(defn image-nagiate[user size & classes]
  [:a {:href (str "/user/" (:id user))}
   [:img {:class (strings/join " " (map name classes))
          :onclick (js (. event stopPropagation))
          :src (images:get size (:images user))}]])

(defn link-navigate[user & [class]]
  [:a {:href (str "/user/" (:id user))
       :class (or class "link")
       :onclick (js (. event stopPropagation))}
   (:name user)])
