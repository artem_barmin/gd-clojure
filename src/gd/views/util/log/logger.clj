(ns gd.views.util.log.logger
  (:refer-clojure :exclude [extend])
  (:use noir.core
        gd.views.util.common
        monger.operators
        taoensso.timbre
        clojure.data.json
        gd.utils.common
        gd.utils.web
        gd.utils.logger
        gd.views.components
        gd.views.messages
        gd.model.model
        hiccup.page
        hiccup.element
        hiccup.core
        com.reasonr.scriptjure
        noir.util.test
        clj-time.core))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Logger by time

(defn- round-hours[time]
  (-> time (.withMillisOfSecond 0) (.withSecondOfMinute 0) (.withMinuteOfHour 0)))

(defn logger-layout[]
  (admin-plain-layout
    (javascript-tag (str "var data = " (json-str (logger:get-all-messages
                                                  (round-hours (now)) (now)))))
    [:div {:style "height:50px;"}]
    [:div.canvas]
    [:div {:style "position:fixed;top:0;background-color:#999999;width:1200px;border-radius:10px;opacity:0.7"}
     [:table {:style "vertical-align:middle;"}
      [:tr
       [:td {:style "width: 300px;"} "Масштаб"]
       [:td [:div#slider-scale {:style "width:800px;"}]]]
      [:tr
       [:td {:style "width: 300px;"}
        [:div.left {:style "margin-top:8px;margin-right:10px;"} "Диапазон "
         [:span#range-value {:style "color:yellow;opacity:1;"}]]
        [:div.left (small-blue-button "Запрос"
                                      :action (cljs (remote* get-more-logs
                                                             (let [start (c* (getStartRange))
                                                                   end (c* (getEndRange))]
                                                               (logger:get-all-messages
                                                                (round-hours (-> (- start 1) hours ago))
                                                                (if (= end 0) (now) (-> (- end 1) hours ago))))
                                                             :success onLogsLoaded)))]]
       [:td [:div#slider-range {:style "width:800px;"}]]]]]
    [:div#details {:style "position:absolute;display:none;color:white;width:600px;"}
     [:div#content]
     [:div.inspector]]))

(defpage "/retail/admin/logger" {}
  (logger-layout))
