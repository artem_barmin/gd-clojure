(ns gd.views.util.log.rss-log
  (:refer-clojure :exclude [extend])
  (:use noir.core
        noir.exception
        noir.content.defaults
        monger.operators
        taoensso.timbre
        [noir.response :only (set-headers status)]
        [noir.options :only (dev-mode?)]
        clojure.data.json
        gd.utils.common
        gd.utils.web
        gd.utils.logger
        gd.views.components
        gd.views.messages
        gd.model.model
        clj-time.format
        hiccup.page
        hiccup.element
        hiccup.core
        com.reasonr.scriptjure
        noir.util.test
        clj-time.core)
  (:require
   [gd.views.security :as security]
   [monger.json]))

(defn- make-html [{:keys [params source timestamp service result original-exception]}]
  (html
   [:table {:bgcolor "#ff4500" :border 2 :width "100%"}
    [:tr
     [:td {:width "260px"} [:pre (with-out-str (clojure.pprint/pprint source)) timestamp]]
     [:td [:pre
           (when original-exception (str "Exception:" original-exception "\n\n"))
           (with-out-str (clojure.pprint/pprint params))]]]]))

(def date-formatter
  (-> (formatter "EEE, dd MMM yyyy hh:mm:ss +0200")
      (with-zone (org.joda.time.DateTimeZone/getDefault))))

(defn- make-item [{:keys [timestamp service _id result uri request-method] :as event}]
  [:item [:title (h (str (when (= result "error") (str "ERROR: " request-method " " uri "/")) service))]
   [:pubDate (unparse date-formatter timestamp)]
   [:guid _id]
   [:description (str "<![CDATA[" (make-html event) "]]>")]])

(defn- generate-rss [items]
  (str
   "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
   (html [:rss {:version "2.0"}
          [:channel [:title "rss-logger"]
           [:link "http://dayte-dve.com.ua"]
           [:description "Private Logger"]
           items]])))

(def secret-key "4237389476892748913274891274891273891273")

(defn make-feed[session hosts]
  (if (= session secret-key)
    (->> (get-rss hosts)
         (map make-item)
         generate-rss)
    (status 404 nil)))

(defpage "/feed" {session :session}
  (make-feed session ["daite-dve.com.ua" "dayte-dve.com.ua"]))

(defpage "/bono/feed" {session :session}
  (make-feed session ["bono.in.ua"]))
