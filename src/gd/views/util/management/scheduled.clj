(ns gd.views.util.management.scheduled
  (:refer-clojure :exclude [extend])
  (:use noir.core
        [clojure.algo.generic.functor :only (fmap)]
        taoensso.timbre
        [noir.response :only (set-headers status)]
        [noir.options :only (dev-mode?)]
        clojure.data.json
        gd.utils.common
        gd.utils.web
        gd.utils.logger
        gd.views.components
        gd.views.messages
        gd.model.model
        hiccup.page
        gd.utils.db
        hiccup.form
        hiccup.element
        korma.core
        hiccup.core
        com.reasonr.scriptjure
        noir.util.test
        clj-time.core)
  (:use clj-stacktrace.core)
  (:require
   [gd.views.security :as security]
   [monger.json]))

(defn sheduled-tasks[]
  [:table {:style "border:1px solid black;width:100%;"}
   [:tr
    [:td "id"] [:td "status"] [:td "type"] [:td "period"]
    [:td "delay"] [:td "data"] [:td "entity"] [:td "operations"]]
   (map (fn[{:keys [scheduled-info id status type period delay data association]}]
          [:tr
           [:td id]
           [:td {:style (if (and scheduled-info (:scheduled? scheduled-info) @(:scheduled? scheduled-info))
                          "background:green;" "background:red;")} status]
           [:td type]
           [:td
            (when (and scheduled-info (:scheduled? scheduled-info) @(:scheduled? scheduled-info))
              (let [seconds (.getDelay (:job scheduled-info) java.util.concurrent.TimeUnit/SECONDS)]
                [:span {:data-title (tooltip (-> seconds secs from-now))}
                 period " sec.(" seconds " sec to start)"]))]
           [:td delay]
           [:td {:style "width:35%;"} (str (dissoc data :token))]
           [:td {:style "width:15%;"} (str ((juxt :id :name) association))]
           [:td [:a.link {:onclick (cljs (remote* cancel-scheduled-task
                                                  (schedule:stop (s* id))
                                                  :success (fn[](. location reload))))}
                 "Cancel"]]])
        (schedule:all-tasks))])

(defn sheduled-results[]
  (pager scheduled-event-results
         (schedule:results page-size (or (c* offset) 0))
         (fn[{:keys [scheduled-events time status data id]}]
           (let [{:keys [fkey_group-deal type]} scheduled-events]
             [:table {:style "border:1px solid black;width:100%;"}
              [:tr
               [:td id]
               [:td fkey_group-deal "/" type]
               [:td status]
               [:td (format-date time)]
               [:td
                (if (and (= status :error) data)
                  (str ((juxt :class :message) (parse-exception (deserialize data)))))]]]))
         :columns 1
         :page 20))
