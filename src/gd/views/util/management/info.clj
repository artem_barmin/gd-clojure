(ns gd.views.util.management.info
  (:use noir.core
        [clojure.core :exclude [extend]]))

(defpage "/management" {type :type}
  (let [memory (java.lang.management.ManagementFactory/getMemoryMXBean)
        info {:heap (:used (bean (.getHeapMemoryUsage memory)))
              :perm (:used (bean (.getNonHeapMemoryUsage memory)))}]
    {:status 200
     :body (str (get info (keyword type)))}))
