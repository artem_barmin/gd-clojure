(ns gd.views.util.parser.parser
  (:refer-clojure :exclude [extend])
  (:import (java.io PrintWriter))
  (:require [gd.views.security :as security]
            [noir.session :as session])
  (:use noir.core
        gd.views.util.parser.ml
        taoensso.timbre
        [noir.response :only (set-headers status)]
        [noir.options :only (dev-mode?)]
        clojure.data.json
        gd.utils.common
        [clojure.algo.generic.functor :only (fmap)]
        gd.utils.web
        gd.utils.logger
        korma.core
        gd.utils.db
        gd.views.components
        gd.model.model
        hiccup.page
        hiccup.element
        gd.parsers.llapi
        hiccup.core
        hiccup.form
        com.reasonr.scriptjure
        noir.util.test
        clj-time.core))

(deftype Token [left v right]
  Object
  (toString [_]
    v))

(defn token[l v r]
  (Token. l v r))

(defn token->vec[token]
  [(.left token) (.v token) (.right token)])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils
(defn map-tails [f xs]
  (map f (take-while not-empty (iterate rest xs))))

(defn- re-maybe-group[re s]
  (let [res (re-find re s)]
    (if (sequential? res)
      (second res)
      res)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Processing utils

(defn- process-split[description {:keys [split]}]
  (let [split (.replaceAll (if (seq split) split "[.]") "&quot;" "\"")

        create-tokens
        (fn[words]
          (map token
               (cons nil (vec (butlast words)))
               words
               (conj (vec (rest words)) nil)))

        process-single-value
        (fn[values]
          (let [values (maybe-seq values)]
            (remove
             empty?
             (if (= split "none")
               values
               (mapcat
                (fn[v] (map prepare-str (.split v split))) values)))))]
    (fmap (comp create-tokens process-single-value) description)))

(defn- process-parameters[categorized {:keys [param-name param-value param-price]}]
  (let [config (map vector
                    (maybe-seq param-name)
                    (maybe-seq param-value)
                    (maybe-seq param-price))
        config (remove (fn[[name value]] (or (empty? name) (empty? value))) config)]
    (reduce (fn[result [param-name param-value param-price]]
              (update-in result [param-name]
                         (partial map (fn[value]
                                        {:value (re-maybe-group (re-pattern param-value) (str value))
                                         :price (re-maybe-group (re-pattern param-price) (str value))
                                         :original (str value)}))))
            categorized config)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Machine Learning categorizer

(defonce classifier (atom nil))

(defonce cases (atom (list ["" "" "" :other])))

(defn- ml:prepare-cases-for-train[]
  (let [words (set (map second @cases))]
    (map (fn[case]
           (conj (vec (map (fn[v] (or (words v) "")) (take 3 case)))
                 (nth case 3)))
         @cases)))

(defn- ml:add-case
  "Adds new case and rebuild classifier."
  [l v r class]
  (swap! cases conj (conj [l v r] class))
  (swap! cases distinct)
  (reset! classifier (ml:create-classifier (ml:prepare-cases-for-train)))
  nil)

(defn- process-categorize[{:keys [other] :as lines} {:keys []}]
  (merge
   (group-by (fn[token] (ml:classify @classifier (token->vec token)))
             other)
   {:other other}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Whole processor

(defn- process-single-description[description config]
  (-> description
      (process-split config)
      (process-categorize config)
      (process-parameters config)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Rendering

(defn- render-param[{:keys [value price original]}]
  (precompile-html
   [:span
    [:span {:style "background:orange;margin-right:2px;"} value
     (when (seq price) (precompile-html [:span {:style "background:#ffaeb9;"} (list "(" price ")")]))]
    [:span {:style "background:grey;"} original]]))

(defn- render-token[token]
  (let [[l v r] (token->vec token)]
    [:span {:onclick (js
                      (if (.. event ctrlKey)
                        (addOther (clj l) (clj v) (clj r))
                        (addCategory (clj l) (clj v) (clj r))))}
     token]))

(defn- render-result[description config]
  (cond
    (sequential? description) (map (fn[val]
                                     (precompile-html
                                      [:span {:style "border: 1px solid red;margin-left:5px;margin-right:5px;"}
                                       (if (map? val) (render-param val) (render-token val))]))
                                   description)
    (map? description) [:div
                        (map (fn[[k v]]
                               (precompile-html
                                [:div [:span {:style "color:#1B4684;background:#dddddd;"} (name k)]
                                 (render-result v config)]))
                             (dissoc description :other))
                        (render-result (:other description) config)]
    true [:span {:style "border: 1px solid red;margin-left:5px;margin-right:5px;"}
          (with-out-str (clojure.pprint/pprint (str description)))]))

(defn- process-descriptions[deal-id config]
  (map (fn[{:keys [id meta-info description]}]
          {:result
           (process-single-description
            (merge
             (when (map? description)
               (clojure.walk/stringify-keys (dissoc description :other)))
             {:other (:description meta-info)})
            config)
           :meta-info meta-info
           :id id})
        (select stock
          (fields-only :id :meta-info :description)
          (where {:fkey_group-deal (parse-int deal-id)})
          (limit (or (parsei (:limit config) :null) 100)))))

(defn- descriptions[deal-id config]
  (let [parsed (process-descriptions deal-id config)]
    [:div

     [:div

      [:div
       (map
        (fn[[val ids]]
          [:div {:style "margin-left:10px;"} val
           (drop-down {:onclick (js (set! (.. location hash) (. ($ this) val)))} :ids (map :id ids))
           (map (fn[v][:span {:style "margin-left:3px;"} v])
                (sort-by count (distinct (map :original ids))))])
        (group-by
         :val
         (mapcat (fn[{:keys [result id]}]
                   (->>
                    result
                    (filter (comp (partial every? map?) second))
                    (mapcat second)
                    (map (fn[val]
                           {:val (:value val)
                            :original (:original val)
                            :id id}))
                    (remove (comp nil? :val))
                    (sort-by :val)))
                 parsed)))]

      "Rest:"
      (->>
       (mapcat (fn[{:keys [result id]}]
                 (->>
                  result
                  (filter (comp (partial every? map?) second))
                  (mapcat second)
                  (map (fn[val] (when (empty? (:value val)) (:original val))))))
               parsed)
       distinct
       (remove nil?)
       (map (fn[val] [:span {:style "background:grey;margin-left:5px;"} val])))]

     (map
      (fn[{:keys [meta-info result id]}]
        (when (seq (:description meta-info))
          (precompile-html
           [:div.container {:style "border: 1px solid black;margin:3px;" :id id}
            (render-result result config)
            (link-to {:class "abottom aright"} (:url meta-info) "to")])))
      parsed)]))

(defn- process-config[config]
  (->
   config
   (dissoc :ids)
   (update-in [:category-show-only-box]
              (fn[box](remove nil?
                              (map (fn[a b]
                                     (condp = [a b]
                                       ["value" "true"] "true"
                                       ["true" "value"] nil
                                       "false"))
                                   box (conj (vec (rest box)) "value")))))))

(defn parser-layout[deal-id]
  (xhtml
   [:head
    (include-js "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js")
    (include-js "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js")
    (include-js "/js/lib/tooltip.js")

    (include-js "/js/custom-lib/common.js")
    (include-js "/js/custom-lib/fixed-panel.js")
    (include-js "/utils/js/inspect.jquery.js")
    (include-js "/utils/css/parser.css")

    (include-css "/css/user/styles.css")

    (for [lib [:styles :buttons :main :modal :login]]
      (include-css (str "/css/user/" (name lib) ".css")))]
   [:body {:style "background:white;"}
    [:div {:style "margin:10px;"}

     [:form#config
      (javascript-tag (js (var refresh (fn[]
                                         (clj (remote* parser-rerender-descriptions
                                                       (let [config (process-config (c* (formState "#config")))]
                                                         (session/put! :config config)
                                                         (descriptions (s* deal-id) config))
                                                       :success (fn[result] (. ($ "#result") html result))
                                                       :result :html))))
                          (var addOther (fn[l v r]
                                          (clj (remote* parser-ml-add-other
                                                        (ml:add-case (c* l) (c* v) (c* r) :other)
                                                        :success refresh))))
                          (var addCategory (fn[l v r]
                                             (clj (remote* parser-ml-add-category
                                                           (ml:add-case (c* l) (c* v) (c* r) :size)
                                                           :success refresh))))))
      (let [config (session/get :config)]
        [:div
         [:div "Название" (text-field :name (:name config))]
         [:div "Limit" (text-field {:onkeyup (js (refresh))} :limit (:limit config))]
         [:div "Предложения" (text-field {:onkeyup (js (refresh))} :split (:split config))]
         [:div "Параметры"
          (let [inputs [:param-name :param-value :param-price]]
            (managed-list
             (apply map-longest vector (constantly nil) (map (comp maybe-seq (partial get config)) inputs))
             (fn[values]
               [:div.left
                (map-indexed
                 (fn[i name] (text-field {:onkeyup (js (refresh))} name (nth values i)))
                 inputs)])))]
         [:div.group
          [:div.left
           (small-blue-button "Применить описания"
                              :action (cljs (remote* parser-apply-descriptions
                                                     (let [config (process-config (c* (formState "#config")))
                                                           result (process-descriptions
                                                                   (s* deal-id)
                                                                   (merge config
                                                                          {:limit "99999"}))]
                                                       (doseq [{:keys [result id]} result]
                                                         (let [result (if (= (keys result) [:other])
                                                                        (clojure.string/join ".\n" (:other result))
                                                                        result)]
                                                           (update stock
                                                             (set-fields {:description result})
                                                             (where {:id id})))))))
                              :width 200)]
          [:div.left
           (small-blue-button "Сохранить конфиг"
                              :action (cljs (remote* parser-save-config
                                                     (let [config (process-config (c* (formState "#config")))
                                                           name (:name config)]
                                                       (when (seq name)
                                                         (spit (str "resources/parser-configs/" name)
                                                               (json-str config :escape-unicode false))
                                                         (spit (str "resources/parser-configs/ml-" name)
                                                               (binding [*print-dup* true] (pr-str @cases)))))))
                              :width 200)]
          [:div.left
           (small-blue-button "Сохранить параметры"
                              :action (cljs (remote* parser-save-params
                                                     (let [config (process-config (c* (formState "#config")))
                                                           result (process-descriptions
                                                                   (s* deal-id)
                                                                   (merge config
                                                                          {:limit "99999"}))]
                                                       (doseq [{:keys [result id]} result]
                                                         (doseq [[name values]
                                                                 (filter (comp (partial every? map?) second)
                                                                         result)]
                                                           (when (= name "размер")
                                                             (stock:update-stock
                                                              id {:params {:size (map :value values)}})))))))
                              :width 200)]
          [:div.left
           (drop-down :config-for-loading
                      (cons "Загрузить описание"
                            (map #(.getName %) (.listFiles (new java.io.File "resources/parser-configs/")))))]
          [:div.left
           (small-blue-button "Загрузить описание"
                              :action (cljs (remote* parser-load-config
                                                     (let [config (c* (. ($ "#config-for-loading") val))]
                                                       (session/put! :config
                                                                     (read-json
                                                                      (slurp
                                                                       (str "resources/parser-configs/"
                                                                            (.replaceAll config "[^a-z]+" ""))))))))
                              :width 200)]
          [:div.left
           (small-blue-button "Чистый конфиг"
                              :action (cljs (remote* parser-clean-config
                                                     (session/put! :config nil)))
                              :width 200)]]
         [:div#result (descriptions deal-id config)]])]]]))

(defpage deal-description-parser "/parser/:id" {deal :id}
  (if (security/root?)
    (parser-layout deal)
    (status 404 nil)))
