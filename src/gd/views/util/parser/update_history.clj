(ns gd.views.util.parser.update-history
  (:refer-clojure :exclude [extend])
  (:use noir.core
        [clojure.algo.generic.functor :only (fmap)]
        taoensso.timbre
        [noir.response :only (set-headers status)]
        [noir.options :only (dev-mode?)]
        clojure.data.json
        gd.utils.common
        gd.utils.web
        gd.utils.logger
        gd.views.components
        gd.views.messages
        gd.model.model
        hiccup.page
        hiccup.form
        hiccup.element
        korma.core
        hiccup.core
        com.reasonr.scriptjure
        noir.util.test
        clj-time.core)
  (:require
   [gd.views.security :as security]
   [monger.json]))

(defn draw-category[category-id]
  (clojure.string/join
   " → "
   (reverse (map :name (category:parents category-id)))))

(def organizer-html (create-template "html/organizer-deals.html"))

(defn update-history[container type]
  ;; (process-template
  ;;  organizer-html

  ;;  [:div.left-block-header]
  ;;  "История изменений"

  ;;  [:div.left-block-content]
  ;;  [:div (map (fn[{:keys [parser-name last-update id]}]
  ;;               [:div.organizer-deal-info-container
  ;;                [:a.link {:href (url-for deal-update-log {:id id})}
  ;;                 (format-date last-update)
  ;;                 [:div.margin5l "Парсер:" (or parser-name "без парсера")]
  ;;                 (map
  ;;                  (fn[{:keys [count type]}]
  ;;                    [:div (! :change type) " - " count])
  ;;                  (changes:changes-statistics id))]])
  ;;             (changes:get-all-deal-changes container :type type))])
  )

(defpage deal-update-history "/deal-update-history/:id" {deal :id}
  (update-history deal :deal))

(defpage retail-update-history "/supplier-update-history/:id" {supplier :id}
  (update-history supplier :supplier))

(defn- render-params-changeset[new old]
  (let [[added deleted] (clojure.data/diff (fmap set new) (fmap set old))
        all-keys (set (concat (keys added) (keys deleted)))
        single-param (fn[param]
                       (let [[name price-change deps] (if (sequential? param) param [param])]
                         [:span {:style "border:1px solid black;"}
                          name (when price-change [:span {:style "background-color:white;"}
                                                   " (+" price-change "грн. )"])]))]

    (for [key all-keys]
      [:div (name key) ":"
       [:span {:style "background-color:red;"} (map single-param (key deleted))]
       [:span {:style "background-color:green;"} (map single-param (key added))]])))

(defn- render-changeset[changeset]
  (interpose [:hr]
             (map (fn[[key {:keys [new old]}]]
                    (cond (= key :params)
                          [:div.group
                           [:div.left "Params:"]
                           [:div.left {:style "border:1px solid black;"} (render-params-changeset new old)]]

                          (= key :meta-info)
                          (let [render-url (fn[{:keys [url]}][:a {:href url} url])]
                            [:div
                             [:div (name key) ":"
                              (str (or (dissoc old :url) "[empty]")) " → " (str (dissoc new :url))]
                             [:div "URL :" (render-url old) " → " (render-url new)]])

                          (= key :fkey_category)
                          [:div.group
                           "Category:"
                           [:span {:style "background:#aFC125;"} (draw-category old)]
                           " → "
                           [:span {:style "background:#aFC125;"} (draw-category new)]]

                          true
                          [:div (name key) ":" (str (or old "[empty]")) " → " (str new)]))
                  changeset)))

(defn- render-reasons[reasons]
  (map (fn[{:keys [type value]}]
         [:div type "(" value ")"])
       reasons))

;; (defpage deal-update-log "/update-history-log/:id/" {change :id}
;;   (process-template
;;    organizer-html

;;    [:div.left-block-header]
;;    "История изменений"

;;    [:div.left-block-content]
;;    [:div ;; (pager update-history-log
;;          ;;        (changes:get-all-deal-changes-log (s* change)
;;          ;;                                          :filters (maybe-keyword-seq
;;          ;;                                                    (:state (c* (formState ($ "#status-filter")))))
;;          ;;                                          :properties (maybe-seq
;;          ;;                                                       (:field (c* (formState ($ "#status-filter")))))
;;          ;;                                          :limit-num page-size
;;          ;;                                          :offset-num (c* offset))
;;          ;;        (fn[{:keys [stock changeset type reasons new-params]}]
;;          ;;          (if (= type :question)
;;          ;;            [:div.text-left.group {:style "border:2px solid grey;"}
;;          ;;             [:dif.left
;;          ;;              [:a {:href ((comp :url :meta-info) stock)}
;;          ;;               (map
;;          ;;                (fn[im]
;;          ;;                  [:div [:img {:src (images:get :stock-deals im)}]])
;;          ;;                (:images stock))]]
;;          ;;             [:div.left {:style "width:520px;"}
;;          ;;              (:name stock)
;;          ;;              [:div "Причины:"
;;          ;;               (map (fn[{:keys [similarity-reasons similarity stock] :as params}]
;;          ;;                      [:div {:style "border:1px solid black;"}
;;          ;;                       "Similarity:" similarity
;;          ;;                       (render-reasons similarity-reasons)
;;          ;;                       (let [{:keys [name images meta-info] :as s} stock]
;;          ;;                         [:div
;;          ;;                          [:a {:href (:url meta-info)} name]
;;          ;;                          (map (fn[im][:img {:src (images:get :stock-medium im)}]) images)])])
;;          ;;                    new-params)]]]
;;          ;;            [:div.text-left.group {:style (str
;;          ;;                                           "border:1px solid grey;"
;;          ;;                                           (case type
;;          ;;                                             :add "background-color:#90ee90;"
;;          ;;                                             :restore "background-color:#9400d3;"
;;          ;;                                             :delete "background-color:#e9967a;"
;;          ;;                                             :change "background-color:#ffc125;"))}
;;          ;;             [:dif.left
;;          ;;              (when stock
;;          ;;                [:a {:href ((comp :url :meta-info) stock)}
;;          ;;                 [:img {:src (images:get :stock-deals (first (:images stock)))}]])]
;;          ;;             [:div.left {:style "width:520px;"}
;;          ;;              (:name stock)
;;          ;;              [:div {:style "border-bottom:3px solid black;"}
;;          ;;               "Изменения:" (render-changeset changeset)]
;;          ;;              [:div "Причины:" (render-reasons reasons)]]]))
;;          ;;        :columns 1)
;;     ]

;;    [:div#filters]
;;    (let [{:keys [fkey_group-deal fkey_supplier dry-run]} (changes:get-change change)
;;          up-url (if fkey_supplier
;;                   (url-for retail-update-history {:id fkey_supplier})
;;                   (url-for deal-update-history {:id fkey_group-deal}))]
;;      [:form#status-filter
;;       [:a.link {:href up-url}
;;        "К списку всех изменений"]
;;       (when dry-run
;;         [:div.warning-info.text-left "Парсер был запущен в режиме dry-run"
;;          (red-button "Выполнить по настоящему"
;;                      :width 250
;;                      :action (cljs (remote* apply-all-dry-run-changes
;;                                             (changes:apply-dry-run-changes (s* change)))))
;;          (green-button "Удалить"
;;                        :width 250
;;                        :action (cljs (remote* delete-all-dry-run-changes
;;                                               (changes:delete-this-change (s* change))
;;                                               :success (fn[]
;;                                                          (set! (.. location href)
;;                                                                (clj up-url))))))])
;;       (map
;;        (fn[{:keys [count type]}]
;;          [:div (check-box {:onclick (js (updateHistoryLogLoadPage 0))} :state true type)
;;           (! :change type) "(" count ")"])
;;        (changes:changes-statistics change))

;;       [:hr]

;;       (map
;;        (fn[{:keys [field-name count]}]
;;          [:div
;;           (check-box {:onclick (js (updateHistoryLogLoadPage 0))} :field false field-name)
;;           field-name "(" count ")"])
;;        (changes:changes-properties-statistics change))])))
