(ns gd.views.util.parser.schedule
  (:use gd.model.model
        gd.parsers.view.process
        gd.log))

(defonce current-parser (atom nil))

(defmethod schedule:execute-job :parser-execution [_ {:keys [supplier]} container-id]
  (let [type (if supplier :fkey_supplier :fkey_group-deal)
        container-id (or supplier container-id)]
    ;; (info "Start parsing" container-id
    ;;       (if supplier "supplier" "deal")
    ;;       "dry-run: " (or (parser-info:dry-run type container-id) "NO"))
    ;; (load-parser-or-config            ;we must load parser syncroniously
    ;;  container-id
    ;;  (parser-info:parser-name type container-id)
    ;;  current-parser
    ;;  supplier
    ;;  true
    ;;  :dry-run (parser-info:dry-run type container-id))
    ))
