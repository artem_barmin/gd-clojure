(ns gd.views.util.parser.ml
  (:use gd.utils.common)
  (:use clojure.test)
  (:use clj-ml.data)
  (:use clj-ml.filters)
  (:use clj-ml.classifiers)
  (:import (weka.core Instance Instances FastVector Attribute)
           (cljml ClojureInstances)))

(defn process-strings[strings & [external-dictionary]]
  (let [dictionary (atom (or external-dictionary #{}))
        process-word (fn[word]
                       (let [word (.toLowerCase (or word ""))]
                         (if external-dictionary
                           (or (external-dictionary word) "")
                           (and (swap! dictionary conj word) word))))
        words (map! (fn[row](doall (concat (map process-word (take 3 row))
                                           (drop 3 row))))
                    strings)]
    {:words words :dict @dictionary}))

(defn attr-values[attr]
  (reduce merge (map (fn[i]{i (.value attr i)}) (range 0 (.numValues attr)))))

(defn ml:classify[{:keys [classifier dictionary dataset]} ngramm]
  (let [values
        (attr-values (first (filter (fn[a] (= (.name a) "class"))
                                    (nominal-attributes dataset))))]
    (->>
     (process-strings [ngramm] dictionary)
     :words
     first
     (make-instance dataset)
     (classifier-classify classifier)
     Math/round
     (get values))))

(defn ml:create-classifier[strings]
  (let [c (make-classifier :support-vector-machine :smo)
        {:keys [words dict]}
        (process-strings strings)

        values (vec dict)

        ds (make-dataset
            "test"
            [{:before values}
             {:word values}
             {:after values}
             {:class [:size :other]}]
            words)]
    (dataset-set-class ds 3)
    (classifier-train c ds)
    {:classifier c :dictionary dict :dataset ds}))

(defn ml:check-classifier[{:keys [classifier dataset]}]
  (let [res (classifier-evaluate classifier :cross-validation dataset 10)]
    (println (:confusion-matrix res))
    (clojure.pprint/pprint
     (select-keys res
                  [:incorrect :percentage-incorrect
                   :correct :percentage-correct
                   :unclassified
                   :false-negative-rate
                   :precision
                   :kappa]))))

(deftest classifier-test
  (let [classifier
        (ml:create-classifier [["р-р" "С-М" "" :size]
                            ["р-р" "С=М" "" :size]

                            ["р-р" "С" "" :size]
                            ["р-р" "М" "С" :size]
                            ["р-р" "М" "" :size]
                            ["р-р" "Л" "М" :size]
                            ["р-р" "Л" "" :size]
                            ["р-р" "Л" nil :size]

                            ["S" "M" "L" :size]
                            ["M" "L" "" :size]
                            ["р-р" "S" "" :size]
                            ["размер" "S" "" :size]
                            ["размеры" "S" "" :size]

                            ["размер" "ткань" "х.б." :other]
                            ["" "ткань" "вискоза" :other]
                            ["" "ткань" "масло" :other]])]
    (let [class (partial ml:classify classifier)]
      (are [x y] (= x y)
           "size" (class ["S" "M" "L"])
           "size" (class [nil "M" "L"])
           "other" (class ["размер" "ткань" "вискоза"])
           "other" (class ["размер" "ткань" "вискоза123"])
           "size" (class ["размер" "L" ""])
           "size" (class ["размер" "M" ""])))))

(run-tests)
