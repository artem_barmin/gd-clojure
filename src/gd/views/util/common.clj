(ns gd.views.util.common
  (:refer-clojure :exclude [extend])
  (:use monger.operators
        [noir.response :only (status)]
        gd.utils.common
        gd.utils.web
        gd.model.model
        gd.model.context
        hiccup.page
        hiccup.element
        noir.core
        hiccup.core
        com.reasonr.scriptjure
        noir.util.test
        clj-time.core)
  (:require
   [gd.views.security :as security]
   [monger.json]))

(defn admin-plain-layout[& body]
  (if (security/root?)
    (xhtml
     [:head
      (include-js "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js")
      (include-js "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js")
      (include-js "/js/lib/tooltip.js")

      (include-js "/js/custom-lib/common.js")
      (include-js "/utils/js/inspect.jquery.js")
      (include-js "http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js")
      (include-js "/utils/js/raphael.group.js")
      (include-js "/utils/js/doT.js")
      (include-js "/utils/js/logger.js")
      (include-js "/js/custom-lib/modal-panel.js")

      (include-css "/css/user/styles.css")
      (include-css "/css/user/main.css")
      (include-css "/css/user/buttons.css")
      (include-css "/css/user/components.css")

      (include-css "/utils/css/jquery-ui-1.8.23.custom.css")
      (include-css "/utils/css/inspect.css")]

     [:body {:style "margin:0;padding:0;background:white;"}
      body])
    (status 404 nil)))
