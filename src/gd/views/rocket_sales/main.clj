(ns gd.views.rocket_sales.main
  (:use gd.views.resources
        compojure.core
        gd.utils.sms
        gd.utils.mail
        gd.utils.web
        gd.utils.common
        gd.views.components
        noir.core
        [noir.options :only (dev-mode?)]
        [noir.response :only (redirect status set-headers)]
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

(defpage "/rocket-sales/favicon.ico" {}
  (let [name "resources/public/img/group-deals/favicon.ico"]
    (set-headers
     {"Content-Type" "image/x-icon"
      "Content-Length" (str (.length (new java.io.File name)))
      "Expires" "Thu, 15 Apr 2020 20:00:00 GMT"}
     (clojure.java.io/input-stream name))))

(defn toggle-info[additional-info]
  [:span.additional-info-container
   [:div.additional-info-link {:title additional-info} "Подробнее..."]])

(defn send-form[]
  [:form {:action "/send" :method "post"}
   [:div
    (text-field* :partner-site "Адрес сайта")]
   [:div
    (text-field* :partner-phone "Контактный телефон")
    (text-field* :partner-name "Имя, Фамилия")]
   [:div.send-contacts.landing-button {:onclick (js (.submit (.closest ($ this) "form")))} "Стать партнером"]])

(defpage [:post "/rocket-sales/send"] {:keys [partner-site partner-phone partner-name]}
  (let [body (strings/join " " [partner-site partner-phone partner-name])]
    (send-sms-unsafe "+380634846336" body)
    (send-sms-unsafe "+380509790587" body)
    (send-mail "artem.barmin@gmail.com" (str partner-site " на крючке") body)
    (send-mail "koval111vladislav@gmail.com" (str partner-site " на крючке") body))
  (html
   (include-css "/css/group-deals/group-deal.css")
   (include-css "/css/user/main.css")
   (include-css "http://fonts.googleapis.com/css?family=PT+Sans&subset=cyrillic")
   [:div.streetch-background-block]
   [:div.fixed-background-block
    [:div.holder980px-width
     [:div.big-text-holder {:style "padding-top:20%;"}
      [:h2 "Спасибо за проявленный интерес!"]
      [:h3 "Мы свяжемся с Вами в ближайшее время"]]
     [:a.back-button {:href "/"} "Вернуться назад"]]]))

(defn yandex-metrika[]
  "<script type='text/javascript'>
     (function (d, w, c) {
                          (w[c] = w[c] || []).push(function() {
                                                               try {
                                                                    w.yaCounter22485805 = new Ya.Metrika({id:22485805,
                                                                                                          webvisor:true,
                                                                                                          clickmap:true});
                                                                    } catch(e) { }
                                                               });

                          var n = d.getElementsByTagName('script')[0],
                          s = d.createElement('script'),
                          f = function () { n.parentNode.insertBefore(s, n); };
                                           s.type = 'text/javascript';
                                           s.async = true;
                                           s.src = (d.location.protocol == 'https:' ? 'https:' : 'http:') + '//mc.yandex.ru/metrika/watch.js';

                                           if (w.opera == '[object Opera]') {
                                                                             d.addEventListener('DOMContentLoaded', f, false);
                                                                             } else { f(); }
                                                                                     })(document, window, 'yandex_metrika_callbacks');
                                                                                       </script>")

(defpage "/rocket-sales/" []
  (html
   (xml-declaration "UTF-8")
   ;; (doctype :xhtml-strict)
   [:html {:xmlns "http://www.w3.org/1999/xhtml"
           "xmlns:og" "http://ogp.me/ns#"
           "xml:lang" "ru"
           :lang "ru"}
    [:object {:type "image/svg+xml" :data "/img/group-deals/ribbon_blue.svg"
              :width 1 :height 1
              :style "width: 0px; height: 0px; position: absolute;visibility:hidden;"}]
    [:head
     [:title "Увеличьте продажи вместе с Rocket Sales"]
     (yandex-metrika)
     (include-js "http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js")
     (include-js "/js/lib/tooltip.js")
     (include-js "/js/custom-lib/inputbf.js")
     (include-css "/css/group-deals/group-deal.css")
     (include-css "/css/user/main.css")
     (include-css "http://fonts.googleapis.com/css?family=PT+Sans&subset=cyrillic")
     [:link {:rel "icon" :href "/img/group-deals/favicon.ico" :type "image/x-icon"}]
     [:link {:rel "shortcut icon" :href "/img/group-deals/favicon.ico" :type "image/x-icon"}]
     [:link {:rel "shortcut icon" :href "/img/group-deals/favicon.ico" :type "image/vnd.microsoft.icon"}]]
    [:body
     (document-ready (js* (.tooltip ($ "[title]:not([title=''])") {:track true
                                                                   :delay 0
                                                                   :showURL false
                                                                   :showBody " - "
                                                                   :fade 250})))
     [:div.light-blue-block.standard-landing-block
      [:div.holder980px-width {:style "padding-top:15px;"}
       [:div.lenta-holder
        [:div.ribbon-blue.ribbon]
        [:p.text-holder.middle-text {:style "left:120px;"} "Увеличьте обороты Вашего интернет-магазина <br/>за счет продаж в формате Совместных Покупок"]]
       [:div.big-text-holder
        [:h2 "Готовы к увеличению продаж?"]
        [:h3 "Оставьте контакты, чтобы мы могли связаться с Вами"]]
       [:div.partner-info-holder
        (send-form)]
       [:div.middle-text.text-holder {:style "padding-top:15px;"} "Преимущества сотрудничества с нами:"]
       [:div.small-icon-holder {:style "margin: 0 auto; width:800px;"}
        [:table
         [:tr
          [:td
           [:img {:src "/img/group-deals/strelki_small.png"}]]
          [:td
           [:img {:src "/img/group-deals/raketi_small.png"}]]
          [:td
           [:img {:src "/img/group-deals/seif_small.png"}]]]
         [:tr
          [:td.text-holder.small-text "Быстрое построение <br/>сети продаж"]
          [:td.text-holder.small-text "Мощная техническая <br/>поддержка"]
          [:td.text-holder.small-text "Нет необходимости <br/>в инвестициях"]]]]]
      [:div.blue-delimeter-top]]]]
   [:div.blue-block.standard-landing-block
    [:div.sp-info-container
     [:div.holder980px-width
      [:div.title-holder "Как работают Совместные Покупки?"]
      [:div.lenta-holder
       [:div.ribbon-blue-long.ribbon]
       [:p.text-holder.middle-text {:style "left:121px;"}"Совместные покупки это новый, набирающий популярность вид продаж, при котором покупатели объединяются, чтобы совершить оптовую покупку."]]
      [:img {:style "margin:-30px 0 70px;" :src "/img/group-deals/sp_info.png"}]
      [:div.lenta-holder
       [:div.ribbon-blue-wide.ribbon]
       [:p.text-holder.middle-text {:style "left:121px;"}"Взаимодействие Потребителей и Поставщика, становится возможным благодаря Организатору СП, который берет на себя все обязанности по сбору заказов и их логистике."]]]]]
   [:div.dark-grey-block.standard-landing-block
    [:div.blue-delimeter-bottom]
    [:div.holder980px-width
     [:div.spacer]
     [:div.title-holder "Совместные Покупки в цифрах"]
     [:img {:style "margin:30px 0;":src "/img/group-deals/info_graphics.svg"}]
     [:div.lenta-holder
      [:div.ribbon-blue-long.ribbon]
      [:p.text-holder.middle-text {:style "left:120px;"}"Ключевыми преимуществами СП являются обширная география, <br/> отсутствие затрат на продвижение и динамично растущая аудитория."]]]
    [:div.white-delimeter-top]]
   [:div.white-block.standard-landing-block
    [:div.holder980px-width
     [:div.title-holder "Почему продажи через обычный оптовый сайт <br/> не эффективны для Совместных Покупок?"]
     [:div.comparison-holder
      [:table
       [:thead
        [:th
         [:img {:src "/img/group-deals/reddoor.jpg"}]
         [:br]
         "Типичная картина"]
        [:th
         [:img {:src "/img/group-deals/greendoor.jpg"}]
         [:br]
         "Как правильно"]]
       [:tbody
        [:tr
         [:td "Работаем через обычный оптовый сайт."]
         [:td "Создаем специализированный сайт <br/> для работы с организаторами СП"]]
        [:tr
         [:td
          [:ul.negative
           [:li "ограниченные возможности получения прибыли организатором СП"
            (toggle-info "<p>Как правило, верхняя планка организационного сбора на большинстве СП-площадок, ограничена 20%.<p/><p>Чтобы заработать больше, организатор зачастую вынужден скрывать сайт с которого ведется закупка, выгружая информацию о товарах на промежуточные сайты (веб-галереи, файловые хранилища) или формировать табличные документы с фотографиями и ценами.</p>")]
           [:li "неэффективная система работы организаторов СП"
            (toggle-info "<p>В 90% случаев, организаторы используют неэффективную систему приема и учета заказов, работа с которой отнимает значительную часть времени, не позволяя организатору сосредоточится на продажах и привлечении новых клиентов.</p>")]
           [:li "неактуальная информация у конечного потребителя"
            (toggle-info "<p>Актуальность информации, представленной конечным покупателям СП, зависит от оперативности работы организатора.<p/><p>Зачастую, покупатели могут делать свой выбор, на основании устаревшей информации, которая была скопирована организатором на промежуточный сайт.<p/><p>Как правило, подобная практика негативно сказывается на результирующих продажах.</p>")]
           [:li "нет доступа к клиентской базе партнеров"
            (toggle-info "<p>В случае прекращения сотрудничества с организатором, вы теряете доступ к его клиентской базе. Не имея возможности влиять на повторные продажи информационными и рекламными рассылками.</p>")]
           [:li "СП не доводятся до конца"
            (toggle-info "<p>Для начинающих организаторов, еще не сформировавших надежного канала продаж из постоянных клиентов, не редки случаи, когда не удается собрать нужное количество участников для завершения СП.</p> <p>В результате, СП закрывается, а люди, все же сделавшие в нем заказы, теряют возможность купить ваш товар.</p>")]
           [:li "ограничения для выхода на новые рынки"
            (toggle-info "<p>Необходимость больших трудозатрат, связаных с ручным копированием информации о товарах на СП-площадку (группу в соц.сети, к примеру) вынуждает организаторов сотрудничать только с известными конечному потребителю торговыми марками, которые гарантируют хороший уровень продаж.<p/><p>Это может быть существенным препятствием для выхода вашей продукции на новые, перспективные рынки.</p>")]
           [:li "отсутствие понимания структуры продаж"
            (toggle-info "<p>Без четкого разделения оптовых партнеров и организаторов СП на отдельные группы, сложно понять какую роль каждая из этих групп играет в вашем бизнесе. Не исключено что именно организаторы СП делают львиную долю оборота.<p/><p>Эта информация была бы очень полезна при принятии решений о стимулировании продаж.</p>")]]
          [:td
           [:ul.positive
            [:li "мини-сайт для организатора СП"
             (toggle-info "<p>После регистрации, организатор получает возможность создать для себя мини-сайт (полную копию сайта для СП, на которой можно установить свои цены на товар, с учетом орг. сбора).</p>")]
            [:li "простое и удобное представление информации конечным потребителям"
             (toggle-info "<p>Мини-сайт получает собственный адрес. Это экономит организатору время на копирование информации о товарах. Для начала продаж, достаточно просто разместить ссылку на свой мини-сайт. <p/><p>Таким образом, организатор не ограничен в размере наценки, а покупатели получают полную и достоверную информацию о ваших товарах.</p>")]
            [:li "высокая эффективность работы организатора СП"
             (toggle-info "<p>С помощью мини-сайта, организатор может вести учет принятых заказов и оплат со всех СП-площадок. Это значительно повышает качество и скорость обслуживания конечных потребителей.</p>")]
            [:li "сбор и эффективное использование информации о покупателях"
             (toggle-info "<p>Поскольку все мини-сайты связаны в одну систему, у вас всегда есть доступ к списку клиентов, совершавших заказы.<p/><p>Это позволяет проводить рекламные рассылки, стимулирующие повторные продажи.</p>")]
            [:li "эффективная работа с социальными сетями"
             (toggle-info "<p>Мини-сайт позволяет автоматически выгружать информацию о товарах в социальные сети. При этом, все фотографии товаров группируются по альбомам-категориям, снабжаются описанием и установленной организатором ценой.<p/><p>Это экономит более 80% времени и позволяет организатору сосредоточится на организации продаж, а не на оформлении товаров.</p>")]
            [:li "только актуальная информация"
             (toggle-info "<p>Вся информация о товарах на сайте СП синхронизируется с вашим оптовым сайтом. Если со временем появляются новые товарные позиции, они автоматически загружаются на мини-сайты организаторов, и в их группы в социальных сетях.<p/><p>Это повышает надежность и эффективность создаваемой распределенной сети продаж.</p>")]
            [:li "минимизация числа незавершенных СП"
             (toggle-info "<p>Мини-сайты позволяют организаторам из близлежащих регионов, объединять собранные заказы для ускорения формирования СП.<p/><p>Это означает что ни один заказ не пропадет из-за того, что организатору так и не удалось собрать нужное количество участников.</p>")]
            [:li "эффективный способ выхода на новые рынки"
             (toggle-info "<p>Автоматическая выгрузка информации о товарах, значительно уменьшает объем работы, необходимый организатору для начала продаж.<p/><p>Это позволяет легко достичь договоренности о сотрудничестве с регионами, в которых ваша продукция еще не была представлена.</p>")]
            [:li "аналитика продаж"
             (toggle-info "<p>Вам доступна аналитика по продажам всех организаторов СП, по всем мини-сайтам и регионам.<p/><p>Бонусы, скидки, специальные условия сотрудничества и реализация партнерских программ значительно упрощаются при наличии полной статистики о количестве клиентов и продажах каждого партнера.</p>")]]]]]
        [:tr
         [:td {:colspan "2" :style "padding:10px;font-size:24px;"} "Результат"]]
        [:tr
         [:td.result-description "Сотрудничество удается наладить с единичными организаторами, только часть которых доводит СП до конца.<br/><br/><br/>"
          [:img {:src "/img/group-deals/unluck.jpg"}]]
         [:td.result-description "Вы работаете с большим количеством организаторов СП, при этом не упуская ни одного сделанного у них заказа. <br/><br/><br/>"
          [:img {:src "/img/group-deals/success.jpg"}]]]
        [:tr
         [:td {:colspan "2"}
          [:div.lenta-holder
           [:div.ribbon-blue-long.ribbon]
           [:p.text-holder.middle-text {:style "left:120px;"}"Рынок Совместных Покупок увеличивается на 90% каждый год. <br/> Хотите чтобы ваши продажи росли так же быстро?"]]]]]]]]
    [:div.blue-delimeter-top {:style "margin-top:-30px;"}]]
   [:div.blue-block.standard-landing-block
    [:div.holder980px-width
     [:div.title-holder {:style "padding:20px 0 20px;"} "Условия сотрудничества"]
     [:div.sub-title "Мы предлагаем"]
     [:table.offer-table
      [:tr
       [:td
        [:img {:src "/img/group-deals/1.png"}]]
       [:td
        [:img {:src "/img/group-deals/2.png"}]]
       [:td
        [:img {:src "/img/group-deals/3.png"}]]]
      [:tr
       [:td "Бесплатную разработку и внедрение сайта для продаж в формате Совместных Покупок. Cайт, упростит взаимодействие с организаторами СП и повысит эффективность их продаж."]
       [:td "Интеграцию нового сайта с админкой и базой вашего существующего интернет-магазина."]
       [:td "Наработанную базу организаторов СП и ее дальнейшее расширение в соответствии со списком стран и регионов, в которые вы готовы отгружать продукцию."]]]
     [:div.sub-title "Требования к компании-партнеру"]
     [:table.offer-table {:style "margin-bottom:60px;"}
      [:tr
       [:td
        [:img {:src "/img/group-deals/4.png"}]]
       [:td
        [:img {:src "/img/group-deals/5.png"}]]]
      [:tr
       [:td "Наша компания получает 4% от оборота <br/> созданного сайта для Совместных Покупок"]
       [:td "Разработанный сайт должен обладать эксклюзивными правами на продажи организаторам Совместных Покупок"]]]
     [:div.lenta-holder
      [:div.ribbon-blue-wide.ribbon]
      [:p.text-holder.middle-text {:style "left:120px;"} "Мы всегда рады новым партнерам, если у Вас появились альтернативные предложения по условиям сотрудничества, свяжитесь с нами. <br/>Мы готовы обсудить Ваши идеи."]]]]
   [:div.light-blue-block.standard-landing-block {:style "padding-bottom:200px;"}
    [:div.blue-delimeter-bottom {:style "padding-bottom:150px;"}]
    [:div.big-text-holder {:style "padding-top:15px; margin-top:0;"}
     [:h2 "Готовы к увеличению продаж?"]
     [:h3 "Оставьте контакты, чтобы мы могли связаться с Вами."]]
    [:div.partner-info-holder
     (send-form)]]))
