(ns gd.views.notifications
  (:use
   gd.model.context
   swiss-arrows.core
   gd.log
   [clojure.algo.generic.functor :only (fmap)]
   gd.views.project_components
   gd.utils.mail
   gd.views.messages
   gd.utils.pub_sub
   [clostache.parser :only (render-resource)]
   clojure.walk
   gd.utils.basic-templates
   gd.model.model
   gd.utils.sms
   [clj-time.core :exclude (extend)]
   [clj-time.coerce :as tm]
   com.reasonr.scriptjure
   hiccup.element
   noir.core
   gd.utils.web
   hiccup.form
   gd.views.components
   gd.utils.common
   hiccup.core
   hiccup.page
   net.cgrand.enlive-html)
  (:require
   [gd.views.security :as security]
   [noir.session :as session]))

(defmulti render-notification (fn[type extra-info source]
                                (let [stacked (:stacked extra-info)]
                                  (if stacked [type stacked] type))))

(def ^:dynamic *current-user*)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sending methods

(defn- send-rendered-to-email[rendered-news]
  (let [mail (get-in *current-user* [:contact :email :value])]
    (doseq [{:keys [user tags context stock
                    header header-overwrite
                    body body-overwrite] :as rendered}
            rendered-news]
      (when rendered
        (send-mail
         mail
         (or header-overwrite
             header)
         (or body-overwrite
             (render-resource "mail/notification.html"
                              {:header (or header-overwrite header)
                               :body (html (binding [*current-context* (merge {:user user} context)]
                                             (render-template body)))
                               :all-news "http://dayte-dve.com.ua/user/news/"
                               :news-settings "http://dayte-dve.com.ua/user/news/settings/"
                               :user-href (str "http://dayte-dve.com.ua/user/" (:id user))
                               :avatar (when (or user stock)
                                         (str "http://dayte dve.com.ua"
                                              (cond user (images:get :messages-avatar-small (:images user))
                                                    stock (images:get :stock-medium (first (:images stock))))))
                               :current-user *current-user*
                               :current-user-href (str "http://dayte-dve.com.ua/user/" (:id *current-user*))
                               :time (format-date (clj-time.coerce/to-date (clj-time.core/now)))
                               :tags (html (map (fn[tag] [:div {:style "background-color:#f8935b;border-radius:5px;color:white;display:inline-block;margin:10px 0 0;padding:5px 10px;"} tag])
                                                (maybe-seq tags)))})))))))

(defn- send-rendered-to-phone[rendered-news]
  (let [phone (get-in *current-user* [:contact :phone :value])]
    (doseq [{:keys [user body tags context stock destination]} rendered-news]
      (when-let [phone (or destination phone)]
        (send-sms phone (html body))))))

(defn- send-news[news-by-transport]
  (doseq [[transport news] news-by-transport]
    ((case transport
       :mail send-rendered-to-email
       :sms send-rendered-to-phone) news)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Rendering methods

(defn- render-news
  "Render news and group them by transport"
  [{:keys [type info source] :as notification}]
  (when notification
    (binding [session/*noir-session* (atom {:user *current-user*})]
      (let [transport (fn[n] (or (:type n) :mail))
            default-content {:header (! :news type)}]
        (doall (group-by transport (map
                                    (fn[rendered] (merge default-content rendered))
                                    (maybe-seq (render-notification type info source)))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fitler news by subscriptions

;; TODO : restore filtering by subscriptions
(defn- filter-news
  "Filter news by notification type and transport type"
  [type news-by-type]
  (reduce merge
          (map (fn[[transport news]]
                 (when (notify:check-subscription (:id *current-user*) type transport)
                   {transport news}))
               news-by-type)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stacking methods(select only last notification from each stack)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Internal check
(defonce stacked-notifications (atom {}))

(defn- stack-should-be-send?
  "If any of the item from the stack is here more than 5 minutes - process stack"
  [stack]
  (some (comp (fn[time] (after? (now) (plus (tm/from-sql-date time) (minutes 5)))) :created)
        stack))

(declare process-notification)
(defn- check-pending-stacks[]
  (doseq [[key stack] @stacked-notifications]
    (when (stack-should-be-send? stack)
      (swap! stacked-notifications dissoc key)
      ;; now we send most fresh notification(in future it is possible to composite stack in to one
      ;; rendered news - see definition of render-notification)
      (let [notification (first stack)]
        (deserialize-context (:context notification)
          (process-notification notification))))))

(defonce stack-checking-thread
  (.start (new Thread
               (fn[]
                 (while true
                   (try
                     (check-pending-stacks)
                     (Thread/sleep 10000)
                     (catch Throwable e
                       (error e))))))))

(defn- print-pending-events[]
  (clojure.pprint/pprint (fmap (partial map (juxt :type :created (comp :id :source))) @stacked-notifications)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Modification of stack queue
(defn- stack-key[{:keys [type fkey_user source]}]
  [type fkey_user (:id source)])

(defn- send-to-collector[notification]
  (swap! stacked-notifications update-in [(stack-key notification)] conj
         (merge notification {:context (serialize-context)})))

(defn- stack-news
  "Check if some news must be stacked(not send immediately, but wait for some time, group them and
then send with one stack)"
  [notification]
  (if (notify-stacked-notifications (:type notification))
    (do (send-to-collector notification) nil)
    notification))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main entry point

(defn- process-notification[notification]
  (let [{:keys [type fkey_user]} notification]
    (binding [*current-user* (when fkey_user (user:get-full fkey_user))]
      (-?<>> notification
             render-news
             ;; (filter-news type)
             send-news))))

(defn process-persistent-notifications
  "Main entry point for notification processing. Responsible for fetching persistent notification."
  [data]
  (-> (notify:get-by-id (:id data))
      stack-news
      process-notification))

(create-subscriber notification-channel #'process-persistent-notifications)
