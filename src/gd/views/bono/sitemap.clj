(ns gd.views.bono.sitemap
  (:use noir.request
        gd.views.seo.sitemap
        gd.utils.common
        [noir.response :only (xml content-type)]
        noir.core
        gd.utils.db
        gd.utils.web
        hiccup.core
        hiccup.page
        korma.core
        gd.model.model)
  (:require [clojure.string :as s]))

(defn bono-sitemap[]
  (xml (apply
        sitemap
        {:freq "weekly"}
        (concat
         ["http://bono.in.ua/guestbook/"
          "http://bono.in.ua/pages/get_order_with_no_reg/"
          "http://bono.in.ua/contacts/"
          "http://bono.in.ua/pages/dostavka_i_oplata/"
          "http://bono.in.ua/news/"]
         (map
          (fn[{:keys [id path]}]
            (str "http://bono.in.ua/stocks" path))
          (category:model))
         (map
          (fn[{:keys [id url]}]
            (str "http://bono.in.ua/news/detail/" url "/"))
          (site-pages:get-pages-list nil nil {:type :news :status :published}))
         (map
          (fn[{:keys [id]}]
            (str "http://bono.in.ua/stock/" id))
          (retail:supplier-stocks nil nil {}))))))

(defpage "/themes/sitemap.xml" []
  (bono-sitemap))
