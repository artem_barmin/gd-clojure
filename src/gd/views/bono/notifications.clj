(ns gd.views.bono.notifications
  (:use
   gd.views.notifications
   gd.views.project_components
   gd.utils.mail
   gd.views.messages
   gd.utils.pub_sub
   [clostache.parser :only (render-resource)]
   clojure.walk
   gd.utils.basic-templates
   gd.model.model
   gd.utils.sms
   com.reasonr.scriptjure
   hiccup.element
   noir.core
   gd.utils.web
   hiccup.form
   gd.views.components
   gd.utils.common
   hiccup.core
   hiccup.page
   net.cgrand.enlive-html)
  (:require
   [gd.views.security :as security]
   [noir.session :as session]))

(defn- user-table[additional-info user]
  (html
   [:div {:style "margin-bottom:30px;"}
    "Информация о покупателе:"

    (let [{:keys [real-name contact extra-info full-address]} user]
      (list [:div "Имя: " real-name]
            [:div "Номер телефона: " (get-in contact [:phone :value])]
            [:div "Адрес: " full-address]
            [:div "Отделение новой почты: " (:novaya_pochta extra-info)]))

    (when (seq additional-info)
      (let [{:keys [type detailed payment-type]}
            additional-info]
        (list [:div "Тип оплаты: " (! :retail.payment payment-type)]
              (when type [:div "Тип доставки: " (! :retail.shipping type)])
              (when (seq detailed) [:div "Предложение по доставке: " detailed]))))]))

(defn- order-table[user-order-item]
  (html
   [:div "Данные о заказе"
    (if (seq (remove :deleted user-order-item))
      [:div
       [:table {:border 1 :style "width:100%;border-collapse:collapse;" :width 100}
        [:tr [:td "Товар"] [:td "Параметры заказа"] [:td "Количество"] [:td "Сумма"]]
        (map
         (fn[{:keys [stock quantity sum deleted] :as order-item}]
           (when (false? deleted)
             [:tr
              [:td (:name stock) (when (= (:selected-context stock) :wholesale-sale) "(распродажа)")]
              [:td (render-params order-item)]
              [:td quantity]
              [:td sum]]))
         user-order-item)]
       ;; because notification info is fetched with modifier 'disable-default-where' we should
       ;; manually recompute sum(because 'deleted' order-items are passed to mail)
       [:div "Сумма заказа: " (reduce + (map :sum (remove :deleted user-order-item))) "грн."]]
      [:div "Все товары из заказа были удалены"])]))

(defmethod render-notification :retail-new-order [_ _ {:keys [id user additional-info user-order-item sum] :as order}]
  {:header-overwrite (str "Новый заказ №" id)
   :body-overwrite
   (clojure.string/join "<br/>"
                        (list
                         (user-table additional-info user)
                         (order-table user-order-item)))})

(defmethod render-notification :retail-order [_ _ {:keys [id user additional-info user-order-item sum] :as order}]
  {:header-overwrite (str "Обновление брони №" id)
   :body-overwrite
   (clojure.string/join "<br/>"
                        (list
                         (user-table additional-info user)
                         (order-table user-order-item)))})

(defmethod render-notification :retail-new-user-order [_ _ {:keys [id user additional-info user-order-item sum] :as order}]
  {:body-overwrite
   (html
    [:div
     [:p "Здравствуйте."]
     [:p "Вашему заказу присвоен номер " id "."]
     [:p "Статус заказа: Создан."]
     [:p "Все вещи, которые Вы уже забронировали и те, которые еще будете бронировать, отражаются на сайте в разделе "
      [:a {:href "http://bono.in.ua/orders/"} "Мои заказы"]]
     (user-table additional-info user)
     (order-table user-order-item)
     [:br]
     [:p "Наши менеджеры свяжутся с Вами в ближайшее время и подтвердят бронь."]
     [:br]
     [:p "С уважением,"]
     [:p "Администрация сайта,"]
     [:a {:href "http://bono.in.ua"} "http://bono.in.ua"]])
   :header-overwrite (str "BONO.IN.UA. Вашему заказу присвоен номер " id ".")})

(defmethod render-notification :retail-order-user-event [_ _ {:keys [id user additional-info user-order-item sum] :as order}]
  {:body-overwrite
   (html
    [:div
     [:p "Здравствуйте."]
     [:p "Ваша бронь №" id " на сайте bono.in.ua обновлена."]
     (user-table additional-info user)
     (order-table user-order-item)
     [:br]
     [:p "С уважением,"]
     [:p "Администрация сайта,"]
     [:a {:href "http://bono.in.ua"} "http://bono.in.ua"]])
   :header-overwrite (str "Обновление брони №" id " на сайте BONO.IN.UA")})

(defmethod render-notification :site-page-news-published [_ _ {:keys [url title content]}]
  {:body-overwrite content
   :header-overwrite title})

(defmethod render-notification :retail-new-review [_ {:keys [text author]} _]
  {:body-overwrite
   (html
    [:div
     [:p "Здравствуйте."]
     [:p "Добавлен новый отзыв."]
     [:p (str "Автор отзыва: " author)]
     [:p (str "Текст отзыва: " text)]
     [:p "Пройдите в Админку для его просмотра: "
      [:a {:href "http://bono.in.ua/admin/review/"} "Отзывы"]]])
   :header-overwrite "Новый отзыв на сайте bono.in.ua"})

(defmethod render-notification :retail-user-registration [_ _ user]
  {:body-overwrite
   (html
    [:div
     [:p "Здравствуйте. На сайте bono.in.ua зарегистрирован новый пользователь."]
     [:p (str "Логин: " (:login (first (:auth-profile user))))]])
   :header-overwrite "Новый пользовтель на сайте bono.in.ua"})
