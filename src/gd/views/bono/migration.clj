(ns gd.views.bono.migration
  (:use gd.views.resources
        gd.parsers.concrete.utils.azra-categories
        gd.parsers.concrete.azra
        compojure.core
        gd.views.bucket.simple
        gd.utils.sms
        gd.views.seo.meta
        gd.utils.mail
        gd.utils.web
        gd.utils.common
        gd.views.components
        noir.core
        [noir.options :only (dev-mode?)]
        [noir.response :only (redirect status set-headers)]
        hiccup.core
        com.reasonr.scriptjure
        hiccup.page
        hiccup.form
        hiccup.element)
  (:use gd.model.model)
  (:require [clojure.string :as strings])
  (:require [gd.views.security :as sec])
  (:require [noir.session :as session]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Categories redirection

(defn- old-category-to-new-category
  "Function that resolve old categories by name from DB"
  [id]
  (let [bono-category-string
        (get bono-categories-tree (parse-int id))

        new-category
        (resolve-categories bono-category-string)]
    (first (category:get-by-ids [new-category]))))

(def old-bono-categories
  (concat
   (list
    [289 443 ["Мужская одежда" "Верхняя одежда" "Рубашки"]]
    [290 449 ["Мужская одежда" "Верхняя одежда" "Шапки"]]
    [261 456 ["Женская одежда" "Верхняя одежда" "Футболки - Майки" "Однотонные футболки"]]
    [293 451 ["Мужская одежда" "Белье" "Носки"]]
    [295 445 ["Женская одежда" "Верхняя одежда" "Штаны, брюки, лосины"]]
    [232 422 ["Женская одежда" "Домашняя одежда" "Комплекты"]]
    [264 458 ["Женская одежда" "Верхняя одежда" "Футболки - Майки" "Однотонные майки"]]
    [296 448 ["Женская одежда" "Белье" "Колготы"]]
    [233 420 ["Женская одежда" "Домашняя одежда" "Халаты"]]
    [234 419 ["Женская одежда" "Домашняя одежда" "Комнатные тапочки"]]
    [266 423 ["Женская одежда" "Белье" "Майки, футболки Бельевые"]]
    [235 421 ["Женская одежда" "Домашняя одежда" "Сорочки"]]
    [267 401 ["Мужская одежда" "Верхняя одежда" "Майки"]]
    [299 399 ["Детская одежда" "Для Девочек" "Белье"]]
    [300 448 ["Детская одежда" "Для Девочек" "Белье" "Колготы"]]
    [301 447 ["Женская одежда" "Домашняя одежда" "Пижамы, Костюмы"]]
    [206 428 ["Женская одежда" "Верхняя одежда" "Гольфы - Водолазки" "Гольф короткий рукав"]]
    [270 426 ["Женская одежда" "Белье" "Трусы"]]
    [302 400 ["Детская одежда" "Для Девочек" "Верхняя одежда"]]
    [303 463 ["Детская одежда" "Для Девочек" "Верхняя одежда" "Свитера, гольфы, кофты"]]
    [176 429 ["Женская одежда" "Верхняя одежда" "Гольфы - Водолазки" "Водолазки Сеточки"]]
    [272 424 ["Женская одежда" "Белье" "Шорты, бриджи"]]
    [304 461 ["Детская одежда" "Для Мальчиков"]]
    [273 404 ["Мужская одежда" "Белье" "Трусы"]]
    [305 462 ["Детская одежда" "Для Девочек"]]
    [242 409 ["Женская одежда" "Верхняя одежда"]]
    [274 401 ["Мужская одежда" "Белье" "Майки"]]
    [306 463 ["Детская одежда" "Для Мальчиков" "Свитера, гольфы, кофты"]]
    [243 410 ["Женская одежда" "Домашняя одежда"]]
    [275 442 ["Мужская одежда" "Верхняя одежда" "Футболки с принтом"]]
    [307 450 ["Мужская одежда" "Белье" "Пижамы"]]
    [244 411 ["Женская одежда" "Белье"]]
    [276 402 ["Мужская одежда" "Белье" "Футболки, гольфы"]]
    [245 400 ["Мужская одежда" "Верхняя одежда"]]
    [277 403 ["Мужская одежда" "Белье" "Штаны, Кальсоны"]]
    [278 405 ["Мужская одежда" "Верхняя одежда" "Футболки длинный рукав"]]
    [247 399 ["Мужская одежда" "Белье"]]
    [279 406 ["Мужская одежда" "Верхняя одежда" "Свитера, толстовки"]]
    [248 441 ["Мужская одежда" "Верхняя одежда" "Футболки"]]
    [280 425 ["Женская одежда" "Белье" "Комбидрессы"]]
    [217 417 ["Женская одежда" "Верхняя одежда" "Шифоновые платья и блузки"]]
    [218 418 ["Женская одежда" "Верхняя одежда" "Шарфы, платки"]]
    [250 455 ["Женская одежда" "Верхняя одежда" "Гольфы - Водолазки" "Теплые гольфы"]]
    [251 415 ["Женская одежда" "Верхняя одежда" "Футболки длинный рукав"]]
    [285 413 ["Женская одежда" "Верхняя одежда" "Гольфы - Водолазки"]]
    [286 414 ["Женская одежда" "Верхняя одежда" "Футболки - Майки"]]
    [255 430 ["Женская одежда" "Верхняя одежда" "Гольфы - Водолазки" "Водолазки с рисунком"]]
    [287 446 ["Женская одежда" "Верхняя одежда" "Свитера, кофты"]])

   (list
    [291 455 ["Женская одежда" "Верхняя одежда" "Гольфы - Водолазки" "Теплые гольфы" "Теплый гольф (Норма)"]]
    [260 459 ["Женская одежда" "Верхняя одежда" "Футболки - Майки" "Майки"]]
    [292 455 ["Женская одежда" "Верхняя одежда" "Гольфы - Водолазки" "Теплые гольфы" "Теплый гольф (Батал)"]]
    [265 457 ["Женская одежда" "Верхняя одежда" "Футболки - Майки" "Футболки Батал"]]
    [298 460 ["Детская одежда"]]
    [240 348 ["Женская одежда"]]
    [241 374 ["Мужская одежда"]]
    [249 457 ["Женская одежда" "Верхняя одежда" "Футболки - Майки" "Футболки"]]
    [281 452 ["Женская одежда" "Верхняя одежда" "Гольфы - Водолазки" "Однотонные водолазки " "Однотонные водолазки (Норма)"]]
    [282 452 ["Женская одежда" "Верхняя одежда" "Гольфы - Водолазки" "Однотонные водолазки " "Однотонные водолазки (Батал)"]]
    [283 456 ["Женская одежда" "Верхняя одежда" "Футболки - Майки" "Однотонные футболки" "Однотонные футболки (Норма)"]]
    [252 452 ["Женская одежда" "Верхняя одежда" "Гольфы - Водолазки" "Однотонные водолазки "]]
    [284 456 ["Женская одежда" "Верхняя одежда" "Футболки - Майки" "Однотонные футболки" "Однотонные футболки (Батал)"]])))

(defn- redirect-category[id]
  (let [[old category] (find-first (fn[[old new desc]] (= (parse-int id) old)) old-bono-categories)]
    (when category
      (persistent-redirect (str "/stocks/"
                                (clojure.string/join "/" (reverse (map :short-name (category:parents category))))
                                "/")))))

(defpage "/themes/catalog/grupa/:id" {id :id}
  (case id
    "219" (persistent-redirect "/new/")
    "220" (persistent-redirect "/sale/")
    (redirect-category id)))

(defpage "/themes/catalog/grupa/:id/page/:page" {id :id page :page}
  (case id
    "219" (persistent-redirect "/new/")
    "220" (persistent-redirect "/sale/")
    (redirect-category id)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stocks redirection

(def old-bono-stocks (read-string (slurp "resources/migration/stocks.txt")))
(def old-bono-stocks-by-name (group-by second old-bono-stocks))

(def old-bono-stocks-from-url
  (delay (binding [gd.model.context/*current-supplier* 1
                   gd.model.context/*omit-cache* true]
           (let [stocks-count (range
                               0
                               (get-count (retail:admin-supplier-stocks nil nil {}))
                               100)]
             (->>
              stocks-count
              (mapcat (fn[offset]
                        (map
                         (fn[{:keys [id meta-info]}]
                           (when (:url meta-info)
                             {(parse-int (re-find #"[0-9]+" (:url meta-info))) id}))
                         (retail:admin-supplier-stocks 100 offset {}))))
              (mreduce identity))))))

(defn- resolve-old-stock-to-new[id]
  (->>
   (get old-bono-stocks id)
   (get old-bono-stocks-by-name)
   (map first)
   (some @old-bono-stocks-from-url)))

(defn- redirect-stock[id]
  (let [id (parse-int id)]
    (if-let [new-stock (resolve-old-stock-to-new id)]
      (persistent-redirect (str "/stock/" new-stock))
      (redirect "/stocks/"))))

(defpage "/themes/catalog/goods/:id" {id :id}
  (redirect-stock id))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Pages redirection

(defpage "/themes/catalog" {}
  (persistent-redirect "/stocks/"))

(defpage "/themes/basket" {}
  (persistent-redirect "/bucket/"))

(defpage "/themes/news" {}
  (persistent-redirect "/news/"))

(defpage "/themes/contact" {}
  (persistent-redirect "/contacts/"))

(defpage "/themes/o_kompanii" {}
  (persistent-redirect "/pages/about/"))

(defpage "/themes/guestbook" {}
  (persistent-redirect "/guestbook/"))

(defpage "/themes/dostavka_i_oplata" {}
  (persistent-redirect "/pages/dostavka_i_oplata/"))

(compojure-route
 (GET ["/themes/news/url/:url/" :url #".*"] [url]
   (persistent-redirect (str "/news/detail/" url "/"))))
