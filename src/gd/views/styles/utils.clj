(ns gd.views.styles.utils
  (:use gd.utils.common)
  (:use gd.utils.styles)
  (:require [clojure.string :as s]
            [clojure.java.io :as io])
  (import javax.imageio.ImageIO))

(def conv (comp vec cons))

(defn with-comment[val comment]
  {:val val
   :comment comment})

(defn url-to-path[url]
  (let [url (if (.startsWith url "/") url (str "/" url))]
    (str "resources/public" url)))

(defn file[url]
  (let [url (if (.startsWith url "/") url (str "/" url))]
    (when-not (.exists (io/as-file (str "resources/public" url)))
      (throwf "File %s does not exists" (str "resources/public" url))))
  (str "url('" url "')"))

(defn file-dims[url]
  (if (.exists (io/as-file (url-to-path url)))
    (let [img (ImageIO/read (io/as-file (url-to-path url)))]
      [(.getWidth img) (.getHeight img)])
    (throwf "File %s does not exists" url)))
