(ns gd.views.styles.common
  (:use gd.utils.common)
  (:use gd.utils.styles)
  (:use gd.views.styles.utils))

(defn image-width[image]
  (let [[w] (file-dims image)]
    (list :width w)))

(defn image-height[image]
  (let [[w h] (file-dims image)]
    (list :height h)))

(defn tile-image[image & [sprite]]
  (list
   :background-image (if sprite
                       (with-comment (file image) (str "sprite-ref: " sprite ";"))
                       (file image))))

(defn bg-simple[image & [sprite]]
  (list
   :background-image (if sprite
                       (with-comment (file image) (str "sprite-ref: " sprite ";"))
                       (file image))
   :background-repeat :no-repeat))

(defn bgimage[image & [sprite]]
  (let [[w h] (file-dims image)]
    (list
     :background-image (if sprite
                         (with-comment (file image) (str "sprite-ref: " sprite ";"))
                         (file image))
     :width w
     :height h)))

(defn grad-image[image & [sprite]]
  (let [[w h] (file-dims image)
        max-dimension (max w h)]
    (list
     :background-image (with-comment (file image)
                         (when sprite
                           (str "sprite-ref: " sprite ";sprite-alignment:repeat;")))
     (when (= w max-dimension) (list :width w))
     (when (= h max-dimension) (list :height h)))))

(defn noselect[]
  (list
   :-webkit-touch-callout :none
   :-webkit-user-select :none
   :-khtml-user-select :none
   :-moz-user-select :none
   :-ms-user-select :none
   :user-select :none))

(defn with-prefixes[name value]
  (mapcat (fn[prefix]
            [(as-str (when prefix (as-str "-" prefix "-")) name) value])
          [nil :ms :o :moz :webkit]))

(defn with-inner-prefixes[rule name value]
  (mapcat #(vector rule (str (when % (as-str "-" % "-")) name "(" value ")"))
          [nil :ms :o :moz :webkit]))

(defn border-radius[& {:keys [left-top left-bottom right-top right-bottom] :as info}]
  (let [rename {:left-top :border-top-left-radius
                :left-bottom :border-bottom-left-radius
                :right-top :border-top-right-radius
                :right-bottom :border-bottom-right-radius}]
    (mapcat (fn[[k v]] [(get rename k) v]) info)))

(defn border[& {:keys [color enable width] :or {width 1}}]
  (let [rename {:left :border-left-width
                :right :border-right-width
                :top :border-top-width
                :bottom :border-bottom-width}]
    (list :border-style :solid
          :border-width 0
          (mapcat (fn[k] [(get rename k) width]) enable)
          :border-color color)))
