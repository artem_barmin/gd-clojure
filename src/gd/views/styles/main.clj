(ns gd.views.styles.main
  (:use gd.utils.common
        gd.utils.styles
        gd.views.styles.utils
        gd.views.styles.common
        gd.views.styles.buttons))

(defn block
  "Define block of content.

- all internal margins will not collapse with parent
- possible to use absolute positioning and go out of bound(not overflow:hidden)
- expands to parent width
- clear after child floats

Use clearfix from :
http://css-live.ru/tricks/novaya-alternativa-clearfix-u-i-overflowhidden.html"
  [name & params]
  (list
   [name
    :display :table
    :width "100%"
    :table-layout :fixed
    :position :relaive
    params]

   [(str name ":after")
    :content "'. . . . . . .'"
    :display :block
    :word-spacing "99in"
    :height 0
    :overflow :hidden

    ;; opera fix
    :font-size "0.13em"
    :line-height 0]))

(defn fixed-block
  "Define block of content. Everything just like simple block, but don't expand
to parent width, and respect only child width. "
  [name & params]
  (list
   [name
    :display :table
    :position :relaive
    params]))

(defn div-center[width]
  (list :margin-left :auto
        :margin-right :auto
        :width width))

(defn fancy-controls[]
  (list
   [".fancy-input-text"
    :border "1px solid #C3CED9"
    :color "#1B4684"
    :border-radius 5
    :height 25
    :padding-left 5
    :padding-right 5]

   [".fancy-input-text:focus"
    :box-shadow "0px 0px 5px #40b5e2"
    :border-color "#40b5e2"]

   [".fancy-textarea"
    :border "1px solid #dfe1e3"
    :color "#1B4684"
    :border-radius 5
    :padding 5]

   [".fancy-textarea:focus"
    :box-shadow "0px 0px 5px #40b5e2"
    :border-color "#40b5e2"]))

(css "resources/public/css/user/main.css"

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Bootstrap fixes
     ["ul,ol" :margin-bottom 0 :margin-top 0]
     [".popover" :max-width "none !important"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Fancy controls
     (fancy-controls)

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Usefull class
     [".left" :float "left"]
     [".right" :float "right"]
     [".clear" :clear "both"]
     [".width" :width "100%"]
     [".none" :display "none"]
     [".none-opacity" :opacity "0"]
     [".hidden" :visibility "hidden"]
     [".collapse" :border-collapse "collapse"]
     [".valign-top" :vertical-align :top]
     [".valign-middle" :vertical-align :middle]
     [".valign-bottom" :vertical-align :bottom]
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Blocks

     [".inline-block"
      :display "inline-block"]

     [".block"
      :display "block"]

     [".pointer"
      :cursor "pointer"]

     (block ".group")
     (fixed-block ".fixed-group")

     (block ".border-group"
            (with-prefixes :box-sizing :border-box))

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Common margins and paddings

     [".padding5percent"
      :padding "5%"]

     ;; padding-all
     (let [common-styles (fn[name postfix property & nums]
                           (map
                            (fn[num] [(str name num postfix) property num])
                            (concat [2 3 5 8 10 13 15 20 25] nums)))]
       (list
        (common-styles ".padding" nil :padding)
        (common-styles ".padding" "t" :padding-top)
        (common-styles ".padding" "r" :padding-right)
        (common-styles ".padding" "b" :padding-bottom)
        (common-styles ".padding" "l" :padding-left)
        (common-styles ".margin" nil :margin)
        (common-styles ".margin" "t" :margin-top)
        (common-styles ".margin" "r" :margin-right)
        (common-styles ".margin" "l" :margin-left)
        (common-styles ".margin" "b" :margin-bottom)))

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Centering
     [".center"
      :margin-left "auto"
      :margin-right "auto"]

     [".align-left"
      :margin-right "auto"]

     [".align-right"
      :margin-left "auto"]

     [".text-center"
      :text-align "center"]

     [".text-left"
      :text-align "left"]

     [".text-right"
      :text-align "right"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Common positions
     [".abottom"
      :position :absolute
      :bottom 0]

     [".atop"
      :position :absolute
      :top 0]

     [".aleft"
      :position :absolute
      :left 0]

     [".aright"
      :position :absolute
      :right 0]

     [".absolute"
      :position :absolute]

     [".relative"
      :position :relative]

     [".container"
      :position :relative]

     [".width80"
      :width "80%"]

     [".width60"
      :width "60%"]

     [".width98"
      :width "98%"]

     [".width100"
      :width "100%"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Links
     [".link"
      :color "#1576A0"]

     [".link:hover"
      :cursor :pointer]

     [".link:visited"
      :color "#1576A0"]

     [".colored-link"
      :color "#1576a0"]

     [".colored-link:hover"
      :color "#1EADE8"
      :cursor :pointer]

     [".gray-link"]

     [".gray-link:hover"
      :color "#1576A0"
      :text-decoration :underline
      :cursor :pointer]

     [".white-link"
      :color "white"]

     [".white-link:hover"
      :cursor :pointer
      :text-decoration :underline]

     [".simple-link"
      :cursor :pointer]

     [".simple-link:hover"
      :text-decoration :underline]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Display
     [".table-row"
      :display "table-row"]

     [".table-cell"
      :display "table-cell"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Text

     [".bold"
      :font-weight :bold]

     [".cutted"
      :overflow :hidden
      :white-space :nowrap
      (with-prefixes "text-overflow" :ellipsis)]

     ;; Colored text
     [".blue"
      :color "#1B4684"]

     [".orange"
      :color "#FF6000"]

     [".dark-blue"
      :color "#2c333c"]

     [".white"
      :color "white"]

     [".justify"
      :text-align :justify]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Ajax in progress - indicator
     [".ajax-in-progress"
      (with-prefixes "opacity" "0.5")]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Validation
     [".reg-validation-error"
      :height :6px
      :font-size :10px
      :color :red
      :margin-top 2
      :display :inline-block
      :text-align :left
      :width 220]

     [".validation-error-tooltip"
      :position :absolute
      :transition "all 0.3s ease-in-out 0s"
      :z-index "2000"]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Spinner
     (simple-buttons ".spinner-top" "/img/spinner/icon-chevron-up" :top 0)
     (simple-buttons ".spinner-bottom" "/img/spinner/icon-chevron-down" :bottom 0)

     [".spinner-button"
      :position :absolute
      :text-align :center
      :right -35
      :cursor :pointer
      (noselect)]

     [".spinner-input"
      :border "1px solid #dfe1e3"
      :border-radius 5
      :color "#1B4684"
      :height 27
      :width "100%"
      :padding-left 5]

     [".spinner-input:hover"
      :box-shadow "0px 0px 5px #40b5e2"
      :border-color "#40b5e2"]

     [".spinner-element"
      :width 100]

     [".spinner-extension"
      :position :absolute
      :top 5
      :right 30
      :color "#9a9a9a"
      :font-size 14]

     [".add-on"
      :background-color "#EEEEEE"
      :border "1px solid #CCCCCC"
      :height 32
      :line-height 20
      :min-width 35
      :padding "4px 5px"
      :text-align :center
      :text-shadow "0 1px 0 #FFFFFF"
      :width :auto
      :position :absolute
      :right -14
      :top 0
      :border-radius "0 3px 3px 0"]
     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Animated input boxes and textareas
     ["input[type=text], textarea"
      (with-prefixes "transition" "all 0.3s ease-in-out")])
