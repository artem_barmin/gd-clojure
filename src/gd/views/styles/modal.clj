(ns gd.views.styles.modal
  (:use gd.utils.common)
  (:use gd.utils.styles)
  (:use gd.views.styles.common)
  (:use gd.views.styles.buttons))

(css "resources/public/css/user/modal.css"

     [".ui-widget-overlay"
      :position :absolute
      :top 0
      :left 0
      :background-color "black"
      :opacity "0.6"
      "-ms-filter" "\"progid:DXImageTransform.Microsoft.Alpha(Opacity=60)\""
      "filter" "alpha(opacity=60)"]

     [".modal-carousel-button"
      :margin-left -4
      :margin-right -4]

     [".modal-carousel"
      :margin-top -10
      :margin-bottom -10]

     (simple-buttons ".modal-close" "/img/modal/exit" :margin-left :auto)
     (simple-buttons ".modal-left-arrow" "/img/modal/arrow_left" :margin-right 40)
     (simple-buttons ".modal-right-arrow" "/img/modal/arrow_right" :margin-left 40)

     [".modal-header" (bgimage "/img/modal/modal_area_top.png" "modal")]
     [".modal-footer" (bgimage "/img/modal/modal_area_bottom.png" "modal")]
     [".modal-main-wrapper" (grad-image "/img/modal/modal_area.png" "modal")]

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Stock modal
     (simple-buttons ".modal-pager-active .modal-carousel-left" "/img/modal/modal_arrow_left")
     (simple-buttons ".modal-pager-active .modal-carousel-right" "/img/modal/modal_arrow_right")

     (simple-buttons ".modal-pager-active .modal-carousel-top" "/img/modal/modal_arrow_top")
     (simple-buttons ".modal-pager-active .modal-carousel-bottom" "/img/modal/modal_arrow_bottom")

     [".modal-carousel-left"
      (bgimage "/img/modal/modal_arrow_left_noactive.png" "main-fixed-horizontal")]

     [".modal-carousel-right"
      (bgimage "/img/modal/modal_arrow_right_noactive.png" "main-fixed-horizontal")])
