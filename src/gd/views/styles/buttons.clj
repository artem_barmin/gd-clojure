(ns gd.views.styles.buttons
  (:use gd.utils.common)
  (:use gd.utils.styles)
  (:use gd.views.styles.utils)
  (:use gd.views.styles.common)
  (:require [clojure.string :as s]))

(def ^:dynamic *buttons-base* "/img/buttons/")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Complex buttons(from multiple parts)
(defn button[button-name & {:keys [color disable-text-shadow]}]
  (let [img (fn[vars] (str *buttons-base*
                           (s/replace button-name "-" "_")
                           "_"
                           (s/join "_" (map name vars)) ".png"))
        name-fn #(str "." button-name "-button-" %)
        rule (fn[prefix & rules] (conv (name-fn prefix) rules))
        image (fn[image-file type]
                (let [img (img image-file)]
                  (case type
                    :corner (bgimage img)
                    :grad (grad-image img))))
        [_ height] (file-dims (img [:left]))]
    (list

     (rule "container"
           :opacity "0.5"
           :text-align :center
           :cursor :pointer)

     (rule "container.enabled"
           :opacity "1")

     (rule "left"
           (image [:left] :corner)
           :float :left)

     (rule "right"
           (image [:right] :corner)
           :float :left)

     (rule "center"
           (image [:grad] :grad)
           :float :left)

     (rule "container:hover.enabled"
           (rule "left" (image [:hover :left] :corner))
           (rule "right" (image [:hover :right] :corner))
           (rule "center" (image [:hover :grad] :grad)))

     (rule "container:active.enabled"
           (rule "left" (image [:clicked :left] :corner))
           (rule "right" (image [:clicked :right] :corner))
           (rule "center" (image [:clicked :grad] :grad)))

     (rule "text"
           :color (or color :white)
           :line-height height
           (if-not disable-text-shadow (list :text-shadow "1px 1px 1px rgba(0, 0, 0, 0.5)"))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Simple buttons
(defn simple-buttons[name image-base & other]
  (let [img (fn[& [pref]] (str image-base (when pref "_") pref ".png"))
        sprite nil]
    (list
     [name
      (bgimage (img) sprite)
      other]

     [(str name ":hover")
      (bgimage (img "hover") sprite)]
     [(str name ":active")
      (bgimage (img "clicked") sprite)])))
