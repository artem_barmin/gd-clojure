(ns gd.utils.vk-import
  (:use noir.request
        gd.utils.vk-common
        noir.core
        swiss-arrows.core
        gd.utils.external-auth
        [gd.parsers.hlapi :only (download)]
        hiccup.core
        gd.utils.web
        hiccup.element
        gd.log
        digest
        clj-time.coerce
        clojure.data.json
        gd.utils.common
        [hiccup.util :only [url]]
        gd.model.model
        gd.parsers.llapi)
  (:refer-clojure :exclude [extend])
  (:require [noir.cookies :as cookies]
            [noir.session :as session]
            [clojure.string :as string]
            [clj-http.client :as client]
            [clojure.set :as sets]))

(defn get-photos-as-stocks[id aid counter & {:keys [entity] :or {entity :uid}}]
  (info "Starting vk import")
  (let [raw-info
        (->>
         (make-server-request "photos.get" {entity (Math/abs id) :aid aid})
         (map (fn[photo]
                {:photo (some identity ((juxt :src_big :src_xbig :src_xxbig) photo))
                 :meta-info {:url (str "http://vk.com/photo" id "_" (:pid photo))}
                 :caption (.replaceAll (:text photo) "<br>" "\n")})))]
    (set-counter counter :fetching (count raw-info))
    (info (format "Found %d stocks" (count raw-info)))
    (doall
     (remove
      nil?
      (map-indexed (fn[i {:keys [photo caption meta-info]}]
                     (info "Processing VK stock" i)
                     (dec-counter counter)
                     (when (prepare-str caption)
                       (let [lines (when caption (seq (.split caption "\n")))
                             get-parameter (fn[data]
                                             (-?<>
                                              (first (grep lines data))
                                              (.split ":")
                                              second
                                              (.split "(,)")
                                              (map prepare-str <>)
                                              (remove nil? <>)
                                              seq))
                             parse-size (let [raw-values (-?<>
                                                          (first (grep lines #"(?ium)РАЗМЕР"))
                                                          (.split ":")
                                                          second)]
                                          (when raw-values
                                            (-?<>
                                             (cond
                                               (or (re-find #"\(.*?[СМЛ -].*?\)" raw-values)
                                                   (re-find #"(универсальный|один размер)" raw-values))
                                               (.split raw-values ",")

                                               (re-find #"см" raw-values)
                                               (map (fn[v] (str v " см."))
                                                    (remove nil? (map prepare-str (.split raw-values "[.,см ]+"))))

                                               (re-find #"лет" raw-values)
                                               [raw-values]

                                               true
                                               (.split raw-values "[., ]+"))
                                             (map prepare-str <>)
                                             (remove nil? <>)
                                             seq)))
                             price (parsei (or (re-parse #"(?iu)Цена[ ]*:?[ ]*ОПТ[ ]*([0-9]+)[ ]*грн?" caption)
                                               (re-parse #"(?iu)Цена[ ]*:?[ ]*([0-9]+)[ ]*грн?[ ]*опт" caption)
                                               (re-parse #"(?ium)цена.*?([0-9]+)" caption)))]
                         {:price (* price 1.15)
                          :name (if lines
                                  (first lines)
                                  (format "%03d" i))
                          :fkey_category (:id (category:get-by-key "other"))
                          :meta-info meta-info
                          :description ""
                          :params (merge
                                   (when-let [size parse-size]{:size size})
                                   (when-let [color (get-parameter #"(?ium)ЦВЕТ")] {:color color})
                                   (when-let [fabric (get-parameter #"(?ium)ТКАНЬ")]{[:fabric false] fabric}))
                          :images (download photo)})))
                   raw-info)))))
