(ns gd.utils.basic-templates
  (:use
   clojure.walk
   com.reasonr.scriptjure
   hiccup.element
   noir.core
   gd.utils.web
   hiccup.form
   gd.utils.common
   hiccup.core
   hiccup.page
   net.cgrand.enlive-html))

(def ^:dynamic *current-context* nil)

(defn render-template[template]
  (prewalk (fn[node]
             (cond (and (vector? node) (= (first node) :template))
                   ((:data (second node)))

                   true node))
           template))

(defmacro defsubstag "Defines tag for substitution" [name & body]
  `(defn ~name[]
     [:template {:data (fn[] ~@body)}]))
