(ns gd.utils.vkontakte
  (:use noir.request
        gd.utils.vk-common
        noir.core
        gd.utils.external-auth
        clj-time.core
        com.reasonr.scriptjure
        hiccup.core
        gd.utils.web
        hiccup.element
        gd.log
        digest
        clj-time.coerce
        clojure.data.json
        gd.utils.common
        [hiccup.util :only [url]]
        gd.model.model)
  (:refer-clojure :exclude [extend])
  (:require [noir.cookies :as cookies]
            [gd.utils.security :as sec]
            [noir.session :as session]
            [clj-http.client :as client]
            [clojure.string :as string]
            [clojure.set :as sets]))

(defn- compute-md5[input-map]
  (let [entry-list (map (fn[key][key (get input-map key)]) (keys input-map))

        filtered (filter #(not= (first %) :sid) entry-list)

        ordered (reverse (sort-by first filtered))

        result (reduce #(str (str (name (first %2)) "=" (second %2)) %1) "" ordered)]
    (md5 (str result (vkontakte-secret)))))

(defn- make-request
  "Make request to vkontakte api method with given params(md5 sum computed automaticaly)"
  [params-input]
  (let [params (merge {:api_id (vkontakte-app-number)
                       :v "3.0"
                       :format "json"}
                      params-input)]
    (read-json
     (:body
      (client/get
       "http://api.vk.com/api.php"
       {:query-params (merge params {:sig (compute-md5 params)})})))))

(defn- parse-cookie
  "Parse vkontakte specific cookie into map"
  []
  (let [vk-cookie
        (filter
         #(= (first %) (str "vk_app_" (vkontakte-app-number)))
         (map
          #(string/split % #"=" 2)
          (string/split (get (:headers (ring-request)) "cookie") #"[ ]*;[ ]*")))]
    (if (not (empty? vk-cookie))
      (let [value (second (first vk-cookie))]
        (parse-url-to-map value)))))

(defn- check-session
  "Check if current vkontakte session is active:
1. If cookie exists
2. If cookies is not expired(:expire property)
3. Check signature of cookie

Returns : nil - if session is not active, session object(with sid, expire, mid) - otherwise"
  []
  (if-let [session (parse-cookie)]
    (if (before?
         (to-time-zone (now) (time-zone-for-offset 0))
         (from-long (* (Long/parseLong (:expire session)) 1000)))
      (if (= (md5 (str
                   "expire=" (:expire session)
                   "mid=" (:mid session)
                   "secret=" (:secret session)
                   "sid=" (:sid session)
                   (vkontakte-secret)))
             (:sig session))
        session
        (fatal "VK session hash not matched"))
      (fatal "VK session expired"))
    (fatal "VK cookie not set")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Authentification
(defn check-credentials
  "Get or create current user entity based on vkontakte session result"
  [code]
  (if-let [session (check-session)]
    (let [{:keys [bdate first_name last_name photo_big sex]}
          (get-in
           (make-request {:uids (:mid session)
                          :fields "first_name,sex,photo_big,bdate"
                          :method "users.get"
                          :sid (:sid session)})
           [:response 0])]
      (invite:use-external
       code
       (user:authorize-external
        (:mid session)
        :vk
        {:real-name (str first_name " " last_name)
         :birthdate (process-bdate bdate #"\." [:day :month :year])
         :info-photo {:path photo_big}
         :sex (tranform-sex 2 1 sex)})))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Social event checks
(defn- get-likes[page-id]
  (:response
   (read-json
    (:body
     (client/get
      (str "https://api.vk.com/method/" "likes.getList")
      {:query-params {:type "sitepage"
                      :filter "copies"
                      :item_id page-id
                      :owner_id (vkontakte-app-number)}})))))

(defn- is-group-member?[group-id]
  (if-let [session (check-session)]
    (= 1 (:response
          (make-request {:gid vkontakte-main-group
                         :uid (:mid session)
                         :method "groups.isMember"
                         :sid (:sid session)})))))

(defmethod check-social-rating :vk-like [_ page-id]
  ((set (:users (get-likes page-id))) (parse-int (sec/vk-uuid))))

(defmethod check-social-rating :vk-group [_ group-id]
  (is-group-member? group-id))
