(ns gd.utils.logger
  (:refer-clojure :exclude [extend])
  (:use gd.log
        [monger.query :exclude [find sort]]
        clj-time.core
        gd.utils.db
        clj-stacktrace.core
        gd.server.middleware
        monger.operators
        [monger.conversion :only [from-db-object]]
        gd.model.model
        gd.model.context)
  (:require
   [monger.core :as mg]
   [monger.collection :as mc]
   [clj-time.coerce :as coerce]
   [clj-time.format :as format])
  (:require
   [noir.request]
   [noir.server :as server]
   [noir.response :as resp]
   [monger.conversion :as mcon]
   [noir.session :as session]
   [monger.joda-time])
  (:import (org.joda.time DateTime)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Retrieveing utils

(defn- process-single-message
  "Process single message. Convert time to time offset from 'start'. Format timestamp.
Deserialize exceptions and parse them."
  [{:keys [timestamp uri exception] :as message} & [start]]
  (let [time-offset
        (if start
          (float (/ (in-secs (interval start timestamp)) 60))
          (coerce/to-long timestamp))

        exceptions
        (when exception
          {:original-exception (deserialize exception)
           :exception (parse-exception (deserialize exception))})]
    (->
     message
     (assoc :timeOffset time-offset)
     (assoc :timeFormatted (format/unparse custom-formatter timestamp))
     (merge exceptions))))

(defn- build-filters[start end filters]
  (merge
   (when (and start end) {:timestamp {$gt start $lt end}})
   {:del false}
   filters))

(defn- logger:retrieve-messages[& {:keys [start end filters message-transform]}]
  (->>
   (build-filters start end filters)
   (mc/find-maps logging)
   (map (fn[item](-> (process-single-message item start)
                     ((or message-transform identity)))))))

(defn- logger:distinct[key & {:keys [start end filters]}]
  (->>
   (mc/distinct logging key (build-filters start end filters))
   (map (fn[i](from-db-object i true)))))

(defn- logger:get-messages[source start end & {:keys [source-field filters]
                                               :or {source-field :source}}]
  (logger:retrieve-messages :start start :end end
                            :filters {source-field source}
                            :message-transform (fn[message]
                                                 (dissoc message :original-exception :session-id))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Logger(real time, group by users, merge distinct sessions if they used by same user)

(defn logger:get-all-messages
  "Extract all messages in form [[source items]]"
  [start end]
  (->>
   (logger:distinct :source :start start :end end :filters {:session-id {$ne nil}})
   (map (fn[source] [source (logger:get-messages source start end)]))
   (remove (comp empty? second))))

(defn logger:get-messages-for-user[user-id limit-num offset-num]
  (let [sessions (logger:distinct :session-id :filters {:source.id user-id})]
    (with-collection logging
      (skip offset-num)
      (limit limit-num)
      (sort {"$natural" -1})
      (find {:session-id {$in (vec sessions)}}))))

(defn logger:sessions[user-id]
  (logger:distinct :session-id :filters {:source.id user-id}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Rss messages

(defn get-rss [hosts]
  (let [end (new DateTime)
        start (.minusDays end 1)]
    (->>
     (mc/find-maps logging {:timestamp {$gt start $lt end}
                            :del false
                            :host {$in hosts}
                            :result :error})
     (map process-single-message))))
