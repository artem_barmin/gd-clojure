(ns gd.utils.vk-export
  (:use noir.request
        [clojure.algo.generic.functor :only (fmap)]
        gd.utils.vk-common
        noir.core
        gd.utils.external-auth
        clj-time.core
        com.reasonr.scriptjure
        hiccup.core
        hiccup.form
        clojure.test
        gd.utils.test
        gd.views.project_components
        gd.views.components
        clojure.data
        gd.views.messages
        gd.utils.web
        hiccup.element
        gd.log
        digest
        clj-time.coerce
        clojure.data.json
        gd.utils.common
        [hiccup.util :only [url]]
        gd.model.model)
  (:refer-clojure :exclude [extend])
  (:require [noir.cookies :as cookies]
            [clojure.string :as string]
            [clojure.set :as sets]
            [clj-http.client :as client]))

(def photos-limit 500)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Delete albums
(defn delete-all-group-albums[gid]
  (doall
   (map (fn[aid]
          (make-server-request "photos.deleteAlbum" {:gid gid :aid aid}))
        (map :aid (make-server-request "photos.getAlbums" {:gid gid})))))

(defn cleanup-empty-albums[gid]
  (doall
   (map (fn[aid]
          (make-server-request "photos.deleteAlbum" {:gid gid :aid aid}))
        (map :aid (filter #(zero? (:size %))
                          (make-server-request "photos.getAlbums" {:gid gid}))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Description parsing

(def ^:dynamic *export-config* nil)

(defn- description-to-category[description]
  (let [album-id (second (re-find #"Альбом #([0-9]+)" description))]
    (when album-id (parse-int album-id))))

(defn- category-to-description[category-id]
  (str "Альбом #" category-id))

(defn- description-to-stock[text]
  (second (or (re-find #"http://dayte-dve.com.ua/s/([0-9]+)" text)
              (re-find #"Товар #([0-9]+)" text))))

(defn- stock-to-description[id]
  (if (:stock-urls *export-config*)
    (str "Чтобы заказать товар - перейдите по ссылке http://dayte-dve.com.ua"
         (url-for stock-shortcut {:id id}))
    (str "Товар #" id)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Exporter categories album -> stocks

(defn- get-existing-category-albums[group-id]
  (or (->>
       (make-server-request "photos.getAlbums" {:gid group-id})
       (filter (fn[{:keys [size]}] (< size photos-limit)))
       (map (fn[{:keys [aid description size]}]
              (let [category (description-to-category description)]
                (when category {category {:album-id aid :size size}}))))
       (remove nil?)
       (reduce merge))
      {}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; VK photos exporter. Works only with OAuth token(because only for it we can request 'offline' scope).

(declare create-category-album)

(defn- send-deal-photos[category group-id {:keys [album-id size]} images counter]
  (let [limit-left (- photos-limit size)
        images-for-this-album (take limit-left images)
        images-left (drop limit-left images)
        url (:upload_url
             (make-server-request "photos.getUploadServer"
                                  {:aid album-id
                                   :gid group-id}))]

    ;; process images for this album we limited to 500 photos per album
    (doall
     (map-indexed
      (fn[i {:keys [image caption]}]
        (if (and (> i 0) (zero? (mod i 250)))
          (Thread/sleep (* 30 1000)))
        (info "Processing" image caption)
        (let [uploaded-photos
              (read-json
               (:body (client/post url {:multipart
                                        [{:name "file1"
                                          :content (clojure.java.io/as-file
                                                    (str "resources/public" image))}]})))

              saved-photos
              (make-server-request "photos.save"
                                   (merge uploaded-photos
                                          {:gid group-id
                                           :aid album-id
                                           :caption (subs caption 0 (min (count caption) 600))}))]
          (dec-counter counter)
          (info "Left VK photos" @counter)
          saved-photos))
      images-for-this-album))

    ;; process rest of images with creation of new album
    (when-not (empty? images-left)
      (let [new-album (create-category-album group-id category)]
        (send-deal-photos category group-id {:album-id new-album :size 0} images-left counter)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Categories creator

(defn- create-album[group-id title description]
  (Thread/sleep (* 10 1000))
  (:aid (make-server-request "photos.createAlbum"
                             {:gid group-id
                              :title title
                              :privacy 0
                              :description description})))

(defn- create-category-album[group-id category-to-create]
  (let [name (:name (group-deal:get-album category-to-create))]
    (info "Category created" category-to-create name)
    (create-album group-id name
                  (category-to-description category-to-create))))

(defn- upload-images-by-categories[group-id images-with-categories counter]
  (let [categories (get-existing-category-albums group-id)]
    (doseq [[category images] (group-by :category images-with-categories)]
      (send-deal-photos category group-id (or (categories category)
                                              {:album-id (create-category-album group-id category)
                                               :size 0})
                        images counter))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Group and deal syncronizer (move photos if they changed category)

(defn delete-photo[photo-id gid]
  (make-server-request "photos.delete" {:pid photo-id :gid gid}))

(defn move-photo-other-album[photo-id target-aid gid]
  (make-server-request "photos.move" {:pid photo-id :target_aid target-aid :gid gid}))

(defn get-album-photos[gid aid]
  (make-server-request "photos.get" {:gid gid :aid aid}))

(defn extract-current-albums-state
  "Returns list of maps {:stock stock-id :photo vk-photo-id :album album-id
:category category-id}. Will return only stocks that belongs to specific deal."
  [gid deal-id counter]
  (let [deal-stocks (set (map :id (stock:organizer-stocks nil nil {:deal deal-id :only-id true})))
        albums (make-server-request "photos.getAlbums" {:gid gid})]
    (set-counter counter :extracting (count albums))
    (remove nil?
            (mapcat (fn[{:keys [aid description title]}]
                      (dec-counter counter)
                      (when-let [category (description-to-category description)]
                        (map (fn[{:keys [pid text]}]
                               (when-let [stock (description-to-stock text)]
                                 (let [stock-id (parse-int stock)]
                                   (when (deal-stocks stock-id)
                                     {:photo pid
                                      :category category
                                      :stock stock-id
                                      :album aid
                                      :album-title title}))))
                             (get-album-photos gid aid))))
                    albums))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; High level logick of stocks moving

(defn- compute-stocks-to-move[current-album-state images-with-categories]
  (let [current-stock-id-info-map (fmap first (group-by :stock current-album-state))
        stock-category-list (fn[lst](map (fn[{:keys [stock category]}] {stock category}) lst))

        requested-stock-to-category (stock-category-list images-with-categories)
        requested-stock-to-category-map (reduce merge requested-stock-to-category)

        stock-with-changed-category
        (nth (diff (set (stock-category-list current-album-state))
                   (set requested-stock-to-category)) 1)]
    (reduce merge
            (map
             (fn[stock]
               ;; we change category only in case when photo was already uploaded
               (let [[stock-id] (first stock)]
                 (when-let [{:keys [category]} (current-stock-id-info-map stock-id)]
                   {stock-id (requested-stock-to-category-map stock-id)})))
             stock-with-changed-category))))

(deftest stock-moving-test
  (is (= (compute-stocks-to-move [{:stock 1 :category 1} {:stock 2 :category 2} {:stock 3 :category 3}]
                                 [{:stock 1 :category 5} {:stock 5 :category 5} {:stock 3 :category 6}])
         {1 5 3 6}))

  (is (= (compute-stocks-to-move [{:stock 1 :category 1} {:stock 2 :category 2}]
                                 [{:stock 1 :category 1} {:stock 2 :category 2} {:stock 3 :category 3}])
         nil)))

(defn sync-deal-with-albums[gid deal-id images-with-categories counter]
  (let [current-album-state (or (extract-current-albums-state gid deal-id counter) {})
        current-stock-id-info-map (fmap first (group-by :stock current-album-state))
        album-names (distinct (map (fn[v](select-keys v [:album :album-title :category])) current-album-state))]

    ;; process addition and deletion of stocks
    (let [all-uploaded-stocks (set (map :stock current-album-state))
          all-requested-stocks (set (map :stock images-with-categories))
          [to-delete to-add] (map (comp set (partial remove nil?)) (diff all-uploaded-stocks all-requested-stocks))]

      (info "Added" to-add)
      (info "Deleted" to-delete)

      (set-counter counter :adding (count to-add))

      ;; process stocks that should be added
      (when (seq to-add)
        (info (format "Adding %d stocks" (count to-add)))
        (upload-images-by-categories gid
                                     (filter (fn[{:keys [stock]}](to-add stock)) images-with-categories)
                                     counter))

      (set-counter counter :deleting (count to-delete))

      ;; process deletion
      (when (seq to-delete)
        (doseq [stock-id to-delete]
          (info "Delete stock" stock-id)
          (delete-photo (:photo (current-stock-id-info-map stock-id))
                        gid)
          (dec-counter counter))))

    ;; process categories change
    (let [categories (atom (get-existing-category-albums gid))
          photos-to-move (compute-stocks-to-move current-album-state images-with-categories)]
      (set-counter counter :moving (count photos-to-move))
      (doseq [[stock-id category-id] photos-to-move]
        (let [register-new-album (fn[album-id category-id]
                                   (create-category-album gid category-id)
                                   (reset! categories (get-existing-category-albums gid))
                                   (@categories category-id))

              {:keys [size album-id]} (or (@categories category-id) (register-new-album nil category-id))

              album-id (if (< size photos-limit)
                         album-id
                         (:album-id (register-new-album album-id category-id)))]

          (info "Move photo" (:photo (current-stock-id-info-map stock-id)) "to album" album-id)

          (move-photo-other-album (:photo (current-stock-id-info-map stock-id))
                                  album-id
                                  gid)

          (dec-counter counter)

          (swap! categories update-in [category-id :size] inc))))

    ;; process albums rename
    (let [all-albums (group-deal:get-albums deal-id :show-all true)]
      (doseq [{:keys [album album-title category]} album-names]
        (let [{:keys [name]} (some (fn[v](and (= category (:id v)) v)) all-albums)]
          (when-not (= name album-title)
            (make-server-request "photos.editAlbum" {:aid album
                                                     :gid gid
                                                     :title name})
            (info "Renamed album" album "from" album-title "to" name)))))

    (set-counter counter :finished)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Top level export method

(defn- prepare-stock-for-export[{:keys [images id stock-parameter-value description
                                        name price fkey_album]}]
  (when (and fkey_album ((:albums *export-config*) fkey_album))
    (let [max-price-change (apply max (map (fn[v](or (:price-change v) 0))
                                           stock-parameter-value))

          caption (string/join
                   "\n"
                   (flatten
                    [(str name " - " (+ price (if (:max-price *export-config*) max-price-change 0)) "грн.")
                     (stock-to-description id)
                     (let [grouped-params (->>
                                           stock-parameter-value
                                           (filter :visible)
                                           (filter :enabled)
                                           (group-by :name))]
                       (map (fn[[key values]]
                              (str (! "deal.profile.filter." key) ": "
                                   (string/join ", " (map :value values))))
                            grouped-params))
                     "\n"
                     (when (seq description)
                       (render-html (html (render-desc description))))]))]
      {:stock id
       :image (images:get nil (first images))
       :caption caption
       :category fkey_album})))

(defn- create-stocks-for-export[deal-id]
  (map prepare-stock-for-export
       (stock:organizer-stocks nil nil {:deal deal-id :state [:visible]})))

(defn vk-export-deal[deal counter group-id & {:keys [stock-urls albums max-price]}]
  (let [all-deal-images (create-stocks-for-export deal)]
    (binding [*export-config* {:stock-urls (boolean stock-urls)
                               :max-price max-price
                               :albums albums}]
      (sync-deal-with-albums (and group-id (parse-int group-id)) deal all-deal-images counter))
    counter))

(run-tests)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Export news

(defn export-news
  "Initialize or update news export settings"
  [old-schedule-id period export-config deal]
  (let [data {:export-config export-config
              :deal deal
              :token *vk-auth-token*}]
    (schedule:start-or-update period :vk-news data deal)))

(defmethod schedule:execute-job :vk-news [_ {:keys [export-config token]} deal]
  (binding [*vk-auth-token* token]
    (info "Start exporting multiple news" (count export-config))
    (doseq [{:keys [gid message]} export-config]
      (info "Export news in vkontakte" gid message token)
      (let [current-album-state (remove nil? (or (extract-current-albums-state gid deal (atom nil)) {}))
            whole-number (count current-album-state)]
        (make-server-request "wall.post" {:message message
                                          :attachments (clojure.string/join ","
                                                                            (map
                                                                             (fn[{:keys [photo]}]
                                                                               (str "photo-" gid "_" photo))
                                                                             (map (fn[_]
                                                                                    (nth
                                                                                     current-album-state
                                                                                     (rand-int (dec whole-number))))
                                                                                  (range 0 7))))
                                          :owner_id (* -1 gid)
                                          :from_group 1})
        (Thread/sleep 5000)))))

(defn current-news-params[deal-id]
  (schedule:get-by-type-and-entity deal-id :vk-news))
