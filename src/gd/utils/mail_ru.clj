(ns gd.utils.mail-ru
  (:use noir.request
        digest
        clj-time.core
        clj-time.coerce
        clojure.data.json
        gd.utils.common
        gd.model.model)
  (:refer-clojure :exclude [extend])
  (:require [noir.cookies :as cookies]
            [clojure.string :as string]
            [clj-http.client :as client]))

(def mail-ru-secret "0facc9117713778b6b3d69fa3955cc2b")
(def mail-ru-app-number 678365)

(defn- compute-md5[input-map]
  (let [entry-list (map (fn[key][key (get input-map key)]) (keys input-map))

        filtered (filter #(not= (first %) :sid) entry-list)

        ordered (reverse (sort-by first filtered))

        result (reduce #(str (str (name (first %2)) "=" (second %2)) %1) "" ordered)]
    (md5 (str result mail-ru-secret))))

(defn- sig-params[params]
  (md5
   (str
    (reduce (fn[res [k v]](as-str res k "=" v)) "" (sort-by first (dissoc params :sig)))
    mail-ru-secret)))

(defn make-request
  "Make request to vkontakte api method with given params(md5 sum computed automaticaly)"
  [params-input]
  (let [params (merge {:api_id mail-ru-app-number
                       :secure "1"}
                      params-input)]
    (read-json
     (:body
      (client/get
       (str "http://www.appsmail.ru/platform/api")
       {:query-params (merge params {:sig (sig-params params)})})))))

(defn- parse-cookie
  "Parse mail ru specific cookie into map"
  []
  (let [cookie (cookies/get "mrc")]
    (reduce merge {}
            (map
             (fn[[k v]][(keyword k) v])
             (map #(string/split % #"=") (string/split cookie #"&"))))))

(defn- check-session
  "Get or create current user entity based on mail ru session result"
  []
  (if-let [session (parse-cookie)]
    (when (= (:sig session)
             (sig-params session))
      session)))

(defn- tranform-sex[value]
  (case value
    1 :female
    0 :male
    nil))

(defn check-credentials
  "Get or create current user entity based on mail ru session result"
  [code]
  (if-let [session (check-session)]
    (let [{:keys [first_name last_name pic_big sex has_pic]}
          (nth (make-request {:uids (:vid session)
                              :method "users.getInfo"
                              :session_key (:session_key session)}) 0)]
      (invite:use-external
       code
       (user:authorize-external
        (:vid session)
        :my-world
        (merge
         {:real-name (str first_name " " last_name)
          :info-photo {:path (when (= has_pic 1) pic_big)}
          :sex (tranform-sex sex)}))))))
