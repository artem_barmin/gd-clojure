(ns gd.utils.test
  (:use robert.hooke)
  (:refer-clojure :exclude [extend])
  (:use clj-time.core)
  (:use korma.core)
  (:use [clojure.test :only [are *test-out*]])
  (:use clojure.math.numeric-tower)
  (:use gd.utils.db)
  (:require
   [noir.session :as session]
   [clojure.java.jdbc :as jdbc]
   [difftest.core :as difftest]
   [noir.options]
   [clj-time.coerce :as tm]
   [faker.lorem :as faker]))

;; Test utilites
(defmacro multidef[& coll]
  `(do ~@(for [[name value] (partition-all 2 coll)]
           `(def ~name ~value))))

(defmacro multi-insert[entity & coll]
  `(map #(insert ~entity (values %)) [~@coll]))

(defn check-ids[coll]
  (some #(:id %) coll))

(defn eq-coll[coll1 coll2]
  (= (set coll1) (set coll2)))

(defmacro are-equals[& coll]
  `(are [x y] (= x y) ~@coll))

(defmacro are-equals-except[keys & coll]
  `(are [x y] (= (dissoc x ~@keys) (dissoc y ~@keys)) ~@coll))

(defmacro are-equals-only[keys & coll]
  `(are [x y] (= (select-keys x ~keys) (select-keys y ~keys)) ~@coll))

(defn sentences[num]
  (reduce str (take num (faker/sentences))))

(defn rnd[from to]
  (+ from (round (* (rand) (- to from)))) )

(defn noir-binding[f & args]
  (binding[noir.options/*options* {:mode :dev}]
    (apply f args)))

(add-hook #'clojure.test/test-var #'noir-binding)

(alter-var-root #'*test-out* (constantly *out*))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Difftest
(difftest/activate)

(alter-var-root #'difftest/*color* (constantly false))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Test utils

(defmacro defspec  [sym & body]
  `(clojure.test/deftest ~sym
     (try
       (transaction
         (jdbc/set-rollback-only)
         (gd.model.model/save-categories-tree [["other" "other"]])
         (reset! (deref #'gd.model.model/cache) {})
         (binding [gd.model.context/*omit-cache* true]
           ~@body))
       (finally
         (reset! gd.model.model/last-executed-scheduled-task {})
         (overtone.at-at/stop-and-reset-pool! gd.model.model/schedule-execution-pool)))))

(defmacro When [& body]
  "Allows the output of this block to be assigned to a
   symbol that can be used in your Then clause"
  `(do ~@body))

(defmacro run-model-tests[]
  `(do
     (gd.model.test.data_builders/enable-dev-mode)
     (gd.model.test.data_builders/clean-db)
     (taoensso.timbre/set-level! :report)
     (try
       (clojure.test/run-tests)
       (finally
         (taoensso.timbre/set-level! :debug)))))
