(ns gd.utils.external-auth
  (:use noir.request
        gd.log
        digest
        clj-time.core
        clj-time.coerce
        clojure.data.json
        gd.utils.common
        gd.model.model)
  (:refer-clojure :exclude [extend])
  (:require [noir.cookies :as cookies]
            [clojure.string :as string]
            [clj-http.client :as client]))

(defn process-bdate[bdate separator fields]
  (try
   (when (and bdate (= 3 (count (string/split bdate separator))))
    (let [splitted
          (string/split bdate separator)

          {:keys [day month year]}
          (reduce merge (map-indexed (fn[i key]{key (nth splitted i)}) fields))]
      (if (and day month year)
        (date-time (parse-int year) (parse-int month) (parse-int day)))))
   (catch Exception e
     (fatal e "Can't parse exception"))))

(defn tranform-sex[male female value]
  (case value
    male :female
    femal :male
    nil))
