(in-ns 'gd.utils.web)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Validation framework
(defn client-side[rules]
  (js*
   (_.flatten (.map (clj (vec (map (fn[{:keys [client]}] (js* (fn[] (clj client)))) rules)))
                    (fn[f] (return (.apply f)))))))

(defn server-side[rules]
  (map :server rules))

(defn validate-client-side[validation-rules]
  (let [validate (client-side validation-rules)]
    (js*
     (fn[form]
       (var formContent (. form serializeObject))
       ((fn[form content](clj validate)) form formContent)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Validators

(defn not-valid[name message]
  (throwf "Validation exception: field %s - %s" name message))

(def ^{:doc "Used for client side validation"}
  url-regexp   #"^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-‌​\.\?\,\'\/\\\+&amp;%\$#_=]*)?$")

(def url-validator (new org.apache.commons.validator.routines.UrlValidator (into-array ["http" "https"])))

(defn- find-element[name]
  (js* (var area (. form find (byParam "name" (clj (as-str name)))))))

(defn- wysiwyg-text-server[str]
  (-> str
      unescape-html
      (.replaceAll "\\[.*?\\]" " ")
      (.replaceAll "&nbsp;" "")
      (.replaceAll "[\r\n]" "")
      (.replaceAll "[ ]+" "")))

(defn- wysiwyg-length-client[name]
  (js*
   (? (aget content (clj name))
      (..
       (aget content (clj name))
       (replace #"\[.*?\]" " ")
       (replace #"&nbsp;" "")
       (replace #"[\r\n]" "")
       (replace #"[ ]+" "")
       length)
      0)))

(defn get-param[name]
  (get-in
   (:params (noir.request/ring-request))
   (map keyword (ring.middleware.nested-params/parse-nested-keys name))))

(defn wysiwyg-required[name message]
  {:client (js* (if (= 0 (clj (wysiwyg-length-client name)))
                  (do
                    (wysiwygValidationError form (clj message))
                    (throw "Empty message"))))
   :server (fn[]
             (if (= 0 (count (wysiwyg-text-server (get-param name))))
               (not-valid name "Empty message")))})

(defn wysiwyg-length[name length message]
  {:client (js* (if (< (clj length) (clj (wysiwyg-length-client name)))
                  (do
                    (wysiwygValidationError form (clj message))
                    (throw "Message too large"))))
   :server (fn[]
             (if (< length (count (wysiwyg-text-server (get-param name))))
               (not-valid name "Message too large")))})

(defn textarea-required-and-length[name length empty-message-text too-long-message-text]
  {:client (js*
            (var length (clj (wysiwyg-length-client name)))
            (clj (find-element name))
            (if (or (> length (clj length)) (= length 0))
              (do
                (if (> length (clj length))
                  (showValidation (clj too-long-message-text) area))
                (if (= length 0)
                  (showValidation (clj empty-message-text) area))
                (throw "Invalid message"))))
   :server (fn[]
             (let [cnt (count (wysiwyg-text-server (get-param name)))]
               (if (< length cnt)
                 (not-valid name "Message too large"))
               (if (= 0 cnt)
                 (not-valid name "Empty message"))))})

(defn textarea-regexp[name regexp bad-message-text & [inverse]]
  {:client (js*
            (clj (find-element name))
            (var str (aget content (clj name)))
            (maybe-seq-body
             str area
             (if (clj (if inverse
                        (js* (. str match (clj regexp)))
                        (js* (! (. str match (clj regexp))))))
               (do
                 (showValidation (clj bad-message-text) area)
                 (throw "Invalid message")))))
   :server (fn[]
             (doseq [param (maybe-seq (get-param name))]
               (if ((if inverse identity not) (re-find regexp param))
                 (not-valid name "Invalid message"))))})

(defn textarea-number[name min-val max-val bad-message-text]
  {:client (js*
            (clj (find-element name))
            (var number (parseInt (aget content (clj name))))
            (if (or (> number (clj max-val) ) (< number (clj min-val)))
              (do
                (showValidation (clj bad-message-text) area)
                (throw "Invalid message"))))
   :server (fn[]
             (let [number (parse-int (get-param name))]
               (if (or (> number max-val ) (< number min-val))
                 (not-valid name "Invalid message"))))})

(defn textarea-url[name bad-message-text]
  {:client (js*
            (clj (find-element name))
            (var str (aget content (clj name)))
            (if (and str (! (. str match (clj url-regexp))))
              (do
                (showValidation (clj bad-message-text) area)
                (throw "Invalid message"))))
   :server (fn[]
             (let [val (get-param name)]
               (if-not (or (empty? val) (.isValid url-validator val))
                 (not-valid name "Invalid message"))))})

(defn textarea-email[name bad-message-text]
  (textarea-regexp name #".+" bad-message-text))

(def phone-regexp #"^(?:\+38)?0[0-9]{9}$")

(def email-regexp #"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")

(defn textarea-phone[name bad-message-text]
  (textarea-regexp name phone-regexp bad-message-text))

(defn textarea-number-format[name bad-message-text]
  (textarea-regexp name #"[0-9]+" bad-message-text))

(defn textarea-required[name empty-message-text]
  (textarea-required-and-length name 999999 empty-message-text nil))

(defn dropdown-required[name bad-message-text]
  {:client (js*
            (clj (find-element name))
            (set! area (.closest area ".dk_container"))
            (var str (aget content (clj name)))
            (if (= str "null")
              (do
                (showValidation (clj bad-message-text) area)
                (throw "Invalid message"))))
   :server (fn[]
             (let [val (get-param name)]
               (if (= val "null")
                 (not-valid name "Invalid message"))))})

(defn values-exists[name element-to-show bad-message-text]
  {:client (js*
            (clj (find-element name))
            (var validationError (. form find (byParam "name" (clj (as-str element-to-show)))))
            (if (or (= (.. area length) 0) (= (typeof (aget content (clj name))) "undefined"))
              (do
                (showValidation (clj bad-message-text) validationError)
                (throw "Invalid message"))))
   :server (fn[]
             (if (empty? (get-param name))
               (not-valid name (format "Value '%s' not exists in params-map" name))))})

(defn validator-or[validator1 validator2]
  {:client (js*
            (var first true)
            (var second true)
            (try
              (clj (:client validator1))
              (catch e (set! first false)))
            (try
              (clj (:client validator2))
              (catch e (set! second false)))
            (if (and (! first) (! second))
              (throw "Validation failed")
              (hideValidation)))
   :server (fn[]
             (when-not
                 (or
                  (try
                    ((:server validator1))
                    true
                    (catch Exception e
                      false))
                  (try
                    ((:server validator2))
                    true
                    (catch Exception e
                      false)))
               (not-valid name "Validation failed")))})

(defn same-passwords[field1 field2 bad-message-text]
  {:client (js*
            (clj (find-element field1))
            (if (! (= (aget content (clj field1)) (aget content (clj field2))))
              (do
                (showValidation (clj bad-message-text) area)
                (throw "Invalid message"))))
   :server (fn[]
             (let [[v1 v2] (map get-param [field1 field2])]
               (if (not= v1 v2)
                 (not-valid field1 (format "Values are not the same : %s %s" v1 v2)))))})

(defn remote-validator[name remote bad-message-text]
  {:client (js*
            (clj (find-element name))
            (var value (aget content (clj name)))
            (var promise (clj remote))
            (.fail promise (fn[] (showValidation (clj bad-message-text) area)))
            (return promise))
   :server (fn[] true)})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Switch on form value
(declare custom-concat)

(defn validator-switch[switch-value-name & validator-pairs]
  {:client (custom-concat
            (list 'do)
            (custom-concat
             [(js* (var str (aget content (clj switch-value-name))))]
             (for [{:keys [value validators]} validator-pairs]
               (js* (if (= (clj value) str)
                      (return (clj (client-side validators))))))))
   :server (fn[]
             (let [switch-value (get-param switch-value-name)]
               (doseq [{:keys [value validators]} validator-pairs]
                 (if (= value switch-value)
                   (doseq [validator (server-side validators)]
                     (validator))))))})

(defn validator-case[case-value & validators]
  {:value case-value
   :validators (flatten validators)})

(defn validator-default-case[& validators]
  {:value nil
   :validators (flatten validators)})
