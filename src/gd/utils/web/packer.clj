(in-ns 'gd.utils.web)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Low level packing - pack list of scripts
(def already-prepared-plans (atom {}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stable set utils(we need them to preserve order)

(defn- intersection[col1 col2]
  (filter (set col2) col1))

(defn- difference[col1 col2]
  (remove (set col2) col1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Plans transformation functions

(def ^{:doc "Plan for packing scripts.
Special values:
:all - pack all directory in one script, so whole directory will be returned on any request to library
:none - don't pack scripts, they all must be distinct"}
  packing-plan
  {:js {:user :all
        :bono :all
        :admin [[:common] :all]
        :themes :none
        :custom-lib [[:common] :all]
        :lib [[:tooltip :history :jquery.fileupload :jquery.elevatezoom]
              [:angular-strap :angular-strap.tpl]]
        :retail :all
        :admin-lib :all
        :util :all
        :external :none}
   :css {:user :none
         :custom-lib :all
         :seo :all
         :lib :all
         :bono :all
         :grid :all
         :themes-external :none
         :admin [[:admin] [:print]]
         :retail :all
         :admin-lib :all
         :util :all
         :external :none}})

(defn- get-plans-for-key[type lib]
  (or (get-in packing-plan [type lib])
      (throwf "No plan defined for {:type %s :lib %s}" type lib)))

(defn- choose-best-plan
  "Choose plan that have maximum intersection with requested plan"
  [requested-plan all-plans]
  (when (seq requested-plan)
    (let [estimated-plans
          (map (fn[possible-plan]
                 (when-not (= possible-plan :all)
                   {:diff (difference requested-plan possible-plan)
                    :intersection (intersection requested-plan possible-plan)
                    :estimate (count (intersection requested-plan possible-plan))}))
               all-plans)

          best-plan
          (reduce (partial max-key :estimate) (remove nil? estimated-plans))]
      (if (or (empty? (:diff best-plan)) (not ((set all-plans) :all)))
        best-plan
        {:diff [:all] :intersection []}))))

(defn- choose-multiple-best-plans[requested-plan all-plans]
  (if (and (every? string? requested-plan) (not= all-plans :none))
    (throwf "External script only supported for :none type of plan %s" requested-plan)
    (case all-plans
        :all [[:all]]
        :none (map vector requested-plan)
        (loop [rest requested-plan
               compacted nil]
          (let [{:keys [diff intersection]} (choose-best-plan rest all-plans)]
            (if (empty? intersection)
              (remove empty? (reverse (concat (reverse (map vector diff)) compacted)))
              (recur diff (cons intersection compacted))))))))

(deftest choose-plan-test
  (is (= (choose-multiple-best-plans [:jquery :bootstrap-js] :all) [[:all]]))

  (is (= (choose-multiple-best-plans [:jquery :bootstrap-js] :none)
         [[:jquery] [:bootstrap-js]]))

  (is (= (choose-multiple-best-plans [:jquery :bootstrap-js :test] [[:jquery :bootstrap-js]])
         [[:jquery :bootstrap-js] [:test]]))

  (testing "preserve order"
    (is (= (choose-multiple-best-plans [:jquery :test :bootstrap-js] [[:jquery :bootstrap-js]])
           [[:jquery :bootstrap-js] [:test]])))

  (testing ":all should match whole directory but only in case if no perfect plan is found, so in case of better estimates it should not be selected"
    (is (= (choose-multiple-best-plans [:common] [[:common] :all])
           [[:common]]))

    (is (= (choose-multiple-best-plans [:common :test] [[:common :test] :all])
           [[:common :test]]))

    (is (= (choose-multiple-best-plans [:common :other1 :other2] [[:common] :all])
           [[:all]]))

    (is (= (choose-multiple-best-plans [:common :test :other1 :other2] [[:common :test] :all])
           [[:all]]))

    (is (= (choose-multiple-best-plans [:common :test :other1 :other2] [[:common :test]])
           [[:common :test] [:other1] [:other2]])))

  (is (= (choose-multiple-best-plans ["http://jquery.js" "http://bootstrap.js"] :none)
         [["http://jquery.js"] ["http://bootstrap.js"]])))

(run-tests)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Top level packing function

(declare concat-scripts)

(defn pack-scripts
  [type lib requested-scripts]
  (let [key [type lib requested-scripts]]
    (when-not (@already-prepared-plans key)
      (println "-------------")
      (let [all-plans (get-plans-for-key type lib)
            plans (choose-multiple-best-plans requested-scripts all-plans)
            files (doall (distinct (mapcat (fn[plan](maybe-seq (concat-scripts type lib plan))) plans)))]
        (clojure.pprint/pprint {:type type
                                :lib lib
                                :plans plans
                                :requested requested-scripts
                                :result files})
        (swap! already-prepared-plans assoc key files)))
    (@already-prepared-plans key)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Build URL to script

(def external-files #{:themes-external :external})

(def packed-path "resources/public")

(defn- all-files[type lib]
  (let [path (str "resources/public/" (name type) "/" (name lib))]
    (->> (new java.io.File path)
         file-seq
         (filter #(.isFile %))
         (map #(.getName %))
         (filter (partial re-find #"(js|css)$"))
         (map (comp keyword second (partial re-find #"(.*)\.(js|css)"))))))

(defn- remote-file?[value]
  (and (string? value) (.startsWith value "http://")))

(defn- build-url[type lib value]
  (let [value (get common-includes value value)]
    (if (or (remote-file? value) (external-files lib))
      value
      (as-str (when-not (dev-mode?) "/packed")  "/" type "/" lib "/" value "." type))))

(defn- concat-scripts[type lib plan]
  (let [build-url
        (partial build-url type lib)

        requested-files
        (order-scripts (if (= plan [:all]) (all-files type lib) plan))

        choosed-files
        (map build-url requested-files)]
    (cond
      (dev-mode?)
      choosed-files

      (or (some remote-file? choosed-files) (external-files lib))
      (do (println "Skipping : " choosed-files) choosed-files)

      true
      (do
        (println "Start concatenate : " type lib requested-files)
        (let [files-to-process
              (map (partial str packed-path "/") choosed-files)

              whole-content
              (->> files-to-process
                   (map slurp)
                   (strings/join (if (= type :js) ";" "\n")))

              content-key
              (md5 whole-content)

              plan-destination
              (as-str packed-path "/packed/" type "/" lib "/" content-key "." type)]
          (println "Concatenate : " requested-files " -> " plan-destination)
          (spit plan-destination whole-content)
          (build-url content-key))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Minification methods

(def uglify (new ro.isdc.wro.extensions.processor.js.UglifyJsProcessor))
(def css-min (new ro.isdc.wro.extensions.processor.css.YUICssCompressorProcessor))

(defn- pack-js-css [in out]
  (.mkdirs (.getParentFile (io/as-file out)))
  (let [type (if (.endsWith in "js") :js :css)

        in-file (io/as-file in)
        reader (io/reader (.getAbsolutePath in-file))

        out-file (io/as-file out)
        writer (io/writer (.getAbsolutePath out-file))

        resource (ro.isdc.wro.model.resource.Resource/create
                  out
                  (if (= type :js) ResourceType/JS ResourceType/CSS))

        processor (if (= type :js) uglify css-min)]
    (println "Minification : " in " -> " out)
    (.process processor resource reader writer)))

(defn- check-resource-file-sum
  "Check if content of file is already processed. Used md5 sum for content."
  [file file-to-check]
  (let [original-sum
        (md5 (slurp (.getAbsolutePath (io/as-file file))))

        should-rebuld
        (or (not (exists? file-to-check)) ;or - file with check sum is not exists
            (not (= (slurp (.getAbsolutePath (io/as-file file-to-check))) original-sum)) ;checksums is not equals
            )]

    ;; save check sum for next start
    (.mkdirs (.getParentFile (io/as-file file-to-check)))
    (spit file-to-check original-sum)

    (when should-rebuld file)))

(defn- minify-scripts[type lib plans]
  (let [lib-path (as-str "resources/public/" type "/" lib "/")]
    ;; prepare files with yui compressor
    (doseq [f (all-files type lib)]
      (let [destination-file (as-str packed-path "/packed/" type "/" lib "/" f "." type)
            f (as-str lib-path f "." type)]
        (if-let [source-file (check-resource-file-sum f (str destination-file ".checksum"))]
          (pack-js-css source-file destination-file))))))

(defn minify-all[]
  (doseq [[type libs-with-plans] packing-plan]
    (doseq[[lib plans] libs-with-plans]
      (minify-scripts type lib plans))))

(defn rebuild-all-scripts[]
  (reset! already-prepared-plans nil)
  (minify-all))
