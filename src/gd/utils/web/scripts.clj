(in-ns 'gd.utils.web)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Process script dependencies

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Scripts inside library ordering
(def ^{:doc "Script dependencies inside library. Keyword :all - will expand to all script in library, and
means that all scripts in library depends from this script"
       :private true}
  script-dependencies {:angular-sanitize :angular
                       [:angular :jquery-ui :bootstrap-js] :jquery
                       [:bootstrap-js] :jquery-ui
                       [:nv.d3] :d3
                       [:legend] :nv.d3
                       :angular-strap.tpl :angular-strap
                       :all :common})

(defn- order-scripts[scripts]
  (let [scripts
        (set scripts)

        process-single-dependency
        (fn[[from to]]
          (if (and (= :all from) (scripts :common))
            ;; connect each script with :common
            (mreduce (fn[from]{from to}) (disj scripts :common))
            ;; connect all dependencies that available in scripts
            (mreduce (fn[from] (when (scripts from) {from to})) (maybe-seq from))))

        dependencies (reduce (partial merge-with concat)
                             (map (partial fmap list) (remove empty? (map process-single-dependency script-dependencies))))]
    (if dependencies
      (->>
       {:nodes (set (concat scripts (keys dependencies) (flatten (vals dependencies))))
        :neighbors (fmap maybe-seq dependencies)}
       dependency-list
       (apply concat))
      (vec scripts))))

(deftest check-scripts-ordering
  (is (= (order-scripts [:angular :jquery :angular-sanitize :some-script :common])
         [:common :some-script :jquery :angular :angular-sanitize]))

  (is (= (order-scripts [:bootstrap-js])
         [:jquery :jquery-ui :bootstrap-js]))

  (is (= (order-scripts [:jquery-ui :bootstrap-js])
         [:jquery :jquery-ui :bootstrap-js]))

  (is (= (order-scripts [:base_skin :commons :structure])
         [:base_skin :commons :structure])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Libraries ordering
(def ^{:doc "Libraries order of inclusion"
       :private true}
  lib-order {:js [:external :lib :custom-lib :user :bono :admin :themes]
             :css [:external :lib :custom-lib :user :themes-external]})

(defn- order-libs[type libs]
  (sort-by (fn[[lib]]
             (let [index (.indexOf (type lib-order) lib)]
               (if (= -1 index) Long/MAX_VALUE index)))
           libs))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main processing function

(def common-includes {;; Javascript
                      :jquery "http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"
                      :jquery-ui "http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"
                      :angular "/js/lib/angular.js"
                      :angular-sanitize "http://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular-sanitize.js"
                      :bootstrap-js "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.js"
                      :bootstrap-css "http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
                      ;; Css
                      :ui-core "/css/lib/jquery.ui.core.css"})

(declare pack-scripts)

(defn process-js-and-css
  [type]
  (doall
   (->>
    @*required-scripts*
    distinct
    (filter (comp (partial = type) :type))
    (group-by :lib)
    (order-libs type)
    (mapcat (fn[[lib scripts]]
              (->>
               scripts
               (map :value)
               (pack-scripts type lib)
               (map (case type :js include-js :css include-css))))))))
