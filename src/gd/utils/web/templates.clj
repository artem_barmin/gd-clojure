(in-ns 'gd.utils.web)

(def ^{:doc "Regular expression that parses a CSS-style id and class from a tag name." :private true}
  re-tag #"([^\s\.#]+)(?:#([^\s\.#]+))?(?:\.([^\s#]+))?")

(defn normalize-element
  "Ensure a tag vector is of the form [tag-name attrs content]."
  [[tag & content]]
  (let [[_ tag id class] (re-matches re-tag (as-str tag))
        tag-attrs        (merge
                          (when id {:id id})
                          (when class {:class (.replace ^String class "." " ")})
                          {})
        map-attrs        (first content)]
    (if (map? map-attrs)
      [tag (merge tag-attrs map-attrs)]
      [tag tag-attrs])))

(defn- search-path[tree selector]
  (let [element
        (normalize-element (if (vector? selector) selector [selector]))

        node-type (first element)

        attrs (second element)]
    (loop [loc tree
           found nil
           result-path nil]
      (cond (z/end? loc) nil
            found result-path
            true (let [[current-node-type current-node-attrs]
                       (when (vector? (z/node loc))
                         (z/node loc))]
                   (recur (z/next loc)
                          (and current-node-attrs
                               (= (as-str current-node-type) node-type)
                               (= (select-keys current-node-attrs (keys attrs)) attrs))
                          loc))))))

(defn- restore-navigation-path[zipper-path selector]
  (loop [loc zipper-path
         res nil]
    (if (z/up loc)
      (recur (z/up loc)
             (cons (cons z/down (replicate (count (z/lefts loc)) z/right)) res))
      (let [resulted-path (reverse (reduce concat res))]
        (if (empty? resulted-path)
          (throwf "Element with selector : %s not found" selector)
          (apply comp resulted-path))))))

(defn- generate-transformer[tree selector]
  (let [path (search-path tree selector)
        search-down (restore-navigation-path path selector)
        search-up (apply comp (replicate (count (z/path path)) z/up))]
    (fn[tree content]
      (-> tree search-down (z/append-child content) search-up))))

(defn generate-multiple-transfomers[tree selectors]
  (let [transformers (doall (map (partial generate-transformer tree) selectors))]
    (fn[contents]
      (reduce (fn[tree [transformer content]]
                (transformer tree (html content)))
              tree
              (map vector transformers contents)))))

(defn create-template[filename]
  {:name filename
   :zipper (z/vector-zip (ts/parse-string (file-content* filename)))})

(defmacro process-template[template & actions]
  (let [actions-partioned (partition 2 actions)]
    `(let [transofrmer#
           (generate-multiple-transfomers (:zipper ~template) ~(vec (map first actions-partioned)))]
       (z/root (transofrmer# ~(vec (map second actions-partioned)))))))
