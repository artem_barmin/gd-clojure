(in-ns 'gd.utils.web)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils
(defn generate-js-function-name[name postfix]
  (symbol (strings/replace (str name postfix) #"-(.)" (comp strings/upper-case second))))

(defn generate-js-function-names[name & postfixes]
  (map (partial generate-js-function-name name) postfixes))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Singleton management

;; list of page parts that should be rendered once on the page(like templates for AngularJS)
(def ^:dynamic *singletons-list*)

(defn add-singleton[partial]
  (swap! *singletons-list* conj partial)
  nil)

(defn render-singletons[]
  (when (thread-bound? #'*singletons-list*)
    (seq @*singletons-list*)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Low level remotes interface

(def ^:dynamic *remote-context* nil)

(def remote-call-handlers (atom {}))

(def body-length-values (atom {}))

(defn- process-remote-request[]
  (let [{:keys [service] :as params} (:params (noir.request/ring-request))
        {:keys [handler]} (@remote-call-handlers service)
        result (apply handler [params])
        params-key (hash params)]
    (if (map? result)
      result
      (external-html (or (get @body-length-values params-key) 0)
                     (fn[len] (swap! body-length-values assoc params-key
                                     (Math/round (* 1.2 len))))
                     result))))

(defpage "/service" []
  (process-remote-request))

(defpage [:post "/service"] []
  (process-remote-request))

(defn defremote
  "Creates remote request handler. This macros hide details of new route
creation, arguments converting, function call and rendering result.

Important thing that you must realize : 'name' parameter should be unique for each
unique handler. Internally handlers are represented as map 'name' -> 'handler', so
they will be overriten in case of same names.

'argument-processors' - parameter that represents converters, sent by GET
request from client side. Each converter is simple function that takes string as
argument and returns value of desired type.
"
  [name handler argument-processors]
  (when (noir.options/dev-mode?)
    (when-let  [{:keys [handler processors]} (@remote-call-handlers name)]
      (when-not (= processors argument-processors)
        (throwf "Different argument processors passed: %s %s. Probably remote defined with different bodies."
                name (seq (take 2 (clojure.data/diff processors argument-processors)))))))
  (let [one-argument-processor
        (fn[res key]
          (assoc res key ((get argument-processors key) (res key))))

        all-arguments-proc
        (fn [params]
          (reduce one-argument-processor params (keys argument-processors)))

        handler-with-arguments-processing
        (fn [params]
          (handler (all-arguments-proc params)))]
    (swap! remote-call-handlers assoc name {:handler handler-with-arguments-processing
                                            :processors argument-processors}))
  name)

(defn int-converter[param]
  (when-not (or (nil? param) (= "null" param) (empty? param))
    (Integer/parseInt param)))

(defn keyword-converter[param]
  (when-not (or (nil? param) (= "null" param))
    (keyword param)))

(defn string-converter[param]
  (when-not (or (nil? param) (= "null" param))
    param))

(defn boolean-converter[param]
  (= "true" param))

(defn null-converter[param]
  nil)

;; utility function for generation client side code for remote calls
(defn defcall
  "Define '$.post' call for remote function, registered with 'defremote' before. Should be
used as callback from javascript side.

Generated function have two parameters : options(additional GET parameters), and success -
callback that will be called in case of success. For details see documentation of jQuery.get
method."
  [remote & [fixed-options]]
  (js* (callRemote (clj (merge fixed-options
                               {:service remote}
                               (when (and *remote-context* @*remote-context*)
                                 {:remote_context @*remote-context*}))))))

(defn deflink
  "Define simple text link for invoking service with given parameters."
  [remote & [fixed-options]]
  (str (url "/service" (merge fixed-options {:service remote}))))

(defn execute-form
  "validate-target - container of validation errors"
  [remote-name server-side-handler &
   {:keys [success fixed-options argument-processors
           validate validate-target
           error keep-data call-type]
    :or {call-type :call}}]
  (defremote remote-name server-side-handler argument-processors)
  (case call-type
    (:call :call*)
    (let [call (js* ((fn[that]
                       (var form (. ($ that) closest "form, .form"))
                       (var content (. form serializeObject))
                       (set! validateTarget
                             (clj (cond
                                    (= validate-target :form) (js* form)
                                    (string? validate-target) (js* (. form closest (clj validate-target)))
                                    true nil)))

                       (var remoteRequest (fn[]
                                            ;; validation is passed - execute remote request
                                            ((clj (defcall remote-name fixed-options)) content
                                             (fn[result]
                                               (clj success)
                                               (clj
                                                (when-not keep-data
                                                  (js* (cleanForm form)))))
                                             (fn[result] (clj error)))))

                       ;; validation - client side validation rules should return list of promises,
                       ;; we are waiting them to complete, and if everything is fine - execute
                       ;; remote call, if some validator failed - you should bound callback to promise
                       (clj
                        (if validate
                          (js* (.then ($.when.apply $ (clj validate)) remoteRequest))
                          (js* (remoteRequest)))))
                     this))]
      (case call-type
        :call (cljs call)
        :call* call))
    :link
    (deflink remote-name fixed-options)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; High level form interface
(defn params-to-converters[params]
  (->> params
       (mreduce (fn[[k v]]
                  {k (if (nil? v)
                       null-converter
                       (condp instance? v
                         java.lang.Number int-converter
                         java.lang.String string-converter
                         clojure.lang.Keyword keyword-converter
                         java.lang.Boolean boolean-converter
                         java.lang.Object (throwf "Unsupported converter for variable %s of type %s" v (type v))))}))
       (keywordize-keys)))

(defn params-to-map[params]
  (stringify-keys (reduce merge (map (fn[k]{(keyword (name k)) k}) params))))

(defn params-to-fixed-values[params]
  (stringify-keys (reduce merge (map (fn[k]{(keyword (name k)) `(if (keyword? ~k) (name ~k) ~k)}) params))))

(defmacro execute-form*[action-name action-params body & {:keys [success fixed-options argument-processors validate error call-type] :as params}]
  (let [closure-context
        (atom nil)

        processed-body
        (postwalk #(if (and (symbol? %) (= \* (first (name %))))
                                    (let [new-name (symbol (.substring (name %) 1))]
                                      (swap! closure-context conj new-name)
                                      new-name)
                                    %) body)

        fixed-params-capture
        (params-to-map @closure-context)

        fixed-params-values
        (params-to-fixed-values @closure-context)

        all-params
        (concat @closure-context action-params)]
    `(execute-form ~(str action-name)
                   (fn[{:keys [~@(distinct all-params)] :as ~'param-map}]
                     (do
                       (doseq [validator# (server-side ~validate)]
                         (validator#))
                       ~processed-body))
                   :fixed-options ~fixed-params-values
                   :argument-processors (params-to-converters ~fixed-params-capture)
                   :validate (client-side ~validate)
                   ~@(mapcat identity (dissoc params :validate)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; High level remote interface
(comment
  "Example of usage"

  (remote* get-more-logs
           (let [end (j* (getEndRange))]
             (json-str (logger:get-all-messages
                        (round-hours (-> (- (j* (getStartRange)) 1) hours ago))
                        (if (= end 0) (now) (-> (- end 1) hours ago)))))
           :success (j* (onLogsLoaded)))

  (j* (javascript-expression)) "Will be evaluated on the client side, and pass
  results to server side automatically. Type coerction is automatic(number and
  strings are supported)"

  "Evaluates to following form"

  (defremote "get-more-logs"
    (fn[{:keys [start end]}]
      (json-str (logger:get-all-messages (round-hours (-> (- start 1) hours ago)) (if (= end 0) (now) (-> (- end 1) hours ago)))))
    {:start int-converter :end int-converter})

  [:div.left (small-blue-button "Запрос" :action (js ((clj (defcall "get-more-logs")) {:start (getStartRange) :end (getEndRange)} onLogsLoaded)))])

(defn auto-converter[{:keys [value __type] :as whole}]
  (cond (and value __type)
        (case __type
          "boolean" (if (= value "true") true false)
          "number" (int-converter value)
          "keyword" (keyword value)
          "string" value)
        (= __type "undefined") nil
        (= whole "null") nil
        true whole))

(def server-params #{'s* 'gd.utils.web/s*})

(def client-params #{'c* 'gd.utils.web/c*})

(defn- unqualify-client-code[code]
  (postwalk (fn [form]
              (if (symbol? form)
                (let[sym (symbol (.replaceAll (str form) ".*/" ""))]
                  (if (= sym 'clj)
                    (throwf "Forbid to use clj inside remote callback parameters. Use s* instead.")
                    sym))
                form))
            code))

(defn- walk-remote-body[form callback]
  (postwalk
   (fn[form]
     (if (seq? form)
       (let [[head] form]
         (cond (server-params head) (callback :server form)
               (client-params head) (callback :client
                                              (unqualify-client-code form))
               true form))
       form))
   (macroexpand-all form)))

(defn- extract-params
  "Process body and extract all marked params (s*) and (c*), also return modified body"
  [body]
  (let [clojure-expressions (atom nil)
        js-expressions (atom nil)

        generate-name
        (fn[form]
          (if (symbol? form)
            form
            (symbol (str "sym" (hash form)))))

        processed-body
        (walk-remote-body
         body (fn[type [_ expression-body]]
                (let [name (generate-name expression-body)]
                  (swap! (case type
                           :server clojure-expressions
                           :client js-expressions)
                         assoc name expression-body)
                  name)))

        param-names
        (concat (keys @js-expressions) (keys @clojure-expressions))

        stringify-keys
        (fn [m]
          (let [f (fn [[k v]] (if (symbol? k) [(str k) v] [k v]))]
            (postwalk (fn [x] (if (map? x) (into {} (map f x)) x)) m)))

        params
        (stringify-keys
         (merge
          (if-let [expr (deref clojure-expressions)]
            (fmap (fn[v]`(clj (js* (~'autoConverter
                                    (clj (cond
                                           (keyword? ~v) (name ~v)
                                           true ~v))
                                    (clj (cond
                                           (keyword? ~v) "keyword")))))) expr))
          (if-let [expr (deref js-expressions)]
            (fmap (fn[v](js* (autoConverter (clj v)))) expr))))]
    [param-names processed-body params]))

(def ^:dynamic *in-remote* false)

;; TODO : don't return jsoned result if 'success' callback is not set(to prevent (do (...) nil) pattern)

(defn auto-converter-for-all-params[param-names]
  (zipmap param-names (replicate (count param-names) auto-converter)))

(defn remote-result[result type]
  (case type
    :json (resp/content-type "application/json; charset=utf-8"
                             (json-str result :escape-unicode false))
    :html result))

(defn remote-name[action-name]
  (if (symbol? action-name)
    (str action-name)
    action-name))

(defmacro remote*[action-name body & {:keys [success result error fixed-options] :or {result :json}}]
  (let [action-name (remote-name action-name)
        [param-names processed-body params] (extract-params body)]
    `(do (defremote ~action-name
           (fn[{:keys [~@param-names] :as ~'param-map}]
             (binding [*in-remote* ~action-name]
               (remote-result ~processed-body ~result)))
           (auto-converter-for-all-params ~(vec (map (comp keyword str) param-names))))
         (js* ((clj (defcall ~action-name ~fixed-options)) ~params ~success ~error)))))

(defmacro remote-body-basic*[body & {:keys [binding]}]
  (walk-remote-body
   body (fn[type form]
          (let [[head expression-body] form]
            (case type
              :server expression-body
              :client (if (symbol? expression-body)
                        (get binding (keyword (str expression-body)))
                        nil))))))

(defmacro callback[params body]
  `(js* (fn[~@params ~'success]
          (return (clj ~body)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Anonymous remote handlers
(comment
  (prn (.getClass (fn[]1)))
  "This expression will return unique name of anonymous function, that can be
  used later for generation of remote name. This name will be guaranted unique
  and will change only on compilation. Also, once generated, it will not change
  during runtime." )

(defmacro remote-anonymous[]
  `(.getName (.getClass (fn[]))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Helpers for remotes composing
(defn s*[& more]
  nil)

(defn c*[& more]
  nil)

(comment
  "Remote composition. If you want to use common body definition for different
remotes. You must create macros:"

  (defmacro test[a]
    `(+ (s* ~a) (c* (formState "#a")) 1))

  "and then just use it"

  (println (cljs
            (let [a 1]
              (remote* test124 (test 1))))))
