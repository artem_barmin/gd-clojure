(ns gd.utils.web
  (:use noir.core
        clojure.test
        gd.utils.test
        hiccup.core
        gd.utils.graph
        hiccup.page
        hiccup.element
        hiccup.core
        [clojure.algo.generic.functor :only (fmap)]
        [noir.options :only (dev-mode?)]
        [hiccup.util :only (url escape-html)]
        gd.utils.common
        gd.utils.db
        hiccup.element
        clojure.data.json
        clj-time.core
        digest
        com.reasonr.scriptjure
        noir.validation
        clojure.walk)
  (:import (java.io PrintWriter))
  (:use [clojure.java.shell :only (sh)])
  (:refer-clojure :exclude [extend])
  (:require
   clojure.data
   ring.middleware.nested-params
   [pl.danieljanus.tagsoup :as ts]
   [noir.options :only (dev-mode?)]
   [clojure.string :as strings]
   [clojure.java.io :as io]
   [noir.request]
   [noir.server :as server]
   [noir.response :as resp]
   [hiccup.compiler]
   [noir.session :as session]
   [clojure.zip :as z]
   [clojure.set :as sets]
   [ring.middleware.session.store :as ringstore]
   [monger.collection :as mc])
  (:import (org.mozilla.javascript Context ScriptableObject)
           java.net.URL
           ro.isdc.wro.extensions.processor.js.UglifyJsProcessor
           ro.isdc.wro.model.resource.Resource
           ro.isdc.wro.model.resource.ResourceType
           (net.htmlparser.jericho Renderer Segment Source)
           [java.util UUID Date]))

(defmacro external-html
  "Render Clojure data structures to a string of HTML."
  [length length-fn & content]
  (apply hiccup.compiler/compile-external-html length length-fn content))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Helpers for requiring scripts to be included in <head>(can be of type :js or
;; :css), also have some common scripts like jquery and jquery-ui
(def ^{:dynamic true} *required-scripts* (atom nil))

(defn require-js[lib name]
  (swap! *required-scripts* conj {:type :js :lib lib :value name})
  nil)

(defn require-css[lib name]
  (swap! *required-scripts* conj {:type :css :lib lib :value name})
  nil)

(defn wrap-script-require-processor
  [handler]
  (fn [request]
    (binding [*required-scripts* (atom nil)]
      (handler request))))

(server/add-middleware wrap-script-require-processor)

(comment "TODO : create checker for execute-form and remote names. If they are
have same name from different source lines - then the problem is occured. This
can be event builtin into runtime.")

(load "web/validation")
(load "web/remotes")
(load "web/scripts")
(load "web/packer")
(load "web/templates")

;; utils
(defn normalize-url[url]
  "Removes traling slash at the end : /deals/ -> /deals"
  (strings/replace url #"/$" ""))

(defn custom-concat
  "For usage with 'js' macros. Because standard 'concat' function, makes some
changes of the symbols, and as result jq macros is not working after"
  [coll1 coll2]
  (if (nil? coll1)
    coll2
    (cons (first coll1) (custom-concat (next coll1) coll2))))

(declare session-map)

(defn clear-sessions[]
  (reset! session-map {})
  (mc/drop "web_sessions"))

(defn persistent-redirect[url]
  {:status 301
   :headers {"Location" (noir.options/resolve-url url)}
   :body ""})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Remove all service url parts(page[0-9]+ for pager) and set environement
;; variables in request. All defpage that based on destructing will not see
;; service url parts.

(def ^:dynamic *component-info*)

(defn-  parse-uri-into-components[{:keys [uri] :as request}]
  {:page (int-converter (second (re-find #"/page([0-9]+)" uri)))
   :limit (int-converter (second (re-find #"/page[0-9]+x?([0-9]*)" uri)))
   :stock-modal (int-converter (second (re-find #"/stock([0-9]+)[^/]*$" uri)))})

(defn- clear-url[url]
  (when url
    (-> url
        (.replaceAll "(?<=/)page[0-9]+x?[0-9]*" "")
        (.replaceAll "(?<=/)stock[0-9]+[^/]*$" ""))))

(defn- process-request-uris[{:keys [uri headers] :as request}]
  (->
   request
   (assoc-in [:referer] (get headers "referer"))
   (assoc-in [:real-uri] uri)
   (update-in [:original-uri] clear-url)
   (update-in [:uri] clear-url)))

(defn wrap-url-service-clear
  [handler]
  (fn [request]
    (binding [*component-info* (parse-uri-into-components request)]
      (handler (process-request-uris request)))))

(server/add-middleware wrap-url-service-clear)

(defn current-url[]
  (let [{:keys [original-uri uri referer]} (noir.request/ring-request)]
    (if (= uri "/service")
      (clear-url (.getPath (new URL referer)))
      original-uri)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Global remote context middleware

(defn wrap-global-remote-context
  [handler]
  (fn [{:keys [params] :as request}]
    (binding [*remote-context* (atom (:remote_context params))]
      (handler request))))

(server/add-middleware wrap-global-remote-context)

(defn add-var-to-global-context[var value]
  (assert (keyword? var))
  (swap! *remote-context* assoc var value)
  nil)

(defn get-var-from-global-context[var]
  (get @*remote-context* var))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Middleware for collecting SEO stuff(title, meta info)

(def ^:dynamic *seo-parts* nil)

(defn wrap-seo
  [handler]
  (fn [request]
    (binding [*seo-parts* (atom nil)]
      (let [user-agent (get-in request [:headers "user-agent"])]
        (handler (assoc request :bot-request
                        (if user-agent
                          (boolean (re-find #"(?iu)(google|yandex|up|bing).*bot" user-agent))
                          "NO AGENT")))))))

(server/add-middleware wrap-seo)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Helpers for escaping all input strings to prevent XSS atack
(defn escape-xss
  "Firstly unescape all text, that arrived from external world to make it
  uniform(and to prevent double escaping), and after that escape all HTML tags."
  [request]
  (let [escape #(rmap (comp escape-html unescape-html) %)]
    (-> request
        (update-in [:query-params] escape)
        (update-in [:params] escape)
        (update-in [:form-params] escape))))

(defn wrap-xss-prevention
  [handler]
  (fn [request]
    (if (some #(.startsWith (:uri request) %) ["/js/" "/css/" "/fonts/" "/img/" "/utils/" ])
      (handler request)
      (handler (escape-xss request)))))

(server/add-middleware wrap-xss-prevention)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; utility jquery code
(defn document-ready[code]
  (javascript-tag (js
                   (. ($ document) ready
                      (fn []
                        (clj code))))))

(defn init-jquery-plugin[selector plugin & params]
  (document-ready (custom-concat (list '. (list '$ selector) (symbol plugin)) params)))

(defn reload[]
  (js* (. location reload)))

(defjsmacro set-html[selectors source]
  (. ($ (clj (apply str selectors))) html (clj source)))

(defjsmacro get-child[child]
  (. ($ this) find (clj (apply str child))))

(defjsmacro get-section[parent child]
  (. (. ($ this) closest (clj (apply str parent))) find (clj (apply str child))))

(defjsmacro get-closest[selector]
  (. (. ($ this) parents) find (clj selector)))

(defjsmacro get-section-el[el parent child]
  (. (. ($ (clj el)) closest (clj (apply str parent))) find (clj (apply str child))))

(defjsmacro append-element-section[parent-selector selectors source]
  (. (get-section (clj parent-selector) (clj selectors)) append (clj source)))

(defjsmacro append-element[selectors source]
  (. ($ (clj (apply str selectors))) append (clj source)))

(defjsmacro on[selectors action body]
  (. ($ (clj (apply str selectors))) (clj action)
     (fn[]
       (var value (. ($ this) val))
       (clj body))))

(defjsmacro in-dialog[selectors]
  ($dialog (clj (apply str selectors))))

(defjsmacro get-val[element]
  (. ($ (clj element)) val))

(defjsmacro by-name[n]
  ($ (clj (str "[name*='" (name n) "']"))))

(defjsmacro maybe-seq-body[variable area-variable body]
  (var maybeArrayVariable (clj variable))
  (var maybeArrayArea (clj area-variable))
  (if (instanceof maybeArrayVariable Array)
    (doseq (arrayIndex maybeArrayVariable)
      (set! (clj variable) (aget maybeArrayVariable arrayIndex))
      (set! (clj area-variable) (aget maybeArrayArea arrayIndex))
      (clj body))
    (clj body)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; JS switch case

(alter-var-root #'special-forms (partial clojure.set/union #{'switch}))

(defmethod emit-special 'switch [type [switch variable forms]]
  (str "switch (" (emit variable) ") { \n"
       (apply str (for [[value form] (partition-all 2 forms)]
                    (if (= value :default)
                      (str "default:" (emit form) statement-separator "break;")
                      (str "case " (emit value) ":" (emit form) statement-separator "break;"))))
       "\n }"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; JS splice macros
(defn splice[forms]
  (custom-concat (list 'do) forms))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extension to scriptjure - add 'g' to regexp
(defmethod emit java.util.regex.Pattern [expr]
  (str \/ expr \/ "g"))

(alter-var-root #'com.reasonr.scriptjure/statement-separator (constantly ";"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extension to scriptjure - convert map keys to string
(defmethod emit clojure.lang.IPersistentMap [expr]
  (letfn [(json-pair [pair] (str (emit (as-str (key pair))) ": " (emit (val pair))))]
    (str "{" (strings/join ", " (map json-pair (seq expr))) "}")))

(defmethod emit java.util.Date [expr]
  (str "\"" expr "\""))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Counting modifier
(defmacro get-count[body]
  `(binding [gd.utils.db/*count-query* true]
     (:count (first ~body))))

(defmacro with-count[body]
  `(let [count# (get-count ~body)
         result# ~body]
     {:count count# :result result#}))

(defmacro modify-result[body modify-fn]
  `(if gd.utils.db/*count-query*
     ~body
     (~modify-fn ~body)))

(alter-var-root #'ring.middleware.nested-params/parse-nested-keys
                (constantly
                 (fn[param-name]
                   (map #(.replaceAll % "\\]" "") (.split param-name "[\\[]+")))))

(defn- monotonic-seq?[a]
  (every? (partial = 1) (map - (rest a) a)))

(defn- assoc-nested-process-arrays[input-map]
  (if (empty? input-map)
    {}
    (postwalk (fn[v]
                (if (and (map? v)
                         (every? (comp (partial re-find #"^[0-9]+$") str) (keys v))
                         (let [[fst :as integer-keys] (sort (map parse-int (keys v)))]
                           (and (zero? fst) (monotonic-seq? integer-keys))))
                  (vec (map second (sort-by first v)))
                  v))
              input-map)))

(defonce fix-arrays (alter-var-root #'ring.middleware.nested-params/nest-params
                                    (fn[nest-params]
                                      (fn[& params]
                                        (assoc-nested-process-arrays (apply nest-params params))))))

(deftest parse-nested-keys-spec
  (testing "[] - should return empty string as part of key"
    (is (= (ring.middleware.nested-params/parse-nested-keys "foo[bar][][baz]")
           (list "foo" "bar" "" "baz")))

    (is (= (ring.middleware.nested-params/parse-nested-keys "foo[bar][][]")
           (list "foo" "bar" "" "")))))

(run-tests)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Composite session key(works with mongo storage)
(defn composite-key[& args]
  (keyword (apply str args)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Noir routes improvement(store all routes in the specific map, so we should
;; not require files for use url-for)

(defonce named-routes (atom nil))

(alter-var-root #'noir.core/defpage (fn[_]
                                      (fn[& args]
                                        (let [fn-simple-name (nth args 2)
                                              {:keys [fn-name action url destruct body]} (parse-args (drop 2 args))]
                                          `(do
                                             (defn ~fn-name {:noir.core/url ~url
                                                             :noir.core/action (quote ~action)
                                                             :noir.core/args (quote ~destruct)} [~destruct]
                                                             ~@body)
                                             (if (symbol? (quote ~fn-simple-name))
                                               (swap! named-routes assoc
                                                      (quote ~fn-simple-name)
                                                      (ns-resolve *ns* (quote ~fn-name))))
                                             (swap! route-funcs assoc ~(keyword fn-name) ~fn-name)
                                             (swap! noir-routes assoc ~(keyword fn-name) (~action ~url {params# :params} (~fn-name params#))))))))

(alter-var-root #'noir.core/url-for (fn[_]
                                      (fn[_ _ fn-name & [arg-map]]
                                        `(url-for-fn* (get @named-routes (quote ~fn-name)) ~arg-map))))


(alter-var-root #'compojure.core/prepare-route
                (fn[_]
                  (fn[route]
                    (cond
                      (string? route)
                      `(clout.core/route-compile ~route)
                      (vector? route)
                      `(clout.core/route-compile
                        ~(first route)
                        ~(apply hash-map (rest route)))
                      :else
                      `(if (string? ~route)
                         (clout.core/route-compile ~route)
                         (clout.core/route-compile
                          (first ~route)
                          (apply hash-map (rest ~route))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extend JSON
(defn- write-json-plain [x ^PrintWriter out escape-unicode?]
  (.print out (json-str (str x))))

(clojure.core/extend java.lang.Class Write-JSON
                     {:write-json write-json-plain})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils for microformats

(def ^:dynamic *microformat* nil)

(defn micro[& {:keys [] :as attrs}]
  (when *microformat*
    attrs))

(defmacro with-micro[& content]
  `(binding [*microformat* true]
     (html
      ~@content)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Improvement for persistent session store. Multiple concurent requests to the
;; same session share same atom. So app-side changes are shared and each save of
;; session will be consistent. Also we must remove memory atoms from map when
;; we want to update user session from outside(we MUST take all info from mongo storage).

(comment "Original problem : when two concurent requests share session and first request write to it, second
can overwrite it, because of ring session handling - changes are writen in the end of request.")

(defonce session-mutex (Object.))

(defonce session-map (atom {}))

(defn- process-concurent-sessions[handler request]
  (let [session-id
        (:_id (:session request))

        build-new-session-object
        (fn[] (atom (:session request)))

        build-and-register-new-session
        (fn[]
          ((swap! session-map assoc session-id (build-new-session-object))
           session-id))]

    ;; check if concurent requests from the same user
    ;; exists. If so - return already allocated session to
    ;; new request, so all changes are shared across
    ;; requests.
    (binding [noir.session/*noir-session*
              (locking session-mutex
                (if (nil? session-id)
                  (atom {})
                  (or
                   (@session-map session-id)
                   (build-and-register-new-session))))]

      (noir.session/remove! :_flash)

      (when-let [resp (handler request)]
        (assoc resp :session
               (merge @noir.session/*noir-session* (:session resp)))))))

(alter-var-root #'noir.session/noir-session
                (fn[_]
                  (fn[handler]
                    (fn [request]
                      (process-concurent-sessions handler request)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Session storage

(def ^{:const true} default-session-store-collection "web_sessions")

(defrecord ClojureReaderBasedMongoDBSessionStoreExtended [^String collection-name])

(extend-protocol ringstore/SessionStore
  ClojureReaderBasedMongoDBSessionStoreExtended

  (read-session [store key]
    (or
     (when-let [session (get @session-map key)] @session)
     (when-let [m (and key (:value (mc/find-one-as-map (.collection-name store) {:_id key})))]
       (if (.startsWith m "#=")
         (read-string m)
         (deserialize-base64 m)))
     {}))

  (write-session [store key data]
    (let [date  (Date.)
          key   (or key (str (UUID/randomUUID)))
          value (serialize-base64 (assoc data :_id key))]
      (mc/save (.collection-name store) {:_id key :value value :date date})
      key))

  (delete-session [store key]
    (mc/remove-by-id (.collection-name store) key)
    nil))

(defn session-store-extended
  ([]
     (ClojureReaderBasedMongoDBSessionStoreExtended. default-session-store-collection))
  ([^String s]
     (ClojureReaderBasedMongoDBSessionStoreExtended. s)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Generate ID

(def client-id-generator (atom 0))

(defn gen-client-id[]
  (str "id" (swap! client-id-generator inc)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Render HTML to text

(defn render-html[html]
  (let [renderer
        (Renderer. (net.htmlparser.jericho.Segment.
                    (net.htmlparser.jericho.Source. html) 0 (count html)))]
    (.setNewLine renderer "\n")
    (->
     renderer
     .toString
     (clojure.string/replace #"(?m)[\u00a0 \t]+" " ")
     (clojure.string/replace #"(?m)[\n]{2,}" "\n\n"))))

(defn render-html-lines[html]
  (seq (.split (render-html html) "\n")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Precompile html
(defmacro precompile-html
  [& content]
  `(delay ~@(hiccup.compiler/optimize-sequential-outs
             (hiccup.compiler/compile-seq content))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Request scope cache

(defn wrap-request-scope-cache
  [handler]
  (fn [request]
    (binding [*request-cache* (atom nil)]
      (let [res (handler request)]
        (check-notifications)
        res))))

(server/add-middleware wrap-request-scope-cache)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Singleton list

(defn wrap-singleton-list
  [handler]
  (fn [request]
    (binding [*singletons-list* (atom #{})]
      (handler request))))

(server/add-middleware wrap-singleton-list)

(defmacro wrap-security
  "Wrap actions into security testing with let user."
  [user & body]
  `(binding [session/*noir-session* (atom {:user (gd.model.model/user:get-full ~user)})]
     ~@body))
