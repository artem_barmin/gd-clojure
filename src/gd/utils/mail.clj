(ns gd.utils.mail
  (:use postal.core
        gd.utils.robust-pool
        gd.log
        gd.model.context
        gd.utils.common
        [robert.bruce :only [try-try-again]]
        ring.util.codec)
  (:require [noir.options :as options])
  (:import (java.util.concurrent Executors)))

(defonce send-mail-thread-poll (create-pool "email-sending" 3))

(defn encode-from[name address]
  (str "=?UTF-8?B?" (base64-encode (.getBytes name)) "=?= <" address ">"))

(def amazon {:host "email-smtp.us-east-1.amazonaws.com"
             :user "AKIAIASBSW5M665OXDMQ"
             :pass "ArzRGFy/eJ63h5r4LKQxJxhcipCGTbO2DkuwxXDfMlV3"
             :port 465
             :ssl :yes})

(defn- really-send-mail[addresses header body]
  (let [{:keys [name address]} (:mail *supplier-info*)]
    (try-try-again
     :sleep 1000
     :tries 5
     :catch [java.net.SocketException]
     (fn[]
       (send-message
        amazon
        {:from (encode-from (or name "Дайте две")
                            (or address "admin@dayte-dve.com.ua"))
         :to (maybe-seq addresses)
         :subject header
         :body [{:type "text/html; charset=utf-8"
                 :content body}]})))))

(defn send-mail[addresses header body]
  (info "Sent email " header addresses)
  (if-not (options/dev-mode?)
    (in-thread-pool send-mail-thread-poll
                    (fn[]
                      (really-send-mail addresses header body)
                      (info "Really sent email" header addresses)))))
