(ns gd.utils.bbcode
  (:use hiccup.core))

(defn process-in-variables[s]
  (-> s
      (.replaceAll "var" "<var inherit=\"true\"/>")
      (.replaceAll "\\$\\{(.*?):escape\\}" "<var name=\"$1\" scope=\"escapeXml\" inherit=\"true\"/>")
      (.replaceAll "\\$\\{\\{(.*?)=(.*?)\\}\\}" "<var name=\"$1\" regexp=\"$2\" inherit=\"true\"/>")
      (.replaceAll "\\$\\{([^}=]*)=(.*?)\\}" "<var name=\"$1\" regexp=\"$2\" inherit=\"true\"/>")
      (.replaceAll "\\$\\{(.*?)\\}" "<var name=\"$1\" inherit=\"true\"/>")))

(defn process-out-variables[s]
  (-> s
      (.replaceAll "var" "<var/>")
      (.replaceAll "\\$\\{(.*?)\\}" "<var name=\"$1\"/>")))

(defn code[name pattern template]
  [:code {:name name}
   [:pattern (process-in-variables pattern)]
   [:template (process-out-variables (h (html template)))]])

(defn code-simple[name template]
  (code name (format "[%s]var[/%s]" name name) template))

(defn generate-bbcode-config[]
  (html [:configuration
         {"xmlns:xsi" "http://www.w3.org/2001/XMLSchema-instance"
          "xmlns" "http://kefir-bb.sourceforge.net/schemas"
          "xsi:schemaLocation" "http://kefir-bb.sourceforge.net/schemas http://kefir-bb.sourceforge.net/schemas/kefir-bb-0.5.xsd"}
         [:scope {:name "ROOT"}
          (code-simple "b" [:b "var"])
          (code "newline" "[br]var[/br]" "<br>")
          (code-simple "i" [:i "var"])
          (code-simple "list" [:ul {:style "margin-left:30px;"} "var"])
          (code "ordered-list" "[list=1]var[/list]" [:ol {:style "margin-left:30px;"} "var"])
          (code-simple "u" [:u "var"])
          (code-simple "s" [:s "var"])
          (code-simple "p" [:p "var"])
          (code-simple "*" [:li "var"])
          (code-simple "h1" [:h1 "var"])
          (code "h1=align" "[h1=${align}]var[/h1]" [:h1 {:align "${align}"} "var"])
          (code-simple "h2" [:h2 "var"])
          (code "h2=align" "[h2=${align}]var[/h2]" [:h2 {:align "${align}"} "var"])
          (code-simple "h3" [:h3 "var"])
          (code "h3=align" "[h3=${align}]var[/h3]" [:h3 {:align "${align}"} "var"])
          (code-simple "center" [:p {:align "center"} "var"])
          (code "align" "[align=${align}]var[/align]"
                [:p {:align "${align}"} "var"])
          (code "size" "[size=${size}]var[/size]"
                [:font {:size "${size}"} "var"])
          (code "frame-with-size" "[iframe width=${width} height=${height}]${url}[/iframe]"
                [:iframe {:width "${width}" :height "${height}" :src "${url}"}])

          (code "div-style-class" "[div style=${style} class=${class}]${content}[/div]"
                "<div style=${style} class=${class}>${content}</div>")

          (code "div-style" "[div style=${style}]${content}[/div]"
                "<div style=${style}>${content}</div>")

          (code "span" "[span]${content}[/span]"
                "<span>${content}</span>")

          (code "internal-image" "[img-internal]${{img=^[a-z0-9]{32}.jpg$}}[/img-internal]"
                [:img {:src "/img/uploaded/w400/h/${img}"}])

          (code "img-with-size" "[img width=${width} height=${height}]${url}[/img]"
                [:img {:width "${width}" :height "${height}" :src "[[__images-parent-host__]]/img/uploaded/original/${url}.jpg"}])

          (code "smiley" "[img]${img=^[0-9]+$}[/img]"
                [:img {:class "smile$img simle-icon"
                       :src "/img/px.png"
                       :style "background-position:${img}px 0;width:24px;height:24px;background-image:url('/img/cleditor/smiles.png');float:none;display:inline-block;margin:0;position:relative;top:6px;"}])

          (code "color" "[color=${color}]${text}[/color]"
                [:font {:color "${color}"} "${text}"])
          (code "link" "[url=${url:escape}]${text}[/url]"
                "<a href=\"${url}\" target=\"_blank\">${text}</a>")
          (map (fn[sym]
                 [:code
                  [:pattern sym]
                  [:template (h sym)]])
               ["&amp;" "&apos;" "&lt;" "&gt;" "&quot;"])]]))
