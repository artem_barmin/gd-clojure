(ns gd.utils.robust-pool
  (:use gd.log)
  (:import (java.util.concurrent Executors LinkedBlockingQueue)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Global storage for all pools(used by watch dog)

(defonce all-robust-pools (atom {}))
(defonce next-pool-id (atom 0))
(defonce next-thread-id (atom 0))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Custom thread pool with watchdog

(comment "Custom thread pool implemetation. Have watch dog that check if some
threads are blocked on IO(like mail sending can hangs on connection
establishing). If such situation detected - restart blocked threads.

Checking algorithm : each thread have associated counter of completed tasks. If
for a long time we have non empty queue, and counters for each thread remains
the same - we assume that thread is hanged.

When threads are restarted - currently active tasks are droped, so we will loose
at most N tasks, where N - is number of threads. This property can be usefull for
dropping infinite tasks from queue.")

(defn- create-thread[name counter queue]
  (let [thread (new Thread
                    (fn[] (while true
                            (try
                              ((.take queue))
                              (finally
                                (swap! counter inc))))))]
    (.setName thread (str name "-" (swap! next-thread-id inc)))
    thread))

(defn- init-threads[{:keys [threads queue name] :as pool} num-threads]
  (dotimes [i num-threads]
    (let [counter (atom 0)
          thread (create-thread name counter queue)]
      (swap! threads assoc i {:counter counter :thread thread})
      (.start thread))))

(defn- reinit-threads[{:keys [threads queue name] :as pool}]
  (doseq [[num {:keys [counter thread]}] @threads]

    ;; try to stop previous thread - if possible(for some blocking IO thread can't be interrupted and blocked,
    ;; so it will be alive until JVM stops)
    (.interrupt thread)
    (.stop thread)

    ;; create new thread to make usefull work, it will use the same counter
    (let [new-thread (create-thread name counter queue)]
      (swap! threads assoc num {:counter counter :thread new-thread})
      (.start new-thread))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Public API

(defn create-pool[name num-threads]
  (let [threads (atom {})
        queue (LinkedBlockingQueue.)
        pool {:threads threads :queue queue :name name}]
    (init-threads pool num-threads)
    (swap! all-robust-pools assoc (swap! next-pool-id inc) pool)
    pool))

(defn submit[pool task]
  (.put (:queue pool) task))

(defn robust-pool?[pool]
  (and (map? pool) (:queue pool) (:threads pool)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Watch dog

(defn- compute-global-state[]
  (let [compute-thread-state
        (fn[[num {:keys [threads queue] :as pool}]]
          {num {:queue (.size queue)
                :pool pool
                :completed (map (comp deref :counter second) (sort-by first @threads))}})]
    (reduce merge (map compute-thread-state @all-robust-pools))))

(defn- check-state[old-state new-state]
  (let [{old-queue :queue old-completed :completed} old-state
        {new-queue :queue new-completed :completed pool :pool} new-state]
    (when (and old-queue new-queue
               (not= old-queue 0)
               (not= new-queue 0)
               (seq old-completed)
               (seq new-completed)
               (>= new-queue old-queue)
               (= old-completed new-completed))
      (warn "Pool hangs - restart it"
            (:name (:pool new-state))
            (select-keys old-state [:queue :completed])
            (select-keys new-state [:queue :completed]))
      (reinit-threads pool))))

;; check state of thread pools each 5 minutes(large enought than any timeouts)
(defonce watch-dog-thread
  (.start
   (new Thread
        (fn[]
          (let [old-state (atom nil)]
            (while true
              (let [new-state (compute-global-state)]
                (doseq [[id new-state] new-state]
                  (check-state (get @old-state id) new-state))
                (reset! old-state new-state)
                (Thread/sleep (* 1000 60 5)))))))))
