(ns gd.utils.json-path
  (:use gd.utils.test)
  (:use clojure.test))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Parser
(declare parse)

(defn extract-sub-tree [start end stream]
  (take-while #(not (= end %)) (drop-while #(= start %) stream)))

(defn parse-expr [remaining]
  (let [ops {"=" :eq, "!=" :neq, "<" :lt, "<=" :lt-eq, ">" :gt, ">=" :gt-eq}
        supported-ops (set (keys ops))
        lhs (take-while #(not (supported-ops %)) remaining)
        op (first (drop-while #(not (supported-ops %)) remaining))
        rhs (rest (drop-while #(not (supported-ops %)) remaining))]
    [(ops op) (parse lhs) (parse rhs)]))

(defn parse-indexer [remaining]
  (let [next (first remaining)]
    (cond
      (= next "*") [:index "*"]
      (= "?(" next) [:filter (parse-expr (extract-sub-tree "?(" ")" (drop 1 remaining)))]
      :else [:index next])))

(defn parse-path-components [parts]
  (let [converter (fn [part]
                    (let [path-cmds {"$" :root, "." :child, ".." :all-children, "@" :current}]
                      (if (path-cmds part)
                        [(path-cmds part)]
                        [:key part])))]
    (vec (map converter parts))))

(defn parse [remaining]
  (let [next (first remaining)]
    (cond
      (empty? remaining) []
      (= "\"" next) [:val (apply str (extract-sub-tree "\"" "\"" remaining))]
      (= "[" next) (do
                     (let [idx (parse-indexer (extract-sub-tree "[" "]" remaining))
                           rem (drop 1 (drop-while #(not (= "]" %)) remaining))]
                       (if (not (empty? rem))
                         [:selector idx (parse rem)]
                         [:selector idx])))
      :else (do
              (let [pth (parse-path-components (extract-sub-tree "" "[" remaining))
                    rem (drop-while #(not (= "[" %)) remaining)]
                (if (not (empty? rem))
                  [:path pth (parse rem)]
                  [:path pth]))))))

(defn parse-path [path]
  (parse (re-seq #"<=|>=|\.\.|[.*$@\[\]\(\)\"=<>]|\d+|[\w-]+|\?\(|!=" path)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Walker
(declare walk eval-expr)

(defn eval-eq-expr [op-form context operands]
  (apply op-form (map #(eval-expr % context) operands)))

(defn eval-expr [[expr-type & operands :as expr] context]
  (let [ops {:eq =, :neq not=, :lt <, :lt-eq <=, :gt >, :gt-eq >=}]
    (cond
      (contains? ops expr-type) (eval-eq-expr (expr-type ops) context operands)
      (= expr-type :val) (first operands)
      (= expr-type :path) (walk expr context))))

(defn select-by [[opcode & operands :as obj-spec] context]

  (cond
    (sequential? (:current context))
    (->>
     (:current context)
     (map #(select-by obj-spec (assoc context :current %)))
     (filter #(not (or (and (sequential? %) (empty? %)) (nil? %))))
     flatten
     vec)

    :else
    (cond
      (= (first operands) "*") (vec (vals (:current context)))
      :else ((keyword (first operands)) (:current context)))))

(defn obj-aggregator [obj]
  (cond (sequential? obj) (vec (flatten (map obj-aggregator obj)))
        (map? obj) (let [obj-vals (vec (mapcat #(cond (map? %) [%] (sequential? %) %) (vals obj)))
                         children (flatten (map obj-aggregator obj-vals))]
                     (vec (concat obj-vals children)))
        true (vector obj)))

(defn- walk-path [[next & parts] context]
  (cond
    (nil? next) (:current context)
    (= [:root] next) (walk-path parts (assoc context :current (:root context)))
    (= [:child] next) (walk-path parts context)
    (= [:current] next) (walk-path parts context)
    (= [:all-children] next) (walk-path parts (assoc context :current (vec (concat [(:current context)]
                                                                                   (obj-aggregator (:current context))))))
    (= :key (first next)) (walk-path parts (assoc context :current (select-by next context)))))

(defn- walk-selector [sel-expr context]
  (cond
    (= :index (first sel-expr)) (if (sequential? (:current context))
                                  (let [sel (nth sel-expr 1)]
                                    (if (= "*" sel)
                                      (:current context)
                                      (nth (:current context) (Integer/parseInt sel))))
                                  (throw (Exception. "object must be an array.")))
    (= :filter (first sel-expr)) (filter #(eval-expr (nth sel-expr 1) (assoc context :current %)) (:current context))))

(defn- walk [[opcode operand continuation] context]
  (let [down-obj (cond
                   (= opcode :path) (walk-path operand context)
                   (= opcode :selector) (walk-selector operand context))]
    (if continuation
      (walk continuation (assoc context :current down-obj))
      down-obj)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main
(defn at-path [path object]
  (walk (parse-path path) {:root object}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tests

(deftest basic-spec
  (is (= (at-path "$.hello" {:hello "world"}) "world"))
  (is (= (at-path "$.hello.world" {:hello {:world "foo"}}) "foo"))
  (is (= (at-path "$..world" {:hello {:world "foo"},
                              :baz {:world "bar",
                                    :quuz {:world "zux"}}})
         ["foo", "bar", "zux"]))
  (is (= (at-path "$.*.world" {:a {:world "foo"},
                               :b {:world "bar",
                                   :c {:world "baz"}}})
         ["foo", "bar"]))
  (is (= (at-path "$.foo[*]" {:foo ["a", "b", "c"]})
         ["a", "b", "c"]))
  (is (= (at-path "$.foo[?(@.bar=\"baz\")].hello"
                  {:foo [{:bar "wrong" :hello "goodbye"}
                         {:bar "baz" :hello "world"}]})
         ["world"]))
  (is (= (at-path "$.foo[?(@.id=$.id)].text"
                  {:id 45, :foo [{:id 12, :text "bar"},
                                 {:id 45, :text "hello"}]})
         ["hello"])))

(deftest json-path-modifications
  (let [json {:status 1 :order-items [{:status 2 :order-item [{:id 100} {:id 200 :hello "string"}]}
                                      {:status 2 :order-item [{:id 10} {:id 20}]}]}]
    (testing "select nested with vectors and params"
      (is (=
           [100 200 10 20]
           (at-path "$..order-item.id" json))))

    (testing "select nested with vectors"
      (is (=
           [{:id 100} {:id 200 :hello "string"} {:id 10} {:id 20}]
           (at-path "$..order-item" json))))

    (testing "select nested with vectors"
      (is (=
           ["string"]
           (at-path "$..order-item.hello" json))))))

(deftest nested-maps-seq
  (is (= (at-path "$..order-item.id"
                  {:status 1 :order-items [{:status 2 :order-item [{:id 100} {:id 200}]}
                                           {:status 2 :order-item [{:id 10} {:id 20}]}]})
         [100 200 10 20]))

  (is (= (at-path "$..user.lst" [{:user {:lst 1}} {:user {:lst 1}}]) [1 1])))

(deftest nested-seq-with-strings
  (is (= (at-path "$..a.quantity" [{:a {:quantity 1 :other ["hello"]}}]) [1]))
  (is (= (at-path "$..a.other" [{:a {:quantity 1 :other ["hello" "world"]}} {:b {:a {:other "first"}}}])
         ["hello" "world" "first"]))
  (is (= (at-path "$..a.other" [{:a {:quantity 1 :other "hello"}}])
         ["hello"])))

(run-tests)
