(ns gd.utils.common
  (:use clojure.data.json
        [clojure.algo.generic.functor :only (fmap)])
  (:use clojure.walk)
  (:use [clojure.test :only [deftest run-tests is]])
  (:use clojure.java.io)
  (:use taoensso.timbre)
  (:require [clj-time.coerce :as coerce]
            [clj-http.client :as c]
            [ring.util.codec :as codec])
  (:import (java.io PrintWriter))
  (:import
   (java.security
    NoSuchAlgorithmException
    MessageDigest)
   (java.math BigInteger)
   (org.joda.time DateTime DateMidnight)
   [java.io File FileOutputStream InputStreamReader]
   [java.util Properties]
   [java.net URI URL]))

(alter-var-root #'clojure.core/*out* (constantly (new java.io.PrintWriter *out*)))

(defn keyword-real-name[k]
  (if (instance? clojure.lang.Named k)
    (if-let [nspace (namespace k)]
      (str nspace "/" (name k))
      (name k))
    k))

(extend clojure.lang.Named Write-JSON
        {:write-json (fn[x ^PrintWriter out escape-unicode?]
                       (#'clojure.data.json/write-json-string (keyword-real-name x) out escape-unicode?))})

(alter-var-root #'clojure.data.json/as-str
                (fn[_]
                  (fn[x]
                    (if (instance? clojure.lang.Named x)
                      (keyword-real-name x)
                      (str x)))))

(defn flatten-coll[x]
  (let [coll-not-map? (fn[a] (and (coll? a) (not (map? a))))]
    (filter (complement coll-not-map?)
            (rest (tree-seq coll-not-map? seq x)))))

(defn find-first[p coll]
  (some (fn[el](and (p el) el)) coll))

(defn find-by-val[key val coll]
  (find-first (fn[m] (= val (key m))) coll))

(defn group-by-single[key coll]
  (fmap first (group-by key coll)))

(defn delete-element [vc pos]
  (vec (concat
        (subvec vc 0 pos)
        (subvec vc (inc pos)))))

(defn dissoc-in
  [m [k & ks :as keys]]
  (if ks
    (if-let [nextmap (get m k)]
      (let [newmap (dissoc-in nextmap ks)]
        (if (empty? newmap)
          (if (map? m)
            (dissoc m k)
            (delete-element m k))
          (assoc m k newmap)))
      m)
    (if (map? m)
      (dissoc m k)
      (delete-element m k))))

(defn throwf [& message]
  (throw (Exception. (apply format message))))

(defn update-multi[map-input keys f]
  (reduce #(assoc %1 %2 (f (get %1 %2))) map-input keys))

(defn update-multi-key[map-input keys f]
  (reduce #(assoc %1 %2 (f %2 (get %1 %2))) map-input keys))

(defn update-if-contains[map key f]
  (if (contains? map key)
    (assoc map key (f (get map key)))
    map))

(defn xor [a b]
  (or (and a (not b)) (and (not a) b)))

(defn parse-int[str]
  (if (string? str)
    (if (.contains str ".")
      (new BigDecimal str)
      (Integer/parseInt str))
    str))

(defn parse-long[str]
  (if (string? str)
    (if (.contains str ".")
      (new BigDecimal str)
      (Long/parseLong str))
    str))

(def map! (comp doall map))

(extend-protocol coerce/ICoerce
  DateMidnight
  (to-date-time [date]
    (.toDateTime date)))

;; Extend JSOON writer
(defn- write-json-delay [x #^PrintWriter out escape-unicode?]
  (.print out (json-str (deref x) :escape-unicode escape-unicode?)))

(extend clojure.lang.Delay Write-JSON
        {:write-json write-json-delay})

(defn file-content*[name]
  (slurp (.getResourceAsStream (clojure.lang.RT/baseLoader) name)))

(defn map-longest
  ([fn missing-value-fn c1]
     (map fn c1))
  ([fn missing-value-fn c1 & colls]
     (lazy-seq
       (when (not-every? empty? (conj colls c1))
         (let [firsts (map first (conj colls c1))]
           (cons
            (apply fn (map #(if (nil? %) (missing-value-fn) %) firsts))
            (apply map-longest
                   (conj (map rest colls) (rest c1) missing-value-fn fn))))))))

(defn load-properties [& paths]
  (let [result (new Properties)]
    (doseq [path paths]
      (with-open [f (new InputStreamReader
                         (.getResourceAsStream (clojure.lang.RT/baseLoader) path)
                         "UTF-8")]
        (.load result f)))
    result))

(defn store-properties[props]
 (reduce #(str %1 "\n" %2) (sort (map str props))))

(defn deep-merge-with
  "Like merge-with, but merges maps recursively, applying the given fn
  only when there's a non-map at a particular level.

  (deepmerge + {:a {:b {:c 1 :d {:x 1 :y 2}} :e 3} :f 4}
               {:a {:b {:c 2 :d {:z 9} :z 3} :e 100}})
  -> {:a {:b {:z 3, :c 3, :d {:z 9, :x 1, :y 2}}, :e 103}, :f 4}"
  [f & maps]
  (apply
   (fn m [& maps]
     (if (every? (fn[m](or (nil? m) (map? m))) maps)
       (apply merge-with m maps)
       (apply f maps)))
   maps))

(defn deep-merge-take-last[& maps]
  (apply deep-merge-with (fn[& args](last args)) maps))

(defn indexed[coll]
  (map-indexed vector coll))

(defn as-str
  "Like clojure.core/str, but if an argument is a keyword or symbol,
its name will be used instead of its literal representation.

Example:
 (str :foo :bar) ;;=> \":foo:bar\"
 (as-str :foo :bar) ;;=> \"foobar\"

Note that this does not apply to keywords or symbols nested within
data structures; they will be rendered as with str.

Example:
 (str {:foo :bar}) ;;=> \"{:foo :bar}\"
 (as-str {:foo :bar}) ;;=> \"{:foo :bar}\" "
  ([] "")
  ([x] (if (instance? clojure.lang.Named x)
         (name x)
         (str x)))
  ([x & ys]
     ((fn [#^StringBuilder sb more]
        (if more
          (recur (. sb (append (as-str (first more)))) (next more))
          (str sb)))
      (new StringBuilder #^String (as-str x)) ys)))

(defn delete-file-recursively
  "Delete file f. If it's a directory, recursively delete all its contents.
Raise an exception if any deletion fails unless silently is true."
  [f & [silently]]
  (let [f (file f)]
    (if (.isDirectory f)
      (doseq [child (.listFiles f)]
        (delete-file-recursively child silently)))
    (delete-file f silently)))

(defn unescape-html
  [text]
  (.. ^String (as-str text)
      (replace "&amp;"  "&")
      (replace "&lt;"  "<")
      (replace "&gt;"  ">")
      (replace "&quot;" "\"")))

(defn pages-count[page-size count]
  (if count
    (long (/ (+ count page-size -1) page-size))
    0))

(defn map-keys[f map-value]
  (reduce merge (map (fn[[k v]] {(f k) v}) map-value)))

(defn mreduce[f lst]
  (reduce merge (map f lst)))

(defn mreduce-indexed[f lst]
  (reduce merge (map-indexed f lst)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Recursive fmap
(defmulti rmap
  "Applies function f to each item in the data structure s and returns
   a structure of the same kind."
  {:arglists '([f s])}
  (fn [f s] (type s)))

(defmethod rmap clojure.lang.IPersistentList
  [f v]
  (map (partial rmap f) v))

(defmethod rmap clojure.lang.IPersistentVector
  [f v]
  (into (empty v) (map (partial rmap f) v)))

(defmethod rmap clojure.lang.IPersistentMap
  [f m]
  (into (empty m) (for [[k v] m] [k (rmap f v)])))

(defmethod rmap clojure.lang.IPersistentSet
  [f s]
  (into (empty s) (map (partial rmap f) s)))

(defmethod rmap :default
  [f s]
  (f s))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Async printing
(def print-seq str-println)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Escape URL
(defn escape-url[url]
  (try
    (let [{:keys [scheme server-name server-port uri user-info query-string]}
          (c/parse-url (codec/url-decode url))]
      (.toExternalForm
       (.toURL (new java.net.URI (as-str scheme) user-info server-name (or server-port 80) uri query-string nil))))
    (catch Throwable e
      (print-seq "URL " url)
      (throw e))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Properties
(defn property[key not-found]
  (or (System/getProperty key) not-found))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; If value - is not sequence, then - create sequence. Otherwise - return as is.
(defn maybe-seq[val]
  (when val
    (if (sequential? val) val [val])))

(defn maybe-int-seq[val]
  (seq (map parse-int (maybe-seq val))))

(defn maybe-keyword-seq[val]
  (seq (map keyword (maybe-seq val))))

(defn maybe-deref-delayed[val]
  (if (delay? val) (deref val) val))

(defmethod fmap nil
  [f s]
  (f nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Distinct by
(defn distinct-by
  [function coll]
  (let [step (fn step [xs seen]
               (lazy-seq
                 ((fn [[f :as xs] seen]
                    (when-let [s (seq xs)]
                      (if (contains? seen (function f))
                        (recur (rest s) seen)
                        (cons f (step (rest s) (conj seen (function f)))))))
                  xs seen)))]
    (step coll #{})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Parallel execution
(defn- split-on-parts[parts-num coll]
  (let [size (count coll)
        part-size (/ size parts-num)]
    (partition-all part-size coll)))

(defn parallel[function coll & [num]]
  (apply concat (doall (pmap (fn[part] (doall (map function part)))
                             (split-on-parts (or num (.availableProcessors (Runtime/getRuntime))) coll)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; File utils

(defn exists?[file]
  (.exists (new java.io.File file)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cond let
(defmacro cond-let
  "Takes a binding-form and a set of test/expr pairs. Evaluates each test
  one at a time. If a test returns logical true, cond-let evaluates and
  returns expr with binding-form bound to the value of test and doesn't
  evaluate any of the other tests or exprs. To provide a default value
  either provide a literal that evaluates to logical true and is
  binding-compatible with binding-form, or use :else as the test and don't
  refer to any parts of binding-form in the expr. (cond-let binding-form)
  returns nil."
  [bindings & clauses]
  (let [binding (first bindings)]
    (when-let [[test expr & more] clauses]
      (if (= test :else)
        expr
        `(if-let [~binding ~test]
           ~expr
           (cond-let ~bindings ~@more))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Local cache

(def ^:dynamic *request-cache*)

(defn wrap-request-cache[var]
  (let [name (:name (meta var))]
    (alter-var-root
     var
     (fn[f]
       (fn[& args]
         (if (thread-bound? #'*request-cache*)
           (or (get-in @*request-cache* [name args])
               (let [ret (apply f args)]
                 (swap! *request-cache* assoc-in [name args] ret)
                 ret))
           (apply f args)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Translit
(defn translit[word]
  (let [letters
        (hash-map
         \а "a" \б "b" \в "v" \г "g" \д "d" \е "e" \ё "yo" \ж "zh" \з "z" \и "i" \й "j" \к "k"
         \л "l" \м "m" \н "n" \о "o" \п "p" \р "r" \с "s" \т "t" \у "u" \ф "f" \х "h" \ц "ts"
         \ч "ch" \ш "sh" \щ "sh'" \ъ "`" \ы "y" \ь "" \э "e" \ю "yu" \я "ya" \№ "No"
         \ї "yi" \і "i" \є "ye")]
    (->
     (apply str (map (fn[letter] (get letters letter letter))
                     (.toLowerCase (.trim word))))
     (.replaceAll "[^a-zA-Z]+" " ")
     (.replaceAll "[\\s]+" "-")
     (.replaceAll "[-]+$" ""))))
