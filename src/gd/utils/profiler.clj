(ns gd.utils.profiler
  (:use robert.hooke)
  (:use korma.core)
  (:use gd.utils.common)
  (:use gd.utils.db)
  (:use monger.result)
  (:use monger.conversion)
  (:use com.reasonr.scriptjure)
  (:use [clojure.algo.generic.functor :only (fmap)])
  (:require
   [noir.request]
   [noir.options :as opt]
   [noir.server :as server]
   [noir.response :as resp]
   [monger.collection :as mc]
   [monger.conversion :as mcon])
  (:import [com.mongodb WriteConcern MapReduceCommand$OutputType])
  (:import [java.io.File]))

(def profiling "profiling")

(def ^{:dynamic true} profiling-context nil)

(defmacro measure
  "Evaluates expr and measure the time it took. Returns : {:res result, :time time}"
  [expr]
  `(let [start# (. System (nanoTime))
         ret# ~expr]
     {:res ret# :time (/ (double (- (. System (nanoTime)) start#)) 1000000.0)}))

(defn- persist-profile-result[type time & [params]]
  (try (mc/insert profiling (merge {:type type :time time} params) WriteConcern/NONE) (catch Exception e)))

(defn- add-profile-result[type time & [params]]
  (when (thread-bound? #'profiling-context)
    (swap! profiling-context
           conj
           (merge {:type type
                   :time time}
                  params))))

(defn get-current-sql-time[]
  (Math/ceil (reduce + (map :time (filter #(= (:type %) :db) @profiling-context)))))

(defn get-current-sql-count[]
  (count (filter #(= (:type %) :db) @profiling-context)))

(defn get-query-distribution[]
  (fmap count
        (group-by identity
                  (map (comp second (partial re-find #"(SELECT|UPDATE|INSERT)") :query)
                       (filter #(= (:type %) :db) @profiling-context)))))

(defmacro quick-profile[& body]
  `(binding [profiling-context (atom nil)]
     (let [res# (measure (do ~@body))
           sql-time# (get-current-sql-time)]
       (println (format "DB: %s CPU: %s" sql-time# (- (:time res#) sql-time#)))
       (:res res#))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Korma profiler
(defn sql-profiler[f & [query]]
  (let [{:keys [res time]} (measure (apply f [query]))]
    (add-profile-result :db time {:query (:sql-str query)})
    res))

(add-hook #'korma.db/do-query #'sql-profiler)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Web Request profiler
(defn- remove-file[obj]
  (cond (map? obj)
        (dissoc obj :tempfile)

        (sequential? obj)
        (map remove-file obj)

        true
        obj))

(defn wrap-profiling
  [handler]
  (fn [request]
    (if (some #(.startsWith (:uri request) %) ["/js/" "/css/" "/fonts/" "/img/" "/utils/" "/packed/"])
      (handler request)
      (binding [profiling-context (atom nil)]
        (let [{:keys [res time]} (measure (handler request))]
          (persist-profile-result :web time {:uri (:uri request)
                                             :params (fmap remove-file (:params request))
                                             :results @profiling-context})
          res)))))

(ensure-mongo-collection profiling {:capped true :size (* 32 1024 1024)})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Analyzer
(defn- process-results[result]
  (doseq [{:keys [sample] :as res}
          (reverse
           (->> result
                (map #(assoc % :avg (/ (:time %) (:num %))))
                (sort-by :num)))]
    (println (-> res
                 (assoc :sample (.replaceAll sample "FROM ([^ ]*).*" "FROM $1"))
                 (dissoc :query)))))

(defn get-web-results[]
  (let [output (mc/map-reduce profiling
                              (js (fn[]
                                    (emit {:uri (aget this "uri") :params (aget this "params")}
                                          {:num 1 :time (aget this "time")})))
                              (js (fn[key arr]
                                    (var sum 0 num 0)
                                    (doseq [i arr]
                                      (+= sum (aget arr i "time"))
                                      (+= num (aget arr i "num")))
                                    (return {:uri key :time sum :num num})))
                              nil MapReduceCommand$OutputType/INLINE {})
        result (map :value (mcon/from-db-object (.results output) true))]
    (process-results result)))

(defn get-sql-results[]
  (let [output (mc/map-reduce profiling
                              (js (fn[]
                                    (var res (aget this "results"))
                                    (doseq [i res]
                                      (if (. (aget res i "query") match #"SELECT")
                                        (emit (. (aget res i "query") replace #"WHERE.*" "")
                                              {:num 1 :time (aget res i "time") :sample (aget res i "query")})))))
                              (js (fn[key arr]
                                    (var sum 0 num 0 sample "")
                                    (doseq [i arr]
                                      (set! sample (aget arr i "sample"))
                                      (+= sum (aget arr i "time"))
                                      (+= num (aget arr i "num")))
                                    (return {:query key :time sum :num num :sample sample})))
                              nil MapReduceCommand$OutputType/INLINE {})
        result (map :value (mcon/from-db-object (.results output) true))]
    (process-results result)))

(defn get-detailed-sql-results-for-service[]
  (let [output (mc/map-reduce profiling
                              (js (fn[]
                                    (var res (aget this "results"))
                                    (doseq [i res]
                                      (if (. (aget res i "query") match #"SELECT")
                                        (emit (. (aget res i "query") replace #"WHERE.*" "")
                                              {:num 1 :time (aget res i "time") :sample (aget res i "query")})))))
                              (js (fn[key arr]
                                    (var sum 0 num 0 sample "")
                                    (doseq [i arr]
                                      (set! sample (aget arr i "sample"))
                                      (+= sum (aget arr i "time"))
                                      (+= num (aget arr i "num")))
                                    (return {:query key :time sum :num num :sample sample})))
                              nil MapReduceCommand$OutputType/INLINE {:uri "/themes/stocks/female/"})
        result (map :value (mcon/from-db-object (.results output) true))]
    (process-results result)))

(defn avg[lst]
  (Math/ceil (float (/ (reduce + lst) (count lst)))))

(defn time-for-sql[service]
  (let [res
        (mc/find-maps profiling {:uri "/service" :params.service service})

        sql-times
        (map #(reduce + (map :time (:results %))) res)

        web-times
        (map :time res)]
    [service
     {:all
      (avg web-times)
      :sql
      (avg sql-times)
      :web
      (avg (map - web-times sql-times))}]))

(defn all-services[]
  (map time-for-sql (mcon/from-db-object (mc/distinct profiling :params.service) true)))
