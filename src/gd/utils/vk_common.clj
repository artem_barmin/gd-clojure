(ns gd.utils.vk-common
  (:use digest
        [hiccup.util :only [url]]
        gd.log
        hiccup.form
        hiccup.element
        gd.utils.web
        clojure.data.json
        gd.model.context
        gd.utils.common)
  (:require [clojure.string :as string]
            [noir.session :as session]
            [clj-http.client :as client]))

(defn vkontakte-secret[]
  (context-info "TrpxodEFdCfN0hGs7c4r" :vk :secret))

(defn vkontakte-app-number[]
  (context-info 3645590 :vk :app-id))

(def vkontakte-standalone-app-number 3661194)
(def vkontakte-main-group 48899846)

(defn parse-url-to-map[value]
  (reduce merge {}
          (map
           (fn[[k v]][(keyword k) v])
           (map #(string/split % #"=") (string/split value #"&")))))

(def ^:dynamic *vk-auth-token*)

(defonce last-action-time (atom nil))

(defn- current-second[]
  (long (/ (System/currentTimeMillis) 1000)))

(add-watch last-action-time :throttle (fn[k ref old new]
                                        (when (>= (:number new) 3)
                                          (Thread/sleep (+ (- 1000 (mod (System/currentTimeMillis) 1000)) 50))
                                          (reset! ref {:sec (current-second) :number 0}))))

(defn make-server-request[method query-params]
  (swap! last-action-time (fn[{:keys [sec number]}] {:sec (current-second) :number (inc (or number 0))}))
  (let [result
        (read-json
         (:body
          (client/get
           (str "https://api.vk.com/method/" method)
           {:query-params (merge {:access_token *vk-auth-token*}
                                 query-params)})))]
    (when-not (:response result)
      (throwf "VK API error : %s" result))
    (:response result)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Counter utils

(defn create-counter[export-counters deal]
  (let [counter (atom {:status :starting})]
    (add-watch counter :remove-when-zero
               (fn[k r o n]
                 (when (= :finished (:status n))
                   (swap! export-counters dissoc deal))))
    (swap! export-counters assoc deal counter)
    counter))

(defn set-counter[counter status & [number]]
  (reset! counter {:status status :number (or number 0)}))

(defn dec-counter[counter]
  (swap! counter update-in [:number] dec))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Async runner

(defn async[function counter & [token-type]]
  (let [token (session/get (or token-type :vk-auth-token))
        token (or (:access_token (parse-url-to-map token)) token)]
    (in-thread
     (fn[]
       (binding [*vk-auth-token* token]
         (function)))
     :error-hook (fn[] (set-counter counter :finished)))))
