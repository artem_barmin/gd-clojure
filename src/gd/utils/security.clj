(ns gd.utils.security
  (:use gd.log)
  (:require [noir.session :as session]
            [noir.server :as server]
            [clj-time.coerce :as tm]
            [clj-time.core :as t]
            [monger.collection :as mc]))

(defonce login-callbacks (atom nil))

(mc/ensure-index "web_sessions" {:user-id 1})

(defn mongo-storage-transfomer[entity]
  (let [map-update
        (fn[ent fn & keys]
          (reduce #(update-in %1 (if (vector? %2) %2 [%2]) fn) ent keys))]
    (when entity
      (-> entity
          (map-update (partial map #(map-update % keyword :type)) :auth-profile)
          (map-update keyword :sex)
          (map-update tm/to-timestamp :info-registered :info-last-visit)
          (map-update tm/to-sql-date :birthdate)))))

(def user-session-key :user)

;; Security helpers
(defn logged[]
  (when (thread-bound? #'session/*noir-session*)
    (if-let [user (session/get user-session-key)]
      (mongo-storage-transfomer user))))

(defn id[]
  (:id (logged)))

(defn- auth-profile-by-type[type]
  (first (filter #(= type (:type %)) (:auth-profile (logged)))))

(defn have-role?[user role]
  (some #{role} (:role (or user (logged)))))

(defn root?[]
  (or (#{"admin" "root"} (:login (auth-profile-by-type :internal)))
      (have-role? (logged) "admin")))

(defn vk-uuid[]
  (:uuid (auth-profile-by-type :vk)))

(defn organizer?[& [user]]
  (have-role? user "organizer"))

(defn admin?[& [user]]
  (or
   (root?)
   (have-role? user "manager")
   (have-role? user "partner")))

(defn partner?[& [user]]
  (or (root?) (have-role? user "partner")))

(defn manager?[& [user]]
  (have-role? user "manager"))

(defn set-logged[user]
  (session/put! :user-id (:id user))
  (session/put! user-session-key user)
  (doseq [[_ callback] @login-callbacks]
    (callback))
  nil)

(defn set-specific-logged[user]
  (mc/update "web_sessions" {:user.id (:id user)} {"$set" {user-session-key user}} :multi true)
  nil)

(defn register-login-callback[name fun]
  (swap! login-callbacks assoc name fun))

(defn get-last-update-time[user-id]
  (when-let [time
             (reduce
              (fn
                ([] nil)
                ([t1] t1)
                ([t1 t2](if (t/after? t1 t2) t1 t2)))
              (map :date (mc/find-maps "web_sessions" {:user-id user-id})))]
    (tm/to-date time)))

(defmacro wrap-model-security
  "Wrap actions into security context with given user."
  [user & body]
  `(binding [session/*noir-session* (atom {:user ~user})]
     ~@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Temporary sessions

(defn- restore-previous-user[]
  (let [previous-user (session/get :previous-user)]
    (info "Temp session : restore previous user and clear data")
    (session/put! :temporary-session-ending nil)
    (set-logged (session/get! :previous-user))))

(defn- exists-previous-user?[]
  (boolean (session/get :previous-user)))

(defn set-temp-logged[user]
  (info "Temp session : assign user " (:id user))
  ;; store previous user session
  (session/put! :previous-user (logged))
  (session/put! :temporary-session-ending (t/from-now (t/minutes 5)))
  ;; login as needed user
  (set-logged user))

(defn logout[]
  (if (exists-previous-user?)
    (restore-previous-user)
    (session/remove! user-session-key)))

(defn temp-session-processing[handler request]
  (if (thread-bound? #'session/*noir-session*)
    (let [temp-session-ending (session/get :temporary-session-ending)]
      (when temp-session-ending
        (session/put! :temporary-session-ending (t/from-now (t/minutes 5))))
      (if (and temp-session-ending (t/before? (if (instance? java.util.Date temp-session-ending)
                                                (tm/from-date temp-session-ending)
                                                temp-session-ending)
                                              (t/now)))
        (do (restore-previous-user)
            (handler request))
        (handler request)))
    (handler request)))

(defn wrap-temporary-session
  [handler]
  (fn [request]
    (temp-session-processing handler request)))

(server/add-middleware wrap-temporary-session)
