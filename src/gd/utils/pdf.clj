(ns gd.utils.pdf
  (:use clj-pdf.core)
  (:import
   java.awt.Color
   [com.lowagie.text FontFactory Font]
   [com.lowagie.text.pdf BaseFont]))

(alter-var-root
 #'clj-pdf.core/font
 (constantly
  (fn[{style    :style
       styles   :styles
       size     :size
       [r g b]  :color
       family   :family
       ttf-name :ttf-name}]
    (FontFactory/getFont
     (if-not (nil? ttf-name)
       ttf-name
       (condp = (when family (name family))
         "courier"      FontFactory/COURIER
         "helvetica"    FontFactory/HELVETICA
         "times-roman"  FontFactory/TIMES_ROMAN
         "symbol"       FontFactory/SYMBOL
         "zapfdingbats" FontFactory/ZAPFDINGBATS
         FontFactory/HELVETICA))

    "cp1251"

    true

    (float (or size 10))
    (cond
     styles (#'clj-pdf.core/compute-font-style styles)
     style (get-style style)
     :else Font/NORMAL)

    (if (and r g b)
      (new Color r g b)
      (new Color 0 0 0))))))

(defn generate-pdf [output template]
  (pdf [{:font {:ttf-name "DejaVu Sans"}
         :register-system-fonts? true}
        template]
       output))
