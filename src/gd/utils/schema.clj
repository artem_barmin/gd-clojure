(ns gd.utils.schema
  (:use gd.utils.db)
  (:refer-clojure :exclude [replace bigint boolean char double float time alter drop compile])
  (:use lobos.compiler)
  (:use lobos.connectivity)
  (:use lobos.core)
  (:use lobos.schema)
  (:require (lobos [ast :as ast]))
  (:use [lobos.utils :only [make-index-name as-list join as-sql-keyword]])
  (:use gd.utils.graph)
  (:use gd.utils.common))

(ast/import-all)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extend compiler for gist and gin indexes
(defrecord CreateExtendedIndexStatement
    [db-spec iname tname columns type array])

(defmethod compile [:postgresql CreateExtendedIndexStatement]
  [statement]
  (let [{:keys [db-spec iname tname columns type array]} statement
        index-column #(format "coalesce(%s::text, '')" (as-identifier db-spec %))
        array-index-column #(if (keyword? %)
                              (as-identifier db-spec %)
                              (let [col (first %)
                                    options (set (rest %))]
                                (apply join \space
                                       (as-identifier db-spec (first %))
                                       (map as-sql-keyword options))))]
    (format (if array
              "CREATE INDEX %s ON %s USING %s %s"
              "CREATE INDEX %s ON %s USING %s (to_tsvector('russian',%s))")
            (as-identifier db-spec iname)
            (as-identifier db-spec tname (:schema db-spec))
            (name type)
            (if array
              (as-list (map array-index-column columns))
              (clojure.string/join " || ' ' || " (map index-column columns))))))

(defrecord FtsIndex [iname tname columns type array]
  Creatable

  (build-create-statement [this db-spec]
    (CreateExtendedIndexStatement. db-spec iname tname columns type array)))

(defn fts-index
  [table columns type & [array]]
  (let [tname (if (keyword? table) table (:name table))
        name (make-index-name tname :index columns)]
    (if (keyword? table)
      (FtsIndex. name tname columns type array)
      (update-in table [:indexes] conj
                 [name (FtsIndex. name tname columns type array)]))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Set all foreing constratins deferable
(defmethod compile [:postgresql ForeignKeyConstraintDefinition]
  [definition]
  (let [{:keys [db-spec cname columns parent-table parent-columns match
                triggered-actions]} definition]
    (join \space
          "CONSTRAINT"
          (as-identifier db-spec cname)
          "FOREIGN KEY"
          (as-list (map (partial as-identifier db-spec) columns))
          "REFERENCES"
          (as-identifier db-spec parent-table (:schema db-spec))
          (as-list (map (partial as-identifier db-spec) parent-columns))
          (when match (str "MATCH " (as-sql-keyword match)))
          (when (contains? triggered-actions :on-delete)
            (str "ON DELETE " (as-sql-keyword (:on-delete triggered-actions))))
          (when (contains? triggered-actions :on-update)
            (str "ON UPDATE " (as-sql-keyword (:on-update triggered-actions))))
          "DEFERRABLE INITIALLY IMMEDIATE")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Add alter to not-null for columns

(defmethod compile [:postgresql AlterModifyAction]
  [action]
  (let [{:keys [db-spec element]} action
        {:keys [data-type default not-null]} element]
    (cond
      not-null (join \space "ALTER COLUMN" (as-identifier db-spec (:cname element)) "SET NOT NULL")
      :else     ((get-method compile [:lobos.compiler/standard AlterModifyAction]) action))))

(defmethod compile [:postgresql AlterModifyDataTypeAndOptionsAction]
  [action]
  (let [{:keys [db-spec element]} action]
    (join \space
          "ALTER COLUMN"
          (as-identifier db-spec (:cname element))
          "SET DATA TYPE" (compile (:data-type element)))))

(defmethod compile [:postgresql CreateIndexStatement]
  [statement]
  (let [{:keys [db-spec iname tname columns options]} statement
        index-column #(cond (keyword? %)
                            (as-identifier db-spec %)

                            (string? %)
                            %

                            true
                            (let [col (first %)
                                  options (set (rest %))]
                              (apply join \space
                                     (as-identifier db-spec (first %))
                                     (map as-sql-keyword options))))]
    (format "CREATE %s INDEX %s ON %s %s"
            (str (when ((set options) :unique) "UNIQUE "))
            (as-identifier db-spec iname)
            (as-identifier db-spec tname (:schema db-spec))
            (as-list (map index-column columns)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extend lobos expression functions
(alter-var-root #'sql-functions (fn[val] (clojure.set/union val '#{coalesce array_length})))
(alter-var-root #'sql-infix-operators (fn[val] (clojure.set/union val '#{is is-not})))

(alter-var-root #'sql-symbols
                (constantly (clojure.set/union sql-infix-operators
                                               sql-prefix-operators
                                               sql-functions)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Case expression(for CHECK constraints)

;;;;;;;;;;;;;;;;;;;;
;; Low level

(defrecord CaseExpressionDefinition
    [db-spec cname where-clauses])

(defn nil-compile-fix[st]
  (.replaceAll st "'NULL'" "NULL"))

(defmethod compile [:postgresql CaseExpressionDefinition]
  [statement]
  (let [{:keys [db-spec cname where-clauses]} statement
        render-where (fn[[condition check]]
                       (prn (build-definition condition db-spec))
                       (format "WHEN %s THEN %s"
                               (nil-compile-fix (compile (build-definition condition db-spec)))
                               (nil-compile-fix (compile (build-definition check db-spec)))))]
    (format "CONSTRAINT %s CHECK (CASE %s END)"
            (as-identifier db-spec cname)
            (clojure.string/join " " (map render-where where-clauses)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; High level
(defrecord CaseExpression [cname where-clauses] Buildable
           (build-definition [this db-spec]
             (CaseExpressionDefinition. db-spec cname where-clauses)))

(defn check-case*
  "Constructs check constratint with case on field value."
  [table constraint-name where-clauses]
  (update-in table [:constraints] conj
             [constraint-name
              (CaseExpression. constraint-name where-clauses)]))

(defmacro check-case
  "Constructs check constratint with case on field value."
  [table constraint-name & where-clauses]
  `(check-case* ~table ~constraint-name
                ~(vec
                  (map (fn[[cond check]]
                         `[(lobos.schema/expression ~cond)
                           (lobos.schema/expression ~check)])
                       (partition 2 where-clauses)))))

(defmacro check-types[table column status-fn postfix & checks]
  (let [checks (mapcat (fn[[values check]]
                         [`(~'in ~column (vec (map ~status-fn ~values))) check])
                       (partition-all 2 checks))]
    `(check-case ~table (str (name (:name ~table)) "-" (name ~column) "-" ~postfix) ~@checks)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Table processing utils

(defn- limit-name[name]
  (let [cnt (count name)
        limit 64]
    (if (> cnt limit)
      (.substring name (- cnt limit 1))
      name)))

(defn refer-to [table ptable & [column no-constraint not-null]]
  (let [column (keyword column)

        table-with-column-and-index
        (-> table
            (integer column (when not-null :not-null))
            (index (keyword
                    (limit-name
                     (.replaceAll
                      (str (:name table) "_" (name column) "_index")
                      "-" "_")))
                   [column]))

        column-name
        (keyword (as-str column "_" (:name table) "_fkey_" ptable))]
    (cond no-constraint
          table-with-column-and-index

          true
          (foreign-key table-with-column-and-index column-name
                       [column] ptable [:id] :on-delete :restrict))))

(defn find-table [name tables]
  (let [tables-with-name (filter #(= (:name %) name) tables)]
    (if (empty? tables-with-name)
      (table name)
      (first tables-with-name))))

(defn replace-table [table tables]
  (cons table (filter #(not= (:name %) (:name table)) tables)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Processing associations
(defn- remove-from-all[key lst]
  (map #(dissoc % key) lst))

(defn process-many-to-one
  "Create new foreign key on the current table side, with column :ref-column-name.
Key points to :ref-table"
  [tables]
  (let [process-reference
        (fn[table {:keys [ref-table ref-column-name no-constraint not-null]}]
          (refer-to table ref-table ref-column-name no-constraint not-null))

        process-table
        (fn[table]
          (reduce process-reference table (:many-to-one table)))]
    (remove-from-all :many-to-one (map process-table tables))))

(defn process-one-to-many
  "Create new foreign key on the :ref-table side, with column :ref-column-name.
Key points to current table"
  [tables]
  (let [process-reference
        (fn[tables {:keys [ref-table table ref-column-name no-constraint not-null]}]
          (replace-table (refer-to (find-table ref-table tables) table ref-column-name no-constraint not-null) tables))

        process-table
        (fn[tables table]
          (reduce process-reference tables (:one-to-many table)))]

    (remove-from-all :one-to-many (reduce process-table tables tables))))

(defn process-many-to-many
  "Create join table. First column - reference to :table, second column - reference to :ref-table"
  [tables]
  (let [table-name
        (fn[table]
          (->> table as-str))

        process-reference
        (fn[{:keys [ref-table from-table custom-table-name]}]
          (let [same-table (= (table-name from-table) (table-name ref-table))
                left (keyword (str (table-name from-table) "-" (when same-table "l") "key"))
                right (keyword (str (table-name ref-table) "-" (when same-table "r") "key"))]
            (table (keyword (or
                             (and custom-table-name (as-str custom-table-name))
                             (str (table-name from-table) "-with-" (table-name ref-table))))
                   (refer-to from-table left)
                   (refer-to ref-table right))))

        all-references
        (->> tables (map #(:many-to-many %)) flatten (filter not-empty))]
    (remove-from-all :many-to-many (concat tables (map process-reference all-references)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Defining associations
(defn many-to-one
  [table table-name & {:keys [column-name no-constraint not-null]}]
  (update-in table [:many-to-one] conj
             {:ref-column-name (or column-name (as-str "fkey_" table-name))
              :ref-table table-name
              :not-null not-null
              :table (:name table)
              :no-constraint no-constraint}))

(defn one-to-many
  [table table-name & {:keys [column-name no-constraint not-null]}]
  (update-in table [:one-to-many] conj
             {:ref-column-name (or column-name (as-str "fkey_" (:name table)))
              :ref-table table-name
              :not-null not-null
              :table (:name table)
              :no-constraint no-constraint}))

(defn many-to-many
  [table table-name & [custom-table-name]]
  (update-in table [:many-to-many] conj
             {:ref-table table-name
              :from-table (:name table)
              :custom-table-name custom-table-name}))

(defn one-to-one [table ptable & [column not-null]]
  (let [cname (-> (->> ptable name (apply str))
                  keyword)]
    (integer table (or column (as-str "fkey_" cname)) not-null)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils for schema definition
(defn id[table]
  (integer table :id :primary-key :auto-inc))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main macroses
;; TODO : rewrite to new graph library
(defmacro process-tables[tables & {:keys [add-id]}]
  `(let [tables#
         (map
          (fn[table#] (if ~add-id
                        (id table#)
                        table#))
          (list ~@tables))

         processed-tables#
         (-> tables#
             process-one-to-many
             process-many-to-many
             process-many-to-one)

         all-tables#
         (set (map :name processed-tables#))

         extract-table-dependency#
         (fn[table#]
           {(:name table#)
            (->> (vals (:constraints table#))
                 (map (fn[constraint#] (:parent-table constraint#)))
                 (remove nil?)
                 (remove (fn[name#](= name# (:name table#))))
                 (filter (fn[name#] (all-tables# name#))))})

         table-dependencies-graph#
         (into {} (map extract-table-dependency# processed-tables#))

         dependencies#
         (->> {:nodes (keys table-dependencies-graph#)
               :neighbors table-dependencies-graph#}
              dependency-list
              (map seq)
              (reduce concat))

         ordered-tables#
         (map (fn[tbl#](find-table tbl# processed-tables#)) dependencies#)]

     ordered-tables#))

(defmacro deftables
  "Macro for simple initial creation of tables. Automatically adds :id field and
resolve associations."
  [& tables]
  `(doseq [tbl# (process-tables ~(vec tables) :add-id true)]
     (create tbl#)))

(defmacro defaltered
  "Macro for altering existing schema definition. Also process associations. New tables
can be added, associations can be used just like in deftables."
  [& tables]
  `(do
     (let [current-schema# (init-metadata)]
       (doseq [tbl# (process-tables ~(vec tables))]
         (if ((:name tbl#) current-schema#)
           (alter :add tbl#)
           (create (-> tbl# (id))))))
     (reset-metadata-cache)))

(defmacro test-tables[& tables]
  `(process-tables ~(vec tables)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Custom types

(defn array[table name type]
  (column table name (data-type (str (clojure.core/name type) "[]"))))

(defn json[table name]
  (column table name (data-type "json")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Table class with fixed indexes(they will created on alter table statement)

(defn- build-table-elements [db-spec method & elements]
  (->> (apply concat elements)
       (map #(when (second %)
               (method (second %) db-spec)))
       (filter identity)))

(defrecord TableWithIndexes [name columns constraints indexes]
  Alterable Creatable Dropable

  (build-alter-statement [this action db-spec]
    (let [elements (build-table-elements db-spec
                                         build-definition
                                         columns
                                         constraints)]
      (concat
       (for [element elements]
         (AlterTableStatement.
          db-spec
          name
          action
          element))
       (build-table-elements db-spec
                             build-create-statement
                             indexes))))

  (build-create-statement [this db-spec]
    (conj
     (build-table-elements db-spec build-create-statement indexes)
     (CreateTableStatement.
      db-spec
      name
      (build-table-elements db-spec build-definition columns constraints))))

  (build-drop-statement [this behavior db-spec]
    (DropStatement. db-spec :table name behavior nil)))

(alter-var-root #'lobos.schema/table*
                (fn[_]
                  (fn [name & [columns constraints indexes]]
                    (name-required name "table")
                    (TableWithIndexes. name
                                       (or columns {})
                                       (or constraints {})
                                       (or indexes {})))))

(alter-var-root #'lobos.schema/table?
                (fn[_]
                  (fn [o]
                    (instance? TableWithIndexes o))))

(derive TableWithIndexes :lobos.schema/definition)
