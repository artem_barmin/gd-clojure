(in-ns 'gd.utils.db)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Table aliases(allow to link entity with multiple same entities)
(comment
  (defentity message
    (belongs-to user {:fk :author})
    (belongs-to user {:fk :recipient}))

  "To make this work, you shoud define second table as alias"
  (defalias user-recipient user)

  "Use this alias for table relation. Use :entity if you wan't to change destination key."
  (defentity message
    (belongs-to user {:fk :author})
    (belongs-to user-recipient {:fk :recipient :entity :recipient}))

  "And after that use 'with-alias' instead of 'with'. This will extract correct relation
for given table."
  (-> query (with-alias user-recipient)))

(defmacro defalias[alias root]
  `(def ~alias (delay (assoc ~root :name ~(str alias)))))

(defmacro with-alias[query entity & rest]
  `(with ~query (force ~entity) ~@rest))

(defn- delay-relation-resolver
  "Allow to pass subentity to relation, that are wrapped into delay object."
  [_ & [ent sub-ent type opts]]
  (let [var-name (-> sub-ent meta :name)
        cur-ns *ns*]
    (assoc-in ent [:rel (name var-name)]
              (delay
                (let [resolved (ns-resolve cur-ns var-name)
                      sub-ent (when resolved
                                (deref sub-ent))
                      sub-ent (if (delay? sub-ent)
                                (force sub-ent)
                                sub-ent)]
                  (when-not (map? sub-ent)
                    (throw (Exception. (format "Entity used in relationship does not exist: %s" (name var-name)))))
                  (create-relation ent sub-ent type opts))))))

(add-hook #'korma.core/rel #'delay-relation-resolver)
