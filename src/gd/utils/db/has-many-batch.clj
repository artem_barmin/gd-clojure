(in-ns 'gd.utils.db)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Performance improvement. Don't execute special sql for each fetched entity, but pack one-to-many and many-to-many
;; associations into batches.

(defn- with-later-batch [rel query ent func]
  (when (not rel)
    (throw (Exception. (str "No relationship defined for table: " (:table ent)))))
  (let [fk (:fk rel)
        map-fk (keyword (.replaceAll (second (.split (:korma.sql.utils/generated fk) "\\.")) "\"" ""))
        pk (get-in query [:ent :pk])
        table (keyword (eng/table-alias ent))
        processing-level (:processing-level query)
        table (conj processing-level table)]
    (post-query query
                (fn[results]
                  (let [results-map
                        (->> (select ent
                               (func)
                               (where {fk [in (map! #(get-in % (conj processing-level pk)) results)]}))
                             (group-by map-fk))]
                    (map! (fn[result-ent]
                            (assoc-in result-ent table
                                      (get results-map (get-in result-ent (conj processing-level pk)))))
                          results))))))

(defn- batch-post-interceptor [f & [query sub-ent func]]
  (let [rel (get-rel (:ent query) sub-ent)]
    (cond
      :has-many
      (with-later-batch rel query sub-ent func)

      :else
      (f query sub-ent func))))

(add-hook #'korma.core/with* #'batch-post-interceptor)
