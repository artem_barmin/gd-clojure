(in-ns 'gd.utils.db)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Miscelaneous utilites
(defmacro defenum
  "Define 'ordinal' function for enum"
  [name & values]
  (let [values-num (->> (indexed values) (map reverse) (reduce concat))
        values-rev (->> (indexed values) (reduce concat))]
    `(do
       (defn ~name[st#]
         (when st#
           (case st# ~@values-num
                 (throwf "No matching clause found : (%s,%s)" ~(clojure.core/name name) st#))))
       (defn ~(symbol (str "from-" name))[st#]
         (when st#
           (case st# ~@values-rev)))
       (defn ~(symbol (str "values-" name))[]
         (vector ~@values))
       (defn ~(symbol (str "check-" name))[st#]
         (#{~@values} st#)))))

(defmacro use-enum
  "Add a function to be applied to results coming from the database"
  [ent name fields]
  `(-> ~ent
       (prepare
        (fn[ent#] (reduce #(if (get %1 %2)
                             (update-in %1 [%2] ~name)
                             %1) ent# ~fields)))
       (transform
        (fn[ent-trans#]
          (let [available-fields# (set (map #(when (get ent-trans# %) %) ~fields))

                processing-fields# (seq (set/intersection (set ~fields) available-fields#))]
            (if processing-fields#
              (update-in
               ent-trans#
               processing-fields#
               ~(symbol (str "from-" name)))
              ent-trans#))))))

(defmacro enum
  "Add a function to be applied to results coming from the database"
  [ent name fields & values]
  `(do
     (defenum ~name ~@values)
     (use-enum ~ent ~name ~fields)))

;; TODO : maybe lzo data to get more efficient reads?

(defprotocol PGJsonExtractor
  (read-from-json [this]))

(extend-protocol PGJsonExtractor
  org.postgresql.util.PGobject
  (read-from-json [this]
    (read-json (.getValue this)))

  String
  (read-from-json [this]
    (read-json this))

  nil
  (read-from-json [this]
    {}))

(defmacro json-data[ent field & [validate-fn]]
  `(-> ~ent
       (prepare
        (fn[~'ent]
          ~(when validate-fn
             `(when-let [field-val# (get ~'ent ~field)]
                (when-not (~validate-fn field-val#)
                  (throwf "JSON data not valid for %s.%s, get %s %s" (:name ~'ent) ~field
                          (if (sequential? field-val#)
                            (seq field-val#)
                            field-val#)
                          ~'ent))))
          (if (get ~'ent ~field)
            (update-in ~'ent [~field] #(json-str % :escape-unicode false))
            ~'ent)))
       (transform
        (fn[ent#]
          (if (get ent# ~field)
            (update-in ent# [~field] read-from-json)
            ent#)))))

(defmacro nippy-data[ent field & [validate-fn]]
  `(-> ~ent
       (prepare
        (fn[~'ent]
          (if (get ~'ent ~field)
            (update-in ~'ent [~field] serialize-base64)
            ~'ent)))
       (transform
        (fn[ent#]
          (if (get ent# ~field)
            (update-in ent# [~field] deserialize-base64)
            ent#)))))

(defn id=
  "Equality by id. Preserve SQL equality semantics : NULL != NULL"
  [ent1 ent2]
  (if (or (nil? (:id ent1)) (nil? (:id ent2)))
    nil
    (= (:id ent1) (:id ent2))))

(defmacro extend-entity
  "Extends existing enity"
  [ent & body]
  `(let [e# (-> ~ent
                ~@body)]
     (def ~ent e#)))


(defmacro select-first[ent & body]
  `(let [query# (-> (select* ~ent)
                    ~@body)
         result# (exec query#)]
     (first result#)))

(defmacro select-single[ent & body]
  `(let [query# (-> (select* ~ent)
                    ~@body)
         result# (exec query#)]
     (if (= (count result#) 1)
       (first result#)
       (throwf "No single entity found, actual size : %d. Where clause: %s"
               (count result#)
               (str (:where query#))))))

(defmacro select-single-null[ent & body]
  `(let [query# (-> (select* ~ent)
                    ~@body)
         result# (exec query#)]
     (first result#)))

(defmacro select-count[& body]
  `(let [result# (select-single ~@body (aggregate (~'count :*) :cnt))]
     (:cnt result#)))

(defmacro select-null[& body]
  `(let [result# (select ~@body)]
     (when-not (empty? result#)
       result#)))

(defn sql-now-date[]
  (tm/to-sql-date (now)))

(defn sql-now-timestamp[]
  (tm/to-timestamp (now)))

(defn fields-only [query & fs]
  (assoc query :fields fs))

(def ^{:dynamic true} *count-query* false)

(defn dry-transforms
  "Remove all post-queries and transforms from query"
  [query]
  (let [empty-transforms
        (fn[query]
          (if (:table (:ent query))
            (assoc-in query [:ent :transforms] [])
            query))]
    (->
     query
     empty-transforms
     (assoc :post-queries []))))

(defn count-query
  "Transforms usuall query to count(*) query with given filters and associated tables.
Must be last statement in query definition, because erase all fields and order specifications."
  [query & {:keys [custom-field]}]
  (if *count-query*
    (-> query
        (assoc :order []
               :limit nil
               :offset nil
               :having nil
               :post-queries [])
        (fields-only)
        (dry-transforms)
        (aggregate (or
                    custom-field
                    (str "count(distinct \"" (:table (:ent query)) "\".id)")) :count))
    query))

(defmacro get-count-db[body]
  `(binding [gd.utils.db/*count-query* true]
     (:count (first ~body))))

(defn serialize
  "Serializes value, returns a byte array"
  [v]
  (let [buff (java.io.ByteArrayOutputStream. 1024)]
    (with-open [dos (java.io.ObjectOutputStream. buff)]
      (.writeObject dos v))
    (.toByteArray buff)))

(defn deserialize
  "Accepts a byte array, returns deserialized value"
  [bytes]
  (with-open [dis (java.io.ObjectInputStream.
                   (java.io.ByteArrayInputStream. bytes))]
    (.readObject dis)))

(defn dry-aggregate[query function field]
  (-> query
      (fields-only)
      (dry-transforms)
      (aggregate function field)))

(defn lock-table[table]
  (exec-raw (format "LOCK TABLE \"%s\" IN EXCLUSIVE MODE" (name table))))
