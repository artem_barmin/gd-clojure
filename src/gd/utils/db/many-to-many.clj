(in-ns 'gd.utils.db)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Many to many relation
(defmacro has-many-join-table
  "Many-to-many with join table - type of relation.

Parameters:
:entity - name of list of associated entities
:join-table - name of join table(if not defined will be used `ent2`-with-`ent1` name) "
  [ent sub-ent & [opts]]
  `(has-many ~ent ~sub-ent (merge {:select-type :join-table
                                   :from-table (:table ~ent)
                                   :to-table (:table ~sub-ent)
                                   :join-table (as-str (:table ~ent) "-with-" (:table ~sub-ent))}
                                  ~opts)))

(defn- many-to-many-processor [rel query ent func]
  (let [table (eng/table-alias ent)

        result-entity (or (:entity rel)
                          (keyword table))

        pk (get-in query [:ent :pk])

        join-table (keyword (:join-table rel))

        entity-pk (keyword (as-str table "." (:pk ent)))

        same-table
        (= table (:table (:ent query)))

        left-key (keyword (as-str join-table "." table "-" (when same-table "l") "key"))

        right-key (keyword (as-str join-table "." (:table (:ent query)) "-" (when same-table "r") "key"))

        map-right-key (keyword (as-str (:table (:ent query)) "-" (when same-table "r") "key"))

        processing-level (:processing-level query)

        table (conj processing-level result-entity)]
  (post-query query
              (fn[results]
                (let [results-map
                      (->> (select ent
                             (func)
                             (join
                              :inner
                              join-table
                              (= entity-pk left-key))
                             (update-in [:fields] conj right-key)
                             (where
                              {right-key [in (map! #(get-in % (conj processing-level pk)) results)]}))
                           (group-by map-right-key))]
                  (map (fn[result-ent]
                         (assoc-in result-ent table
                                   (map #(dissoc % map-right-key)
                                        (get results-map (get-in result-ent (conj processing-level pk))))))
                       results))))))

(defn- many-to-many-interceptor [f & [query sub-ent func]]
  (let [rel (get-rel (:ent query) sub-ent)]
    (cond
      (and (= :has-many (:rel-type rel)) (= (:select-type rel) :join-table))
      (many-to-many-processor rel query sub-ent func)

      :else
      (f query sub-ent func))))

(add-hook #'korma.core/with* #'many-to-many-interceptor)
