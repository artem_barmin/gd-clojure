(in-ns 'gd.utils.db)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Mongo DB connectivity
(def mongo-db-name (property "mongo.db.name" "group-deals"))

(try
  (mg/connect!)
  (mg/set-db! (mg/get-db mongo-db-name))
  (catch Exception x))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Mongo utility functions

(def mongo-collections-info (atom {}))

(defn ensure-mongo-collection[name params & [indexes]]
  (swap! mongo-collections-info assoc name params)
  (when-not (mc/exists? name)
    (mc/create name params))
  (doseq [index indexes]
    (mc/ensure-index name {index 1})))

(defn recreate-mongo-collection[name]
  (mc/drop name)
  (mc/create name (get @mongo-collections-info name)))
