(in-ns 'gd.utils.db)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Impelementation of cache invalidation due to DB changes

(defn exec-sql-on-listen[sql-query]
  (connectivity/with-connection :notify-listen
    (do-prepared* sql-query)))

(defonce listen-con (connectivity/open-global :notify-listen (:default lobos-db-info)))
(defonce listen-subscription (exec-sql-on-listen "LISTEN cache_invalidation"))

(def registered-entities (atom #{}))

(defn- register-entities
  [original & [table]]
  (swap! registered-entities conj table)
  (original table))

(add-hook #'korma.core/create-entity #'register-entities)

(def notifications-map (atom {}))
(def notification-tracks (atom {}))

(defn process-notification[notification]
  (let [[op table & fields] (.split (.getParameter notification) ":")]
    (when-let [{:keys [field]} (get @notification-tracks table)]
      (let [field-num (get-in @notifications-map [table field])
            field-val (IntParse/parseInt (nth fields field-num))]
        [table field-val]))))

(defonce notify-change (Object.))

(defn check-notifications[]
  (locking notify-change
    (.notify notify-change)))

(def on-flush-event-listener (atom (fn[ids])))

(defn process-notifications[notifications]
  (when (seq notifications)
    (@on-flush-event-listener
     (distinct
      (->>
       notifications
       (map process-notification)
       (remove nil?)
       (group-by first)
       (fmap (partial map second))
       (mapcat (fn[[table values]]
                 (when table
                   (let [{:keys [function]} (get @notification-tracks table)]
                     (distinct (function (distinct values)))))))))))
  (locking notify-change
    (.wait notify-change)))

(defn add-notification-track[{:keys [track] :as fun}]
  (swap! notification-tracks assoc track fun))

(defonce cache-notifying-processor
  (.start (new Thread
               (fn[]
                 (connectivity/with-connection :notify-listen
                   (while true
                     (try
                       (with-query-results* ["select 1"] identity)
                       (process-notifications (.getNotifications (:connection (:notify-listen listen-con))))
                       (catch Throwable e
                         (.printStackTrace e *out*)))))))))

(defn refresh-db-notifications[]
  (doseq [[table fields] (init-metadata)]
    (let [table (name table)
          fields-to-track (->> fields
                               (map name)
                               (filter (fn[field] (re-find #"(id|fkey|-key)" field)))
                               sort)
          fields (fn[type](strings/join " || ':' || " (map (fn[f](str "coalesce(" type ".\"" f "\"" ",'0')")) fields-to-track)))
          function (str (.replaceAll table "-" "_") "_cache_invalidation_track")
          trigger (str table "-cache-invalidation")
          data {:function function
                :table table
                :trigger trigger}]
      (swap! notifications-map assoc table (reduce merge (map-indexed (fn[i field] {field i}) fields-to-track)))
      (exec-raw (render-resource "tools/pg-cache/remove-trigger.sql" data))
      (when (and (seq fields-to-track) ((set (keys @notification-tracks)) table))
        (exec-raw (render-resource "tools/pg-cache/function.sql"
                                   (merge data {:insert (fields "NEW")
                                                :update (fields "NEW")
                                                :delete (fields "OLD")})))
        (exec-raw (render-resource "tools/pg-cache/trigger.sql" data))))))
