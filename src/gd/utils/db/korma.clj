(in-ns 'gd.utils.db)

(alter-var-root #'korma.db/handle-exception
                (fn[_]
                  (fn[e sql params]
                    (println "Failure to execute query with SQL:")
                    (println sql " :: " params)
                    (.printStackTrace e *out*)
                    (throw e))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Database metadata mananager
(def login "postgres")
(def password (property "postgres.db.password" "1q%!@GSasd!@G"))
(def db-name (property "postgres.db.name" "group_deals"))
(def demo-db-name (property "postgres.db.name" "group_deals_demo"))

(defdb prod (postgres {:db db-name
                       :user login
                       :password password}))

(def ^:dynamic *database-override* nil)

(def custom-sql-databases
  {:demo (atom (create-db (postgres {:db demo-db-name
                                     :user login
                                     :password password})))})

(defn- get-database-connection[db]
  (let [db (if (map? db) (:pool db) db)]
    (if-not db
      (throw (Exception. "No valid DB connection selected."))
      (if (delay? db)
        @db
        db))))

(alter-var-root #'get-connection
                (fn[old]
                  (fn[db]
                    (get-database-connection (if *database-override*
                                               @*database-override*
                                               db)))))

(defonce lobos-db-info
  {:default {:classname "org.postgresql.Driver"
             :subprotocol "postgresql"
             :user login
             :password password
             :subname (str "//localhost:5432/" db-name)}
   :demo {:classname "org.postgresql.Driver"
          :subprotocol "postgresql"
          :user login
          :password password
          :subname (str "//localhost:5432/" demo-db-name)}})

(def ^:dynamic *lobos-database-override* :default)
(defn- get-lobos-db[& [type]]
  (get lobos-db-info (or type *lobos-database-override*)))

(defonce lobos-db (connectivity/open-global (get-lobos-db)))
(defonce lobos-demo-db (connectivity/open-global :demo (get-lobos-db :demo)))

(def ^:private metadata-cache (atom nil))

(defn reset-metadata-cache[]
  (reset! metadata-cache nil))

(defn init-metadata[]
  (if-let [metadata (get @metadata-cache *lobos-database-override*)]
    metadata
    (let [data (let [schema "public"]
                 (lobos.metadata/with-db-meta (get-lobos-db)
                   (doall (reduce merge (map (fn[table]
                                               {table (map (comp keyword :column_name)
                                                           (lobos.metadata/columns-meta schema (name table)))})
                                             (lobos.metadata/tables schema))))))]
      (swap! metadata-cache assoc *lobos-database-override* data)
      data)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fixed nested transactions

(def ^:dynamic *after-transaction-hook* nil)

(defmacro transaction
  "Execute all queries within the body in a single transaction."
  [& body]
  `(if (:connection clojure.java.jdbc.internal/*db*)
     (jdbc/transaction
       ~@body)
     (binding[*after-transaction-hook* (atom nil)]
       (let [result# (jdbc/with-connection (get-connection @_default)
                       (jdbc/transaction
                         ~@body))]
         (when-let [hooks# @*after-transaction-hook*]
           (doseq [hook# hooks#]
             (hook#)))
         result#))))

(defmacro transaction-with-connection[connection & body]
  `(binding [clojure.java.jdbc.internal/*db* ~connection]
     (jdbc/transaction
       ~@body)))

(defn add-after-transaction-hook[hook]
  (swap! *after-transaction-hook* conj hook))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Add transactions support to Lobos
(defmacro lobos-transaction[& body]
  `(let [db-spec# (get-in @connectivity/global-connections [:default-connection :db-spec])]
     (with-open [cnx# (connectivity/*get-cnx* db-spec#)]
       (binding [clojure.java.jdbc.internal/*db* {:connection cnx#
                                                  :level 1
                                                  :rollback (atom false)
                                                  :db-spec db-spec#}]
         (jdbc/transaction
           (jdbc/do-commands "SET CONSTRAINTS ALL DEFERRED")
           ~@body)))))

(alter-var-root #'connectivity/with-spec-connection
                (fn[_]
                  (fn[db-spec func]
                    (if (:connection clojure.java.jdbc.internal/*db*)
                      (func)
                      (lobos-transaction
                       (func))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Addional metadata interceptor
(defn- additional-relation-metadata
  "Allows additional metadata to be passed into relation, by simple merging 'rel' with custom
metadata. Used for passing :entity to 'one-to-one' relation"
  [f & [ent sub-ent type opts]]
  (merge opts (f ent sub-ent type opts)))

(add-hook #'korma.core/create-relation #'additional-relation-metadata)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Add support for 'AS' clause for table subqueries
(alter-var-root #'eng/map-val
                (constantly
                 (fn[v]
                   (let [func (utils/func? v)
                         generated (utils/generated? v)
                         args (utils/args? v)
                         sub (utils/sub-query? v)
                         pred (utils/pred? v)]
                     (cond
                       generated generated
                       pred (apply pred args)
                       func (let [vs (eng/comma-values args)]
                              (format func vs))
                       sub (let [as (:as sub)
                                 sql (utils/wrap (:sql-str sub))]
                             (swap! eng/*bound-params* #(vec (concat % (:params sub))))
                             (if as (format "%s AS \"%s\"" sql as) sql))
                       :else (eng/pred-map v))))))

(defn as[query name]
  (assoc query :as (clojure.core/name name)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Support for subqueries UNION

(defn union [& queries]
  (let [sub-query-sqls (map (comp :sql-str :korma.sql.utils/sub) queries)
        new-sql (clojure.string/join (str " UNION ") sub-query-sqls)]
    (->
     (first queries)
     (assoc-in [:korma.sql.utils/sub :sql-str] new-sql)
     (assoc-in [:korma.sql.utils/sub :params] (vec (apply concat (map (comp :params :korma.sql.utils/sub) queries)))))))

(defn union-all [& queries]
  (let [sub-query-sqls (map (comp :sql-str :korma.sql.utils/sub) queries)
        new-sql (clojure.string/join (str " UNION ALL ") sub-query-sqls)]
    (->
     (first queries)
     (assoc-in [:korma.sql.utils/sub :sql-str] new-sql)
     (assoc-in [:korma.sql.utils/sub :params] (vec (apply concat (map (comp :params :korma.sql.utils/sub) queries)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Maybe where(conditional filtering)

(defn check-condition[condition]
  (or (nil? condition)
      (false? condition)
      (and (sequential? condition)
           (or (empty? condition)
               (every? nil? condition)))))

(defmacro where-if[query condition body]
  `(if (check-condition ~condition)
     ~query (where ~query ~body)))

(defmacro modify-if[query condition body]
  `(if (check-condition ~condition)
     ~query (-> ~query ~body)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Allow adding default 'where' conditions for entities. Does not work for
;; one-to-one relations(only has-many) - because korma does not execute select*
;; for fetched entity.

(def ^:dynamic *disable-default-where* false)

(defn- default-where-processor
  [f & [{:keys [default-where] :as ent}]]
  ((if *disable-default-where* identity (or default-where identity)) (f ent)))

(add-hook #'korma.core/select* #'default-where-processor)

(defmacro default-where[entity & body]
  `(assoc ~entity :default-where (fn[query#] (where query# ~@body))))

(defmacro disable-default-where[& body]
  `(binding [*disable-default-where* true]
     ~@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Allow adding default fetch commands for entities

(def ^:dynamic *disable-default-fetch* false)

(defn- default-fetch-processor-select
  [f & [{:keys [default-fetch] :as ent}]]
  ((if *disable-default-fetch* identity (or default-fetch identity)) (f ent)))

(defn- default-fetch-processor-with
  [f & [query {:keys [default-fetch] :as ent} sub-ent-func]]
  (f query ent (fn[q]
                 (-> q
                     ((if *disable-default-fetch* identity (or default-fetch identity)))
                     sub-ent-func))))

(add-hook #'korma.core/select* #'default-fetch-processor-select)
(add-hook #'korma.core/with* #'default-fetch-processor-with)

(defn add-default-fetch[entity function]
  (update-in entity [:default-fetch]
             (fn[old-default-fetch]
               (comp (or old-default-fetch identity) function))))

(defmacro default-fetch[entity & body]
  `(add-default-fetch ~entity (fn[query#] (with query# ~@body))))

(defmacro disable-default-fetch[& body]
  `(binding [*disable-default-fetch* true]
     ~@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Postgres array

(comment

  "Example usage : korma and lobos"

  (defentity group-deal-update-log-entry
    (array-transform :changed-param-names))

  (defaltered
    (table :stock
           (array :images :varchar))))

(declare contains)
(declare overlaps)

(defrecord PostgresArray [values type])

(defn wrap-pg-array[values type]
  (str "ARRAY[" (eng/comma-values values) "]" (when type (str "::" type))))

(defonce array-type
  (alter-var-root #'eng/str-value
                  (fn[original]
                    (fn[v]
                      (cond
                        (instance? PostgresArray v)
                        (let [{:keys [values type]} v]
                          (if (empty? values)
                            "NULL"
                            (wrap-pg-array values type)))

                        true
                        (original v))))))

(defn- pg-array[coll & {:keys [type]}]
  (PostgresArray. coll type))

(defn from-array[arr]
  (seq (.getArray arr)))

(defn array-transform[ent name & {:keys [val-transform val-prepare as-set]}]
  (->
   ent
   (transform  (fn[obj]
                 (if-let [value (get obj name)]
                   (let [value (from-array value)]
                     (assoc obj name ((if as-set set identity)
                                      (if val-transform (map val-transform value) value))))
                   obj)))
   (prepare (fn[obj]
              (if-let [value (get obj name)]
                (assoc obj name (pg-array (if val-prepare (map val-prepare value) value)))
                obj)))))

(defn pred-contains [k v]
  (eng/infix k "@>" (utils/pred (fn[op values]
                                  (str op "[" (eng/comma-values v) "]"))
                                ["ARRAY" v])))

(defn pred-overlaps [k v]
  (eng/infix k "&&" (utils/pred (fn[op values]
                                  (str op "[" (eng/comma-values v) "]"))
                                ["ARRAY" v])))

(defonce array-predicates (alter-var-root #'eng/predicates
                                          (fn[old]
                                            (-> old
                                                (assoc 'contains 'pred-contains)
                                                (assoc 'overlaps 'pred-overlaps)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extract fkey for entity relationship

(defn fkey-for-relation[left-entity right-entity]
  (let [relation (get-in left-entity [:rel (:name right-entity)])]
    (if relation
      (keyword
       (last
        (.split
         (:korma.sql.utils/generated
          (:fk (deref relation)))
         "[.\"]")))
      (throwf "No relation defined for pair '%s' -> '%s'" (:name left-entity) (:name right-entity)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Upset - insert or update

(defmacro upset[entity fields condition]
  `(transaction
     (let [filter# (fn[query#] (where query# ~condition))
           values# ~fields]
       (if (zero? (count (select ~entity filter#)))
         (:id (insert ~entity (values values#)))
         (:id (update ~entity (set-fields values#) filter#))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Add support for 'HAVING' clause

(defn sql-having [query]
  (if (seq (:having query))
    (let [clauses (map #(if (map? %) (eng/map-val %) %)
                       (:having query))
          clauses-str (strings/join " AND " clauses)
          neue-sql (str " HAVING " clauses-str)]
      (update-in query [:sql-str] str neue-sql))
    query))

(defmacro having
  [query form]
  `(let [q# ~query]
     (update-in q# [:having] conj
                (eng/bind-query q#
                  (eng/pred-map
                   ~(eng/parse-where `~form))))))

(defmethod eng/->sql :select [query]
  (eng/bind-params
    (-> query
        (eng/sql-select)
        (eng/sql-joins)
        (eng/sql-where)
        (eng/sql-group)
        (sql-having)
        (eng/sql-order)
        (eng/sql-limit-offset))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Add support for INSERT INTO 'table1' SELECT

(defn insert-select*
  [ent]
  (if (:type ent)
    ent
    (let [q (empty-query ent)]
      (merge q {:type :insert-select
                :values []
                :results :keys}))))

(defmacro insert-select
  "INSERT INTO 'table' SELECT ..."
  [ent & body]
  `(let [query# (-> (insert-select* ~ent)
                    ~@body)]
     (exec query#)))

(defn select-values
  [query values]
  (assoc-in query [:values] values))

(defn sql-insert-select [query]
  (let [select-query (:values query)
        keys-clause (utils/comma (map eng/field-identifier (:fields query)))
        {:keys [params sql-str]} (eng/->sql select-query)
        neue-sql (str "INSERT INTO " (eng/table-str query) " " (utils/wrap keys-clause) " " sql-str)]
    (swap! eng/*bound-params* #(vec (concat % params)))
    (assoc query :sql-str neue-sql)))

(defmethod eng/->sql :insert-select [query]
  (eng/bind-params
    (-> query
        (sql-insert-select))))
