(in-ns 'gd.utils.db)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Full text searcher
(declare search)

(defn search:prepare-query-string
  "Transform plain query string into tsquery. Replace spaces with & symbol,
append wildcards to each word.

Also possible to change behavior using flags at start of string in format:
  (?w)

If w is specified - processors will be switched to explicit wildcard mode(by
default we search using phrase search, and only if word is passed as word* -
will enable wildcard append). By default this mode is turned off.
"
  [str]
  (let [flags (set (when (map? str) (:flags str)))
        str (if (map? str) (:str str) str)

        wildcard-processing
        (fn[str]
          (if (flags :w)
            (.replaceAll str "[*]" ":*"); transform all '*' in the end of word into ':*'
            (->
             str
             (.replaceAll "[*]" "")
             (.replaceAll "(?<=[^ \t])$" ":*") ; if before end of string there is no spaces
             (.replaceAll "[\\p{Space}]+" ":* ")
             (.replaceAll "[,]+" ":*,")))) ; between words
        ]
    (-> str
        (.replaceAll "[&]" "")
        (.replaceAll "[()]" "")
        (.replaceAll "[\\p{Space}]*,[\\p{Space}]*" ",")
        clojure.string/trim
        wildcard-processing
        (.replaceAll "[\\p{Space}]+" " & ") ; space - AND
        (.replaceAll "[,]+" " | ") ; comma - OR
        )))

(deftest query-prepare-test
  (is (= (search:prepare-query-string "test1 test2") "test1:* & test2:*"))
  (is (= (search:prepare-query-string "test1* test2*") "test1:* & test2:*"))
  (is (= (search:prepare-query-string "te*st1* && test2*") "test1:* & test2:*"))
  (is (= (search:prepare-query-string "test1 , test2 test abc") "test1:* | test2:* & test:* & abc:*"))
  (is (= (search:prepare-query-string "test1,test2 test abc") "test1:* | test2:* & test:* & abc:*"))
  (is (= (search:prepare-query-string {:flags [:w] :str "test1 test2"}) "test1 & test2"))
  (is (= (search:prepare-query-string {:flags [:w] :str "test1* test2"}) "test1:* & test2")))

(defn do-wrapper [op v]
  (str op (utils/wrap (str "'russian'," (eng/str-value v)))))

(defn pred-full-text [k v]
  (if (empty? v)
    (korma.sql.utils/generated "true")
    (eng/infix
     (korma.sql.utils/generated
      (format "to_tsvector('russian', %s)"
              (clojure.string/join
               " || ' ' || "
               (map (fn[field]
                      (format "coalesce(%s::text,'')" (korma.sql.engine/field-str field)))
                    (if (sequential? k) k [k])))))
     "@@"
     (utils/pred do-wrapper ["to_tsquery" (search:prepare-query-string v)]))))

(defmacro search:ids-by-search-string[query entity search-string field & sub-queries]
  `(if (empty? ~search-string)
     ~query
     (where
      ~query
      {:id [~'in
            (union
             ~@(map
                (fn[[_ sub-entities sub-fields]]
                  `(subselect ~entity
                              ~@sub-entities
                              (fields-only ~field)
                              (where {~sub-fields [~'search ~search-string]})))
                sub-queries))]})))

(defmacro search:part[plan fields]
  `[~plan ~fields])

(defonce full-text-search-predicate (alter-var-root #'eng/predicates (fn[old] (assoc old 'search 'pred-full-text))))

(run-tests)
