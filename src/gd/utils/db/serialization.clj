(in-ns 'gd.utils.db)

(defmethod print-dup java.sql.Date
  [d out]
  (.write out (str "#=" `(java.sql.Date. ~(.getTime d)))))

(defmethod print-dup java.sql.Timestamp
  [d out]
  (.write out (str "#=" `(java.sql.Timestamp. ~(.getTime d)))))

(defn deserialize-base64[object]
  (nippy/thaw-from-bytes (base64-decode object) :read-eval? true :compressed? false))

(defn serialize-base64[object]
  (base64-encode (nippy/freeze-to-bytes object :print-dup? true :compress? false)))
