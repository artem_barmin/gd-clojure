(in-ns 'gd.utils.db)

(comment "Хорошая презентация : http://www.pgcon.org/2008/schedule/events/69.en.html.")

(comment "
Создаем функцию on-stock-full-text-search-update(table,id-value)

on insert:выгребаем все из обычной вьюхи с where table.id=id-value и вставляем
в таблицу

on update:удаляем все с where table.id=id-value и вставляем заново с тем же условием

on delete:удаляем все с where table.id=id-value

Для каждой таблицы создаем тригер на обновление который вызывает функцию:
on-stock-full-text-search-update")

(defn exec-create-view[[sql params]]
  (exec-raw (reduce (fn[res param](.replaceFirst res "\\?" (str "'" param "'"))) sql params)))

(let [fix (fn[s](.replaceAll s "-" "_"))
      wrap-quotes (fn[s] (str "\"" s "\""))
      render-resource (fn[path vars]
                        (.replaceAll (render-resource path vars) "&quot;" "\""))]

  (defn- update-materialized-functions[name deps]
    (transaction
      (let [mat-name (wrap-quotes name)
            name (fix name)]

        (doseq [[table field source-field query] deps]
          (let [source-field
                (wrap-quotes (or source-field "id"))

                common-fields
                {:fn-name (fix (str name "-" table "-update-fn"))
                 :source-table-field source-field
                 :mat-view mat-name
                 :dep-id (str name "." (wrap-quotes field))
                 :mat-dep-id (wrap-quotes field)
                 :simple-view name
                 :dep-table (wrap-quotes table)
                 :trigger-name (fix (str name "-" table "-update-trigger"))}]
            (println "Create function : " (:fn-name common-fields))
            (exec-raw
             (if (map? query)
               (render-resource "tools/matview/custom-query-function.sql"
                                (merge common-fields
                                       {:new-query (sql-only (-> query
                                                                 (where {:id (raw (str "NEW." source-field))})
                                                                 select))
                                        :old-query (sql-only (-> query
                                                                 (where {:id (raw (str "OLD." source-field))})
                                                                 select))}))
               (render-resource "tools/matview/function.sql" common-fields))))))))

  (defn- update-materialized-triggers[view-name deps]
    (doseq [[table field] deps]
      (let [dep-table (wrap-quotes table)
            trigger (fix (str view-name "-" table "-update-trigger"))]
        (exec-raw
         (render-resource "tools/matview/remove-trigger.sql"
                          {:dep-table dep-table
                           :trigger-name trigger}))
        (exec-raw
         (render-resource "tools/matview/trigger.sql"
                          {:fn-name (fix (str view-name "-" table "-update-fn"))
                           :dep-table dep-table
                           :trigger-name trigger})))))

  (defn create-materialized-view[name query deps]
    (transaction
      (let [query (query-only (exec query))
            mat-name (wrap-quotes name)
            view-name (fix name)]

        ;; create unmaterizlized view - will be used as source of rows
        (println "Creating unmaterialized view")
        (exec-create-view [(str "CREATE VIEW " view-name " AS " (:sql-str query)) (:params query)])

        ;; initialize table for mat view
        (println "Initialize table from mat view")
        (exec-raw [(str "SELECT * INTO " mat-name " FROM " view-name)])

        (update-materialized-functions name deps)
        (update-materialized-triggers view-name deps))))

  (defn alter-materialized-view[alter-type name query deps]
    (transaction
      (let [query (query-only (exec query))
            mat-name (wrap-quotes name)
            view-name (fix name)]

        ;; Drop unmaterialized view and create it from scratch
        (println "Drop unmaterialized view")
        (exec-raw [(str "DROP VIEW IF EXISTS " view-name)])
        (exec-create-view [(str "CREATE VIEW " view-name " AS " (:sql-str query)) (:params query)])

        ;; initialize table for mat view
        (println "Re-initialize table from mat view")
        (case alter-type

          :alter
          (do (exec-raw [(str "DROP TABLE IF EXISTS " mat-name)])
              (exec-raw [(str "SELECT * INTO " mat-name " FROM " view-name)]))

          :alter-query
          (do (exec-raw [(str "TRUNCATE TABLE " mat-name)])
              (exec-raw [(str "INSERT INTO " mat-name " SELECT * FROM " view-name)])))

        (update-materialized-functions name deps)
        (update-materialized-triggers view-name deps))))

  (defn extract-deps-from-single-query[query]
    (let [tables (filter string?
                         (concat (map :table (:from query))
                                 (map (fn[[join-type table]] (if (map? table) (:table table) table))
                                      (:joins query))))]
      [(map (fn[val] [val (str "fkey_" val)]) tables)
       (map (fn[val] [(keyword (str val ".id")) (str "fkey_" val)]) tables)])))

(defn extract-deps-from-query
  "Process top level queries and also all subqueries"
  [query]
  (let [single-query-results
        (let [subqueries (atom [query])]
          (clojure.walk/postwalk
           (fn[m]
             (when (and (map? m) (:korma.sql.utils/sub m))
               (swap! subqueries conj (:korma.sql.utils/sub m)))
             m)
           query)
          (map extract-deps-from-single-query @subqueries))]
    [(distinct (mapcat first single-query-results))
     (distinct (mapcat second single-query-results))]))

(defn prepare-triggers[trigger-fields]
  (map (fn[triggers]
         (map (fn[field]
                (if (or (keyword? field) (string? field))
                  (name field)
                  field))
              triggers))
       trigger-fields))

(defmacro defmatview
  "
name - name of result table
query - plan for view creation
entity-fields - foreign keys to source entities that must be present in result
trigger-fields - [table 'fkey to retrieve info from view' 'fkey in source table to sync']
joined-trigger-fields - [table 'fkey to retrieve info from view' 'fkey in source query to sync' 'query that should be used']
type - :create,:alter,:alter-query
on :alter all indexes and constratins are dropped - because table is recreated
on :alter-query - only view query changed, data table is truncated and refilled with data

example

:trigger-fields
 [:rating :fkey_stock :fkey_stock] - means that on insert/update/delete of the 'rating' table,
we will process all entities in materialized view that have 'fkey_stock' the same as
'fkey_stock' of the NEW rating row.

 [:stock :fkey_stock] - means that on up/in/del on the stock table all entities in mat view that have
'fkey_stock' the same as 'id' of NEW stock row will be processed.
"
  [name query & {:keys [entity-fields trigger-fields type] :or {type :create}}]
  `(try
     (let [query-obj# ~query
           [deps# fields#] (extract-deps-from-query query-obj#)
           fields# (if (seq ~entity-fields)
                     (filter (comp (set ~entity-fields) first) fields#)
                     fields#)]
       ((if (= ~type :create) create-materialized-view (partial alter-materialized-view ~type))
        ~(str name)
        (apply fields query-obj# fields#)
        (or (seq (prepare-triggers ~trigger-fields)) deps#)))
     (catch java.sql.BatchUpdateException e#
       (throw (.getNextException e#)))))

(defmacro altermatview
  [name query & {:keys [entity-fields trigger-fields]}]
  `(defmatview ~name ~query
     :entity-fields ~entity-fields
     :trigger-fields ~trigger-fields
     :type :alter))

(defmacro altermatview-query-only
  [name query & {:keys [entity-fields trigger-fields]}]
  `(defmatview ~name ~query
     :entity-fields ~entity-fields
     :trigger-fields ~trigger-fields
     :type :alter-query))

(defmacro update-mat-functions[name query]
  `(let [[deps#] (extract-deps-from-query ~query)]
     (update-materialized-functions ~(str name) deps#)))
