(in-ns 'gd.utils.db)

(comment "we should add special type of accociation : map view for child collection.

Transformer will work as : group-by by some field, and make that each key have only one value(but not list)")

(defn transform-map-assoc[field group expect-single entity]
  (update-in
   entity
   [field]
   (fn[field-value]
     (fmap
      (fn[values]
        (if expect-single
          (if (= (count values) 1) (first values) (throwf "More than one value occured in map."))
          values))
      (group-by group field-value)))))

(defmacro map-association[ent assoc field group & {:keys [expect-single] :or {expect-single true}}]
  `(-> ~ent
       ~assoc
       (transform (partial transform-map-assoc ~field ~group ~expect-single))))
