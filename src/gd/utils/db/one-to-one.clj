(in-ns 'gd.utils.db)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; one-to-one postprocessors

(defn- process-results[{:keys [entity-name rename-keys keys] :as config} entity]
  (let [nested-entity
        (-> entity
            (select-keys keys)
            (set/rename-keys rename-keys))

        parent-entity
        (apply dissoc entity keys)]
    (if (every? nil? (vals nested-entity))
      parent-entity
      (assoc-in parent-entity entity-name nested-entity))))

(defn- add-one-to-one-processor
  "Add post-processor for one-to-one query. Also it is modifies current query(add fields for
request, assign them custom names).

table-name : name of table that should be transformed to sub-entity
table-alias : alias of sub-entity table in the current query
entity-name : name of the sub-entity, that should be build(example : parent)"
  [query table-name table-alias entity-name]
  (let [current-column-number
        (or (query :current-column-number) 0)

        table-metadata
        ((init-metadata) (keyword table-name))

        table-columns
        (map-indexed
         (fn[index ent]
           [(as-str "\"" table-alias "\"" "." "\"" ent "\"")
            (str "id_" (+ current-column-number index))])
         table-metadata)

        nested-entity-name
        (conj (vec (get query :processing-level)) entity-name)

        reverse-tranform
        (apply merge (map (fn[real [_ generated]]
                            {(keyword generated) real})
                          table-metadata table-columns))]
    (->
     query
     (assoc :current-column-number (+ current-column-number (count table-metadata)))
     (update-in [:fields] concat table-columns)
     (post-query (partial map (partial process-results {:entity-name nested-entity-name
                                                        :rename-keys reverse-tranform
                                                        :keys (keys reverse-tranform)}))))))

(defn- resolve-name-clash
  "Return new alias for table, that will not conflicts with already used joined tables in this query.
Using this construction you can join multiple tables in the same query.

Algorithm:
1) check 'from' list of query - if table is found, then generate alias by appending '1' to name
2) check if query contains duplicates number for given table name, if so - generate name wich 1+ to previous number
3) if table not found in 'from' list and not being processed - return name 'as is'(for simplicity) and set number of duplicates to 1"
  [query name]
  (let [from-tables (map :table (:from query)) ;get tables that can't be covered by interceptor
        search-path [:join-entities-duplicates name]
        used-table-names (or
                          (get-in query search-path) ;we already processed this table(1..)
                          (and (some #{name} from-tables) 1) ;not processed, but have in 'from'(1..)
                          )]
    {:query (assoc-in query search-path (inc (or used-table-names 0)))
     :alias (str name used-table-names)}))

(defn- get-resolved-name
  "Should be used when we need to use key that reference previously joined table. If this table
was used first time - then name will return as is(dublicates=1), otherwise it will returns previously
choosed duplicate number(dublicate--)"
  [query name]
  (let [current-index (dec (or (get-in query [:join-entities-duplicates name]) 1))]
    (if (= current-index 0)
      name
      (str name current-index))))

(def wrap #(str "\"" % "\""))

(defn- prepare-fkey[key table-name alias]
  {:korma.sql.utils/generated
   (strings/replace (:korma.sql.utils/generated key) (wrap table-name) (wrap alias))})

(defn- prepare-pkey[key query]
  (let [key (:korma.sql.utils/generated key)
        [table field] (.split key "\\.")]
    {:korma.sql.utils/generated
     (str (wrap (get-resolved-name query (.replaceAll table "\"" ""))) "." field)}))

(defn- one-to-one-processor[query {:keys [pk fk rel-type]} sub-ent used-entity-name]
  (let [table-name (:table sub-ent)
        [pk fk] (if (= rel-type :has-one) [pk fk] [fk pk])
        pk (prepare-pkey pk query)
        {:keys [query alias]} (resolve-name-clash query table-name)
        fk (prepare-fkey fk table-name alias)
        query (join query [table-name alias] (= pk fk))]
    (add-one-to-one-processor query table-name alias used-entity-name)))

(defn- one-to-one-interceptor [f & [query sub-ent func]]
  (let [rel (get-rel (:ent query) sub-ent)]
    (cond
      (#{:has-one :belongs-to} (:rel-type rel))
      (let [used-entity-name
            (or (:entity rel)
                (keyword (:table sub-ent)))

            ;; process current level of one-to-one relation
            first-level
            (one-to-one-processor query rel sub-ent used-entity-name)

            ;; save previous :processing-level
            previous-processing-level
            (get first-level :processing-level)

            ;; set current processing level
            current-processing-level
            (conj (vec previous-processing-level) used-entity-name)

            ;; set sub entity as main entity of query and process it throught
            ;; rest of modificators
            second-level
            (func (-> first-level
                      (assoc-in [:ent] sub-ent)
                      (assoc-in [:processing-level] current-processing-level)))

            ;; restore all parameters back
            third-level
            (-> second-level
                (assoc-in [:ent] (:ent query))
                (assoc-in [:processing-level] previous-processing-level)
                (post-query #(if-let [trans (seq (:transforms sub-ent))]
                               (let [trans-fn (apply comp trans)]
                                 (map (fn[ent]
                                        (if (get-in ent current-processing-level)
                                          (update-in
                                           ent
                                           current-processing-level
                                           trans-fn)
                                          ent)) %))
                               %)))]
        third-level)

      :else
      (f query sub-ent func))))

(add-hook #'korma.core/with* #'one-to-one-interceptor)
