(ns gd.utils.sms
  (:refer-clojure :exclude [extend])
  (:use gd.log
        clj-time.core
        gd.model.context
        clojure.test
        gd.utils.common
        hiccup.core)
  (:require [noir.options :as options]
            [clj-time.coerce :as tm]
            [gd.utils.security :as sec]
            [clj-http.client :as client])
  (:import (java.util.concurrent Executors)))

(def send-sms-thread-pool (Executors/newFixedThreadPool 1))

;; TODO : make global constraint for sms count in HOUR - this may help from errors

(def ^:private login "daite_dve")
(def ^:private password "&bh1Tyqy")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SOAP sender

(def auth
"<?xml version=\"1.0\" encoding=\"utf-8\"?>
<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
<soap:Body>
<Auth xmlns=\"http://turbosms.in.ua/api/Turbo\">
<login>daite_dve</login>
<password>&amp;bh1Tyqy</password>
</Auth>
</soap:Body>
</soap:Envelope>")

(defn sms-xml [number text] (str
"<?xml version=\"1.0\" encoding=\"utf-8\"?>
<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
               xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
               xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
<soap:Body>
<SendSMS xmlns=\"http://turbosms.in.ua/api/Turbo\">
<sender>bono.in.ua</sender>
<destination>" number "</destination>
<text>" text "</text>
</SendSMS>
</soap:Body>
</soap:Envelope>"))

;request body for auth test, checks credits amount
(def test-auth-body
  (str "<?xml version=\"1.0\" encoding=\"utf-8\"?>
        <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
                       xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
                       xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
        <soap:Body>
        <GetCreditBalance xmlns=\"http://turbosms.in.ua/api/Turbo\">
        </GetCreditBalance>
        </soap:Body></soap:Envelope>"))

(defn soap-request [number text]
  (let [cookies (:cookies (client/post "http://turbosms.in.ua/api/soap.html"
                                         {"Content-Type" "text/xml"
                                          :body auth
                                          :throw-entire-message? true
                                          :as :utf-8}))]
    (prn (client/post "http://turbosms.in.ua/api/soap.html"
                      {:headers {"Content-Type" "text/xml"
                                 "Cookie" (str "PHPSESSID=" (:value (get cookies "PHPSESSID")))}
                       :body (sms-xml number text)
                       :throw-entire-message? true
                       :as :utf-8}))))

(defn- source[]
  (context-info "dayte-dve" :sms :name))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Constraints
;; TODO : restore constraints for sms(per system and per user)
(defn- check-last-time-sent-messages[]
  0)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main api
(defn normalize-number[number]
  (let [number (clojure.string/replace number #"[\( \) \- \_]" "")
        res (if (.startsWith number "+38")
              number
              (if (.startsWith number "38")
                (str "+" number)
              (if (.startsWith number "+0")
                (str "+38" (clojure.string/replace-first number #"\+" ""))
                (str "+38" number))))]
    (when-not (re-find #"\+380[0-9]{9}" res)
      (violation "Attempt to send sms to invalid number" number)
      #_(throwf "Invalid number")number)
    res))

(deftest number-test
  (is (= "+380509790590" (normalize-number "0509790590")))
  (is (= "+380509790590" (normalize-number "+380509790590")))
  (is (= "+380509790590" (normalize-number "+38(050)979-05-90")))
  (is (= "+380509790590" (normalize-number "+05(097)905-90__")))
  (is (thrown-with-msg?
        Exception #"Invalid number"
        (normalize-number "1509790590"))))

(defn send-sms[number text]
  (let [number (normalize-number number)]
    (info "Sending sms" number (source) text)
    (if (< (count text) 160)
      (if (options/dev-mode?)
        (info "Skip SMS sending - dev-mode enabled")
        (if (sec/logged)
          (in-thread-pool send-sms-thread-pool #(soap-request number text))
          (violation "Attempt to send sms without security context")))
      (violation "Refuse to send SMS - it will span on more then 2 real sms" text))))

(defn send-sms-unsafe
  "Only for test purposes!!!"
  [number text]
  (warn "Sending UNSAFE sms" number text)
  (in-thread-pool send-sms-thread-pool #(soap-request (normalize-number number) text)))

(run-tests)
