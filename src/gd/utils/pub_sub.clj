(ns gd.utils.pub_sub
  (:refer-clojure :exclude [extend])
  (:use [monger.conversion :only [from-db-object]]
        [robert.bruce :only [try-try-again]]
        clojure.test
        gd.model.context
        gd.log
        gd.utils.db)
  (:require
   monger.joda-time
   [monger.collection :as mc]
   [monger.conversion :as mcon])
  (:import [com.mongodb WriteConcern MapReduceCommand$OutputType Bytes]
           [java.util.concurrent Executors]))

(defonce existing-subscribers (atom #{}))
(defonce processing-pools (atom {}))

(defn- process-single-message
  "Single processing step. "
  [action collection x]
  (try
    (let [{:keys [context value]} (from-db-object x true)]
      (deserialize-context (read-string context)
        (action (read-string value))))
    (catch Exception e
      (error "Problem while processing notification"
             collection
             (:value (from-db-object x true)))
      (error e)))
  (mc/update-by-id collection (get x "_id") {:processed true}))

(defn process-collection
  "Function that fetch messages from queue."
  [collection action]
  (info "Add subscriber" collection)
  (doseq [x (->
             (mc/find collection {:processed false})
             (.addOption Bytes/QUERYOPTION_TAILABLE)
             (.addOption Bytes/QUERYOPTION_AWAITDATA)
             (.addOption Bytes/QUERYOPTION_NOTIMEOUT))]
    (if (get x "__skip")
      (mc/update-by-id collection (get x "_id") {:processed true})
      (let [exec (fn[](process-single-message action collection x))]
        (if-let [pool (get @processing-pools collection)]
          (in-thread-pool pool exec)
          (exec))))))

(defn create-subscriber[collection action & {:keys [threads-num] :or {threads-num 1}}]
  (when-not (@existing-subscribers collection)
    (swap! existing-subscribers conj collection)
    (when (> threads-num 1)
      (swap! processing-pools assoc collection (Executors/newFixedThreadPool threads-num)))
    (doto (new Thread (fn[]
                        (try
                          (while true
                            (try-try-again {:sleep 1000
                                            :tries :unlimited
                                            :catch [Exception]
                                            :error-hook (fn[e] (println "Stale subscription cursor" e))}
                                           #(process-collection collection action))
                            (info "Subscriber exits without exception - repeat"))
                          (catch Throwable e
                            (error e))
                          (finally
                            (info "Remove subscriber" collection)
                            (swap! existing-subscribers disj collection)))))
      (. setName (str "subscriber-" collection))
      (. start))))

(defn create-publisher[collection size]
  (ensure-mongo-collection collection
                           {:capped true :size (* size 1024 1024)}
                           [:processed])
  (when (zero? (mc/count collection))
    (mc/insert collection
      {:processed false :__skip true}
      WriteConcern/SAFE)))

(defn publish[collection data]
  (mc/insert collection
    {:value (binding [*print-dup* true]
              (pr-str data))
     :context (binding [*print-dup* true]
                (pr-str (serialize-context)))
     :processed false}
    WriteConcern/NONE))

(defn clear-queue[collection size]
  (mc/drop collection)
  (create-publisher collection size))

(defn queue-size[collection]
  (mc/count collection {:processed false}))

(defn view-queue[collection]
  (mc/find-maps collection {:processed false}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tests
(def pub-sub-test "pub_sub_test")

(deftest cache-test
  (mc/drop pub-sub-test)

  (swap! existing-subscribers disj pub-sub-test)
  (create-publisher pub-sub-test 16)
  (create-subscriber pub-sub-test prn)
  (publish pub-sub-test {:a (sql-now-timestamp)}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Test runner
(run-tests)
