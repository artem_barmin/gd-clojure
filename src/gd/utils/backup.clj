(ns gd.utils.backup
  (:use
   gd.utils.mail
   [clj-time.core :exclude (extend)]
   clj-time.format
   [robert.bruce :only [try-try-again]]
   [clojure.java.shell]
   gd.utils.common
   gd.log
   clj-time.coerce
   clojure.data
   [clojure.string :exclude (replace reverse)])
  (:require [overtone.at-at :as at])
  (:import [com.github.sardine SardineFactory]
           (java.io StringWriter PrintWriter)
           [java.io InputStream]))

(defonce backup-execution-pool (at/mk-pool :cpu-count 1))
(defonce sardine (SardineFactory/begin "dayte-dve" "Mig(Uwki3" nil))
(defonce base "https://webdav.yandex.ru/")

(defn with-base
  "Prepends the baseurl to the given url."
  [url]
  (str base url))

(defn create-directory[path]
  (let [splitted (remove empty? (.split path "/"))]
    (doseq [dir (remove empty? (map-indexed take (cons [] (replicate (count splitted) splitted))))]
      (let [path (with-base (join "/" dir))]
        (when-not (.exists sardine path)
          (info "Create directory" path)
          (.createDirectory sardine path))))))

(defn put
  "Sends a file to a DAV server."
  [url file]
  (info "Save file" url)
  (.put sardine (with-base url) (clojure.java.io/input-stream file)))

(defn list-dirs[url]
  (map :path (filter :directory (map bean (.list sardine (with-base url) 1)))))

(defn list-files[url]
  (map :name (remove :directory (map bean (.list sardine (with-base url) 1)))))

(def try-repeat (partial try-try-again {:sleep 1000
                                        :tries 50
                                        :error-hook (fn[e] (println "Backup error" e))
                                        :catch [java.lang.Throwable]}))

(defn save-images[]
  (let [images-base "resources/public/img/uploaded/original/"
        buckets (group-by (fn[s] (subs s 0 2))
                          (->>
                           (file-seq (new java.io.File images-base))
                           (remove (fn[f] (.isDirectory f)))
                           (map (fn[s] (.getName s)))))
        counter (atom 0)
        existing-directories (set (list-dirs "/img/"))]
    (parallel
     (fn[[bucket files]]
       (try-repeat (fn[]
                     (let [dir (str "/img/" bucket "/")]
                       (when-not (existing-directories dir)
                         (create-directory dir))
                       (let [[_ new saved] (diff (set (list-files dir)) (set files))]
                         (doseq [file new]
                           (put (str dir file) (str images-base file))))
                       (info "Bucket processed" bucket (swap! counter inc) "of" (count buckets))))))
     buckets 4)))

(defn save-db[db-name]
  (let [name
        (str "dump-" db-name "-" (unparse (formatter "dd-MM-yyyy") (now)) ".backup")

        file
        (str "/home/webserver/pg-dumps/" name)

        {:keys [exit out err] :as res}
        (with-sh-env {:PGPASSWORD "1q%!@GSasd!@G"}
          (sh "pg_dump"
              "--host" "localhost"
              "--port" "5432"
              "--username" "postgres"
              "--format" "custom"
              "--blobs"
              "--file" file
              db-name))

        size
        (float (/ (.length (new java.io.File file)) (* 1024 1024)))]
    (if (zero? exit)
      (do (try-repeat (fn[] (put (str "/pg_dumps/" name) file)))
          (info "Database backup was successfull" db-name name "with size:" size)
          size)
      (throwf "Problem while processing backup : %s" res))))

(defn- format-error[ex]
  (let [writer (new StringWriter)]
    (.printStackTrace ex (new PrintWriter writer))
    (str "\n" (.toString writer))))

(defonce scheduled (at/every (* 1000 60 60 24)
                             (fn[]
                               (try
                                 (let [size (save-db "group_deals")]
                                   (send-mail "artem.barmin@gmail.com"
                                              (str "DB backup sucessfully completed " size "MB") "OK"))
                                 (catch Throwable e
                                   (error e)
                                   (send-mail "artem.barmin@gmail.com"
                                              "URGENT DB backup problem : group_deals"
                                              (format-error e))))
                               (try
                                 (save-images)
                                 (send-mail "artem.barmin@gmail.com" "Images backups sucessfully completed" "OK")
                                 (catch Throwable e
                                   (error e)
                                   (send-mail "artem.barmin@gmail.com"
                                              "URGENT Images backup problem"
                                              (format-error e))))
                               (try
                                 (sh "java" "-jar" (str (clojure.string/replace  ((sh "pwd") :out) #"\n" "")  "/resources/tools/updatepochta/UpdateNovayaPochraList.jar"))
                                 (catch Throwable e
                                   (error e)
                                   (prn (format-error e)))))
                             backup-execution-pool
                             :initial-delay (in-msecs (interval (now) (->
                                                                       (today-at-midnight)
                                                                       to-date-time
                                                                       (plus (days 1))
                                                                       (plus (hours 4)))))))

(info "Backup scheduled")
