(ns gd.utils.classmates
  (:use noir.request
        gd.utils.external-auth
        clj-time.core
        digest
        clj-time.coerce
        clojure.data.json
        gd.utils.common
        gd.model.model)
  (:refer-clojure :exclude [extend])
  (:require [noir.cookies :as cookies]
            [clojure.string :as string]
            [clj-http.client :as client]))

(def public "CBALLNIGABABABABA")
(def secret "F90C7B63A294D1E0E41A522B")
(def client "93108736")

(defn check-credentials[code invite]
  (let [token
        (:access_token
         (read-json
          (:body (client/post
                  "http://api.odnoklassniki.ru/oauth/token.do"
                  {:headers {"Content-type" "application/x-www-form-urlencoded"}
                   :body (client/generate-query-string
                          {:code code
                           :redirect_uri (str "http://dayte-dve.com.ua/login?invite=" invite)
                           :grant_type "authorization_code"
                           :client_id client
                           :client_secret secret})}))))

        {:keys [uid name gender birthday pic_2] :as data}
        (read-json
         (:body
          (client/get "http://api.odnoklassniki.ru/fb.do"
                      {:query-params
                       {"method" "users.getCurrentUser"
                        "access_token" token
                        "application_key" public
                        "sig" (md5 (str
                                    "application_key=" public
                                    "method=users.getCurrentUser"
                                    (md5 (str token secret))))}})))]
    (invite:use-external
     invite
     (user:authorize-external
      uid
      :classmates
      {:real-name name
       :birthdate (process-bdate birthday #"-" [:year :month :day])
       :info-photo {:path (when-not (re-find #"stub.*gif" pic_2)
                            (.replace pic_2 "photoType=2" "photoType=3"))}
       :sex (tranform-sex "male" "female" gender)}))))
