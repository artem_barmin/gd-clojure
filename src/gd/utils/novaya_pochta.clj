(ns gd.utils.novaya-pochta
  (:use cheshire.core
        gd.utils.common)
  (:require [clj-http.client :as client]))

(def url "https://api.novaposhta.ua/v2.0/json/")
(def api-key "ee66cf036f7c7395cb42541b856709cf")

(defn api-body [method properties]
  {:apiKey api-key
     :modelName "Address"
     :calledMethod method
     :methodProperties properties})

(defn np-api [method properties]
    (get
      (->>
        {:method       :post
         :url          url
         :body         (generate-string (api-body method properties))
         :content-type :json}
        client/request
        :body
        parse-string) "data"))

(def zone-translate
  {"Хмельницька" "Хмельницкая область"
   "Вінницька" "Винницкая область"
   "Львівська" "Львовская область"
   "Житомирська" "Житомирская область"
   "Тернопільська" "Тернопольская область"
   "Волинська" "Волынская область"
   "Полтавська" "Полтавская область"
   "Чернігівська" "Черниговская область"
   "Київська" "Киевская область"
   "Миколаївська" "Николаевская область"
   "Закарпатська" "Закарпатская область"
   "Рівненська" "Ровненская область"
   "Донецька" "Донецкая область"
   "АРК" "Крым"
   "Сумська" "Сумская область"
   "Черкаська" "Черкасская область"
   "Харківська" "Харьковская область"
   "Івано-Франківська" "Ивано-Франковская область"
   "Чернівецька" "Черновицкая область"
   "Запорізька" "Запорожская область"
   "Кіровоградська" "Кировоградская область"
   "Херсонська" "Херсонская область"
   "Дніпропетровська" "Днепропетровская область"
   "Луганська" "Луганская область"
   "Одеська" "Одесская область"})

(defn get-areas []
  (let [areas (np-api "getAreas" nil)
        areas (mreduce (fn [{:strs [Description Ref]}]
                      {Ref (zone-translate Description)}) areas)]
  areas))

(defn get-cities []
  (let [cities  (np-api "getCities" nil)
        cities (mapv (fn [{:strs [DescriptionRu Ref Area]}] [Area [DescriptionRu Ref]]) cities)]
    cities))

(defn get-warehouses [city-key]
  (let [warehouses (np-api "getWarehouses" {:CityRef city-key})
        warehouses (mapv (fn [{:strs [DescriptionRu]}] DescriptionRu) warehouses)]
    warehouses))

(defn fill-dictionary []
  (let [areas (get-areas)
        cities (get-cities)
        i (atom 0)
        cities-count (count cities)
        cities-with-warehouses
        (reduce (partial merge-with merge)
          (map (fn [[area-key [city-name city-key]]]
                                        (prn (str (swap! i inc) "/" cities-count))
                                        {(get areas area-key) {city-name (get-warehouses city-key)}}) cities))]
    (spit "resources/tools/novya-pochta-new.txt" (generate-string cities-with-warehouses))))

(defn replace-content []
  (spit "resources/tools/novya-pochta.txt" (slurp "resources/tools/novya-pochta-new.txt")))