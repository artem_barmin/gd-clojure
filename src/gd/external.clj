(require 'clojure.test)

(alter-var-root #'clojure.test/run-tests (fn[_] (fn[])))
(alter-var-root #'clojure.test/run-all-tests (fn[_] (fn[])))

(ns gd.external
  (:use [clojure.java.shell :only (sh)]
        gd.utils.logger
        gd.server.middleware
        gd.utils.common
        gd.utils.web
        gd.utils.db
        gd.log
        gd.parsers.common
        gd.utils.backup
        gd.model.migrations.migrations
        lobos.connectivity
        org.httpkit.server)
  (:require [gd.model.model])
  (:require [clojure.tools.nrepl.server :as nrepl])
  (:require [noir.server :as server])
  (:require [gd.api.opencart.rest]))


(def megabyte (* 1024 1024))

(defn -main []
  (alter-var-root #'clojure.core/*out* (constantly (new java.io.PrintWriter *out*)))

  (nrepl/start-server :port 7888)

  ;; Migrate DB
  (info "Main DB migrations")
  (migrate-schema)
  (migrate-metamodel)

  (info "Demo DB migrations")
  (gd.model.model/with-demo
    (with-named-connection :demo
      (fn[](migrate-schema))))

  ;; clean up packed directory
  (delete-file-recursively "resources/public/packed/img" true)

  ;; Load views, also can generate CSS files
  (server/load-views "src/gd/views/")

  ;; Generate CSS sprites
  (println
   (clojure.string/join
    "\n"
    (filter (partial re-find #"Writing CSS to")
            (let [{:keys [out err exit]}
                  (sh "resources/tools/smartsprites/smartsprites.sh"
                      "--root-dir-path"
                      "resources/public"
                      "--document-root-dir-path"
                      "resources/public")]
              (when (= exit 1)
                (println out)
                (println err)
                (throwf "Problems while generating CSS sprites"))
              (clojure.string/split out #"\n")))))

  ;; Minify CSS and JS scripts
  (minify-all)

  ;; run web server
  (run-server (server/gen-handler {:mode :prod :ns 'gd
                                   :session-store (session-store-extended)
                                   :session-cookie-attrs {:max-age (* 30 24 60 60)}})
              {:port 8080
               :thread (* 2 (.availableProcessors (Runtime/getRuntime)))
               :queue-size 20480
               :worker-name-prefix "worker-"
               :max-line 4096
               :max-body (* 2 megabyte)})

  (println "Server started in"
           (/ (.getUptime (java.lang.management.ManagementFactory/getRuntimeMXBean)) 1000.0)
           "sec")

  ;; restore all scheduled events
  (defonce restore-at-start (gd.model.model/schedule:restore-timers)))
