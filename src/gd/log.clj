(ns gd.log
  (:refer-clojure :exclude [extend])
  (:use clj-time.core
        gd.utils.db
        [clojure.algo.generic.functor :only (fmap)]
        [monger.conversion :only [from-db-object]]
        monger.operators
        clj-stacktrace.core
        com.reasonr.scriptjure
        hiccup.core
        gd.utils.profiler
        gd.utils.common)
  (:require [taoensso.timbre :as log]
            [clj-time.format :as format]
            [clojure.string :as str]
            [monger.collection :as mc]
            [clj-time.coerce :as coerce])
  (:import (org.joda.time DateTimeZone))
  (:import (java.io StringWriter PrintWriter))
  (:import [com.mongodb WriteConcern MapReduceCommand$OutputType]))

(defmacro info[& args]
  `(log/info ~@args))

(defmacro debug[& args]
  `(log/debug ~@args))

(defmacro trace[& args]
  `(log/trace ~@args))

(defmacro fatal[& args]
  `(log/fatal ~@args))

(defmacro warn[& args]
  `(log/warn ~@args))

(defmacro error[& args]
  `(log/error ~@args))

(defmacro violation[& args]
  `(log/fatal ~@args))

(def custom-formatter (format/formatter "dd MMM HH:mm:ss.SSS" (DateTimeZone/getDefault)))

(def ^:dynamic *current-session* (atom nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Mongo DB connector
(def logging "logging")

(def date-formatter
  (-> (format/formatter "dd-MM-yyyy")
      (format/with-zone (DateTimeZone/getDefault))))

(ensure-mongo-collection
 logging {:capped true :size (* 512 1024 1024)}
 [:source :timestamp :del :session-id :source.id :uri :result :host])

(defn- format-actions[actions]
  (if-not (empty? actions)
    (str "\n\t"
         (->>
          actions
          reverse
          (map (fn[[_ level & rest]] (str (.toUpperCase (name level)) " " (str/join " " rest))) )
          (str/join "\n\t")))
    ""))

(defn- format-error[ex]
  (let [writer (new StringWriter)]
    (.printStackTrace (deserialize ex) (new PrintWriter writer))
    (str "\n" (.toString writer))))

(defonce log-listener (agent nil :error-mode :continue))

(defn- persist-log-message[params]
  ;; write to mongo
  (try (mc/insert logging params WriteConcern/NONE) (catch Exception e))

  ;; publish
  (send-off log-listener (constantly params))

  ;; plain text apenders
  (let [{:keys [timestamp request-method service uri source params actions result exception time status
                sql-time sql-count web-time components user-agent host-info host-id]}
        params

        timestamp
        (format/unparse custom-formatter timestamp)

        log-file-time
        (format/unparse date-formatter (coerce/from-date (new java.util.Date)))

        robot
        (and user-agent (second (re-find #"(?iu)(google|yandex|up|bing).*bot" user-agent)))

        source-message
        (cond request-method
              (if (seq (:name source))
                (str "(" (:id source) "," (:name source) ")")
                (or robot "USER"))

              (not request-method)
              (str "(" (nth actions 2) ")"))

        supplier-message
        (str "(sup:" (:supplier host-info) ",host:" host-id ")")

        log-message
        (str
         (if request-method
           (str/join
            \space
            [timestamp
             (name request-method)
             (str time "ms") "(web:" web-time "/ db:" sql-time "/ db-cnt:" sql-count ")"
             source-message
             supplier-message
             (or service uri)
             (or components "")
             (or params "")
             (format-actions actions)
             (if (= :error result)
               (if exception (format-error exception) (or status "NO ROUTE")))])
           (let [[_ level ns & message] actions]
             (str/join
              \space
              (flatten
               (concat
                [timestamp "system" (.toUpperCase (name level)) source-message]
                message)))))
         \newline)]

    ;; write to file
    (if robot
      (spit (str "log/" log-file-time "-robots")
            log-message
            :append true)
      (spit (str "log/" log-file-time)
            log-message
            :append true))

    ;; write to stdout
    (when-not robot
      (print log-message))
    (flush)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Web Request log capture
(def ^{:dynamic true} logging-context nil)

;; TODO : remove password from params, to forbid logging
(defn- remove-file[obj]
  (cond (map? obj)
        (dissoc obj :tempfile)

        (sequential? obj)
        (map remove-file obj)

        true
        obj))

(defn- web-logger[handler {:keys [params uri headers remote-addr components host-info host-id] :as request}]
  (binding [logging-context (atom [])]
    (let [request-params (select-keys request [:uri :request-method])

          params (when-not (empty? params) (fmap remove-file params))

          main-params (merge
                       (select-keys params [:service])
                       request-params
                       {:type (if (= uri "/service") :service :web-request)
                        :host (get headers "host")
                        :components components
                        :timestamp (now)
                        :host-info (select-keys host-info [:supplier :context :context-filter])
                        :host-id host-id
                        :params (dissoc params :service)
                        :remote-addr (or (get headers "X-Real-IP") remote-addr)
                        :referer (get headers "referer")
                        :user-agent (get headers "user-agent")
                        :del false})]
      (try
        (let [start (System/currentTimeMillis)
              res (handler request)]
          (let [sql-time (get-current-sql-time)
                sql-count (get-current-sql-count)
                whole-time (- (System/currentTimeMillis) start)
                user-agent (:user-agent main-params)
                session-id (or (:_id @*current-session*)
                               (let [set-session-id (when-let [headers (:headers res)] (headers "Set-Cookie"))]
                                 (when-let [cookie-to-set
                                            (first
                                             (filter (partial re-find #"PHPSESSID") set-session-id))]
                                   (second (.split cookie-to-set "[=;]")))))
                params-after-handler
                {:time whole-time
                 :sql-time sql-time
                 :sql-count sql-count
                 :source (select-keys (:user @*current-session*) [:id :name :auth-type])
                 :session-id (when-not (and user-agent (re-find #"(?iu)(google|yandex|up|bing).*bot" user-agent)) session-id)
                 :web-time (- whole-time sql-time)}]
            (if (#{200 302} (:status res)) ;Ok and redirect
              (persist-log-message (merge (when (seq @logging-context) {:actions @logging-context})
                                          main-params
                                          params-after-handler
                                          {:result :success}))
              (persist-log-message (merge (when (seq @logging-context) {:actions @logging-context})
                                          main-params
                                          params-after-handler
                                          {:result :error
                                           :status (:status res)}))))
          res)
        (catch Throwable e
          (try (persist-log-message (merge (when (seq @logging-context) {:actions @logging-context})
                                           main-params
                                           {:result :error
                                            :source (select-keys (:user @*current-session*) [:id :name :auth-type])
                                            :exception (serialize e)}))
               (catch Throwable e))
          (throw e))))))

(defn wrap-logging
  [handler]
  (fn [request]
    (if (reduce #(or %1 %2) (map #(.startsWith (:uri request) %) ["/js/" "/css/" "/fonts/" "/img/" "/utils/" "/logger" "/packed/"]))
      (handler request)
      (web-logger handler request))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Logger config
(log/set-config!
 [:appenders :mongodb-appender]
 {:doc       "MongoDB appender"
  :min-level nil
  :enabled?  true
  :async?    false
  :max-message-per-msecs nil
  :fn (fn [{:keys [ap-config level prefix message more] :as args}]
        (let [{:keys [timestamp hostname ns]} prefix
              message-content [timestamp level ns message more]]
          (if (thread-bound? #'logging-context)
            (swap! logging-context conj message-content)
            (persist-log-message
             {:type :system
              :source :system
              :actions (remove (partial instance? Throwable) message-content)
              :timestamp (now)}))))})

;; disable default stdout writer - we will write from mongo appender
(log/set-config!
 [:appenders :standard-out]
 {:min-level nil
  :enabled?  false})

(log/set-config! [:prefix-fn] identity)
