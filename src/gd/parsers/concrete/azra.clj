(ns gd.parsers.concrete.azra
  (:use gd.utils.common)
  (:use gd.parsers.llapi)
  (:use gd.parsers.hlapi)
  (:use gd.utils.web)
  (:use korma.core)
  (:use [gd.model.model :exclude (stock)])
  (:require [clojure.string :as str])
  (:require [net.cgrand.enlive-html :as enlive]))

(def categories nil)

(defn resolve-category-by-name[name]
  (filter
   (fn[{cat-name :name key :key}]
     (let [name (.toLowerCase name)
           cat-name (.toLowerCase cat-name)]
       (and (re-find #"stocks-.*" key)
            (= cat-name name))))
   categories))

(defn resolve-categories[breadcrumbs]
  (let [current (resolve-category-by-name (last breadcrumbs))
        global-type (cond
                      (some (re-parse #"(?ium)муж") breadcrumbs) "Мужчинам"
                      (some (re-parse #"(?ium)жен") breadcrumbs) "Женщинам")]
    (some (fn[{:keys [id]}]
            (let [parents (map :name (category:parents id))]
              (if global-type
                (and (some (partial = global-type) parents) id)
                id)))
          current)))

(defn process-stock[{:keys [name description params category misc] :as stock}]
  (let [sizes-info (reduce merge
                           (map (fn[s]
                                  {(or (attr :data-id s)
                                       (attr :data-type (first description)))
                                   (text s)})
                                (:size params)))
        name-words (.split name "[ ]+")
        [name color] (split-with (comp not (partial re-find #"^[А-ЯA-Z]?-?[0-9-/]+[А-ЯA-Z]?$")) name-words)
        [name color] (map (partial str/join " ")
                          (if (empty? color)
                            [(take 1 name-words) (drop 1 name-words)]
                            [(concat name (take 1 color)) (drop 1 color)]))
        {:keys [price params]} (params-price-to-difference
                                (map (fn[option]
                                       (let [sizes-table (attr :data-id option)
                                             price (attr :data-cost option)]
                                         [(.replaceAll (text option) "\\(.*\\)" "") (parsei price) sizes-table]))
                                     (:size params)))
        price (if (zero? price) (:price stock) price)]
    (->
     stock
     (assoc-in [:name] "")
     (assoc-in [:category] (resolve-categories (map text category)))
     (assoc-in [:price] (* 1.8 price))
     (assoc-in [:params :size] (map (comp vec (partial take 2)) params))
     (assoc-in [:description] (str/join "\n"
                                        (grep-take
                                         (render-html-lines (apply str (enlive/emit* (first description))))
                                         #"Замеры" #"Купить" #"Размер" #"ПОГ")))
     (assoc-in [:misc :sizes-info] sizes-info)
     (assoc-in [:params [:color false]] [(prepare-str (-> color
                                                          (.replaceAll "\\(.*\\)" "")
                                                          (.replaceAll "\\.[^.]+" "")
                                                          (.replaceAll "\".*\"" "")))])
     (update-in [:params [:brand false]] (partial map prepare-str))
     (dissoc-in [:params :color]))))
