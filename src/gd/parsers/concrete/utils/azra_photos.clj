(ns gd.utils.azra-photos
  (:use gd.utils.common
        [clojure.java.shell :only (sh)]
        [clojure.java.io :as io])
  (import javax.imageio.ImageIO))

(defn process-photo[url]
  (println url)
  (let [img (ImageIO/read (io/as-file url))
        [w h] [(.getWidth img) (.getHeight img)]
        cropped-raw-info (:out (sh "convert" url
                                   "-crop" (str w "x" 3000 "+0+0")
                                   "-median" "3"
                                   "-fuzz" "20%"
                                   "-trim" "info:"))]
    (let [[res-w res-h off-x off-y]
          (->>
           cropped-raw-info
           (re-find #"JPEG ([0-9]+)x([0-9]+).*\+([0-9]+)\+([0-9]+)")
           (drop 1)
           (map parse-int))

          border 50]
      (sh "mogrify"
          "-crop" (str (+ res-w (* 2 border)) "x" (+ res-h (- h 3000))
                       "+" (- off-x border) "+" (- off-y border))
          "-thumbnail" "815x1229!"
          "-strip"
          "-quality" "80"
          url))))

(defn process-photo-resize[url]
  (let [img (ImageIO/read (io/as-file url))
        [w h] [(.getWidth img) (.getHeight img)]]
    (when (and (not= [w h] [815 1229]) (> (float (/ h w)) 1))
      (prn (.getAbsolutePath url))
      (sh "mogrify"
          "-gravity" "North"
          "-thumbnail" "815x1229!^"
          "-strip"
          "-normalize"
          "-quality" "85"
          (.getAbsolutePath url)))))

(defn fix-orient[url]
  (prn (.getAbsolutePath url))
  (sh "mogrify"
      "-auto-orient"
      (.getAbsolutePath url)))

;; (parallel
;;  process-photo-resize
;;  (filter
;;   (fn[file](re-find #"(?ium)jpg$" (.getAbsolutePath file)))
;;   (org.apache.commons.io.FileUtils/listFiles
;;    (io/as-file "/home/artem/Downloads/Новая папка (3)")
;;    nil
;;    true)))
