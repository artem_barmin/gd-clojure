(ns gd.parsers.concrete.bono-news
  (:use gd.parsers.concrete.utils.azra-categories)
  (:use gd.parsers.concrete.azra)
  (:use gd.utils.web)
  (:use gd.utils.common)
  (:use gd.model.context)
  (:use [korma.core :only (insert values select where)])
  (:use [gd.model.model :exclude (stock)])
  (:use gd.parsers.llapi)
  (:use [gd.parsers.common :only (emit collect visited-pages execute page)])
  (:use gd.parsers.hlapi)
  (:require [clojure.string :as str])
  (:require [clojure.zip :as zip]))

(set-basic-auth "bono" "OoOfcr4I")

(start "http://bono.in.ua/")

(prod)

(page "http://bono.in.ua/backend/news/index/menu/2"
      [all-pages "a:contains(Редактировать)"]
      (doseq [page all-pages]
        (emit :stock-raw (attr :href page) (attr :href page))))

(defn parse-date[date]
  (clj-time.coerce/from-date
   (.parse (new java.text.SimpleDateFormat "dd.MM.yyyy" (new java.util.Locale "ru" "RU")) date)))

(stock [other]
    [[name] "#zag"
     [url] "#url"
     short ["#text_short" text]
     content ["#content" text]
     [date] "#datepicker"
     [seo-title] "#title"]
  (when date
    (emit :stock {:url (attr :value url)
                  :short short
                  :content content
                  :date (parse-date (attr :value date))
                  :name (attr :value name)})))

(defn html-to-bbcode[content]
  (-> content
      (.replaceAll "&lt;" "<")
      (.replaceAll "&gt;" ">")
      (.replaceAll "&.*?;" "")
      (.replaceAll "</p>" "</p>\n")
      (.replaceAll "<span[^>]*color: ([a-z]+)[^>]*>(.*?)<\\/span>" "[color=$1]$2[/color]")
      (.replaceAll "<p[^>]*center[^>]*>(.*?)<\\/p>" "[center]$1[/center]")
      (.replaceAll "<strong>(.*?)<\\/strong>" "[b]$1[/b]")
      (.replaceAll "<[^>]*>" "")
      (.replaceAll "\n[ \t]+\n" "\n\n")
      (.replaceAll "\n{2,}" "\n\n")
      (.replaceAll "\n" "[br][/br]")))

(defn remove-html[content]
  (when content
    (-> content
        (.replaceAll "&lt;" "<")
        (.replaceAll "&gt;" ">")
        (.replaceAll "<[^>]*>" "")
        (.replaceAll "&.*?;" ""))))

(collect
 :stock [items]
 (doseq [{:keys [url short content date name]} (sort-by :date items)]
   (gd.utils.db/upset site-page
                      {:url url
                       :title name
                       :type :news
                       :status :published
                       :content (html-to-bbcode content)
                       :content-short (remove-html short)
                       :time (clj-time.coerce/to-timestamp date)}
                      {:url url})))

(reset! visited-pages nil)
(reset! pages-visited 0)
(execute "bono-news" 10)
