(ns gd.parsers.concrete.test
  (:use gd.utils.common)
  (:use gd.utils.test)
  (:use gd.model.test.data_builders)
  (:use gd.parsers.llapi)
  (:use gd.parsers.hlapi))

(prod)

(start (str "http://localhost:" (property "server.port" "8080") "/")
       :categories ".top-menu [href*='/stocks/']"
       :stocks-elements ".breadcrumbs-link")

(stock-element [other]
    [name ["a" text]]
  (dotimes [i 5]
    (send-stock name other 10
                [{:path (use-image :items (inc i))}])))

(common-deal "test-parsers")
