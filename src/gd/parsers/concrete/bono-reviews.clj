(ns gd.parsers.concrete.bono-reviews
  (:use gd.parsers.concrete.utils.azra-categories)
  (:use gd.parsers.concrete.azra)
  (:use gd.utils.web)
  (:use gd.utils.common)
  (:use gd.model.context)
  (:use [korma.core :only (insert values select where)])
  (:use [gd.model.model :exclude (stock)])
  (:use gd.parsers.llapi)
  (:use [gd.parsers.common :only (emit collect visited-pages execute page)])
  (:use gd.parsers.hlapi)
  (:require [clojure.string :as str])
  (:require [clojure.zip :as zip]))

(set-basic-auth "bono" "OoOfcr4I")

(start "http://bono.in.ua/")

(prod)

(page "http://bono.in.ua/backend/gb/index/menu/8"
      [all-pages "tr:not(:contains(Нет)) a:contains(Редактировать)"]
      (doseq [page all-pages]
        (emit :stock-raw (attr :href page))))

(stock [other]
    [[name] "#name"
     [textarea] "textarea#content"]
  (emit :stock {:text (.replaceAll (text textarea) "&lt;/?p&gt;" "")
                :name (attr :value name)}))

(defn parse-date[date]
  (let [date
        (if (= date "Сегодня")
          (clj-time.core/now)
          (clj-time.coerce/from-date
           (.parse (new java.text.SimpleDateFormat "dd MMMMM" (new java.util.Locale "ru" "RU")) date)))]
    (clj-time.core/date-time 2013
                             (clj-time.core/month date)
                             (clj-time.core/day date))))

(def dates
  (map parse-date
       (list "Сегодня" "14 ноября" "9 ноября" "7 ноября" "4 ноября" "25 октября" "20 октября" "15 октября" "15 октября" "12 октября" "12 октября" "5 октября" "3 октября" "29 сентября" "18 сентября" "11 сентября" "5 сентября" "30 августа" "15 августа" "7 августа" "5 августа" "27 июля" "8 июля" "3 июля" "26 июня" "18 июня" "18 июня" "14 июня" "14 июня" "7 июня" "3 июня" "2 июня" "2 июня" "1 июня" "1 июня" "31 май" "31 май" "31 май" "31 май" "31 май" "31 май" "31 май" "31 май" "31 май" "30 май" "28 май" "26 май" "23 май" "22 май" "21 май" "18 май" "17 май" "14 май" "14 май" "7 май" "29 апреля" "29 апреля" "28 апреля" "26 апреля" "26 апреля" "24 апреля" "23 апреля" "23 апреля" "2 апреля")))

(collect
 :stock [items]
 (doseq [[date {:keys [text name]}] (map vector dates items)]
   (when (empty? (select message (where {:text (str name ":" text)})))
     (insert message (values
                      {:text (str name ":" text)
                       :opened false
                       :type :supplier-review
                       :removed false
                       :sent-time (clj-time.coerce/to-timestamp date)
                       :author nil
                       :recipient 1})))))

(reset! visited-pages nil)
(reset! pages-visited 0)
(execute "bono-quantity" 10)


