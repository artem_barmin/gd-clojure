(ns gd.parsers.concrete.bono
  (:use gd.parsers.concrete.utils.azra-categories)
  (:use gd.parsers.concrete.azra)
  (:use gd.utils.web)
  (:use gd.utils.common)
  (:use gd.model.context)
  (:use [gd.model.model :exclude (stock)])
  (:use gd.parsers.llapi)
  (:use [gd.parsers.common :only (emit collect visited-pages execute page)])
  (:use gd.parsers.hlapi)
  (:require [clojure.string :as str])
  (:require [clojure.zip :as zip]))

(set-basic-auth "bono" "OoOfcr4I")

(def other-category (:id (category:get-by-key "other")))

(start "http://bono.in.ua/" :stocks "[id=sortable] > tr a:contains(Редактировать)")

(prod)

(page "http://bono.in.ua/backend/catalog/index/page/1"
      [all-pages ".paginator_pages"]
      (dotimes [i (parsei all-pages)]
        (emit :category (str "http://bono.in.ua/backend/catalog/index/page/" i))))

(stock [other]
    [[artikul] "[id=artikul]"
     [name] "[id=name]"
     [size] "[id=size]"
     brand "[id=brand] option"
     measurements ["[id=content]" text]
     [price] "[id=cost]"
     images ".item_1:contains(Фото товара) a[href*=images]"
     category "[id=id_parent] option"]
  (let [images (download images)]
    (when (seq images)
      (emit :stock
            {:name (attr :value name)
             :art (attr :value artikul)
             :price (parsei (attr :value price))
             :size (attr :value size)
             :brand (text (find-first (attr :selected) brand))
             :breadcrumbs (get bono-categories-tree (parse-int (attr :value (find-first (attr :selected) category))))
             :measurement (when measurements
                            (map (fn[line]
                                   (if (.contains line "см")
                                     (let [[name size] (map str/trim (.split line "[:-]"))]
                                       [name (parsei size)])
                                     line))
                                 (->
                                  measurements
                                  (.replaceAll "&lt;" "<")
                                  (.replaceAll "&gt;" ">")
                                  render-html
                                  (.split "\n+"))))
             :meta-info {:url (gd.parsers.common/absolutize-url info)}
             :images images}))))

(defn merge-stocks[stocks]
  (let [{:keys [brand breadcrumbs] :as main} (first stocks)
        other (remove (re-parse #"Замеры") (filter string? (:measurement main)))

        {:keys [price params]}
        (params-price-to-difference
         (map vector (map :size stocks) (map :price stocks)))]
    (merge (dissoc main :measurement :size :art :breadcrumbs :brand)
           {:fkey_category (or (when breadcrumbs (resolve-categories breadcrumbs)) other-category)
            :params (merge {:size params}
                           (when brand {[:brand false] [[brand]]}))
            :price price
            :description (reduce merge
                                 (cons
                                  {:other (str/join "\n" other)}
                                  (map (fn[{:keys [size measurement]}]
                                         (let [measurement (filter sequential? measurement)]
                                           (when (seq measurement)
                                             {(str/trim size)
                                              {:name (map first measurement)
                                               :value (map second measurement)}})))
                                       stocks)))})))

(collect
 :stock [items]
 (let [stocks (distinct items)]
   (spit "resources/migration/stocks.txt"
         (with-out-str
           (clojure.pprint/pprint
            (reduce merge (map (fn[{:keys [name meta-info]}]
                                 {(parsei (:url meta-info)) name}) stocks)))))))

(reset! visited-pages nil)
(reset! pages-visited 0)
(execute "bono-quantity" 10)
