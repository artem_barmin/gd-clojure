(ns gd.parsers.jquery-parser
  (:use [clojure.java.shell :only (sh)])
  (:use gd.log)
  (:use gd.utils.common)
  (:use [robert.bruce :only [try-try-again]])
  (:use hiccup.core)
  (:use clojure.data.json)
  (:use clojure.walk)
  (:import (java.net Socket)
           (java.io PrintWriter InputStreamReader BufferedReader))
  (:require [net.cgrand.enlive-html :as enlive]
            [clojure.string :as string]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Request/Response processing utils
(defn- xml-str
  "Like clojure.core/str but escapes < > and &."
  [x]
  (-> x (.replace "&" "&amp;") (.replace "<" "&lt;") (.replace ">" "&gt;")))

(defn- transform-to-xml[code]
  (letfn [(node?[val]
            (and (vector? val)
                 (> (count val) 0)
                 (string? (first val))))

          (process-node[val]
            (cond (node? val)
                  (let [attr (second val)]
                    {:tag (keyword (.toLowerCase (first val)))
                     :attrs (if (map? attr) attr {})
                     :content (map process-node (drop (if (map? attr) 2 1) val))})

                  (string? val)
                  (xml-str val)

                  true
                  val))]
    [(process-node code)]))

(defn- process-selector-result[{:keys [type values selector]}]
  (case type
    "simple" (vector (mapcat transform-to-xml values))
    "composite" (map (partial map #(first (process-selector-result {:type "simple" :values %}))) values)
    "empty" [nil]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Interaction with parsing server

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Start node

(def port-to-pid (atom nil))

(defn- stream-to-string [stream]
  (with-open [writer (java.io.StringWriter.)]
    (clojure.java.io/copy stream writer)
    (.toString writer)))

(defn- start-node[node-port]
  (let [proc (.exec (Runtime/getRuntime) (str "resources/tools/envjs/start-node.sh " node-port))
        pid (do
              (.waitFor proc)
              (Long/valueOf (.replace (stream-to-string (.getInputStream proc)) "\n" "")))]
    (info "Starting parsing server" "PID" pid "PORT" node-port)
    (swap! port-to-pid assoc node-port pid)
    pid))

(declare number-of-requests)

(defn- stop-all-nodes[]
  (sh "killall" "node")
  (sh "killall" "nodejs")
  (reset! number-of-requests 0))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Round robin choosing

(def num-nodes 4)

(def max-requests-to-node 50)

(def node-locks (map (fn[i] [(+ 2000 i) (Object.)])
                     (range 1 (inc num-nodes))))

(defonce number-of-requests (atom 0))

(defonce node-selection-lock (Object.))

(defn- select-next-node-instance[]
  (locking node-selection-lock
    (let [next (rem (swap! number-of-requests inc) num-nodes)
          [port lock :as result] (nth node-locks next)]
      (when (zero? (rem @number-of-requests (* num-nodes max-requests-to-node)))
        (info "Restart after" max-requests-to-node "*" num-nodes "requests")
        (stop-all-nodes))
      result)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Api

(defn- connect-or-start[node-port]
  (try (doto (Socket. "localhost" ^Long node-port)
         (.setSoTimeout 10000))
       (catch java.net.ConnectException e
         (start-node node-port)
         (try-try-again {:sleep 100
                         :tries 100
                         :catch [java.net.ConnectException]}
                        #(Socket. "localhost" ^Long node-port)))))

(defn- execute-request[data]
  (let [[port lock] (select-next-node-instance)
        result
        (locking lock
          (try-try-again
           {:sleep 100
            :tries 5
            :catch [Exception]
            :error-hook (fn[e]
                          (print-seq "Restart all nodes" (.getMessage e))
                          (stop-all-nodes))}
           #(let [socket (connect-or-start port)
                  in (BufferedReader. (InputStreamReader. (.getInputStream socket)))
                  out (PrintWriter. (.getOutputStream socket))]
              (.println out (json-str data))
              (.flush out)
              (let [result (.readLine in)]
                (if result
                  (let [parsed (read-json result)]
                    (if (and (string? parsed) (re-find #"bad selector" parsed))
                      {:result :error}
                      (when (sequential? parsed)
                        (map! process-selector-result parsed))))
                  (throwf "Parsing server error occured"))))))]
    (if (and (map? result) (= :error (:result result)))
      (throwf "Selector syntax error")
      result)))

(defn node-request
  "Perform request to server. If it is not started - start it and wait while it
accept connections.

without args - send stop request
with args - send url and selectors
"
  ([]
     (stop-all-nodes))
  ([url selectors]
     (execute-request {:url (str "file://" url) :selectors selectors})))

(comment
  (node-request)                             ; stop server
  (node-request "http://veer-mar.com/product.jsp?pid=267&cat=15"
                ["div.header"           ;-> list of matched DOM nodes
                 ])

  "nested lists forall(a in $('div.header')){return [a.find('a'),a.find('b'),a.find('div')])}"
  (node-request "http://veer-mar.com/product.jsp?pid=267&cat=15"
                [["div.header" "a" "b" "div"]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main parsing function

(defn prepare-selector
  "Process each selector in the map. Dispatch by types of first two elements in request.
  Return vector with two elements : raw selectors that should be send to parsing server,
  and post processing functions."
  [selector]
  (cond (and (vector? selector) (map? (second selector)) (:child (second selector)))
        (let [single-res-processor (reduce comp (drop 2 selector))]
          [(cons (first selector) (:child (second selector)))
           (fn[& args] (map (partial map single-res-processor) args))])

        (vector? selector)
        [(first selector) (reduce comp (rest selector))]

        true
        [selector identity]))

(defmacro jquery-parse-html[url bindings & body]
  (let [partitioned-bindings (partition 2 bindings) ;split bindings and request
        vars (map first partitioned-bindings)       ;take variables
        requests (map second partitioned-bindings)  ;take requests
        ]
    `(let [selectors# (map prepare-selector (list ~@requests)) ;take requests
           raw-selectors# (map first selectors#)   ;take raw selectors
           post-selectors# (map second selectors#) ;post process functions
           [~@vars]
           (map! apply post-selectors# (node-request ~url raw-selectors#))]
       (do ~@body))))

(defn stop[]
  (node-request))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils
(defn texts[nodes]
  (if (and (map? nodes) (:tag nodes))
    [(enlive/text nodes)]
    (enlive/texts nodes)))

(def text (comp (partial string/join " ") texts))

(defn child[& child-selectors]
  {:child child-selectors})
