(ns gd.parsers.jsoup-parser
  (:use gd.log)
  (:use gd.utils.common)
  (:use hiccup.core)
  (:use clojure.data.json)
  (:use clojure.walk)
  (:import (org.jsoup Jsoup)
           (org.jsoup.nodes Document TextNode Element))
  (:require [net.cgrand.enlive-html :as enlive]
            [clojure.string :as string]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Additional info utils
(defn texts[nodes]
  (if (and (map? nodes) (:tag nodes))
    [(enlive/text nodes)]
    (enlive/texts nodes)))

(def text (comp (partial string/join " ") texts))

(defn child[& child-selectors]
  {:child child-selectors})

(defn jsoup-to-nested-map[el]
  (cond (instance? TextNode el)
        (.getWholeText el)

        (instance? Element el)
        (merge
         (let [content (map jsoup-to-nested-map (.childNodes el))]
           (when (seq content) {:content content}))
         {:tag (keyword (.tagName el))
          :attrs (reduce merge
                         (map (fn[att] {(keyword (.getKey att)) (.getValue att)})
                              (.asList (.attributes el))))})))

(defn process-child-selectors
  "For each parent - extract all childs and store them together."
  [doc parent childs]
  (let [parents (.select doc (name parent))]
    (map (fn[parent]
           (cons (jsoup-to-nested-map parent)
                 (map (fn[child]
                        (map jsoup-to-nested-map (mapcat #(.select % (name child)) (.children parent)))) childs)))
         parents)))

(defn extract-by-selectors[html selectors]
  (let [doc (Jsoup/parse (.replaceAll html "(?m)<[\\s]*([a-zA-Z]+)(.*?)>[\\s]*</\\1>" "<$1$2>&nbsp;</$1>"))]
    (map (fn[selector]
           (cond
             (sequential? selector) (process-child-selectors doc (first selector) (rest selector))
             (or (string? selector) (keyword? selector)) [(map jsoup-to-nested-map (.select doc (name selector)))]
             (nil? selector)[nil]))
         selectors)))

(defn prepare-selector
  "Process each selector in the map. Dispatch by types of first two elements in request.
  Return vector with two elements : raw selectors that should be send to parsing server,
  and post processing functions."
  [selector]
  (cond (and (vector? selector) (map? (second selector)) (:child (second selector)))
        (let [single-res-processor (reduce comp (drop 2 selector))]
          [(cons (first selector) (:child (second selector)))
           (fn[& args] (map (partial map single-res-processor) args))])

        (vector? selector)
        [(first selector) (reduce comp (rest selector))]

        true
        [selector identity]))

(defmacro jsoup-parse-html[html bindings & body]
  (let [partitioned-bindings (partition 2 bindings)
        vars (map first partitioned-bindings)
        requests (map second partitioned-bindings)]
    `(let [selectors# (map prepare-selector (list ~@requests))
           raw-selectors# (map first selectors#)
           post-selectors# (map second selectors#)
           [~@vars] (doall (map apply post-selectors# (extract-by-selectors ~html raw-selectors#)))]
       (do ~@body))))
