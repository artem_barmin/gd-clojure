(ns gd.parsers.help
  (:use gd.parsers.common))

(comment "Рассмотрим простейший парсер для магазина Oddy.

Для начала нужно запомнить главное правило парсинга HTML:
1. Не нужно парсить HTML регулярками, только куски сырого текста вытащеного из тэгов самых нижних уровней.

В вашем распоряжении есть следующие парсеры:
1. html парсер - определяется функцией `html-parse'
2. regexp парсер - определяется функцией `re-parse'

html парсер - это точка входа для каждого определенного глобального парсера. Они всегда имеют имя(в отличие от
парсеров на регулярках, которые вызываются по месту и имени не имеют).

ВНИМАНИЕ : далее по тексту jQuery селектор = CSS селектор

Макрос html-parse принимает 3 параметра:
1. имя - под которым он будет известен в остальной части программы
2. список css селекторов, с соответствующими биндингами переменных.
3. правила форматирования.

Самая сложная часть из всего этого: css селекторы, с помощью которых собственно и происходит парсинг. Для понимания что
это такое можете почитать : http://rubydev.ru/2011/08/jquery-2-selectors/(первое что нагуглилось). Конкретный синтаксис
селекторов используется из библиотеки enlive : https://github.com/cgrand/enlive/ (там есть таблица соответствия между css
синтаксисом и её).

Пример №1:
Допустим я хочу вытащить первый заголовок с классом 'product', тогда я напишу :h1.product(здесь основная разница с обычным
синтаксисом - наличие двоеточия перед селектором). После вытаскивания я, понятное дело, захочу сохранить его в
переменную. Здесь используется стандартный синтаксис let для кложуры(вы уже им пользовались). По умолчанию все селекторы
возвращают _списки тегов_, так что мы можем вытащить первый элемент как обычно : [product](напоминаю - я уже это вам
рассказывал, когда про веб показывал).

Таким образом полная запись для вытаскивания первого заголовка с классом product будет выглядеть так:

[product] [:h1.product]

, и потом можно использовать переменную product как угодно.

Пример №2:
Допустим я хочу вытащить массив тэгов label, которые содержат инфу о размерах, тогда я напишу

labels [:label]

, внимание - вокруг labels нет скобочек, я работаю со списком а не с первым элементом!

Пример №3

Я знаю что описание товара представлено двумя тегами `p', которые лежат в диве с классом tab-description

Тогда я напишу следующее:
[:div#tab-description :p]

что является прямой аналогией следующего css селектора(http://api.jquery.com/descendant-selector/), и я
точно знаю что мне вернется как максимум два элемента(хотя может и ничего - похер, тогда в переменные
запишутся nil)

[temper country] [:div#tab-description :p]

в переменную temper - запишется содержимое первого `p` тега, в country - содержимое второго `p` тега

Форматирование:
для форматирования используйте функцию format-csv, в большинстве случаев он сама разберется как из тегов
выгрести текст, и как и склеить. Читайте доки к ней.

Парсинг регулярками:

иногда текст нужно обработать перед сохранение в результат, для этого есть функция re-parse которая
принимает регулярку, в которой должна быть как минимум одна группа захвата! Ну здесь думаю все
понятно и так.

Проверить парсер можно так : (<имя парсера> <путь к файлу>) из REPL

")

(comment "Главные изменения: теперь акцент с парсинга страниц сместился на процедуру организации обхода страниц.

Как было раньше : качаем сайт при помощи httrace, долго и мучительно настраивая фильтры для ограничения траффика(аналогия : все разрешено, фильтруем - blacklist).

Как стало сейчас : мы эмулируем навигацию пользователя на сайте, и качаем только то что нужно(аналогия : все запрещено, выбираем то что нужно - whitelist).

Рассмотрим упрощенный пример:
1. Заходим на главную страницу
2. переходим на список колекций
3. со страницы колекции переходим на страницу товара
4. собственно парсим каждый товар
5. собираем все распарсеные товары и создаем сп

Можно представлять каждый парсер как сеть, где узлами являются
страницы(объявленые при помощи page), а связями - конструкции emit.

Еще одна аналогия : ООП, страницы - являются классами(определяют общее
поведение), а вызовы emit - создание инстанса класса, с конкретной страницей. Но
важно что бы конкретная страница была корректно распознана парсером. Можно
записать так emit B A, где A instanceof B, но это довольно условное обобщение.

Еще одна аналогия : узлы - это каналы, emit - посылка сообщения в канал

Каждый узел может делать определенные приготовление перед запуском собственно парсера:

1. page - принимает на вход ссылку(строка), скачивает страницу, преобразовывает
её в понятную кложуре форму, а только потом запускает html парсер. На выходе -
все что угодно(можно послать ссылку на другой page узел, можно послать уже
распарсеный товар в коллектор).

конструкция (page \"blah\") - сразу скачивает страницу и запускает парсер. (page
:category) - создает узел который ожидает прихода задач от других узлов.

2. process - принимает на вход произвольную структуру данных кложуры, на выходе
- все что угодно. Удобно для разбивания сложных обработок на несколько шагов.

3. auth - узел авторизации, принимает селектор формы для посылки и список
параметров - какие поля в какое значение установить -> отсылает форму и
сохраняет session id в cookie storage. Всегда выполняется самый первый.

4. post - на вход ссылка, выполняет post запрос, конвертирует в json и запускает
наш обработчик.

в принципе все можно посмотреть в коде соответствующих узлов, он довольно
простой(есть функции prepare и process).

5. collect - может быть только один на парсер(все остальные - произвольное
количество). Является финальным узлом сети, ждет пока не обработаются все
прошлые узлы. После этого передает обработчику список всех собранных айтемов,
где уже создается СП.

Работа с картинками

базовая функция download-file : на вход принимает строку с ссылкой, на выходе
строка с локальным путем файла.

! Обращайте внимание откуда качаете, часто на img вешают лишь маленькую
  превьюшку, а большая картинка лежит рядом завернутая в <a>

Хелперы:
1. для скачки из массива ссылок :showcase (download-links links) === (map #(download-file (attr :href %)) links)
2. для скачки из массива картинок :showcase (download-images links) === (map #(download-file (attr :src %)) links)
3. для перехода по массиву ссылок (follow-links :next links) === (map #(emit :next %) links)
4. parsei - парсит целое число из строки(первое попавшее, игнорирует мусор)
4. parsed - парсит дробное число из строки(первое попавшее, игнорирует мусор)

")

(comment

  "Устанавливаем главную страницу, если будут попадаться относительные ссылки, к
ним будет автоматически добавлятся этот адрес в начало."

  (main-url "http://very.ua/")

  "Проходим по списку всех ссылок на главной странице и запускаем их на
следующий уровень(уровень прасинга конкретной коллекции) - сейчас мы на входной
точке"

  (page "http://very.ua/kollekczii.html"
        [categories [:ul.banners-list :a]]
        (follow-links :category categories))

  "Уровень обработки коллекций, собираем ссылки на все товары и идем дальше по
ним на страницы обработки конкретного товара."

  (page :category
        [items [:div#mycarousel :a]]
        (follow-links :item items))

  (def nuivery-category (category:get-by-short-name "up"))

"Страница обработки товара. Здесь обработка описания довольно специфичная, лучше
посмотрим другие примеры, там все достаточно понятно.

Все описание разбивается на категории, и на выходе имеем мапу. Все что не попало
в категории отправляется в other.

Потом можно её отрендерить при помощи render-desc - он формирует короткое
и длинное описание(отличается только тем что в короткое не включается категория
other)

Если хотите выбрать конкретные ключи для короткого описания - нужно использовать
select-keys(може потом упрощу, или сами упростите)
".

  (page :item
        [name [:h2 text-node]
         description [:div.heading [:p (nth-child 3)] text-node]
         description-param-names [:div.parameters :dt text-node]
         description-param-values [:div.parameters :dd text-node]
         images [:ul.image.jcarousel-skin-tango :img] ;выбираем все картинки
         colors [:div.colors-area :img]]
        (let [description-params (zipmap description-param-names description-param-values)
              sizes (get description-params "Размеры:")
              description-params (dissoc description-params "Размеры:")
              short (:short (render-desc (select-keys description-params ["Ткань:" "Подкладка:" "Мех:"])))
              full (:full (render-desc description-params))]
          (emit :stock {:name name
                        :price 0
                        :category nuivery-category
                        :params {:size (map parsei (str/split sizes #","))} ;разбиваем список размеров по запятой, и парсим как число(parsei)
                        :short-description short
                        :description full
                        :showcase (download-images images) ; качем все картинки
                        })))

"Собираем все товары с прошлого уровня, и создаем СП"

  (collect :stock [items]
           (println "Nuivery created"
                    (group-deal:create
                     {:fkey_user (:id (user:get-by-name "root"))
                      :name "Nuivery"
                      :deal-info {:constraints-type :min_sum+sizes :min_sum 5000}
                      :showcase (showcase items 0 1 2)
                      :stock (distinct items)})))

  (execute "Nuivery"))
