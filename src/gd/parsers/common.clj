(ns gd.parsers.common
  (:use clojure.data.json
        digest
        clojure.set
        gd.parsers.jquery-parser
        gd.log
        gd.utils.profiler
        gd.utils.common
        [gd.utils.db :only [ensure-mongo-collection recreate-mongo-collection]]
        clojure.test
        gd.model.model
        gd.utils.test
        clj-time.coerce
        [noir.options :only (dev-mode?)]
        [clojure.java.shell :only (sh)]
        [clj-time.core :exclude [start extend]]
        [clojure.algo.generic.functor :only (fmap)]
        [robert.bruce :only [try-try-again]])
  (:require
   [monger.collection :as mc]
   [clj-http.cookies :as cook]
   [robert.hooke :as hook]
   [clojure.string :as str]
   [ring.util.codec :as codec]
   [clj-http.client :as c]
   [clj-http.util :as util])
  (:import
   org.apache.http.cookie.MalformedCookieException
   (java.net URI URL)
   (java.io File FileOutputStream)
   (java.util.concurrent Executors TimeUnit Semaphore)
   (java.util.concurrent.locks ReentrantReadWriteLock)
   (it.sauronsoftware.cron4j Scheduler)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Statistics

(def stats (atom {}))

(defn add-stats[key num]
  (swap! stats update-in [key] (fn[v] (if v (+ v num) num))))

(defn show-statistics[]
  (let [{:keys [prepare process hit miss]} @stats
        whole (+ prepare process)]
    (print-seq "Prepare time:" (* 100 (/ prepare whole))
               "\nProcessing time:" (* 100 (/ process whole))
               "\nCache hit:" hit
               "\nCache miss:" miss "\n")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils for monitoring parser progress

(def ^:dynamic *progress-monitor* (atom nil))
(def ^:dynamic *update-mode* nil)

(defn set-progress[state]
  (swap! *progress-monitor* merge state))

(defn load-single-parser
  "Asyncroniously load parser, with update mode."
  [deal-id parser-name monitor dry-run supplier-mode]
  (let [execution-thread
        (new Thread (fn[]
                      (try
                        (binding[*progress-monitor* monitor
                                 *update-mode* {:container deal-id
                                                :supplier-mode supplier-mode
                                                :dry-run dry-run}]
                          (require (symbol (str "gd.parsers.concrete." parser-name)) :reload))
                        (catch Exception e
                          (error e))
                        (finally
                          (reset! monitor nil)))))]
    (swap! monitor merge {:current-thread execution-thread})
    (.start execution-thread)))

(defn wait-until-parser-stopped[monitor]
  (.join (:current-thread @monitor)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils for work with nodes
(defn attr
  "Take attribute of element. For example (attr :href link), will return link of
matched element."
  ([att] (fn[node] (attr att node)))
  ([att node] (get-in node [:attrs att])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Parsers
(defn is-404-exception?[e]
  (let [bean (bean e)]
    (and
     (contains? bean :data)
     (= (get-in bean [:data :object :status]) 404))))

(defmacro save-current-page
  "Store current page into permanent place, that will not be removed. Should be used
for error detection."
  []
  `(let [new-file# (File/createTempFile "stored" ".html")]
     (info "Saved to" (.getAbsolutePath new-file#))
     (spit new-file# (slurp ~'current-tmpfile))))

(defn normalize-html[html]
  (let [doc (->
             html
             (.replaceAll "(?m)<[\\s]*([a-zA-Z]+)(.*?)>[\\s]*</\\1>" "<$1$2>&nbsp;</$1>")
             (org.jsoup.Jsoup/parse))]
    (.remove (.select doc "iframe"))
    doc))

(defmacro html-parse
  "Use CSS selectors for parse structured HTML documents. Main entry point for
  all parsers. Generate parser with supplied name. This function can be used
  later with file enumerators, or directly by passing filename to it."
  [html bindings body]
  `(let [tmpfile# (File/createTempFile "parsing" ".html")
         ~'current-tmpfile tmpfile#
         result# (delay (jquery-parse-html
                            (.getAbsolutePath tmpfile#)
                            ~bindings ~body))]
     (spit tmpfile# (normalize-html ~html))
     (force result#)
     (.delete tmpfile#)
     (force result#)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Download utils
(def base-urls (atom nil))

(defn main-url
  "Set base for all url from current namespace"
  [url]
  (swap! base-urls assoc *ns* url))

(defn- reverse-tails [xs]
  (let [xs (reverse xs)]
    (map (comp (partial apply str) reverse) (take-while not-empty (iterate rest xs)))))

(defn resolve-relative-url[current url]
  (when-let [tail (some (fn[tail] (and (.endsWith current tail) tail)) (reverse-tails url))]
    (str (subs current 0 (- (count current) (count tail))) url)))

(deftest resolve-realtive-test
  (is (= (resolve-relative-url "http://www.glem.com.ua/zhenskie-platya-optom/" "zhenskie-platya-optom/?page=2")
         "http://www.glem.com.ua/zhenskie-platya-optom/?page=2"))

  (is (nil? (resolve-relative-url "http://www.glem.com.ua/zhenskie-platya-optom/" "zhenskie-bluzki-optom/?page=2"))))

(defn absolutize-url[url & [base-url]]
  (if-let [base (or base-url (get @base-urls *ns*))]
    (if (or (.startsWith url base) (.startsWith url "http://"))
      url
      (str (if (.endsWith base "/") base (str base "/"))
           (if (.startsWith url "/") (subs url 1) url)))
    url))

(defn- try-again[body info]
  (try-try-again
   {:sleep 5000
    :tries 5
    :catch [java.lang.Exception]
    :error-hook (fn[e] (if (is-404-exception? e)
                         false
                         (print-seq "I got error while downloading" info e)))}
   body))

(def cookies (atom nil))
(def ^:dynamic *cookies*)

(defn get-cookie-storage[]
  (if (thread-bound? #'*cookies*)
    *cookies*
    (if-let [cs (get @cookies *ns*)]
      cs
      (let [cs (clj-http.cookies/cookie-store)]
        (swap! cookies assoc *ns* cs)
        cs))))

(defn clear-cookies[]
  (swap! cookies dissoc *ns*))

(def basic-auth (atom nil))

(defn set-basic-auth[login password]
  (swap! basic-auth assoc *ns* [login password]))

(defn get-basic-auth[]
  (get @basic-auth *ns*))

(defn-
  wrap-result-encoding
  "Middleware converting the :accept-encoding option to an acceptable
  Accept-Encoding header in the request."
  [client]
  (fn [req]
    (let [res (client req)
          content-type (get (:headers res) "content-type")
          is-text? (and content-type (re-find #"text" content-type))
          charset
          (when is-text?
            (or
             (second (and content-type
                          (re-find #"charset=([^;]+);?" content-type)))
             (second (re-find #"(?im)<meta[^>]*charset=([^\"']+)"
                              (new String (:body res) "utf8")))))]
      (if is-text?
        (update-in res [:body] (fn[v](if v
                                       (new String v (or charset "utf8"))
                                       "")))
        res))))

(hook/add-hook #'cook/decode-cookie-header
               (fn[f & [response]]
                 (try
                   (f response)
                   (catch Throwable e
                     (if (instance? MalformedCookieException (.getCause e))
                       response
                       (throw e))))))

(defn- cookie-processor
  "Disable clj-http cookie processor. Beacause they are already processed by Apache HTTP client,
and cause duplicate Cookie header in the request."
  [f & [request :as args]]
  request)

(hook/add-hook #'cook/encode-cookie-header #'cookie-processor)

(def ^:dynamic *custom-headers* nil)

(defmacro with-custom-headers[headers & body]
  `(binding [*custom-headers* ~headers]
     ~@body))

(defn unsafe-request[url params]
  (let [cs (get-cookie-storage)]
    ((wrap-result-encoding c/request)
     (merge
      params
      {:url url
       :socket-timeout 5000
       :conn-timeout 5000
       :cookie-store cs
       :basic-auth (get-basic-auth)
       :as :byte-array
       :headers (merge
                 {"User-Agent" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:11.0) Gecko/20100101 Firefox/20.0"
                  "Accept" "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
                  "Accept-Encoding" "gzip,defalte"
                  "Host" (:server-name (c/parse-url url))}
                 *custom-headers*)}))))

(defn- request[url params]
  (let [url (escape-url (absolutize-url url))]
    (try-again (fn[](unsafe-request url params)) url)))

(defn req-get[url]
  (request url {:method :get}))

(defn req-post[url params]
  (request url {:method :post
                :content-type "application/x-www-form-urlencoded"
                :body (c/generate-query-string params)}))

(defn- download-escaped[path url]
  (with-open [out (new java.io.FileOutputStream path)]
    (try
      (.write out (:body (try-again #(c/get url {:as :byte-array}) url)))
      (catch Throwable e
        (error e)))))

(defn- download-unescaped[path url]
  (sh "curl" "-s" "-S" "--retry" "3" "--retry-delay" "1"
      "-o" (.getAbsolutePath (new File path)) url))

(defn check-file-already-downloaded[url]
  (let [url (escape-url (absolutize-url url))
        {:keys [server-name uri query-string]} (c/parse-url url)
        path (str "data/" server-name "/files/" (sha1 (str uri query-string)) ".jpg")
        file (new File path)]
    (.exists file)))

(defn download-file
  "Download binary file, and store it to UIR that extracted from URL."
  [url]
  (if (and (.startsWith url "data/prices/") (.exists (new File url)))
    url
    (let [url (escape-url (absolutize-url url))
          {:keys [server-name uri query-string]} (c/parse-url url)
          path (str "data/" server-name "/files/" (sha1 (str uri query-string)) ".jpg")
          file (new File path)]
      (when-not (.exists file)
        (.mkdirs (.getParentFile file))
        (if (re-find #"%7C" url)        ;if URL contains vertical bar(|) - we
                                        ;should use unescaped downloader(curl)
          (download-unescaped path (.replaceAll url "%7C" "|"))
          (download-escaped path url)))
      ;; check size of file - if zero it means that file cant be downloaded(404) -
      ;; return nil, but dont remove file - to cache errors and improve
      ;; performance
      (if (= (.length file) 0)
        nil
        path))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Middle level information extraction API(pages and emits)
(defmacro eager[computation]
  `(let [res# ~computation]
     (if (seq? res#)
       (doall res#)
       res#)))

(def max-threads-count 20)

(def consumers (atom {}))

(def producers (atom {}))

(def event-execution-thread-poll (Executors/newFixedThreadPool max-threads-count))

(def events (atom nil))

(defonce completed (atom nil))

(def in-work (atom nil))

(defn- inc-nil[num]
  (if num
    (inc num)
    1))

(def ^{:doc "Semaphore that used for throttling of pages consuming. In order to
not recreate thread poll we use this. If you want to limit number of concurrent
connections - use this semaphore. But it should be changed only before any real
work began."}
  throttle-semaphore (atom (new Semaphore max-threads-count)))

(def pause-lock (new ReentrantReadWriteLock))

(defn pause-execution[]
  (.lock (.writeLock pause-lock)))

(defn resume-execution[]
  (try (.unlock (.writeLock pause-lock)) (catch Exception e)))

(defn emit
  "Send info to agents witch connected to CHANNEL"
  [channel info & [extra-info]]
  (let [ns *ns*
        update-mode *update-mode*
        additional-keys (if (vector? channel) (rest channel) nil)
        channel (if (vector? channel) (first channel) channel)
        pending (partial swap! events update-in [ns channel :pending])
        completed (partial swap! events update-in [ns channel :completed])
        working (partial swap! events update-in [ns channel :working])
        in-work-urls (partial swap! in-work update-in [ns channel])]
    (if-let [{:keys [process prepare]} (get @consumers [ns channel])]
      (do
        (pending inc-nil)
        (.submit event-execution-thread-poll #(binding [*ns* ns
                                                        *update-mode* update-mode]
                                                (try
                                                  (.acquire @throttle-semaphore)
                                                  (.lock (.readLock pause-lock))
                                                  (pending dec)
                                                  (working inc-nil)
                                                  (in-work-urls (fn[s](set (conj s info))))
                                                  (let [prepared (measure (prepare {:addtional-keys additional-keys
                                                                                    :info info
                                                                                    :extra-info extra-info}))
                                                        processed (measure (process info (:res prepared)))]
                                                    (add-stats :prepare (:time prepared))
                                                    (add-stats :process (:time processed))
                                                    (eager (:res processed)))

                                                  (catch Throwable e
                                                    (if (is-404-exception? e)
                                                      (print-seq "For(" channel ") " info "404")
                                                      (do (print-seq "For(" channel ") " info)
                                                          (error e))))
                                                  (finally
                                                    (working dec)
                                                    (in-work-urls (fn[s](disj s info)))
                                                    (completed inc-nil)
                                                    (.unlock (.readLock pause-lock))
                                                    (.release @throttle-semaphore))))))
      (print-seq "Empty consumer for (" channel ", " *ns* ") " " with " info))))

(defn add-consumer[channel method]
  (swap! consumers assoc [*ns* channel] method))

(defmacro post
  "Executes POST request to URL with PARAMS, and return result as described in RESULTS form.

RESULTS : [name type], where type can be :json :html :xml, result will be transformed according to
requested type, and can be used later from 'name' binding in ACTIONS form "
  [url params results & actions]
  (let [[binding type] results
        result-transformer (case type
                             :json read-json)]
    `(swap! producers assoc-in [*ns* ~url]
            (fn[]
              (let [~binding (~result-transformer (:body (req-post ~url ~params)))]
                ~@actions)))))

(def ^{:doc "Contains set of already visited pages, to prevent parsing duplicates"} visited-pages (atom nil))

(defmacro page
  "Create agent or send request, depends on source type. If SOURCE is string - send GET request, and
execute immediately, if keyword - create agent and wait for new data arrive on given channel."
  [source html-bindings & actions]
  (cond
    (keyword? source)
    `(add-consumer ~source
                   {:prepare (fn[info#]
                               (let [should-start# (atom false)]
                                 (locking visited-pages
                                   (let [processed# (get @visited-pages *ns*)]
                                     (when-not (and processed# (processed# (:info info#)))
                                       (reset! should-start# true)
                                       (swap! visited-pages update-in [*ns*] union #{(:info info#)}))))
                                 (when @should-start#
                                   [((comp :body req-get :info) info#) (:extra-info info#)])))
                    :process (fn[~'info [html# ~'extra-info]]
                               (when html#
                                 (html-parse html# ~html-bindings (do ~@actions))))})

    true
    `(swap! producers assoc-in [*ns* ~source]
            (fn[]
              (doall
               (-> (req-get ~source)
                   :body
                   (html-parse ~html-bindings (do ~@actions))))))))

(defmacro element
  "Create node that accept raw HTML(or xml nodes), and allow process it
separately(similar to 'page', but dont produce http request)"
  [source html-bindings & actions]
  `(add-consumer ~source
                 {:prepare (fn[info#]
                             (let [data# (:info info#)
                                   extra-info# (:extra-info info#)]
                               (if (string? data#)
                                 [data# extra-info#]
                                 [(reduce str (net.cgrand.enlive-html/emit* data#)) extra-info#])))
                  :process (fn[~'info [html# ~'extra-info]]
                             (html-parse html# ~html-bindings (do ~@actions)))}))

(declare flush-single-page)

(defmacro auth
  "Helper for simplify authentification process."
  [source form-selector params]
  `(swap! producers assoc-in [*ns* :auth]
          (fn[]
            (doall
             (do
               (flush-single-page ~source)
               (clear-cookies)
               (-> (req-get ~source) :body
                   (html-parse
                    [[action#] [~form-selector]
                     fields# [(str ~form-selector " input")]]
                    (let [action# (or (attr :action action#) "")
                          attrs# (remove #(= (attr :type %) "submit") fields#)
                          attrs-map# (zipmap (map (attr :name) attrs#) (map (attr :value) attrs#))
                          login-map# (merge attrs-map# ~params)]
                      (println "Auth action" action# login-map#)
                      (println "Auth cooikes" (:cookies (req-post action# login-map#)))))))))))

(defmacro file
  "Create agent or send request, depends on source type. If SOURCE is string - send GET request, and
execute immediately, if keyword - create agent and wait for new data arrive on given channel."
  [source path & actions]
  `(swap! producers assoc-in [*ns* ~source]
          (fn[]
            ((fn[~@path](do ~@actions)) (download-file ~source)))))

(defmacro process[channel params & actions]
  `(add-consumer ~channel {:prepare :info
                           :process (fn[~'info info#]
                                      (let [[~@params] info#]
                                        (eager (do ~@actions))))}))

(defn execute-all[]
  (print-seq "Start processing " *ns* " " (get @producers *ns*))
  (let [producers (get @producers *ns*)]
    (if-let [f (:auth producers)]
      (f))
    (map! (fn[[k f]](do
                      (print-seq "Executing " *ns* k)
                      (f)))
          (dissoc producers :auth))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Collector

(def stopped (atom nil))

(defn force-stop[]
  (reset! stopped true))

(defn not-zero[key collection]
  (remove nil? (map key collection)))

(defn all-events-processed
  "Check that all previous levels are processed - no pending events, and only one can be working - current processor."
  [channel]
  (or
   @stopped
   (let [events-for-me (vals (dissoc (get @events *ns*) channel))]
     (if-not (nil? events-for-me)
       (let [pending (reduce + (not-zero :pending events-for-me))
             working (reduce + (not-zero :working events-for-me))]
         (and (= pending 0) (= working 0)))))))

(defn is-processing-started[]
  (or
   @stopped
   (let [events-for-me (vals (get @events *ns*))]
     (if-not (nil? events-for-me)
       (let [completed (reduce + (not-zero :completed events-for-me))]
         (> completed 0))))))

(defmacro collect[channel params & actions]
  `(let [queue# (atom nil)
         ns# *ns*
         update-mode# *update-mode*]
     (add-consumer ~channel {:prepare identity
                             :process (fn[~'info event#]
                                        (let [additional-keys# (:addtional-keys event#)
                                              info# (:info event#)]
                                          (swap! queue# update-in (or additional-keys# [:items]) conj info#)))})
     (.start
      (new Thread (fn[]
                    (try
                      (binding [*ns* ns#
                                *update-mode* update-mode#]
                        ;; wait while started processing
                        (loop []
                          (Thread/sleep 1000)
                          (if-not (is-processing-started)
                            (recur)))

                        ;; wait while all events are processed
                        (loop []
                          (Thread/sleep 1000)
                          (if (all-events-processed ~channel)
                            (do
                              (if-not (every? #(not (empty? (get @queue# %))) ~(vec (map keyword params)))
                                (throwf "All event processed, but no items collected. Exiting"))
                              (let [{:keys [~@params] :as ~'collected-map} (fmap (comp distinct vec) @queue#)]
                                (doall (do ~@actions))))
                            (recur))))
                      (catch Throwable external#
                        (error external#))
                      (finally
                        (swap! completed update-in [ns#] (constantly true)))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Executors
(defonce parsing-mutex (Object.))

(defn execute[name & [threads-throttle pause-each pause-for]]
  (locking parsing-mutex
    (when threads-throttle
      (swap! throttle-semaphore (constantly (new Semaphore threads-throttle))))
    (print-seq "Parsing" name)
    (reset! events {})
    (reset! completed {})
    (execute-all)
    (let [processing-seconds (atom 0)]
      (loop []
        (Thread/sleep 1000)

        ;; pause
        (when (and pause-each pause-for
                   (= (mod (swap! processing-seconds inc) pause-each) 0))
          (print-seq "Pause for "pause-for)
          (pause-execution)
          (Thread/sleep (* pause-for 1000))
          (resume-execution))

        ;; monitoring progress
        (set-progress {:status :working
                       :ns *ns*
                       :events (get @events *ns*)
                       :thumbnails (gd.model.model/left-thumbnails)})
        (print-seq *ns* (get @events *ns*) (gd.model.model/left-thumbnails))

        ;; check for completeness
        (if-not (and (get @completed *ns*) (zero? (gd.model.model/left-thumbnails)))
          (recur))))))

(defn load-parsers[& ns-syms]
  (try
    (doseq [ns-sym ns-syms]
      (require (symbol (str "gd.parsers.concrete." (name ns-sym)))))
    (finally
      (stop))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cache for pages(ttl for 48 hours - allow efficient parser :prod testing)
;; Used only in dev-mode. In production - all requests bypass cache.

(def pages-cache "pages_cache")

(ensure-mongo-collection pages-cache {:expireAfterSeconds (* 60 60 24 2)} [:deleted])

(defn- put-to-cache-and-get[key f args]
  (mc/find-and-modify
   pages-cache
   {:_id key}
   (merge
    {:_id key}
    (->
     (apply f args)
     (assoc :request-time (to-date (now)))
     (assoc :deleted false)
     (select-keys [:body :request-time :calls :data :deleted])))
   :upsert true
   :return-new true))

(defn get-page-from-cache-or-request[f & [url :as args]]
  (if (dev-mode?)
    (let [key
          (str (ns-name *ns*) url)

          {:keys [request-time] :as value}
          (mc/find-one-as-map pages-cache {:_id key :deleted false})]
      (dissoc (if (or (not value) (before? (plus (to-date-time request-time) (hours 48)) (now)))
                (do
                  (add-stats :miss 1)
                  (put-to-cache-and-get key f args))
                (do
                  (add-stats :hit 1)
                  value))
              :_id :deleted))
    (apply f args)))

(hook/add-hook #'req-get #'get-page-from-cache-or-request)

(defn flush-cache[]
  (recreate-mongo-collection pages-cache))

(defn flush-single-page[url]
  (mc/update pages-cache {:_id (str (ns-name *ns*) url)} {:deleted true}))

(deftest cache-test
  (monger.collection/drop "pages_cache_test")
  (with-redefs [pages-cache "pages_cache_test"]

    (let [calls (atom 0)
          function (fn[url]
                     (swap! calls inc)
                     {:data url :calls @calls})]
      (testing "first call - put value to cache"
        (let [res (get-page-from-cache-or-request function "1")]
          (is (= (dissoc res :request-time) {:data "1" :calls 1}))
          (is (contains? res :request-time))
          (is (= @calls 1))))

      (testing "get value from cache"
        (get-page-from-cache-or-request function "1")
        (is (= @calls 1)))

      (testing "cache is expired - call function"
        (with-redefs [now (constantly (plus (now) (hours 60)))]
          (is (= (dissoc (get-page-from-cache-or-request function "1") :request-time)
                 {:data "1" :calls 2}))
          (is (= @calls 2))))

      (testing "flush one page"
        (flush-single-page "1")
        (is (= (dissoc (get-page-from-cache-or-request function "1") :request-time)
               {:data "1" :calls 3}))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Test runner
(run-tests)
