(ns gd.parsers.view.pages
  (:use gd.parsers.view.process
        gd.log
        clojure.data.json
        gd.parsers.common
        hiccup.core
        [net.cgrand.enlive-html :only (emit* html-snippet transform-content)]
        hiccup.form
        hiccup.page
        hiccup.element
        [com.reasonr.scriptjure :only (cljs js js*)]
        gd.utils.web
        gd.utils.common
        gd.views.components
        [gd.parsers.common :only (attr normalize-html unsafe-request)]))

(def local-cache (atom nil))

(defn load-or-get-from-cache[url]
  (or
   (get @local-cache url)
   (get (swap! local-cache assoc url
               (try (unsafe-request url {:method :get})
                    (catch Exception e nil)))
        url)))

(defonce stocks-list (atom nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Config manipulation utils

(defonce config-store (atom nil))

(defn save-config[]
  (spit (str "resources/parser-description/" (:parser-name @config-store))
        (with-out-str (pprint-json @config-store :escape-unicode false))))

(defn load-config[]
  (println "Loading config" (:parser-name @config-store))
  (reset!
   config-store
   (read-json
    (slurp (str "resources/parser-description/" (:parser-name @config-store))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Dom manipulation utils

(defn- append-to[doc selector elem-to-append]
  (let [elem (.first (.select doc selector))]
    (.append elem (html elem-to-append))))

(defn- prepend-to[doc selector elem-to-append]
  (let [elem (.first (.select doc selector))]
    (.prepend elem (html elem-to-append))))

(defn- remove-elt[doc selector]
  (.remove (.select doc selector)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HTTP request to config processor
(def check-config-structure
  (partial clojure.walk/prewalk (fn[node]
                                  (if (map? node)
                                    (do
                                      (assert (not (some sequential? (vals node))))
                                      node)
                                    node))))

(defn store-config[req]
  ((->
    (fn[req]
      (let [config (clojure.walk/keywordize-keys (:params req))]
        (check-config-structure config)
        (reset! config-store config)))
    ring.middleware.nested-params/wrap-nested-params
    ring.middleware.params/wrap-params)
   req))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Config render

(defn with-config-values[panel]
  (clojure.string/join
   (emit*
    ((transform-content
       [:input] (fn[node]
                  (if (#{"radio"} (attr :type node))
                    node
                    (let [config-key
                          (map keyword
                               (ring.middleware.nested-params/parse-nested-keys
                                (attr :name node)))

                          config-value
                          (get-in @config-store config-key)]
                      (if (= "checkbox" (attr :type node))
                        (if (seq config-value) (assoc-in node [:attrs :checked] "checked") node)
                        (assoc-in node [:attrs :value] config-value))))))
     (first (html-snippet (html panel)))))))

(defn process-filters-config[param-name config-html]
  (clojure.string/join
   (emit*
    ((transform-content
       [:input] (fn[node]
                  (assoc-in node [:attrs :name]
                            (format
                             "%s[filters][---position---][%s]"
                             param-name
                             (attr :name node)))))
     (first (html-snippet (html config-html)))))))

(defn- filters-modal[param-name]
  (let [filter-config-panel
        (fn[name config]
          (process-filters-config
           param-name
           [:li.ui-state-default.filter-item {:style "padding:5px;"}
            [:span.ui-icon.ui-icon-circle-close {:onclick (js (removeFilter this)) :style "float:left;"}]
            [:input {:name "type" :type "hidden" :value name}]
            (or config [:div (str name)])]))]
    [:div

     [:div {:style "margin:10px;"}
      "Add new filter:"
      [:select
       (map (fn[{:keys [name config]}]
              [:option {:onclick
                        (js (addFilter (clj (filter-config-panel name config)))
                            (refreshMasonry))}
               name])
            processors)]]

     [:ul#sortable {:name (str "filters-" param-name)}
      (map
       (fn[[num {:keys [type]}]]
         (.replace
          (filter-config-panel
           type
           (:config (processor-by-name type)))
          "---position---"
          (name num)))
       (sort-by first (get-in @config-store [(keyword param-name) :filters])))]

     [:button.text-button {:onclick (js (saveFilters (clj (str "[name=filters-" param-name "]")))
                                        (refreshStocks)
                                        (closeDialog))}
      "Save"]
     [:button.text-button {:onclick (js (closeDialog))}
      "Close"]]))

(defn- control-panel[]
  (let [switch-button
        (js*
         (. (wQuery "#stock-parsing,#site-traversing,#final-parsing") hide)
         (. (wQuery ".parsing-switch-button") css "background-color" "white")
         (. (wQuery this) css "background-color" "#ffdead"))]
    [:form#config-form
     [:div.parsing-switch-button
      {:style "float:left;border:1px solid gray;background-color:#ffdead;cursor:pointer;"
       :onclick (js (clj switch-button)
                    (. (wQuery "#site-traversing") show))}
      "Site traversing"]
     [:div.parsing-switch-button
      {:style "float:left;border:1px solid gray;cursor:pointer;"
       :onclick (js (clj switch-button)
                    (. (wQuery "#stock-parsing") show))}
      "Stock parsing"]
     [:div.parsing-switch-button
      {:style "float:left;border:1px solid gray;cursor:pointer;"
       :onclick (js (clj switch-button)
                    (. (wQuery "#final-parsing") show))}
      "Merger testing"]

     [:div#current-selector {:style "float:left;"}]
     [:div {:style "float:right;background:grey;"
            :onclick (js (if (= (. ($ ".skip-searching") css "height") "300px")
                           (. ($ ".skip-searching") css "height" "90%")
                           (. ($ ".skip-searching") css "height" "300px")))}
      "_____"]

     [:div {:style "clear:both;"}]

     [:div#site-traversing
      [:div {:style "float:left;"}
       [:div
        [:button.text-button {:onclick (js (saveConfig)
                                           (return false))}
         "Save config"]
        [:button.text-button {:onclick (js (loadConfig)
                                           (return false))}
         "Load config"]]
       [:table
        [:tr [:td "Parser name"] [:td (text-field "parser-name")]]
        [:tr [:td "Start url"] [:td (text-field "start-url")]]
        [:tr [:td "Categories"] [:td (text-field "categories")]]
        [:tr [:td "Next page"] [:td (text-field "next-page")]]
        [:tr [:td "Stocks"] [:td (text-field "stocks")]]
        [:tr [:td "Stocks limit"] [:td (text-field "stocks-limit")]]]
       [:button.text-button {:onclick (js (fetchStocks)
                                          (return false))}
        "Fetch stocks"]]
      [:div#fetched-stocks {:style "float:left;"}]]

     [:div#stock-parsing {:style "display:none;"}
      (let [param-config-panel
            (fn[name]
              (let [name (clojure.core/name name)
                    cap-name (clojure.string/capitalize name)]
                [:tr.param-config
                 [:td (radio-button :param false name) cap-name]
                 [:td (text-field {:onkeyup (js (refreshStocks)) :style "width:400px;"}
                                  (str name "[sel]"))]
                 [:td
                  [:button {:onclick (js (. (. (. (. (wQuery this) closest ".param-config") find ".config") clone)
                                            dialog {:modal true :width "auto"})
                                         (afterModalInit)
                                         (return false))
                            :href ""}
                   "⚙"]
                  [:div.config {:title (str cap-name " filters config") :style "display:none;"}
                   (filters-modal name)]]]))]
        [:div {:style "float:left;"}
         [:table#parameters-table
          [:tr [:td [:select
                     (map (fn[name]
                            [:option {:onclick (js (addParameter (clj (html (param-config-panel name)))))} (clojure.core/name name)])
                          custom-params)]]]
          [:tr [:td (radio-button :param true "none") "None"]]
          (map param-config-panel
               (concat always-params
                       (clojure.set/intersection
                        (set custom-params)
                        (set (keys @config-store)))))]

         [:div
          (text-field {:style "width:130px;"} "reparse-stocks") "items"
          [:button.text-button {:onclick (js (refreshStocks true)
                                             (return false))}
           "Force parsing"]]

         [:div
          (check-box "stock-processor-enabled" false "enabled")
          (text-field {:style "width:130px;"} "stock-processor")
          "stock processor ns"]])
      (map (fn[i]
             [:div {:style "float:left;width:300px;margin-bottom:10px;"
                    :id (str "stock-parsed" i)}])
           (range 0 200))]

     [:div#final-parsing {:style "display:none;"}
      [:div
       (check-box "stock-merger-enabled" false "enabled")
       (text-field {:style "width:130px;"} "stock-merger")
       "stock merger ns"
       [:button.text-button {:onclick (js (startMerger)
                                          (return false))}
        "Show merger result"]]
      [:div#merger-result]]]))

(defn process-html[body]
  (let [doc (normalize-html body)]

    (remove-elt doc "base")

    ;; include our custom jquery and jquery ui library and make it non conflicting
    (prepend-to
     doc "head" (include-js "http://localhost:8080/utils/js/parser-view.js"))
    (prepend-to
     doc "head" (javascript-tag (js (var wQuery (. $ noConflict true)))))
    (prepend-to
     doc "head" (include-js "http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"))
    (prepend-to
     doc "head" (include-js "http://masonry.desandro.com/masonry.pkgd.js"))
    (prepend-to
     doc "head" (include-js "http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"))
    (append-to
     doc "head"
     (html
      [:style "
#sortable { list-style-type: none; margin: 0; padding: 0; }
#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-weight:normal;}
#sortable li span { position: absolute; margin-left: -1.3em; }
.ui-dialog {background: url('images/ui-bg_highlight-soft_100_eeeeee_1x100.png') repeat-x scroll 50% top #EEEEEE !important; min-width:500px;}
.ui-menu { position: absolute; width: 100px; }
.ui-state-highlight { height: 1.5em; line-height: 1.2em; }
body {overflow-x:hidden;}
:link, *:visited {color: #666666 !important; text-decoration: none;}
a:hover {color: #0B2756 !important;}
#config-form * {font-size:14px !important; font-family:Sans;color: #222222;}
#config-form .stock-parsing-result * {font-size:12px !important;}
#fetched-stock-info td {padding-left:10px;padding-right:10px;}
"]))

    (append-to
     doc "body"
     [:div.skip-searching {:style "width:100%;height:300px;background:#f5f5f5;position:fixed;bottom:0;left:0;z-index:10000;overflow-y:scroll;overflow-x:hidden;border-top:2px solid #b22222;"}
      (with-config-values (control-panel))])
    (append-to
     doc "head" (include-css "http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-lightness/jquery-ui.css"))

    (str doc)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stock parser

(defn parsed-stock-view[{:keys [name price params showcase description misc]} relative-url stock-num]
  [:div.stock-parsing-result {:style "width:300px;"}
   [:div {:style "border: 1px solid black;background-color:#2e3436;color:white;font-size:14px;"}
    [:a {:href relative-url :style "color:white !important;"} name]
    (when stock-num
      [:a {:onclick (js (refreshSingleStocks (clj stock-num))
                        (return false))
           :style "color:red !important;float:right;"
           :href "#"}
       "R"])
    [:div {:style "clear:both;"}]]
   [:div {:style "border: 1px solid black;"}
    (letfn [(render-img[img]
              [:div
               (cond (string? img)
                     [:a {:href img :target "_blank"} img]

                     (nil? img)
                     "NIL"

                     (and (map? img) (not (:tag img)))
                     [:div
                      [:a {:href (:img img) :target "_blank"} (:img img)]
                      (str (:params img))]

                     true
                     (str img))])]
      (map render-img showcase))]
   [:div {:style "border: 1px solid black;background-color:#afeeee;"} params]
   [:div {:style "border: 1px solid black;background-color:#ffdead;"}
    (if (or (string? description) (sequential? description))
      [:pre {:style "white-space:pre-wrap;"}description]
      description)]
   [:div {:style "border: 1px solid black;background-color:#9b8a5b;"}
    (map (fn[[key val]]
           [:div [:span {:style "color:white;"} key]
            (cond (= key :custom-sizing-table)
                  (html val)

                  (sequential? val)
                  (str (seq val))

                  true
                  (str val))])
         misc)]])

(defn stocks-rerender[req]
  (store-config req)
  {:body (html
          (let [config @config-store

                stock-num
                (Long/valueOf (:stockNumber config))

                referer-url
                (.replaceAll (get (:headers req) "referer") "http://localhost:9000" "")

                relative-url
                (if (= stock-num 0)
                  referer-url
                  (nth (remove (partial = referer-url) @stocks-list) stock-num))]
            (try
              (->
               (str current-site relative-url)
               load-or-get-from-cache
               (parse-using-config :stock config)
               (parsed-stock-view relative-url stock-num))
              (catch Exception e
                (error e)
                [:div
                 [:a {:style "color:red;" :href relative-url}
                  "Parsing error: " (.getMessage e)]]))))})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stocks merger

(defn stocks-merge[req]
  (store-config req)
  {:body (html
          (let [{:keys [stock-merger stock-merger-enabled] :as config} @config-store
                merge-fn (get-merge-fn (when (seq stock-merger-enabled) stock-merger))]
            (map
             (fn[{:keys [type url result]}]
               (case type
                 :stock [:div.merged-stock {:style "float:left;width:300px;margin-bottom:10px;"}
                         (parsed-stock-view result url nil)]
                 :error result))
             (merge-fn
              (parallel
               (fn[relative-url]
                 (try
                   {:type :stock
                    :url relative-url
                    :result (->
                             (str current-site relative-url)
                             load-or-get-from-cache
                             (parse-using-config :stock config))}
                   (catch Exception e
                     (error e)
                     {:type :error
                      :result [:div
                               [:a {:style "color:red;" :href relative-url}
                                "Parsing error: " (.getMessage e)]]})))
               @stocks-list)))))})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Site traverser

(defn categories-fetch
  ([config link]
     (categories-fetch config (atom #{}) link))
  ([config processed link]
     (when-not (@processed link)
       (let [current-page-links
             (map (attr :href)
                  (parse-using-config
                   (load-or-get-from-cache (absolutize-url link current-site))
                   :all-categories
                   config))]
         (swap! processed conj link)
         (->>
          current-page-links
          (map (partial categories-fetch config processed))
          (concat current-page-links)
          flatten
          distinct
          (remove nil?))))))

(defn stocks-fetch[req]
  (store-config req)
  (let [{:keys [start-url stocks stocks-limit next-page] :as config}
        @config-store

        stocks-limit
        (or (when (seq stocks-limit) (Long/valueOf stocks-limit)) 12)

        category-links
        (categories-fetch config (if (seq start-url) start-url current-site))

        stock-links
        (when (seq stocks)
          (->>
           (mapcat
            (fn[cat-link]
              (map
               (fn[stock]
                 [(attr :href stock) cat-link])
               (parse-using-config
                (load-or-get-from-cache (str current-site cat-link))
                :all-stocks
                config)))
            category-links)
           (distinct-by first)
           (take stocks-limit)))

        category-links
        (distinct (map second stock-links))

        stock-links
        (distinct (map first stock-links))]
    (reset! stocks-list (set stock-links))

    ;; fetch all stock refs in background thread
    (.start
     (new Thread
          (fn[]
            (try
              (doseq [ref @stocks-list]
                (load-or-get-from-cache (str current-site ref)))
              (catch Throwable e
                (error e))))))

    {:body (html [:table#fetched-stock-info
                  [:tr
                   [:td {:style "font-size:20px;color:#1E6189;"}
                    "Категории(" (count category-links) ")"]
                   [:td {:style "font-size:20px;color:#1E6189;"}
                    "Товары(" (count stock-links) ")"]]
                  (map-longest (fn[cat stock]
                                 [:tr
                                  [:td [:a {:href cat} cat]]
                                  [:td [:a {:href stock} stock
                                        (if (@stocks-list stock) [:span {:style "color:#32cd32;"} " LOAD"])]]])
                               (constantly nil)
                               category-links
                               stock-links)])}))
