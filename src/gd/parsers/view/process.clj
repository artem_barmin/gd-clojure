(ns gd.parsers.view.process
  (:use gd.log
        clojure.data.json
        hiccup.core
        [clojure.algo.generic.functor :only (fmap)]
        gd.parsers.llapi
        gd.parsers.hlapi
        gd.utils.common
        [gd.parsers.common :exclude [attr auth set-basic-auth]]))

(def current-site "http://bono.in.ua")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Processing blocks

(defn processor[name fn config]
  {:name name :fn fn :config config})

(defn create-map-from-lst[lst]
  (let [{:keys [out first current rest]}
        (reduce (fn[{:keys [out rest current] :as res} v]
                  (if (and (map? v) (= :special (:type v)))
                    (if current
                      (merge res {:rest [] :current v :out (assoc out (text (:content current)) rest)})
                      (merge res {:rest [] :current v :first rest}))
                    (merge res {:rest (cons v rest)})))
                {}
                lst)

        [last-key last-content]
        [(text (:content current)) rest]

        res
        (fmap
         (comp (partial remove empty?) (partial map prepare-str))
         (merge
          out
          (when (and last-key (seq rest)) {last-key rest})
          (when (seq first) {:other first})))]
    (if (empty? res) nil res)))

(def processors
  [(processor "attr-extractor"
              (fn[nodes {:keys [attr-name]}] (map (attr (keyword attr-name)) nodes))
              [:div "attr" [:input {:name "attr-name"}]])
   (processor "regexp-extractor"
              (fn[nodes {:keys [regexp]}]
                (map (fn[node] (re-parse (re-pattern regexp) node)) nodes))
              [:div "regexp" [:input {:name "regexp"}]])
   (processor "join-strings"
              (fn[nodes {:keys [separator]}]
                (clojure.string/join separator nodes))
              [:div "separator" [:input {:name "separator"}]])
   (processor "text-nodes"
              (fn[nodes {:keys []}]
                (remove nil? (map text nodes)))
              nil)
   (processor "options-extractor"
              (fn[nodes {:keys []}]
                (let [options (atom nil)]
                  (clojure.walk/postwalk
                   (fn[node]
                     (when (and (map? node) (= :option (:tag node)))
                       (swap! options conj (text node)))
                     node)
                   nodes)
                  @options))
              nil)
   (processor "image-options"
              (fn[nodes {:keys [blur-region crop-region]}]
                (map
                 (fn[img]
                   {:params {:blur blur-region :crop crop-region} :img img})
                 nodes))
              [:div
               [:div "blur" [:input {:name "blur-region"}]]
               [:div "crop" [:input {:name "crop-region"}]]])
   (processor "description-parser-by-delimiters"
              (fn[nodes {:keys [selector]}]
                (->>
                 nodes
                 (mapcat (partial tree-seq map? :content))
                 (filter (fn[node]
                           (and (map? node)
                                (every? string? (:content node)))))
                 (map (fn[{:keys [tag attrs content]}]
                        (if (= (:style attrs)
                               "font-size: 11.0pt; font-family: Tahoma; color: red;")
                          {:type :special :content content}
                          content)))
                 flatten
                 create-map-from-lst))
              [:div
               "description-parser-by-delimiters"
               [:div "selector" [:input {:name "selector"}]]])])

(defn processor-by-name[name-to-find]
  (first (filter (fn[{:keys [name]}] (= name name-to-find)) processors)))

(defn apply-processors[parsing-config name nodes]
  (reduce (fn[nodes {:keys [type] :as config}]
            ((:fn (processor-by-name type)) nodes config))
          nodes
          (map second (sort-by first (get-in parsing-config [name :filters])))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Execute parser programmaticaly with given selectors and functions

(defn send-stock-map[category extra-info info
                     {:keys [name price params showcase description misc] :as stock}]
  (send-stock name (or (:category stock) category)
              (if (number? price) price (parsei (prepare-str price)))
              (download-strings showcase)
              (merge {[:collection false] [(text (:category extra-info))]} params)
              nil
              description
              :misc misc))

(defn resolve-fn[ns fn-name]
  (when ns
    (assert (re-find #"^[a-z]+$" ns))
    (require (symbol (str "gd.parsers.concrete." ns)))
    (resolve (symbol (str "gd.parsers.concrete." ns "/" fn-name)))))

(defn get-merge-fn[ns]
  (or
   (resolve-fn ns "merge-stocks")
   identity))

(defmacro execute-parser-by-config[mode stock-selectors stock-body
                                   & {:keys [name url stocks categories current-page
                                             stock-postprocessor-ns start-url next-page
                                             stock-merger-ns]}]
  `(let [post-fn#
         (or
          (resolve-fn ~stock-postprocessor-ns "process-stock")
          identity)

         custom-merge-fn#
         (get-merge-fn ~stock-merger-ns)]
     (case ~mode

       :whole
       (let [monitor# *progress-monitor*
             mode# *update-mode*]
         (doto (new Thread
                    (fn[]
                      (try
                        (binding [*ns* (create-ns (symbol ~name))
                                  *progress-monitor* monitor#
                                  *update-mode* mode#]

                          (use 'clojure.core)
                          (use 'gd.parsers.hlapi)
                          (use 'gd.parsers.llapi)

                          (prod)
                          (start ~url
                                 :stocks ~stocks
                                 :categories ~categories
                                 :start-url ~start-url
                                 :next-page ~next-page)

                          (stock [~'other] ~stock-selectors
                            (send-stock-map ~'other ~'extra-info ~'info (post-fn# ~stock-body)))

                          (deal ~name
                                {:constraints-type :min_quantity :min_quantity 5}
                                {:conditions ""
                                 :info ""}
                                :requests-throttle 10
                                :custom-merge-fn custom-merge-fn#))
                        (catch Throwable e#
                          (error e#))
                        (finally
                          (reset! monitor# nil)))))
           (.start)))

       :stock
       (post-fn# (html-parse ~current-page ~stock-selectors ~stock-body))

       :all-categories
       (html-parse ~current-page [categories# ~categories
                                  next-page# ~next-page]
                   (concat categories# next-page#))

       :all-stocks
       (html-parse ~current-page [stocks# ~stocks] stocks#))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; General parsing interface(build complete parser from config, or execute it partially)

(def always-params [:name :images :price :description])
(def custom-params [:images2 :color :size :collection :sizing-table :brand :category :price2])

(defn parse-using-config[page mode {:keys [name price price2
                                           images images2 brand
                                           description color
                                           available size collection
                                           start-url next-page category

                                           stock-processor stock-processor-enabled

                                           stock-merger stock-merger-enabled

                                           parser-name categories stocks]
                                    :as config}]
  ;; (execute-parser-by-config
  ;;  mode

  ;;  [stock-name (:sel name)
  ;;   stock-price (:sel price)
  ;;   stock-price2 (:sel price2)
  ;;   stock-images (:sel images)
  ;;   stock-images-secondary (:sel images2)
  ;;   stock-description (:sel description)
  ;;   params-color (:sel color)
  ;;   params-size (:sel size)
  ;;   collection (:sel collection)
  ;;   brand (:sel brand)
  ;;   category (:sel category)
  ;;   avaliability [(:sel available) text]]
  ;;  (when-not (and avaliability
  ;;                 (:regexp available)
  ;;                 (re-find
  ;;                  (re-pattern (:regexp available))
  ;;                  avaliability))
  ;;    {:name (prepare-str stock-name)
  ;;     :price (parsei stock-price)
  ;;     :category category
  ;;     :params (merge
  ;;              {:color (apply-processors config :color params-color)
  ;;               :size (apply-processors config :size params-size)}
  ;;              (when (seq collection)
  ;;                {[:collection false] (apply-processors config :collection collection)})
  ;;              (when (seq brand)
  ;;                {[:brand false] (apply-processors config :brand brand)}))
  ;;     :showcase (distinct (concat (apply-processors config :images stock-images)
  ;;                                 (apply-processors config :images2 stock-images-secondary)))
  ;;     :description (apply-processors config :description stock-description)
  ;;     :misc {:secondary-price (apply-processors config :price2 stock-price2)}})

  ;;  :name parser-name
  ;;  :start-url start-url
  ;;  :next-page next-page
  ;;  :stocks stocks
  ;;  :categories categories
  ;;  :current-page (:body page)
  ;;  :url current-site
  ;;  :stock-merger-ns (when (seq stock-merger-enabled) stock-merger)
  ;;  :stock-postprocessor-ns (when (seq stock-processor-enabled) stock-processor))
  )

(defn load-parser-or-config[container-id parser monitor supplier-mode sync & {:keys [dry-run] :or {dry-run true}}]
  (flush-cache)
  (when-not @monitor
    (if-let [[_ parser] (re-find #"config:(.*)" parser)]
      (binding[*progress-monitor* monitor
               *request-cache* (atom nil)
               *update-mode* {:container container-id
                              :supplier-mode supplier-mode
                              :dry-run dry-run}]
        (swap! monitor merge
               {:current-thread
                (parse-using-config nil :whole (read-json (slurp (str "resources/parser-description/" parser))))}))
      (load-single-parser container-id parser monitor dry-run supplier-mode))
    (when sync
      (wait-until-parser-stopped monitor))))
