(ns gd.parsers.view.server
  (:use org.httpkit.server
        gd.log
        gd.utils.common
        gd.parsers.view.pages
        gd.parsers.view.process))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main request processing function

(defn process-request[req]
  (try
    (cond
      (= (:uri req) "/stocks-rerender")
      (stocks-rerender req)

      (= (:uri req) "/stocks-fetch")
      (stocks-fetch req)

      (= (:uri req) "/stocks-merge")
      (stocks-merge req)

      (= (:uri req) "/save-config")
      {:body (try
               (store-config req)
               (save-config)
               "Saved!"
               (catch Exception e
                 (str "Error while saving : " (.getMessage e))))}

      (= (:uri req) "/load-config")
      (do (store-config req)
          (load-config))

      (re-find #"ui-bg_highlight-soft_100_eeeeee_1x100" (:uri req))
      nil

      true
      (let [url
            (str current-site (:uri req)
                 (when-let [q (:query-string req)]
                   (str "?" q)))

            response
            (load-or-get-from-cache url)

            response
            (->
             response
             (dissoc-in [:headers "content-length"])
             (dissoc-in [:headers "server"])
             (dissoc-in [:headers "transfer-encoding"])
             (dissoc-in [:headers "content-encoding"])
             (dissoc-in [:headers "vary"]))

            content
            (get (:headers response) "content-type")

            is-html?
            (when content (re-find #"text/html" content))

            cacheable?
            (when content
              (or (re-find #"image" content)
                  (re-find #"javascript" content)
                  (re-find #"css" content)))

            response
            (if cacheable?
              (->
               response
               (assoc-in [:headers "expires"] "Mon, 14 Jul 2014 19:16:52 GMT")
               (assoc-in [:headers "cache-control"] "max-age=31536000"))
              response)]
        (if (string? (:body response))
          (if is-html?
            (update-in response [:body] process-html)
            response)
          (update-in response [:body]
                     (fn[v] (when v (new java.io.ByteArrayInputStream v)))))))
    (catch Throwable e
      (print-seq "URL:" (:uri req))
      (error e)
      (throw e))))

(defonce
  server
  (do
    (run-server #'process-request
                {:port 9000
                 :thread 1
                 :queue-size 20480
                 :worker-name-prefix "parser-worker-"
                 :max-line 4096})
    (println "Parsing view server started at port 9000")))
