(ns gd.parsers.hlapi
  (:require [gd.model.model :as model])
  (:use
   gd.utils.web
   clojure.test
   clojure.core.cache
   gd.utils.test
   dk.ative.docjure.spreadsheet
   [clojure.java.shell :only (sh with-sh-dir)]
   [clojure.algo.generic.functor :only (fmap)]
   gd.utils.common
   gd.log
   gd.parsers.llapi
   [gd.parsers.common :exclude [attr auth set-basic-auth]]))

(comment "High Level API for parsers")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Test api
(defonce test-enabled (atom :prod))

(defn prod[]
  (reset! test-enabled :prod))

(defn dev[]
  (flush-cache)
  (reset! test-enabled :dev))

(defn dev-try[]
  (reset! test-enabled :try))

(defn dev-merge[]
  (reset! test-enabled :merge))

(defn dev-single-stock-with-merge[]
  (reset! test-enabled :single-stock-with-merge))

(defn dev-all-stocks-merge
  "Just like (prod) but don't create deal. Also output a lot of usefull
information for merging. What positions was not merged, different numbers and so
on."
  []
  (reset! test-enabled :dev-all-stocks-merge))

(defn take-test[type items]
  (case @test-enabled
    (:merge :dev)
    (case type
      :category (take 1 items)
      :stock (take 3 items)
      :stock-price items)

    :try
    (case type
      :category (take 2 items)
      :stock (take 10 items)
      :stock-price items)

    :single-stock-with-merge
    (case type
      :stock-price items
      nil)

    items))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Create description visualized as table from set of tr
(defn create-table-description[trs]
  (assert (every? (fn[tr](= :tr (:tag tr))) trs)
          "You should pass list of trs")
  {:rows (map (fn[cells]
                (remove nil? (map text cells)))
              (map :content trs))})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Work with price parsing results
(defn- check-stocks-keys[stock]
  (if (= :merge @test-enabled)
    true
    (and
     (every?
      (partial contains? stock)
      [:name :price :fkey_category :description :params :images])
     (:price stock)
     (:name stock)
     (:params stock))))

(defn- merge-price-results[merge-fn collected-map]
  (if (= (keys collected-map) [:items])
    (map #(dissoc % :price-key) (:items collected-map))
    (doall
     (->>
      collected-map
      vals
      (remove nil?)
      (map (comp (partial fmap first) (partial group-by :price-key))) ;group each collection(price,item) by price-key
      (reduce (partial deep-merge-with (fn[& args]
                                         (if (= @test-enabled :single-stock-with-merge)
                                           (println "Merging :" (vec (remove nil? args))))
                                         (apply merge-fn args))))
      (map second)
      vec
      (filter check-stocks-keys)
      (map #(if (#{:merge :dev-all-stocks-merge} @test-enabled) % (dissoc % :price-key)))))))

(def merge-all-results (partial merge-price-results (fn[& args] (some identity args))))

(deftest common-merge-spec
  (let [stock (fn[name price & [price-key]]
                (merge {:name name :price price :fkey_category 2 :description 3 :params 5 :images 6}
                       (when price-key {:price-key price-key})))
        items-from-site [(stock "a" nil 1) (stock "b" nil 2) (stock "c" nil 3)]
        items-from-price [{:price 10 :price-key 1} {:price 20 :price-key 2}]]
    (with-redefs [test-enabled (atom :prod)]
      (is
       (= (merge-all-results {:items items-from-site :price items-from-price})
          [(stock "a" 10) (stock "b" 20)])))))

(def override-all-results (partial merge-price-results (fn[& args] (last (remove nil? args)))))

(deftest common-override-spec
  (let [stock (fn[name price & [price-key]]
                (merge {:name name :price price :fkey_category 2 :description 3 :params 5 :images 6}
                       (when price-key {:price-key price-key})))
        items-from-site [(stock "a" 1 1) (stock "b" 2 2) (stock "c" 3 3)]
        items-from-price [{:price 10 :price-key 1} {:price 20 :price-key 2}]]
    (with-redefs [test-enabled (atom :prod)]
      (let [expected [(stock "a" 10) (stock "b" 20) (stock "c" 3)]]
        (is
         (= (override-all-results {:a items-from-site :b items-from-price}) expected))

        (testing "order of keys for merging does not affect result - more rich
 entities, have lower priority"
          (is
           (= (override-all-results {:b items-from-site :a items-from-price}) expected)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Parsing api
(defmacro auth[source form-selector params]
  `(gd.parsers.common/auth ~source ~form-selector ~params))

(defn- download-files[links]
  (remove nil? (map download-file (remove empty? (distinct links)))))

(defn download*
  "Smart function for downloading different type of links or images. With ability to set
special keys for procesing. "
  [keys & elements]
  (let [files
        (download-files
         (map! (fn[element]
                 (cond (string? element)
                       element

                       (map? element)
                       (case (:tag element)
                         :a (attr :href element)
                         :img (attr :src element))))
               (flatten elements)))]
    (map! (fn[path]{:path path :params keys}) files)))

(defn download-strings
  "Download list of strings with each string maybe annotated with params"
  [links]
  (remove nil?
          (map (fn[link]
                 (cond (map? link)
                       (let [{:keys [params img]} link
                             already-exists (check-file-already-downloaded img)
                             path (download-file img)]

                         ;; if file is just downloaded we must apply some filters(blur)
                         (when (and (not already-exists) (seq (:blur params)))
                           (convert path path
                                    "(" "-clone" "0" "-blur" "15x15" ")"
                                    "(" "-clone" "0" "-gamma" "0" "-fill"
                                    "white" "-draw" (str "rectangle " (:blur params)) "-blur" "2x2" ")"
                                    "-composite"))

                         (when (seq path)
                           {:path path
                            :params params}))

                       (string? link)
                       {:path (download-file link)}))
               (remove empty? (distinct links)))))

(defn download
  "Smart function for downloading different type of links or images."
  [& elements]
  (download* {} elements))

(defn follow-links[category links current-info]
  (map! #(emit category (attr :href %) (merge {category %} current-info)) links))

(defn showcase
  "Function that extracts photos from stock to be used in group deal showcase"
  [items & indexes]
  (map #(first (:images (nth items %))) indexes))

(def pages-visited (atom 0))

(def php-errors ":contains('Could not execute query to database'):last, b:contains(Warning), :contains('more than \\'max_user_connections\\' active connections'):last")

(defmacro start[url & {:keys [start-url categories next-page
                              stocks stocks-elements stocks-nested-links
                              skip-errors]}]
  (do
    (when (string? url)
      (let [path (.getPath (new java.net.URL url))]
        (when-not (or (= path "/") (= path ""))
          (throwf "Not absolute domain url! You should use 'start-url' parameter if want to change
start page for fetching."))))
    `(do
       (reset! consumers {})
       (reset! stopped nil)
       (main-url ~url)

       ~(when categories
          `(page ~(or start-url url)
                 [categories# ~categories]
                 (println *ns* "Found " (count categories#) " categories")
                 (if (zero? (count categories#))
                   (force-stop))
                 (if (= @test-enabled :dev)
                   (clojure.pprint/pprint (map (attr :href) categories#)))
                 (follow-links :category (take-test :category categories#) nil)))

       ~(when (or stocks stocks-elements)
          `(page :category
                 [stocks# ~(or stocks stocks-elements)
                  [next-page# :as next-pages#] ~next-page
                  title# ["title" text]
                  errors# php-errors]

                 ;; check for errrors
                 (when-not ~skip-errors
                   (when (some
                          identity
                          [(and (= 0 (count stocks#))
                                (do (warn "Page does not have stocks! Maybe error " ~'info) true))
                           (and (empty? title#)
                                (do (warn "Page does not have title! Mayber error " ~'info) true))
                           (and (not (empty? errors#))
                                (do (warn "Page have PHP errors! " ~'info) true))])
                     (save-current-page)))

                 ;; move throught the pager
                 (when next-page#
                   (if (= (:tag next-page#) :a)
                     (do
                       (when (= @test-enabled :dev)
                         (info "Found next page on page " ~'info "to" (attr :href next-page#)))
                       (->>
                        next-pages#
                        (map (attr :href))
                        distinct
                        (map! (fn[link#]
                                (when (or (#{:prod :dev-all-stocks-merge} @test-enabled) (< @pages-visited 3))
                                  (swap! pages-visited inc)
                                  (let [url#
                                        (or
                                         (resolve-relative-url (attr :href (:category ~'extra-info)) link#)
                                         (if (or (.startsWith link# "/") (.startsWith link# "http://"))
                                           link#
                                           (str (attr :href (:category ~'extra-info)) link#)))]
                                    (emit :category url# ~'extra-info)))))))
                     (warn "Next-page element is not link")))

                 ;; extract list of stocks
                 (cond ~stocks
                       (if ~stocks-nested-links
                         (map! #(let [link-to-follow# (~(or stocks-nested-links identity) %)]
                                  (when link-to-follow#
                                    (emit :stock-raw
                                          (attr :href link-to-follow#)
                                          [% ~'extra-info])))
                               (take-test :stock stocks#))
                         (follow-links :stock-raw (take-test :stock stocks#) ~'extra-info))

                       ~stocks-elements
                       (map! #(emit :stock-raw % ~'extra-info) (take-test :stock stocks#))))))))

(defmacro stock[categories vars & body]
  `(let [{:keys [~@categories]}
         ~(reduce merge
                  (map (fn[cat] {(keyword cat) `(:id (model/category:get-by-key ~cat))})
                       (map str categories)))]
     (page :stock-raw ~(vec (concat vars
                                    '[check-title ["title" text]
                                      check-errors php-errors]))
           (do
             (when (some
                    identity
                    [(and (empty? ~'check-title)
                          (do (warn "Page does not have title! Mayber error" ~'info) true))
                     (and (not (empty? ~'check-errors))
                          (do (warn "Page have PHP errors!" ~'info) true))])
               (save-current-page))
             ~@body))))

(defmacro stock-element[categories vars & body]
  `(let [{:keys [~@categories]}
         ~(reduce merge
                  (map (fn[cat] {(keyword cat) `(:id (model/category:get-by-key ~cat))})
                       (map str categories)))]
     (element :stock-raw ~vars (do ~@body))))

(def default-price-plan [[400 1.2] [750 1.15] [nil 1.1]])

(declare global-merge-fn)

(defmacro deal[name deal-info description & {:keys [price-plan requests-throttle override
                                                    pause-each pause-for custom-merge-fn
                                                    supplier-mode]}]
  `(do
     (reset! visited-pages nil)
     (reset! pages-visited 0)

     (reset! stored-prices nil)

     ~(let [merge-fn (if override 'override-all-results 'merge-all-results)]
        (reset! global-merge-fn (resolve merge-fn))
        (case @test-enabled
          (:dev :single-stock-with-merge)
          `(execute-all)

          :merge
          `(do
             (collect :stock []
                      (clojure.pprint/pprint  ~'collected-map))
             (execute ~name))

          (:dev-all-stocks-merge)
          `(do
             (collect :stock []
                      (let [items# (~merge-fn ~'collected-map)
                            stocks# (set (map :price-key (:items ~'collected-map)))
                            prices# (set (map :price-key (:prices ~'collected-map)))
                            stock-by-key# (group-by :price-key items#)
                            [only-in-a# only-in-b# in-both#] (clojure.data/diff stocks# prices#)]
                        (info "Deal stocks after merge : "
                              "matched" (count in-both#)
                              "merged" (count items#)
                              "from price" (count (:prices ~'collected-map)))
                        (clojure.pprint/print-table
                         [:stock-key :price-key :stock]
                         (concat
                          (map (fn[both#]
                                 {:stock-key both#
                                  :price-key both#
                                  :stock (seq (map (juxt :name :price) (get stock-by-key# both#)))})
                               in-both#)
                          (map (fn[both#]{:stock-key both# :price-key nil}) only-in-a#)
                          (map (fn[both#]{:stock-key nil :price-key both#}) only-in-b#)))
                        (info "Deal stocks after merge : "
                              (count items#)
                              (count (:prices ~'collected-map)))))
             (execute ~name))

          `(do
             (collect :stock []
                      (let [items# (~merge-fn ~'collected-map)
                            items# ((or ~custom-merge-fn identity) items#)]
                        (info "Deal stocks after merge : " (count items#) (count (:prices ~'collected-map)))
                        (info "Update mode:" *update-mode*)
                        (try
                          (stop)
                          (catch Exception e#))
                        (if *update-mode*
                          (model/group-deal:update-stocks (:container *update-mode*)
                                                          (distinct items#)
                                                          :dry-run (:dry-run *update-mode*)
                                                          :supplier-mode (:supplier-mode *update-mode*))
                          (if (> (count items#) 3)
                            (model/group-deal:create
                             {:fkey_user (:id (model/user:get-by-name "admin"))
                              :name ~name
                              :short-name (translit ~name)
                              :visibility :whole
                              :deal-info ~deal-info
                              :description (merge {:payment ""
                                                   :location []
                                                   :short-conditions (:conditions ~description)
                                                   :short-info (:info ~description)}
                                                  ~description)
                              :images (showcase items# 0 1 2)
                              :stock (distinct-by (comp set :images) items#)})
                            (warn "Number of stocks is too small. Found only : " (count items#) "stocks")))))
             (execute ~name ~requests-throttle ~pause-each ~pause-for))))))

(defn parse-xls-by-groups[path groups keep-partial]
  (let [groups
        (if (sequential? groups)
          groups
          (list groups))

        sheets
        (sheet-seq (load-workbook path))

        result
        (->> groups
             (mapcat (fn[group]
                       (mapcat (fn[data](select-columns group data)) sheets)))
             (remove nil?))]
    (take-test :stock-price
               (if keep-partial
                 result
                 (remove #(some (comp nil? second) %) result)))))

(defn parse-xls-full[path]
  (let [sheets (sheet-seq (load-workbook path))]
    (take-test :stock-price
               (mapcat (fn[sheet](map (comp (partial map read-cell) cell-seq) (row-seq sheet))) sheets))))

(defmacro price[url {:keys [columns skip keep-partial] :or {skip 0}} & body]
  `(file
    ~url [path#]
    (let [~'rows (parse-xls-by-groups path# ~columns ~keep-partial)]
      (do ~@body))))

(defmacro price-full[url & body]
  `(file
    ~url [path#]
    (let [~'rows (parse-xls-full path#)]
      (do ~@body))))

(defn remove-nil-entries[map-input]
  (let [with-removed-entities (map (fn[[k v]]
                                     (let [v (remove nil? v)]
                                       (when (and v (seq v))
                                         {k v})))
                                   map-input)]
    (or (reduce merge with-removed-entities) {})))

(defn normalize-description[map-or-string-input]
  (let [process-key
        (fn[key]
          (if (keyword? key)
            key
            (-> key
                (.replaceAll ":[ ]*$" "")
                clojure.string/capitalize)))]
    (cond (string? map-or-string-input)
          map-or-string-input

          (map? map-or-string-input)
          (or (reduce merge (map (fn[[k v]] {(process-key k) v}) map-or-string-input)) {})

          true map-or-string-input)))

(defmacro send-stock[name category & [price showcase params short-description description &
                                      {:keys [price-key additional misc raw-description]}]]
  `(do
     (assert (or ~name ~price-key) "Name is null, and will not be fetched later")
     (assert (or ~price ~price-key) "Price is null, and will not be fetched later")
     (assert ~category "Category is null")
     (assert (or (nil? ~price-key) (number? ~price-key) (string? ~price-key) (keyword? ~price-key))
             "Price key should be number, string, or keyword")
     (let [images# ~showcase]
       (when (seq images#)
         (let [stock# {:name ~name
                       :price ~price
                       :fkey_category ~category
                       :description (normalize-description ~description)
                       :params (remove-nil-entries ~params)
                       :images (doall images#)
                       :price-key ~price-key
                       :meta-info (merge (when (string? ~'info) {:url (absolutize-url ~'info)})
                                         (when ~additional {:additional-windows ~additional})
                                         (when ~raw-description {:description
                                                                 (extract-content-elements ~raw-description)})
                                         ~misc)}]
           (if (= @test-enabled :single-stock-with-merge)
             (prn ~price-key (@global-merge-fn {:items [stock#] :prices @stored-prices}))
             (emit :stock stock#)))))))

(defmacro send-stock-simple[name price images & [params description &
                                                 {:keys [price-key additional misc raw-description]}]]
  `(send-stock (prepare-str ~name)
               ~'other
               (let [price# ~price]
                 (if (number? price#) price# (parsei (prepare-str price#))))
               (download ~images)
               ~params
               nil
               ~description
               :price-key ~price-key
               :additional ~additional
               :misc ~misc
               :raw-description ~raw-description))

;; Used for test merging of single stock with prices
(defonce stored-prices (atom nil))
(defonce global-merge-fn (atom nil))

(defn send-price
  ([key item]
     (if (= @test-enabled :single-stock-with-merge)
       (swap! stored-prices conj (merge {:price-key key} item))
       (emit [:stock :prices] (merge {:price-key key} item))))
  ([key name price]
     (send-price key {:name name :price price})))

(defn send-raw[data & [extra-info]]
  (flush-single-page data)
  (reset! gd.parsers.common/visited-pages {})
  (emit :stock-raw data {:category (list extra-info)}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Small helpers for common tasks
(defn parse-size[size-string & [default-start default-end]]
  (if size-string
    (rangei (parsei size-string) (parsei size-string 1))
    (rangei default-start default-end)))

(defn range-sizes[from to]
  (let [to to
        from (re-parse #"([SMXL]+)" from)]
    (concat
     (->> ["XS" "S" "M" "L" "XL" "XXL" "XXXL"]
          (drop-while (partial not= from))
          (take-while (partial not= to)))
     [to])))

(defn parse-size-complex[size]
  (let [size (prepare-str size)]
    (cond-let [sizes]

      (re-parse #"([SMLX]+-[SMLXS]+)" size)
      (let [[from to] (.split sizes "-")]
        (range-sizes from to))

      (re-parse #"(универсальный|[SMLXS]+)" size)
      [sizes]

      (re-parse #"([0-9]+-[0-9]+)" size)
      (let [[from to] (map parsei (.split sizes "-"))]
        (assert (and (> from 0) (> to 0)) (str "Bad sizes after parsing " size))
        (rangei from to))

      (re-parse #"([0-9, ]+)" size)
      (map parsei (.split sizes "[ ]*,[ ]*"))

      (= nil size)
      nil

      true
      (throwf "Unrecognized size template : %s" size))))

(defn find-after[strings word]
  (second (drop-while (fn[w] (not (re-find word w))) strings)))

(defn remove-after[strings word]
  (let [[fst snd] (split-with (fn[w] (not (re-find word w))) strings)]
    (concat fst (drop 2 snd))))

(defn params-price-to-difference[params-list]
  (if (empty? params-list)
    {:price 0 :params []}
    (let [base (reduce min (map second params-list))]
      {:price base
       :params (map (fn[[n price & other]] (vec (concat [n (- price base)] other))) params-list)})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Simplier api

(defmacro common-deal[name & [throttle]]
  `(deal ~name
         {:constraints-type :min_sum :min_sum 1000}
         {:conditions "" :info ""}
         :requests-throttle (or ~throttle 10)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Prom parser

(defmacro prom-parser[url deal-name one-of-category-name]
  `(do
     (start ~url
            :categories ~(str "li:contains(" one-of-category-name ") > ul a")
            :stocks ".b-product-line:contains('В наличии') a[id*=link_to_product]"
            :next-page "a:contains(→),.b-categories-tails__header a")

     (stock [~'other]
         [name# [".b-product__name" text]
          avaliability# [".b-product__state_type_available" text]
          price# ".b-product__price:not(.b-product__price_type_old)"
          desc-raw# [".b-user-content"]
          collection# ["div.b-breadcrumb__item_state_visible:last a" text]
          images# ["a.b-centered-image"]]

       (when (re-find #"В наличии" avaliability#)
         (let [desc# (joinn (map text desc-raw#))
               params# {[:collection false] [collection#]}]
           (when-not (empty? (download images#))
             (send-stock name# ~'other
                         (parsei price#)
                         (download images#)
                         params#
                         nil desc#
                         :raw-description desc-raw#)))))

     (deal ~deal-name
           {:constraints-type :min_quantity :min_quantity 5}
           {:conditions ""
            :info ""}
           :requests-throttle 1)))

(run-tests)
