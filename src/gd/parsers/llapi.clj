(ns gd.parsers.llapi
  (:refer-clojure :exclude [extend])
  (:use gd.utils.common
        gd.utils.test
        clojure.test
        hiccup.core
        clj-time.core
        [gd.parsers.common :exclude [attr set-basic-auth]]
        [clojure.java.shell :only (sh with-sh-dir)])
  (:require [clojure.string :as str]
            [gd.parsers.jquery-parser :as jq]
            [gd.model.model :as model])
  (:import java.util.Scanner
           java.util.Locale
           (org.apache.http.impl.cookie BasicClientCookie2)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ## Utils

(def attr gd.parsers.common/attr)
(def child jq/child)
(def stop jq/stop)
(def set-basic-auth gd.parsers.common/set-basic-auth)

(defmacro relative-to-current[url]
  `(.getPath (new java.io.File (.getParentFile (new java.io.File ~'info)) ~url)))

(defn rangei
  "Range with right inclusive"
  ([from to step]
     (cond
       (or (not from) (= from 0)) (list to)
       (or (not to) (= to 0)) (list from)
       (and from to) (range from (+ 1 to) step)))
  ([from to]
     (rangei from to 1)))

(defn get-category-id [cat]
  (:id (model/category:get-by-key cat)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ## String utils

(defn- trim-nil
  "Trim string, and if result is empty - return nil."
  [str]
  (let [trimmed (str/trim str)]
    (if (empty? trimmed)
      nil
      trimmed)))

(defn- normalize
  "Trim, remove strange symbols from supplied line, or in case of seq - from each line.
Also remove empty lines."
  [lines]
  (if (sequential? lines)
    (remove empty? (map normalize lines))
    (when lines
      (-> lines
          (str/replace #"(?m)[^а-яА-Яa-zA-Z0-9!їіє@#$%^&*()+-\|/`~Ёё.,:;'\" ]+" " ")
          (str/replace #"(?m)[\u00a0 \t\n]+" " ")
          trim-nil))))

(def text (comp normalize jq/text))
(def texts (comp (partial map normalize) jq/texts))

(defn prepare-str
  "Extract text node and normalize it"
  [el]
  (if (string? el)
    (normalize el)
    (-> el
        (text)
        normalize)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ## Regexp parsers

(defn re-parse
  "Function for parsing single match group.

1. Always return first matched group.
2. Can skip matches(use version with integer parameter).
3. Automaticaly extract text from supplied element(you can just pass matched element).
4. In case when used with single argument - create function with one argument(element).
"
  ([regexp]
     (fn[element](re-parse 0 regexp element)))
  ([regexp element]
     (re-parse 0 regexp element))
  ([n regexp element]
     (let [n (or n 0)
           string (prepare-str element)]
       (when string
         (let [groups (re-seq regexp string)]
           (if (> (count groups) n)
             (second (nth groups n))))))))

(defn re-parse-multi
  "Use regexps for parsing given string, and return all groups concatenated."
  ([regexp element]
     (str/join " " (rest (re-find regexp (prepare-str element))))))

(defn re-parse-groups
  "Use regexps for parsing given string, and return all groups as list."
  ([regexps element]
     (map (fn[regexp](second (re-find regexp (prepare-str element)))) regexps)))

(defn re-split
  ([string]
     (if-let [str (prepare-str string)]
      (re-split #"[ ]*,[ ]*" str)))
  ([regexp string]
     (if-let [str (prepare-str string)]
       (str/split str regexp)))
  ([regexp string num]
     (nth (str/split (prepare-str string) regexp) num)))

;; # Example usage
(deftest re-parse-tests

  (are [x y] (= x y)

       ;; Single argument usage
       (map (re-parse #"([0-9]+)") ["a1" "b2" "c3"]) ["1" "2" "3"] ; -> [1 2 3]

       ;; Two argument usage
       (re-parse #"([0-9]+)" "artem123") "123"

       ;; Three argument usage
       (re-parse #"([0-9]+)" "123 456") "123"

       ; mention that 123 is skiped, number starts from 0
       (re-parse 1 #"([0-9]+)" "123 456") "456"

       ;; i use here lazy matching .*? to avoid capturing everything after first
       ;; match
       (re-parse-multi #"([0-9]+).*?([0-9]+)" "123 hello 456 world 789") "123 456"

       (re-parse-groups [#"([0-9]+)" #"(artem)"] "123 artem 456") ["123" "artem"]

       ;; split test
       (re-split "artem,barmin") ["artem" "barmin"]
       (re-split #":" "artem:barmin") ["artem" "barmin"]))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ## Common string utilites

(defn remove-words[string & words]
  (reduce (fn[result word](.replaceAll result (str "(?iu)" word) "")) string words))

(defn- grep-common[fun lines regexps]
  (fun (fn[line] (some (fn[r](re-find r line)) regexps))
       (normalize lines)))

(defn grepv
  "Inverse match"
  [lines & regexps]
  (grep-common remove lines regexps))

(defn grep
  "Match lines"
  [lines & regexps]
  (grep-common filter lines regexps))

(defn grep-take
  "Take while each line is not matched with regexp"
  [lines & regexps]
  (grep-common (fn[pred coll](take-while #(not (pred %)) coll)) lines regexps))

(defn grepo
  "Like grep -o - keep only part of line, that are match with regexp. First matched
regexp is used. Each regexp must have at least one capture group"
  [lines & regexps]
  (map
   (fn[line](some #(re-parse 1 % line) regexps))
   (grep-common filter lines regexps)))

(defn parsed
  "Parse double in flexible form(. or , - does not matter)"
  [d-str & [nth :as full]]
  (let [replace (fn[s match replace]
                  (when s (str/replace s match replace)))
        nth (first (filter integer? full))]
    (if (and d-str (prepare-str d-str))
      (let [number (re-parse nth #"([0-9]+[.,]?[0-9]*)" d-str)]
        (if number
          (let [scanner (-> (new Scanner (.replaceAll number "," "."))
                            (.useLocale Locale/US))]
            (when (.hasNextDouble scanner)
              (.nextDouble scanner)))
          nil))
      nil)))

(defn parsei
  "Parse first int in given string. If number can't be parsed - return 0.

Arguments : string to parse and list of modifiers.

1. nth - how many numbers to skip before return result(starts from 0)
2. thouthands - treat spaces not as number separator, but rather decimal groups of single number
3. null - instead of 0 will return null
"
  [int-str & [nth thouthands null signed :as full]]
  (let [replace (fn[s match replace]
                  (when s (str/replace s match replace)))
        thouthands (some #{:thouthands} full)
        null (some #{:null} full)
        signed (some #{:signed} full)
        nth (first (filter integer? full))
        null-result (if null nil 0)]
    (if (and int-str (prepare-str int-str))
      (let [regex (re-pattern (str "(" (when signed "[-+]?")
                                   "[0-9" (when thouthands " ") "]+)"))
            number (if thouthands
                     (replace (re-parse nth regex int-str) #"[ ]+" "")
                     (re-parse nth regex int-str))]
        (if number (Long/parseLong number) null-result))
      null-result)))

(defn parsei-all[int-str & args]
  (take-while identity (map #(apply parsei int-str % :null args) (range))))

;; # Example of usage
(deftest parsei-test
  (are [x y] (= x y)
       (parsei "123 456") 123
       (parsei "123 456" 1) 456 ; (first number skipped)
       (parsei "artem") 0 ; (default to zero)
       (parsei "artem" :null) nil ; (default to null)
       (parsei "123 456" :thouthands) 123456 ;  (use space as decimal groups separator)

       (parsei "123-456" :signed) 123
       (parsei "123-456" 1) 456
       (parsei "123-456" 1 :signed) -456 ;parse possibly signed number
       (parsei "-123" :signed) -123 ;parse possibly signed number
       (parsei "123-456") 123))

(defn joinn
  "Join strings with newline"
  [lines]
  (str/join "\n" lines))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ## Image utils

(defn convert[src dst & params]
  (apply sh (concat ["convert" src] params [dst]))
  dst)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ## Description parsing

(defn parse-description[lines config]
  (loop [[[section regexps field-delim field-fun] & config] config
         lines lines
         res nil]
    (let [regx (map #(re-pattern (str "(?uism).*" % ".*")) regexps)
          match (apply grep lines regx)
          rest (apply grepv lines regx)
          result-part (when-not (empty? match)
                        {section
                         (map str/trim
                              (if field-delim
                                (let [fields (str/split (reduce str match) (re-pattern field-delim))]
                                  (if (> (count fields) 1)
                                    [((or field-fun last) fields)]))
                                match))})
          result (cons result-part res)]
      (if (empty? config)
        (reduce merge (remove empty? (cons {:other rest} result)))
        (recur config
               rest
               result)))))

(defn- fancy-string[str]
  (-> str
      (str/replace #"(?sm)," ", ")  ;space after coma
      (str/replace #"(?sm)%" "% ")))  ;space after percent

(defn- prepare-description
  "Split into sentences, also can take until first match of 'until' element."
  [description-element & {:keys [sentence until] :or {sentence #"(?m)[\n.]+"}}]
  (->>
   (if (sequential? description-element)
     (map jq/text description-element)
     (list (jq/text description-element)))
   (mapcat #(str/split % sentence))
   (map fancy-string)
   (map normalize)
   (#(if until (grep-take % until) %))
   (remove nil?)))

(defn parse-desc[description-element config & keys]
  (when-not (empty? description-element)
    (assert (or (and (map? description-element) (:tag description-element))
                (and (sequential? description-element) (every? :tag description-element)))
            "Should be html element")
    (let [prepared (apply prepare-description description-element keys)]
      (parse-description prepared config))))

(def common-names {:colors "Цвета"
                   :color "Цвет"
                   :fabric "Ткань"
                   :size "Размер"
                   :number "Артикул"})

(defn- transform-default-names[desc names]
  (reduce merge (map (fn[[k v]] {(or (get (merge common-names names) k) k) v}) desc)))

(defn render-desc
  "Render parsed description as definition list. And append 'other' to the end
  as paragraph."
  [desc & {:keys [short-keys other ignore names] :or {other :other}}]
  {:pre [(or (not ignore) (sequential? ignore))]}
  (let [other-val (remove nil? (flatten [(when-not (= other :other) (get desc other)) (:other desc)]))
        desc (apply dissoc desc :other other ignore)]
    {:short (transform-default-names (if short-keys (select-keys desc short-keys) desc) names)
     :full (transform-default-names (assoc desc :other other-val) names)}))

(defn process-desc[desc config & [short-keys other ignore sentence until names :as keys]]
  (apply render-desc (apply parse-desc desc config keys) keys))

(defn classify[config value]
  (let [res (first (keys (remove #(empty? (second %))
                                 (parse-desc {:tag :div :content [value]} config))))]
    (when (and res (not= res :other))
      res)))

(defn extract-content-elements
  "Extract all strings. Preserve division by elements."
  [dsc]
  (->>
   (mapcat (partial tree-seq map? :content) dsc)
   (filter string?)
   (map normalize)
   (remove empty?)))

(defn extract-all-content-elements
  "Extract all strings. Preserve division by elements. Keep empty elements."
  [dsc]
  (->>
   (mapcat (partial tree-seq map? :content) dsc)
   (filter string?)
   (map normalize)))

;; TODO : write unit tests for description parsing

(deftest description-formatter-test
  (is
   (=
    {:short {:a "1"}, :full {:a "1" :other ["Hello"]}}
    (render-desc {:general "Hello" :a "1"}
                 :other :general
                 :ignore [:size])))

  (is
   (=
    {:short {:a "1" "Цвет" 1}, :full {:a "1" :other [] "Цвет" 1}}
    (render-desc {:a "1" :color 1 :size "a"}
                 :ignore [:size])))

  (is
   (=
    {:short {:a "1"}, :full {:a "1" :other (list "Hello" "2")}}
    (render-desc {:general "Hello" :a "1" :other "2"}
                 :other :general
                 :ignore [:size])))

  (is
   (=
    {:short {"Верх" ["кожа натуральная"] "Подкладка" ["натуральная"]}
     :full {"Верх" ["кожа натуральная"] "Подкладка" ["натуральная"] :other ["Туфли мужские"]}}
    (render-desc {"Верх" ["кожа натуральная"] "Подкладка" ["натуральная"]
                  :sizes ["40-45"] :other ["Туфли мужские"]}
                 :ignore [:sizes]))))

(deftest classify-test
  (is (nil? (classify [[:female ["жен" "серьги"]]] "привет")))

  (let [class #(classify [[:female ["жен" "серьги"]] [:male ["муж" "ремни"]]] %)]
    (is (= :female (class "серьги")))
    (is (= :male (class "очень ремни большие")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cookie creation functions

(defn add-cookie[name value & [path]]
  (.addCookie
   (get-cookie-storage)
   (doto (BasicClientCookie2. name value)
     (.setDomain (.replaceAll (.replaceAll (get @base-urls *ns*) "^http://" "") "/$" ""))
     (.setExpiryDate (.toDate (plus (now) (months 1))))
     (.setPath (or path "/"))))
  (prn (get-cookie-storage)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ## Sizing table formatting functions

(defn row
  "Helper for table row, used in sizing table."
  [& items]
  [:tr (map #(vector :td (if (= % "none") "" %)) items)])

(defn list-row
  "Helper for list item, used in sizing table."
  [name text]
  [:li [:span.list-row-name name] " - " [:span.list-row-text text] "."])

(defn clothes-how-to-measure []
  [:ol
   (list-row "Рост" "измеряем по прямой линии от макушки до пят, стоя без обуви ступни вместе")
   (list-row "Обхват груди" "измеряем горизонтально по выступающим точкам груди вокруг тела")
   (list-row "Обхват под грудью" "измеряем горизонтально под грудью")
   (list-row "Обхват талии" "измеряем горизонтально в наиболее узком месте туловища")
   (list-row "Обхват бедер" "измеряем горизонтально по самым выступающим местам ягодиц")
   (list-row "Длина по боковому шву" "измеряем вдоль бедер, от талии до пят")])

(defn sizes-table[header & rows]
  (let [row-size (count header)
        rows (if (and (= (count rows) 1) (string? (first rows)))
               (str/split (first rows) #"[ \n\t]+")
               rows)]
    [:table
     (apply row header)
     (map #(apply row %) (partition row-size rows))]))

(deftest sizes-table-spec
  (is (= (html
          (sizes-table
           ["Одежда Россия/Украина" "Одежда Европа" "Джинсы Европа" "Объем бедер"]
           "40-42 none 25 85-87
            40-41  none none 12 "))
         (html
          [:table
           [:tr
            [:td "Одежда Россия/Украина"]
            [:td "Одежда Европа"]
            [:td "Джинсы Европа"]
            [:td "Объем бедер"]]
           [:tr
            [:td "40-42"]
            [:td ""]                    ;none - is ignored
            [:td "25"]
            [:td "85-87"]]
           [:tr
            [:td "40-41"]
            [:td ""]
            [:td ""]
            [:td "12"]]])))

  (is (= (html [:table
                [:tr
                 [:td "Сантиметры"]
                 [:td "Россия"]
                 [:td "Европа"]
                 [:td "США"]]
                [:tr [:td 1] [:td 2] [:td 3] [:td 4]]
                [:tr [:td 5] [:td 6] [:td 7] [:td 8]]])
         (html
          (sizes-table ["Сантиметры" "Россия" "Европа" "США"]
                       1 2 3 4
                       5 6 7 8
                       9                ; not aligned row is ignored
                       )))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ## Tests for parsers

(deftest basic-html-parse
  (html-parse (slurp "test/resources/html/basic.html")
              [divs :div.a
               divs-text [:div.a text]
               divs-texts [:div.a texts]
               [img] ["img"]

               ;; description lists
               lst-dt "#lists dt"
               lst-dd "#lists dd"
               strong :strong

               [russian-link] :a.colorbox

               ;; check 'not' selector
               hello "hello:not([class])"]
              (are [x y] (= x y)
                   (count divs) 2
                   divs-text "Hello World"
                   divs-texts (list "Hello" "World")
                   (attr :src img) "hello"

                   (count lst-dt) (count lst-dd)

                   (count strong) 1

                   (attr :href russian-link) "/image/data/Зима_12_13/_MG_4220.jpg"

                   (count hello) 1)))

(deftest processing-html
  (html-parse (slurp "test/resources/html/processing.html")
              [labels "div#options label"
               doubles "div#price"
               types ["div.option:contains(Тип покупки:)"]
               multi-lines ["span:contains(Тип теста:)" text]]
              (are [x y] (= x y)

                   ;; несколько чисел в наборе узлов(работаем с каждым отдельно)
                   (map parsei labels) [51 52 53]

                   ;; число в наборе узлов(работаем со всеми сразу)
                   (parsei labels) 51
                   (parsei labels 1) 50 ;(+50грн)
                   (parsei labels 2) 52
                   (parsei labels 3) 53

                   ;; парсинг одного узла
                   (parsei (first labels)) 51
                   (parsei (first labels) 1) 50

                   ;; парсинг отрицательных чисел в наборе нод
                   (parsei types :signed) -50

                   ;; парсинг дробных чисел
                   (parsed doubles) 1.35
                   (parsed doubles 1) 1.56
                   (parsed doubles 2) nil

                   ;; парсинг фразы в наборе узлов
                   (re-parse #"(опт\(.*?\))" types) "опт(-50 грн.)"

                   ;; много переносов
                   multi-lines "Тип теста: привет")))

(deftest child-selectors
  (html-parse (slurp "test/resources/html/childs.html")
              [labels [".parent" (child ".relative-child a" ".text")]]
              (do
                (is (= (count labels) 2))
                (is (= (count (first labels)) 3))
                (is (= (count (second labels)) 3))

                (let [[parent1 a1 text-to-parse] (first labels)]
                  (is (= (text a1) "Test1"))
                  (is (= (text text-to-parse) "text to parse"))
                  (is (= (attr :href (first a1)) "test1"))
                  (is (= (text parent1) "Test1 text to parse first-text")))

                (let [[parent2 a2 text-to-parse] (second labels)]
                  (is (= (text a2) "Test2"))
                  (is (empty? text-to-parse))
                  (is (= (attr :href (first a2)) "test2"))
                  (is (= (text parent2) "Test2 second-text"))))))

(deftest other-childs-selectors
  (html-parse (slurp "test/resources/html/basic.html")
              [
               ;; child selectors, matched nodes should be relative to parent
               [[parent1 a b div] :as matched-childs] [:div.header (child :a.a-class :b :div)]
               [[parent2 a-text b-text div-text]] [:div.header (child :a :b :div) text]

               [[parent3 first-spans] [second-spans] [third-spans] :as a1] [:div.a1 (child :span)]
               [[parent4 first-spans-t] [second-spans-t] [third-spans-t]] [:div.a1 (child :span) text]]
              (are [x y] (= x y)
                   ;; 2 div.headers matched(and 2 groups are returned)
                   (count matched-childs) 2

                   (count a) 2
                   (count b) 1
                   (count div) 1

                   a-text "First A Second A"
                   b-text "First b"
                   div-text "First div"

                   (count a1) 3
                   (count first-spans) 2
                   (count second-spans) 1
                   (count third-spans) 1

                   first-spans-t "a a1"
                   second-spans-t "b"
                   third-spans-t "c")))

(deftest processing-errors
  (html-parse (slurp "test/resources/html/processing.html")
              [labels ["div#options label" text]
               errors nil]
              (is (= "51(+50 грн) 52 53" labels))))

(deftest description-test
  (html-parse (slurp "test/resources/html/description.html")
              [desc1 "#first div.descriptionn"]
              (let [{:keys [sizes count color single-price price] :as desc}
                    (parse-desc desc1
                                [[:count ["в ящике"]]
                                 [:sizes ["размер"]]
                                 [:single-price ["цена за пару"]]
                                 [:price ["цена" "грн"]]
                                 [:color ["цвет"] " "]]
                                :sentence #"(?m)[.,]+")]
                (is (= ["Фиолетовый"] color))
                (is (= ["Цена за пару 107 грн"] single-price))
                (is (= ["Цена: 642" "00 грн"] price)))))

(deftest iframe-test
  (html-parse (slurp "test/resources/html/iframe.html" :encoding "cp1251")
              [title ["title" text]]
              (is (= "sumki.dina.com.ua - Сумки Дина|Модные женские сумки оптом|Продажа сумок"
                     title))))

(deftest style-attibute-test
  (html-parse (slurp "test/resources/html/attr-match.html")
              [title "[style*=line-through]"]
              (is (seq title))))

(deftest selector-error-test
  (stop)
  (is
   (thrown-with-msg? Exception #"Selector syntax error"
     (html-parse (slurp "test/resources/html/iframe.html" :encoding "cp1251")
                 [title "title > "]
                 nil)))

  ;; second parsing request - should return immediately
  (is
   (thrown-with-msg? Exception #"Selector syntax error"
     (html-parse (slurp "test/resources/html/iframe.html" :encoding "cp1251")
                 [title "title > "]
                 nil))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ## Run all tests

(run-tests)

;; stop node server
(stop)
