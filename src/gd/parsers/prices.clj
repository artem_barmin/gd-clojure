(ns gd.parsers.prices
  (:require [gd.model.model :as model]
            [monger.collection :as mc])
  (:use
   clojure.test
   gd.utils.test
   dk.ative.docjure.spreadsheet
   gd.utils.common
   gd.log)
  (:import
   (org.apache.poi.ss.usermodel Workbook Sheet Cell Row WorkbookFactory DateUtil
                                IndexedColors CellStyle Font CellValue)))

(defn- group-by-on-change
  "Applies f to each value in coll, splitting it each time f returns a new value."
  [f coll]
  (when-let [s (seq coll)]
    (let [fst-val (first s)
          fst (f fst-val)
          first (cons fst-val (take-while #(= fst (f %)) (next s)))
          second (take-while #(not= fst (f %)) (drop (count first) s))]
      (cons (concat first second)
            (group-by-on-change f (seq (drop (+ (count first) (count second)) s)))))))

(def ^{:private true} borders
  {0   nil
   1   :border-thin
   2   :border-medium
   3   :border-dashed
   4   :border-hair
   5   :border-thick
   6   :border-double
   7   :border-dotted
   8   :border-medium-dashed
   9   :border-dash-dot
   10  :border-medium-dash-dot
   11  :border-dash-dot-dot
   12  :border-medium-dash-dot-dot
   13  :border-slanted-dash-dot})

(defn- sparse-vector-set[vec n value]
  (if (< (count vec) n)
    (let [diff (- n (count vec))
          part (repeat diff nil)]
      (concat vec part [value]))
    (let [[l r] (split-at n vec)]
      (concat l [value] (rest r)))))

(deftest sparse-vector-test
  (testing "insert into shorter vector"
    (is (= (sparse-vector-set [] 2 1) [nil nil 1])))

  (testing "insert into longer vector"
    (is (= (sparse-vector-set [1 2 3 4] 2 1000) [1 2 1000 4]))))

(defn- read-cell-style[cell]
  (let [style (.getCellStyle cell)
        column (.getColumnIndex cell)
        resolve-border #(get borders %)]
    {:border {:left (resolve-border (.getBorderLeft style))
              :top (resolve-border (.getBorderTop style))
              :right (resolve-border (.getBorderRight style))
              :bottom (resolve-border (.getBorderBottom style))}
     :column column}))

(defn- build-border-delimiters-model[file {:keys [drop-rows]}]
  (let [rows (->> (load-workbook file)
                  (sheet-seq)
                  first
                  row-seq
                  (drop drop-rows))
        delims (fn[type](map (fn[row]
                               (reduce
                                (fn[res cell]
                                  (let [{:keys [border column]} (read-cell-style cell)]
                                    (sparse-vector-set res column (type border))))
                                []
                                row))
                             rows))
        bottom (delims :bottom)
        top (delims :top)
        count-borders (comp #(if (> (count %) 0) 1 0) (partial remove nil?) vector)]
    (map (partial map count-borders) bottom (drop 1 top))))

(defn parse-with-border-delimiters[file & {:keys [drop-rows] :as params}]
  (let [border (fn[row] (>= (count (remove zero? row)) 3))
        borders-model (->>
                       (build-border-delimiters-model file params)
                       (map border))
        data-rows (->>
                   (load-workbook file)
                   (sheet-seq)
                   first
                   row-seq
                   (drop drop-rows)
                   (map (partial map read-cell)))]
    (->>
     (map vector borders-model data-rows)
     (group-by-on-change first)
     (map (partial map second)))))

(deftest delimiters-test
  (is
   (= (build-border-delimiters-model "test/resources/xls/border.xls" {:drop-rows 0})
      [[1 1 1] [0 0 0] [0 0 0] [1 1 1] [0 0 0] [0 0 0]]))
  (is
   (= (map count (parse-with-border-delimiters "test/resources/xls/border.xls" :drop-rows 0))
      [3 3])))

(run-tests)
