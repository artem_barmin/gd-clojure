(ns gd.server
  (:use
   gd.model.test.data_builders
   gd.utils.web
   gd.server.middleware
   gd.utils.common
   org.httpkit.server
   gd.model.migrations.schema)
  (:require [noir.server :as server]
            [gd.model.model]
            [gd.api.opencart.rest]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main

(server/load-views "src/gd/views/")

(def megabyte (* 1024 1024))

(run-server (server/gen-handler {:mode :dev :ns 'gd
                                 :session-store (session-store-extended)
                                 :session-cookie-attrs {:max-age (* 30 24 60 60)}})
            {:port 8080
             :thread (* 2 (.availableProcessors (Runtime/getRuntime)))
             :queue-size 20480
             :worker-name-prefix "worker-"
             :max-line 4096
             :max-body (* 5 megabyte)})

(println "Server started in"
         (/ (.getUptime (java.lang.management.ManagementFactory/getRuntimeMXBean)) 1000.0)
         "sec")

(defonce restore-at-start (gd.model.model/schedule:restore-timers))

(enable-dev-mode)
