(ns gd.server.middleware
  (:use org.httpkit.server
        gd.server.thumbnails
        gd.model.model
        gd.model.context
        gd.log
        gd.utils.profiler)
  (:require [noir.server :as server]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Hosts router
(comment "Router based on HOST variable. Used for retail portal and possibly for parser view.")

(defn- add-cookie-for-host[response {:keys [headers] :as request}]
  (when response
    (assoc-in response [:session-cookie-attrs :domain]
              (str "." (first (.split (get headers "host") ":"))))))

(defn wrap-host-cookie[handler]
  (fn[request]
    (add-cookie-for-host (handler request) request)))

(defn wrap-hosts-routing
  [handler]
  (fn [request]
    (if (some #(.startsWith (:uri request) %) ["/js/" "/css/" "/fonts/" "/img/" "/utils/" "/packed/"])
      (handler request)
      (route-hosts handler request))))

(defn uri-to-lowercase
  [handler]
  (fn [request]
    (let [{:keys [uri]} request]
      (if (and (not (= uri "/service"))
               (not (re-matches #".*\.ico" uri))
               (not (some #(.startsWith (:uri request) %) ["/js/" "/css/" "/fonts/" "/img/" "/utils/" "/packed/"])))
           (if (= (.toLowerCase uri) uri)
               (handler request)
               (gd.utils.web/persistent-redirect (.toLowerCase uri)))
           (handler request)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Wrapper with respect of priority

(defonce wrap-priority
  (alter-var-root
   #'ring.middleware.session/wrap-session
   (fn[original-wrapper]
     (fn[& [handler options]]
       (let [session-store-wrapper
             (fn [handler] (fn [request]
                             (reset! *current-session* (:session request))
                             (handler request)))

             session-store-dynamic-bind-and-host
             (fn [handler] (fn [{:keys [headers] :as request}]
                             (binding [*current-session* (atom nil)]
                               (handler request))))]
         (->
          handler
          wrap-host-cookie
          stock-context:resolve-types
          session-store-wrapper
          (original-wrapper
           (merge options {:cookie-name "PHPSESSID"}))
          wrap-logging
          wrap-profiling
          session-store-dynamic-bind-and-host
          wrap-hosts-routing
          uri-to-lowercase))))))