(ns gd.server.thumbnails
  (:use org.httpkit.server
        gd.utils.common
        gd.model.model
        gd.model.context
        gd.log
        gd.utils.profiler)
  (:require [noir.server :as server]
            [noir.options :as options]))

(comment "Module that responsible for thumbnails generation. Can work in
different modes(with modifiers like - mirror, and just as thumbnails generator).
Store result automatically and next time thumbnail will be taken directly by
nginx.

Original taken from resources/public/img/uploaded/original ")

(defn- generate-thumbnail[uri code modifier size]
  (let [target (new java.io.File (str "resources/public" uri))]
    (when-not (.exists target)
      (let [directory (.getParentFile target)]
        (when-not (.exists directory)
          (.mkdirs directory)))
      (time (process-image-sync
             (delay (str "resources/public/img/uploaded/original/" (if (options/dev-mode?)
                                                                     "acd47a87c0eba67d99053d094546f963f7eaeaf1"
                                                                     code ) ".jpg"))
             target
             size
             (case modifier
               "mirror" {:flop true}
               "uploaded" {}
               nil {})))))
  {:status 200
   :headers {"X-Accel-Redirect" uri}})

(defn process-thumbnails[default request]
  (let [uri (:uri request)]
    (if (.exists (new java.io.File (str "resources/public" uri)))
      (default request)
      (cond-let [[_ modifier w h code]]

        (re-find #"/img/uploaded/([a-z]+)/w([0-9]+)/h([0-9]+)/([a-z0-9]{40})" uri)
        (generate-thumbnail uri code modifier [w h])

        (re-find #"/img/uploaded/([a-z]+)/w([0-9]+)/h()/([a-z0-9]{40})" uri)
        (generate-thumbnail uri code modifier [w])

        (re-find #"/img/uploaded/([a-z]+)/(original)()/([a-z0-9]{40})" uri)
        (generate-thumbnail uri code modifier nil)

        (re-find #"/img/uploaded/()w([0-9]+)/h([0-9]+)/([a-z0-9]{40})" uri)
        (generate-thumbnail uri code nil [w h])

        :else
        (default request)))))

(defn trace-handler [app]
  (fn [request] (process-thumbnails app request)))

(server/wrap-route :resources trace-handler)
