(ns gd.api.opencart.mappers
  (:use noir.core
        clojure.algo.generic.functor
        hiccup.page
        cheshire.core
        gd.model.model
        gd.utils.db
        korma.core
        gd.api.utils
        gd.utils.common
        gd.views.bucket.simple
        gd.views.admin.stock-model)
  (:require [schema.core :as s]
            [clojure.string :as str]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Converter [name,val] -> id in both directions
(def filter-ids (atom {}))
(def filter-number (atom 0))

(defn gen-filter-id[name value]
  (let [key [name value]]
    (or (get @filter-ids key)
        (let [id (swap! filter-number inc)]
          (swap! filter-ids assoc key id)
          id))))

(defn filter-ids-to-filters[ids]
  (->> @filter-ids
       (filter (comp (set ids) second))
       (map first)                      ;take keys => [name value]
       (group-by first)                 ;group by name
       (fmap (partial map second))                    ;leave only values
       ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Mappers
(def order-status
  {:created "Создан"                        ;только создан
 :in-work "В обработке"                       ;обрабатывается менеджером
 :processed "Обработан"                     ;обработан менеджером
 :shipping "В доставке"                      ;в доставке
 :canceled "Отменён"                      ;отменен
 :closed "Закрыт"                        ;закрыт
 :merged "Слит"                         ;слит с другим заказом(эквивалентно закрытию, не должен отображаться)
 :payed-cash "Оплачен"                     ;оплата наличными
 :payed-clearing "Наложеный платеж"                ;оплата по безналу
 :prepared-to-work "Собран" })

(defn translate [propertie type]
  (let [dictionary (:param (dictionary:get-simple-dictionary))
        translation (first (filter #(= (:type %) propertie) dictionary))]
    (case type
      :filter
        (or (:filter translation)
            propertie)
      :order-status
        (or (order-status propertie)
            propertie)

      :option
        (or (:name translation)
            propertie))))

(defn option-mapper[{:keys [id params]}]
  (map
   (fn[[name values]]
     {:option_id (gen-filter-id name "--option-group-id--")
      :product_option_id id
      :name (translate name :option)
      :required "1"
      :type "radio"
      :option_value (map (fn[value]
                           {:option_value_id (gen-filter-id name value)
                            :product_option_value_id (gen-filter-id name value)
                            :name value
                            :price_prefix "+"
                            :weight_prefix "+"
                            :quantity 0
                            :subtract 0
                            :price 0
                            :weight 0
                            :image ""})
                         values)})
   params))


(defn product-name-mapper [name category]
  (let [splited-name (str/split name #" ")]
    (str (str/join " " (filter
                         (fn [part] (and
                                      (not (re-matches #"\d{3}\d*" part))
                                      (not (re-matches #"\d{4}-\d{4}" part))
                                      (not (re-matches #"\d{2,3}." part))
                                      (not (re-matches #"WLA-\d{2,3}" part))
                                      (not (re-matches #"\d{2,4}-\d{2,4}" part))
                                      (not (re-matches #"WZA-\d{2,4}" part))))
                         splited-name)) " - " category)))

(defn product-mapper [product]
  (let [images (vec (map-indexed
                     (fn[i code]
                       {:image code
                        :product_id (:id product)
                        :product_image_id 0
                        :sort_order i})
                     (:images product)))

        price (if (is-sale? product)
                    (apply min
                        (map :price
                          (select :stock-variant
                            (where {:fkey_stock (:id product)
                                    :fkey_stock-context (stock-context-type (:selected-context product))}))))
                    (:price product))

        new-price (when (is-sale? product)
                    (:price product))

        generated-meta (str
                         (:name product)
                         " - цена "
                         (:price product)
                         " грн., купить оптом. Качественные и дешевые "
                         (.toLowerCase (or (:title (:page (category:get-by-id (:id (:category product))))) (:name (:category product)))))]
    (option-mapper product)
    {:images images
     :options          (option-mapper product)

     :title            (product-name-mapper (:name product) (:name (:category product)))
     :date_added       (str (:created product))
     :date_available   "2015-02-03"
     :date_modified    "2011-09-30 01:05:46"
     :description      (:other (:description product))
     :measurements     (:measurements (:description product))
     :image            (first (:images product))
     :manufacturer     ""
     :manufacturer_id  "9"
     :meta_description generated-meta
     :meta_keyword     ""
     :minimum          "1"
     :model            ""
     :name             (:name product)
     :points           "0"
     :price            price
     :prices           (apply merge (map (fn [[key value]] {(:size key) (:price value)}) (:stock-variant product)))
     :product_id       (:id product)
     :quantity         0
     :rating           0
     :reviews          0
     :reward           0
     :sort_order       0
     :special          (when new-price new-price)
     :weight           0.0
     :weight_class_id  0
     :length           0.0
     :length_class_id  0
     :width            0.0
     :height           0.0
     :status           1
     :stock_status     "Есть в наличии"
     :subtract         0
     :tag              ""
     :tax_class_id     "9"
     :viewed           "0"
     :breadcrumbs      (reverse (map :id (category:parents (get-in product [:category :id]))))}))

(defn category-mapper [category]
  {:title (:title (:seo (:page category)))
   :page_title (:title (:page category))
   :category_id      (:id category)
   :column           (if (every? (fn [[_ value]] (zero? (count (:child value)))) (:child category)) 1 4)  ;columns in dropdown top section
   :description      (:content (:page category))
   :image            ""
   :language_id      ""
   :meta_description (:description (:seo (:page category)))
   :meta_keyword     ""
   :name             (:name category)
   :parent_id        (if (:top-category category) 0 (:fkey_category category)) ;parent category id
   :sort_order       "0"                                    ;???
   :status           "1"                                    ;ACTIVE| UNACTIVE
   :store_id         "0"
   :top   "1"                                                  ;(if (nil? (:fkey_category category)) "1" "0")
   })

(defn filter-mapper[[name values]]
  {:filter_group_id (gen-filter-id name "--group-id--")
   :name (translate name :filter)
   :filter (map (fn[value] {:filter_id (gen-filter-id name value)
                            :name value})
                values)})

(defn cart-mapper[{:keys [stock id quantity sum params]}]
  {:image (first (:images stock))
   :key id
   :model (:name stock)
   :price (:price stock)
   :product_id (:id stock)
   :total sum
   :quantity quantity
   :name (:name stock)
   :option (map
            (fn[[name value]]
              {:name name
               :option_value value
               :type "select"

               :option_id (gen-filter-id name "--option-group-id--")
               :option_value_id (gen-filter-id name value)
               :product_option_id (:id stock)
               :product_option_value_id (gen-filter-id name value)

               :points 0
               :points_prefix "+"
               :price 0
               :price_prefix "+"
               :quantity 0
               :subtract 0
               :weight 0})
            params)

   :download []
   :height 0
   :minimum 0
   :length 0
   :length_class_id 0
   :points 0
   :profile_id 0
   :profile_name ""
   :recurring false
   :recurring_cycle 0
   :recurring_duration 0
   :recurring_frequency 0
   :recurring_price 0
   :recurring_trial 0
   :recurring_trial_cycle 0
   :recurring_trial_duration 0
   :recurring_trial_frequency 0
   :recurring_trial_price 0
   :reward 0
   :shipping 0
   :stock true
   :subtract 0
   :tax_class_id 0
   :weight 0
   :weight_class_id 0
   :width 0})

(defn user-mapper[{:keys [id real-name contact name] :as user}]
  (let [name (if real-name (str/split real-name #" ") [name " "])]
  {:customer_id id
   :firstname (first name)
   :lastname (last name)
   :email (get-in contact [:email :value])
   :telephone (get-in contact [:phone :value])
   :fax ""
   :newsletter ""
   :customer_group_id "1"
   :address_id ""}))

(defn address-mapper[{:keys [id address address-region country extra-info]}]
  {:address_1 (:novaya_pochta extra-info)
   :zone (or address-region "")
   :city (or address "")
   :country (or country "")
   :country_id (or country "")
   :zone_id (or address-region "")

   :company ""
   :company_id ""
   :firstname (:real-name (gd.utils.security/logged))
   :iso_code_2 ""
   :iso_code_3 ""
   :lastname ""
   :postcode ""
   :tax_id ""
   :address_2 ""
   :address_format ""
   :address_id id
   :zone_code ""
   })

(defn zone-mapper [name]
  {:zone_id name
   :name name
   :status 1
   :country_id "Украина"
   :code ""})

(defn additional-expenseses-mapper [expenses total]
  (let [expenses (apply merge (map (fn [item] {(item :type) (select-keys item [:money :percent])}) expenses))]
    (letfn [(count-expense [type]
                           {:total (+ (Math/round (float (/ (* total (or (get-in expenses [type :percent]) 0)) 100)))
                                      (or (get-in expenses [type :money]) 0))
                            :money (or (get-in expenses [type :money]) 0)
                            :percent (or (get-in expenses [type :percent]) 0)})]

      {:discount (count-expense :discount)
      :payment-expenses (count-expense :payment-expenses)
      :delivery-expenses (count-expense :delivery-expenses)
      :return-expenses (count-expense :return-expenses)})))

(defn partial-order-mapper [{:keys [user-order-item id status changed total additional-expense]}]
  {:currency_code "USD"
   :currency_value "1.00000"
   :date_added changed
   :firstname (:real-name (gd.utils.security/logged))
   :lastname ""
   :order_id id
   :status (translate status :order-status)
   :total_products (count user-order-item)
   :total total
   :additional_expense (additional-expenseses-mapper additional-expense total)
   :editable (not (nil? ((set order-statuses-allowed-to-modify) status)))})

(defn full-order-mapper [{:keys [user order items address additional-expense]}]
  (let [total (reduce + (map #(* (:price %) (:quantity %)) items))]
  {:editable (not (nil? ((set order-statuses-allowed-to-modify) (:status order))))
  :additional_expense (additional-expenseses-mapper additional-expense total)
  :declaration_number (:declaration-number (:additional-info order))
  :order_id (:id order)
  :email (if (get-in user [:contact :email :active]) (get-in user [:contact :email :value]) "")
  :store_id "0"
  :store_name ""
  :store_url ""
  :customer_id (:id user)
  :firstname (:real-name user)
  :lastname ""
  :telephone ""
  :payment_city (:address address)
  :payment_zone_id (:address-region address)
  :payment_zone (:address-region address)
  :payment_country_id (:country address)
  :payment_country (:country address)
  :total total
  :date_modified (:changed order)
  :date_added (:changed order)
  :order_status_id ""
  :order_status (translate (:status order) :order-status)
  :payment_firstname (:real-name user)
  :payment_lastname ""

  :payment_address_1 (:novaya_pochta (:extra-info address))
  :payment_address_2 ""
  :language_id "2"
  :language_filename ""
  :language_directory ""
  :currency_id "2"
  :currency_code "USD"
  :currency_value "1.0000"
  :language_code "2"
  :payment_code ""
  :payment_method "Оплата при доставке"
  :payment_iso_code_2 ""
  :payment_iso_code_3 ""
  :payment_address_format ""
  :payment_zone_code ""
  :payment_company ""
  :payment_company_id ""
  :payment_tax_id ""
  :payment_postcode ""
  :invoice_no "0"
  :invoice_prefix "INV-2013-00"
  :fax ""
  :shipping_firstname ""
  :shipping_lastname ""
  :shipping_company ""
  :shipping_address_1 ""
  :shipping_address_2 ""
  :shipping_postcode ""
  :shipping_city ""
  :shipping_zone_id ""
  :shipping_zone ""
  :shipping_zone_code ""
  :shipping_country_id "0"
  :shipping_country ""
  :shipping_iso_code_2 ""
  :shipping_iso_code_3 ""
  :shipping_address_format ""
  :shipping_method ""
  :shipping_code ""
  :comment ""
  :ip ""
  :forwarded_ip ""
  :user_agent ""
  :accept_language ""
  }))

(defn order-stock-mapper [{:keys [stock fkey_retail-order quantity price id closed] :as unit}]
  {:model (:name stock)
   :image (first (:images (:stock unit)))
   :name (:name stock)
   :order_id fkey_retail-order
   :order_product_id id
   :price price
   :product_id id
   :quantity quantity
   :closed closed
   :reward ""
   :tax ""
   :total (* price quantity)})

(defn order-options-mapper [params order]
  {:name (translate (name (first params)) :option)
  :value (second params)
  :order_id (:fkey_retail-order order)
  :type "select"
  :order_option_id ""
  :order_product_id ""
  :product_option_id ""
  :product_option_value_id ""})

(defn prepare-message [{:keys [text sent-time user id] :as message}]
  (let [old-review-regexp #"^[а-яА-Яa-zA-Z0-9 ]+:"]
    (cond
      (re-find old-review-regexp text) (let [user (re-find old-review-regexp text)]
                                         [user (.substring text (count user))])
      (:address user) [(str (:name user) "(" (:address user) ")") text]
      true [(:name user) text])))

(defn testimonial-mapper [{:keys [user text sent-time id] :as message}]
  (let [[name text] (prepare-message message)]
  {:city ""
   :date_added sent-time
   :description text
   :email "email@email.com"
   :language_id "2"
   :name name
   :rating "5"
   :status "1"
   :testimonial_id id
   :title ""}))

(defn news-mapper [{:keys [seo content-short content time id title url]}]
  {:blog_id id
   :url url
   :create_time time
   :date time
   :intro_text content-short
   :language_id 2
   :meta_description (:description seo)
   :meta_keyword (:keywords seo)
   :sort_order 1
   :status 1
   :text content
   :title title
   :titleseo (:title seo)
   :update_time time})

(defn information-mapper [{:keys [seo content-short content time id title url type]}]
  {:information_id id
   :keyword url
   :create_time time
   :date time
   :intro_text content-short
   :language_id 2
   :meta_description (:description seo)
   :meta_keyword (:keywords seo)
   :sort_order 1
   :status 1
   :type type
   :description content
   :title title
   :update_time time
   :bottom 0})

(defn attribute-mapper [[key values]]
  (letfn [(toString [{:keys [name value]}]
                    (str "<b>" name ":</b> " value "<br>"))]
  {:attribute_id 0
   :name (name key)
   :text (str "<div style='text-align:left'>" (reduce str (map toString values)) "</div>")}))

(defn attribute-group-mapper [measurements]
  [{:attribute_group_id (str 3)
   :name "Замеры для размеров"
   :attribute (map attribute-mapper measurements)}])
