(ns gd.api.opencart.rest
  (:use noir.core
        gd.model.context
        clojure.algo.generic.functor
        gd.api.opencart.mappers
        hiccup.page
        cheshire.core
        gd.model.model
        gd.utils.db
        [clojure.set :only [difference]]
        korma.core
        gd.api.utils
        gd.utils.common
        gd.views.bucket.simple)
  (:require [schema.core :as s]
            [gd.utils.security :as internal-sec]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clj-time.core :as t]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Filters

(defpage "/restapi/filters/:categoryid" {categoryid :categoryid}
  (let [categoryid (if (= categoryid "0") nil (parse-int categoryid))
        filter-params (cond
                        (= categoryid -1)
                        {:mode :new}
                        (= categoryid -2)
                        {:mode :sale}
                        :else
                        {:categories (when categoryid [{:id categoryid}])
                          :mode :main})]
    (result
     {:filters (->> (retail:get-user-stock-params filter-params)
                    (map filter-mapper))
      :counts (->> (meta:get-filters-for-stocks-with-count filter-params)
                   (group-by-single (fn[{:keys [name value]}]
                                      (gen-filter-id name value)))
                   (fmap :count))})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Stock

(def stock-request
  {(s/optional-key :filter_category_id)  (s/named long "category id")
   (s/optional-key :filter_filter)  (s/named [long] "filter ids")
   (s/optional-key :order)              (s/enum "ASC" "DESC" "asc" "desc")
   (s/optional-key :limit)              long
   (s/optional-key :start)              long
   (s/optional-key :sort)               (s/named [s/Keyword] "criterium of orderding. ex: price,name")
   (s/optional-key :filter_tag) (s/named String "search-string")
   s/Keyword s/Any})

(defpage [:post "/restapi/stock"] params
  (let [{:keys [filter_category_id filter_filter order limit start sort filter_tag]}
        (coerce stock-request params)

        pre-params
        (cond
          (= filter_category_id -2) ;sale
            {:context-status {:wholesale-sale :visible
                              :wholesale :visible}}
          (= filter_category_id -1) ;new
            {:new true}
          (string? filter_tag)
            {:category (if (= filter_category_id 0) nil filter_category_id)
             :search-string filter_tag}
          :else
            {:category filter_category_id})

        sort
        (cond
          (= (first sort) :p.sort_order)
            nil
          (= (first sort) :p.date_added)
            {:new true}
          (and sort order)
            {:sort-key (keyword (last (str/split (name (first sort)) #"\.")))
             :sort (keyword (.toLowerCase order))}
          :else
            nil)

        stock-params (merge pre-params {:filters (filter-ids-to-filters filter_filter)} sort)

        stocks (retail:supplier-stocks limit start stock-params)
        total (count (retail:supplier-stocks nil nil stock-params))]
         (result {:stocks (map product-mapper stocks)
                  :total total})))

(defpage "/restapi/stock/:id" params
  (let [{:keys [id]} (coerce {:id long} params)
        stock (retail:get-stock id)]
    (result (when stock (product-mapper stock)))))

(defpage "/restapi/stock/attributes/:id" {id :id}
         (let [id (parse-int id)
               measurements (get-in (stock:get-stock id) [:description :measurements])]
           (result (attribute-group-mapper measurements))))

(defpage "/restapi/stock/related/:id" {id :id}
  (result []))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Categories

(def custom-categories
  [{:title "Новые поступления"
    :category_id -1
    :column 0
    :description ""
    :image ""
    :language_id ""
    :meta_description "Новые поступлени - продажа по низким ценам. Купите в Украине по низким ценам в интернет-магазине Боно."
    :meta_keyword ""
    :name "Новинки"
    :parent_id 0
    :sort_order "0"
    :status "1"
    :store_id "0"
    :top "1"}
   {:title "Акционные товары"
    :category_id -2
    :column 0
    :description ""
    :image ""
    :language_id ""
    :meta_description "Акционные товары - продажа по низким ценам. Купите в Украине по низким ценам в интернет-магазине Боно."
    :meta_keyword ""
    :name "Акции"
    :parent_id 0
    :sort_order "0"
    :status "1"
    :store_id "0"
    :top "1"}])

(defpage "/restapi/categories/" params
  (result
    {:categories (concat (map category-mapper (category:model)) custom-categories)
     :counts (merge (->> (category:model)
                         (group-by-single :id)
                         (fmap #(:stocks %)))
                    {:-1 (count (retail:supplier-stocks nil nil {:new true}))
                     :-2 (count (retail:supplier-stocks nil nil {:context-status {:wholesale-sale :visible
                                                                 :wholesale :visible}}))})}))

(defpage "/restapi/category/:id" {id :id}
         (let [category  (case id
                            "-1"
                              {:id "-1" :name "Новинки"}
                            "-2"
                              {:id "-2" :name "Распродажа"}
                            (category:get-by-id (parse-int id)))]
         (->>
           category
           category-mapper
           result)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cart

(def cart-add {:quantity long
               :options {long long}
               :stock long})

(defpage [:post "/restapi/cart/add/"] params
  (let [{:keys [quantity options stock]} (coerce cart-add params)]
    (bucket:add-to-bucket (merge
                           {:fkey_stock stock
                            :quantity quantity}
                           (fmap first (filter-ids-to-filters (vals options)))))
    "OK"))

(def cart-update {:quantity long
                  :key long})

(defpage [:post "/restapi/cart/update/"] params
  (let [{:keys [quantity key]} (coerce cart-update params)]
    (bucket:change-item key {:quantity quantity})
    "OK"))

(def cart-delete {:key long})

(defpage [:post "/restapi/cart/remove/"] params
  (let [{:keys [key]} (coerce cart-delete params)]
    (bucket:delete key)
    "OK"))

(defpage "/restapi/cart/clear/" data
         (bucket:clear))

(defpage "/restapi/cart/has/" {}
  (result (> (bucket:count) 0)))

(defpage "/restapi/cart/get/" {}
  (result (map cart-mapper (bucket:items))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Login/Register

(defpage [:post "/restapi/login/remind/"] data
         (let [{:keys [email]} (coerce {:email s/Str :phone (s/maybe s/Str)} data)]
         (result
             (if (and (not-empty email) (user:check-email-exists email))
              (do (user:remind-password email)
                true)
              false))))

(defpage [:post "/restapi/login/"] {:keys [login password]}
  (result
   (if-let [auth-info (user:check-credentials login password)]
     (do (internal-sec/set-logged auth-info)
         true)
     false)))

(def register-schema
  {:email String
   :password String
   :telephone String

   (s/optional-key :firstname) String
   (s/optional-key :lastname) String

   (s/optional-key :newsletter) (s/named long "0-disable,1-enable")

   :country_id String
   (s/optional-key :city) String
   :zone_id String
   :address_1 String

   s/Keyword String})

(defpage [:post "/restapi/register/"] params
  (let [{:keys [firstname lastname password email city country_id telephone address_1 zone_id]}
        (coerce register-schema params)]
    (when-let [user-id (:id (invite:use-internal nil email password email telephone))]
      (do

         (user:update user-id
                      {:real-name (str firstname " " lastname)
                       :contact {:phone telephone}})
         (user:update-address user-id {:address city
                                       :address-region zone_id
                                       :country country_id
                                       :extra-info {:novaya_pochta address_1}})
         (user:create-auth user-id email telephone password)
         (when-let [auth-info (user:check-credentials email password)]
            (internal-sec/set-logged auth-info)
         (gd.utils.sms/send-sms-unsafe (gd.utils.sms/normalize-number telephone)
            (str "Registracija na saite BONO.IN.UA proshla uspeshno! Login " (gd.utils.sms/normalize-number telephone) ", parol 1234. Esli Vam nuzhna pomoshh'  - zvonite 0504136990 s 8 do 21.00.")))))
         (result true)))

(def user-schema
  {:telephone s/Str
   :email s/Str
   :lastname s/Str
   :firstname s/Str
   :country_id s/Str
   (s/optional-key :city) s/Str
   (s/optional-key :zone_id) s/Str
   (s/optional-key :address_1) (s/named s/Str "nova-poshta department")
   (s/optional-key :address_2) (s/named s/Str "full address for other countries")})

(defpage [:post "/restapi/user/edit/"] params
         (let [{:keys [firstname lastname telephone city zone_id country_id address_1 address_2]} (coerce user-schema params)
               id (internal-sec/id)]
           (user:update id
                        {:real-name (str firstname " " lastname)
                         :contact {:phone telephone}})
           (user:update-address id {:address city
                                         :address-region zone_id
                                         :country country_id
                                         :extra-info {:novaya_pochta (if (empty? address_1) address_2 address_1)}})
           (internal-sec/set-logged (user:get-full (internal-sec/id)))
         (result "OK")))

(defpage "/restapi/logout/" {}
  (result (internal-sec/logout)))

(defpage "/restapi/logged/" {}
  (result
   (or (when-let [user (internal-sec/logged)]
         (user-mapper user))
       {})))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; User address

(defpage "/restapi/countries/" {}
         (result [{:country_id "Украина"
                   :name "Украина"
                   :postcode_required 0
                   :status 1
                   :iso_code_2 "UA"
                   :iso_code_3 "UKR"
                   :address_format ""}
                  {:country_id "Другая"
                  :name "Другая"
                  :postcode_required 0
                  :status 1
                  :iso_code_2 ""
                  :iso_code_3 ""
                  :address_format ""}
                  ]))

(defpage "/restapi/country/" {}
         (result {:country_id "Украина"
                  :name "Украина"
                  :postcode_required 0
                  :status 1
                  :iso_code_2 "UA"
                  :iso_code_3 "UKR"
                  :address_format ""}))

(defpage "/restapi/zones/" {}
         (result (map zone-mapper (keys (cheshire.core/parse-string (slurp "resources/tools/novya-pochta.txt") false)))))

(defpage [:post "/restapi/cities/"] {:keys [district]}
         (let [data (cheshire.core/parse-string (slurp "resources/tools/novya-pochta.txt") false)]
         (result (keys (get data district)))))

(defpage [:post "/restapi/departments/"] {:keys [district city]}
         (let [data (cheshire.core/parse-string (slurp "resources/tools/novya-pochta.txt") false)]
         (result (get-in data [district city]))))

(def address-schema
  {:country_id (s/named String "страна")
   (s/optional-key :zone_id) (s/named String "область")
   (s/optional-key :city) (s/named String "Город")
   :address_1 (s/named String "отделение новой почты")
   s/Keyword s/Any})


(defpage "/restapi/address/get" data
         (let [address (first (select user-address (where {:fkey_user (internal-sec/id)})))]
           (result (address-mapper address))))

(defpage [:post "/restapi/address/update"] params
         (let [{:keys [city country_id zone_id address_1 address_2] :as request} (coerce address-schema params)]
           (user:update-address (internal-sec/id) {:address city
                                                  :address-region zone_id
                                                  :country country_id
                                                  :extra-info {:novaya_pochta (if (empty? address_1) address_2 address_1)}})
           "OK"))


;(defpage "/restapi/addresses/:id" {id :id}
;         (->>
;         (into [] (map address-mapper (select user-address (where {:fkey_user (parse-int id)}))))
;         (map #(array-map (keyword (str (:address_id %))) %))
;         (reduce merge)
;         result))
;(defpage [:post "/restapi/address/add"] params
;         (let [{:keys [city zone_id country_id]} params]
;           (insert user-address (values {:fkey_user (:id (internal-sec/logged))
;                                         :address city
;                                         :address-region (if zone_id zone_id "")}))
;           "OK"))
;(defpage "/restapi/address/delete/:id" {id :id}
;         (delete user-address (where {:id (parse-int id)})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Order

(def order-schema
  {:products [{:quantity long
             :product_id long
             :option [{:value String
                      s/Keyword s/Any}]
             s/Keyword s/Any}]
   (s/optional-key :comment) String
   :currency_code (s/named String "USD")
   :currency_id long
   :currency_value Double
   (s/optional-key :customer_id) long
   (s/optional-key :email) String
   (s/optional-key :payment_address_1) String
   (s/optional-key :payment_address_2) String
   :store_id long
   :store_name String
   :store_url String
   (s/optional-key :telephone) String
   :total (s/named long "общая стоимость")
   s/Keyword s/Any
   })

(defpage [:post "/restapi/order/create/"] params
         (let [{:keys [products]} (coerce order-schema params)
               size? (partial = "size")]
         (gd.log/info "Process opencart bucket for user" (internal-sec/id) (:name (internal-sec/logged)))
             (->>
               products
               (map (fn [item]
                      (let [size (:value (first (filter (comp size? :name) (:option item))))
                            quantity (:quantity item)
                            id (:product_id item)]
                        {:fkey_stock id :quantity quantity :size size})))
               (into [])
               (noir.session/put! :oc-order-items))))

(defpage [:post "/restapi/order/confirm/"] data
         (let [{:keys [comment]} (coerce {:comment s/Str} data)]
           (->
             (retail:make-order (noir.session/get! :oc-order-items))
             (retail:change-order-additional-info {:comment comment}))

           (bucket:clear)

           (noir.session/remove! :oc-order-items)

           "OK"))

(defpage "/restapi/order/get/:id" {id :id}
         (let [id (parse-int id)
               order (retail:get-order (parse-int id))
               items (:user-order-item order)
               additional-expense (:additional-expense order)
               user (user:get-full (:fkey_user order))
               address (first (:user-address user))]

             (->>
             {:order order :user user :items items :address address :additional-expense additional-expense}
             full-order-mapper
             result)))

(def order-edit-schema
  {(s/optional-key :delete) s/Num
   (s/optional-key :update) {:id s/Num
                             :quantity s/Num}})

(defpage [:post "/restapi/order/edit"] data
         (let [input (coerce order-edit-schema data)
               delete (:delete input)
               update (when (:update input)
                        [(:update input)])]
           (when delete
             (retail:delete-order-item delete))
           (when update
             (retail:change-order update))
           (result "ok")))

(defpage [:post "/restapi/orders/get/"] data
         (let [{:keys [offset limit]} (coerce {(s/optional-key :limit) s/Num (s/optional-key :offset) s/Num} data)
               orders (retail:get-user-orders offset limit)
               order-keys (map
                            #(merge
                                 (select-keys % [:id :status :changed :additional-expense :user-order-item])
                                 (->>
                                  (map (fn [item] (* (:price item) (:quantity item))) (:user-order-item %))
                                  (reduce +)
                                  (array-map :total)))
                               orders)]

           (result (map partial-order-mapper order-keys))))

(defpage "/restapi/orders/get/stocks/:id" {id :id}
         (let [stocks (->>
                        id
                        parse-int
                        retail:get-order
                        :user-order-item)]
           (result (map order-stock-mapper stocks))))

(defpage [:post "/restapi/orders/get/stock/options/"] {:keys [order-id stock-id]}
  (let [order-item (first (select user-order-item (where (= :id (parse-int stock-id)))))
        params       (:params (orders:get-order-item (:id order-item)))
        parts (map #(into [] %) params)]

    (-> #(order-options-mapper % (select-keys order-item [:fkey_retail-order :fkey_stock]))
        (map parts)
        result)))

(defpage [:post "/restapi/password/"] {:keys [password]}
         (if-let [user (internal-sec/logged)]
           (do (user:update-password (user :id) (:password (first (:auth-profile user))) password)
               (result true))
           (result false)))

(defpage [:post "/restapi/rewrites"] data
  (result data))

(def get-reviews-schema
  {(s/optional-key :start) Long
   (s/optional-key :limit) Long})

(defpage [:post "/restapi/reviews"] data
         (let [{:keys [start limit]} (coerce get-reviews-schema data)]
           (result (map testimonial-mapper (retail:get-review limit start)))))

(def add-review-schema
  {:description s/Str
   s/Keyword s/Any})

(defpage [:post "/restapi/reviews/add"] data
         (let [text (:description (coerce add-review-schema data))]
           (retail:send-review text)))

(def news-schema
  {(s/optional-key :start) Long
   (s/optional-key :limit) Long
   s/Keyword s/Any})

(defpage [:post "/restapi/news/get"] data
         (let [{:keys [start limit]} (coerce news-schema data)]
           (result (map news-mapper (site-pages:get-pages-list limit start {:type :news :status :published})))))

(defpage "/restapi/news/get/:id" {id :id}
         (result (news-mapper (site-pages:get-page-for-id (parse-int id)))))

(defpage [:post "/restapi/blogs/get"] data
         (let [{:keys [start limit]} (coerce news-schema data)]
           (result (map news-mapper (site-pages:get-pages-list limit start {:type :blogs :status :published})))))

(defpage "/restapi/blogs/get/:id" {id :id}
         (result (news-mapper (site-pages:get-page-for-id (parse-int id)))))

(defpage "/restapi/information/:id" {id :id}
         (result (information-mapper (site-pages:get-page-for-id (parse-int id)))))

(defpage "/restapi/informations" data
         (result (map information-mapper (site-pages:get-pages-list 9999999 0 {:type :page :status :published}))))

(defpage "/restapi/informations/typebyurl/:url" {url :url}
         (let [x (site-pages:get-page-for-url url)]
           (if (= :page (:type x))
             (result (:id x))
             (result false))))

(defpage "/restapi/seo/get" data
         (let [categories (merge (category:model)
                                 {:id -1
                                  :path "/new/"
                                  :short-name "new"}
                                 {:id -2
                                  :path "/sale/"
                                  :short-name "sale"})
               blogs (site-pages:get-pages-list nil nil {:type :blogs :status :published})
               news (site-pages:get-pages-list nil nil {:type :news :status :published})]
         (result {:products (retail:supplier-stocks nil nil {:only-id [1]})
                  :categories (map #(select-keys % [:short-name :path :id]) categories)
                  :news (map #(select-keys % [:url :id]) news)
                  :blogs (map #(select-keys % [:url :id]) blogs)})))

(defpage [:post "/restapi/seo/category"] data
         (let [{:keys [filter_category_id filter_filter]} (coerce stock-request data)
               {{:keys [description title]} :seo :as category} (:page (category:get-by-id filter_category_id))
               filters (filter-ids-to-filters filter_filter)
               filter-str (reduce (fn [result [filter values]]
                                    (str result " " (translate filter :option) ":" (reduce #(str %1 " " %2 ) "" values) ";"))
                                  "" filters)]
           (result filter-str)))

(def default-reference "bono/catalog/view/theme/bono/stylesheet")
(def opencarts "/home/webserver/opencarts/")

(defn get-css-files [reference]
  (let [reference (or reference default-reference)
        dir (str opencarts reference)
        files (.listFiles (io/file dir))
        files-str (map #(.getName %) files)
        css-files (filter (partial re-matches #".*.css" )  files-str)]
    (map (fn [file] {:reference (str reference "/" file)
                     :filename (str/replace file #".css" "")}) css-files)))

(defn- backup-file [path]
  (let [dir-name (fn [filename]
                   (str "backup-" (second (re-find #"(.*)(\.)" filename))))

        file (io/file path)
        file-dir (.getParent file)
        file-name (.getName file)
        file-content (slurp path)

        backup-name (str (.toString (t/to-time-zone (t/now) (t/time-zone-for-offset 3)) "dd-MM-YY:HH-mm-ss") ":" file-name)
        backup-dir (str file-dir "/" (dir-name file-name))
        backup-dir-file (io/file backup-dir)
        backup-path (str backup-dir "/" backup-name)]
    (when-not (.exists backup-dir-file) (.mkdir backup-dir-file))
    (spit backup-path file-content)))

(def css-schema
  {:filename s/Str
   :reference s/Str
   :css s/Str})

(defpage "/restapi/contact/check/phone" data
         (if (= (user:check-is-empty-phone (:telephone_guest data)) 0)
           (result true) (result false)))

(defpage "/restapi/user/restoredatasms" data
         (if (= "no phone" (user:restore-by-sms (str "+"(:phone data))))
           (result "error") (result "ok"))
         )


(defpage "/restapi/seo/css" data
         (let [files (get-css-files nil)]
           (result {:files files})))

(defpage [:post "/restapi/seo/css"] {:keys [reference]}
         (when reference
           (let [file (io/file (str opencarts reference))
                 path (.getPath file)]
             (when (and (.exists file) (not (.isDirectory file)))
               (result {:css       (slurp (.getPath file))
                        :filename  (.getName file)
                        :reference (second (str/split path #"opencarts/"))})))))

(defpage [:post "/restapi/seo/css/update"] data
         (if (internal-sec/id)
           (let [{:keys [css filename reference]} (coerce css-schema data)
                 valid-content (unescape-html css)
                 path (str opencarts reference)]
             (backup-file path)
             (spit path valid-content)
             (result true))))

(def redirect-schema
  {:redirects [{:from s/Str
                :to s/Str
                :enabled s/Bool
                (s/optional-key :id) s/Num}]})

(defpage "/restapi/seo/redirect" data
         (let [redirects (map (fn [ent] (dissoc ent :fkey_domain)) (domain:get-redirects))]
           (result redirects)))

(defpage [:post "/restapi/seo/redirect"] data
         (let [{list :redirects} (coerce redirect-schema data)
               deleted (difference
                         (set (map :id (domain:get-redirects)))
                         (set (keep :id list)))]
           (transaction
             (doseq [id deleted]
               (domain:delete-redirect id))
             (doseq [{id :id :as redirect} list]
                  (upset redirects (dissoc redirect :id) {:id id})))
           (result true)))
