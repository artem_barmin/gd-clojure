(ns gd.api.utils
  (:use gd.utils.common
        [clojure.string :only (split)]
        [clojure.data.json :as json])
  (:require [schema.core :as s]
            [schema.coerce :as c]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Schema coerce
(defn safe-list-coercion[elem-fn]
  (c/safe (fn[s]
            (when (seq s)
              (map (comp elem-fn c/edn-read-string) (split s #","))))))

(def ^:no-doc +coma-list-coercions+
  (merge
   c/+string-coercions+
   {[long] (safe-list-coercion c/safe-long-cast)
    [Long] (safe-list-coercion c/safe-long-cast)
    [s/Int] (safe-list-coercion c/safe-long-cast)
    [s/Keyword] (safe-list-coercion keyword)}))

(defn coma-list-coercion-matcher
  "A matcher that coerces keywords, keyword enums, s/Num and s/Int,
  and long and doubles (JVM only) from strings."
  [schema]
  (or (+coma-list-coercions+ schema)
      (c/keyword-enum-matcher schema)
      (c/set-matcher schema)))

(defn coerce[schema value]
  (let [result ((c/coercer schema coma-list-coercion-matcher) value)]
    (when-let [err (:error result)]
      (throw (Exception. (pr-str err))))
    result))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HTTP utils
(defn result[res]
  (noir.response/content-type "application/json" (json/json-str res)))
