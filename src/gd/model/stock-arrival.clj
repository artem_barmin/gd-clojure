(in-ns 'gd.model.model)

(declare stock:arrival-parameters)
(declare stock:arrival-parameter-values)
(declare stock-arrival-parameter)

(defentity stock-arrival
  (enum stock-arrival-type [:type]
        :arrival
        :procurement
        :inventory)
  (enum stock-arrival-status [:status]
        :opened
        :closed
        :special                        ;lifecycle is managed by system(used for simple accounting)
        )
  (belongs-to user {:fk :fkey_user})
  (has-many stock-arrival-parameter {:fk :fkey_stock-arrival})
  (json-data :parameters))

(defentity stock-arrival-parameter
  (transform #'stock:arrival-parameter-values)
  (has-many-join-table stock-parameter-value)
  (belongs-to stock {:fk :fkey_stock})
  (belongs-to stock-arrival {:fk :fkey_stock-arrival}))

(extend-entity stock
  (has-many stock-arrival-parameter {:fk :fkey_stock})
  (enum stock-accounting [:accounting]
        :simple
        :warehouse)
  (transform #'stock:arrival-parameters))

(defentity inventory-available-stocks
  (array-transform :params)
  (belongs-to stock {:fk :fkey_stock})
  (belongs-to stock-arrival {:fk :fkey_stock-arrival}))

(extend-entity stock
  (has-many inventory-available-stocks {:fk :fkey_stock}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stock list processors(sort by price avaliability + filter by arrival)

(defn fetch-arrival-and-order-by-price-existense
  "Filter stocks by list of stocks in arrival, and sort by price availability"
  [arrival arrival-session query]
  (-> query
      (join :left
            [(subselect stock-arrival-parameter
                        (modifier "distinct")
                        (fields-only [:fkey_stock :id] [(raw "1") :ordering])
                        (where {:fkey_stock-arrival arrival})
                        (where (or {:price 0}
                                   {:price nil})))
             :arrival-ordering]
            (= :stock.id :arrival-ordering.id))
      (order :arrival-ordering.ordering)
      (order :stock.name)
      (where {:stock.id [in (binding [*exec-mode* nil]
                              (concat (arrival:stocks-from-arrival arrival)
                                      arrival-session))]})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Postprocessing

(defn stock:arrival-parameter-values[{:keys [stock-parameter-value] :as stock}]
  (->
   stock
   (assoc :parameters (params-as-map stock-parameter-value))
   (dissoc :stock-parameter-value)))

(defn stock:arrival-parameters[{:keys [stock-arrival-parameter] :as stock}]
  (if (sequential? stock-arrival-parameter)
    (assoc stock :stock-arrival-parameter
           (group-by :fkey_stock-arrival stock-arrival-parameter))
    stock))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; General API

(defn- arrival:stocks-from-arrival[arrival-id]
  (map :fkey_stock
       (select stock-arrival-parameter
         (fields-only :fkey_stock)
         (where {:fkey_stock-arrival arrival-id}))))

(defn- arrival:save-stock-arrival
  "overwrite-everything - overwrite arrival info only for single parameter map, and not for all stocks.
Used in special-arrival mode"
  [stock-id arrival-id info & {:keys [overwrite-everything] :or {overwrite-everything true}}]
  (let [main-props {:fkey_stock stock-id :fkey_stock-arrival arrival-id}
        existing-arrival-params (select stock-arrival-parameter
                                  (with stock-parameter-value)
                                  (where main-props))

        delete-arrival-params
        (fn[arrival-param-ids]
          (delete :stock-arrival-parameter-with-stock-parameter-value
            (where {:stock-arrival-parameter-key [in arrival-param-ids]}))
          (delete stock-arrival-parameter (where {:id [in arrival-param-ids]})))]
    (transaction
      ;; save new param values
      (meta:save-stock-parameters
       stock-id (reduce (partial merge-with concat)
                        (map (comp (partial fmap maybe-seq) :parameters)
                             info)))

      ;; delete all old relations(because we need to overwrite everything)
      (when overwrite-everything
        (delete-arrival-params (map :id existing-arrival-params)))

      (let [arrival-params-by-param
            (group-by-single :parameters existing-arrival-params)

            stock-parameters
            (select stock-parameter-value (where {:fkey_stock stock-id}))]
        (doseq [{:keys [parameters] :as single-param} info]

          (when-not overwrite-everything
            (delete-arrival-params [(:id (get arrival-params-by-param parameters))]))

          (let [old-params
                (select-keys (get arrival-params-by-param (fmap name parameters)) [:price])

                param-id
                (:id (insert stock-arrival-parameter
                       (values (-> (merge old-params single-param main-props)
                                   (dissoc :parameters)))))]

            (insert :stock-arrival-parameter-with-stock-parameter-value
              (values
               (map (fn[val]{:stock-parameter-value-key (:id val)
                             :stock-arrival-parameter-key param-id})
                    (meta:resolve-params-map stock-parameters parameters))))))))))

(declare arrival:get-arrival-by-id)

(defn- inventory:check-snapshot-quantity[arrival-id stock-to-info-map]
  (let [snapshot-quantity
        (->> (select inventory-available-stocks
                     (where {:fkey_stock-arrival arrival-id
                             :fkey_stock [in (keys stock-to-info-map)]}))
             (map (fn[snap]
                    (update-in snap [:params]
                               (comp params-as-map meta:get-params-from-id-list))))
             (group-by :fkey_stock)
             (fmap (comp (partial fmap :quantity) (partial group-by-single :params))))]

    (doseq [[stock params] stock-to-info-map]
      (doseq [{:keys [parameters quantity]} params]
        (let [snapshot (get-in snapshot-quantity [stock parameters])]
          ;; requested quantity can be in [-snapshot,+inf]
          (when (> (- snapshot) quantity)
            (info "Can't set less than in snapshot." stock parameters snapshot quantity)
            (throwf "Can't set less than in snapshot.")))))))

(s/defn arrival:save-multiple-stock-arrivals[arrival-id :- (s:id "arrival")
                                             stock-to-info-map :- {(s:id "stock")
                                                                   [{:parameters s:params-map
                                                                     :quantity Long}]}]
  (transaction
   (let [{:keys [status type]} (arrival:get-arrival-by-id arrival-id)]
     (when-not (= status :opened)
       (throwf "Attempt to change closed arrival"))
     (when (= type :inventory)
       ;; check that quantity is less than snapshot
       (inventory:check-snapshot-quantity arrival-id stock-to-info-map))
     (doseq [[stock-id params] stock-to-info-map]
       (arrival:save-stock-arrival stock-id arrival-id params)))))

(defn arrival:all-arrivals[limit-num offset-num & {:keys [type] :or {type :arrival}}]
  (let [result
        (select stock-arrival
          (with user (with auth-profile))
          (limit limit-num)
          (offset offset-num)
          (order :created :desc)
          (count-query)
          (where {:fkey_supplier *current-supplier*
                  :type (stock-arrival-type type)}))

        statistics
        (group-by-single
         :fkey_stock-arrival
         (select stock-arrival-parameter
           (aggregate "count (distinct fkey_stock)" :stocks)
           (aggregate (sum (raw "coalesce(price,0) * coalesce(quantity,0)")) :sum)
           (fields :fkey_stock-arrival)
           (group :fkey_stock-arrival)
           (where {:fkey_stock-arrival [in (map :id result)]})))

        not-filled-prices
        (fmap :stocks
              (group-by-single
               :fkey_stock-arrival
               (select stock-arrival-parameter
                 (join stock)
                 (aggregate "count (distinct fkey_stock)" :stocks)
                 (group :fkey_stock-arrival)
                 (fields :fkey_stock-arrival)
                 (where {:fkey_stock-arrival [in (map :id result)]
                         :stock.status [in (map stock-status [:visible :invisible])]})
                 (where (or {:price 0}
                            {:price nil})))))]
    (-> result
        (annotate-map-from-sql (fmap :stocks statistics) :stocks)
        (annotate-map-from-sql (fmap :sum statistics) :sum)
        (annotate-map-from-sql not-filled-prices :not-filled-stocks))))

(defn arrival:create-new-stock-arrival[arrival-name supplier & [type]]
  (:id (insert stock-arrival (values {:fkey_supplier *current-supplier*
                                      :fkey_user (sec/id)
                                      :created (sql-now-timestamp)
                                      :type (or type :arrival)
                                      :parameters {:name arrival-name :supplier supplier}}))))

(defn arrival:count-in-database
  "Count stock in given arrival with respect of current state of session map."
  [arrival-id session-stock-ids removed-ids]
  (count
   (set/difference
    (set/union
     (set session-stock-ids)
     (set (map :fkey_stock (select stock-arrival-parameter
                             (modifier "distinct")
                             (fields-only :fkey_stock)
                             (where {:fkey_stock-arrival arrival-id})))))
    (set removed-ids))))

(defn arrival:get-arrival-by-id[arrival-id]
  (select-single stock-arrival
    (where {:id arrival-id})))

(defn arrival:remove-arrival-by-id[arrival-id]
  (delete stock-arrival
    (where {:id arrival-id})))

(defn arrival:get-arrival-sum[arrival-id]
  (select-single stock-arrival-parameter
    (aggregate (sum (raw "coalesce(price,0) * coalesce(quantity,0)")) :sum)
    (where {:fkey_stock-arrival arrival-id})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Procurement specific methods

(declare consumed-arrived-stocks)

(defn procurement:need-procurement-stock-ids
  "Return list of stocks that was at least one time arrived for current
  supplier, and have too low remaining quantity."
  [procurement-id]
  (let [{:keys [supplier]}
        (:parameters (arrival:get-arrival-by-id procurement-id))

        arrivals-with-given-supplier
        (filter (comp (partial = supplier) (comp :supplier :parameters)) (select stock-arrival))]
    (map :id
         (select stock
           (modifier "distinct")
           (fields-only :stock.id)

           ;; filter by arrivals for same supplier
           (join [:stock-arrival-parameter :sap] (= :stock.id :sap.fkey_stock))
           (join [:stock-arrival :sa] (= :sa.id :sap.fkey_stock-arrival))
           (where {:sa.id [in (map :id arrivals-with-given-supplier)]})

           ;; join stock parameters with 'left' property attached for each parameter
           (join stock-parameter-value)
           (where {:stock-parameter-value.enabled true
                   :stock-parameter-value.visible true})

           (join :left [(subselect [(consumed-arrived-stocks) :left]
                                   (fields-only :fkey_stock ["unnest(params)" :param] :arrived :consumed)) :left]
                 (= :left.param :stock-parameter-value.id))

           ;; left only parameters that are not available(or not yet arrived in the system)
           (where (or {(raw "arrived - consumed") 0}
                      {:left.arrived nil}))

           (dry-transforms)))))

(defn procurement:stocks-from-same-supplier
  "Return list of stocks that was at least one time arrived for current
  supplier, and have too low remaining quantity."
  [procurement-id]
  (let [{:keys [supplier]}
        (:parameters (arrival:get-arrival-by-id procurement-id))

        arrivals-with-given-supplier
        (filter (comp (partial = supplier) (comp :supplier :parameters)) (select stock-arrival))]
    (map :id
         (select stock
           (modifier "distinct")
           (fields-only :stock.id)

           ;; filter by arrivals for same supplier
           (join [:stock-arrival-parameter :sap] (= :stock.id :sap.fkey_stock))
           (join [:stock-arrival :sa] (= :sa.id :sap.fkey_stock-arrival))
           (where {:sa.id [in (map :id arrivals-with-given-supplier)]})

           (dry-transforms)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Inventory

(declare consumed-arrived-stocks-query)
(declare order-stock-consumed)
(declare analytics:annotate-stock-with-params)

(defn inventory:create-inventory
  "Create stock arrival with type :inventory + creates snapshot of current
  state of the warehouse"
  [arrival-name supplier type]
  (transaction
    (let [arrival (arrival:create-new-stock-arrival arrival-name supplier type)]
      (insert-select inventory-available-stocks
        (fields-only :fkey_stock :params :quantity :fkey_stock-arrival)
        (select-values
         (-> (select* [(subselect (consumed-arrived-stocks-query
                                   :consumed (-> (order-stock-consumed)
                                                 (join :inner [:retail-order :ro] (= :uoi.fkey_retail-order :ro.id))
                                                 (where (not (= :ro.status (retail-order-status :created)))))))
                       :snapshot])
             (fields-only "fkey_stock" "params" (raw "arrived-consumed") (raw arrival)))))
      arrival)))

(defn inventory:annotate-stock-with-snaphost[inventory-id stocks]
  (analytics:annotate-stock-with-params
    stocks
    :left-snapshot
    (select inventory-available-stocks
      (where {:fkey_stock-arrival inventory-id
              :fkey_stock [in (map :id stocks)]}))
    :quantity))

(defn inventory:annotate-single-stock-with-snaphost[inventory-id stock]
  (first (inventory:annotate-stock-with-snaphost inventory-id [stock])))

(defn inventory:missed-quantity-stocks[inventory-id]
  (map :fkey_stock
       (select stock-arrival-parameter
         (fields-only :fkey_stock)
         (modifier "distinct")
         (where {:fkey_stock-arrival inventory-id})
         (where (not= :quantity 0)))))

(defn inventory:close-inventory
  "Mark inventory as closed + refresh closed"
  [inventory-id]
  (transaction
   ;; set status 'closed' for inventory
   (update stock-arrival
           (set-fields {:status :closed})
           (where {:id inventory-id}))

   ;; refresh all associated arrival parameters so mat-view will catch update
   (update stock-arrival-parameter
           (set-fields {:quantity :quantity})
           (where {:fkey_stock-arrival inventory-id}))

   ;; check if some orders are became with negative quantity - and recompute 'closed' quantity for them.
   ;; only 'created' orders allowed, so total decrease of quantity can't be more than =
   ;; left on warehouse + quantity in created orders
   (doseq [retail-order-id
           (map :id (select [(consumed-arrived-stocks) :left]
                            (join :inner [:user-order-item :uoi] (= :uoi.fkey_stock :left.fkey_stock))
                            (join :inner [:retail-order :ro] (= :ro.id :uoi.fkey_retail-order))
                            (join :inner [:stock :s] (= :s.id :left.fkey_stock))
                            (fields-only [:ro.id :id])
                            (order :ro.id :desc)
                            (where (< (raw "arrived - consumed") 0))
                            (where {:ro.status (retail-order-status :created)})
                            (where {:s.status [not= (stock-status :deleted)]})))]
     (retail:order-recompute-closed retail-order-id))

   ;; check that no negative quantity entries exists - if exists - throws an exception
   (let [stocks-from-inventory
         (map :fkey_stock (select stock-arrival-parameter
                                  (modifier "distinct")
                                  (fields-only :fkey_stock)
                                  (where {:fkey_stock-arrival inventory-id})))]
     (when-let [negative-quantity
                (seq (select [(consumed-arrived-stocks) :left]
                             (join :inner [:stock :s] (= :s.id :left.fkey_stock))
                             (where {:s.status [not= (stock-status :deleted)]})
                             (fields-only :s.id :left.arrived :left.consumed)
                             (where (< (raw "arrived - consumed") 0))
                             (where {:s.id [in stocks-from-inventory]})))]
       (info "Negative quantity" negative-quantity)
       ;; select all orders that was done between inventory snapshot creating and closing, and was closed
       (let [inventory-start (:created (select-single stock-arrival (where {:id inventory-id})))
             bad-orders (select retail-order
                                (where (and {:changed [>= inventory-start]}
                                            {:changed [< (sql-now-timestamp)]}))
                                (where {:status [not= (retail-order-status :created)]}))]
         (info "Bad orders" (vec (map :id bad-orders)))
         (throwf "Inventory should be recreated. Some orders was closed between snapshot and close events."))))))

(declare order-stock-arrived)

(defn inventory:sums[inventory-id]
  (let [last-arrived-price
        [(subselect (select*
                     (-> (order-stock-arrived)
                         (fields-only :fkey_stock :params
                                      [(raw "array_agg(price ORDER BY sa.id)") :prices])
                         (where {:sa.type (stock-arrival-type :arrival)}))))
         :last-prices]

        given-inventory-quantity
        [(subselect (select*
                     (-> (order-stock-arrived)
                         (where {:fkey_stock-arrival inventory-id}))))
         :inventory-quantity]

        sign
        (fn[{:keys [sign]}]
          (case sign
            1.0 :positive
            -1.0 :negative
            0.0 :zero))]
    (fmap
     :sum
     (group-by-single
      sign
      (select (-> (select* last-arrived-price)
                  (join :inner given-inventory-quantity (= :last-prices.params :inventory-quantity.params))
                  (aggregate (sum (raw "quantity * prices[array_length(prices,1)]")) :sum (raw "sign(quantity)"))
                  (fields (raw "sign(quantity)"))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Special arrivals(used for simple accounting). Managed by systems, created and
;; closed automatically by system

(defn- special-arrival:get[]
  (:id (or (first
            (select-null stock-arrival
                         (where {:type (stock-arrival-type :arrival)
                                 :status (stock-arrival-status :special)
                                 :fkey_supplier *current-supplier*})))
           (insert stock-arrival
             (values {:type :arrival
                      :status :special
                      :fkey_supplier *current-supplier*
                      :parameters {:name (str "Приход за " (now)) :supplier "random"}
                      :created (sql-now-timestamp)})))))

(defn- special-arrival:get-all-for-stock[stock-id]
  (group-by
   :fkey_stock-arrival
   (select stock-arrival-parameter
     (with stock-parameter-value)
     (join stock-arrival)
     (where {:stock-arrival.type (stock-arrival-type :arrival)
             :stock-arrival.status [in (map stock-arrival-status [:special :closed])]
             :stock-arrival.fkey_supplier *current-supplier*
             :fkey_stock stock-id}))))

(defn special-arrival:close[]
  (update stock-arrival
    (set-fields {:status :closed})
    (where {:id (special-arrival:get)})))

(defn- special-arrival:save[stock-id params quantity]
  (let [last-arrival (special-arrival:get)
        all-arrivals (special-arrival:get-all-for-stock stock-id)
        quantity-for-arrival (fn[arrival requested-quantity]
                               (let [{:keys [quantity]} (->>
                                                         arrival
                                                         (get all-arrivals)
                                                         (find-by-val :parameters params))
                                     new-quantity (+ (or quantity 0) requested-quantity)]
                                 new-quantity))]

    (reduce (fn[quantity arrival]
              (let [new-quantity (quantity-for-arrival arrival quantity)
                    [left new-quantity] (if (< new-quantity 0) [new-quantity 0] [0 new-quantity])]
                (info "Set special quantity for arrival:" arrival "quantity: "new-quantity)
                (arrival:save-stock-arrival
                 stock-id arrival [{:parameters params :quantity new-quantity}]
                 :overwrite-everything false)
                left))
            quantity
            (distinct (cons last-arrival (reverse (sort (keys all-arrivals))))))))
