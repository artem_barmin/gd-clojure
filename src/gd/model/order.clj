(in-ns 'gd.model.model)

;; Post-processing hooks
(declare orders:user-order-stock-parameter-post)
(declare orders:compute-deal-order-completness)
(declare orders:user-order-sum)
(declare orders:deal-order-name)
(declare orders:user-order-booking)
(declare stock-context:check-order-item)
(declare additional-expense:compute-entity-expense)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Safe delete - mark entity as deleted
(defmacro safe-delete[entity & body]
  `(update ~entity (set-fields {:deleted true}) ~@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Entities definition

(defentity user-order-item
  (enum order-item-payment-status [:payment-status]
        :payed                          ;оплачен
        :created                        ;только создан
        )
  (transform #'stock-context:check-order-item)
  (transform #'additional-expense:compute-entity-expense)
  (transform #'orders:user-order-stock-parameter-post)
  (transform #'orders:user-order-booking)
  (prepare (fn[item] (merge item {:changed (raw "now()")})))
  (belongs-to stock {:fk :fkey_stock})
  (has-many-join-table stock-parameter-value)
  (default-where (= :deleted false)))

(extend-entity stock
  (has-many user-order-item {:fk :fkey_stock}))

(defn orders:user-order-stock-parameter-post
  "Compute sum of order-item with respect to selected params(and their price
  change). Also respect discount and additional cost fields."
  [{:keys [stock-parameter-value stock quantity closed price status]
    :as user-order-item-entity}]
  (if (:id stock)
    (let [params (params-as-map stock-parameter-value)
          price (or price (:price (get (:stock-variant stock) params)) (:price stock))
          quantity (- quantity closed)
          sum (* price quantity)]
      (-> user-order-item-entity
          (assoc :sum sum
                 :quantity quantity
                 :original-quantity (:quantity user-order-item-entity)
                 :params (params-as-map stock-parameter-value))
          (assoc-in [:stock :price] price)))
    user-order-item-entity))

(defn orders:user-order-booking[{:keys [payment-status] :as order-item}]
  (if (not= payment-status :created)
    (assoc order-item :booked-until nil)
    order-item))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Order lists

(defn orders:get-order-item[order-item-id]
  (select-single user-order-item
    (with stock (with stock-parameter-value))
    (with stock-parameter-value)
    (where {:id order-item-id})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Order creation functions
;; TODO : Validate : all item id's show be available from given group deal, otherwise - send exception
(defn orders:get-stock-meta-parameters
  "Resolve parameter from simple map, that came from web, to list of stock-value entities, that
will be used for storing selected values. We should not check params for match if we edit order-item."
  [stock-id params-map & [check-params]]
  (let [stock-parameters
        (select stock-parameter-value
          (where {:fkey_stock stock-id
                  :enabled true}))

        resolved-params
        (meta:resolve-params-map stock-parameters params-map)]
    (if (and check-params
             (not (= (count (distinct (map :name resolved-params)))
                     (count (distinct (map :name (remove (comp not :visible) stock-parameters))))
                     (count params-map))))
      (do
        (fatal "Params"
               "Request:" params-map
               "Resolved:" (distinct (map :name resolved-params))
               "Stock params:" (map #(select-keys % [:name :value])
                                    (remove (comp not :visible) stock-parameters)))
        (throwf "Not all stock parameters are choosed for order")))
    resolved-params))

(defn- orders:save-order-param-assoc[order-item-id changed-params & [previous-params]]
  (doseq [param changed-params]
    ;; if exists - delete previous associations of order items with params
    (if previous-params
      (doseq [param-to-delete (filter #(= (:name %) (:name param)) previous-params)]
        (delete :user-order-item-with-stock-parameter-value
          (where {:user-order-item-key order-item-id
                  :stock-parameter-value-key (:id param-to-delete)}))))
    ;; create new association (user order item -> stock param)
    (insert :user-order-item-with-stock-parameter-value
      (values {:user-order-item-key order-item-id
               :stock-parameter-value-key (:id param)}))))

(defn- orders:existing-order-item-for-params-and-parent[parent-order-id
                                                        parent-order-key
                                                        fkey_stock
                                                        params]
  (let [param-ids
        (set (map :id params))

        items-with-params
        (map
         (fn[item] (update-in item [:params] (comp set from-array)))
         (select [:user-order-item :uoi]
           (join :inner [:user-order-item-with-stock-parameter-value :uop]
                 (= :uop.user-order-item-key :uoi.id))
           (fields
            :uoi.id
            [(sqlfn :array_agg :stock-parameter-value-key) :params])
           (where {parent-order-key parent-order-id
                   :uoi.deleted false
                   :fkey_stock fkey_stock})
           (group :uoi.id)))]
    (:id (find-first (comp (partial = param-ids) :params) items-with-params))))

(let [specific-order-item-params [:fkey_stock :quantity :note :status]]
  (defn- orders:order-item-to-parameters[item]
    (let [params-map (apply dissoc item specific-order-item-params)]
      (orders:get-stock-meta-parameters (:fkey_stock item) params-map true)))

  (defn- orders:save-user-order-item[parent-order-id item & {:keys [parent-order-key used-context]
                                                             :or {parent-order-key :fkey_user-order}}]
    {:pre [parent-order-id]}
    (transaction
      (let [stock-parameter-entities (orders:order-item-to-parameters item)]
        (if-let [existing-order-item
                 (orders:existing-order-item-for-params-and-parent
                  parent-order-id parent-order-key
                  (:fkey_stock item) stock-parameter-entities)]
          (do (update user-order-item
                (set-fields {:quantity (raw (str "quantity+" (parse-int (:quantity item))))
                             :closed 0})
                (where {:id existing-order-item}))
              existing-order-item)
          (let [used-context
                (or
                 used-context
                 (:fkey_stock-context
                  (select-single stock
                    (with stock-overwrite)
                    (where {:id (:fkey_stock item)}))))

                now-tm (sql-now-timestamp)

                order-item-to-save
                (-> item
                    (select-keys specific-order-item-params)
                    (merge {parent-order-key parent-order-id
                            :fkey_stock-context used-context
                            :closed 0
                            :created now-tm
                            :changed now-tm
                            :booked-until (tm/to-timestamp (plus (now) (days 3)))}))

                saved-user-order-item-id
                (:id (insert user-order-item
                       (values order-item-to-save)))]
            (when (and (seq *context-types*) (not used-context))
              (throwf "Stock don't have single context in multi-context mode. So we can't create order"))
            (orders:save-order-param-assoc
             saved-user-order-item-id stock-parameter-entities)
            saved-user-order-item-id))))))

(defn- orders:fixate-user-order-item-price-fun[order-items]
  (transaction
    (doseq [{:keys [id price fkey_stock stock]} order-items]
      (when-not price
        (let [new-price (:price stock)]
          (when-not (number? new-price)
            (throwf "Price is not a number %s" new-price))
          (info "Fixate price for item" id "with value" new-price "in context" *context-types*)
          (update user-order-item
            (set-fields {:price new-price})
            (where {:id id})))))))

(defmacro orders:fixate-user-order-item-price
  "Copy price from stock into order item. This allow us to safely change price on the stock, and
also keep real history of sales"
  [condition]
  `(orders:fixate-user-order-item-price-fun
    (select user-order-item (with stock-parameter-value) (with stock) (where ~condition))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Booking management for orders

(defn order:extend-booking-time[order-item-id hours-to-extend]
  (transaction
    (let [{:keys [booked-until]}
          (select-single user-order-item (where {:id order-item-id}))]
      (update user-order-item
        (set-fields {:booked-until (tm/to-timestamp (plus (tm/from-date booked-until) (hours hours-to-extend)))})
        (where {:id order-item-id})))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Order item payment

(defn order:set-item-payment-status[order-item-id payment-status]
  (update user-order-item
    (set-fields {:payment-status payment-status})
    (where {:id order-item-id})))
