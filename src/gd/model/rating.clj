(in-ns 'gd.model.model)

(defmulti rating:check-constraints :rating-type)

(defmethod rating:check-constraints :default [_]
  true)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Model

(defentity rating
  (enum rating-type [:rating-type] :consumer :stock :compensate)
  (enum rating-status [:status] :new :accepted :rejected)
  (default-where (= :status (rating-status :accepted)))
  (belongs-to stock {:fk :fkey_stock})
  (belongs-to user {:fk :author}))

(defentity rating-visits
  (belongs-to user {:fk :fkey_user}))

(extend-entity stock
  (has-many rating {:fk :fkey_stock}))

(extend-entity user
  (has-many rating {:fk :fkey_user}))

(defn- resolve-type-to-field[type]
  (case type
    (:consumer :compensate) :fkey_user
    :stock :fkey_stock))

(defn rating:TEST:insert-raw-stock-rating
  "Must only be used for testing. Insert rating value without any constraint checks."
  [author-id stock-id positive negative & [comment status]]
  (dotimes [i (+ positive negative)]
    (insert rating (values {:fkey_stock stock-id
                            :rating-value (if (>= i positive) -1 1)
                            :status (or status :new)
                            :author author-id
                            :comment comment
                            :rating-type :stock
                            :created (sql-now-timestamp)}))))

(defn rating:rate[target type comment rate & {:keys [status] :or {status :new}}]
  (let [value
        {:author (sec/id)
         (resolve-type-to-field type) target
         :comment comment
         :rating-value rate
         :status status
         :rating-type type
         :created (sql-now-timestamp)}]
    (when-not (rating:check-constraints value)
      (throwf "Rating constraints violation"))
    (insert rating (values value))))

(defn rating:compute-rate
  "Simple rate computing function"
  [target type]
  (:overall-rating
   (select-single rating
                  (aggregate (sum :rating-value) :overall-rating)
                  (where {(resolve-type-to-field type) target
                          :rating-type (rating-type type)}))))

(defn rating:fetch-rating
  "Simple rate computing function"
  [query]
  (-> query
      (with rating (with user (with auth-profile))
        (order :created :desc))))

(defn rating:get-single-rating[id]
  (disable-default-where (select-single rating (where {:id id}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; View helpers of rating constratins

(defn rating:can-leave-review?[target type]
  (rating:check-constraints {:rating-type type
                             :author (sec/id)
                             (resolve-type-to-field type) target}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Reviews moderation

(defn rating:waiting-for-moderation[limit-num offset-num & {:keys [status] :or {status [:new]}}]
  (disable-default-where
    (select rating
      (where {:status [in (map rating-status status)]})
      (join stock)
      (where {:stock.fkey_supplier *current-supplier*})
      (offset offset-num)
      (limit limit-num)
      (order :id)
      (count-query))))

(defn rating:change-state[rating-id good]
  (transaction
    (let [current-status (:status (rating:get-single-rating rating-id))
          next-status (if good :accepted :rejected)]
      (update rating
        (set-fields {:status next-status})
        (where {:id rating-id})))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Function that compute rating for order by

(defn- rating:positive-negative-query[type]
  (-> (select* rating)
      (aggregate "sum(case when \"rating-value\" > 0 then \"rating-value\" else 0 end)" :positive)
      (aggregate "-1 * sum(case when \"rating-value\" < 0 then \"rating-value\" else 0 end)" :negative)
      (fields (resolve-type-to-field type))
      (group (resolve-type-to-field type))
      (where {:rating-type (rating-type type)})))

(defn- rating:average-rating-query[type]
  (let [rating-query "1.0 * positive / (positive + negative) - 0.6 * (positive / (positive + negative)) / sqrt(positive + negative)"]
    (->
     (select* (subselect (as (rating:positive-negative-query type) :computed-rating)))
     (fields [rating-query "rating-value"] "positive" "negative" (resolve-type-to-field type)))))

(defn rating:compute-rating-query[query type join-field]
  (->
   query
   (join (subselect (as (rating:average-rating-query type) :computed-rating))
         (= join-field (raw (str "\"computed-rating\"." (name (resolve-type-to-field type))))))
   (update-in [:fields] concat
              [[(sqlfn :coalesce :computed-rating.rating-value 0) :rating-value]
               :computed-rating.positive
               :computed-rating.negative])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fillness rating(the better stock or user information - better rating)

(defn rating:compute-user-fillness-old[{:keys [real-name sex birthdate id real-activities contact images] :as user}]
  (let [{:keys [phone email]} contact

        compute-value (fn[{:keys [count weight] :as obj}]
                        (assoc obj :value (* (or count 0) weight)))

        compensation (rating:compute-rate id :compensate)

        reasons (map (fn[[name checked value]]
                       (compute-value {:name name :count (if checked 1 0) :weight value}))
                     [[:name (seq real-name) 5]
                      [:birthdate birthdate 2]
                      [:sex sex 3]
                      [:activites (seq real-activities) 5]
                      [:avatar (not ((set (vals nophoto)) (:code images))) 5]
                      [:phone (:active phone) 45]
                      [:email (:active email) 10]
                      [:compensation compensation (or compensation 0)]])

        external-reasons (map (fn[[name weight count]]
                                (compute-value {:name name :count count :weight weight}))
                              [[:review 10 (select-count rating (where {:author id}))]
                               [:invite 10 (get-count-db (invite:list user))]
                               [:forum 2 (forum:get-messages-count id)]])]
    (assoc user
      :fillness (reduce + (map :value (concat reasons external-reasons)))
      :fillness-reasons (->>
                         reasons
                         (remove (comp zero? :count))
                         (concat external-reasons))
      :fillness-improve (->>
                         (remove (comp (partial = :compensation) :name) reasons)
                         (filter (comp zero? :count))
                         (concat external-reasons)
                         (remove (comp zero? :weight))
                         (sort-by :weight (comparator >))
                         (map (juxt :name :weight))))))

(defn rating:compute-user-fillness[{:keys [real-name sex birthdate id real-activities contact images] :as user}]
  user)

(defn rating:compute-organizer-fillness[{:keys [real-name sex birthdate id real-activities contact images] :as user}]
  (let [{:keys [phone email]} contact
        reasons (map (fn[[name checked]] {:name name
                                          :checked checked})
                     [[:phone (:active phone)]
                      [:email (:active email)]])]
    (assoc user :org-fillness-reasons (->>
                                       reasons
                                       (remove :checked)
                                       (map :name)))))

(defn rating:compensate-rating
  "Used for compensate rating changes when fillness weight are changed"
  [type old-weight new-weight]
  (sec/wrap-model-security (user:get-by-name "root")
    (doseq [{:keys [fillness-reasons id]} (user:get-all-users nil nil)]
      (let [count (reduce + (map :count (filter (comp (partial = type) :name) fillness-reasons)))
            old-rating (* count old-weight)
            new-rating (* count new-weight)
            change (- old-rating new-rating)]
        (when (> change 0)
          (info "User rating compensation" id type old-rating new-rating)
          (rating:rate id :compensate
                       (format "compensation %s(weight:%s->%s) : %s -> %s = +%d"
                               type old-weight new-weight
                               old-rating new-rating change)
                       change))))))
