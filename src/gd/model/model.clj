(ns gd.model.model
  (:use gd.utils.pub_sub
        gd.model.context
        gd.utils.test
        clojure.test
        gd.utils.common
        gd.log
        gd.utils.db
        [gd.utils.mail :only [send-mail]]
        [gd.utils.sms :only [send-sms send-sms-unsafe]]

        [clojure.java.shell :only (sh)]
        [robert.bruce :only [try-try-again]]
        clojure.algo.generic.functor
        clostache.parser
        robert.hooke

        korma.core
        gd.utils.bbcode

        [clj-time.core :exclude [extend]]
        digest
        gd.utils.json-path)

  (:require [noir.options :as options]
            [noir.session :as session])
  (:require
   [overtone.at-at :as at]
   [gd.utils.profiler :as profiler]
   [clojure.data]
   [crypto.random :as crypto]
   [clj-time.coerce :as tm]
   [clojure.string :as strings]
   [gd.utils.security :as sec]
   [clojure.java.io :as io]
   [gd.utils.vk-common :as vk]
   [clojure.set :as set]
   [clojure.walk :as walk]
   [closchema.core :as schema]
   [schema.core :as s])
  (:import
   (java.io File FileOutputStream)
   (ru.perm.kefir.bbcode BBProcessorFactory TextProcessor)
   (java.net ConnectException)
   (java.util.concurrent locks.ReentrantLock)))

(declare rating:fetch-rating)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Generic context annotation resolver for entities - resolve list of context
;; ids to their types

(declare stock-context:resolve-id-to-type)
(declare stock-context:resolve-or-create-type)

(defn stock-context:resolve-entity-contexts[entity]
  (array-transform entity
                   :contexts
                   :val-transform #'stock-context:resolve-id-to-type
                   :as-set true
                   :val-prepare #'stock-context:resolve-or-create-type))

(defmacro with-demo[& body]
  `(binding [*database-override* (custom-sql-databases :demo)
             *lobos-database-override* :demo]
     ~@body))

(defn only-visible-stocks?[]
  (= :available-from-arrival (:stocks-mode *supplier-info*)))

(declare visible-stocks)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Schema utils

(defn s:id [name]
  (s/named Long name))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Load files

(load "db-cache")
(load "debug")

(load "images")
(load "message")
(load "forum")
(load "category")
(load "user-category")
(load "user")
(load "stock-parameters")
(load "stock-variants")
(load "stock")
(load "order")
(load "persistent")
(load "retail")
(load "domain")
(load "dictionary")
(load "invite")
(load "rating")
(load "search")
(load "user-account")
(load "scheduled")
(load "site-pages")

(load "stock-arrival")
(load "stock-context")
(load "analytics")
(load "reports")

(load "security")
(load "notifications")
(load "cache")
(load "opencart")

(defmacro wrap-in-host[host & body]
  `(set-global-context
    (#'gd.model.context/resolve-host-to-info-id ~host "/")
    (wrap-context *stock-context* ~@body)))
