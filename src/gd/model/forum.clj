(in-ns 'gd.model.model)

(declare forum:count-child-themes)

(declare forum-theme)
(defentity forum-theme
  (transform #(forum:count-child-themes %))
  (belongs-to forum-theme {:fk :fkey_forum-theme :entity :parent})
  (belongs-to message {:fk :last-message})
  (belongs-to user {:fk :fkey_user}))
(extend-entity message
  (belongs-to forum-theme {:fk :author}))

(defn- forum:get-theme[theme-id]
  (select-single forum-theme (where {:id theme-id})))

(defn- forum:get-theme-by-name[name]
  (first (select-null forum-theme (where {:name name}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; List forum messages
(defn forum:theme-messages[theme-id limit-num offset-num]
  (let [theme (forum:get-theme theme-id)]
    (message:messages theme :inbox :forum
                      {:offset-num offset-num
                       :limit-num limit-num
                       :fetch-reply true
                       :ordering :asc})))

(def forum:pages-count (partial pages-count 10))

(defn- forum:count-child-themes[theme]
  (assoc theme :childs-count
         (:cnt
          (select-single forum-theme
                         (aggregate (count :*) :cnt)
                         (dry-transforms)
                         (where {:fkey_forum-theme (:id theme)})))))

(defn forum:get-page-by-message-id[message-id]
  (let [{:keys [recipient sent-time]} (select-single message (where {:id message-id}))]
    (->
     (select-single message
       (aggregate (count :*) :cnt)
       (where
        (and {:recipient recipient :removed false :type (message-type :forum)}
             (< :message.sent-time sent-time))))
     :cnt
     inc
     forum:pages-count)))

(defn forum:theme-childs[parent-id]
  (select forum-theme
    (where {:fkey_forum-theme parent-id})
    (with user)
    (with message (with user (with auth-profile)))
    (order :id :asc)))

(defn forum:top-themes[]
  (forum:theme-childs nil))

(defn forum:theme-editable?[theme-id]
  (:editable (select-single forum-theme (where {:id theme-id}))))

(defn forum:breadcrumbs[theme-id]
  (loop [parent-id theme-id
         breadcrumbs nil]
    (let [theme (select-single forum-theme (with forum-theme) (where {:id parent-id}))
          new-breadcrumbs (cons theme breadcrumbs)]
      (if (:parent theme)
        (recur (:id (:parent theme))
               new-breadcrumbs)
        new-breadcrumbs))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Added new forum message
;; TODO : add multiple one to one connections and rewrite last message theme relation
(defn forum:add-message[theme-id text author & [reply-to]]
  (let [theme-entity (forum:get-theme theme-id)]
    (if (:editable theme-entity)
      (transaction
        ;; send message
        (let [message (message:send {:text text :type :forum :reply-to reply-to} author theme-entity)
              parents (map! :id (forum:breadcrumbs theme-id))]
          ;; update message counter all themes up
          (update forum-theme
            (set-fields {:messages-count (raw "\"messages-count\" + 1")
                         :last-message (:id message)
                         :last-message-theme-id (:id theme-entity)
                         :last-message-theme-name (:name theme-entity)})
            (where {:id [in parents]}))
          message))
      (throwf "Can't add message to non-editable theme. Possible bug, or atack."))))

(defn forum:remove-message[theme-id message-id]
  (transaction
    (message:remove message-id)
    (update forum-theme
      (set-fields {:messages-count (raw "\"messages-count\" - 1")})
      (where {:id theme-id}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Themes manipulation
(defn- forum:add-theme[name editable author & {:keys [parent description category]}]
  (let [parent-theme
        (when (and parent (:editable (forum:get-theme parent)))
          (throwf "Can't add theme to editable theme."))

        theme-entity
        (insert forum-theme (values {:fkey_forum-theme parent :messages-count 0
                                     :name name
                                     :editable editable
                                     :description description
                                     :category category
                                     :fkey_user (:id author)}))]
    theme-entity))

(defn forum:add-theme-with-first-message[name message author & params]
  (let [theme-id
        (:id (apply forum:add-theme (concat (list name true author) params)))]
    (forum:add-message theme-id message author)
    theme-id))

(defn forum:add-parent-theme[name author & params]
  (:id (apply forum:add-theme (concat (list name false author) params))))

(defn forum:save-or-update-tree
  ([tree author]
     (transaction
       (forum:save-or-update-tree tree nil author)))
  ([tree parent-id author]
     (doseq [[name & {:keys [desc childs exists cat first-message] :as params}] tree]
       (let [theme (if-let [theme (select-null forum-theme (where {:name name
                                                                   :fkey_forum-theme parent-id}))]
                     (first theme)
                     (if exists
                       (throwf "Theme '%s' is not exist" name )
                       (if first-message
                         (forum:add-theme-with-first-message
                           name first-message author
                           :parent parent-id
                           :description desc
                           :category cat)
                         (forum:add-theme
                          name false author
                          :parent parent-id
                          :description desc
                          :category cat))))]
         (assert (or (every? :first-message childs) (every? (comp not :first-message) childs))
                 "Themes should be of same type on each given level")
         (forum:save-or-update-tree childs (:id theme) author)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Misc utils
(defn forum:get-theme-users[theme-id]
  (map :author
       (select (message:request-query {:id theme-id} :inbox :forum false)
         (fields-only :author)
         (modifier "distinct"))))

(defn forum:get-messages-count[user-id]
  (select-count message
                (fields-only)
                (where {:author user-id
                        :type (message-type :forum)})))
