(in-ns 'gd.model.model)

(declare user:set-name)
(declare user:set-nophoto-image)
(declare user:contacts-processor)
(declare user:role-names)
(declare user:full-address)
(declare user:update-address)

(declare stock)
(declare rating)
(declare user)

(declare rating:compute-user-fillness)
(declare notify-about-user-registration)

(defentity contact
  (enum contact-type [:type] :email :phone)
  (belongs-to user {:fk :fkey_user}))

(defalias user-owner user)

(defentity bookmark
  (belongs-to user-owner {:fk :owner})
  (belongs-to user {:fk :fkey_user}))

(defentity auth-profile
  (enum auth-type [:type] :vk :internal :classmates :my-world)
  (belongs-to user {:fk :fkey_user}))

(defentity role)

(defentity user-address
   (json-data :extra-info)
   (belongs-to user {:fk :fkey_user}))

(def all-roles #{"organizer" "user" "moderator" "manager" "partner" "seo" "admin" "copywriter"})

(defentity user
  (transform #'user:set-name)
  (transform #'user:set-nophoto-image)
  (transform #'user:contacts-processor)
  (transform #'user:role-names)
  (transform #'user:full-address)
  stock-context:resolve-entity-contexts
  (enum user-sex [:sex] :male :female)

  (has-many bookmark {:fk :fkey_user})
  (has-many auth-profile {:fk :fkey_user})
  (has-many contact {:fk :fkey_user})
  (has-many user-address {:fk :fkey_user})
  (has-many-join-table role))

(def avatar-sizes [nil :large-avatar :small-avatar :tiny-avatar :messages-avatar-small :messages-avatar-large])

(defn- user:set-name[{:keys [auth-profile real-name] :as user-entity}]
  (assoc user-entity :name (if (empty? real-name)
                             (some identity (map :login auth-profile))
                             real-name)))

(defn- user:role-names[user-entity]
  (update-in user-entity [:role] (partial map :name)))

(defn- user:full-address[{:keys [user-address extra-info] :as user-entity}]
  (let [{:keys [address address-region] :as addr} (first user-address)]
    (assoc user-entity
      :full-address (cond
                     (and address address-region)
                     (format "%s(%s)" address address-region)

                     address address)
      :extra-info (:extra-info addr))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; No photo processor
(def nophoto {:male (images:generate-image-thumbnails-with-code {:path "resources/public/img/user-profile/ava_male.jpg"} avatar-sizes)
              :female (images:generate-image-thumbnails-with-code {:path "resources/public/img/user-profile/ava_female.jpg"} avatar-sizes)
              nil (images:generate-image-thumbnails-with-code {:path "resources/public/img/user-profile/ava_unisex.jpg"} avatar-sizes)})

(defn- user:set-nophoto-image[{:keys [sex images] :as user-entity}]
  (assoc user-entity :images (or images {:code (get nophoto (from-user-sex sex))})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; User contacts processor
(defn- user:contacts-processor
  "Algorithm for active contacts(only one active contact can exists in group):
1. Contacts are stored as list of entities
2. We group list by :type
3. In each group sort by :active property, to put 'true' entities at the start
4. Take first entity from each group - this is current contact to show

Algorithm for new not-active contacts(determined by ids order)
"
  [{:keys [contact] :as user-entity}]
  (let [keys-to-select
        [:id :active :value :activation-code]

        process-active-list
        (fn[contacts]
          (select-keys
           (->> contacts
                (sort-by :active)
                reverse                  ;put active contacts at start of the
                                        ;list(if they exists)
                first                    ;take first contact(it can be active -
                                        ;if active exists, or not active - if
                                        ;no contacts is activated)
                )
           keys-to-select))

        process-not-active-list
        (fn[contacts]
          (let [{:keys [active] :as last-contact} (last (sort-by :id contacts))]
            (if-not active
              (select-keys last-contact keys-to-select))))]
    (assoc user-entity
      :contact-raw (:contact user-entity)
      :new-contact (fmap process-not-active-list (group-by :type contact))
      :contact (fmap process-active-list (group-by :type contact)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; User news

(defn user:news[user limit-num offset-num]
  (message:messages user :inbox :news {:limit-num limit-num :offset-num offset-num}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bookmarks

(defn user:bookmarks[limit-num offset-num]
  (select bookmark
    (with user)
    (limit limit-num)
    (offset offset-num)
    (order :created :asc)
    (where {:owner (sec/id)})
    (count-query)))

(defn user:get-bookmark[& {:keys [fkey_user]}]
  (select-single-null bookmark
    (where {:owner (sec/id)
            :fkey_user fkey_user})))

(defn user:add-or-update-bookmark[note & {:keys [fkey_user]}]
  (let [criteria {:owner (sec/id)
                  :fkey_user fkey_user}]
    (if (zero? (select-count bookmark (where criteria)))
      (insert bookmark
        (values (merge criteria {:created (sql-now-timestamp) :note note})))
      (update bookmark
        (set-fields {:note note})
        (where criteria)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; User contacts activation subsystem
(defn- user:generate-activation-code[type]
  (case type
    :email (crypto/url-part 64)
    :phone (format "%04d" (rand-int 10000))))

;; TODO : move template to external file
(defn- user:activate-email
  "Send activation code to email(approve that it is real email)"
  [user-id]
  (let [user (select-single user (with contact) (with auth-profile) (where {:id user-id}))]
    (when-not (get-in user [:contact :email :active])
      (info "Send email activation" (:id user) (:name user) (get-in user [:contact :email :activation-code]))
      (send-mail (get-in user [:contact :email :value])
                 (context-info "Приветствуем на сайте \"Bono\"" :activation :email :header)
                 (render-resource
                  (context-info "mail/bono-registration.html" :activation :email :template)
                  {:mail (get-in user [:contact :email :value])
                   :login (last(get-in user [:auth-profile :login]))
                   :password (last(get-in user [:auth-profile :login]))
                   :link (str
                          (context-info "http://dayte-dve.com.ua/activation?code=" :activation :email :link)
                          (get-in user [:contact :email :activation-code]))})))))
(defn user:send-new-code
  "Send activation code to phone(approve that it is real phone). Check if
contact realy need activation(exists new not activated contact)."
  [user-id]
  (when (context-info-by-key :activation :sms)
    (let [user (select-single user (with contact) (where {:id user-id}))]
      (cond
        ;; brand new not active contact
        (and (not (get-in user [:contact :phone :active])) (get-in user [:contact :phone :value]))
        (send-sms (get-in user [:contact :phone :value]) (str "Код: " (get-in user [:contact :phone :activation-code])))

        ;; previous version exists - but new should be activated(it is always not active)
        (get-in user [:new-contact :phone])
        (send-sms (get-in user [:new-contact :phone :value]) (get-in user [:new-contact :phone :activation-code]))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; External handerls for activation requests
(defn user:activate-contact-by-code[code type]
  (let [activate-condition
        (case type
          :phone {:fkey_user (:id (sec/logged))
                  :activation-code code
                  :type (contact-type type)}
          :email {:activation-code code
                  :type (contact-type type)})]
    (transaction
      (if-let [[{:keys [fkey_user]}] (select-null contact (where activate-condition))]
        (do (info "Activate" type code)
            ;; mark all previous contacts as not active(only for phone)
            (update contact
              (set-fields {:active false})
              (where {:fkey_user (case type
                                   :phone (:id (sec/logged))
                                   :email fkey_user)
                      :type (contact-type type)}))
            ;; mark new contact as active
            (update contact
              (set-fields {:active true})
              (where activate-condition)))
        (throwf "Code not valid")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Internal registration subsystem

(defn- user:save-auth-profiles[user-id profiles]
     (map! #(insert auth-profile (values (merge {:fkey_user user-id} %))) profiles))

;; TODO : send activation codes to users
(defn- user:save-or-update-contact
  "Invariants:
1. Only one contact of given type can be active
2. If contacts in group are more than one - one of them MUST be active(it will
be on top)
3. All contacts are ordered by time of creation, only one contact can have date
of creation later then currently active contact.
"
  [user-id current-contacts type value last-non-active-by-time]
  ;; condtion on list - action
  (let [first-contact (first current-contacts)]
    (when
        (cond
          ;; empty list of contracts - just save it + send code
          (= (count current-contacts) 0)
          (insert contact
            (values
             {:fkey_user user-id :type type :value value :active false
              :created (sql-now-timestamp) :activation-code (user:generate-activation-code type)}))

          ;; single non active contact - update it(only if it is really changed)
          (and (= (count current-contacts) 1) (not (:active first-contact))
               (not= value (:value first-contact)))
          (update contact
            (set-fields {:value value :activation-code (user:generate-activation-code type)})
            (where {:id (:id (first current-contacts))}))

          (and (or (> (count current-contacts) 1) (some :active current-contacts)))
          (if last-non-active-by-time
            ;; (one active or (multiple contacts -> we have at least one active contact)) and exists contact that created after - update last contact by date
            (update contact
              (set-fields {:value value :activation-code (user:generate-activation-code type)})
              (where {:id (:id last-non-active-by-time)}))
            ;; (one active or (multiple contacts -> we have at least one active contact)) and no contacts that created after - create new contact
            (insert contact
              (values
               {:fkey_user user-id :type type :value value :active false
                :created (sql-now-timestamp) :activation-code (user:generate-activation-code type)}))))
      (case type
        :phone (user:send-new-code user-id)
        :email (user:activate-email user-id)))))

(defn- user:save-contacts
  [user-id contacts]
  (let [user
        (select-single user
                       (with contact)
                       (where {:id user-id}))

        current-contacts
        (group-by :type (:contact-raw user))]
    (map! (fn[[type value]] (user:save-or-update-contact user-id (type current-contacts) type value (get-in user [:new-contact type]))) contacts)))

(defn- user:save-or-update-roles [user-id roles]
  {:pre [(not (empty? roles)) (every? all-roles roles)]}
  (delete :user-with-role (where {:user-key user-id}))
  (doseq [name (distinct roles)]
    (let [role-id (:id (or (select-single-null role (where {:name name}))
                           (insert role (values {:name name}))))
          connection {:user-key user-id :role-key role-id}]
      (insert :user-with-role (values connection)))))

(defn- user:save[{:keys [role auth-profile contact] :as params} phone]
  (transaction
    (let [{:keys [id] :as user}
          (insert user
            (values
             (-> (dissoc params :auth-profile :contact :role)
                 (update-in [:birthdate] tm/to-sql-date)
                 (merge {:info-registered (sql-now-timestamp)
                         :info-last-visit (sql-now-timestamp)
                         :contexts *context-types*}))))
          mail (:email contact)]
      (user:save-or-update-roles id (if (empty? role) ["user"] role))
      (user:save-auth-profiles id auth-profile)
      (let [auth-profile [{:type ((auth-profile 0) :type)
                           :login (if (empty? phone)
                                    mail
                                    (clojure.string/replace phone #"[()-]" ""))
                           :password  ((auth-profile 0) :password) }]]
      (user:save-auth-profiles id auth-profile))
      (user:save-contacts id contact)
      (notify-about-user-registration id)
      (info "User created" (str user) auth-profile role)
      user)))

(defn user:create-user-internal [params phone]
  (user:save params phone))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main user interaction API
;; Query for fetching user with all needed for future work information
(declare user:get-user-by-auth-profile)

(defn user-full-fetch-query[query]
  (-> query
      (with auth-profile)
      (with contact)
      (with role)
      (with user-address)))

(defn user-full-fetch[] (-> (select* user)
                            (user-full-fetch-query)))

(defn user:get-with-roles[id]
  (select-single user (with role) (where {:id id})))


(defn user:process-address [{:keys [user-address] :as user}]
  (let [user-address (first user-address)
        extra-info (:extra-info user-address)]

    (merge
      user
      (select-keys user-address [:address :address-region])
      {:extra-info extra-info})))

(defn user:get-full
  "Retrieve all information for usage user in session(images, rating, message boxes)"
  [id]
  (-> (select-single
        (-> (user-full-fetch)
            (where {:id id})))
  user:process-address
  rating:compute-user-fillness))

(defn user:get-all-users[limit-num offset-num & {:keys [search-string]}]
  (map rating:compute-user-fillness
       (select (user-full-fetch)
         (limit limit-num)
         (offset offset-num)
         (where-if (seq *stock-context*) {:contexts [overlaps *stock-context*]})
         (where-if search-string {:id [in (subselect :users-search
                                                     (fields :fkey_user)
                                                     (where {
                                                             [:user :contact :login :uuid :fkey_user]
                                                             [search search-string]
                                                             }))]})
         (count-query))))

(defn user:get-by-name[login]
  (user:get-user-by-auth-profile {:login login}))


(defn- user:update-phone [user-id phone]
  (transaction
    (update contact
           (set-fields
             {:value (gd.utils.sms/normalize-number phone)})
           (where {:fkey_user user-id :type (contact-type :phone)}))
    ))

(defn- user:update-auth [user-id login phone password]
  (transaction
    (delete auth-profile
            (where {:fkey_user user-id}))

    (insert auth-profile
            (values {:fkey_user user-id
                     :login     login
                     :password  password
                     :type      :internal}))

    (if (not= login (gd.utils.sms/normalize-number phone))
    (insert auth-profile
            (values {:fkey_user user-id
                     :login     (gd.utils.sms/normalize-number phone)
                     :password  password
                     :type      :internal}))
    )))


(declare user:update-password)

(defn user:update[id {:keys [auth-profile contact role] :as params}]
  {:pre [id]}
  (prn params)
  (transaction
    (let [previous-user-state (select-single user (where {:id id}))]
      (if (contains? params :role)
        (user:save-or-update-roles id (if (empty? role) ["user"] role)))

      ;; TODO : make function for update auth profiles, but only when multiple auth profiles
      ;; will be available. Also check that user can't change login to 'root'
      ;; (if (contains? params :auth-profile)
      ;;   (user:save-auth-profiles id auth-profile))

      (if (contains? params :contact)
        (user:save-contacts id contact))
      (user:update-address id params)
      (update user
              (set-fields
                (-> previous-user-state
                    (merge params)
                    (select-keys [:real-name :birthdate :sex :real-activities :login :info-photo])
                    (update-in [:birthdate] tm/to-sql-date)))
              (where {:id id})))))

(defn user:update-from-admin[id {:keys [auth-profile contact role] :as params} name phone password password_old login]
  {:pre [id]}
  (transaction
    (let [previous-user-state (select-single user (where {:id id}))]
      (if (contains? params :role)
        (user:save-or-update-roles id (if (empty? role) ["user"] role)))

      ;; TODO : make function for update auth profiles, but only when multiple auth profiles
      ;; will be available. Also check that user can't change login to 'root'
      ;; (if (contains? params :auth-profile)
      ;;   (user:save-auth-profiles id auth-profile))
      (if (< (count name) 1)
      (if (contains? params :contact)
        (user:save-contacts id contact)
      (user:update-address id params)))
      (update user
        (set-fields
         (-> previous-user-state
             (merge params)
             (select-keys [:real-name :birthdate :sex :real-activities :login :info-photo])
             (update-in [:birthdate] tm/to-sql-date)))
        (where {:id id})))
    (if (> (count name) 0)
      (do
    (update user
            (set-fields
              {:real-name name})
            (where {:id id}))
    (user:update-password id password_old password)
    (if (> (count phone) 3)
      (do
    (user:update-phone id phone)
    (user:update-auth id login phone password))
      (user:update-auth id login login password))))))

(defn user:create-auth [id login phone password]
  (user:update-phone id phone)
  (user:update-auth id login phone password))

(defn user:update-password[id old new]
  (transaction
    (when (= (:password (select-first auth-profile
                                       (fields-only :password)
                                       (where {:fkey_user id
                                               :type (auth-type :internal)})))
             old)
      (update auth-profile (set-fields {:password new}) (where {:fkey_user id})))))

(defn user:update-address [user-id {:keys [address address-region extra-info country] :as params}]
  (upset user-address
         (merge {:address address
                 :address-region address-region
                 :country country
                 :fkey_user user-id}
                (when extra-info
                  {:extra-info extra-info}))
         {:fkey_user user-id}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Authorization subsystem
;; TODO : add preconditions
(defn- user:get-user-by-auth-profile[params]
  (if-let [[user-ent]
           (select-null auth-profile (where params))]
    (select-single
      (-> (user-full-fetch)
          (where {:id (:fkey_user user-ent)})))))

(defn user:check-credentials
  "Returns user entity if login and password are correct,
false - in all other cases(even if login does not exists)"
  [login password]
  (user:get-user-by-auth-profile {:type (auth-type :internal) :login login :password password}))

(defn user:check-social-credentials
  "Returns user entity if login and password are correct,
false - in all other cases(even if login does not exists)"
  [type uuid]
  (user:get-user-by-auth-profile {:type (auth-type type) :uuid uuid}))

(defn user:authorize-external
  "Try to authorize user from external source by some unique identifier.
Examples:
- vkontakte - id of user(:mid from cookie)

If user for such uid already exists - return it, otherwise - create new user, set auth type
and uid.

Third parameter - map with additional parameters(like photo, first name and surname, city),
it will be used in both cases(first - for creation of user, second - for update existing
user information).

IMPORTANT : we trust to uuid and type, and create or get user without any doubts, please
check all credentials in platform specific way."
  [uuid type params]
  {:pre [uuid type]}
  (let [params (reduce merge (map (fn[[k v]](when v {k v})) params))]
    (if-let [user-ent
             (user:get-user-by-auth-profile {:uuid uuid :type (auth-type type)})]
      (if (empty? params)
        user-ent
        (do
          (user:update (:id user-ent) params)
          (user:authorize-external uuid type {})))
      (do
        ;; create new user
        (user:save
         (merge params {:auth-profile [{:type type :uuid uuid}]}))
        ;; retrieve already saved user from database with all special information
        (user:authorize-external uuid type {})))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Password reminder
(defn- get-email-body [login pass]
  (-> "Здравствуйте!
    На этот e-mail был послан запрос о востановлении учетных данных на интернет-магазине <a href=\"http://bono.in.ua\">bono.in.ua</a>.<br>
    Ваш логин: %login%
    Ваш новый пароль: %pass%<br>
    С уважением,
    Администрация сайта <a href=\"http://bono.in.ua\">bono.in.ua</a>."
    (.replaceAll "%login%" login)
    (.replaceAll "%pass%" pass)
    (.replaceAll "\n[ ]+" "<br>")))

(defn- user:generate-new-password[size]
  (let [chars (concat (range 65 91)                 ;A-Z
                      (range 97 123)                ;a-z
                      (range 48 58)                 ;0-9
                      )]
    (apply str (map #(char (nth chars (%))) (replicate size #(rand-int (count chars)))))))

(defn user:remind-password
  "Generate new password(8 symbols - letters in different cases + numbers) and
send it to last active email, but only if :internal type of auth-profile
exists."
  [email]
  (let [{:keys [id login]}
        (->
         (select-single-null contact
           (with user (with auth-profile))
           (where {:type (contact-type :email) :value email}))
         (get-in [:user :auth-profile])
         ((fn[profiles](filter #(= :internal (:type %)) profiles)))
         first)]
    (when id
      (let [new-pass (user:generate-new-password 8)]
        (update auth-profile
          (set-fields {:password new-pass})
          (where {:id id}))
        (send-mail email "Восстановление пароля" (get-email-body login new-pass))))))

(defn user:check-email-exists[email]
  (select-null contact (where {:type (contact-type :email) :value email})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Email uniquness validator
(defn user:check-email-uniquness[email]
  (= (select-count contact (where {:type (contact-type :email) :value email})) 0))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; User messages sending

(defn user:send-mail[id header body]
  (let [email (get-in (user:get-full id) [:contact :email :value])]
    (send-mail email header body)))

(defn user:send-sms[id text]
  (let [phone (get-in (user:get-full id) [:contact :phone :value])]
    (send-sms phone text)))

(defn user:send-any-sms[phone text]
    (send-sms (gd.utils.sms/normalize-number phone) text))

(defn user:restore-by-sms [phone]
  (let [password (:password (first (select auth-profile (where  {:login phone}))))]
    (if (= password nil)
      "no phone" (send-sms-unsafe (gd.utils.sms/normalize-number phone) (str "Ваш логин:" phone " и пароль:" password)))
    )
  )

(defn user:get-all-emails[]
  (->>
   (user:get-all-users nil nil)
   (map (comp :email :contact))
   (map :value)
   (remove nil?)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Login as user(this temporary session is valid for only 5 minutes, and on 'logout' previous user
;; will be taken from session)
(defn user:check-is-empty-phone [phone]
  (count(select auth-profile (where {:login (gd.utils.sms/normalize-number phone)}))))

(defn- allowed-to-login-as?[user]
  (if (or (sec/partner?) (sec/root?))
    true
    (not (sec/have-role? user "partner"))))

(defn user:login-as-user[user-id]
  (let [user (user:get-full user-id)]
    (if (allowed-to-login-as? user)
      (sec/set-temp-logged user)
      (throwf "Illegal attempt to login"))))

(declare site-page:add-not-mail)
(defn user:delete-from-natification [user-id]
  (let [user (user:get-full user-id)]
    (site-page:add-not-mail (:value (:email (:contact user))))))

(defn user:delete-user-all [user-id]
  (info (str "DELETED USER " user-id))
  (transaction
    (update user (set-fields {:contexts []}) (where {:id user-id}))
    (delete auth-profile (where {:fkey_user user-id}))
    (delete contact (where {:fkey_user user-id}))
    (delete user-address (where {:fkey_user user-id}))
    ))
