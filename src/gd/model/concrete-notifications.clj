(in-ns 'gd.model.model)

(comment "Service layer for notifications. All notifications are implemented as
AOP 'after' advices. We use only 'after' type - because some actions may fail, and
we should not notify user. ")

(comment "Model for notifications :
 fkey_user - recipient of notification(who will see message in feed, MUST be set to user, or NULL),
 source - source of notification(id of message author, id of forum theme, id of deal that changed status)
 info - json additional info
 opened - flag is message is opened
 ")

(comment "Addition of new type of notification:
1. add type in enumeration - notification-type. In the end! Because DB store them as numbers.
2. add type in notify-constraints - to show what entity is SOURCE of the notification
3. add new string representation to main.ru.properties
4. OPTIONAL : if you want to fetch more for source entity - add fetch plan to notify-fetch-plans
5. OPTIONAL : if you want to make notification stacked(wait for serveral minutes until whole pack is collected - add them to the notify-stacked-notifications), where value is number of minutes
")

(defalias user-source user)

;; WARNING : add new types only to the end of enum. Otherwise this will break model.
(defentity notification
  (belongs-to user {:fk :fkey_user})

  (belongs-to user-source {:fk :fkey_source-user :entity :source})
  (belongs-to forum-theme {:fk :fkey_forum-theme :entity :source})
  (belongs-to retail-order {:fk :fkey_retail-order :entity :source})
  (belongs-to site-page {:fk :fkey_site-page :entity :source})

  (enum notification-type [:type]
        :new-forum-message
        :new-private-message

        :user-invite-use
        :global-news

        :money-transaction

        :retail-order
        :retail-order-user-event
        :retail-order-shipping-info

        :site-page-news-published

        :retail-new-user-order

        :retail-new-review
        :retail-user-registration

        :retail-new-order)
  (json-data :info))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Types, constraints and fetching plans

(def forum-events [:new-forum-message])

(def news-events [:global-news :deal-news])

(def retail-events [:retail-order :retail-order-user-event :retail-order-shipping-info :retail-new-user-order :retail-new-order])

(def notify-constraints
  {(concat [:user-invite-use :new-private-message :money-transaction :retail-new-review :retail-user-registration] news-events) user-source
   forum-events forum-theme
   retail-events retail-order
   [:site-page-news-published] site-page})

(def notify-fetch-plans
  {retail-order (fn[query] (-> query
                               (with user
                                     (with auth-profile)
                                     (with user-address)
                                     (with contact))
                               (with user-order-item (with stock) (with stock-parameter-value))))
   @user-source (fn[query] (-> query (with auth-profile)))})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stacked notifications(how long we should wait and group notifications)

(def notify-stacked-notifications #{:retail-order
                                    :retail-order-user-event})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Forum messages tracking
(defnotify message:send [msg from to]
  (cond (= :forum (:type msg))
        (let [theme-id (:id to)
              all-theme-authors (disj (set (forum:get-theme-users theme-id)) (:id from))]
          (map!
           (fn [user] (notify:send user theme-id :new-forum-message {:text (:text msg)
                                                                     :author (:id from)
                                                                     :id (:id fn-result)}))
           all-theme-authors))

        (= :private (:type msg))
        (notify:send (:id to) (:id from) :new-private-message {:text (:text msg)
                                                               :id (:id fn-result)})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Invites
(defnotify invite:use-code[code user-id]
  (when (seq code)
    (let [owner (invite:get-owner-of-code code)]
      (notify:send owner user-id :user-invite-use {:user user-id}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; User money
(defnotify account:transact-money [user-id amount comment]
  (notify:send user-id (sec/id) :money-transaction {:amount amount :comment comment}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Retail notifications
(defn- notify-about-new-order[retail-order-id]
  (let [{:keys [fkey_user]} (retail:get-order retail-order-id)]
    (notify:send fkey_user retail-order-id :retail-new-user-order)
    (doseq [{:keys [id]} (retail:supplier-users)]
      (notify:send id retail-order-id :retail-new-order))))

(defn- notify-about-order-change[retail-order-id]
  (let [{:keys [fkey_user]} (retail:get-order retail-order-id)]
    (notify:send fkey_user retail-order-id :retail-order-user-event)
    (when-not (sec/admin?)
      (doseq [{:keys [id]} (retail:supplier-users)]
        (notify:send id retail-order-id :retail-order)))))

(defnotify retail:delete-order-item [id]
  (let [{:keys [fkey_retail-order]}
        (disable-default-where (select-single user-order-item (where {:id id})))]
    (notify-about-order-change fkey_retail-order)))

(defnotify retail:change-order-item [id]
  (let [{:keys [fkey_retail-order]}
        (disable-default-where (select-single user-order-item (where {:id id})))]
    (notify-about-order-change fkey_retail-order)))

(defnotify retail:send-review[text]
  (doseq [{:keys [id]} (retail:supplier-users)]
    (notify:send id (sec/id) :retail-new-review {:text text :author (sec/id)})))

(defn notify-about-user-registration[user-id]
  (info "Notify about new user" user-id (map :id (retail:supplier-users)))
  (doseq [{:keys [id]} (retail:supplier-users)]
    (notify:send id user-id :retail-user-registration {})))
