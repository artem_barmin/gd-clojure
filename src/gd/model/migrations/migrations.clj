(ns gd.model.migrations.migrations
  (:use gd.utils.db)
  (:use gd.utils.common)
  (:use gd.model.model)
  (:use [lobos.core :only [migrate rollback]])
  (:use [lobos.migration :only [*migrations-namespace* *migrations-table*]]))

(defn migrate-schema[]
  (binding [*ns* (create-ns 'gd.model.migrations.schema-top)]
    (binding [*migrations-namespace* 'gd.model.migrations.schema
              *migrations-table* :schema-migrations]
      (println "Schema migrations")
      (migrate)
      (reset-metadata-cache))))

(defn migrate-metamodel[]
  (binding [*ns* (create-ns 'gd.model.migrations.metamodel-top)]
    (binding [*migrations-namespace* 'gd.model.migrations.metamodel
              *migrations-table* :metamodel-migrations
              noir.options/*options* {:mode :dev}]
      (println "Metamodel migrations")
      (migrate))))
