(ns gd.model.migrations.schema
  (:use [korma.core :exclude (table exec)])
  (:use gd.utils.common)
  (:use gd.utils.schema)
  (:use gd.model.model)
  (:use gd.utils.db)
  (:refer-clojure :exclude [alter drop bigint boolean char double float time complement])
  (:use [lobos.migration :only (defmigration)])
  (:use lobos.schema)
  (:use lobos.core))

(comment "ВНИМАНИЕ : Таблицы называть в единственном числе")

(load "schema/initial-schema")

(defn stock-rating-query[]
  (-> (select* stock)
      (rating:compute-rating-query :stock :stock.id)
      (fields-only
       [(raw "(coalesce(\"rating-value\",0) + stock.priority * 1)")
        :rating-with-priority]
       [(sqlfn :coalesce :computed-rating.rating-value 0) :rating-value]
       [:computed-rating.positive :positive]
       [:computed-rating.negative :negative])))

(defmigration materialized-views-for-stock-search-and-ratings
  (up
   (disable-default-where
     (defmatview stock-rating (stock-rating-query)
       :entity-fields [:stock.id]
       :trigger-fields [[:stock :fkey_stock]
                        [:rating :fkey_stock :fkey_stock]]))
   (defmatview
     stock-search
     (-> (select* stock)
         (join category)
         (fields-only [:stock.name :stock]
                      [:category.name :category])))

   (defaltered
     (table :stock-rating
            (unique [:fkey_stock])
            (index :stock-rating-order-index [[:rating-with-priority :desc] [:fkey_stock :asc]]))
     (table :stock-search
            (fts-index [:stock :category :fkey_stock] :gin)
            (unique [:fkey_stock])))))

(defmigration stock-tags-and-retail-order-search
  (up
   (defmatview
     retail-order-search
     (-> (select* retail-order)
         (join user-order-item)
         (join user)
         (join stock (= :stock.id :user-order-item.fkey_stock))
         (join contact (= :user.id :contact.fkey_user))
         (fields-only [:user.real-name :user]
                      [:contact.value :contact]
                      [:stock.name :stock])))

   (defaltered
     (table :retail-order-search (fts-index [:user :contact :stock] :gin)))))

(defmigration users-full-text-search
  (up
   (defmatview
     users-search
     (-> (select* user)
         (join contact (= :user.id :contact.fkey_user))
         (join auth-profile (= :user.id :auth-profile.fkey_user))
         (fields-only [:user.real-name :user]
                      [:contact.value :contact]
                      [:auth-profile.login :login]
                      [:auth-profile.uuid :uuid])))

   (defaltered
     (table :users-search (fts-index [:user :contact :login :uuid] :gin)))))

(defmigration stock-arrival-status-migration
  (up
   (defmatview consumed-arrived-stocks (consumed-arrived-stocks-query)
     :entity-fields [:fkey_stock]
     :trigger-fields [[:stock-arrival-parameter :fkey_stock :fkey_stock]
                      [:user-order-item :fkey_stock :fkey_stock]
                      [:user-order-item-with-stock-parameter-value
                       :fkey_stock
                       :user-order-item-key
                       (-> (select* user-order-item)
                           (modifier "distinct")
                           (fields-only :fkey_stock))]
                      [:stock-arrival-parameter-with-stock-parameter-value
                       :fkey_stock
                       :stock-arrival-parameter-key
                       (-> (select* stock-arrival-parameter)
                           (modifier "distinct")
                           (fields-only :fkey_stock))]
                      [:additional-expense
                       :fkey_stock
                       :id
                       (-> (select* additional-expense)
                           (modifier "distinct")
                           (fields-only :user-order-item.fkey_stock)
                           (join user-order-item))]])
   (defaltered
     (table :consumed-arrived-stocks
            (index :left-stocks-index ["(arrived - consumed)"])))))

(defmigration stock-complex-status-mat-view-migration
  (up
   (defmatview stock-context-statuses (stock-context:stocks-with-context-statuses-query)
     :entity-fields [:id]
     :trigger-fields [[:stock-overwrite :id :fkey_stock]
                      [:stock-context :context :id]
                      [:stock :id :id]])

   (defaltered
     (table :stock-context-statuses
            (index :stock-context-statuses-context-status [:context :status])))))

(defmigration stock-accounting-variants
  (up
   (defmatview
    availabe-param-values
    (available-stock-values-query)
    :entity-fields [:fkey_stock]
    :trigger-fields [[:consumed-arrived-stocks :fkey_stock :fkey_stock]
                     [:stock :fkey_stock :id]
                     [:stock-parameter-value :fkey_stock :fkey_stock]])

   (defaltered
     (table :availabe-param-values
            (unique [:name :value :fkey_stock])))))


(defmigration closed-equals-quantity
              (up
                (exec-raw "UPDATE \"user-order-item\" SET closed = quantity WHERE closed > quantity;")))

(defmigration constraint-closed-equals-or-above-quantity
              (up
                (exec-raw  "ALTER TABLE \"user-order-item\" ADD CONSTRAINT closed_above_quantity CHECK (closed <= quantity);")))

(defmigration constraint-quantity-not-negative
              (up
                (exec-raw "ALTER TABLE \"user-order-item\" ADD CONSTRAINT quantity_not_negative CHECK (quantity >= 0)")))

(defmigration user-address-creation
              (up
                  (deftables
                    (table :user-address
                           (text :address)
                           (text :address-region)
                           (text :country)
                           (json :extra-info)
                           (many-to-one :user)))

                  (let [users (select user
                                      (fields-only :id :address :address-region :extra-info))]
                    (doseq [{:keys [id address address-region extra-info]} users]
                      (insert :user-address
                              (values
                                (conj {:fkey_user id
                                       :address address
                                       :address-region address-region}
                                      (if extra-info
                                        {:extra-info extra-info}
                                        {}))))))

              (exec-raw "ALTER TABLE \"user\" DROP COLUMN address;")
              (exec-raw "ALTER TABLE \"user\" DROP COLUMN \"address-region\";")
              (exec-raw "ALTER TABLE \"user\" DROP COLUMN \"extra-info\";")))

(defmigration redirect-table-addition
              (up
                (deftables
                  (table :redirect
                         (text :from :not-null)
                         (text :to :not-null)
                         (many-to-one :domain)))))

(defmigration site-page-not-mail-migrations
              (up
                (deftables
                  (table :site-page-not-mail
                         (text :mail :not-null)
                          ))))

(defmigration redirect-enabled-column
              (up
                (exec-raw "ALTER TABLE redirect ADD COLUMN enabled BOOLEAN DEFAULT FALSE")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Create JSON cast
(exec-raw "drop cast if exists (varchar as json);")
(exec-raw "create cast (varchar as json) without function as implicit;")
