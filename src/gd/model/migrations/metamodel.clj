(ns gd.model.migrations.metamodel
  (:use gd.utils.db)
  (:use gd.utils.web)
  (:use gd.utils.common)
  (:use clojure.algo.generic.functor)
  (:use gd.model.model)
  (:use clojure.java.io)
  (:refer-clojure :exclude [alter drop bigint boolean char double float time complement])
  (:use [korma.core :exclude [table]])
  (:use [lobos.migration :only (defmigration)])
  (:use lobos.schema))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Migrations of metamodel and categories tree

(defmigration initial
  (up
   (save-categories-tree [["Одежда" "clothes"
                           [["Мужская" "male"]
                            ["Женская" "female"]
                            ["Унисекс" "unisex"]
                            ["Для беременных" "pregnant"]
                            ["Нижнее белье" "underwear"]
                            ["Чулки, носки, колготки" "socks"]]]

                          ["Обувь" "shoes"
                           [["Мужская" "male"]
                            ["Женская" "female"]
                            ["Унисекс" "unisex"]]]

                          ["Товары для детей" "kids"
                           [["Одежда и обувь для детей 0-12 мес" "less1"]
                            ["Одежда и обувь для детей 1-3 года" "1to3"]
                            ["Одежда и обувь для детей 3-6 лет" "3to6"]
                            ["Одежда и обувь для детей старше 6 лет" "older6"]
                            ["Игрушки" "toys"]
                            ["Коляски, автокресла" "strollers"]
                            ["Товары для творчества" "creativity"]
                            ["Украшения, аксессуары" "trinketry"]
                            ["Товары по уходу за ребенком" "care"]]]

                          ["Красота и здоровье" "beauty"
                           [["Косметика" "cosmetics"]
                            ["Парфюмерия" "perfumery"]
                            ["Гигиена" "hygiene"]]]

                          ["Украшения и аксессуары" "accessories"
                           [["Зонты" "umbrellas"]
                            ["Аксессуары" "accessory"]
                            ["Бижутерия" "bijouterie"]
                            ["Сумки" "bags"]
                            ["Кошельки" "wallets"]
                            ["Ремни" "belts"]
                            ["Часы" "watches"]
                            ["Оптика" "optics"]]]

                          ["Товары для дома" "goods"
                           [["Бытовая химия" "household"]
                            ["Посуда" "dishes"]
                            ["Сад, огород" "garden"]
                            ["Рукоделие" "handmade"]]]

                          ["Разное" "other"]

                          ["Иностранные закупки" "foreign"]])))

(defmigration main-organizer
  (up
   (user:create-user-internal
    {:real-name "Виктория"
     :real-activities "Организатор"
     :role ["user" "organizer"]
     :auth-profile [{:type :internal :login "root" :password "Gh$%hs76SFBj"}]
     :contact {:phone "+380990800049" :email "koval.vika@ukr.net"}})))

(defmigration invites
  (up
   (invite:initial-generation 10000)))

(defmigration measurements-model-change
  (up
   (let [stocks (select stock (fields-only :id :description))
         counter (atom 0)]
     (doseq [{:keys [id description]} stocks]
       (if (and (map? description) (not (:measurements description)))
         (let [measurements (dissoc description :other :fabric)
               {:keys [other fabric]} description
               measurements (fmap (fn[{:keys [name value]}] (vec (map (fn[n v] {:name n :value v})
                                                                      (maybe-seq name)
                                                                      (maybe-seq value))))
                                  measurements)]
           (prn id (swap! counter inc) (count stocks) measurements)
           (update stock
             (set-fields {:description {:measurements measurements
                                        :other other
                                        :fabric fabric}})
             (where {:id id}))))))))

(defn filter-by-existing[value]
  (> (select-count dictionary-param
                   (where {:fkey_supplier gd.model.context/*current-supplier*
                           :value value})) 0))

(defmigration dictionary-strict-model
  (up
   (binding [gd.model.context/*current-supplier* 1]
     (retail:fill-dictionary-from-params)
     (let [dictionary (:dictionary (select-single supplier (where {:id 1})))
           measurements (distinct (mapcat :measurement (vals (:brand dictionary))))]
       (dictionary:update-dictionary
        (->
         {:conditions {"size"
                       (remove nil?
                               (map (fn[[_ params]]
                                      (when (filter-by-existing (:name params))
                                        {:condition {"brand" (:name params)}
                                         :values (filter filter-by-existing (:size params))}))
                                    (:brand dictionary)))}
          :measurements measurements}
         (merge dictionary)
         (dissoc :brand)))))))

(defmigration description-model-change
              (up
                (transaction
                (loop [x 0]
                  (let [stocks (select stock (fields-only :id :description) (order :id) (limit (+ x 200)) (offset x))]
                    (prn "offset : " x)
                    (when-not (empty? stocks)
                      (doseq [{:keys [id description]} stocks]
                        (if (and (map? description) (not (description :properties)))
                          (update stock (set-fields {:description (-> (conj description {:properties []})
                                                                      (assoc-in [:properties (count (description :properties))]
                                                                                (if (description :fabric) {:name "fabric" :value (description :fabric)} {}))
                                                                      (dissoc  :fabric))})
                                  (where (= :id id)))))
                      (recur (+ x 200))))))))

(defmigration categories-to-lower-case
              (up
                (let [category (select category (fields-only :id :short-name :key))]
                  (doseq [{:keys [id short-name key]} category]
                    (update :category (set-fields {:short-name (.toLowerCase short-name) :key (.toLowerCase key)}) (where (= :id id)))))))

(defmigration site-page-to-lower-case
              (up
                (let [sp (select site-page (fields-only :id :url))]
                  (doseq [{:keys [id url]} sp]
                    (update :site-page (set-fields {:url (.toLowerCase url)}) (where (= :id id)))))))

(defn disable-triggers[]
  (exec-raw "ALTER TABLE stock DISABLE TRIGGER ALL;"))

(defn enable-triggers[]
  (exec-raw "ALTER TABLE stock ENABLE TRIGGER USER;"))

(defmigration fabric-translation
  (up
   (transaction
    (disable-triggers)
    (loop [x 0]
      (let [stocks (select stock (fields-only :id :description) (where {:description [like "%fabric%"]}) (order :id) (limit (+ x 200)) (offset x))]
        (prn "offset : " x)
        (when-not (empty? stocks)
          (doseq [{:keys [id description]} stocks]
            (when (and (map? description) (some #(= (:name %) "fabric") (:properties description)))
              (update stock (set-fields {:description (->>
                                                       (into [] (map #(if (= (:name %) "fabric") (assoc-in % [:name] "Состав") %) (:properties description)))
                                                       (assoc-in description [:properties]))})
                      (where (= :id id)))))
          (recur (+ x 200)))))
    (enable-triggers))))

(defmigration measurements-nil-values-fix
  (up
   (disable-triggers)
   (loop [x 0]
     (let [stocks (select stock (fields-only :id :description) (order :id) (limit (+ x 100)) (offset x))]
       (prn "offset : " x)
       (when-not (empty? stocks)
         (doseq [{:keys [id description]} stocks]
           (when (seq (:measurements description))
             (let [measurements (:measurements description)
                   valid-measurements (apply merge (map! (fn [key] {key (into [] (filter #(and (and (not= "null" (:name %)) (or (number? (:name %))  (seq (:name %))))
                                                                                               (and (not= "null" (:value %)) (or (number? (:value %)) (seq (:value %)))))
                                                                                         (measurements key)))}) (keys measurements)))
                   valid-description (assoc-in description [:measurements] valid-measurements)]

               (update stock (set-fields {:description valid-description}) (where (= :id id))))))
         (recur (+ x 100)))))
   (enable-triggers)))

(defmigration seo-categories
              (up
                (transaction
                (update category (set-fields {:short-name "golfi_vodolazki"
                                              :key "stocks-female-outerwear-golfi_vodolazki"}) (where {:short-name "golfi_vodolazkii"})))))

(defmigration doublicate-category-detyam-fix
              (up
                (transaction
                  (update category (set-fields {:short-name"sportivnie_shtani"
                                                :key "stocks-detyam-sportivnie_shtani"}) (where {:id 476})))))

(defmigration prices-getting-higher
              (up
                (disable-triggers)
                (transaction
                  (update :stock-variant (set-fields {:price (raw "price * 1.05")}))
                  (update :stock (set-fields {:price (raw "price * 1.05")})))
                  (enable-triggers)))

(defmigration redirects-for-bono
              (up
                (let [redirects [["/stocks/male/wear/sweater/" "/stocks/male/wear/"]
                                 ["/stocks/male/wear/sportivnie_kostumi/" "/stocks/male/wear/"]
                                 ["/stocks/male/wear/caps/" "/stocks/male/wear/"]
                                 ["/stocks/female/outerwear/djempera_tyniki_jiletki/" "/stocks/female/outerwear/"]
                                 ["/stocks/female/underwear/kombidress/" "/stocks/female/underwear/"]
                                 ["/stocks/female/underwear/shorts/" "/stocks/female/underwear/"]
                                 ["/stocks/female/homewear/kits/" "/stocks/female/outerwear/komplekti/"]
                                 ["/stocks/female/homewear/platya_i_sarafanu/" "/stocks/female/outerwear/platja-dlja-progulok/"]
                                 ["/stocks/detyam/kits_clothing/" "/stocks/detyam/underwear/pizhami/"]
                                 ["/stocks/detyam/kombinezoni/" "/stocks/detyam/outerwear/"]
                                 ["/stocks/detyam/sportivnui_kostum/" "/stocks/detyam/outerwear/sportivnui-kostum/"]
                                 ["/stocks/detyam/baby_socks/" "/stocks/detyam/underwear/"]
                                 ["/stocks/detyam/dlya_devochek/" "/stocks/detyam/outerwear/platja-sarafani/"]
                                 ["/stocks/detyam/sportivnie_shtani/" "/stocks/detyam/outerwear/futbolki-majki/"]
                                 ["/stocks/detyam/svitera_koftu/" "/stocks/detyam/outerwear/kofti/"]
                                 ["/stocks/detyam/kolgotu/" "/stocks/detyam/underwear/kolgoti/"]]]
                  (doseq [[from to] redirects]
                    (insert :redirect
                            (values {:fkey_domain 1
                                     :from from
                                     :to to}))))))

(defmigration categories-urls-fix
              (up
                (let [categories [{:id 626
                                   :short-name "besshovnoe-bele"
                                   :key "stocks-female-underwear-besshovnoe-bele"
                                   :from "/stocks/female/underwear/besshovnoe-bel'e/"
                                   :to "/stocks/female/underwear/besshovnoe-bele/"}
                                  {:id 592
                                   :short-name "teplye-golfy"
                                   :key "stocks-female-outerwear-golfi_vodolazki-teplye-golfy"
                                   :from "/stocks/female/outerwear/golfi_vodolazki/teplye golfy /"
                                   :to "/stocks/female/outerwear/golfi_vodolazki/teplye-golfy/"}
                                  {:id 593
                                   :short-name "tonkie-golfy"
                                   :key "stocks-female-outerwear-golfi_vodolazki-tonkie-golfy"
                                   :from "/stocks/female/outerwear/golfi_vodolazki/tonkie golfy /"
                                   :to "/stocks/female/outerwear/golfi_vodolazki/tonkie-golfy/"}
                                  {:id 614
                                   :short-name "platja"
                                   :key "stocks-female-outerwear-platja"
                                   :from "/stocks/female/outerwear/plat'ja/"
                                   :to "/stocks/female/outerwear/platja/"}
                                  {:id 621
                                   :short-name "shorty-bridzhi"
                                   :key "stocks-female-outerwear-shorty-bridzhi"
                                   :from "/stocks/female/outerwear/shorty,-bridzhi/"
                                   :to "/stocks/female/outerwear/shorty-bridzhi/"}
                                  {:id 623
                                   :short-name "kapronovye-noski-golfy"
                                   :key "stocks-female-underwear-kapronovye-noski-golfy"
                                   :from "/stocks/female/underwear/kapronovye-noski-gol'fy/"
                                   :to "/stocks/female/underwear/kapronovye-noski-golfy/"}
                                  {:id 628
                                   :short-name "majki-futbolki-belevye"
                                   :key "stocks-female-underwear-majki-futbolki-belevye"
                                   :from "/stocks/female/underwear/majki-futbolki-bel'evye/"
                                   :to "/stocks/female/underwear/majki-futbolki-belevye/"}]]
                  (doseq [{:keys [key short-name from to id]} categories]
                    (transaction
                      (update category
                              (set-fields {:key key
                                           :short-name short-name})
                              (where {:id id}))
                      (insert :redirect
                              (values {:fkey_domain 1
                                       :from from
                                       :to to
                                       :enabled false})))))))

(defmigration phonenumbers-to-auth-migrates
              (up
                (transaction
                  (loop [x 0]
                    (prn (str "phonenumbers to auth offset:" x))
                    (let [users (select user (with contact) (with auth-profile)
                                       (order :id)
                                       (offset x)
                                       (limit (+ x 200)))]
                    (when-not (empty? users)
                    (doseq [i users]
                      (doseq [contactdata (:contact-raw i)]
                        (if (= (:type contactdata) :phone)
                          (if (= (count(select auth-profile (where {:login (gd.utils.sms/normalize-number (:value contactdata))}))) 0)
                            (try
                              (insert auth-profile
                                      (values {:fkey_user (:fkey_user ((:auth-profile i) 0))
                                               :login     (gd.utils.sms/normalize-number (:value contactdata))
                                               :password  (:password ((:auth-profile i) 0))
                                               :type      :internal}))
                              (prn "added" (gd.utils.sms/normalize-number (:value contactdata)))
                              (catch Throwable e
                                (prn (serialize e))))))))
                    (recur (+ x 200))))))))
