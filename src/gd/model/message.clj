(in-ns 'gd.model.model)

;; Post-processing hooks
(declare messages:bbcode-to-html)

;; All posible authors of message
(declare user)
(declare auth-profile)
(declare forum-theme)
(declare supplier)

(defalias user-recipient user)
(defalias forum-theme-recipient forum-theme)
(defalias supplier-recipient supplier)

(declare message)

(def contacts [])

(defentity message
  (transform #'messages:bbcode-to-html)

  ;; all possible authors
  (belongs-to user {:fk :author})

  ;; ;; all possible recipients
  (belongs-to user-recipient {:fk :recipient :entity :recipient})
  (belongs-to forum-theme-recipient {:fk :recipient :entity :recipient})
  (belongs-to supplier-recipient {:fk :recipient :entity :recipient})

  (enum message-type [:type] :private :discussion :forum :suggestion :error :question :supplier-review)
  (belongs-to message {:fk :fkey_message :entity :reply-to}))

(def bb-code-processor (.create (BBProcessorFactory/getInstance)
                                (org.apache.commons.io.IOUtils/toInputStream (generate-bbcode-config))))

(defn bbcode-to-html[text]
  (when text
    (-> (.process bb-code-processor text)
        (.replaceAll "\\[\\[__images-parent-host__\\]\\]"
                     (context-info "" :mail :images-server))
        (.replaceAll "&lt;" "<")
        (.replaceAll "&gt;" ">")
        (.replaceAll "&quot;" "\"")
        (.replaceAll "&amp;nbsp;" "&nbsp;"))))

(defn- messages:bbcode-to-html[msg]
  (update-in msg [:text] bbcode-to-html))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Send and update

(defn process-text[text]
  (prn text)
  (-> text
      unescape-html ;unescape HTML entities, this is completely safe, because
                                        ;content of message is handled by BBCODE processor, also
                                        ;this prevents double escaping
      (.replaceAll "&ndash;" "-")
      (.replaceAll "&ldquo;" "\"")
      (.replaceAll "&rdquo;" "\"")
      #_(.replaceAll "<[^>]*>" "")))

;; TODO : if we will add attachement, we should check img src, because it also can
;; contain script
(defn message:send
  [{:keys [removed sent-time reply-to] :as msg} from to]
  (let [message-inserted
        (insert message (values
                         (-> msg
                             (update-in [:text] process-text)
                             (dissoc :reply-to)
                             (assoc :opened false
                                    :removed (if (contains? msg :removed) removed false)
                                    :fkey_message reply-to
                                    :sent-time (or sent-time (sql-now-timestamp))
                                    :author (:id from)
                                    :recipient (:id to)))))]
    (messages:bbcode-to-html message-inserted)))

(defn message:update[id text]
  (update message (set-fields {:text (process-text text)}) (where {:id id})))

;; NOTE : about 'author' column in the message table. It is reference to the
;; parent entity of :sent message box for given entity. For example : if user1
;; send message to user2, them message will be added to (:sent message box of
;; user1) and (:inbox message box of user2), and column 'author' will have id of
;; user1
;; TODO : maybe in future we should use different column for each possible type
;; of author, for the sake of performance(foreing keys can't reference to
;; multiple tables)
(defn- message:fetch-author
  "Modify query for fetching author entity, according to message type.
:private - author is user
other - author is deal"
  [query message-type-in]
  (case message-type-in
    (:private :discussion :forum :suggestion :error :question :supplier-review)
    (-> query (with user (with auth-profile)))))

(defn- message:fetch-recipient
  "Modify query for fetching recipient entity, according to message type.
:private,:notifiction,:new - recipient is user
:forum - recipient is forum theme"
  [query message-type-in]
  (case message-type-in
    (:forum)
    (-> query (with-alias forum-theme-recipient))

    (:private)
    (-> query (with-alias user-recipient))

    (:supplier-review)
    (-> query (with-alias supplier-recipient))

    (:suggestion :error :question)
    query))

(defn- get-message-by-id[message-id message-type-in]
  (select-single message
                 (message:fetch-author message-type-in)
                 (message:fetch-recipient message-type-in)
                 (where
                  {:id message-id})))

(defn- message:request-query[entity box-type message-type-in show-removed]
  (->
   (select* message)
   (where
    {:type (message-type message-type-in)
     (case box-type :sent :author :inbox :recipient) (:id entity)})
   (where-if (not show-removed) {:removed false})))

(defn message:messages
  "Get all messages from message box of specified type.

Poscondition: check that for given type of message :author is correctly
fecthed. Private - user, notification, news, discussion - group deal."
  [entity box-type message-type-in & [{:keys [reply-to limit-num offset-num fetch-reply
                                              ordering show-removed]}]]
  {:post [(or *count-query*
              (every? #(case message-type-in
                         (:private :discussion :forum :supplier-review) (map? (:user %))
                         (:suggestion :error :question) true) %))]}
  (select (message:request-query entity box-type message-type-in show-removed)
    (where-if reply-to {:fkey_message [in reply-to]})
    (message:fetch-author message-type-in)
    (message:fetch-recipient message-type-in)
    (modify-if fetch-reply (with message (with user)))
    (order :sent-time (or ordering :desc))
    (limit limit-num)
    (offset offset-num)
    (count-query)))

(defn message:messages-tree
  "Get all messages from message box of specified type, and return them in form of tree
representation."
  [entity box-type message-type-in]
  (let [all-messages
        (message:messages entity box-type message-type-in)

        get-all-for-level
        (fn[id] (filter #(= (:fkey_message %) id) all-messages))

        build-tree-fn
        (fn build-tree[current-level-entity]
          (let [current-level-childs
                (get-all-for-level (:id current-level-entity))

                current-level-childs-processed
                (map build-tree current-level-childs)]
            (assoc current-level-entity :childs current-level-childs-processed)))]
    (:childs                            ; unwrap childs from first level(root of result is always
                                        ; empty node with only list of childs)
     (doall (build-tree-fn nil)))))

;; TODO : add check that message have :private type
(defn message:reply-to-private[from message-id text]
  (let [message-author
        (:user (select-single message (with user) (where {:id message-id})))]
    (message:send {:text text :type :private :reply-to message-id} from message-author)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Read/unread marks
(defn message:mark-as-read[id]
  (let [message-recipient (get-in (get-message-by-id id :private) [:recipient :id])]
    (if (= message-recipient (:id (sec/logged)))
      (update message (set-fields {:opened true}) (where {:id id}))
      (violation "Messages : attempt to mark as read"))))

(defn message:number-of-new-messages[user]
  (:cnt
   (select-single message
                  (aggregate (count :*) :cnt)
                  (where {:recipient (:id user)
                          :message.opened false
                          :message.removed false
                          :message.type (message-type :private)}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Deletion
(defn message:remove[id]
  (update message (set-fields {:removed true :opened true}) (where {:id id})))

(defn message:restore[id]
  (update message (set-fields {:removed false}) (where {:id id})))
