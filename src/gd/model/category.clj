(in-ns 'gd.model.model)

(comment "Common declarations and functions that don't depend on the view type")

(declare category-annotation)
(declare category)
(declare redirects)
(declare category:fix-category-parent)
(declare replace:key)
(declare replace:path-to-old)
(declare extract)

(defentity category
  (json-data :seo)
  (belongs-to category {:fk :fkey_category :entity :parent})
  (transform #'category:fix-category-parent))

(declare category:get-by-key)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Internal API(respect filtering)

(comment "Categories tree can be filtered by top level. ")

(defn- category:get-current-parent-category[]
  (when-let [cat (context-info-by-key :category)]
    (category:get-by-key cat)))

(defn- category:get-current-parent[]
  (:id (category:get-current-parent-category)))

(defn- category:fix-category-parent
  "If parent category is current context parent - make it null"
  [{:keys [parent] :as cat}]
  (assoc cat :parent (when-not (= (:key parent) (context-info-by-key :category)) parent)))

(defn- build-categories-path[short-names]
  (str "/" (strings/join "/" short-names) "/"))

(defn- category:build-tree-path
  "Build tree from given categories list"
  [all-categories parent-id & [path]]
  (let [parent-id (or parent-id (category:get-current-parent))
        path (or path [])
        categories (filter #(= (:fkey_category %) parent-id) all-categories)]
    (reduce merge
            (map (fn[{:keys [name short-name id] :as category}]
                   (let [path (conj path short-name)]
                     {short-name (assoc category
                                   :path (build-categories-path path)
                                   :child (category:build-tree-path all-categories id path)
                                   :level (count path))}))
                 categories))))

(defn category:whole-model
  "Just select all categories. Respect current context settings :category key from *supplier-info*"
  []
  (let [parent-category
        (category:get-current-parent)

        parent-category-key
        (context-info-by-key :category)

        all-categories-from-parent
        (select category
          (with category)
          (where-if parent-category {:key [like (str parent-category-key "-%")]}))]
    (map
     (fn[{:keys [fkey_category] :as cat}]
       (assoc cat :top-category (= fkey_category parent-category)))
     all-categories-from-parent)))

(defn category:parents[root-id]
  (let [category-by-id (fn[category-id] (find-by-val :id category-id (category:whole-model)))]
    (loop [current-category root-id
           path nil
           result nil]
      (if (nil? current-category)
        (let [path (remove nil? path)
              result (remove nil? result)
              result-path (map-indexed (fn[i v] (build-categories-path (take (inc i) path))) path)]
          (reverse (map (fn[cat path] (assoc cat :path path)) result result-path)))
        (let [{:keys [fkey_category short-name] :as cat} (category-by-id current-category)]
          (recur fkey_category (cons short-name path) (cons cat result)))))))

(defn- category:path "Returns path to given category"
  [category-id]
  (build-categories-path (reverse (map :short-name (category:parents category-id)))))

(defn- category:descendant-categories
  "Returns all descendants of given categories without filtering by stock"
  [categories]
  (loop [current-childs categories
         descendants categories]
    (let [childs-of-current-level
          (filter
           #(some (fn[parent](= (:fkey_category %) (:id parent))) current-childs)
           (category:whole-model))]
      (if (empty? childs-of-current-level)
        descendants
        (recur childs-of-current-level (concat descendants childs-of-current-level))))))

(defn- category:to-filter-by-parent[parent-id]
  (map
   :id
   (category:descendant-categories
    (if (nil? parent-id)
      (filter :top-category (category:whole-model))
      (list {:id parent-id})))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; General API for getting categories

(defn category:get-by-key[key & {:keys [relative]}]
  (select-single category (with category)
                 (where {:key (if relative
                                (str (context-info-by-key :category) "-" key)
                                key)})))

(defn category:get-by-ids[ids]
  (select category (with category) (where {:id [in ids]})))

(defn category:get-by-id[id]
  (let [cat (select-single category (with category) (where {:id id}))]
    (assoc cat :path (category:path (:id cat)))))

(defn category:tree
  "Build global tree with ALL categories(usefull for admin page)"
  []
  (category:build-tree-path (category:whole-model) nil))

(declare retail:annotate-categories)
(declare annotate-map-from-sql)

(defn category:tree-with-annotations[]
  (let [annotate-with-delete-availability
        (fn[categories]
          (let [category-to-stock-num
                (fmap :count
                      (group-by-single
                       :fkey_category
                       (select :stock
                         (fields :fkey_category)
                         (aggregate (count :*) :count :fkey_category)
                         (dry-transforms)
                         (where {:fkey_category [in (map :id categories)]}))))]
            (annotate-map-from-sql categories category-to-stock-num :stocks)))]
    (-> (category:whole-model)
        retail:annotate-categories
        annotate-with-delete-availability
        (category:build-tree-path nil))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Save/update API

(defn category:save[parent-id name short-name]
  (transaction
    (let [parent-id (or parent-id (category:get-current-parent))
          {:keys [key id]} (select-single category (where {:id parent-id}))]
      (insert category
        (values
         {:name name
          :short-name short-name
          :fkey_category id
          :key (str key "-" short-name)})))))

(defn category:delete[id]
  (transaction
    (delete category-annotation (where {:fkey_category id}))
    (delete category (where {:id id}))))

(defn category:update[id name]
  (update category (set-fields {:name name}) (where {:id id})))

(defn category:update-short[id short-name short-name-original]
  (transaction
    (update category
          (set-fields {:short-name short-name :key (replace:key id short-name short-name-original)})
          (where {:id id }))

    (extract id short-name short-name-original)))

(defn extract [parent-id short-name short-name-original]
  (let [child-ids (map :id (select category (where {:fkey_category parent-id})))]
    (doseq [child-id child-ids]
           (update category
                   (set-fields { :key (replace:key child-id short-name short-name-original)})
                   (where {:id child-id }))
           (insert redirects
                   (values {:fkey_domain *current-supplier*
                            :from (replace:path-to-old
                                    (category:path child-id)
                                    short-name-original
                                    short-name)
                            :to (category:path child-id) :enabled true}))
           (extract child-id short-name short-name-original))))



(defn replace:key [id replace-with replace-value]
  (clojure.string/replace (first (->>
                                   (select category
                                           (fields-only :key)
                                           (where {:id id}))
                                   (map :key )))
                          replace-value
                          replace-with))

(defn replace:path-to-old [path replace-with replace-value]
  (clojure.string/replace path replace-value replace-with))

(defn category:update-seo[id seo]
  (update category (set-fields {:seo seo}) (where {:id id})))
