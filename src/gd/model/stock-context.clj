(in-ns 'gd.model.model)

(comment "Stock can have different parameters in different contexts(retail
website, wholesale website, organizers subdomains with custom prices and
visibility).

Contexts - organizer subdomain, retail, wholesale website.

All properties that can be overwritten stored in stock-overwrite entity.
Specific entity fetched based on current context variable *stock-context* and
merged into result entity.

Context can be defined as list [id1,id2]. Then price and status will not be
merged, and defined as delayed exceptions(every attempt to access price or
status - will fire exception). ")

(defentity stock-context
  (enum stock-context-type [:type] :retail :retail-sale :wholesale :wholesale-sale :organizer)
  (belongs-to supplier {:fk :fkey_supplier}))

(defentity stock-overwrite
  (belongs-to stock-context {:fk :fkey_stock-context})
  (belongs-to stock {:fk :fkey_stock})
  (use-enum stock-status [:status]))

(extend-entity stock
  (default-fetch stock-overwrite)
  (has-many stock-overwrite {:fk :fkey_stock}))

(def ^:dynamic *context-types-to-id* nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Resolve id->type and type->id functions

(defn- stock-context:resolve-type[type]
  (let [condition {:type (stock-context-type (keyword type))
                   :fkey_supplier *current-supplier*}]
    (cached :context-resolve :stock-context condition
            (:id (select-single-null stock-context (where condition))))))

(defn stock-context:resolve-or-create-type[type]
  (let [type (keyword type)]
    (or (stock-context:resolve-type type)
        (:id (insert stock-context (values {:fkey_supplier *current-supplier*
                                            :type type}))))))

(defn- stock-context:resolve-id-to-type[context-id]
  (or (first (find-by-val second context-id *context-types-to-id*))
      (:type (select-single stock-context (where {:id context-id})))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Middleware for ring that resolve types to ids

(defn- resolve-context-types[handler request]
  (let [effective-contexts (if (context-info-by-key :context-switch)
                             (or (maybe-seq (:context-override @*current-session*)) *stock-context*)
                             *stock-context*)]
    (binding [*context-types* effective-contexts
              *stock-context* (map! stock-context:resolve-or-create-type effective-contexts)]
      (binding [*context-types-to-id* (zipmap *context-types* *stock-context*)]
        (handler request)))))

(defn stock-context:resolve-types[handler]
  (fn[request]
    (resolve-context-types handler request)))

(defn stock-context:set-override[context]
  (session/put! :context-override context)
  nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils for wrapping code into selected context type
(defmacro wrap-context[context-type & body]
  `(binding [*stock-context* ~context-type]
     ((stock-context:resolve-types (fn[_#] ~@body)) {})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Filtering utils

(defn- filter-by-context[query & [contexts]]
  (let [contexts (or *stock-context* contexts)]
    (where-if query (seq contexts) {:contexts [overlaps contexts]})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils functions

(defmulti stock-context:multi-context-process (fn[context] (set *context-types*)))

(defn- make-fail[]
  (delay (throwf "Attempt to access param in multi-context mode")))

(def default-context-for-empty {:retail :wholesale
                                :retail-sale :retail
                                :wholesale-sale :wholesale})

(defn- stock-context:prepare-context-info-for-stock
  "Build map 'context type' -> 'context info'.

For some contexts like :organizer can exists multiple instanses for this
keyword, that's why we must pass list of context-ids that will filter stored
contexts to only needed set of contexts.

Can create default contexts by copying info from already existing
contexts(working for :wholesale and :retail contexts) "
  [{:keys [stock-overwrite price status]} context-ids]
  (let [filtered-contexts
        (->>
         (map stock-context:resolve-type (vals default-context-for-empty))
         (concat context-ids)
         (remove nil?)
         set)

        persistent-overwrite
        (->>
         stock-overwrite
         (filter (comp filtered-contexts :fkey_stock-context))
         (group-by-single (comp stock-context:resolve-id-to-type :fkey_stock-context)))]
    (->>
     (set (concat *context-types* (keys persistent-overwrite)))
     (mapcat
      (fn[type]
        [type (merge
               ;; default context. Copy current status and price, but for sale contexts -
               ;; default status is :invisible
               {:price price
                :fkey_stock-context (stock-context:resolve-or-create-type type)
                :status (case type
                          (:wholesale-sale :retail-sale) :invisible
                          status)}

               ;; specific default context. For retail we can copy all info from wholesale context,
               ;; if it's empty
               (select-keys
                (persistent-overwrite (default-context-for-empty type))
                [:price])

               ;; stored context
               (select-keys
                (type persistent-overwrite)
                [:price :status]))]))
     (apply hash-map))))

(defn- stock-context:filter-stock-variants
  "Constraint list of variants to only contexts that passed(usefull for work on
client side, on the admin side we will have list of variants, but on client -
only one variant).

Also if variant for some contexts don't exists(like retail) we can copy info
from wholesale contexts."
  [{:keys [stock-variant] :as stock} contexts]
  (let [contexts (if contexts (maybe-seq contexts) [nil])]
    (assoc stock :stock-variant
           (fmap (fn[variants]
                   (let [variants (map
                                   (fn[context]
                                     (or (find-by-val :context context variants)
                                         (when-let
                                             [default-context
                                              (or
                                               (find-by-val :context (default-context-for-empty context) variants)
                                               (find-by-val :context nil variants))]
                                           (assoc default-context
                                             :context context
                                             :fkey_stock-context (stock-context:resolve-type context)))))
                                   contexts)]
                     (if (= 1 (count variants))
                       (first variants)
                       variants)))
                 stock-variant))))

;;;;;;;;;;;;;;;;;;;;
;; Process stocks and order-items - they are depends on the context

(defn- stock-context:process-stock[{:keys [stock-overwrite price status stock-parameter-value] :as stock}
                                   & {:keys [context from-processor]}]
  (let [context (or context *context-types*)
        stock-context (stock-context:prepare-context-info-for-stock
                       stock (map! stock-context:resolve-type (set (concat context *context-types*))))]
    (stock:parameters-for-choosing
     (cond
       (and (not price) (not status))
       stock

       ;; context - is integer, we extract specified context and merge it into stock
       (= 1 (count context))
       (let [[context] context
             overwrite (get stock-context context)]
         (-> stock
             (merge overwrite {:context stock-context :selected-context context})
             (stock-context:filter-stock-variants context)))

       ;; context - is list of integers, we mark price and status as not-readable fields
       (seq context)
       (let [resolved-context (stock-context:multi-context-process stock-context)]
         (if (or (= *context-types* resolved-context) from-processor)
           (-> stock
               (merge {:price (make-fail)
                       :status (make-fail)
                       :context stock-context})
               (stock-context:filter-stock-variants *context-types*))
           (stock-context:process-stock
            stock
            :from-processor true
            :context resolved-context)))

       ;; keep stock as is
       true
       (-> stock
           (merge {:context stock-context})
           (stock-context:filter-stock-variants nil))))))

(defn- stock-context:check-order-item
  "Update fetched stock according to context that was used on order creation. Also update stock parameter values."
  [{:keys [fkey_stock-context stock stock-parameter-value] :as item}]
  (if fkey_stock-context
    (let [stock-context (stock-context:prepare-context-info-for-stock stock [fkey_stock-context])
          overwrite (get stock-context (stock-context:resolve-id-to-type fkey_stock-context))]
      (assoc item :stock
             (merge stock overwrite {:selected-context (stock-context:resolve-id-to-type fkey_stock-context)})))
    item))

;;;;;;;;;;;;;;;;;;;;
;; Process main stock properties(example : we want to change name of stock to
;; 'category' + id for seo purpose) then you should set 'stock process' function
;; in context.clj

(defn- stock-context:process-main-properties [stock]
  ((context-info identity :process-main-properties) stock))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Public API

(defn stock-context:save-context-info[stock-id context]
  (transaction
    (doseq [[type info] context]
      (let [context {:fkey_stock-context (stock-context:resolve-or-create-type type)
                     :fkey_stock stock-id}]
        (upset stock-overwrite (merge info context) context)))))

(defn stock-context:all-contexts[]
  (select stock-context
    (with supplier)
    (with user)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Specific context processing functions. Applied to stock in case of multi context environement.
;; Can describe relation between some contexts(for example : wholesale and wholesale-sale - we should
;; copy info from wholesale in stock is not in sale status, otherwise - show sale info).

(defmethod stock-context:multi-context-process :default [context]
  *context-types*)

(defn- sale-initialized
  "Sale context is initialized if status is set to :visible and result price might not be zero(actually
this depends on the avaliability, but here i check stock price and price changes to make assumption)"
  [{:keys [status price] :as context}]
  (and (= status :visible) (not= 0 price)))

(defmethod stock-context:multi-context-process #{:retail :retail-sale} [context]
  [(if (sale-initialized (:retail-sale context)) :retail-sale :retail)])

(defmethod stock-context:multi-context-process #{:wholesale-sale :wholesale} [context]
  [(if (sale-initialized (:wholesale-sale context)) :wholesale-sale :wholesale)])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stock list by context status filtering function

(defn stock-context:stocks-with-context-statuses-query[]
  (let [empty-context-status
        (str
         "case "
         (strings/join
          " "
          (map (fn[type]
                 (format "when type=%s then %s"
                         (stock-context-type type)
                         (case type
                           (:wholesale-sale :retail-sale) (stock-status :invisible)
                           "stock.status")))
               (values-stock-context-type)))
         " end")]
    (-> (select* :stock)
        (fields [:stock.id :id]
                [:stock-context.type :type]
                [:stock-context.id :context]
                [(sqlfn :coalesce :stock-overwrite.status (raw empty-context-status)) :status])
        (join :left :stock-context true)
        (join :left :stock-overwrite
              {:stock.id :stock-overwrite.fkey_stock
               :stock-overwrite.fkey_stock-context :stock-context.id}))))

(defn- stock-context:stocks-with-context-statuses[]
  :stock-context-statuses)

(defn- stock-context:context-status-general-filter
  "Filter stock by their composite context status.

Example status : {:context-status {:wholesale-sale :visible :wholesale :invisible}}

Type can be keywords :and or :or, and that means how filtering is applied(all contexts must match, or any context
must match).

If in context defined 'context-filter' they will be used by default for filtering.
"
  [query context-status type]
  (let [context-status
        (or context-status (reduce merge (map (fn[type] {type :visible})
                                              (context-info *context-types* :context-filter))))

        filters
        (binding [*exec-mode* nil]
          (map!
           (fn[[context status]]
             {:context (stock-context:resolve-or-create-type context)
              :status (stock-status status)})
           context-status))

        and-or-filter
        (fn[query]
          (if (and (= type :and) (> (count context-status) 1))
            (having query {(raw "count(*)") (count context-status)})
            query))

        ids-query
        (subselect
         [(subselect (stock-context:stocks-with-context-statuses)) :overwrite]
         (fields-only :id)
         (group :id)
         (where (apply or filters))
         and-or-filter)]
    (if *count-query*
      ;; because top level query already have 'distinct' modifier, we can just join table
      (join query :inner [ids-query :overwrite] (= :overwrite.id :stock.id))
      (where query {:stock.id [in ids-query]}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Mat view debug

(defonce stock-context-status-mat-view-debug
  (make-mat-view-debugger
   :stock-context-status
   (fn[] (select (stock-context:stocks-with-context-statuses-query)))
   (fn[] (select :stock-context-statuses))))
