(in-ns 'gd.model.model)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Database tracking cache

(defn- collect-entities
  "Collect all transitive dependecies for root entity. All changes in this entities will
be treated as invalidation event.
stop-entities - entities that sure will not be used from root entity. For example: for stock, supplier->user
will not be used in any case"
  [entity-name stop-entities]
  (letfn [(collect[sym prop path path-sym]
            (when-not (or ((set stop-entities) sym) ;should stop on this entity
                          ((set path-sym) sym))     ;recursion detection
              (let [fkey (cond (:join-table prop)
                               (let [table (:join-table prop)
                                     {:keys [to-table from-table]} prop
                                     table (name table)]
                                 [{:table to-table
                                   :fkey "id"}
                                  {:table table
                                   :fkey-left from-table
                                   :fkey-right to-table}])

                               (= :belongs-to (:rel-type prop))
                               {:table sym
                                :fkey (when-let [fk (:korma.sql.utils/generated (:fk prop))]
                                        (last (.split fk "(\\.|\")")))
                                :side :other}

                               true
                               {:table sym
                                :fkey (when-let [fk (:korma.sql.utils/generated (:fk prop))]
                                        (last (.split fk "(\\.|\")")))})
                    path (cons fkey path)
                    {:keys [rel table] :as ent} (deref (resolve (symbol sym)))]
                (list {:path (flatten path)}
                      (map (fn[[k prop]] (collect k @prop path (cons sym path-sym))) rel)))))]
    (remove nil? (flatten (collect entity-name {} [] [])))))

(defn- check-bidirectional-relations-for-root[root-entity]
  (let [relations (mreduce (fn[ent] {ent (keys (:rel (deref (resolve (symbol ent)))))}) @registered-entities)
        is-bidirectional? (fn[ent1 ent2] (and (some #{ent2} (relations ent1)) (some #{ent1} (relations ent2))))
        result (atom false)]
    (doseq [[ent rels] relations]
      (doseq [rel rels]
        (when-not (is-bidirectional? ent rel)
          (when (= rel root-entity)
            (fatal "You should define bidirectional relation for cached entity" ent)
            (reset! result true)))))
    (when @result
      (throwf "Bidirectional relation is not defined"))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Immediate tracking

(defn- wrap-table[table field]
  (let [wrap (fn[& s] (str "\"" (apply str s) "\""))]
    (str (wrap table) "." (wrap field))))

(defn generate-immediate-track[[table base :as tables]]
  (let [query-fn (fn[params]
                   (-> (select* (:table base))
                       (fields-only "id")
                       (where {(raw (:fkey table)) [in params]})))]
    (if (:side table)
      (do
        (query-fn [1 2 3])
        {:track (:table table)
         :field "id"
         :function (fn[params]
                     (map :id (select (query-fn params))))})
      {:track (:table table)
       :field (name (if (= 1 (count tables)) :id (:fkey table)))
       :function identity})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Multi step tracking
(defn prepare-query-for-dependent-entities
  "Generate query for getting root entity ids from updated deeply nested entities."
  [tables]
  (let [[first-table :as tables] tables
        butlast-table (last (butlast tables))
        tables (map vector tables (rest tables))
        wrap (fn[& s] (str "\"" (apply str s) "\""))
        key (fn[field](wrap (str field "-key")))
        last-key (let [{:keys [fkey fkey-left]} butlast-table]
                   (or fkey (key fkey-left)))]
    (reduce (fn[result
                [{table1 :table left1 :fkey-left right1 :fkey-right fkey1 :fkey side1 :side}
                 {table2 :table left2 :fkey-left right2 :fkey-right fkey2 :fkey side2 :side}]]
              (cond
                ;; process many-to-many associations
                right2 (join result table2
                             (= (raw (wrap-table table1 "id"))
                                (raw (key right2))))
                left1 (join result table2
                            (= (raw (wrap-table table2 "id"))
                               (raw (key left1))))

                ;; many to one
                (and (not= fkey1 last-key) side1)
                (join result table2
                      (= (raw (wrap-table table2 fkey1))
                         (raw (wrap-table table1 "id"))))

                ;; one to many
                (not= fkey1 last-key)
                (join result table2
                      (= (raw (wrap-table table2 "id"))
                         (raw (wrap fkey1))))
                true result))
            (-> (select* (:table first-table))
                (fields-only (let [{:keys [fkey fkey-left]} butlast-table]
                               (if fkey
                                 (wrap-table (:table butlast-table) fkey)
                                 (key fkey-left)))))
            tables)))

(defn generate-multi-step-track
  "When child entity is not connected directly to root entity, and we should make additional query to
find root entities."
  [[table :as tables]]
  (let [query
        (prepare-query-for-dependent-entities tables)

        query-fn
        (fn[params]
          (select (-> query
                      (where {(raw (wrap-table (:table table) "id")) [in params]}))))]
    ;; run query to test if it is correct
    (query-fn [1 2 3])
    ;; tracker
    {:track (:table table)
     :field (if (:side table) "id" (:fkey table))
     :function (fn[params] (map (comp second first) (query-fn params)))}))

(defn- check-track[{:keys [track field]}]
  (select track (fields-only (str "\"" field "\"")) (limit 1)))

(defn track-entity-change[entity stop-entities]
  (let [predicate (fn[p] (> (count p) 2))
        pathes (map :path (collect-entities entity stop-entities))
        immediate (->> pathes
                       (remove predicate)
                       (map generate-immediate-track))
        multi-steps (->> pathes
                         (filter predicate)
                         (map generate-multi-step-track))]
    (check-bidirectional-relations-for-root entity)
    (doseq [track (concat immediate multi-steps)]
      (info "Add cache tracking for root" entity "entity" (:track track) (:field track))
      (check-track track)
      (add-notification-track track))

    (refresh-db-notifications)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cache wrapper

(def ^:private cache (atom {}))

(defn in-cache[key root-entity id]
  (when-not *omit-cache*
    (get-in @cache [root-entity id *database-override* *context-types* key])))

(defn put-cache[key root-entity id value]
  (when-not *omit-cache*
    (swap! cache assoc-in [root-entity id *database-override* *context-types*  key] value)))

(defn info-from-caching-object[entity-object]
  (if (map? entity-object)
    [entity-object (:bypass-cache entity-object)]
    [entity-object false]))

(defmacro cached[key root-entity entity-object body]
  `(let [[id# bypass-cache#] (info-from-caching-object ~entity-object)]
     (if-let [cached# (when-not (or *omit-cache* bypass-cache#) (in-cache ~key ~root-entity id#))]
       cached#
       (let [result# ~body]
         (put-cache ~key ~root-entity id# result#)
         result#))))

(defn retrieve-cached[results key root-entity fetch-additional-fn]
  (if *count-query*
    results
    (if *omit-cache*
      (fetch-additional-fn (map :id results))
      (let [not-in-cache (map :id (remove (fn[{:keys [id]}] (in-cache key root-entity id)) results))]
        (when (seq not-in-cache)
          (doseq [entity (fetch-additional-fn not-in-cache)]
            (put-cache key root-entity (:id entity) entity) ))
        (map (fn[{:keys [id]}] (in-cache key root-entity id)) results)))))

(reset! on-flush-event-listener (fn[ids]
                                  (swap! cache assoc-in [:stock :all] nil)
                                  (doseq [id ids]
                                    (println "Flush stock : " id)
                                    (swap! cache assoc-in [:stock id] nil))))
