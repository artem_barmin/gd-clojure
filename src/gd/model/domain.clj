(in-ns 'gd.model.model)

(declare domain)

(defentity domain-host
  (belongs-to domain {:fk :fkey_domain}))

(defentity domain
  (belongs-to supplier)
  (has-many domain-host {:fk :fkey_domain})
  (json-data :info)
  (array-transform :domains))

(defentity redirects
   (table :redirect)
   (belongs-to domain {:fk :fkey_domain}))

(defn domain:get-redirects []
  (select redirects))

(defn domain:delete-redirect [id]
  (:id (delete redirects (where {:id id}))))

(defn domain:add-redirect [from to]
  (insert redirects
    (values {:from from :to to})))

(defn- domain:get-domain-info[]
  (map
   (fn[{:keys [domain-host info fkey_supplier]}]
     (let [info (-> info
                    (merge {:hosts (mapcat (fn[{:keys [host]}]
                                             [(str host ".smart.com.ua")
                                              (str host ".smart.com.ua:8080")
                                              (str host ".smartcommerce.com.ua")
                                              (str host ".sm24.com.ua")
                                              (str host ".sc24.com.ua")])
                                           domain-host)
                            :supplier fkey_supplier})
                    (update-in [:context] (partial map keyword))
                    (update-in [:database :sql] keyword)
                    (update-in [:design :buttons] keyword)
                    (assoc-in [:theme] [:custom :basic]))]
       (clojure.pprint/pprint info)
       info))
   (select domain (with domain-host))))

(defn- domain:refresh-prod-and-demo[]
  (let [hosts (domain:get-domain-info)]
    (refresh-hosts-map hosts)))

(def basic-template-settings
  {:storeName "Новый интернет магазин"
   :banners []
   :reviewSettings {:mainReviewNumber 3
                    :detailReviewNumber 8}
   :newsSettings {:mainNewsNumber 3
                  :detailNewsNumber 8}
   :footerSettings {:footerTop [{:rowSize "col-lg-3 col-md-3"
                                 :icon "fa fa-tags icon"
                                 :title "Лучшие цены"
                                 :description "Только у нас самые лучшие цены"}
                                {:rowSize "col-lg-3 col-md-3"
                                 :icon "fa fa-gift icon"
                                 :title "Бесплатные подарки"
                                 :description "Каждый месяц бесплатные подарки"}
                                {:rowSize "col-lg-3 col-md-3"
                                 :icon "fa fa-lock icon"
                                 :title "Безопасная оплата"
                                 :description ""}
                                {:rowSize "col-lg-3 col-md-3"
                                 :icon "fa fa-calendar icon"
                                 :title "Поддержка 24/7"
                                 :description "Мы всегда рады вам помочь"}]
                    :footerCenter [{:rowSize "col-lg-6 col-md-3"
                                    :heading "Свяжитесь с нами"
                                    :description [{:text "Телефон: "
                                                   :href ""}
                                                  {:text "Факс: "
                                                   :href ""}
                                                  {:text "Email: "
                                                   :href ""}]}
                                   {:rowSize "col-lg-6 col-md-3"
                                    :heading "Мой профиль"
                                    :description [{:text "Профиль"
                                                   :href "/profile/"}
                                                  {:text "Заказы"
                                                   :href "/orders/"}
                                                  {:text "Корзина"
                                                   :href "/bucket/"}]}]}})

(defn domain:save-host[supplier-id type hosts & [template-settings]]
  (transaction
   (let [main-host (:name (select-single supplier (where {:id supplier-id})))
         type-to-context
         {:wholesale {:context-filter [:wholesale]
                      :context [:wholesale :wholesale-sale]}
          :retail {:context-filter [:retail]
                   :context [:retail :retail-sale]}}]

     ;; delete previous version of domain info
     (let [{:keys [id]}
           (find-first (fn[{:keys [info]}]
                         (= (sort (:context (type type-to-context)))
                            (sort (map keyword (:context info)))))
                       (select domain (where {:fkey_supplier supplier-id})))]
       (delete domain-host (where {:fkey_domain id}))
       (delete domain (where {:id id})))

     ;; insert new version of domain
     (let [domain (:id (insert domain
                               (values {:fkey_supplier supplier-id
                                        :info (merge {:design {:buttons :complex}
                                                      :template-settings (or template-settings
                                                                             basic-template-settings)
                                                      :category main-host
                                                      :stocks-mode :available-from-arrival}
                                                     (type type-to-context))})))]
       (doseq [host hosts]
         (let [host (if (seq host) (str host "." main-host) main-host)]
           (insert domain-host (values {:fkey_domain domain :host host}))
           (info "Registered host for supplier" supplier-id host type))))

     ;; refresh info
     (domain:refresh-prod-and-demo))))

(defn- domain:save-admin-host[supplier-id main-host]
  (let [domain
        (:id (insert domain
                     (values {:fkey_supplier supplier-id
                              :info {:url "/admin"
                                     :offset-url "/retail"
                                     :design {:buttons :plain}
                                     :category main-host
                                     :context [:retail :wholesale
                                               :wholesale-sale :retail-sale]}})))]
    (insert domain-host (values {:fkey_domain domain :host main-host}))

    ;; refresh info
    (domain:refresh-prod-and-demo)))

(declare dictionary:update-dictionary)

(defn domain:add-admin[supplier-id]
  (transaction
   (let [main-host (:name (select-single supplier (where {:id supplier-id})))]
     (info "Registered admin domain for supplier" supplier-id main-host)
     (save-categories-tree
      [["top-level-category" main-host
        [["Одежда" "clothes"
          [["Мужская" "male"]
           ["Женская" "female"]]]
         ["Обувь" "shoes"
          [["Мужская" "male"]
           ["Женская" "female"]]]
         ["Распродажа" "sale"]
         ["Новые поступления" "new"]]]
       ["other" "other"]])
     (binding [*current-supplier* supplier-id]
       (dictionary:update-dictionary {:param [{:type "size" :name "Размер" :filter "По размерам"}
                                              {:type "color" :name "Цвет" :filter "По цветам"}
                                              {:type "brand" :name "Бренд" :filter "По брендам"}]}))
     (domain:save-admin-host supplier-id main-host)))

  ;; add sample host(test)
  (domain:save-host supplier-id :wholesale [""]))

(defn- domain:build-url[host]
  (if (seq host)
    (if (= host "bono")
      "http://bono.in.ua"
      (str "http://" host ".sc24.com.ua"))
    "http://sc24.com.ua"))

(defn domain:get-admin-domain[supplier-id]
  (domain:build-url (:name (select-single supplier (where {:id supplier-id})))))

(defn domain:get-client-domains[]
  (map
   (fn[host] (update-in host [:host] domain:build-url))
   (select domain-host
           (with domain)
           (where {:domain.fkey_supplier *current-supplier*}))))

(try (domain:refresh-prod-and-demo)
     (catch Exception e
       (error "Not yet initialized - skipping")))
