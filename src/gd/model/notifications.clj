(in-ns 'gd.model.model)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Notification subscriptions

(defmacro defnotify[f-var args & body]
  (let [notify-name (symbol (str f-var "-notify"))]
    `(do
       (defn ~notify-name [f# & [~@args :as args#]]
         (let [res# (apply f# args#)
               ~'fn-result res#]
           (do ~@body)
           res#))
       (add-hook (var ~f-var) (var ~notify-name)))))

(declare notify:send)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Include concrete implementation of notifications
(load "concrete-notifications")

(defentity notification-silence
  (use-enum notification-type [:type])
  (enum notification-service [:service] :mail :sms :vk))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Notifications publish/subscriber

(def notification-channel "notification-channel")

(create-publisher notification-channel 16) ;16MB size of queue

(def ^:dynamic *should-publish* false)  ;used only for testing purposes - will the message published
                                        ;throught mongo queue in testing mode(rollbacked transaction)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Notification service functions

(defn- notification-type-to-entity[type]
  (maybe-deref-delayed (second (first (filter (fn[[keys]] ((set keys) type)) notify-constraints)))))

(defn- notification-fkey-by-type[type]
  (fkey-for-relation notification (notification-type-to-entity type)))

(defn- notify:send[recipient source type & [info]]
  (transaction
    (let [data {:fkey_user recipient
                (notification-fkey-by-type type) source
                :type type
                :opened false
                :created (sql-now-timestamp)
                :info info}]
      ;; if we are in rolled-back transaction(test environement) - save notification now
      (if (clojure.java.jdbc/is-rollback-only)
        (let [id (:id (insert notification (values data)))]
          (when *should-publish*
            (publish notification-channel {:id id})))
        (add-after-transaction-hook
         (fn[]
           ;; we must insert notification after current outermost transaction, because
           ;; 1. notification recieved in the subscriber are executed in other thread, and
           ;; must be able to observe all DB data from current operation(like created user and so on)
           ;; 2. all data should be saved because of foreign key constraints
           (let [id (:id (insert notification (values data)))]
             ;; publish notification
             (publish notification-channel {:id id}))))))))

(defn- fetch-sources-for-notifications[notifications]
  (map (fn[{:keys [type source-serialized] :as result}]
         (let [entity (notification-type-to-entity type)
               extra-info (or (get notify-fetch-plans entity) identity)]
           (assoc result
             :source
             (disable-default-where
               (select-single entity extra-info (where {:id ((notification-fkey-by-type type) result)}))))))
       notifications))

(defn notify:current-user-notifications[limit-num offset-num filters]
  (let [notifications
        (select notification
          (with user (user-full-fetch-query))
          (limit limit-num)
          (offset offset-num)
          (order :created :desc)
          (where {:fkey_user (sec/id)})
          (where-if (seq filters) (apply or (mapcat (fn[[types sources]]
                                                      (map (fn[type]
                                                             (merge
                                                              {:type (notification-type type)}
                                                              (when sources
                                                                {(notification-fkey-by-type type) [in sources]})))
                                                           types))
                                                    filters)))
          (count-query))]
    (if *count-query*
      notifications
      (fetch-sources-for-notifications notifications))))

(defn notify:get-by-id[id]
  (first (fetch-sources-for-notifications (select notification
                                            (with user (user-full-fetch-query))
                                            (where {:id id})))))

(defn notify:get[source type]
  (fetch-sources-for-notifications
   (select notification
     (with user (user-full-fetch-query))
     (where {(notification-fkey-by-type type) source
             :opened false
             :fkey_user (sec/id)
             :type (notification-type type)}))))

(defn notify:get-counts-map[types]
  (let [sources-with-count
        (fmap count
              (group-by
               :source
               (select notification
                 (where {:opened false
                         :fkey_user (sec/id)
                         :type [in (map notification-type types)]}))))]
    {:overall (reduce + (vals sources-with-count))
     :map sources-with-count}))

(defn notify:mark-as-opened[ids]
  (update notification
    (set-fields {:opened true})
    (where {:id [in ids]
            :fkey_user (sec/id)})))

(defn notify:all-new-events[]
  (:cnt (select-single notification
                       (aggregate (count :id) :cnt)
                       (where {:fkey_user (sec/id)
                               :opened false}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Notification subscriptions

(defn notify:set-subscriptions[subscriptions]
  (transaction
    ;; delete all current notifications
    (delete notification-silence (where {:fkey_user (sec/id)}))

    ;; find what subscriptions must be silent
    (let [silence (set/difference (set (values-notification-type)) (set subscriptions))]
      (when (seq silence)
        (insert notification-silence
          (values (map (fn[type] {:type type :service :mail :fkey_user (sec/id)}) silence)))))))

(defn notify:current-subscriptions[]
  (set/difference
   (set (values-notification-type))
   (set (map :type (select notification-silence (where {:fkey_user (sec/id)}))))))

(defn notify:check-subscription[user-id type service]
  (comment "If entry is found - we should not send notification to this service.")
  (not (select-null notification-silence (where {:type (notification-type type)
                                                 :service (notification-service service)
                                                 :fkey_user user-id}))))
