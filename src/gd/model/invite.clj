(in-ns 'gd.model.model)

(defentity invite
  (belongs-to user {:fk :invited :entity :registered}))
(extend-entity user
  (has-many invite {:fk :invited}))

(comment "Invites model :
1. All invites are preallocated during metamodel initialization
2. When user click 'Generate new code' - random free code is choosed from table, and :captured is set to now
3. If man is captured more than 10 codes - block until next day
4. After 2 months - captured and not used code is marked as free(by periodical processor).
 ")

(defn invite:left
  "Number of invites for this user."
  []
  (- 10
     (select-count invite
                   (where (and (= :fkey_user (:id (sec/logged)))
                               (> :captured (tm/to-timestamp (today-at-midnight)))
                               (> :captured (tm/to-timestamp (minus (today-at-midnight) (days 1)))))))))

(defn invite:can-be-send-now
  "True - if inivite can be send now(number of today invitations are less than 10)."
  []
  (< (select-count invite
                   (where (and (= :fkey_user (:id (sec/logged)))
                               (> :captured (tm/to-timestamp (today-at-midnight)))
                               (> :captured (tm/to-timestamp (minus (today-at-midnight) (days 1)))))))
     10))

(defn invite:capture[]
  (transaction
    ;; lock invites table
    (lock-table :invite)

    ;; choose new code from free codes
    (if (invite:can-be-send-now)
      (let [whole-count
            (select-count invite (where {:fkey_user nil}))

            choosed-invite
            (:id (select-single invite
                                (offset (rand-int whole-count))
                                (limit 1)))]
        (assert choosed-invite)
        (update invite
          (set-fields {:captured (sql-now-timestamp) :fkey_user (:id (sec/logged))})
          (where {:id choosed-invite})))
      (throwf "More than 10 invites are already captured during this day. Try again later."))))

(defn invite:initial-generation[batch]
  (dotimes [i 10]
    (insert invite
      (values
       (map
        (fn[j]
          {:id (+ (* i batch) j)
           :code (format "%06d" (+ (* i (* 10 batch)) (* j 10) (rand-int 10)))})
        (range 1 batch))))))

(defn invite:check[code]
  (= (count (select invite (where {:code code :invited nil :captured [not= nil]}))) 1))

;used in defnotify
(defn invite:use-code[code user-id]
  (update invite (set-fields {:invited user-id}) (where {:code code})))

(defn invite:get-owner-of-code[code]
  (:fkey_user (select-single invite (where {:code code}))))

(defn invite:use-internal[code login password email phone]
  (transaction
    (if (or (empty? code) (invite:check code))
      (let [user (user:create-user-internal
                  {:auth-profile [{:type :internal :login login :password password}]
                   :contact {:email email }}
                  phone)]
        (when code
          (invite:use-code code (:id user)))
        user)
      (throwf "Code are already in use or invalid"))))

(defn invite:use-external
  "If code is null and user already exists - just authorize him. Otherwise -
usuall mechanism of invite code checking."
  [code {:keys [id] :as user}]
  (transaction
    (if (empty? code)
      user
      (let [[activated-code-for-user] (select-null invite (where {:invited id}))]
        (cond activated-code-for-user user
              (invite:check code) (do (invite:use-code code id) user))))))

(defn invite:list
  "Return list of already used invitations."
  ([] (invite:list (sec/logged)))
  ([user-to-check]
     (select invite
       (with user (with auth-profile) (with contact))
       (where (and (not= :invited nil) (= :fkey_user (:id user-to-check))))
       (count-query))))

(defn invite:sent-email-invite
  "Send invite by email"
  [address]
  (let [code (:code (invite:capture))
        {:keys [name id]} (sec/logged)]
    (info "Email invite code" id name code)
    (send-mail address
               (render (context-info "Приглашение" :invite :header) {:name name})
               (render-resource
                (context-info "mail/invite.html" :invite :template)
                {:name name
                 :link (str "http://dayte-dve.com.ua?code=" code)}))))

(defn invite:sent-sms-invite
  "Send invite by email"
  [phone]
  (let [code (:code (invite:capture))
        {:keys [name id]} (sec/logged)]
    (info "SMS invite code" id name code)
    (send-sms
     phone
     (render
      (context-info
       "{{name}} просит Вас стать участником совместной покупки на dayte-dve.com.ua Код приглашения: {{code}}"
       :invite :sms)
      {:name name :code code}))))
