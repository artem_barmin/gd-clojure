(in-ns 'gd.model.model)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Simple request based caches
(wrap-request-cache #'stock-context:resolve-or-create-type)
(wrap-request-cache #'stock-context:resolve-type)
(wrap-request-cache #'stock-context:resolve-id-to-type)

(wrap-request-cache #'category:model)
(wrap-request-cache #'category:whole-model)
(wrap-request-cache #'category:get-current-parent-category)

(wrap-request-cache #'meta:get-filters-for-stocks-with-count)
(wrap-request-cache #'meta:get-params-from-id-list)

(wrap-request-cache #'dictionary:all-params)

(wrap-request-cache #'analytics:sale-report)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DB tracking invalidation

(track-entity-change "stock" ["user" "retail-order" "inventory-available-stocks"])
;; (flush-whole "category")
