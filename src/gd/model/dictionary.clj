(in-ns 'gd.model.model)

(comment "dictionary-param - union of all available parameters for supplier.

User can create dependencies between parameters, merge them.
")

(defentity dictionary-param)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Internal API

(defn- dictionary:extract[{:keys [name value]}]
  {:name (keyword-real-name name)
   :value (keyword-real-name value)})

(defn- dictionary:already-existing-params[parameters]
  (when (seq parameters)
    (select dictionary-param
      (where {:fkey_supplier *current-supplier*})
      (where (apply or (map dictionary:extract parameters))))))

(defn- dictionary:add-parameters[parameters]
  (transaction
    (let [already-existing-params (->> (dictionary:already-existing-params parameters)
                                       (group-by-single dictionary:extract))]
      (doseq [param (map dictionary:extract parameters)]
        (when-not (get already-existing-params param)
          (insert dictionary-param (values (merge param {:fkey_supplier *current-supplier*}))))))))

(defn- dictionary:add-properties [parameters]
  (transaction
    (let [dict (select supplier
                       (fields-only :dictionary)
                       (where {:id *current-supplier*}))
          existing-properties (:properties (:dictionary (first dict)))
          new-props (into [] (map #(:name %) (:properties (:description parameters))))
          merged (into [] (distinct (concat existing-properties new-props)))]

      (update supplier
              (set-fields {:dictionary (assoc-in (:dictionary (first dict)) [:properties] merged)})
              (where {:id *current-supplier*})))))

(def dictionary:all-params)

(defn- dictionary:prepare-params[dictionary]
  (let [existing
        (dictionary:all-params)

        params-map
        (fmap :id (group-by-single (juxt :name :value) existing))

        params
        (fn[[name value :as key]]
          (let [existing (get params-map key)]
            (when-not existing
              (throwf "Parameter %s %s not found" name value))
            existing))

        transform-values (fn[values param]
                           (map (fn[value] (params [(keyword-real-name param) value])) values))
        transform-condition (fn[param-map]
                              (map (fn[[param value]] (params [(keyword-real-name param) value])) param-map))]
    (mreduce (fn[[param conditions]]
               {param (map (fn[condition]
                             (-> condition
                                 (update-in [:values] transform-values param)
                                 (update-in [:condition] transform-condition)))
                           conditions)})
             dictionary)))

(defn- dictionary:transform-params[dictionary]
  (let [params (group-by-single :id (dictionary:all-params))
        transform-values (fn[values param] (map (fn[value] (:value (params value))) values))
        transform-condition (fn[param-list] (mreduce (fn[value]
                                                       (let [{:keys [name value]} (params value)]
                                                         {name value}))
                                                     param-list))]
    (mreduce (fn[[param conditions]]
               {param (->>
                       conditions
                       (map (fn[condition]
                              (when (:condition condition)
                                (-> condition
                                    (update-in [:values] transform-values param)
                                    (update-in [:condition] transform-condition)))))
                       (remove nil?))})
             dictionary)))

(defn- diction:rename-parameter-value[name old new]
  (info "Rename parameter" name "values" old "->" new)
  (transaction

    (let [params-by-stock
          (->>
           (select stock-parameter-value
             (where {:fkey_stock [in (subselect stock
                                                (fields :id)
                                                (where {:fkey_supplier *current-supplier*
                                                        :stock.id :stock-parameter-value.fkey_stock}))]
                     :name name})
             (where (or {:value old} {:value new})))
           (group-by :fkey_stock))]
      (doseq [[id params] params-by-stock]
        (cond
          ;; old parameter should be removed, and all arrivals should be bound to new parameter
          (= (count params) 2)
          (let [{old-id :id} (find-by-val :value old params)
                {new-id :id} (find-by-val :value new params)
                conditions (fn[query]
                             (-> query
                                 (set-fields {:stock-parameter-value-key new-id})
                                 (where {:stock-parameter-value-key old-id})))]

            ;; fix all relations from arrivals and order items
            (update :stock-arrival-parameter-with-stock-parameter-value conditions)
            (update :user-order-item-with-stock-parameter-value conditions)

            ;; remove old version of parameter
            (delete stock-parameter-value (where {:id old-id})))

          ;; just rename
          (= (count params) 1)
          (update stock-parameter-value (set-fields {:value new}) (where {:id (:id (first params))})))))

    ;; update params
    (update dictionary-param
      (set-fields {:value new})
      (where {:name name
              :value old
              :fkey_supplier *current-supplier*}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Public API
(defn dictionary:merge-params[param1-id param2-id])

(defn dictionary:all-params[]
  (let [left-values
        (->>
         (select stock-parameter-value
           (fields-only :name :value)
           (modifier "distinct")
           (where {:id [in (subselect (left-stocks) (fields-only "unnest(params)"))]}))
         (group-by-single (juxt :name :value)))]
    (->>
     (select dictionary-param (where {:fkey_supplier *current-supplier*}))
     (map (fn[{:keys [name value] :as param}]
            (assoc param :used (boolean (get left-values [name value]))))))))

(defn dictionary:update-dictionary[dictionary & [params-change]]
  (transaction
    ;; process params changing
    (doseq [{:keys [old value name]} params-change]
      (diction:rename-parameter-value name old value))

    ;; process dictionary save
    (update supplier
      (set-fields
       {:dictionary (->
                     dictionary
                     (update-in [:conditions]
                                dictionary:prepare-params)
                     (update-in [:conditions]
                                (fn[conditions]
                                  (fmap (partial filter (comp seq :condition)) conditions)))
                     (update-in [:param]
                                (fn[params]
                                  (->>
                                   params
                                   (filter (every-pred :name :type))
                                   (map (fn[param] (select-keys param [:type :name :filter])))))))})
      (where {:id *current-supplier*}))))

(defn dictionary:get-dictionary[]
  (let [dictionary
        (:dictionary (select-single supplier (where {:id *current-supplier*})))

        existing
        (dictionary:all-params)

        conditions
        (dictionary:transform-params (:conditions dictionary))

        saved-params
        (->> existing
             (map dictionary:extract)
             (group-by :name))

        processed-conditions
        (mreduce (fn[[name saved-values]]
                   (let [dict-values
                         (get conditions (keyword name))

                         used-in-dict
                         (map (fn[v] {:name name :value v}) (flatten (map :values dict-values)))

                         other-values
                         (set/difference (set saved-values) (set used-in-dict))]
                     {name
                      (remove nil?
                              (concat [(when (seq other-values) {:values (vec (map :value other-values))
                                                                 :special "other"})
                                       (when (seq dict-values) {:values (vec (map :value saved-values))
                                                                :special "all"})]
                                      dict-values))}))
                 saved-params)]
    (->>
     {:raw-conditions (dictionary:prepare-params processed-conditions)
      :conditions processed-conditions}
     (merge dictionary)
     (walk/postwalk (fn[m] (if (sequential? m) (vec m) m))))))

(defn dictionary:get-simple-dictionary[]
  (:dictionary (select-single supplier (where {:id *current-supplier*}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Initial filling
(declare retail:get-stock-params)

(defn retail:fill-dictionary-from-params[]
  (transaction
    (let [old-dict (:dictionary (select-single supplier (where {:id *current-supplier*})))]
      (update supplier
        (set-fields {:dictionary (dissoc old-dict :conditions :raw-conditions)})
        (where {:id *current-supplier*})))
    (delete dictionary-param (where {:fkey_supplier *current-supplier*}))
    (dictionary:add-parameters (params-map-to-list nil (retail:get-stock-params)))))
