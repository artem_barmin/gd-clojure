(in-ns 'gd.model.model)

(defn save-categories-tree
  ([tree]
     (save-categories-tree tree nil nil))
  ([tree path parent-id]
     (doseq [[name short-name childs] tree]
       (let [key
             (strings/join "-" (reverse (cons short-name path)))

             [current-value]
             (select category (where {:key key}))

             cat-saved
             (if current-value
               (update category (set-fields {:name name :short-name short-name :fkey_category parent-id}) (where {:key key}))
               (insert category (values {:name name :short-name short-name :fkey_category parent-id :key key})))]
         (save-categories-tree childs (cons short-name path) (:id cat-saved))))))
