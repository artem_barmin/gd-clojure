(in-ns 'gd.model.model)

(defn search:find-stock[search-string]
  (select stock
    (with category)
    (join :inner :stock-search (= :stock-search.fkey_stock :stock.id))
    (where {[:stock-search.deal :stock-search.stock :stock-search.category] [search search-string]
            :stock.status (stock-status :visible)})
    (limit 10)))
