(in-ns 'gd.model.model)

(declare site-pages:process-content)

(defentity site-page
  (json-data :seo)
  (enum site-page-type [:type] :news :page :category-page :visual-news :blogs)
  (enum site-page-status [:status] :published :not-published)
  (transform #'site-pages:process-content)
  stock-context:resolve-entity-contexts)

(defentity site-page-not-mail)

(defn- site-pages:process-content[entity]
  (update-in entity [:content] bbcode-to-html))

(declare prepare-url-for-fetching)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; General API

(defn site-page:page-create [{:keys [title type] :as params}]
  (transaction
    (let [{:keys [id] :as page}
          (insert site-page
                  (values
                    (merge
                      (-> (select-keys params [:title :url :type :seo :contexts :status :content :content-short])
                          (assoc-in [:url] (prepare-url-for-fetching (:url params))))
                      {:time (sql-now-timestamp)})))]
      ;; for news we create url automaticaly : id + title
      (cond (or (= type :news) (= type :blogs)) (update site-page
                                                        (set-fields {:url (.toLowerCase (str id "-" (translit title)))})
                                                        (where {:id id}))))))

(defn site-pages:create-page [{:keys [title type] :as params}]
  (if (and (= (get params :contexts) nil) (= (get params :page-type) :blogs))
    (let [params (assoc params :contexts ["wholesale" "retail"])] (site-page:page-create  params))
    (site-page:page-create params))
  )


(defn site-pages:update-page[id params]
  (update site-page
    (set-fields
     (merge
      (select-keys params [:title :seo :contexts :status :content :content-short])
      {:time (sql-now-timestamp)}))
    (where {:id (parse-int id)})))

(defn site-page:delete [id]
  (prn id)
  (transaction
  (delete :notification
          (where {:fkey_site-page id}))
  (delete site-page
          (where {:id id}))
  ))


(defn site-page:add-not-mail [mail]
  (insert
    site-page-not-mail
    (values {:mail mail})))

;;;;;;;;;;;;;;;;;;;;
;; List

(declare filter-by-context)

(defn site-pages:get-pages-list [limit-num offset-num  {:keys [type status contexts]}]
  (select site-page
    (limit limit-num)
    (offset offset-num)
    (filter-by-context contexts)
    (where-if type {:type (site-page-type type)})
    (where-if status {:status (site-page-status status)})
    (order :time :desc)
    (count-query)))

;;;;;;;;;;;;;;;;;;;;
;; Getters

(defn- prepare-url-for-fetching
  "Remove traling and leading slashes"
  [url]
  (when url
    (-> url
        (.replaceAll "^/" "")
        (.replaceAll "/$" ""))))

(defn site-pages:get-page-for-url [url]
  (select-single-null site-page
    (where {:url (prepare-url-for-fetching url)})
    filter-by-context))

(defn site-pages:annotate-categories [categories]
  (let [pages
        (group-by-single
         :url
         (select site-page
           (where {:url [in (map (comp prepare-url-for-fetching :path) categories)]})
           filter-by-context))]
    (map (fn[{:keys [path name] :as cat}]
           (let [page (get pages (prepare-url-for-fetching path))]
             (assoc cat :page page :name (if (seq (:custom-name (:seo page)))
                                           (:custom-name (:seo page))
                                           name))))
         categories)))

(defn site-pages:annotate-single-category [category]
  (first (site-pages:annotate-categories [category])))

(defn site-pages:get-page-for-id [id]
  (select-single-null site-page
    (where {:id id})
    filter-by-context))

(defn site-pages:get-page-for-url [url]
  (select-single-null site-page (where {:url url})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; News publishing

(declare notify:send)

(defn news:publish-new[page-id]
  (transaction
   (let [{:keys [title content published-by-mail]} (select-single site-page (where {:id page-id}))]
     (when (not published-by-mail)
       (update site-page (set-fields {:published-by-mail true}) (where {:id page-id}))
       (info "Publish news" page-id title)
       (doseq [{:keys [id contact]} (user:get-all-users nil nil)]
         (when-not ((->>
                      (select site-page-not-mail (fields :mail))
                      (map :mail)
                      set) #_#{"gold_aa@mail.ru"
                      "zemlyanichka1507@gmail.com"
                      "sun.draw@mail.ru"
                      "jenar@ukr.net"
                      "a.lukiv13@gmail.com"
                      "ximkaIV@gmail.com"
                      "laelis@ukr.net"
                      "meri1977@mail.ua"
                      "Sergeyy-dubrov@rambler.ru"
                      "lenpopova@ukr.net"
                      "0684015844@ukr.net" }
                     ((comp :value :email) contact) )
           (notify:send id page-id :site-page-news-published)))

       (let [old-emails (set (map strings/trim (.split (slurp "resources/migration/bono.txt") "\n")))
             new-emails (set (user:get-all-emails))]
         (doseq [address (set/intersection old-emails new-emails)]
           (info "Already registered user" address))
         (doseq [address (set/difference old-emails new-emails)]
           (send-mail address title content))))
     (when published-by-mail
       (info "News is already published")))))

(defn news:test-publish-new[page-id email]
  (let [{:keys [title content published-by-mail]} (select-single site-page (where {:id page-id}))]
    (when (not published-by-mail)
      (send-mail email title content))))
