(ns gd.model.test.sample
  (:refer-clojure :exclude [extend])
  (:require [clj-time.coerce :as tm])
  (:use gd.model.test.data_builders
        [faker.name :only (names)]
        clj-time.core
        lobos.connectivity
        gd.utils.test
        gd.utils.web
        gd.log
        gd.parsers.llapi
        gd.model.model
        gd.utils.common
        gd.utils.db))

(def sample-categories
  [["Retail-stocks" "stocks"
    [["Распродажа" "sale"]
     ["Новые поступления" "new"]
     ["Женщинам" "female"
      [["Майки и Футболки" "topsAndTShirts"
        [["Майка" "top"]
         ["Майка бельевая" "underTop"]
         ["Борцовка" "highTop"]
         ["Футболка" "tShirt"
          [["Нормальные" "normal"]
           ["Батал" "batal"]]]]]
       ["Гольфы и водолазки" "golfAndTurtleNeck"
        [["Короткий рукав" "shortSleeve"]
         ["Сетка" "net"]
         ["Начес" "bouffant"]
         ["Кашемир" "kashemir"]
         ["Однотонный" "monochromatic"]
         ["С рисунком" "withImage"]
         ["Футболка (длинный рукав)" "tShirtWithlongSleeve"]]]
       ["Одежда для дома" "homeClothes"
        [["Комплект" "complect"]
         ["Тапочки" "sneakers"]
         ["Халат" "bathrobe"]
         ["Ночная рубашка" "nightShirt"]]]
       ["Аксессуары" "accessories"
        [["Платок" "shawl"]]]
       ["Платья" "dress"
        [["Короткое платье" "shortDress"]]]
       ["Блузы и Балеро" "blouseAndBaler"
        [["Блуза" "blouse"]
         ["Балеро" "baler"]]]]]
     ["Мужчинам" "male"
      [["Майки и футоболки" "topsAndTShirts"
        [["Борцовка" "highTop"]
         ["Футболка" "tShirt"]
         ["Безрукавка" "withoutSleeve"]]]]]]]

   ["other" "other"]])

(defn make-sample-data[]

  (enable-dev-mode)

  (save-categories-tree sample-categories)

  (let [categories (map category:get-by-key
                        ["stocks-female-topsAndTShirts-top"
                         "stocks-female-topsAndTShirts-tShirt-normal"
                         "stocks-female-topsAndTShirts-tShirt-batal"
                         "stocks-female-topsAndTShirts-underTop"
                         "stocks-female-topsAndTShirts-highTop"
                         "stocks-female-golfAndTurtleNeck-net"
                         "stocks-male-topsAndTShirts-withoutSleeve"])

        root (make:user-internal
              "admin" "admin"
              :real-name "Администратор" :contact {:phone "+380509790587" :email "root@root.com"})

        organizer1 (make:user-internal
                    "abarmin" "123456"
                    :real-name "Артем Бармин" :contact {:phone "+380509790587" :email "c@c.com"})

        subscriber1 (make:user-internal
                     "kmotorniy" "123456"
                     :real-name "Константин Моторный")

        categories-counter (atom 0)

        sup
        (make:supplier
         :stock-num 50
         :user {:contact {:phone "+380634846336" :email "koval111vladislav@gmail.com"}}
         :stock (fn[i]
                  {:images [{:path (use-image :items (rnd 1 5))}
                            {:path (use-image :items (rnd 1 5))}
                            {:path (use-image :items (rnd 1 5))}]
                   :fkey_category (:id (nth categories (mod (swap! categories-counter inc) (count categories))))
                   :description {:measurements {:xl [{:name "талия" :value "10"} {:name "шея" :value "20"}]
                                                :xxl [{:name "талия" :value "30"} {:name "шея" :value "40"}]
                                                :m [{:name "талия" :value "50"} {:name "шея" :value "60"}]}
                                 :other (sentences 5)}
                   :price (* (inc i) 20)
                   :accounting :warehouse
                   :stock-variant (mreduce
                                   (fn[size]
                                     {{:size size} [{:context :wholesale :price (* (inc i) 30)}
                                                    {:context :retail :price (* (inc i) 2 30)}]})
                                   [:s :m :xl :xxl])
                   :params {[:color false] [(rand-nth [:red :green :yellow])]
                            [:brand false] [(rand-nth ["TM Azra" "TM Bono" "TM First" "TM Second" "TM Third"])]
                            :size [:s :m :l :xl :xxl]
                            [:collection false] ["Лето"]}}))]

    (wrap-supplier sup
      (wrap-context [:wholesale :wholesale-sale]

        (make:arrival sup :parameters [{:size "m"} {:size "s"} {:size "xxl"} {:size "xl"}] :count 500)

        (let [orders-num 100
              stocks (map :id (sup-stocks sup))
              sizes [:m :s :xl :xxl]
              names (take orders-num (names))
              dates (sort (map (fn[_] (-> (rand-int 400) days ago)) (range 0 orders-num)))]
          (dotimes [i orders-num]
            (wrap-security (make:user-internal (str "kmotorniy" i) "123456" :real-name (nth names i))
              (with-redefs-fn {#'clj-time.core/now (fn[] (nth dates i))}
                (fn[]
                  (retail:order-next-status
                   (retail:make-order (map (fn[i] {:fkey_stock (rand-nth stocks)
                                                   :size (rand-nth sizes)
                                                   :quantity (inc (rand-int 5))})
                                           (range 0 (inc (rand-int 5)))))))))))

        (retail:fill-dictionary-from-params)

        (dictionary:update-dictionary {:param [{:type "size" :name "Размер" :filter "По размерам"}
                                               {:type "color" :name "Цвет" :filter "По цветам"}
                                               {:type "brand" :name "Бренд" :filter "По брендам"}]})))))

(defn make-demo-sample-data[]
  (binding [*database-override* (custom-sql-databases :demo)
            *lobos-database-override* :demo]
    (with-named-connection :demo
      (fn[](clean-db-safe :force)))
    (make-sample-data)))
