(ns gd.model.test.data_builders
  (:refer-clojure :exclude [extend])
  (:require [noir.options :as options])
  (:use faker.company
        [clojure.algo.generic.functor :only (fmap)]
        clojure.java.io
        clj-time.core
        korma.core
        clojure.walk

        gd.utils.pub_sub
        gd.model.model
        gd.model.context

        gd.utils.common
        gd.utils.test
        gd.utils.db
        gd.utils.json-path
        gd.utils.web

        gd.parsers.llapi
        [lobos.core :only [migrate rollback]]
        [lobos.migration :only [*migrations-namespace* *migrations-table*]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils
(def img-path "test/resources/img/")
(defn use-image[type img-name]
  (str img-path (name type) "/" img-name ".jpg"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Shortcuts for retrieving

(defn theme-by-name-whole[name]
  (select-single forum-theme
                 (with forum-theme)
                 (where {:name name})))

(defn theme-by-name[name]
  (:id (theme-by-name-whole name)))

(defn simple-category-model[]
  (prewalk (fn[tree]
             (if (contains? tree :child)
               (:child tree)
               tree))
           (category:tree)))

(defn stock-variants[stock-variants]
  (fmap (fn[m] (when m (select-keys m [:context :price]))) stock-variants))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Data builders

(defonce counter (atom 0))

(defn- get-counter[]
  (swap! counter inc))

(defn make:category[& [name]]
  (let [prefix (get-counter)
        name (or name (str "Одежда" prefix))]
    (save-categories-tree [[name name]])
    (category:get-by-key name)))

(defn make:user-whole[& {:keys [] :as override}]
  (let [prefix (get-counter)
        info (merge
              {:real-name (str "Юзер Юзерович" prefix)
               :birthdate (now)
               :sex :male
               :real-activities "Программист"
               :role ["user" "organizer"]
               :auth-profile [{:type :internal
                               :login (str "abarmin" prefix)
                               :password (str "abarmin" prefix)}]
               :contact {:phone "+380509790587" :email "c@c.com"}}
              override)]
    (user:create-user-internal info)
    (user:check-credentials (:login (first (:auth-profile info)))
                            (:password (first (:auth-profile info))))))

(defn make:user[& override]
  (:id (apply make:user-whole override)))

(defn make:root[]
  (:id (make:user-whole :auth-profile [{:type :internal
                                        :login "admin"
                                        :password "root"}])))

(defn make:user-internal[login password & override]
  (apply make:user (concat [:auth-profile
                            [{:type :internal
                              :login login
                              :password password}]]
                           override)))

(defn make:stock
  ([override category num]
     (merge
      {:name (str "stock" num)
       :fkey_category (:id category)
       :description {"Ткань" (sentences 1) :other [(sentences 4)]}
       :price (* num 10)
       :status :visible
       :accounting :warehouse
       :params {:color [:red :green :yellow]
                :size [:s :m :l]
                [:collection false] ["Лето"]}
       :stock-variant {{:color :yellow} 20}
       :images [{:path (use-image :items (rnd 1 5))}]
       :meta-info {:additional-windows {:size "sample-deal"}}}
      (override num)))
  ([override category]
     (merge
      {:name "stock"
       :fkey_category (:id category)
       :description {"Ткань" (sentences 1) :other [(sentences 4)]}
       :status :visible
       :accounting :warehouse
       :price 10
       :params {:color [:red :green [:yellow 20]]
                :size [:s :m :l]
                [:collection false] ["Лето"]}
       :stock-variant {{:color :yellow} 20}
       :images [{:path (use-image :items (rnd 1 5))}]
       :meta-info {:additional-windows {:size "sample-deal"}}}
      override))
  ([override]
     (make:stock override (make:category))))

(defn make:private-message[from-id to-id text]
  (message:send {:text text :type :private} {:id from-id} {:id to-id}))

(defn make:discussion-tree
  ([tree from to]
     (make:discussion-tree nil tree from to))
  ([reply-to tree from to]
     (cond
       (nil? tree) nil
       (vector? tree) (doall (map #(make:discussion-tree reply-to % from to) tree))
       :else (let [sent (message:send
                         {:text (:text tree)
                          :type :discussion
                          :reply-to reply-to}
                         from to)]
               (make:discussion-tree (:id sent) (:child tree) from to)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stock context functions

(defmacro wrap-organizer-context[user & body]
  `(wrap-security ~user
     (binding [*current-organizer* ~user
               *stock-context* [:organizer]]
       ((stock-context:resolve-types (fn[_#] ~@body)) {}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Retail and supplier functions

(defmacro wrap-supplier[supplier & body]
  `(binding [*current-supplier* ~supplier]
     (do ~@body)))

(defn make:supplier[& {:keys [stock-num stock owner category name user]
                       :or {stock-num 5 stock (constantly {})}
                       :as options}]
  (let [category (or category (make:category))
        supplier (retail:create-supplier (str (or name "Supplier") (get-counter)))]

    (wrap-supplier supplier
      (wrap-context [:wholesale :wholesale-sale :retail :retail-sale]
        (dotimes [i stock-num]
          (stock:save-stock-general (make:stock stock category i)))))

    (when user
      (let [sup-admin (apply make:user (mapcat identity user))]
        (retail:assign-supplier-user sup-admin supplier)))

    supplier))

(defn sup-stocks[supplier]
  (wrap-supplier supplier
    (retail:supplier-stocks nil nil {})))

(defn retail-order-items[retail-order-id]
  (:user-order-item (retail:get-order retail-order-id)))

(defn make:arrival[supplier & {:keys [parameters count price supplier-name]
                               :or {parameters [{:size "m" :color "red"}]
                                    count 50
                                    price 100
                                    supplier-name "supplier"}}]
  (wrap-supplier supplier
    (let [arrival (arrival:create-new-stock-arrival "name" supplier-name)]
      (arrival:save-multiple-stock-arrivals
       arrival (reduce merge
                       (map (fn[{:keys [id]}]
                              {id (map (fn[param]
                                         {:parameters param
                                          :price price
                                          :quantity count})
                                       parameters)})
                            (sup-stocks supplier))))
      arrival)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Cleanup functions

(def schema-already-prepared (atom false))

(defn drop-everything[]
  (when-not @schema-already-prepared
    (doseq [file (.listFiles (as-file "resources/public/img/uploaded"))]
      (delete-file-recursively file true))))

(defn- reinit-schema[mode]
  (let [already-prepared (if (= mode :force) false @schema-already-prepared)]
    (if (and already-prepared
             (or (= "test" (System/getProperty "migration.mode"))
                 (= :fast mode)))
      (with-out-str
        (doseq [tbl (keys (init-metadata))]
          (try
            (exec-raw [(str "alter sequence \"" (name tbl) "_id_seq\" restart with 1")])
            (exec-raw [(str "truncate \"" (name tbl) "\" cascade")] :results)
            (catch Exception e))))
      (binding [*migrations-namespace* 'gd.model.migrations.schema
                *migrations-table* :schema-migrations]
        (with-out-str
          (doseq [tbl (keys (init-metadata))]
            (try
              (exec-raw [(str "drop table \"" (name tbl) "\" cascade")])
              (catch Exception e))))
        (migrate)
        (reset! schema-already-prepared true)))))

(defn clean-db[& [mode]]
  (when (options/dev-mode?)
    (clear-sessions)
    (reinit-schema mode)
    (drop-everything)
    (clear-queue notification-channel 16)
    (clear-queue images-queue 128)))

(defn clean-db-safe[& [mode]]
  (when (options/dev-mode?)
    (reinit-schema mode)))

(defn clean-scheduler[]
  (overtone.at-at/stop-and-reset-pool! gd.model.model/schedule-execution-pool :strategy :kill)
  (reset! last-executed-scheduled-task {}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Enabling dev mode for noir
(defn enable-dev-mode[]
  (alter-var-root #'noir.options/*options* (constantly {:mode :dev})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Redef multimethods

(defmacro with-redef-multimethods[method new-map & body]
  `(let [old-map# (methods ~method)
         restore-old# (fn[]
                        (doseq [[value# function#] old-map#]
                          (defmethod ~method value# [& args#] (apply function# args#))))]
     (remove-all-methods ~method)

     (try
       (doseq [[value# function#] ~new-map]
         (defmethod ~method value# [& args#] (apply function# args#)))

       ~@body

       (finally
         (remove-all-methods ~method)
         (restore-old#)))))
