(ns gd.model.test.unsafe
  (:use gd.model.test.sample
        gd.model.test.data_builders))

(defn UNSAFE:make-sample-data[]
  (clean-db-safe)
  (make-sample-data))
