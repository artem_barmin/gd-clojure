(in-ns 'gd.model.model)

(declare retail:order-sum)
(declare retail:order-diff-since-last-visit)
(declare additional-expense:compute-entity-expense)
(declare retail:category-as-stock-name)
(declare analytics:annotate-left-stocks)
(declare analytics:hide-invisible-params)
(declare additional-expense:compute-expense)
(declare notify-about-new-order)
(declare notify-about-order-change)

(defentity additional-expense
  (enum additional-expense-type [:type]
        :discount                       ;скидка по заказу
        :payment-expenses               ;расходы на оплату(включаются в сумму заказа), например обналичка 1%
        :return-expenses ;расходы по возврату(сопутсвующие - пересылка, компенсация стоимости)
        :return-stocks   ;возвраты товаров(количество возвращенных товаров)
        :delivery-expenses              ;расходы на доставку
        )
  (belongs-to user-order-item {:fk :fkey_user-order-item}))

(defentity retail-order
  (enum retail-order-status [:status]
        :created                        ;только создан
        :in-work                        ;обрабатывается менеджером
        :processed                      ;обработан менеджером
        :shipping                       ;в доставке
        :canceled                       ;отменен
        :closed                         ;закрыт
        :merged                         ;слит с другим заказом(эквивалентно закрытию, не должен отображаться)
        :payed-cash                     ;оплата наличными
        :payed-clearing                 ;оплата по безналу
        :prepared-to-work               ;собран и ждет подтверждения от пользователя
        )
  (json-data :additional-info)
  (transform #'retail:order-diff-since-last-visit)
  (transform #'retail:order-sum)
  (transform #'additional-expense:compute-entity-expense)
  stock-context:resolve-entity-contexts
  (belongs-to user {:fk :fkey_user})
  (has-many additional-expense {:fk :fkey_retail-order})
  (has-many user-order-item {:fk :fkey_retail-order}))

(defentity supplier
  (has-many user {:fk :fkey_supplier})
  (array-transform :uploaded-images)
  (json-data :parser-info)
  (json-data :dictionary))

(defentity category-annotation
  (array-transform :images)
  (belongs-to category)
  (belongs-to supplier))

(declare domain)

(defentity domain-host
  (belongs-to domain {:fk :fkey_domain}))

(defentity domain
  (belongs-to supplier)
  (has-many domain-host {:fk :fkey_domain})
  (json-data :info)
  (array-transform :domains))

(extend-entity user-order-item
  (has-many additional-expense {:fk :fkey_user-order-item})
  (belongs-to retail-order {:fk :fkey_retail-order}))

(extend-entity user
  (belongs-to supplier {:fk :fkey_supplier}))

(defn retail:order-sum[{:keys [user-order-item] :as order}]
  (let [should-not-be-discounted (comp :skip-discount :meta-info :stock)
        can-discounted (remove should-not-be-discounted user-order-item)
        cant-discounted (filter should-not-be-discounted user-order-item)
        compute-sum (fn[items] (reduce + (map :sum items)))]
    (assoc order
      :sum-can-discounted (compute-sum can-discounted)
      :sum-cant-discounted (compute-sum cant-discounted)
      :sum (compute-sum user-order-item))))

(def order-statuses-allowed-to-modify [:created :in-work :prepared-to-work])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Compute diff since last order change(what items added, what deleted and what changed)

(defn retail:order-diff-since-last-visit[{:keys [last-viewed user-order-item] :as order}]
  (let [type-of-item
        (fn[{:keys [created changed deleted]}]
          (cond
            (and
             (not deleted)
             (or (not changed) (= created changed))
             (after? (tm/from-sql-date created)
                     (tm/from-sql-date last-viewed)))
            :added

            (and
             changed
             (not deleted)
             (after? (tm/from-sql-date changed)
                     (tm/from-sql-date last-viewed)))
            :changed

            (and
             deleted
             (after? (tm/from-sql-date (or changed created))
                     (tm/from-sql-date last-viewed)))
            :deleted

            true
            :not-processed))]
    (assoc order
      :diff (dissoc (group-by type-of-item user-order-item) :not-processed)
      :user-order-item (remove :deleted user-order-item))))

(defn retail:update-last-viewed[order-id]
  (update retail-order (set-fields {:last-viewed (sql-now-timestamp)}) (where {:id order-id})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Retail order management

(defn- retail:existing-order-for-user
  "Retrieve already existing order for current user(check only some statuses), and
allows merging only registered orders"
  []
  (select-single-null retail-order
    (where {:fkey_user (sec/id)
            :fkey_supplier *current-supplier*
            :status [in (map retail-order-status order-statuses-allowed-to-modify)]})))

(declare analytics:get-left-quantity-stocks)
(declare special-arrival:save)

(defn- retail:order-recompute-closed[retail-order-id]
  (let [order-items (select user-order-item
                      (with stock)
                      (with stock-parameter-value)
                      (where {:fkey_retail-order retail-order-id}))
        available-quantity (analytics:get-left-quantity-stocks (map :fkey_stock order-items))]

    (doseq [{:keys [original-quantity quantity id stock-parameter-value
                    fkey_stock closed stock]
             :as item} order-items]
      (let [params (sort (map :id stock-parameter-value))
            current-quantity (get available-quantity [fkey_stock params])
            left-quantity (+ quantity current-quantity)
            new-closed (let [left-after-item (- left-quantity original-quantity)]
                         (if (< left-after-item 0)
                           (min (* -1 left-after-item) original-quantity)
                           0))]
        (case (:accounting stock)

          ;; close items that not available
          :warehouse
          (when (not= closed new-closed)
            (info "Close items for item:"
                  (select-keys item [:params :fkey_stock :id])
                  "old-closed:" closed "new-closed:" new-closed
                  "current-quantity:" current-quantity
                  "left-quantity:" left-quantity
                  "quantity:" quantity
                  "original-quantity:" original-quantity)
            (update user-order-item
              (set-fields {:closed new-closed})
              (where {:id id})))

          ;; add unavailable items to the special arrival
          :simple
          (let [arrival-quantity (- original-quantity left-quantity)]
            (info "Add items to special arrival:"
                  (select-keys item [:params :fkey_stock :id])
                  "closed:" arrival-quantity)
            (special-arrival:save fkey_stock (:params item) arrival-quantity)))))))

(defn- retail:move-order-to-created[retail-order-id]
  (update retail-order
    (set-fields {:status :created})
    (where {:id retail-order-id})))

(defn retail:make-order[order-items & {:keys [additional-info]}]
  (transaction
   (let [exist-order (retail:existing-order-for-user)]
      (let [{:keys [id]} (or
            exist-order
            (insert retail-order (values {:fkey_user (sec/id)
                                          :additional-info additional-info
                                          :fkey_supplier *current-supplier*
                                          :contexts *context-types*
                                          :changed (sql-now-timestamp)
                                          :status :created})))]
       (doseq [item order-items]
         (info "Process user order item" item)
         (orders:save-user-order-item
          id
          (dissoc item :id)
          :parent-order-key :fkey_retail-order))

       ;; immediately fixate price of the stock - so we can check history of sales in future
       (orders:fixate-user-order-item-price {:fkey_retail-order id})

       ;; recompute 'closed' items
       (retail:order-recompute-closed id)

       ;; move order to 'created' status
       (retail:move-order-to-created id)

       (if exist-order
         (notify-about-order-change id)
         (notify-about-new-order id))

       (info "Retail order created" id order-items)
       id))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Order and single order item change

(defn retail:change-order[order-items-with-ids]
  (transaction
    (let [items
          (select user-order-item
            (with stock-parameter-value)
            (with stock)
            (where {:id [in (map :id order-items-with-ids)]}))

          [retail-order-id :as orders]
          (distinct (map :fkey_retail-order items))

          items-map
          (group-by-single :id items)]

      (assert (= 1 (count orders)) "Order items belongs to different retail orders")

      (doseq [{:keys [id quantity] :as item} order-items-with-ids]
        (when (not= quantity (:original-quantity (get items-map id)))
          (info "Process user order item change"
                (select-keys (get items-map id) [:fkey_stock :params :quantity])
                "set"
                quantity)
          (update user-order-item
            (set-fields {:quantity quantity :closed 0})
            (where {:id id}))))

      ;; recompute 'closed' items
      (retail:order-recompute-closed retail-order-id)

      ;; move order to 'created' status
      (retail:move-order-to-created retail-order-id)

      (notify-about-order-change retail-order-id)

      retail-order-id)))

(defn retail:change-order-item[item-id params]
  (retail:change-order [(merge {:id item-id} params)]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Delete order item
(defn retail:delete-order-item[item-id]
  (transaction
    (let [item
          (orders:get-order-item item-id)

          {:keys [fkey_retail-order fkey_stock]}
          (update user-order-item
            (set-fields {:deleted true})
            (where {:id item-id}))

          {:keys [user-order-item]}
          (select-single retail-order
            (with user-order-item (with stock)
              (with stock-parameter-value))
            (where {:id fkey_retail-order}))]
      (special-arrival:save fkey_stock (:params item) (* -1 (:quantity item)))
      (update retail-order
        (set-fields {:status (if (empty? user-order-item) :canceled :created)})
        (where {:id fkey_retail-order})))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extra info change
(defn retail:change-order-additional-info[order-id info & {:keys [merge-fn]
                                                           :or {merge-fn (fn[old new] new)}}]
  (transaction
    (let [{:keys [additional-info]}
          (select-single retail-order (where {:id order-id}))]
      (update retail-order
        (set-fields {:additional-info (merge-with merge-fn additional-info (assoc info :comment (str (:comment info) "." (:comment additional-info) )))})
        (where {:id order-id})))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Retail order list management

(defn- retail-order-full-fetch[]
  (->
   (select* retail-order)
   (with additional-expense)
   (with user-order-item (with stock-parameter-value)
         (with additional-expense)
         (with stock))
   (with user
         (with auth-profile)
         (with user-address)
         (with contact))))

(defn- extended-retail-orders-list[limit-num offset-num {:keys [sort search-string status]}]
  (let [sort (or sort :date)            ;sort key can be passed, but may be nil

        sort-by-user
        (fn[query]
          (-> query
              (order :user.real-name)
              (order :retail-order.id)))

        fts-search
        (fn[query]
          (where-if query search-string
                    {:retail-order.id [in (subselect :retail-order-search
                                                     (fields :fkey_retail-order)
                                                     (where {[:user :contact :stock]
                                                             [search search-string]}))]}))

        status-filter
        (fn[query]
          (if status
            (where query {:retail-order.status [in (map retail-order-status (maybe-seq status))]})
            query))]
    (->
     (select* (retail-order-full-fetch))
     (limit limit-num)
     (offset offset-num)
     (where {:fkey_supplier *current-supplier*})
     (where (not (in :status (map retail-order-status [:canceled :merged]))))
     (modify-if (= sort :user) sort-by-user)
     (modify-if (= sort :date) (order :retail-order.id :desc))
     fts-search
     status-filter
     (count-query))))

(defn retail:active-orders[limit-num offset-num & {:keys [sort search-string status] :as params}]
  (disable-default-where (select (extended-retail-orders-list limit-num offset-num params))))

(defn retail:active-orders-sum[& {:keys [status] :as params}]
  (reduce + (map :sum (select (extended-retail-orders-list nil nil params)))))

(defn retail:get-order[id]
  (disable-default-where (select-single (retail-order-full-fetch) (where {:id id}))))

(defn retail:set-order-status [order-id status]
  (update retail-order
    (set-fields {:status status})
    (where {:id order-id})))

(defn retail:order-next-status[order-id & {:keys [extra-info statuses]}]
  (transaction
    (let [{:keys [status additional-info]}
          (select-single retail-order (where {:id order-id}))

          [_ next-status]
          (drop-while (comp not #{status})
                      (or statuses (values-retail-order-status)))]
      (when-not next-status
        (throwf "Incorrect status chain"))
      (update retail-order
        (set-fields {:status next-status
                     :additional-info (merge additional-info extra-info)})
        (where {:id order-id})))))

(defn retail:get-user-orders
  ([]
  (select (-> (extended-retail-orders-list nil nil {})
              (where {:fkey_user (sec/id)})
              (order :id :desc))))
  ([offset limit]
  (select (-> (extended-retail-orders-list limit offset {})
              (where {:fkey_user (sec/id)})
              (order :id :desc)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Additional expenses

(defn additional-expense:assign-or-update[entity-id entity-type expenses]
  (transaction
    (let [check {(case entity-type
                   :retail-order :fkey_retail-order
                   :user-order-item :fkey_user-order-item) entity-id}]

      (delete additional-expense (where check))
      (doseq [{:keys [type money percent items] :as expense} expenses]
        (when-not (case type
                    (:payment-expenses :discount) percent
                    (:return-expenses :delivery-expenses) money
                    :return-stocks items)
          (throwf "For type %s needed field not defined %s" type expense))
        (insert additional-expense (values (merge check expense)))))))

(def expense-priorities (indexed [:discount :payment-expenses]))

(defn- sort-by-priorities[expenses]
  (sort-by (fn[{:keys [type]}]
             (or (first (find-by-val second type expense-priorities)) Long/MAX_VALUE))
           expenses))

(defn- percent-from-sum[sum percent]
  (if percent
    (Math/round (float (* sum (/ percent 100))))
    0))

(defn additional-expense:compute-entity-expense[{:keys [additional-expense] :as entity}]
  (->>
   additional-expense
   sort-by-priorities
   (reduce (fn[res expense]
             (additional-expense:compute-expense expense res))
           entity)))

(defmulti additional-expense:compute-expense
  (fn[expense entity] [(cond
                         (:fkey_retail-order expense) :retail-order
                         (:fkey_user-order-item expense) :user-order-item)
                       (:type expense)]))

(defmethod additional-expense:compute-expense [:retail-order :discount]
  [{:keys [percent]}
   {:keys [sum original-sum sum-can-discounted sum-cant-discounted] :as order}]
  (let [discount (percent-from-sum sum-can-discounted percent)]
    (assoc order
      :original-sum (or original-sum sum)
      :discount discount
      :sum-with-discount (- sum discount)
      :sum (- sum discount))))

(defmethod additional-expense:compute-expense [:retail-order :return-expenses] [_ order]
  order)

(defmethod additional-expense:compute-expense [:retail-order :delivery-expenses] [_ order]
  order)

(defmethod additional-expense:compute-expense [:retail-order :payment-expenses]
  [{:keys [percent]}
   {:keys [sum original-sum] :as order}]
  (let [expense (percent-from-sum sum percent)]
    (assoc order
      :payment-expenses expense
      :original-sum (or original-sum sum)
      :sum (+ sum expense))))

(defmethod additional-expense:compute-expense [:user-order-item :return-stocks] [{:keys [items]} item]
  (update-in item [:quantity] - items))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Suppliers

(defn retail:create-supplier[name]
  (info "Supplier created" name)
  (:id (insert supplier (values {:name name}))))

(defn retail:supplier-users
  "Returns current users associated for supplier(admins)"
  []
  (when *current-supplier*
    (select user (where {:fkey_supplier *current-supplier*}))))

(defn retail:supplier-list
  "Returns current users associated for supplier(admins)"
  []
  (select supplier
    (count-query)))

(defn retail:assign-supplier-user
  "Associate selected user with specified supplier"
  [user-id supplier-id]
  (update user
    (set-fields {:fkey_supplier supplier-id})
    (where {:id user-id})))

(declare stock-context-type)

(defn retail:get-all-partners[limit-num offset-num]
  (let [partner
          (:id (select-single-null role (where {:name "partner"})))

          group-by-supplier
          (fn[results]
            (->> results
                 (group-by-single :fkey_supplier)
                 (fmap :cnt)))

          stocks-count
          (group-by-supplier (select stock
                               (join [:stock-context-statuses :scs] (= :scs.id :id))
                               (where {:scs.type [in (map stock-context-type [:wholesale :retail])]
                                       :scs.status (stock-status :visible)})
                               (group :fkey_supplier)
                               (fields "count(distinct stock.id) as cnt" :fkey_supplier)))

          orders-count
          (group-by-supplier (select retail-order
                               (group :fkey_supplier)
                               (aggregate (count :*) :cnt)
                               (fields :fkey_supplier)))

          users-count
          (group-by-supplier (select user
                               (join :stock-context (contains :contexts [:stock-context.id]))
                               (group :stock-context.fkey_supplier)
                               (fields "count(distinct \"user\".id) as cnt" :stock-context.fkey_supplier)))]
      (-> (select (user-full-fetch)
            (with supplier)
            (limit limit-num)
            (offset offset-num)
            (where {:user.id [in (subselect
                                  :user-with-role
                                  (fields :user-key)
                                  (where {:role-key partner}))]})
            (order :id)
            (count-query))
          (annotate-map-from-sql stocks-count :stocks :entity-key :fkey_supplier)
          (annotate-map-from-sql users-count :users :entity-key :fkey_supplier)
          (annotate-map-from-sql orders-count :orders :entity-key :fkey_supplier))))

(defn retail:get-partner[id]
  (select-single (user-full-fetch)
    (with supplier)
    (where {:id id})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; User view stock management

(declare stock-arrival)
(declare stock-arrival-type)
(declare retail:supplier-stocks)

(defn- retail:new-stocks[]
  (union
   (subselect stock
              (fields-only :id)
              (where {:show-until [> (sql-now-timestamp)]}))

   (subselect stock-arrival-parameter
              (fields-only :fkey_stock)
              (join stock-arrival)
              (where {:stock-arrival.created [> (tm/to-timestamp (-> 2 days ago))]
                      :stock-arrival.type (stock-arrival-type :arrival)}))))

(defn retail:supplier-stocks[limit-num offset-num {:keys [only-id] :as params}]
  (let [fetch-from-db
        (fn[ids]
          (->
           (extended-stocks-list limit-num offset-num (merge params {:selection-type :supplier
                                                                     :limit-ids ids
                                                                     :only-id (empty? ids)
                                                                     :supplier *current-supplier*}))
           (modify-if (:sort-key params)
              (order
                (:sort-key params)
                (or (:sort params) :asc)))
           visible-stocks
           (where-if (:new params) {:stock.id [in (retail:new-stocks)]})
           (count-query)
           select
           (modify-if (seq ids) analytics:hide-invisible-params)))]
    (->
     (fetch-from-db nil)
     (modify-if (not only-id) (retrieve-cached :stock-view-db-value :stock fetch-from-db)))))

(defn retail:get-multiples-stocks[ids]
  (analytics:hide-invisible-params (stock:get-multiple-stock ids)))

(defn retail:get-stock[id]
  (first (retail:get-multiples-stocks [id])))

(defn retail:get-admin-stock[id]
  (first
   (analytics:annotate-left-stocks
    (select (->
             (select* stock)
             (stock-full-fetch)
             (with stock-arrival-parameter
                 (with stock-arrival)
               (with stock-parameter-value))
             (where {:stock.id id}))))))

(defn- retail:sale-stocks[]
  (retail:supplier-stocks
   nil nil {:context-status {:wholesale-sale :visible
                             :wholesale :visible}
            :only-id true}))

(defn- retail:search-stocks[search-string]
  (retail:supplier-stocks nil nil {:only-id true :search-string search-string}))

(let [prepare-params (fn[{:keys [mode search] :as params}]
                       (merge {:limit-ids (case mode
                                            :new (map :id (select [(retail:new-stocks) :new]))
                                            :search (map :id (retail:search-stocks search))
                                            :sale (map :id (retail:sale-stocks))
                                            nil)}
                              params))]
  (defn retail:get-user-stock-params[& [params]]
    (meta:get-filters-for-stocks (prepare-params params)))

  (defn retail:get-user-stock-params-count[& [params]]
    (meta:get-counts-for-stocks (prepare-params params))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Retail admin stock management
(declare event:get-event-maps)

(defn retail:admin-supplier-stocks[limit-num offset-num {:keys [only-id] :as params}]
  (let [fetch-from-db
        (fn[ids]
          (->
           (extended-stocks-list
            limit-num offset-num
            (merge params {:selection-type :supplier
                           :supplier *current-supplier*
                           :limit-ids ids
                           :only-id (empty? ids) ;we request only ids when no specific stocks passed to fetch
                           :show-invisible true}))
           (order :name)
           (count-query)
           select
           (modify-if (not only-id) analytics:annotate-left-stocks)))]
    (->
     (fetch-from-db nil)
     (modify-if (not only-id) (retrieve-cached :stock-db-value :stock fetch-from-db)))))

(defn retail:get-stock-params[]
  (meta:get-filters-for-stocks {}))

(declare stock-context:resolve-type)

(defn retail:set-retail-price[stock-ids {:keys [max-price min-price change-percent]}]
  (let [stocks-to-process
        (let [effective-contexts [:wholesale :wholesale-sale]]
          (binding [*context-types* effective-contexts
                    *stock-context* (map! stock-context:resolve-or-create-type effective-contexts)]
            (doall (retail:admin-supplier-stocks nil nil {:limited-ids stock-ids}))))

        compute-new-price
        (fn[{:keys [price params] :as variant}]
          (when price
            (let [price-change (float (* (/ change-percent 100) price))]
              {:price (+ price (cond (> price-change max-price) max-price
                                     (< price-change min-price) min-price
                                     true price-change))
               :params params})))]
    (transaction
      ;; delete all previous retail stock variants for given set of stocks
      (let [old-variants
            (map :id (select stock-variant
                       (where {:fkey_stock-context (stock-context:resolve-type :retail)
                               :fkey_stock [in stock-ids]})))]
        (delete :stock-parameter-variants (where {:stock-variant-key [in old-variants]}))
        (delete stock-variant (where {:id [in old-variants]})))
      ;; generate new variant for each stock and save it
      (doseq [{:keys [id stock-variant stock-parameter-value] :as stock} stocks-to-process]
        (doseq [{:keys [params price]} (remove nil? (map compute-new-price (vals stock-variant)))]
          (stock-variant:store-single-variant
           id stock-parameter-value params {:price price :context :retail}))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Categories annotations(additional images, specific for supplier)

(defn retail:annotate-categories[categories-list]
  (let [annotations
        (select category-annotation (where {:fkey_category [in (map :id categories-list)]
                                            :fkey_supplier *current-supplier*}))

        annotations
        (group-by-single :fkey_category annotations)]
    (map (fn[cat] (assoc cat :annotation (get annotations (:id cat)))) categories-list)))

(defn retail:add-or-update-annotation[category-id text images]
  (upset category-annotation
         (merge
          (when text {:comment text})
          (when images {:images images})
          {:fkey_category category-id
           :fkey_supplier *current-supplier*})
         {:fkey_category category-id
          :fkey_supplier *current-supplier*}))


(defn retail:change-order-user [old-user-id new-user-id]
  (transaction
    (update retail-order
            (set-fields {:fkey_user  (Integer. (re-find  #"\d+" new-user-id ))})
            (where {:fkey_user old-user-id}))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Merge multiple orders

(defn retail:merge-orders
  "Merge all orders into first order. All orders should belongs to the same
user."
  [retail-order-ids]
  (transaction
    (let [[target & sources] (sort retail-order-ids)]
      ;; check that all orders belongs to same user
      (let [used-users
            (-> (map :fkey_user
                     (select retail-order
                       (fields-only :fkey_user)
                       (where {:id [in retail-order-ids]})))
                distinct)]
        (when (or (not= 1 (count used-users)) (some nil? used-users))
          (throwf "Orders are belongs to different users, or unregistered")))

      ;; move order items to target retail order
      (let [items
            (select user-order-item
              (with stock (with stock-parameter-value))
              (with stock-parameter-value)
              (where {:fkey_retail-order [in retail-order-ids]}))

            order-used-context
            (set (map (comp stock-context:resolve-id-to-type :fkey_stock-context) items))]

        (when (and (seq (set/intersection order-used-context #{:wholesale :wholesale-sale}))
                   (seq (set/intersection order-used-context #{:retail :retail-sale})))
          (throwf "Orders are made in non compatible contexts - can't merge"))

        (doseq [[{:keys [params fkey_stock]} items]
                (group-by (fn[m](select-keys m [:fkey_stock :params])) items)]

          (let [used-context (distinct (map :fkey_stock-context items))]

            ;; check that all items that we want to merge - have same context(so we can merge
            ;; order that have :wholesale and :wholesale-sale items), but will not merge
            ;; retail and wholesale orders
            (when (or (not= 1 (count used-context)) (some nil? used-context))
              (throwf "Order items are made in different contexts - can't merge"))

            (info "Merging order items" {:params params :stock fkey_stock}
                  "effective context:" (stock-context:resolve-id-to-type (first used-context)))

            ;; check if target item exists - then merge all other items into it
            ;; othwerwise - create new order item
            (let [id (or (:id (find-by-val :fkey_retail-order target items))
                         (orders:save-user-order-item
                          target (merge params {:fkey_stock fkey_stock
                                                :quantity 0})
                          :parent-order-key :fkey_retail-order
                          :used-context (first used-context)))]
              (update user-order-item
                (set-fields {:quantity (reduce + (map :original-quantity items))
                             :closed (reduce + (map :closed items))})
                (where {:id id}))

              ;; mark all other items as deleted
              (update user-order-item
                (set-fields {:deleted true})
                (where {:id [in (map :id items)]})
                (where (not (= :id id))))))))

      ;; write to Note what orders was merged
      (retail:change-order-additional-info
       target
       {:note (str "Объединены заказы №" (strings/join ", " sources))}
       :merge-fn (fn[old new] (str old "\n" new)))

      ;; mark target order as 'created'
      (update retail-order
        (set-fields {:status :created})
        (where {:id target}))

      ;; mark all other orders as closed
      (update retail-order
        (set-fields {:status :merged})
        (where {:id [in sources]})))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Reviews

(defn retail:send-review[text]
  (message:send {:text text :type :supplier-review :removed true} (sec/logged) {:id *current-supplier*}))

(defn retail:send-review-admin[msg from]
  (message:send msg from {:id *current-supplier*}))

(defn retail:get-review[limit offset]
  (message:messages {:id *current-supplier*} :inbox :supplier-review
                    {:offset-num offset :limit-num limit}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Uploaded images

(defn retail:save-supplier-images[images]
  (transaction
    (let [{:keys [uploaded-images]} (select-single supplier (where {:id *current-supplier*}))]
      (update supplier
        (set-fields {:uploaded-images (set images)})
        (where {:id *current-supplier*})))))

(defn retail:supplier-images[]
  (:uploaded-images (select-single supplier (where {:id *current-supplier*}))))
