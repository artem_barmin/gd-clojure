(in-ns 'gd.model.model)

(def stock-image-sizes [nil :deal-huge :stock-very-large :stock-large :stock-medium
                        :stock-deals :stock-bucket :stock-bono])

(def model-url-validator (new org.apache.commons.validator.routines.UrlValidator (into-array ["http" "https"])))

(declare stock)
(declare stock-arrival-parameter)
(declare stock-arrival)
(declare stock-overwrite)
(declare stock:parameters-for-choosing)
(declare stock-context:process-stock)
(declare stock-context:process-main-properties)

;; Диаграмма состояний товара:
;;
;; новые товары(в сессии):
;; new <-> invisible. При сохранении new становится visible
;;
;; товары в базе new -> (visible <-> invisible)
;; new - может поставить только парсер. Пользователь может перевести его в
;; видимый/невидимый и после перевода только переключаться между ними
;;
;; товар со статусом :new можно спокойно удалять - т.к. его никто не видел и не мог заказать

(defentity stock
  (enum stock-status [:status]
        :visible                   ;visible to all users
        :invisible                 ;will not showed on the list
        :not-prepared              ;not yet processed stock(invisible for users)
        :available ;used for available stocks. Will be showed only by special filters
        :deleted   ;will not showed in all cases
        )
  (enum stock-update-behavior [:stock-update-behavior]
        :normal                         ;can be deleted, updated or restored
        :dont-delete                    ;can't be deleted
        :dont-restore ;can't be restored(used for merged stocks that marked as invisible)
        :dont-update  ;don't update params after change
        )
  (transform #'stock-variant:prepare-stock-variants)
  (transform #'stock-context:process-stock)
  (transform #'stock-context:process-main-properties)
  (array-transform :images)
  (array-transform :tags
                   :val-transform keyword
                   :as-set true
                   :val-prepare name)
  (has-many stock-parameter-value {:fk :fkey_stock})
  (has-many stock-variant {:fk :fkey_stock})
  (belongs-to category {:fk :fkey_category})
  (default-fetch stock-variant)

  (json-data :description (fn[d](or (string? d)
                                    (every? string? d)
                                    (map? d))))
  (json-data :meta-info #(and (map? %)
                              (or (not (contains? % :url))
                                  (empty? (:url %))
                                  (.isValid model-url-validator (:url %)))
                              (or (not (contains? % :min-quantity))
                                  (integer? (:min-quantity %))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Transformer for stock - build map of params that enabled for choosing

(defn stock:parameters-for-choosing[{:keys [stock-parameter-value] :as stock}]
  (let [process-params
        (fn process-params
          [only-visible with-visibility]
          (reduce merge
                  (map
                   (fn[[[name visible] values]]
                     {(if with-visibility [name visible] name)
                      (sort (map :value values))})
                   (->> stock-parameter-value
                        (filter (if only-visible :visible (constantly true)))
                        (filter :enabled)
                        (group-by (juxt :name :visible))))))]
    (->
     stock
     (assoc :params (process-params true false))
     (assoc :all-params (process-params false false))
     (assoc :params-with-visibility (process-params false true)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stock operations on list

(declare arrival:stocks-from-arrival)
(declare fetch-arrival-and-order-by-price-existense)

(defn stock-full-fetch[query]
  (-> query
      (with category (with category))
      (with stock-parameter-value (order :id))))

(declare stock-context:context-status-general-filter)

(defn- extended-stocks-list
  "Generate extended search query for stock. Apply all filters."
  [limit-num offset-num {:keys [deal filters search-string
                                only-id show-invisible price
                                category only-available
                                state only-new limited-ids force-limited-ids
                                exclude-ids arrival arrival-session tags
                                context-status at-least-arrived-stocks]
                         :as params}]
  (let [filters (remove-empty-entries filters)

        ;;;;;;;;;;;;;;;;;;;;
        ;; Common filters
        categories-filter
        (fn [query]
          (let [categories (when category
                             (if (sequential? category)
                               category
                               (category:to-filter-by-parent category)))]
            (where-if query category {:stock.fkey_category [in categories]})))

        price-filter
        (fn [query]
          (where-if query
                    (not (or (empty? price) (every? nil? price)))
                    (and (>= :price (first price)) (<= :price (second price)))))

        fts-search
        (fn [query]
          (if (seq search-string)
            (-> query
                (join :inner :stock-search (= :stock-search.fkey_stock :stock.id))
                (where {[:stock-search.deal :stock-search.stock :stock-search.category :stock-search.fkey_stock]
                        [search search-string]}))
            query))

        context-status-filter
        (fn[query]
          (cond (seq context-status)
                (stock-context:context-status-general-filter query context-status :and)

                (and (seq *stock-context*) (not show-invisible))
                (stock-context:context-status-general-filter query nil :or)

                true
                query))

        visible-and-status-filter
        (fn [query]
          (cond
            only-new (where query {:status (stock-status :not-prepared)})
            only-available (where query {:status (stock-status :available)})
            show-invisible (where query {:status [in
                                                  (map stock-status
                                                       (if (seq state)
                                                         state
                                                         (disj (set (values-stock-status)) :available)))]})
            (not show-invisible) (where query {:status (stock-status :visible)})))

        limited-ids-filter
        (fn [query]
          (-> query
              (where-if (or (seq limited-ids) force-limited-ids) {:stock.id [in limited-ids]})
              (where-if (seq exclude-ids) (not (in :stock.id exclude-ids)))))

        tags-filter
        (fn [query]
          (if (seq tags)
            (let [positive (filter string? tags)
                  negative (map :inverse (filter map? tags))]
              (-> query
                  (where-if (seq positive) {:tags [overlaps positive]})
                  (where-if (seq negative) (not (sqlfn :coalesce (overlaps :tags negative) (raw "false"))))))
            query))

        meta-filter
        (fn [query]
          (where-if query (seq (remove-empty-entries filters))
                    {:stock.id [in (meta:get-stocks-with-match filters)]}))

        arrival-filter
        (fn[query]
          (let [fetch-arrival-and-order-by-price-existense
                (partial fetch-arrival-and-order-by-price-existense arrival arrival-session)]
            (->
             query
             (with stock-arrival-parameter
                 (with stock-arrival)
                 (with stock-parameter-value))
             (modify-if arrival fetch-arrival-and-order-by-price-existense))))

        only-id-filter-with-supplier
        (fn [query]
          (if only-id
            (-> query
                (join gd.model.model/supplier (= :supplier.id :stock.fkey_supplier))  ;we should join supplier table to enable filtering
                (fields-only :id)
                (dry-transforms))
            (stock-full-fetch query)))

        at-least-once-arrived-stocks
        (fn [query]
          (if at-least-arrived-stocks
            (-> query
                (where {:id [in (subselect :consumed-arrived-stocks
                                           (fields-only :fkey_stock))]}))
            query))

        supplier-meta-filter
        (fn [query]
          (-> query
              (where {:fkey_supplier *current-supplier*})
              (where-if (seq (remove-empty-entries filters))
                        {:stock.id [in (meta:get-stocks-with-match filters)]})))]
    (->
     (select* stock)
     (where (not= :status (stock-status :deleted)))

     meta-filter
     categories-filter
     visible-and-status-filter
     fts-search
     price-filter
     limited-ids-filter
     tags-filter
     context-status-filter

     (modify-if limit-num (limit limit-num))
     (modify-if offset-num (offset offset-num))

     arrival-filter
     supplier-meta-filter
     at-least-once-arrived-stocks
     only-id-filter-with-supplier)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Single stock select
(defn stock:get-stock[id]
  (select-single (->
                  (select* stock)
                  (stock-full-fetch)
                  (where {:stock.id id}))))

(defn stock:get-multiple-stock[ids]
  (select (->
           (select* stock)
           (stock-full-fetch)
           (where {:stock.id [in ids]}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Save stock

(declare stock-context:save-context-info dictionary:add-properties)

(defn- stock:save-stock
  "Main save function. Should be used by specific containers."
  [container-id {:keys [images additional-stock-window params context stock-variant] :as stock-entity}]
  (transaction
    (let [{:keys [id]}
          (insert stock
            (values
             (-> stock-entity
                 (select-keys [:name :description :price :meta-info :fkey_category :status :tags :accounting])
                 (merge {:fkey_supplier container-id
                         :images (images:save-showcase-to-code images stock-image-sizes)}))))]

      (stock-context:save-context-info id context)
      (meta:save-stock-parameters id params)
      (stock-variant:update-stock-variants id stock-variant)
      (info "Stock created" id (str stock-entity))
      (dictionary:add-properties stock-entity)
      id)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Update stock
(defn- stock:update-stock-params[id params whole-stock]
  (let [group (partial group-by-single (juxt :name :value))
        new-params (->>
                    params
                    (mapcat
                     (fn[[k vals]]
                       (map (fn[val]
                              (let [[name visible] (if (vector? k) k [k true])]
                                {:name (as-str name)
                                 :visible visible
                                 :value (as-str val)
                                 :new true}))
                            vals)))
                    group)
        old-params (group (:stock-parameter-value whole-stock))
        merged-params (vals (merge-with merge old-params new-params))

        new-params (remove :id merged-params)
        removed-params (remove :new merged-params)
        changed-params (filter :id (filter :new merged-params))]

    (doseq [{:keys [name value] :as deleted-param} removed-params]
      (meta:update-stock-parameter (:id deleted-param) {:enabled false}))

    (doseq [{:keys [name value visible]} new-params]
      (meta:save-stock-parameters
       id {[name visible] [value]}))

    (doseq [{:keys [id visible enabled new name value]} changed-params]
      (meta:update-stock-parameter id {:visible visible
                                       :enabled (or enabled new)}))))

(defn- check-next-stock-status[status]
  (when-not (#{:visible :invisible} status)
    (throwf "Can't change stock status to something other :visible and :invisible")))

(defn stock:update-stock[id {:keys [images context stock-variant] :as params}]
  (transaction

    (let [whole-stock (stock:get-stock id)
          main-properties [:name :description :status
                           :quantity :price :stock-update-behavior
                           :images :tags :accounting]]

      ;; update simple properties
      (let [fields-to-update
            (merge (when-let [cat (:fkey_category params)]
                     {:fkey_category cat})
                   (when (:meta-info params)
                     (let [{:keys [meta-info]} whole-stock]
                       {:meta-info (merge meta-info (:meta-info params))}))
                   (when-not (empty? (select-keys params main-properties))
                     (when-let [status (:status main-properties)]
                       (check-next-stock-status status))
                     (merge (select-keys params main-properties)
                            (when-let [images (seq (images:save-showcase-to-code images stock-image-sizes))]
                              {:images images}))))]
        (when (seq fields-to-update)
          (update stock
            (set-fields fields-to-update)
            (where {:id id}))))

      ;; update context info
      (stock-context:save-context-info id context)

      ;; update meta params
      (when (contains? params :params)
        (stock:update-stock-params id (:params params) whole-stock))

      ;; update variants - should be after params update, becase links to params
      (when (contains? params :stock-variant)
        (stock-variant:update-stock-variants id stock-variant))
      (dictionary:add-properties params))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Hard stock deletion

(defn- delete-stock-from-db
  "We are allowed only delete stock-parameters, images, and actual stock entity.
          Any other associations and tables must not be touched. In case of
          programmer error, orders, ratings will be kept in consistent state due
          to database constraints."
  [id]
  (transaction
    (delete stock-parameter-value (where {:fkey_stock id}))
    (delete stock-overwrite (where {:fkey_stock id}))
    (delete stock (where {:id id}))))

(defn stock:delete-stock[id]
  (delete-stock-from-db id))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Soft stock deletion(mark as deleted and move to 'other' category)

(defn stock:soft-delete[id]
  (let [other (:id (category:get-by-key "other"))]
    (stock:update-stock id {:status :deleted
                            :fkey_category other})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Multiple stock merge into one (merge photos and keep all other stocks)
(defn stock:merge-stocks[ids]
  (let [duplicated-images (->>
                           (select stock (where {:id [in ids]}))
                           (mapcat :images))]
    (stock:update-stock (first ids) {:images duplicated-images})
    (doseq [id (rest ids)]
      (stock:update-stock id {:status :deleted
                              :stock-update-behavior :dont-restore}))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Specific aggregated views of stock list

(defn stock:max-min-price[& {:keys [] :as params}]
  (->
   (extended-stocks-list nil nil params)
   (fields-only)
   (aggregate "max(stock.price)" :max)
   (aggregate "min(stock.price)" :min)
   dry-transforms
   select-single))

(defn stock:container-categories-tree[params]
  (let [categories
        (category:get-by-ids (map :id
                                  (->
                                   (extended-stocks-list nil nil params)
                                   (fields-only :category.id)
                                   (modifier "distinct")
                                   dry-transforms
                                   select)))]
    (category:build-tree-path
     (->>
      categories
      (mapcat (fn[cat](cons cat (category:parents (:id cat)))) )
      distinct) nil)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; General stock saving - multiplexes save based on the context

(defn stock:save-stock-general[stock-entity & [deal-id]]
  (stock:save-stock *current-supplier* stock-entity))

(defn stock:set-as-new [stock-ids]
  (map! #(update stock (set-fields {:show-until (tm/to-timestamp (-> 2 days from-now))}) (where {:id %})) stock-ids))
