(in-ns 'gd.model.model)

(defonce schedule-execution-pool (at/mk-pool :cpu-count 2))

(declare scheduled-events)

(defentity scheduled-events-results
  (json-data :info)
  (nippy-data :data)
  (enum scheduled-events-result-type [:status]
        :success
        :error)
  (belongs-to scheduled-events {:fk :fkey_scheduled-events}))

(defentity scheduled-events
  (enum scheduled-event-type [:type]
        :compute-reports)
  (enum scheduled-status [:status]
        :started
        :paused)
  (enum scheduled-delay [:delay]
        :at-night
        :at-start-of-hour
        :now)
  (json-data :data)
  (belongs-to supplier {:fk :fkey_supplier :entity :association})
  (has-many scheduled-events-results {:fk :fkey_scheduled-events}))

(defn- generate-map-for-event-values[val]
  (reduce merge (map (fn[v] {v val})(values-scheduled-event-type))))

(defmulti schedule:execute-job (comp first vector))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Preferences specific to type of task

(def type-to-association {[:compute-reports] supplier})

(def scheduled-task-mutexes (generate-map-for-event-values (new ReentrantLock)))

(def offsets-for-tasks (generate-map-for-event-values (* 60 1000)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main functions

(defn- schedule-type-to-entity[type]
  (maybe-deref-delayed (second (find-first (fn[[keys]] ((set keys) type)) type-to-association))))

(defn- schedule-fkey-by-type[type]
  (fkey-for-relation scheduled-events (schedule-type-to-entity type)))

(def common-periods {:every-hour (* 60 60)
                     :every-2hour (* 2 60 60)
                     :every-3hour (* 3 60 60)
                     :every-4hour (* 4 60 60)
                     :every-day (* 24 60 60)})

(defn schedule:find-common-period[period]
  (some (fn[[common value]] (if (= value period) common)) common-periods))

(def last-executed-scheduled-task (atom {}))

(defn- job-id-to-task-id[id]
  (:id (first (filter (comp (partial = id) :id :desc) (at/scheduled-jobs schedule-execution-pool)))))

(defn- schedule:every
  "initial-delay - delay in msec"
  [id period type data delay & [initial-delay]]
  (locking schedule-execution-pool
    (let [association ((schedule-fkey-by-type type)
                       (select-single scheduled-events (where {:id id})))

          delay
          (+
           (or initial-delay 0)
           (let [start-time
                 (case delay
                   :at-night (plus (today-at-midnight) (days 1))
                   :at-start-of-hour (plus
                                      (-> (now)
                                          (.withMinuteOfHour 0)
                                          (.withSecondOfMinute 0)
                                          (.withMillisOfSecond 0))
                                      (hour 1))
                   nil)]
             (if start-time (in-msecs (interval (now) start-time)) 0)))

          reschedule
          (fn[fun delay]
            (at/stop (job-id-to-task-id id) schedule-execution-pool)
            (at/every (* 1000 period) fun schedule-execution-pool
                      :desc {:id id :type type}
                      :initial-delay delay))

          reschedule-time
          (fn[]
            (let [start (type @last-executed-scheduled-task)
                  passed-time (and start (in-msecs (interval (tm/from-date start) (now))))
                  result (and start (- (or (type offsets-for-tasks) 0) passed-time))]
              (or result 0)))

          execution-block
          (fn[]
            (let [start (System/currentTimeMillis)]
              (try
                (info "Execute scheduled task" type (or association "nil") data)
                (schedule:execute-job type data association)
                (insert scheduled-events-results
                  (values {:fkey_scheduled-events id
                           :status :success
                           :time (sql-now-timestamp)}))
                (info "Task " type (or association "nil") data
                      "executed in " (float (/ (- (System/currentTimeMillis) start) 1000)) " secs.")
                (catch Throwable e
                  (error e)
                  (insert scheduled-events-results
                    (values {:fkey_scheduled-events id
                             :status :error
                             :data (serialize e)
                             :time (sql-now-timestamp)})))
                (finally
                  (swap! last-executed-scheduled-task assoc type (sql-now-timestamp))
                  (update scheduled-events
                    (set-fields {:last-execution-time (sql-now-timestamp)})
                    (where {:id id}))))))

          task
          (at/every (* 1000 period)
                    (fn execution[]
                      (try
                        ;; try to lock task mutex
                        (if (.tryLock (type scheduled-task-mutexes))
                          (try
                            (let [delay-time (reschedule-time)]
                              (if (> delay-time 0)
                                ;; if mutex is locked - check time of last execution of this task,
                                ;; if it was too close - reshedule on the later time
                                (reschedule execution delay-time)
                                ;; otherwise - execute task, it must execute in blocking mode, so
                                ;; other tasks will wait until this is finished
                                (execution-block)))
                            (finally
                              (.unlock (type scheduled-task-mutexes))))
                          ;; we can't lock task mutex - reshedule on the later time
                          (reschedule execution (or (type offsets-for-tasks) (* 60 1000))))
                        (catch Throwable e
                          (error e))))
                    schedule-execution-pool
                    :desc {:id id :type type}
                    :initial-delay delay)]
      (info "Scheduled job" type data (with-out-str (println task))))))

(defn schedule:stop[id]
  (transaction
    (when-let [task-id (job-id-to-task-id id)]
      (update scheduled-events
        (set-fields {:status :paused})
        (where {:id id}))
      (at/stop task-id schedule-execution-pool))))

(defn schedule:start-or-update[period type data association & {:keys [delay]}]
  (transaction
    (let [period (if (keyword? period) (period common-periods) period)]
      (if-let [[{:keys [id]}] (select-null scheduled-events (where {(schedule-fkey-by-type type) association
                                                                    :type (scheduled-event-type type)}))]
        (do
          (when-let [task-id (job-id-to-task-id id)]
            (at/stop task-id schedule-execution-pool))
          (update scheduled-events
            (set-fields {:period period
                         :data data
                         :status :started
                         :delay delay})
            (where {:id id}))
          (schedule:every id period type data delay))
        (if period
          (let [{:keys [id] :as obj} (insert scheduled-events
                                       (values {:period period
                                                (schedule-fkey-by-type type) association
                                                :type type
                                                :last-execution-time (tm/to-timestamp (tm/from-long 0))
                                                :data data
                                                :status :started
                                                :delay delay}))]
            (schedule:every id period type data delay))
          (schedule:execute-job type data association))))))

(defn schedule:get-by-type-and-entity[entity-id type]
  (first
   (select-null scheduled-events
                (where {(schedule-fkey-by-type type) entity-id
                        :type (scheduled-event-type type)
                        :status (scheduled-status :started)}))))

(defn schedule:get-by-type[type]
  (select scheduled-events
    (where {:type (scheduled-event-type type)
            :status (scheduled-status :started)})))

(defn schedule:all-tasks[]
  (let [real-jobs (group-by-single (comp :id :desc) (at/scheduled-jobs schedule-execution-pool))]
    (map
     (fn[{:keys [id type] :as event}]
       (assoc event
         :scheduled-info (get real-jobs id)
         :association (when-let [key-val ((schedule-fkey-by-type type) event)]
                        (select-single (schedule-type-to-entity type)
                          (where {:id key-val})))))
     (select scheduled-events (order :id)))))

(defn schedule:results[limit-num offset-num]
  (select scheduled-events-results
    (with scheduled-events)
    (limit limit-num)
    (offset offset-num)
    (order :time :desc)
    (count-query)))

(defn schedule:get-execution-results[id]
  (select scheduled-events-results (where {:fkey_scheduled-events id})))

(defn schedule:restore-timers
  "Fetch all data from table and restart timers."
  []
  ;; (doseq [{:keys [delay id period type data last-execution-time] :as event}
  ;;         (select scheduled-events (where {:status (scheduled-status :started)}))]
  ;;   (let [last-time (tm/to-long last-execution-time)
  ;;         now-time (tm/to-long (sql-now-timestamp))
  ;;         next-execution (+ last-time (* period 1000))
  ;;         initial-delay (- next-execution now-time)]
  ;;     (schedule:every id period type data delay
  ;;                     (when (> initial-delay 0) initial-delay))))
  )
