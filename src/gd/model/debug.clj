(in-ns 'gd.model.model)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils for debugging mat view(launch check every 30 minutes and compare mat-view with original query)

(def mat-view-debuggers (atom nil))

(defn- mat-view-compute-diff[query-fn table-fn]
  (set/difference (set (query-fn)) (set (table-fn))))

(defn- mat-view-check-diff[name query-fn table-fn]
  ;; (let [difference-with-mat-view (mat-view-compute-diff query-fn table-fn)]
  ;;   (info "Check mat view diff" name)
  ;;   (doseq [diff difference-with-mat-view]
  ;;     (info diff))
  ;;   (when (> (count difference-with-mat-view) 0)
  ;;     (doseq [email ["artem.barmin@gmail.com" "koval111vladislav@gmail.com"]]
  ;;       (let [info (str "Mat view diff not empty: " name)]
  ;;         (send-mail email info info)))))
  )

(defn- make-mat-view-debugger[name query-fn table-fn]
  (.start
   (new Thread (fn[]
                 (let [check (fn[] (mat-view-check-diff name query-fn table-fn))]
                   (try
                     (swap! mat-view-debuggers assoc name check)
                     (while true
                       (check)
                       (Thread/sleep (* 30 60 1000)))
                     (catch Exception e
                       (error e))))))))

(defn- force-check[name]
  ((name @mat-view-debuggers)))
