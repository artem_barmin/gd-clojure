(in-ns 'gd.model.model)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sale report

(comment "Расчет отчета идет по следующему алгоритму:
1. Выгребаем все приходы по товарам начиная с начала
2. Выгребаем все продажи по товарам начиная с начала
3. Расходуем приходы начиная с первого

Товар1(в скобках - цена прихода)
приходы: 2(1) 2(2) 5(3)
расходы: 2 1

результат:
1. в остатках: 1 ед товара с ценой 1 грн, и 5 ед товара с ценой 3 грн
2. было продано: 2 ед товара с ценой прихода 1 грн, и 1 ед товара с ценой прихода 2 грн

расходы берем одним числом(order-stock-consumed) и постепенно вычитаем его
из приходов

")

(defn- analytics:sale-report-query[filters]
  (let [arrived (-> (order-stock-arrived)
                    (fields
                     [(raw "array_agg(price ORDER BY sa.id)") :arrived-prices]
                     [(raw "array_agg(quantity ORDER BY sa.id)") :arrived-quantities]
                     [(raw "array_agg(sa.id ORDER BY sa.id)") :arrivals]))
        consumed (-> (order-stock-consumed)
                     (fields [(raw "array_agg(price ORDER BY uoi.id)") :consumed-prices]
                             [(raw "array_agg(quantity - closed - coalesce(returned,0) ORDER BY uoi.id)") :consumed-quantity]
                             [(raw "array_agg(uoi.created ORDER BY uoi.id)") :consumed-dates]
                             [(raw "array_agg(uoi.id ORDER BY uoi.id)") :items]))]
    (->
     (select* [(subselect arrived) :arrived])
     (join [(subselect consumed) :consumed]
           (and (= :arrived.fkey_stock :consumed.fkey_stock)
                (= :arrived.params :consumed.params)))
     (join :left stock (= :arrived.fkey_stock :stock.id))

     ;; fields
     (fields-only :arrived-prices
                  :arrived-quantities
                  :arrivals

                  :consumed-prices
                  :consumed-quantity
                  :consumed-dates
                  :items

                  :arrived.fkey_stock
                  :arrived.params)

     ;; filtering
     (where {:arrived.fkey_stock [in (map :id (retail:admin-supplier-stocks nil nil (merge filters {:only-id true})))]})

     ;; count
     (count-query :custom-field "count(*)"))))

(defn- analytics:resolve-stock-and-params[result]
  (let [stocks
        (group-by-single :id (stock:get-multiple-stock (map :stock result)))

        resolve-params
        (fn[stock params]
          (->>
           (:stock-parameter-value stock)
           (filter (comp (set params) :id))
           (group-by :name)
           (fmap (partial map :value))))]
    (map (fn[{:keys [stock params] :as m}]
           (let [stock (get stocks stock)]
             (assoc m :stock stock :params (resolve-params stock params))))
         result)))

(defn- analytics:take-arrivals
  "Input :
consumed - number of all consumed stocks in this item
arrivals - all arrivals

Result: all used arrivals for given quantity(:sold - contains how much of stocks is really used
for arrival)"
  [consumed arrivals]
  (let [positive (fn[res] (if (> res 0) res 0))]
    (->> arrivals
         (reductions (fn[{:keys [consumed]} {:keys [price quantity arrival]}]
                       (let [left (positive (- quantity consumed))]
                         {:consumed (positive (- consumed quantity))
                          :quantity left
                          :sold (- quantity left)
                          :arrival arrival
                          :price price}))
                     {:consumed consumed})
         (filter :price)
         (map (fn[m] (dissoc m :consumed))))))

(defn- analytics:init-arrivals[arrived]
  (map (fn[[price quantity arrival]]
         {:price price :quantity quantity :arrival arrival})
       arrived))

(defn- analytics:compute-order-item-arrivals
  "Compute for each order item - what arrival is used for it stocks with what quantity"
  [arrived consumed]
  (let [initial-arrivals (analytics:init-arrivals arrived)]
    (->>
     consumed
     (reductions (fn[{:keys [arrived]} [price quantity item date]]
                   (let [all-arrivals (analytics:take-arrivals quantity arrived)
                         arrivals-used-for-item (filter (comp not zero? :sold) all-arrivals)]
                     {:quantity quantity
                      :price price
                      :arrivals arrivals-used-for-item
                      :arrived all-arrivals
                      :created date
                      :fkey_user-order-item item}))
                 {:arrived initial-arrivals})
     (map (fn[m] (dissoc m :arrived)))
     (filter :price))))

(defn- analytics:compute-left-stock-arrivals
  "Compute for each order item - what arrival is used for it stocks with what quantity"
  [arrived consumed]
  (let [{:keys [arrival] :as last-used-arrival}
        (last (:arrivals (last (analytics:compute-order-item-arrivals arrived consumed))))

        arrived
        (analytics:init-arrivals arrived)]
    (if (seq consumed)
      (->>
       (rest (drop-while (comp not (partial = arrival) :arrival) arrived))
       (cons last-used-arrival)
       (map (fn[m] (dissoc m :sold))))
      arrived)))

(deftest compute-items-arrivals
  (is
   (= (analytics:compute-order-item-arrivals
       [[5 10 1] [6 20 2]]    ;two arrivals, first time 10 items, second 20 items
       [[7 1 1] [7 2 2] [7 3 3] [7 5 4] [7 5 5]] ;5 order items, item #4 will be splitted across two arrivals
       )
      [{:quantity 1,
        :price 7,
        :created nil
        :arrivals [{:quantity 9, :sold 1, :arrival 1, :price 5}],
        :fkey_user-order-item 1}
       {:quantity 2,
        :price 7,
        :created nil
        :arrivals [{:quantity 7, :sold 2, :arrival 1, :price 5}],
        :fkey_user-order-item 2}
       {:quantity 3,
        :price 7,
        :created nil
        :arrivals [{:quantity 4, :sold 3, :arrival 1, :price 5}],
        :fkey_user-order-item 3}
       {:quantity 5,
        :created nil
        :price 7,
        :arrivals
        [{:quantity 0, :sold 4, :arrival 1, :price 5}
         {:quantity 19, :sold 1, :arrival 2, :price 6}],
        :fkey_user-order-item 4}
       {:quantity 5,
        :created nil
        :price 7,
        :arrivals [{:quantity 14, :sold 5, :arrival 2, :price 6}],
        :fkey_user-order-item 5}])))

(deftest compute-left-stock-arrivals
  (is
   (= (analytics:compute-left-stock-arrivals
       [[5 10 1] [6 20 2] [7 30 3] [8 40 4] [9 50 5]]    ;arrivals, 10 items, 20 items, 30 items, 40 items
       [[1 17 1] [1 14 2]]                      ;#1 items splitted to first and
                                                ;second arriva, #2 - third and
                                                ;fourth arrivals
       )
      [{:quantity 29, :arrival 3, :price 7}
       {:price 8, :quantity 40, :arrival 4}
       {:price 9, :quantity 50, :arrival 5}]))

  (is
   (= (analytics:compute-left-stock-arrivals
       [[5 10 1]] [])
      [{:quantity 10, :arrival 1, :price 5}])))

(defn- analytics:group-by
  "Input [{:stock, :params, :statistics}]
Out: {group-field: [{:stock,:params,:statistics}]}"
  [field items]
  (if field
    (let [group-fn (cond (= field :whole)
                         (constantly :whole)

                         (vector? field)
                         (let [[_ param] field

                               stocks
                               (group-by-single :id (select stock
                                                      (fields-only :id :stock-parameter-value.value)
                                                      (join stock-parameter-value
                                                            (= :stock-parameter-value.fkey_stock :stock.id))
                                                      (dry-transforms)
                                                      (where {:stock.id [in (map :stock items)]
                                                              :stock-parameter-value.name (name param)
                                                              :stock-parameter-value.enabled true})))]
                           (fn[{:keys [stock] :as item}] (get-in stocks [stock :value])))

                         true
                         (let [stocks
                               (group-by-single :id (select stock
                                                      (fields-only :id field)
                                                      (dry-transforms)
                                                      (where {:id [in (map :stock items)]})))]
                           (fn[{:keys [stock] :as item}] (get-in stocks [stock field]))))]
      (group-by group-fn items))
    items))

;; TODO : respect discount for items
(defn- analytics:aggregate [aggregate period items]
  (if period
    (let [aggregate-items
          (fn[{:keys [sold arrived]} {:keys [quantity price arrivals]}]
            (let [arrived-price (reduce + (map (fn[{:keys [sold price]}] (* sold price)) arrivals))
                  sold-price (* quantity price)]
              {:sold (+ sold-price sold)
               :arrived (+ arrived-price arrived)}))

          aggregate-items-for-period
          (partial reduce aggregate-items {:sold 0 :arrived 0})

          split-items-for-periods
          (partial group-by (comp (case period
                                    :month (juxt year month)
                                    :day-of-week day-of-week
                                    :day (juxt year month day)
                                    :week (fn[d][(year d) (.getWeekOfWeekyear d)])
                                    :whole (constantly :whole))
                                  tm/from-date
                                  :created))]
      (fmap (fn[stocks-with-params]
              (->>
               stocks-with-params
               (mapcat :statistics)
               split-items-for-periods
               (fmap aggregate-items-for-period)))
            items))
    items))

(defn analytics:sale-report[& {:keys [filters from to group aggregate period]}]
  (binding [*count-query* false]
    (let [arrived-consumed
          (select (analytics:sale-report-query filters))

          extract-lists
          (fn[m & keys] (apply map vector ((apply juxt keys) m)))

          filter-items-by-date
          (partial filter (fn[{:keys [created] :as item}]
                            (let [created (tm/from-date created)
                                  from (if from (after? created from) true)
                                  to (if to (before? created to) true)]
                              (and from to))))]
      (->>
       arrived-consumed
       (map (partial mreduce (fn[[k v]] {k (if (= org.postgresql.jdbc4.Jdbc4Array (class v))
                                             (from-array v)
                                             v)})))
       (map (fn[{:keys [fkey_stock params] :as info}]
              (let [arrived (extract-lists info :arrived-prices :arrived-quantities :arrivals)
                    consumed (extract-lists info :consumed-prices :consumed-quantity :items :consumed-dates)

                    left-statistics (analytics:compute-left-stock-arrivals arrived consumed)
                    sold-statistics (analytics:compute-order-item-arrivals arrived consumed)]
                {:stock fkey_stock
                 :params params
                 :statistics (filter-items-by-date sold-statistics)
                 :left-stock-statistics left-statistics})))
       (analytics:group-by group)
       (analytics:aggregate aggregate period)))))

(defn- annotate-map-from-sql[results additional-map key & {:keys [entity-key] :or {entity-key :id}}]
     (map #(assoc % key (or (get additional-map (entity-key %)) 0)) results))

(defn- annotate-entity-map-from-sql[results additional-map key]
  (map #(assoc % key (get additional-map (:id %))) results))

(defn analytics:left-stocks-statistics[limit offset filters]
  (if *count-query*
    (retail:admin-supplier-stocks limit offset filters)
    (let [stocks
          (retail:admin-supplier-stocks limit offset filters)

          statistics
          (->>
           (analytics:sale-report :filters {:limited-ids (map :id stocks)})
           (map (fn[m] (dissoc m :statistics)))
           (group-by :stock))]

      (annotate-entity-map-from-sql stocks statistics :left-stock-statistics))))

;; TODO : check that all arrivals list not empty on real database

(defn analytics:sale-report-sum-for-filters[filters]
  (let [report (analytics:sale-report :filters filters)]
    {:sold (reduce + (map :sold report))
     :left (reduce + (map :left report))}))

(run-tests)
