(ns gd.model.context
  (:use
   gd.utils.robust-pool
   gd.utils.db
   gd.utils.common
   [clojure.algo.generic.functor :only (fmap)]
   gd.log
   [noir.options :only (dev-mode? *options*)])
  (:require [noir.session :as session])
  (:import java.net.URL))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Hosts management
(comment "Contains global context variables. They should be bound by ring
middleware. Also shedulers and thread pools should be avare of them.

Can be used to configure arbitrary host mappings.

Main host info is bound to supplier or organizer user. Different hosts can be
bound to same user(for example vika.dayte-dve.com.ua and vika.bono.in.ua - are
bound to same organizer, but first is global - don't bound to supplier, and
other is specific to supplier).

Match is performed using HOST and URL variables from HTTP request. URL is
matched from most long to most short")

(def admin-hosts ["retail.com.ua" "legka-pokupka.com.ua" "legkapokupka.com.ua"
                  "bono.in.ua" "test.rocket-sales.biz" "bonolocal.in.ua"])

(def kolesnik-supplier
  {:vk {:app-id 3834743
        :secret "TtpgKXgzzS2jwrUa0EB8"}
   :supplier 1
   :category "stocks"
   :design {:buttons :plain}})

(def constant-host-info
  [
   ;; bono + lp admin
   (merge kolesnik-supplier
          {:hosts admin-hosts
           :sms {:name "bono.in.ua"}
           :mail {:name "bono.in.ua"
                  :address "admin@bono.in.ua"
                  :provider :amazon
                  :images-server "http://bono.in.ua"}
           :url "/admin"
           :offset-url "/retail"
           :context [:retail :wholesale :wholesale-sale :retail-sale]})

   (merge kolesnik-supplier
          {:hosts admin-hosts
           :sms {:name "bono.in.ua"}
           :mail {:name "bono.in.ua"
                  :address "admin@bono.in.ua"
                  :provider :amazon
                  :images-server "http://bono.in.ua"}
           :url "/copywriter"
           :offset-url "/retail"
           :context [:retail :wholesale :wholesale-sale :retail-sale]})

   ;; opencart rest api
   (merge kolesnik-supplier
          {:hosts admin-hosts
           :sms {:name "bono.in.ua"}
           :mail {:name "bono.in.ua"
                  :address "admin@bono.in.ua"
                  :provider :amazon
                  :images-server "http://bono.in.ua"}
           :url "/restapi"
           :stocks-mode :available-from-arrival
           :offset-url ""
           :context-filter [:wholesale] ;show only stocks that are visible in wholesale
           :context [:wholesale :wholesale-sale]})

   ;; seo(default - bono, but can be choosed in the menu)
   (merge kolesnik-supplier
          {:hosts admin-hosts
           :url "/seo"
           :context-switch true
           :offset-url "/retail"
           :context [:wholesale]})


   ;; legka pokupka client view
   (merge kolesnik-supplier
          {:hosts ["retail.com.ua" "legka-pokupka.com.ua" "legkapokupka.com.ua"]
           :offset-url "/retail"
           :images-modifier "mirror/"
           :stocks-mode :available-from-arrival
           :url-mode {:category :id}
           :process-main-properties (fn[{:keys [category id all-params] :as stock}]
                                      (assoc stock :name (str (:name category) " " id " "
                                                              (first (:color all-params)))))
           :sms {:name "legkapokupk"}
           :mail {:name "Легка Покупка"
                  :address "admin@legka-pokupka.com.ua"
                  :provider :amazon}
           :context-filter [:retail] ;show only stocks that are visible in retail
           :context [:retail :retail-sale]})

   ;; bono client view
   (merge kolesnik-supplier
          {:hosts ["bono.in.ua" "retail.com.ua" "bono.in.ua:8080"]
           :offset-url ""
           :design {:buttons :complex}
           :stocks-mode :available-from-arrival
           :sms {:name "bono.in.ua"}
           :activation {:email {:template "mail/bono-registration.html"
                                :header "Приветствуем на сайте \"bono.in.ua\""
                                :link "http://bono.in.ua/activation?code="}
                        :sms nil}
           :mail {:name "bono.in.ua"
                  :address "bono.opt@gmail.com"
                  :provider :amazon
                  :images-server "http://bono.in.ua"}
           :context-filter [:wholesale] ;show only stocks that are visible in wholesale
           :context [:wholesale :wholesale-sale]})

   ;; demo view for smartcommerce with bono frontend
   {:hosts ["test.smartcommerce.com.ua" "test.smart.com.ua"]
    :offset-url "/themes"
    :design {:buttons :complex}
    :stocks-mode :available-from-arrival
    :sms {:name "smartcom"}
    :context-filter [:wholesale] ;show only stocks that are visible in wholesale
    :context [:wholesale :wholesale-sale]
    :supplier 1
    :category "stocks"}

   ;; admin for smart commerce
   {:hosts ["smartcommerce.com.ua" "smart.com.ua" "sm24.com.ua" "sc24.com.ua"]
    :design {:buttons :plain}
    :offset-url "/smartcommerce"}

   {:hosts ["smartcommerce.com.ua" "smart.com.ua" "sm24.com.ua" "sc24.com.ua"]
    :url "/admin"
    :design {:buttons :plain}
    :category "stocks"
    :supplier 1
    :offset-url "/retail"
    :context [:retail :wholesale :wholesale-sale :retail-sale]}

   {:hosts ["smartcommerce.com.ua" "smart.com.ua" "sm24.com.ua" "sc24.com.ua"]
    :url "/copywriter"
    :design {:buttons :plain}
    :category "stocks"
    :supplier 1
    :offset-url "/retail"
    :context [:retail :wholesale :wholesale-sale :retail-sale]}

   ;; custom theme
   {:hosts ["custom-theme.com"]
    :offset-url "/themes"
    :theme [:custom :basic]
    :design {:buttons :complex}
    :stocks-mode :available-from-arrival
    :sms {:name "smartcom"}
    :context-filter [:wholesale] ;show only stocks that are visible in wholesale
    :context [:wholesale :wholesale-sale]
    :supplier 1
    :category "stocks"}

   {:hosts ["rocket-sales.biz"]
    :offset-url "/rocket-sales"}])

(defonce host-info (atom nil))

(defonce host-url-to-id (atom nil))

(defn- resolve-host-to-info-id [host url]
  (let [url-to-id (get @host-url-to-id host)]
    (some (fn[[search-url id]]
            (if search-url
              (and (.startsWith url search-url) id)
              id))
          url-to-id)))

(defn- get-host-info[host-id]
  (when host-id (nth @host-info host-id)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Public API to hosts info

(defn refresh-hosts-map[variable-host-info]
  (reset! host-info (concat constant-host-info variable-host-info))
  (reset! host-url-to-id
          (->>
           @host-info
           (map-indexed
            (fn[index {:keys [hosts url]}]
              (->>
               ["" ":8080" ":8090"]
               (mapcat (fn[postfix] (map (fn[host] (str host postfix)) hosts)))
               (map (fn[host] {host [[url index]]})))))
           (apply concat)
           (reduce (partial merge-with concat))
           (fmap (comp reverse (partial sort-by (comp count first)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Context that will be passed

(def ^:dynamic *host-id* nil)
(def ^:dynamic *current-supplier* nil)
(def ^:dynamic *supplier-info* nil)
(def ^:dynamic *stock-context* nil)
(def ^:dynamic *context-types* nil)     ;filled later by stock-context:resolve-types(ids of context)
(def ^:dynamic *current-theme* :basic)
(def ^:dynamic *omit-cache* false)

(defn get-custom-database[{:keys [sql]}]
  (when sql (get custom-sql-databases sql)))

(defmacro set-global-context[host-id & body]
  `(let [info# (#'get-host-info ~host-id)]
     (binding [*host-id* ~host-id
               *current-supplier* (:supplier info#)
               *stock-context* (:context info#)
               *current-theme* (or (:theme info#) :basic)
               *supplier-info* info#
               *database-override* (get-custom-database (:database info#))]
       ~@body)))

(defn serialize-context[]
  {:host-id *host-id*
   :dev (dev-mode?)})

(defmacro deserialize-context[context & body]
  `(set-global-context (:host-id ~context)
     (binding [*options* {:mode (if (:dev ~context) :dev :prod)}]
       ~@body)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Router middleware

(def keep-uri #{"/service" "/management" "/admin/events"})

(defn route-hosts [handler {:keys [uri headers] :as request}]
  (if-let [host-id (resolve-host-to-info-id
                    (get headers "host")
                    (if (= "/service" uri)
                      (.getPath (new URL (get headers "referer")))
                      uri))]
    (set-global-context host-id
      (binding [*omit-cache* (or *omit-cache* (= :demo (:sql (:database *supplier-info*))))]
        (handler (->
                  (if (keep-uri uri)
                    request
                    (-> request
                        (assoc :uri (str (:offset-url *supplier-info*) uri))
                        (assoc :original-uri uri)))
                  (assoc :host-info *supplier-info*)
                  (assoc :host-id host-id)))))
    (handler request)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Thread support with context passing

(defn in-thread[function & {:keys [error-hook]}]
  (let [context (serialize-context)
        session session/*noir-session*]
    (.start
     (new Thread (fn[]
                   (try
                     (binding [session/*noir-session* session]
                       (deserialize-context context (function)))
                     (catch Exception e
                       (error e)
                       (error-hook))))))))

(defn in-thread-pool[thread-pool function & {:keys [error-hook]}]
  (let [context (serialize-context)
        session session/*noir-session*
        task (fn[]
               (try
                 (binding [session/*noir-session* session]
                   (deserialize-context context (function)))
                 (catch Exception e
                   (error e)
                   (error-hook))))]
    (cond
      (instance? java.util.concurrent.Executor thread-pool)
      (.submit thread-pool task)

      (robust-pool? thread-pool)
      (submit thread-pool task))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils for extracting context information

(defn context-info[default & keys]
  (or (get-in *supplier-info* keys) default))

(defn context-info-by-key[& keys]
  (get-in *supplier-info* keys))
