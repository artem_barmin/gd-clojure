(in-ns 'gd.model.model)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Left stocks computations(take arrived stocks and minus all requested stocks
;; in order items)

(defn- order-stock-consumed
  "Builds table
stock-id, params, quantity, closed

So we can get all concrete stocks quantity are consumed by users. "
  []
  (let [order-item-with-params
        (-> (select* :user-order-item-with-stock-parameter-value)
            (fields
             [:user-order-item-key :item-id]
             [(raw "array_agg(\"stock-parameter-value-key\" ORDER BY \"stock-parameter-value-key\")") :params])
            (group :item-id))

        returned-items
        (-> (select* :additional-expense)
            (fields
             [:fkey_user-order-item :item-id]
             [:items :returned])
            (where {:type (additional-expense-type :return-stocks)}))]
    (->
     (select* [:user-order-item :uoi])
     (fields :fkey_stock :params
             [(sqlfn :sum (raw "quantity - closed - coalesce(returned,0)")) :quantity])
     (join :inner
           (subselect (as order-item-with-params :up))
           (= :uoi.id :up.item-id))
     (join :left
           (subselect (as returned-items :ri))
           (= :uoi.id :ri.item-id))
     (where {:uoi.deleted false})
     (group :fkey_stock :params))))

(defn- order-stock-arrived
  "Builds table
stock-id, params, quantity

So we can get all arrived quantity for stocks"
  []
  (let [stock-arrival-with-params
        (->
         (select* :stock-arrival-parameter-with-stock-parameter-value)
         (fields [:stock-arrival-parameter-key :arrival-id]
                 [(raw "array_agg(\"stock-parameter-value-key\" ORDER BY \"stock-parameter-value-key\")") :params])
         (group :arrival-id))]
    (->
     (select* [:stock-arrival-parameter :sap])
     (fields :fkey_stock :params
             [(sqlfn :sum :quantity) :quantity])
     (join :inner
           (subselect (as stock-arrival-with-params :ap))
           (= :sap.id :ap.arrival-id))
     (join :inner
           [:stock-arrival :sa]
           (= :sa.id :sap.fkey_stock-arrival))
     (where (or {:sa.type (stock-arrival-type :arrival)}
                {:sa.type (stock-arrival-type :inventory)
                 :sa.status (stock-arrival-status :closed)}))
     (group :fkey_stock :params))))

(defn consumed-arrived-stocks-query
  "Builds table stock-id,array of param ids, consumed, arrived"
  [& {:keys [arrived consumed]}]
  (let [arrived (or arrived (order-stock-arrived))
        consumed (or consumed (order-stock-consumed))]
    (->
     (select* (subselect (as arrived :arrived)))
     (fields [(sqlfn :coalesce :arrived.fkey_stock :consumed.fkey_stock)
              :fkey_stock]
             [(sqlfn :coalesce :arrived.params :consumed.params)
              :params]
             [(sqlfn :coalesce :consumed.quantity 0) :consumed]
             [(sqlfn :coalesce :arrived.quantity 0) :arrived])
     (join :full
           (subselect (as consumed :consumed))
           (and (= :arrived.fkey_stock :consumed.fkey_stock)
                (= :arrived.params :consumed.params))))))

(defn consumed-arrived-stocks[]
  (subselect :consumed-arrived-stocks))

;; filter for stocks
(defn- left-stocks[]
  (-> (select* [(consumed-arrived-stocks) :left])
      (fields :fkey_stock)
      (where (> (raw "arrived - consumed") 0))))

(defn- visible-stocks[query]
  (where-if query (only-visible-stocks?)
            (or {:stock.id [in (subselect (left-stocks))]}
                {:stock.accounting (stock-accounting :simple)})))

;; filter for parameters
(defn- only-available[query]
  (where-if query (only-visible-stocks?) (or {:available true}
                                             {:accounting (stock-accounting :simple)})))

(defn available-stock-values-query[]
  (let [params-query
        (-> (select* stock-parameter-value)
            (fields-only
             :name :value :fkey_stock
             :stock.status :stock.fkey_category :stock.fkey_supplier :stock.accounting)
            (modifier "distinct")
            (join stock)
            (group :name :value :fkey_stock
                   :stock.status :stock.fkey_category :stock.fkey_supplier :stock.accounting)
            (where {:stock-parameter-value.enabled true
                    :stock.status [not= (stock-status :deleted)]}))

        ;; extract params from warehouse stocks
        warehouse-query
        (->
         params-query
         (fields [(sqlfn :coalesce (sqlfn :bool_or :left.available) false) :available])
         (join :left [(subselect
                       (-> (select* :consumed-arrived-stocks)
                           (fields-only
                            :fkey_stock
                            ["(arrived - consumed)>0" :available]
                            ["unnest(params)" :param])))
                      :left]
               true)
         (where {:stock.accounting (stock-accounting :warehouse)})
         (where (or
                 {:stock-parameter-value.id :left.param}
                 {:stock-parameter-value.visible false
                  :stock-parameter-value.fkey_stock :left.fkey_stock})))

        ;; extract all stocks with simple accounting
        simple-query
        (-> params-query
            (fields (raw "FALSE"))
            (where {:stock.accounting (stock-accounting :simple)}))]
    (-> (select* [(union-all (subselect warehouse-query) (subselect simple-query)) :res])
        (fields (raw "*")))))

(defn analytics:get-left-quantity-stocks[stock-ids]
  (let [arrived-stocks
        (map
         (fn[m] (update-in m [:params] from-array))
         (select [(consumed-arrived-stocks) :left]
           (where {:fkey_stock [in stock-ids]})))]
    (fmap
     (fn[{:keys [consumed arrived]}]
       (if (and consumed arrived) (- arrived consumed) 0))
     (group-by-single (juxt :fkey_stock :params) arrived-stocks))))

(defn- analytics:annotate-stock-with-params[stocks key extra-params-list extra-info-fn]
  (let [extra-params
        (group-by :fkey_stock extra-params-list)

        all-stock-params
        (->>
         extra-params-list
         (map :params)
         flatten
         distinct
         meta:get-params-from-id-list
         (group-by-single :id))

        param-to-map
        (fn[{:keys [params] :as whole}]
          {(->>
            params
            (map (comp (fn[{:keys [name value]}] {(keyword name) value}) all-stock-params))
            (reduce merge))
           (extra-info-fn whole)})]
    (map (fn[{:keys [id] :as stock}]
           (assoc stock key (reduce merge (map param-to-map (get extra-params id)))))
         stocks)))

(defn- analytics:annotate-left-stocks[stocks]
  (let [analytics-info
        (map
         (fn[m] (update-in m [:params] from-array))
         (select [(consumed-arrived-stocks) :left]
           (where {:fkey_stock [in (map :id stocks)]})))]
    (analytics:annotate-stock-with-params
      stocks
      :left-stocks
      analytics-info
      (fn[{:keys [consumed arrived]}] (- arrived consumed)))))

(defn- analytics:hide-invisible-params[stocks]
  (let [compute-stock-min-price
        (fn [{:keys [stock-variant] :as stock}]
          (let [all-prices (->>
                            stock-variant
                            vals
                            (map maybe-seq)
                            flatten
                            (map :price)
                            (remove nil?))]
            (if (seq all-prices)
              (assoc stock :price (reduce min all-prices))
              stock)))

        collect-params-from-map
        (fn[params-map filter-fn]
          (->>
           params-map
           (map (fn[[params values]] (when (filter-fn values) (fmap maybe-seq params))))
           (reduce (partial merge-with concat))
           (fmap set)))]
    (if (only-visible-stocks?)
      (->>
       stocks
       analytics:annotate-left-stocks
       (map (fn[{:keys [stock-parameter-value left-stocks stock-variant accounting] :as stock}]
              (if stock-variant
                (let [modified-stock
                      (case accounting

                        ;; keep only available stocks on warehouse
                        :warehouse
                        (let [filter-single-stock-variant
                              (fn[variant]
                                (let [available (get left-stocks (:params variant))]
                                  (and available (> available 0) variant)))

                              filtered-stock-variant
                              (fmap (fn[variant]
                                      (if (map? variant)
                                        (filter-single-stock-variant variant)
                                        (filter filter-single-stock-variant variant)))
                                    stock-variant)

                              available-params
                              (collect-params-from-map left-stocks (fn[quantity] (> quantity 0)))

                              filtered-stock-params
                              (filter (fn[{:keys [name value visible]}]
                                        (or (not visible) (get-in available-params [(keyword name) value])))
                                      stock-parameter-value)]
                          {:stock-variant filtered-stock-variant
                           :stock-parameter-value filtered-stock-params})

                        ;; keep all parameters that have variants
                        :simple
                        (let [available-params
                              (collect-params-from-map stock-variant (constantly true))

                              filtered-stock-params
                              (filter (fn[{:keys [name value visible]}]
                                        (or (not visible) (get-in available-params [(keyword name) value])))
                                      stock-parameter-value)]
                          {:stock-parameter-value filtered-stock-params}))]
                  (-> stock
                      (merge modified-stock)
                      stock:parameters-for-choosing
                      compute-stock-min-price))
                stock))))
      stocks)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Mat view debug

(let [process (partial map (fn[m] (update-in m [:params] from-array)))]
  (defonce analytics-mat-view-debug
    (make-mat-view-debugger
     :analytics
     (fn[] (process (select [(subselect (consumed-arrived-stocks-query)) :left])))
     (fn[] (process (select :consumed-arrived-stocks))))))

(defonce available-param-values-mat-view-debug
  (make-mat-view-debugger
   :available-param-values
   (fn[] (select [(subselect (available-stock-values-query)) :left]))
   (fn[] (select :availabe-param-values))))
