(in-ns 'gd.model.model)

(defalias user-transaction-author user)

(defentity account-transactions
  (enum account-transactions-type [:type] :passed)
  (belongs-to user {:fk :fkey_user})
  (belongs-to user-transaction-author {:fk :author :entity :author}))

(defn- get-transaction[id]
  (select-single account-transactions (where {:id id})))

(defn account:transact-money[user-id amount comment]
  {:pre [user-id (seq comment) (not= amount 0)]}
  (insert account-transactions (values {:fkey_user user-id
                                        :author (sec/id)
                                        :amount amount
                                        :type :passed
                                        :comment comment
                                        :created (sql-now-timestamp)}))
  true)

(defn account:reject-transaction[transaction-id]
  (transaction
    (lock-table :account-transactions)
    (let [{:keys [fkey_user amount comment]} (get-transaction transaction-id)]
      (account:transact-money fkey_user (* -1 amount) (str "Отмена:" comment)))))

(defn account:transactions[user-id]
  (select account-transactions
    (with-alias user-transaction-author)
    (order :created)
    (where {:fkey_user user-id})))

(defn account:current-balance[user-id]
  (:sum
   (select-single account-transactions
                  (with-alias user-transaction-author)
                  (aggregate (sum :amount) :sum)
                  (where {:fkey_user user-id
                          :type (account-transactions-type :passed)}))))
