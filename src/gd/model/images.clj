(in-ns 'gd.model.model)

(comment
  "General idea of image storage : we store some specific code(sha1 hash)
for each given image. Also on storing stage we can pre-generate all requested
thumbnails.

If you wan't to get URI to image - you should take unique code(from entity), and
pass it to function, together with requested dimensions : images:get(code [width
height]). This function will generate real path to image.

example:
1. We have entity User with avatar image : user (retrieved with function
users:get)
2. We want to show it's image with directions 77x76
3. call function with next params : (images:get (:avatar user) [77 76]) - and
use result as URL
NOTE : this code is not a real user management model, just example

IMPORTANT : you MUST not try to generate pathes directly with hacks!")

(defn- is-local[path]
  (.isFile (io/file path)))

(defn generate-local-code
  "Generate code for locally located file(example : for files uploaded to the
  server)"
  [source-path]
  (sha1 (io/file source-path)))

(defn- generate-remote-code
  "Generate code for remote files"
  [source-path]
  (sha1 source-path))

(defn- generate-code-from-path[path]
  (if (is-local path)
    (generate-local-code path)
    (generate-remote-code path)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HTTP image downloader
(defn- download-binary [from]
  (try-try-again {:sleep 1000
                  :tries 5
                  :catch [java.net.ConnectException]
                  :error-hook (fn[e] (println "I got error while downloading avatar" e))}
                 #(let [to (java.io.File/createTempFile "download" ".jpg")]
                    (with-open [out (FileOutputStream. to)]
                      (io/copy
                       (io/input-stream (io/as-url from))
                       out))
                    (.getAbsolutePath to))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Code -> Path transformators
(defn- generate-directory[[width height :as dimensions]]
  (str
   "/img/uploaded/" (context-info-by-key :images-modifier)
   (if dimensions
     (str "w" width "/h" height "/")
     (str "original/"))))

(defn- generate-public-path
  "Function that generate local path for storage files(from where they will be
available for web). If no width and height is passed - then store image with
original dimensions. As source path you should pass reference to local temporal
storage of uploaded file."
  [code size]
  (str (generate-directory size) code ".jpg"))

;; TODO : add support of non JPEG files
(defn generate-storage-path
  "Function that generate local path for storage files(from where they will be
available for web). If no width and height is passed - then store image with
original dimensions. As source path you should pass reference to local temporal
storage of uploaded file."
  [code size]
  ;; create nessesary directory
  (let [dir (str "resources/public" (generate-directory size))]
    (.mkdirs (io/file dir))
    (str dir code ".jpg")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Resize queue processor

(def images-queue "images-queue")

(create-publisher images-queue 16)

(def image-threads 4)

(def gm-pool
  (new org.gm4java.engine.support.PooledGMService
       (doto (new org.gm4java.engine.support.GMConnectionPoolConfig)
         (.setMaxActive image-threads)
         (.setTimeBetweenEvictionRunsMillis (* 60 1000))
         (.setMinEvictableIdleTimeMillis (* 60 1000)))))

(defn process-images[original-params]
  (let [{:keys [source target type params]} original-params]
    (case type
      :batch (map! process-images params)
      :hard-link (locking images-queue
                   (let [target (.toPath (new java.io.File target))
                         source-path (.getAbsolutePath (new java.io.File source))
                         source (.toPath (.getAbsoluteFile (new java.io.File source)))]
                     (if (or (.endsWith source-path ".jpeg")
                             (.endsWith source-path ".jpg")
                             (.endsWith source-path ".png"))
                       (do (java.nio.file.Files/deleteIfExists target)
                           (java.nio.file.Files/createLink target source))
                       (fatal "File" source " have bad type"))))
      :gm (.execute gm-pool (cons "convert" params)))))

(create-subscriber images-queue #'process-images :threads-num image-threads)

(defn left-thumbnails[]
  (queue-size images-queue))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Image processing params generator

(defn- stamp[[stamp-source dst-gravity & [stamp-resize]]]
  (list "("
        "+clone" "-crop" stamp-source
        (when stamp-resize ["-resize" (str stamp-resize "!")])
        ")"
        "-gravity" dst-gravity "-composite"))

(defn- make-image-processing-params[source-file target-file
                                    [width height disable-extent :as size]
                                    {:keys [fit gravity crop trim shave stamps quality shrink flop]
                                     :or {fit true gravity "North" quality 87}}]
  (cond

    (and (nil? size) (or (re-find #"^data/.*/files" @source-file)
                         (re-find #"gd-clojure/data/.*/files" @source-file)
                         (re-find #"test/resources/img/items" @source-file)))
    {:type :hard-link
     :source @source-file
     :target (.getAbsolutePath target-file)}

    true
    {:type :gm
     :target (.getAbsolutePath target-file)
     :params
     (let [full-size (= 2 (count size))]
       (remove empty?
               (flatten
                [@source-file
                 (map stamp stamps)
                 (when (seq crop) ["-crop" crop])
                 (when (seq shave) ["-shave" shave])
                 (when (seq trim) ["-fuzz" (str trim "%") "-trim" "+repage"])
                 (when flop ["-flop"])
                 (when size ["-resize" (str width "x" height
                                            (when (and full-size fit) "^")
                                            (when (and full-size shrink) ">"))])
                 (when quality ["-quality" (str quality)])
                 "-gravity" gravity
                 (when (and size (not disable-extent) full-size)
                   ["-extent" (str width "x" height)])
                 "-strip"
                 "-interlace" "Plane"
                 (.getAbsolutePath target-file)])))}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sync and async task executors

(defn- create-processing-task
  "
Create task for execution

crop - cut image from given geometry
shave - remove surrounding borders(10x10 - will remove 10 pixels from all sides)
gravity - gravity for resizing
fit - do we need to fit image in requested size
stamps - [[stamp-source(ex : 100x100+400+100) dst-gravity(ex : NorthWest)]]
trim - fuzzy crop
"
  [source-file target-file size params-map]
  (when (or (:force-overwrite params-map) (not size) (not (.exists target-file)))
    (make-image-processing-params source-file target-file size params-map)))

(defn process-image-sync
  "Generate image immediatelly without passing task to queue."
  [& params]
  (when-let [param (apply create-processing-task params)]
    (process-images param)))

(defn- process-image-async
  "Send task to queue"
  [& params]
  (when-let [param (apply create-processing-task params)]
    (publish images-queue param)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Internal saving functions

(declare resolve-size)

(defn- images:generate-image-thumbnails-with-code[img all-sizes & [force-sizes]]
  (let [path (:path img)
        all-sizes (cons nil all-sizes)  ;always store original
        code (generate-code-from-path path)
        source-file (delay
                      (if (is-local path)
                        path
                        (download-binary path)))
        process-sizes-list
        (fn[proces-fn sizes]
          (map! (fn[size]
                  (let [size (resolve-size size)
                        target-file (io/file (generate-storage-path code size))]
                    (proces-fn source-file target-file size (:params img))))
                sizes))
        lazy-sizes (set/difference (set all-sizes) (set force-sizes))]
    ;; process sync images
    (process-sizes-list process-image-sync force-sizes)
    ;; put other images into queue in batch mode
    (publish images-queue
             {:type :batch
              :params (remove nil? (process-sizes-list create-processing-task lazy-sizes))})
    ;; return code result
    code))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Some common sizes
(def image-common-sizes
  {:large-avatar [160 160]
   :small-avatar [75 75]
   :messages-avatar-small [50 50]
   :messages-avatar-large [100 100]
   :tiny-avatar [25 25]

   :deal-huge [340 480]
   :deal-large [270 380]
   :deal-medium [220 315]
   :deal-small [135 190]

   :stock-very-large [250 359]
   :stock-large [198 280]
   :stock-bono [180 273]
   :bono-small [69 90]
   :stock-medium [54 54]
   :stock-deals [106 109]
   :stock-bucket [75 75]
   :retail-large-image [404 608]

   :annotations-small [78]
   :annotations-large [290 422]

   :admin-large [340 450]
   :admin-sq-large [115 115]
   :base-template-wide [190 270]})

(defn resolve-size[size]
  (if (or (keyword? size) (string? size))
    (or ((keyword size) image-common-sizes) (throwf "Size %s not found" size))
    size))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Public save API

(defn images:wait-while-thumbnails-processed[]
  (while (> (left-thumbnails) 0)
    (Thread/sleep 30)))

(defn images:generate-new-image-thumbnails[code source-file params & sizes]
  (doseq [size (map resolve-size sizes)]
    (let [target-file (io/file (generate-storage-path code size))]
      (process-image-async (atom source-file) target-file size params))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; New interface for images - not based on the specific entity in DB. Just plain strings

(defn images:save-to-code[img sizes & [force-sizes]]
  (cond (:path img)
        (images:generate-image-thumbnails-with-code img sizes force-sizes)

        (string? img)
        img))

(defn images:save-showcase-to-code[images sizes]
  (map! (fn[im](images:save-to-code im sizes)) images))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Public retrieval API
(defn images:get[size img]
  (let [code (if (string? img) img (:code img))]
    (generate-public-path code (resolve-size size))))

(defn images:get-size[size]
  (partial images:get size))

(defn images:parse-size-from-src[src]
  {:width (when-let [w (second (re-find #"/w([0-9]+)/" src))]
            (parse-int w))
   :height (when-let [h (second (re-find #"/h([0-9]+)/" src))]
             (parse-int h))
   :code (second (re-find #"/([a-f0-9]+).jpg" src))})
