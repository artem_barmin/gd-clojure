(in-ns 'gd.model.model)

(declare stock-variant:prepare-params)

(defentity stock-variant
  (default-fetch stock-parameter-value)
  (transform #'stock-variant:prepare-params)
  (prepare (fn[{:keys [context] :as ent}]
             (-> ent
                 (assoc :fkey_stock-context (when context (stock-context:resolve-or-create-type context)))
                 (dissoc :context))))
  (transform (fn[{:keys [fkey_stock-context] :as ent}]
               (assoc ent :context (when fkey_stock-context (stock-context:resolve-id-to-type fkey_stock-context)))))
  (has-many-join-table stock-parameter-value
                       {:join-table :stock-parameter-variants
                        :entity :stock-parameter-value}))

(defn- stock-variant:prepare-params[{:keys [stock-parameter-value] :as entity}]
  (-> entity
      (assoc :params (params-as-map stock-parameter-value))
      (dissoc :stock-parameter-value)))

(defn- stock-variant:prepare-stock-variants[{:keys [stock-variant] :as entity}]
  (assoc entity :stock-variant (group-by :params stock-variant)))

(defn- stock-variant:store-single-variant[stock-id all-stock-params params-map variant]
  (when variant
    (let [{:keys [price context] :as variant}
          (cond (map? variant) variant
                (integer? variant) {:price variant}
                true (throwf "Unsupported variant, maybe int or map : %s" variant))

          {:keys [id]}
          (insert stock-variant
                  (values {:fkey_stock stock-id :context context :price (or price 0)}))]
      (let [resolved-params (sort (map :id (meta:resolve-params-map all-stock-params params-map)))]
        (when-not (= (count resolved-params) (count params-map))
          (throwf "Not all params can be resolved for variant: %s %s %s"
                  (count resolved-params)
                  (count params-map)
                  params-map))
        (doseq [param-id resolved-params]
          (insert :stock-parameter-variants (values {:stock-variant-key id
                                                     :stock-parameter-value-key param-id})))))))

(defn- stock-variant:update-stock-variants[stock-id variants]
  (let [params (select stock-parameter-value (where {:fkey_stock stock-id}))]

    ;; clear previous variants
    (let [variants (map :id (select stock-variant (where {:fkey_stock stock-id})))]
      (delete :stock-parameter-variants (where {:stock-variant-key [in variants]}))
      (delete stock-variant (where {:id [in variants]})))

    ;; store new variants
    (doseq [[params-map variant] variants]
      (doseq [variant (maybe-seq variant)]
        (stock-variant:store-single-variant stock-id params params-map variant)))))
