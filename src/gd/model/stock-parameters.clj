(in-ns 'gd.model.model)

(declare dictionary:add-parameters)

(comment "Plain dictionary about stock parameters. Each parameter can be
disabled, but generally params avaliability is computed using arrival info.

Params is immutable(we can't change name and value). Only disable certain params
and enable other.

Stock price dependency of parameters stored in stock-variants.")

;; Metamodel for stock parameters
(defentity stock-parameter-value
  (belongs-to stock {:fk :fkey_stock}))

(def s:params-map
  {(s/named s/Any "param name")
   (s/named String "param value")})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Saving and updating functions

(defn- params-map-to-list[stock-id parameters]
  (let [prepare (comp strings/trim as-str)]
    (->>
     parameters
     (map! (fn[[name param-values]]
             (let [[name visible] (if (vector? name) name [name true])]
               (map! (fn[value]
                       (let [[value] (if (vector? value) value [value])]
                         (when (seq (prepare value))
                           {:name (prepare name)
                            :value (prepare value)
                            :fkey_stock stock-id
                            :visible visible})))
                     param-values))))
     flatten
     (remove nil?)
     distinct)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn- meta:save-stock-parameters
  "Save parameters for current stock. Only add params to stock, don't disable params that don't exists in
params map."
  [stock-id parameters]
  (let [parameters (dissoc parameters ["size" false])
        params-list (params-map-to-list stock-id parameters)]

    ;; save params(if they not yet saved)
    (let [conditions (map (fn[m] (select-keys m [:name :value :fkey_stock])) params-list)
          key-fn (juxt :name :value :fkey_stock)

          already-existing-params
          (when (seq params-list)
            (->>
             (select stock-parameter-value (where {:fkey_stock stock-id}))
             (group-by-single key-fn)))]
      (doseq [param params-list]
        (when-not (get already-existing-params (key-fn param))
          (insert stock-parameter-value (values (merge param {:enabled true}))))))

    ;; update dictionary
    (dictionary:add-parameters params-list)))

(defn- meta:update-stock-parameter
  "Update single stock parameter. Works only for enabled and visible params."
  [param-id new-values]
  (update stock-parameter-value
    (set-fields (select-keys new-values [:enabled :visible]))
    (where {:id param-id})))

(defn- meta:resolve-params-map[stock-parameters params-map]
  (filter (fn[{:keys [name value]}] (= value (as-str (get params-map (keyword name))))) stock-parameters))

(defn- meta:get-params-from-id-list[ids]
  (select stock-parameter-value
    (where {:id [in ids]})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Common utils

(defn- params-as-map[params]
  (mreduce (fn[{:keys [name value]}] {(keyword name) value}) params))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Selecting based on metaparameters

(defn- remove-empty-entries[filters]
  (mreduce (fn[[k v]] (when v {k v})) filters))

(declare only-available)

(defn- meta:get-stocks-with-match[filters-state]
  (let [filters-state
        (remove-empty-entries filters-state)

        values
        (mapcat
         (fn[[name vals]]
           (map
            (fn[val] {:name (as-str name) :value val})
            (maybe-seq vals)))
         filters-state)]
    (subselect
     :availabe-param-values
     (fields-only [:fkey_stock :id])
     (group :fkey_stock)
     (where-if (seq values) (apply or values))
     only-available
     (having {(raw "count(distinct name)") (count filters-state)}))))

(defn meta:get-filters-for-stocks-with-count
  "Returns possible values for filters. Set of possible values is based on the
set of all stocks meta-values from given group deal. Also we can filter stocks
by statuses. Also we can pass limit-param-ids - to limit params search, this will affect
only visible parameters(used for filter by arrival for parameters like :size)"
  [{:keys [statuses categories limit-ids filters]}]
  (select :availabe-param-values
    (fields :name :value (raw "count(distinct fkey_stock)"))
    (group :name :value)
    (where-if (seq statuses) {:status [in (map stock-status statuses)]})
    (where-if (seq categories) {:fkey_category [in (map :id (category:descendant-categories categories))]})
    (where-if (not (nil? limit-ids)) {:fkey_stock [in limit-ids]})
    (where-if (seq (remove-empty-entries filters)) {:fkey_stock [in (meta:get-stocks-with-match filters)]})
    only-available
    (where {:fkey_supplier *current-supplier*})))

(defn- meta:get-filters-for-stocks
  "Returns possible values for filters. Set of possible values is based on the
set of all stocks meta-values from given group deal. Also we can filter stocks
by statuses. Also we can pass limit-param-ids - to limit params search, this will affect
only visible parameters(used for filter by arrival for parameters like :size)"
  [params]
  (->>
   (meta:get-filters-for-stocks-with-count params)
   (group-by :name)
   (fmap (comp vec sort (partial map :value)))))

(defn- meta:get-counts-for-stocks
  [params]
  (->>
   (meta:get-filters-for-stocks-with-count params)
   (group-by :name)
   (fmap (fn[values](fmap :count (group-by-single :value values))))))
