(in-ns 'gd.model.model)

(comment "Contains user view of categories tree(with filtering by stocks and group deals)")

(declare stock)
(declare stock-status)
(declare supplier)
(declare left-stocks)
(declare stock-context:context-status-general-filter)
(declare site-pages:annotate-categories)

(defn- non-empty-category-model
  "Build map {category-id -> count}"
  []
  (binding [*count-query* true]
    (->
     (select* stock)
     (aggregate "count(distinct stock.id)" :count)
     (dry-transforms)
     (fields [:fkey_category :id])
     (group :fkey_category)
     (where {:stock.status (stock-status :visible)})
     visible-stocks
     (where {:stock.fkey_supplier *current-supplier*})
     (stock-context:context-status-general-filter nil :or))))

(defn category:model
  "Extract full categories model into memory, and represent it as simple list.
Also keep only categories that have assigned stocks to it, and all parents."
  []
  (let [build-map-from-results
        (fn[results]
          (->> results
               (group-by :id)
               (fmap (comp :count first))))

        non-empty-start-categories
        (build-map-from-results (select (non-empty-category-model)))

        all-categories
        (map
         (fn[cat] (assoc cat :stocks (get non-empty-start-categories (:id cat))))
         (category:whole-model))

        non-empty-categories
        (loop [res (set (keys non-empty-start-categories))]
          (let [new-parents (filter #(res (:id %)) all-categories)
                new-res (set (concat (map :fkey_category new-parents)
                                     (map :id new-parents)
                                     res))]
            (if (= new-res res)
              new-res
              (recur new-res))))

        category-tree
        (category:build-tree-path (filter #(non-empty-categories (:id %)) all-categories) nil)

        compute-stocks-for-category
        (fn[{:keys [child stocks] :as category}]
          (assoc category
            :stocks
            (+ (or stocks 0) (reduce + (remove nil? (map :stocks (vals child)))))))

        ;; function for check if category is real(:parent entities does not contains :child)
        real-category?
        #(and (map? %) (:id %) (contains? % :child))

        compute-aggregate-stock-numbers
        (walk/postwalk
         (fn[category]
           (if (real-category? category)
             (compute-stocks-for-category category)
             category))
         category-tree)

        categories
        (let [result (atom nil)]
          (walk/postwalk (fn[category]
                           (when (real-category? category)
                             (swap! result conj category))
                           category)
                         compute-aggregate-stock-numbers)
          @result)]
    (site-pages:annotate-categories categories)))

(defn category:child-categories
  "Extract all child categories of given category. If id - nil, returns top level ategories"
  [id]
  (sort-by
   :name
   (if (nil? id)
     (filter :top-category (category:model))
     (filter (comp (partial = id) :fkey_category) (category:model)))))

(defn category:tranform-path-to-categories
  "Transform string path to list of entities.
Example : /parent/child1 will be transformed to list of entites {parent, child1}"
  [path]
  (let [path (or path "")]
    (reverse
     (reduce
      (fn[[head :as result] path-element]
        (cons
         (some #(and (= (:short-name %) path-element) %)
               (category:child-categories (:id head)))
         result))
      nil
      (remove strings/blank? (strings/split path #"/"))))))

(defn category:tranform-path-to-current-category
  "Extract last category from string path"
  [path]
  (last (category:tranform-path-to-categories path)))

(defn category:user-tree
  "Build global tree with ALL categories(usefull for admin page)"
  []
  (category:build-tree-path (category:model) nil))

(defn category:get-by-id[id]
  (find-by-val :id id (category:model)))
