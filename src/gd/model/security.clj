(in-ns 'gd.model.model)

(defmacro defsecurity[f-var args body]
  (let [security-name (symbol (str f-var "-security"))]
    `(do
       (defn ~security-name [f# & [~@args :as args#]]
         (when-not ~body
           (throwf "Security violation"))
         (apply f# args#))
       (add-hook (var ~f-var) (var ~security-name)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; User management security

;; TODO : allow only root to change roles. Now too many tests are not passing.
;; (defsecurity user:save-or-update-roles[user-id roles]
;;   (or (every? #{"user"} roles) (sec/root?)))

(defsecurity user:login-as-user[]
  (sec/admin?))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Group deals security check

(defn- check-role-and-owner[user]
  (or (sec/root?)
      (and (= (:id (sec/logged)) (:id user)) (sec/organizer? user))))

(defsecurity stock:save-stock[container-id entity & {:keys [container-key]}]
  true)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Messages security check
(defn- message:check-author[message-id]
  (let [{:keys [author type recipient]} (select-single message (where {:id message-id}))
        current (:id (sec/logged))]
    (case type
      :private (or (= current author) (= current recipient))
      (or (= current author) (sec/admin?)))))

(defsecurity message:update [id] (message:check-author id))
(defsecurity message:remove [id] (message:check-author id))
(defsecurity message:restore [id] (message:check-author id))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Money transactions security

(defsecurity account:transact-money[] (sec/root?))
(defsecurity account:reject-transaction[] (sec/root?))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Retail security

(defn- retail:check-item-owner [order-item-id]
  (or (sec/admin?)
      (= (:id (sec/logged))
         (get-in
          (select-single user-order-item (with retail-order) (where {:id order-item-id}))
          [:retail-order :fkey_user]))))

(defn- retail:check-order-owner [order-id]
  (or (sec/admin?)
      (= (:id (sec/logged))
         (:fkey_user (select-single retail-order (where {:id order-id}))))))

(defsecurity retail:delete-order-item [user-order-id]
  (retail:check-item-owner user-order-id))

(defsecurity retail:change-order-item [user-order-id]
  (retail:check-item-owner user-order-id))

(defsecurity retail:change-order-additional-info [order-id]
  (retail:check-order-owner order-id))

(defsecurity retail:change-order [order-items]
  (let [owners
        (->>
         (select user-order-item (with retail-order) (where {:id [in (map :id order-items)]}))
         (map (comp :fkey_user :retail-order))
         distinct)

        check-owner
        (fn[user-id] (or (sec/admin?) (= (:id (sec/logged)) user-id)))]
    (and (= (count owners) 1)
         (every? check-owner owners))))
