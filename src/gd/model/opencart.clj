(in-ns 'gd.model.model)

(comment "sources:
  opencart-template-instance - dir with defaul opencart (source for copy);
  resources/integration - dir with custom .template files for nginx and opencart configuration;
  resources/integration/opencart-dump.sql - default db backup;

  requirements:
  chmod 777    /etc/hosts
  chmod -R 777 /etc/nginx/sites-available
  chmod -R 777 /etc/nginx/sites-enabled
  chmod -R 777 /home/webserver/opencarts/")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;constants
(defn try-sh [& args]
  (let [e (:err (apply sh args))]
    (when (seq e)
      (throwf e))))

(def mysql-pswd "SjC5Xj68s26U3NjR")
(def webserver-path "/home/webserver/")
(def opencarts-path (str webserver-path "opencarts/"))
(def opencart-default "/home/webserver/opencart-template-instance/")
(def mysql-fn         (partial sh "mysql" "-h" "127.0.0.1" "-P" "3306" "-u" "root" (str "-p" mysql-pswd) "-Bse"))
(defn tpl [name] (str "integration/" name ".template"))
(defn fill-data [name] {:name  name
                        :frontend   (str name ".sc24.com.ua")
                        :root       (str opencarts-path name "/")
                        :smart-root (str webserver-path "gd-clojure/")
                        :dbpass mysql-pswd})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Methods

(defn create-instance [instance]
  (let [data (fill-data instance)
        config-content      (render-resource (tpl "oc-config") data)
        integration-content (render-resource (tpl "integration") data)]

      ;opencart copy
      (try-sh "cp" "-npRH" opencart-default (str opencarts-path instance "/"))
      (spit (str opencarts-path instance "/config.php") config-content)
      (spit (str opencarts-path instance "/admin/config.php") config-content)
      (spit (str opencarts-path instance "/integration.php") integration-content)

      ;db-create-restore
      (mysql-fn (str "CREATE DATABASE " instance " ;"))
      (mysql-fn (str "use " instance "; source /home/webserver/gd-clojure/resources/integration/" "opencart-dump.sql;"))))

;TODO to manual disabling instanses should generate nginx "!<name>" file with hardcode server_name
