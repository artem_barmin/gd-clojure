#!/bin/bash
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/bin/X11"
export HOME=/home/webserver
export LANG=en_US.UTF-8
export TZ=Europe/Zaporozhye
export MAIL=/var/mail/webserver
export SHELL=/bin/bash
export USER=webserver

cd /home/webserver/gd-clojure
killall java
lein deps
echo "Lein deps completed"
nohup lein trampoline run -m gd.external > /home/webserver/log.out &
echo "Server started"
